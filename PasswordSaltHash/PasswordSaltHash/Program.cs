﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PasswordSaltHash
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] salt = CreateSalt(50);

            string saltString = Convert.ToBase64String(salt);

            //Insert Password here
            string password = "";

            byte[] pwd = new byte[password.Length * sizeof(char)];
            System.Buffer.BlockCopy(password.ToCharArray(), 0, pwd, 0, pwd.Length);

            byte[] hashedPwd = CreatePasswordHash(pwd, salt);
            string hashString = Convert.ToBase64String(hashedPwd);
        }

        private static byte[] CreateSalt(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);

            return buff;
        }

        private static byte[] CreatePasswordHash(byte[] pwd, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] saltPwd = new byte[pwd.Length + salt.Length];

            for (int i = 0; i < pwd.Length; i++)
            {
                saltPwd[i] = pwd[i];
            }

            for (int i = 0; i < salt.Length; i++)
            {
                saltPwd[pwd.Length + i] = salt[i];
            }

            return algorithm.ComputeHash(saltPwd);
        }
    }
}
