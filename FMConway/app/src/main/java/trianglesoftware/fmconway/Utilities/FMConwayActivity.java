package trianglesoftware.fmconway.Utilities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Accident.SelectAccidentActivity;
import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Comment.SelectCommentActivity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Enums.TaskTypeEnum;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.Observation.SelectObservationActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Task.AreaCallActivity;
import trianglesoftware.fmconway.Task.PhotosActivity;
import trianglesoftware.fmconway.Task.TaskEquipmentActivity;
import trianglesoftware.fmconway.Task.FreeTextDetailActivity;
import trianglesoftware.fmconway.Task.FreeTextDetailPhotoActivity;
import trianglesoftware.fmconway.Task.FreeTextTaskActivity;
import trianglesoftware.fmconway.Task.SignatureTaskActivity;
import trianglesoftware.fmconway.Task.TaskChecklistActivity;
import trianglesoftware.fmconway.Task.TaskWeatherActivity;
import trianglesoftware.fmconway.Task.TrafficCountActivity;

/**
 * Created by Jamie.Dobson on 05/05/2016.
 * Note: Set shiftID before calling super.onCreate in inherited class else Home button wont work correctly
 */
public abstract class FMConwayActivity extends FMConwayActivityBase {
    private String shiftID;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(getLayoutResourceId());
        shiftID = setShiftID();

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        TextView header = (TextView) findViewById(R.id.header_title);
        header.setText(setHeader());

        Button backToHomeButton = (Button) findViewById(R.id.back_to_home);
        backToHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToHome(v);
            }
        });

        Button backButton = (Button) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back(v);
            }
        });

        Button observationButton = (Button) findViewById(R.id.observationButton);
        observationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observationClick(v);
            }
        });

        Button accidentButton = (Button) findViewById(R.id.accidentButton);
        accidentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accidentClick(v);
            }
        });

        Button riskAssessmentButton = (Button) findViewById(R.id.riskAssessmentButton);
        riskAssessmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                riskAssessmentClick(v);
            }
        });

        Button commentButton = (Button) findViewById(R.id.commentButton);
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentClick(v);
            }
        });

        // logo
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest" && headerLogo != null) {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }
    }

    protected abstract int getLayoutResourceId();

    protected abstract String setShiftID();

    protected abstract String setHeader();

    private void backToHome(View view) {
        User user = User.getUser(sp.getString("UserID", ""));
        boolean isCrewMember = Objects.equals(user.employeeType, "Crew Member");

        if (Objects.equals(shiftID, "")) {
            Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
            noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            noShiftIntent.putExtra("ShiftID", shiftID);
            noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
            startActivity(noShiftIntent);
        } else if (isCrewMember) {
            Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
            startIntent.putExtra("UserID", sp.getString("UserID", ""));
            startActivity(startIntent);
        } else {
            Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
            startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startIntent.putExtra("ShiftID", shiftID);
            startIntent.putExtra("UserID", sp.getString("UserID", ""));
            startActivity(startIntent);
        }
    }

    private void back(View view) {
        finish();
    }

    private void observationClick(View view) {
        Intent selectObservationActivity = new Intent(getApplicationContext(), SelectObservationActivity.class);
        selectObservationActivity.putExtra("ShiftID", shiftID);
        startActivity(selectObservationActivity);
    }

    private void riskAssessmentClick(View view) {
        JobPack jobPack = JobPack.GetJobPacksForShift(shiftID).get(0);
        Activity activity = Activity.GetActivitiesForJobPack(jobPack.jobPackID).get(0);
        List<Task> tasks = Task.GetTasksForActivity(activity.activityID);

        for (Task task : tasks) {
            if (TaskTypeEnum.get(task.taskTypeID) == TaskTypeEnum.Checklist) {
                Checklist checklist = Checklist.getChecklist(task.checklistID);
                if (checklist.isJettingRiskAssessment || checklist.isTankeringRiskAssessment || checklist.isCCTVRiskAssessment) {
                    TaskSelectionBase(task, tasks.size(), jobPack.jobPackID, shiftID);
                    return;
                }
            }
        }

        Toast.makeText(getApplicationContext(), "No risk assessment checklist task found in this workflow", Toast.LENGTH_LONG).show();
    }

    private void accidentClick(View view) {
        Intent selectAccidentActivity = new Intent(getApplicationContext(), SelectAccidentActivity.class);
        selectAccidentActivity.putExtra("ShiftID", shiftID);
        startActivity(selectAccidentActivity);
    }

    private void commentClick(View view) {
        Intent selectCommentActivity = new Intent(getApplicationContext(), SelectCommentActivity.class);
        selectCommentActivity.putExtra("ShiftID", shiftID);
        startActivity(selectCommentActivity);
    }

    public void TaskSelection(Task task, int taskCount, String jobPackID) {
        TaskSelectionBase(task, taskCount, jobPackID, shiftID);
    }
}

