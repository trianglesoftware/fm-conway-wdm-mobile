package trianglesoftware.fmconway.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.location.Location;
import android.location.LocationManager;
import android.os.Environment;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.ActivitySignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskSignature;
import trianglesoftware.fmconway.R;


public class Signature extends FMConwayActivityBase {

    private SignatureView sigView;
    private String shiftID;
    private String vehicleID;
    private String briefingID;
    private String activityID;
    private String staffID;
    private String userID;
    private String loggedInUserID;
    private String briefingStart;
    private String briefingEnd;
    private String taskID;
    private String client;
    private String loadingSheetID;

    private boolean showPrintedName;
    private EditText printedNameEditText;

    private boolean showClientVehicleReg;
    private EditText clientVehicleRegEditText;

    //public boolean lineDrawn;

    private String disclaimerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_signature);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID");
            vehicleID = extras.getString("VehicleID");
            briefingID = extras.getString("BriefingID");
            activityID = extras.getString("ActivityID");
            staffID = extras.getString("StaffID");
            userID = extras.getString("UserID");
            loggedInUserID = extras.getString("LoggedIn");
            briefingStart = extras.getString("BriefingStart");
            briefingEnd = extras.getString("BriefingEnd");
            taskID = extras.getString("TaskID");
            client = extras.getString("Client");
            loadingSheetID = extras.getString("LoadingSheetID");
            showPrintedName = extras.getBoolean("ShowPrintedName", false);
            showClientVehicleReg = extras.getBoolean("ShowClientVehicleReg", false);
        }

        TextView signatureDisclaimer = (TextView) findViewById(R.id.signature_disclaimer);

        sigView = (SignatureView) findViewById(R.id.signaturePanel);
        sigView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        sigView.layout(0, 0, sigView.getMeasuredWidth(), sigView.getMeasuredHeight());
        sigView.signatureActivity = this;

        sigView.setDrawingCacheEnabled(true);
        sigView.buildDrawingCache(false);

        if ((briefingID != null || activityID != null || taskID != null) && staffID != null) {
            disclaimerText = Staff.GetStaffName(staffID);
        } else if (loadingSheetID != null) {
            disclaimerText = "Staff Signature";
        } else {
            disclaimerText = "Client Signature";
        }

        signatureDisclaimer.setText(disclaimerText);

        printedNameEditText = (EditText) findViewById(R.id.printed_name);

        if (!showPrintedName) {
            printedNameEditText.setVisibility(View.GONE);
        }

        clientVehicleRegEditText = (EditText) findViewById(R.id.vehicle_reg);

        if (!showClientVehicleReg) {
            clientVehicleRegEditText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void clearSig(View view) {
        sigView.clear();
    }

    public void saveSig(View view) {
        if (!sigView.lineDrawn) {
            displayToast("Please add a signature.");
            return;
        }

        if (showPrintedName) {
            String printedName = printedNameEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(printedName)) {
                displayToast("Name for signature is required.");
                return;
            }
        }

        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            displayToast(R.string.location_error);
            return;
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null) {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        double longitude = 0;
        double latitude = 0;

        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        try {
            Bitmap b = sigView.getBitmap();

            // External sdcard location
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "FMConway");

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    displayToast("Unable to create data directory.");
                }
            }

            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "Signature-" + timeStamp + ".jpg");

            b.compress(Bitmap.CompressFormat.PNG, 90, new FileOutputStream(mediaFile));

            AddData(mediaFile.getPath(), longitude, latitude);

            //FMConwayUtils.openImage(this, mediaFile.getPath());
        } catch (Exception e) {
            ErrorLog.CreateError(e, "Save Signature");
            displayToast("Failed to save the signature. Please, try again.");
        }
    }

    private void AddData(String imageLocation, double longitude, double latitude) {
        String printedName = null;
        if (showPrintedName) {
            printedName = printedNameEditText.getText().toString();
        }

        String clientVehicleReg = null;
        if (showClientVehicleReg) {
            clientVehicleReg = clientVehicleRegEditText.getText().toString();
        }

        if (shiftID != null && vehicleID != null && loadingSheetID != null) {
            LoadingSheetSignature sig = new LoadingSheetSignature();
            sig.loadingSheetID = loadingSheetID;
            sig.shiftID = shiftID;
            sig.vehicleID = vehicleID;
            sig.userID = userID;
            sig.longitude = longitude;
            sig.latitude = latitude;
            sig.imageLocation = imageLocation;
            sig.time = new Date();
            LoadingSheetSignature.addLoadingSheetSignature(sig);

            finish();
        } else if (briefingID != null && staffID != null) {
            BriefingChecklistSignature sig = new BriefingChecklistSignature();
            sig.setBriefingID(briefingID);
            sig.setStaffID(staffID);
            sig.setLongitude(longitude);
            sig.setLatitude(latitude);
            sig.setImageLocation(imageLocation);
            sig.setUserID(loggedInUserID);
            sig.shiftID = shiftID;
            BriefingChecklistSignature.addBriefingChecklistSignature(sig);
            BriefingChecklistSignature.UpdateSignature(briefingID, staffID, briefingStart, briefingEnd);

            finish();
        } else if (activityID != null && staffID != null) {
            ActivitySignature sig = new ActivitySignature();
            sig.setActivityID(activityID);
            sig.setStaffID(staffID);
            sig.setLongitude(longitude);
            sig.setLatitude(latitude);
            sig.setImageLocation(imageLocation);
            sig.setUserID(userID);
            sig.shiftID = shiftID;
            ActivitySignature.addActivitySignature(sig);

            finish();
        } else if (taskID != null && staffID != null) {
            TaskSignature sig = new TaskSignature();
            sig.taskSignatureID = UUID.randomUUID().toString();
            sig.staffID = staffID;
            sig.isClient = false;
            sig.taskID = taskID;
            sig.latitude = latitude;
            sig.longitude = longitude;
            sig.imageLocation = imageLocation;
            sig.userID = loggedInUserID;
            sig.shiftID = shiftID;
            sig.printedName = printedName;
            sig.clientVehicleReg = clientVehicleReg;

            TaskSignature.addTaskSignature(sig);

            finish();
        } else if (taskID != null && client != null) {
            TaskSignature sig = new TaskSignature();
            sig.taskSignatureID = UUID.randomUUID().toString();
            sig.staffID = staffID;
            sig.isClient = true;
            sig.taskID = taskID;
            sig.latitude = latitude;
            sig.longitude = longitude;
            sig.imageLocation = imageLocation;
            sig.userID = loggedInUserID;
            sig.shiftID = shiftID;
            sig.printedName = printedName;
            sig.clientVehicleReg = clientVehicleReg;

            TaskSignature.addTaskSignature(sig);

            finish();
        }
    }

    public static class SignatureView extends View {
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private final Path mPath;
        private final Paint mBitmapPaint;
        public boolean lineDrawn = false;

        public final Paint mPaint;
        public Activity signatureActivity;

        public SignatureView(Context c) {
            super(c);

            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);

            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setColor(0xFF000000);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(6);
        }

        public SignatureView(Context c, AttributeSet attrs) {
            super(c, attrs);

            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);

            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setColor(0xFF000000);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(6);
        }

        public void clear() {
            if (mCanvas != null) {
                mBitmap = Bitmap.createBitmap(mCanvas.getWidth(), mCanvas.getHeight(), Bitmap.Config.ARGB_8888);
                mCanvas = new Canvas(mBitmap);

                this.invalidate();
            }
        }

        public Bitmap getBitmap() {
            Bitmap b = Bitmap.createBitmap(mCanvas.getWidth(), mCanvas.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(b);
            draw(canvas);
            return b;
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            if(mBitmap == null) {
                mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
                mCanvas = new Canvas(mBitmap);

                mCanvas.drawColor(0xFFFFFFFF);
                mCanvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
                mCanvas.drawPath(mPath, mPaint);

                if (signatureActivity != null) {
                    // if the keyboard is open on the previous activity when open the signature activity, this causes the height ('h') to create a small bmp which then
                    // doesn't allow user to draw to bottom of screen where the keyboard once was. I've changed manifest from adjustResize to stateHidden which forces the
                    // keyboard hidden at first which correctly calculates the full sized height, then we reset the layout back to adjustResize here once mBitmap has been created
                    signatureActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                }
            }
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(0xFFFFFFFF);
            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
            canvas.drawPath(mPath, mPaint);
        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        /*public void newCanvas() {
            mBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);

            this.invalidate();
        }*/

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
            mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            lineDrawn = true;

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }
    }

}

