package trianglesoftware.fmconway.Utilities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.Task.ActivityDetailsActivity;
import trianglesoftware.fmconway.Task.ActivityRiskAssessmentsActivity;
import trianglesoftware.fmconway.Task.AreaCallActivity;
import trianglesoftware.fmconway.Task.CleansingLogsActivity;
import trianglesoftware.fmconway.Task.FreeTextDetailActivity;
import trianglesoftware.fmconway.Task.FreeTextDetailPhotoActivity;
import trianglesoftware.fmconway.Task.FreeTextTaskActivity;
import trianglesoftware.fmconway.Task.PhotosActivity;
import trianglesoftware.fmconway.Task.SignatureTaskActivity;
import trianglesoftware.fmconway.Task.TaskChecklistActivity;
import trianglesoftware.fmconway.Task.TaskEquipmentActivity;
import trianglesoftware.fmconway.Task.TaskWeatherActivity;
import trianglesoftware.fmconway.Task.TrafficCountActivity;
import trianglesoftware.fmconway.Task.UnloadingProcessesActivity;

public abstract class FMConwayActivityBase extends Activity {
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        
        // no need to check / request permissions on earlier sdk versions, just add permissions to the manifest
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            ArrayList<String> permissions = new ArrayList<String>();
            
            if (checkCallingOrSelfPermission("android.permission.INTERNET") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.INTERNET");
            }
            if (checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.ACCESS_NETWORK_STATE");
            }
            if (checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.ACCESS_FINE_LOCATION");
            }
            if (checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.ACCESS_COARSE_LOCATION");
            }
            if (checkCallingOrSelfPermission("android.permission.CAMERA") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.CAMERA");
            }
            if (checkCallingOrSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.READ_EXTERNAL_STORAGE");
            }
            if (checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == PackageManager.PERMISSION_DENIED) { 
                permissions.add("android.permission.WRITE_EXTERNAL_STORAGE");
            }
            if (checkCallingOrSelfPermission("android.permission.WRITE_INTERNAL_STORAGE") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.WRITE_INTERNAL_STORAGE");
            }
            if (checkCallingOrSelfPermission("android.permission.REQUEST_INSTALL_PACKAGES") == PackageManager.PERMISSION_DENIED) {
                permissions.add("android.permission.REQUEST_INSTALL_PACKAGES");
            }
            
            if (permissions.size() > 0) {
                requestPermissions(permissions.toArray(new String[0]), 1);
            }
        }
    }

    public void TaskSelectionBase(Task task, int taskCount, String jobPackID, String shiftID) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        // set shift progress TasksStarted
        if (!ShiftProgress.DoesProgressExist(shiftID, "TasksStarted")) {
            ShiftProgress.SetShiftProgress(shiftID, "TasksStarted");
            ShiftProgress.SyncProgress();
        }

        if (task != null) {
            switch (task.getTaskTypeID()) {
                // Traffic Count
                case 1:
                    Intent trafficCountActivity = new Intent(getApplicationContext(), TrafficCountActivity.class);
                    trafficCountActivity.putExtra("JobPackID", jobPackID);
                    trafficCountActivity.putExtra("ShiftID", shiftID);
                    trafficCountActivity.putExtra("TaskID", task.taskID);
                    trafficCountActivity.putExtra("TaskCount", taskCount);
                    trafficCountActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(trafficCountActivity);

                    Task.StartTask(task.getTaskID());
                    break;
                // Area Call
                case 2:
                    Intent areaActivity = new Intent(getApplicationContext(), AreaCallActivity.class);
                    areaActivity.putExtra("JobPackID", jobPackID);
                    areaActivity.putExtra("ShiftID", shiftID);
                    areaActivity.putExtra("TaskID", task.taskID);
                    areaActivity.putExtra("TaskCount", taskCount);
                    areaActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(areaActivity);

                    Task.StartTask(task.taskID);
                    break;
                // Checklist
                case 3:
                    if (!TaskChecklist.CheckIfChecklistCreated(task.taskID, task.checklistID)) {
                        TaskChecklist tc = new TaskChecklist();
                        tc.taskChecklistID = UUID.randomUUID().toString();
                        tc.taskID = task.taskID;
                        tc.checklistID = task.checklistID;
                        tc.shiftID = shiftID;
                        TaskChecklist.addTaskChecklist(tc);

                        TaskChecklist newTaskChecklist = TaskChecklist.GetTaskChecklist(task.taskID, task.checklistID);

                        List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(task.checklistID);
                        for (int j = 0; j < checklistQuestions.size(); j++) {
                            TaskChecklistAnswer tca = new TaskChecklistAnswer();
                            tca.taskChecklistAnswerID = tc.taskChecklistID = UUID.randomUUID().toString();
                            tca.taskChecklistID = newTaskChecklist.taskChecklistID;
                            tca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                            tca.shiftID = shiftID;
                            tca.reason = "";
                            //tca.setAnswer(false);
                            TaskChecklistAnswer.addTaskChecklistAnswer(tca);
                        }
                    }

                    TaskChecklist taskChecklist = TaskChecklist.GetTaskChecklist(task.getTaskID(), task.getChecklistID());
                    Intent checklistActivity = new Intent(getApplicationContext(), TaskChecklistActivity.class);
                    checklistActivity.putExtra("JobPackID", jobPackID);
                    checklistActivity.putExtra("ShiftID", shiftID);
                    checklistActivity.putExtra("TaskID", task.taskID);
                    checklistActivity.putExtra("TaskCount", taskCount);
                    checklistActivity.putExtra("TaskChecklistID", taskChecklist.taskChecklistID);
                    checklistActivity.putExtra("ChecklistID", taskChecklist.checklistID);
                    checklistActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(checklistActivity);

                    Task.StartTask(task.getTaskID());
                    break;
                // Install
//                case 4:
//                    Intent installActivity = new Intent(getApplicationContext(), InstallActivity.class);
//                    installActivity.putExtra("JobPackID", jobPackID);
//                    installActivity.putExtra("ShiftID", shiftID);
//                    installActivity.putExtra("TaskID", task.getTaskID());
//                    installActivity.putExtra("TaskCount", taskCount);
//                    startActivity(installActivity);
//
//                    Task.StartTask(task.getTaskID());
//                    break;
                // Free Text
                case 5:
                    Intent freeTextActivity = new Intent(getApplicationContext(), FreeTextTaskActivity.class);
                    freeTextActivity.putExtra("JobPackID", jobPackID);
                    freeTextActivity.putExtra("ShiftID", shiftID);
                    freeTextActivity.putExtra("TaskID", task.taskID);
                    freeTextActivity.putExtra("TaskCount", taskCount);
                    freeTextActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(freeTextActivity);

                    Task.StartTask(task.taskID);
                    break;
                //Free Text with Detail
                case 6:

                    if (task.photosRequired) {
                        Intent freeTextDetailActivity = new Intent(getApplicationContext(), FreeTextDetailPhotoActivity.class);
                        freeTextDetailActivity.putExtra("JobPackID", jobPackID);
                        freeTextDetailActivity.putExtra("ShiftID", shiftID);
                        freeTextDetailActivity.putExtra("TaskID", task.taskID);
                        freeTextDetailActivity.putExtra("TaskCount", taskCount);
                        freeTextDetailActivity.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(freeTextDetailActivity);

                        Task.StartTask(task.taskID);
                    } else {
                        Intent freeTextDetailActivity = new Intent(getApplicationContext(), FreeTextDetailActivity.class);
                        freeTextDetailActivity.putExtra("JobPackID", jobPackID);
                        freeTextDetailActivity.putExtra("ShiftID", shiftID);
                        freeTextDetailActivity.putExtra("TaskID", task.taskID);
                        freeTextDetailActivity.putExtra("TaskCount", taskCount);
                        freeTextDetailActivity.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(freeTextDetailActivity);

                        Task.StartTask(task.taskID);
                    }
                    break;
                // Signatures
                case 7:
                    if (!FMConwayUtils.isNullOrWhitespace(task.checklistID) && !TaskChecklist.CheckIfChecklistCreated(task.taskID, task.checklistID)) {
                        TaskChecklist tc = new TaskChecklist();
                        tc.taskChecklistID = UUID.randomUUID().toString();
                        tc.taskID = task.taskID;
                        tc.checklistID = task.checklistID;
                        tc.shiftID = shiftID;
                        TaskChecklist.addTaskChecklist(tc);

                        TaskChecklist newTaskChecklist = TaskChecklist.GetTaskChecklist(task.taskID, task.checklistID);

                        List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(task.checklistID);
                        for (int j = 0; j < checklistQuestions.size(); j++) {
                            TaskChecklistAnswer tca = new TaskChecklistAnswer();
                            tca.taskChecklistAnswerID = UUID.randomUUID().toString();
                            tca.taskChecklistID = newTaskChecklist.taskChecklistID;
                            tca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                            tca.shiftID = shiftID;
                            tca.reason = "";
                            //tca.setAnswer(false);
                            TaskChecklistAnswer.addTaskChecklistAnswer(tca);
                        }
                    }

                    try {
                        String taskChecklistID = "";

                        if (!FMConwayUtils.isNullOrWhitespace(task.checklistID)) {
                            TaskChecklist taskSignatureChecklist = TaskChecklist.GetTaskChecklist(task.taskID, task.checklistID);
                            taskChecklistID = taskSignatureChecklist.taskChecklistID;
                        }

                        Intent taskSignatureActivity = new Intent(getApplicationContext(), SignatureTaskActivity.class);
                        taskSignatureActivity.putExtra("JobPackID", jobPackID);
                        taskSignatureActivity.putExtra("ShiftID", shiftID);
                        taskSignatureActivity.putExtra("TaskID", task.taskID);
                        taskSignatureActivity.putExtra("TaskCount", taskCount);
                        taskSignatureActivity.putExtra("TaskChecklistID", taskChecklistID);
                        taskSignatureActivity.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(taskSignatureActivity);
                    } catch (Exception e) {
                        ErrorLog.CreateError(e, "SignatureTask");
                    }

                    Task.StartTask(task.taskID);
                    break;

                case 8: // Equipment Install task
                    Intent equipmentInstallTask = new Intent(getApplicationContext(), TaskEquipmentActivity.class);
                    equipmentInstallTask.putExtra("ShiftID", shiftID);
                    equipmentInstallTask.putExtra("JobPackID", jobPackID);
                    equipmentInstallTask.putExtra("TaskCount", taskCount);
                    equipmentInstallTask.putExtra("TaskID", task.taskID);
                    equipmentInstallTask.putExtra("UserID", sp.getString("UserID", ""));
                    equipmentInstallTask.putExtra("IsInstallation", true);
                    equipmentInstallTask.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(equipmentInstallTask);
                    break;

                case 9: // Equipment Collection task
                    Intent equipmentCollectionTask = new Intent(getApplicationContext(), TaskEquipmentActivity.class);
                    equipmentCollectionTask.putExtra("ShiftID", shiftID);
                    equipmentCollectionTask.putExtra("JobPackID", jobPackID);
                    equipmentCollectionTask.putExtra("TaskCount", taskCount);
                    equipmentCollectionTask.putExtra("TaskID", task.taskID);
                    equipmentCollectionTask.putExtra("UserID", sp.getString("UserID", ""));
                    equipmentCollectionTask.putExtra("IsInstallation", false);
                    equipmentCollectionTask.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(equipmentCollectionTask);
                    break;

                case 10: // Photos task
                    Intent photosActivity = new Intent(getApplicationContext(), PhotosActivity.class);
                    photosActivity.putExtra("JobPackID", jobPackID);
                    photosActivity.putExtra("ShiftID", shiftID);
                    photosActivity.putExtra("TaskID", task.taskID);
                    photosActivity.putExtra("TaskCount", taskCount);
                    photosActivity.putExtra("UserID", sp.getString("UserID", ""));
                    photosActivity.putExtra("PhotosRequired", task.photosRequired);
                    photosActivity.putExtra("ShowDetails", task.showDetails);
                    photosActivity.putExtra("ShowClientSignature", task.showClientSignature);
                    photosActivity.putExtra("ClientSignatureRequired", task.clientSignatureRequired);
                    photosActivity.putExtra("Tag", task.tag);
                    startActivity(photosActivity);

                    Task.StartTask(task.taskID);
                    break;

                case 11: // Weather task
                    Intent weatherActivity = new Intent(getApplicationContext(), TaskWeatherActivity.class);
                    weatherActivity.putExtra("JobPackID", jobPackID);
                    weatherActivity.putExtra("ShiftID", shiftID);
                    weatherActivity.putExtra("TaskID", task.taskID);
                    weatherActivity.putExtra("TaskCount", taskCount);
                    startActivity(weatherActivity);

                    Task.StartTask(task.taskID);
                    break;

                case 12: // Battery
                    break;

                case 13: // Activity Details
                    Intent activityDetails = new Intent(getApplicationContext(), ActivityDetailsActivity.class);
                    activityDetails.putExtra("JobPackID", jobPackID);
                    activityDetails.putExtra("ShiftID", shiftID);
                    activityDetails.putExtra("TaskID", task.taskID);
                    activityDetails.putExtra("TaskCount", taskCount);
                    activityDetails.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(activityDetails);

                    Task.StartTask(task.taskID);
                    break;

                case 14: // Cleansing Logs
                    Intent cleansingLogsActivity = new Intent(getApplicationContext(), CleansingLogsActivity.class);
                    cleansingLogsActivity.putExtra("JobPackID", jobPackID);
                    cleansingLogsActivity.putExtra("ShiftID", shiftID);
                    cleansingLogsActivity.putExtra("TaskID", task.taskID);
                    cleansingLogsActivity.putExtra("TaskCount", taskCount);
                    cleansingLogsActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(cleansingLogsActivity);

                    Task.StartTask(task.taskID);
                    break;

                case 15: // Unloading Processes
                    Intent unloadingProcessesActivity = new Intent(getApplicationContext(), UnloadingProcessesActivity.class);
                    unloadingProcessesActivity.putExtra("JobPackID", jobPackID);
                    unloadingProcessesActivity.putExtra("ShiftID", shiftID);
                    unloadingProcessesActivity.putExtra("TaskID", task.taskID);
                    unloadingProcessesActivity.putExtra("TaskCount", taskCount);
                    unloadingProcessesActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(unloadingProcessesActivity);

                    Task.StartTask(task.taskID);
                    break;

                case 16: // Activity Details
                    Intent activityRiskAssessments = new Intent(getApplicationContext(), ActivityRiskAssessmentsActivity.class);
                    activityRiskAssessments.putExtra("JobPackID", jobPackID);
                    activityRiskAssessments.putExtra("ShiftID", shiftID);
                    activityRiskAssessments.putExtra("TaskID", task.taskID);
                    activityRiskAssessments.putExtra("TaskCount", taskCount);
                    activityRiskAssessments.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(activityRiskAssessments);

                    Task.StartTask(task.taskID);
                    break;

                default:
                    break;
            }
        }
    }

    protected void displayToast(final String msg){
        FMConwayActivityBase.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast _toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
                _toast.show();
            }
        });
    }

    protected void displayToast(final int stringID){
        FMConwayActivityBase.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast _toast = Toast.makeText(getApplicationContext(), getString(stringID), Toast.LENGTH_LONG);
                _toast.show();
            }
        });
    }
    
}

