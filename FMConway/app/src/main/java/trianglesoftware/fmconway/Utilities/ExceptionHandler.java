package trianglesoftware.fmconway.Utilities;

import java.io.PrintWriter;
import java.io.StringWriter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;

/**
 * Created by Adam.Patrick on 06/09/2016.
 */

public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
    private final Context myContext;
    private final String LINE_SEPARATOR = "\n";
    private SharedPreferences sp;

    public ExceptionHandler(Context context) {
        myContext = context;
    }

    public void uncaughtException(Thread thread, Throwable exception) {

        sp = PreferenceManager.getDefaultSharedPreferences(myContext);
        String userID = sp.getString("UserID", "");

        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        errorReport.append(stackTrace.toString());

        errorReport.append("\n************ USER INFORMATION ***********\n");
        errorReport.append("UserID: ");
        errorReport.append(userID);

        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK_INT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(BuildConfig.VERSION_NAME);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);

        ErrorLog error = new ErrorLog();

        error.setDescription(myContext.getClass().getSimpleName());
        error.setException(errorReport.toString());

        ErrorLog.AddErrorLog(error);

        Intent intent = new Intent(myContext, ErrorActivity.class);
        intent.putExtra("error", errorReport.toString());
        intent.putExtra("description", myContext.getClass().getSimpleName());
        myContext.startActivity(intent);

//        android.os.Process.killProcess(android.os.Process.myPid());
//        System.exit(10);
    }

}
