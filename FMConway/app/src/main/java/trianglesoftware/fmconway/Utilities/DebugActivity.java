package trianglesoftware.fmconway.Utilities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseHelper;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.R;

public class DebugActivity extends FMConwayActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);

        Button pushDatabase = findViewById(R.id.pushDatabase);
        pushDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DebugActivity.this, R.style.Base_Theme_AppCompat_Light_Dialog);
                alertDialog.setTitle("Push Database");
                alertDialog.setMessage("Upload app database for inspection, are you sure?");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendDatabase();
                        dialog.cancel();
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });

        Button gpsTest = findViewById(R.id.gpsTest);
        gpsTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gpsTest();
            }
        });

        // logo
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest") {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sendDatabase() {
        try {
            String queryURL = this.getResources().getString(R.string.QUERY_URL);

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            String userName = (sp.getString("Username", ""));
            final String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

            File file = new File(getDataDir() + "/databases", DatabaseHelper.DATABASE_NAME);
            String base64 = FMConwayUtils.file2Base64(file);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DataString", base64);

            PublicParams urlParams = new PublicParams(queryURL + "PushDatabase", jsonObject.toString(), encodedHeader);
            new PublicPushData(new DataSync.PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    showToast("Database sent successfully!");
                }

                @Override
                public void onError(Throwable t) {
                    showToast("Failed to send database");
                }
            }).execute(urlParams).get();

        } catch (Exception ex) {
            showToast("Failed to send database");
        }
    }

    private void gpsTest() {
        LocationManager locationManager = (LocationManager) FMConwayApp.getContext().getSystemService(LOCATION_SERVICE);

        boolean hasAccessFineLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean hasAccessCoarseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        //Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude = 0;
        double latitude = 0;

        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            long time = location.getTime();

            Date date = new Date(time);
            DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateFormatted = formatter.format(date);

            showToast("longitude: " + location.getLongitude() + " latidude: " + location.getLatitude() + " time: " + dateFormatted);
        }

        LocationListener listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    showToast("longitude: " + location.getLongitude() + " latidude: " + location.getLatitude());
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        //locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, listener, null);
    }

    private void showToast(final String toast) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(FMConwayApp.getContext(), toast, Toast.LENGTH_LONG).show();
            }
        });
    }
}




