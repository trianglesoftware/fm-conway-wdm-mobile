package trianglesoftware.fmconway.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLogPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.R;

public class FMConwayUtils {

    public static SimpleDateFormat simpleIsoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public static SimpleDateFormat simpleShortFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    public static SimpleDateFormat simpleLongFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
    public static SimpleDateFormat simpleLongWithoutSecondsFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());

    public static Date getCursorDate(Cursor c, String column) {
        try {
            int index = c.getColumnIndex(column);
            if (!c.isNull(index)) {
                return simpleIsoFormat.parse(c.getString(index));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // dateString: /Date(1662537600000+0100)/
    public static Date parseAspNetDate(String dateString) {
        try {
            if (isNullOrWhitespace(dateString)) {
                return null;
            }
            Calendar date = Calendar.getInstance();
            String dateStringTrimmed = dateString.replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
            long timeInMillis = Long.parseLong(dateStringTrimmed);
            date.setTimeInMillis(timeInMillis);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDateIsoString(Date date) {
        if (date != null) {
            return simpleIsoFormat.format(date);
        }
        return null;
    }

    public static String getShortDateString(Date date) {
        if (date != null) {
            return simpleShortFormat.format(date);
        }
        return null;
    }

    public static String getLongDateString(Date date) {
        if (date != null) {
            return simpleLongFormat.format(date);
        }
        return null;
    }

    public static String getLongDateStringWithoutSeconds(Date date) {
        if (date != null) {
            return simpleLongWithoutSecondsFormat.format(date);
        }
        return null;
    }

    public static Date jsonDateToDate(String string) {
        if (string == null || string.equals("null")) {
            return null;
        }

        Calendar startTime = Calendar.getInstance();
        String startTimeString = string.replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
        Long timeInMillis = Long.valueOf(startTimeString);
        startTime.setTimeInMillis(timeInMillis);
        return startTime.getTime();
    }

    public static Double getDouble(Cursor c, String column) {
        try {
            int index = c.getColumnIndex(column);
            if (!c.isNull(index)) {
                return c.getDouble(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Uri GetUri(Context context, File file){
        return FileProvider.getUriForFile(context,context.getString(R.string.provider_name), file);
    }

    public static void saveBitmap(Bitmap bitmap, String path, int quality) {
        if (bitmap != null) {
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // reduceResolution uses the inSampleSize decoder to scale down the image by a factor of eg 2,3,4 etc
    // making the image resolution more transferable friendly, reducing its data size without dropping below a 400 x 400 minimum resolution
    public static void saveBitmap(Bitmap bitmap, String filepath, int quality, boolean reduceResolution) {
        if (reduceResolution) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] bytes = baos.toByteArray();

            // use image bounds to calculate inSampleSize (0 = full resolution, resolution is divided by a factor of this)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.outWidth = bitmap.getWidth();
            options.outHeight = bitmap.getHeight();
            options.inSampleSize = FMConwayUtils.calculateInSampleSize(options, 400, 400);

            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        }

        FMConwayUtils.saveBitmap(bitmap, filepath, quality);
    }
    
    public static Bitmap filepathToBitmap(String filepath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);
        return bitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    
    public static File getPictureFile(String filename) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");
        
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(FMConwayApp.getContext(), "Unable to create FM Conway folder within pictures", Toast.LENGTH_LONG).show();
            }
        }
        
        return new File(mediaStorageDir.getPath() + File.separator + filename);
    }

    public static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");
        
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(FMConwayApp.getContext(), "Unable to create FM Conway folder within pictures", Toast.LENGTH_LONG).show();
            }
        }
        
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    public static byte[] fileToByteArray(File file) {
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bytes;
    }
    
    public static String fileToBase64(File file) {
        return Base64.encodeToString(fileToByteArray(file), Base64.DEFAULT);
    }
    
    public static Bitmap bitmapRotationFix(Bitmap bmp) {
        // bitmap.compress which is called in saveBitmap will rotate the image on save when called for the
        // first time, we counter this by pre-countering the rotation. This isn't clean but will do for now.
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        return bmp;
    }
    
    public static Bitmap base64ToBmp(String base64) {
        byte[] byteArray = Base64.decode(base64, Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        return bmp;
    }

    public static boolean isNullOrWhitespace(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static Double parseDouble(String value, Double defaultValue) {
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static boolean isNumber(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String dateToString(Date date, String format, String defaultValue) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        if(date != null){
            return dateFormat.format(date);
        } else {
            return defaultValue;
        }
    }

    public static String file2Base64(File file){
        return android.util.Base64.encodeToString(file2Array(file), Base64.DEFAULT);
    }

    public static byte[] file2Array(File file){
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            //Logger.Log(e);
        } catch (IOException e) {
            //Logger.Log(e);
        }
        return bytes;
    }

    public static void confirmDialog(Context context, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Confirmation");
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Yes", onClickListener);
        alertDialog.setNegativeButton("Cancel", null);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public static void infoDialog(Context context, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Information");
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", onClickListener);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public static void warningDialog(Context context, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Warning");
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", onClickListener);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    // eg ChecksCompleted to "Checks Completed" (https://stackoverflow.com/a/2560017/3961743)
    public static String splitCamelCase(String s) {
        if (s == null) {
            return null;
        }
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    // test image compression and file size, opens image for inspection afterwards (overwrites original image)
    public static void debugImageCompression(Activity activity, String filepath) {
        // setImageQuality of 70 seems to be a good average between quality and file size
        int setImageQuality = 70;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // just decode image bounds in first decodeFile (options.outHeight and options.outWidth outputted)
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filepath, options);
        int width = options.outWidth;
        int height = options.outHeight;

        // use image bounds to calculate inSampleSize (0 = full resolution, resolution is divided by a factor of this)
        options.inSampleSize = FMConwayUtils.calculateInSampleSize(options, 400, 400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(filepath, options);
        width = bm.getWidth();
        height = bm.getHeight();

        // convert to byte array just to check file size
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, setImageQuality, baos);
        byte[] b = baos.toByteArray();
        String filesize = getFileSize(b);

        // overwrite filepath with compressed image then open
        FMConwayUtils.saveBitmap(bm, filepath, setImageQuality);
        openImage(activity, filepath);
    }

    public static String getFileSize(byte[] bytes) {
        long size = bytes.length;
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static void openImage(Activity activity, String filepath) {
        Uri uri = FileProvider.getUriForFile(activity.getApplicationContext(), activity.getApplicationContext().getPackageName() + ".provider", new File(filepath));

        String extension = MimeTypeMap.getFileExtensionFromUrl(filepath);
        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Intent fileIntent = new Intent(Intent.ACTION_VIEW);
        fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        fileIntent.setDataAndType(uri, type);

        activity.startActivity(fileIntent);
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (imm != null) {
                View view = activity.getCurrentFocus();
                view = view == null ? new View(activity) : view;
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "HideKeyboard");
        }
    }

    public static Date truncateTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static void setListViewHeightBasedOnChildren(ListView myListView) {
        ListAdapter adapter = myListView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View item = adapter.getView(i, null, myListView);
            item.measure(0, 0);
            totalHeight += item.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (adapter.getCount() - 1));
        myListView.setLayoutParams(params);
    }

    public static void copyFile(String sourceFilepath, String destinationFilepath) throws IOException {
        InputStream in = new FileInputStream(sourceFilepath);
        try {
            OutputStream out = new FileOutputStream(destinationFilepath);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}
