package trianglesoftware.fmconway.Utilities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Task.AreaCallActivity;
import trianglesoftware.fmconway.Task.InstallActivity;
import trianglesoftware.fmconway.Task.TaskChecklistActivity;
import trianglesoftware.fmconway.Task.TrafficCountActivity;

/**
 * Created by Jamie.Dobson on 14/04/2016.
 */
public abstract class JobPackActivity extends FMConwayActivityBase {
    private String jobPackID;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        setContentView(getLayoutResourceId());
        jobPackID = setJobPackID();

        Button backToJobPackButton = (Button) findViewById(R.id.back_to_job_pack);
        backToJobPackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToJobPack(v);
            }
        });
    }

    protected abstract int getLayoutResourceId();

    protected abstract String setJobPackID();

    private void backToJobPack(View view)
    {
//        Intent jobMain = new Intent(getApplicationContext(), JobPackMainActivity.class);
//        jobMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        jobMain.putExtra("JobPackID", jobPackID);
//        startActivity(jobMain);
    }

    protected void TaskSelection(Task task,int taskCount)
    {
        //task = Task.GetTaskFromActivity(task.getActivityID());
        if(task != null) {
            switch (task.getTaskTypeID()) {
                // Traffic Count
                case 1:
                    Intent trafficCountActivity = new Intent(getApplicationContext(), TrafficCountActivity.class);
                    trafficCountActivity.putExtra("JobPackID", jobPackID);
                    trafficCountActivity.putExtra("TaskID", task.taskID);
                    trafficCountActivity.putExtra("TaskCount", taskCount);
                    startActivity(trafficCountActivity);
                    break;
                // Area Call
                case 2:
                    Intent areaActivity = new Intent(getApplicationContext(), AreaCallActivity.class);
                    areaActivity.putExtra("JobPackID", jobPackID);
                    areaActivity.putExtra("TaskID", task.taskID);
                    areaActivity.putExtra("TaskCount", taskCount);
                    startActivity(areaActivity);
                    break;
                // Checklist
                case 3:
                    if(!TaskChecklist.CheckIfChecklistCreated(task.getTaskID(), task.getChecklistID()))
                    {
                        TaskChecklist tc = new TaskChecklist();
                        tc.setTaskID(task.taskID);
                        tc.setChecklistID(task.checklistID);
                        tc.setShiftID(Shift.GetShiftIDFromJobPack(jobPackID));
                        TaskChecklist.addTaskChecklist(tc);

                        TaskChecklist newTaskChecklist = TaskChecklist.GetTaskChecklist(task.getTaskID(), task.getChecklistID());

                        List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(task.checklistID);
                        for (int j = 0; j < checklistQuestions.size(); j++) {
                            TaskChecklistAnswer tca = new TaskChecklistAnswer();
                            tca.taskChecklistAnswerID = tc.taskChecklistID = UUID.randomUUID().toString();
                            tca.setTaskChecklistID(newTaskChecklist.taskChecklistID);
                            tca.setChecklistQuestionID(checklistQuestions.get(j).checklistQuestionID);
                            tca.setShiftID(Shift.GetShiftIDFromJobPack(jobPackID));
                            //tca.setAnswer(false);
                            TaskChecklistAnswer.addTaskChecklistAnswer(tca);
                        }
                    }

                    TaskChecklist taskChecklist = TaskChecklist.GetTaskChecklist(task.taskID, task.checklistID);
                    Intent checklistActivity = new Intent(getApplicationContext(), TaskChecklistActivity.class);
                    checklistActivity.putExtra("JobPackID", jobPackID);
                    checklistActivity.putExtra("TaskID", task.taskID);
                    checklistActivity.putExtra("TaskCount", taskCount);
                    checklistActivity.putExtra("TaskChecklistID", taskChecklist.taskChecklistID);
                    startActivity(checklistActivity);
                    break;
                // Install / Removal
                case 4:
                    Intent installActivity = new Intent(getApplicationContext(), InstallActivity.class);
                    installActivity.putExtra("JobPackID", jobPackID);
                    installActivity.putExtra("TaskID", task.taskID);
                    installActivity.putExtra("TaskCount", taskCount);
                    startActivity(installActivity);
                    break;
                default:
                    break;
            }
        }
    }
}
