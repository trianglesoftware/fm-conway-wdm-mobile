package trianglesoftware.fmconway.Utilities;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;

class PublicPushData extends AsyncTask<PublicParams, Void, Integer> {

    private final DataSync.PushDataListenerProgress _listener;

    public PublicPushData(DataSync.PushDataListenerProgress listener) {
        _listener = listener;
    }

    public PublicPushData() {
        _listener = null;
    }

    @Override
    protected Integer doInBackground(PublicParams... params) {
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL(params[0].url);

            HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            urlConn.setRequestProperty("Authorization", params[0].encodedHeader);

            urlConn.connect();

            String str = params[0].jsonData.toString();
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConn.getOutputStream();
            os.write(outputInBytes);
            os.close();

            int HttpResult = urlConn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(), "utf-8"));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();

                if (_listener != null) {
                    _listener.onComplete();
                }
            } else {
                System.out.println(urlConn.getResponseMessage());
                ErrorLog.CreateError(new Throwable(urlConn.getResponseMessage()), "PushData Failed");
                if (_listener != null) {
                    _listener.onError(new Throwable(urlConn.getResponseMessage()));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            ErrorLog.CreateError(e, "PushData Failed");
            if (_listener != null) {
                _listener.onError(e);
            }
        }

        return null;
    }
}
