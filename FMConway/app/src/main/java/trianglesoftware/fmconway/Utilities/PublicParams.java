package trianglesoftware.fmconway.Utilities;

class PublicParams {
    final String url;
    final String jsonData;
    final String encodedHeader;

    PublicParams(String Url, String JsonData, String EncodedHeader) {
        this.url = Url;
        this.jsonData = JsonData;
        this.encodedHeader = EncodedHeader;
    }
}
