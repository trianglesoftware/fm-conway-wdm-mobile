package trianglesoftware.fmconway.Utilities;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

public class MultipleClickHandler {
    private ImageView imageView;
    private int targetClicks;
    private OnMultipleClickListener onMultipleClickListener;

    private long timeout = 350;
    private int count = 0;
    private long lastSubmitTime;

    public MultipleClickHandler(ImageView imageView, final int targetClicks, final OnMultipleClickListener onMultipleClickListener) {
        this.imageView = imageView;
        this.targetClicks = targetClicks;
        this.onMultipleClickListener = onMultipleClickListener;

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long currentTimeMillis = System.currentTimeMillis();

                if (lastSubmitTime == 0 || currentTimeMillis - timeout <= lastSubmitTime) {
                    count++;

                    if (count >= targetClicks) {
                        // target clicks reached within timeout
                        onMultipleClickListener.onClick(count);
                        onMultipleClickListener.onFinalClick();
                        count = 0;
                    } else {
                        // additional click within timeout
                        onMultipleClickListener.onClick(count);
                    }
                } else {
                    // reset
                    count = 1;
                    onMultipleClickListener.onClick(count);
                }

                lastSubmitTime = currentTimeMillis;
            }
        });
    }

    public interface OnMultipleClickListener {
        public void onClick(int click);
        public void onFinalClick();
    }
}
