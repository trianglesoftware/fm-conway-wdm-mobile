package trianglesoftware.fmconway.Utilities;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.R;

public class ErrorActivity extends FMConwayActivityBase {
    private String errorString;
    private String description;
    private String shiftID;
    private String userID;
    private ErrorLog error;
    private TextView errorDetail;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            errorString = extras.getString("error");
            description = extras.getString("description");
            shiftID = extras.getString("ShiftID");
            userID = extras.getString("UserID");
        }

        errorDetail = (TextView)findViewById(R.id.error_details);

        errorDetail.setText(errorString);

        error = new ErrorLog();
        error.setDescription(description);
        error.setSent(false);
        error.setException(errorString);

        // logo
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest") {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }

//        int id = ErrorLog.AddErrorLog(error);
//
//        error.setLogID(id);

    }

//    public void submitError(View view)
//    {
//        if (error.getLogID() > 0)
//        {
//            try {
//                if(SendErrorLog(error))
//                {
//                    error.setSent(true);
//
//                    ErrorLog.UpdateErrorLog(error);
//
//                    Intent intent = new Intent(this, StartActivity.class);
//                    intent.putExtra("ShiftID", shiftID);
//                    intent.putExtra("UserID", userID);
//                    this.startActivity(intent);
//                }
//            }
//            catch (ExecutionException | InterruptedException e)
//            {
//
//            }
//        }
//
//    }

//    public boolean SendErrorLog(ErrorLog error) throws ExecutionException, InterruptedException
//    {
//        boolean result = false;
//
//        try {
//            sp = PreferenceManager.getDefaultSharedPreferences(this);
//
//            String username = (sp.getString("Username", ""));
//
//            String encodedHeader = "Basic " + Base64.encodeToString((username + ":" + sp.getString(username, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
//
//            String url = this.getResources().getString(R.string.QUERY_URL);
//
//            JSONArray errorLog = ErrorLog.GetErrorLogToSend(error.getLogID());
//            Params urlParams;
//            urlParams = new Params(url + "PushError", errorLog.toString(), encodedHeader);
//            new PushData().execute(urlParams).get();
//
//            result = true;
//        }
//        catch (ExecutionException e)
//        {
//            result = false;
//        }
//
//        return result;
//    }
}

/*class Params{
    final String url;
    final String jsonData;
    final String encodedHeader;

    Params(String Url, String JsonData, String EncodedHeader)
    {
        this.url = Url;
        this.jsonData = JsonData;
        this.encodedHeader = EncodedHeader;
    }
}*/

/*class PushData extends AsyncTask<Params, Void, Integer> {
    @Override
    protected Integer doInBackground(Params... params) {

        try {
            StringBuilder sb = new StringBuilder();
            URL url;
            HttpURLConnection urlConn;
            url = new URL(params[0].url);

            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            //urlConn.setRequestProperty("Content-Type", "applicat ion/json");
            urlConn.setRequestProperty("Authorization", params[0].encodedHeader);

            urlConn.connect();

            String str =  params[0].jsonData.toString();
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConn.getOutputStream();
            os.write(outputInBytes);
            os.close();

            int HttpResult = urlConn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(), "utf-8"));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();

                if(sb.toString().contains("true"))
                {
                    //Delete file from device
                    try {
                        JSONObject json = new JSONObject(params[0].jsonData);
                        String imageLocation = json.getString("ImageLocation");
                        File file = new File(imageLocation);
                        file.delete();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } else {
                System.out.println(urlConn.getResponseMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}*/


