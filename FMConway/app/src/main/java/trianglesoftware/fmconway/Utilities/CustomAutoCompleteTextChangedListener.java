package trianglesoftware.fmconway.Utilities;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;

import trianglesoftware.fmconway.LoadingSheetEquipment.SelectNewLoadingSheetEquipDialogActivity;
import trianglesoftware.fmconway.Staff.SelectNewStaffDialogActivity;
import trianglesoftware.fmconway.Vehicle.SelectNewVehicleDialogActivity;

/**
 * Created by Jamie.Dobson on 03/03/2016.
 */
public class CustomAutoCompleteTextChangedListener implements TextWatcher{
    private final Context context;
    private final int activityType;

    public CustomAutoCompleteTextChangedListener(Context context, int activityType){
        this.context = context;
        this.activityType = activityType;
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
    }

    @Override
    public void onTextChanged(CharSequence userInput, int start, int before, int count) {

        switch(activityType)
        {
            case 1:
                SelectNewStaffDialogActivity activityStaff = ((SelectNewStaffDialogActivity) context);
                activityStaff.item = activityStaff.getItemsFromDb(userInput.toString());

                // update the adapter
                activityStaff.myAdapter.notifyDataSetChanged();
                activityStaff.myAdapter = new ArrayAdapter<>(activityStaff, android.R.layout.simple_dropdown_item_1line, activityStaff.item);
                activityStaff.newStaff.setAdapter(activityStaff.myAdapter);
                break;
            case 2:
                SelectNewVehicleDialogActivity activityVehicle = ((SelectNewVehicleDialogActivity) context);
                activityVehicle.item = activityVehicle.getItemsFromDb(userInput.toString());

                // update the adapter
                activityVehicle.myAdapter.notifyDataSetChanged();
                activityVehicle.myAdapter = new ArrayAdapter<>(activityVehicle, android.R.layout.simple_dropdown_item_1line, activityVehicle.item);
                activityVehicle.newVehicle.setAdapter(activityVehicle.myAdapter);
                break;
            case 3:
                SelectNewLoadingSheetEquipDialogActivity activityLoadingSheetEquipment = ((SelectNewLoadingSheetEquipDialogActivity) context);
                activityLoadingSheetEquipment.item = activityLoadingSheetEquipment.getItemsFromDb(userInput.toString());

                // update the adapter
                activityLoadingSheetEquipment.myAdapter.notifyDataSetChanged();
                activityLoadingSheetEquipment.myAdapter = new ArrayAdapter<>(activityLoadingSheetEquipment, android.R.layout.simple_dropdown_item_1line, activityLoadingSheetEquipment.item);
                activityLoadingSheetEquipment.newItem.setAdapter(activityLoadingSheetEquipment.myAdapter);
                break;
        }

    }
}
