package trianglesoftware.fmconway.Utilities;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

/**
 * Created by Adam.Patrick on 07/10/2016.
 */
public class FMConwayApp extends Application {
    private static FMConwayApp mContext;
    public static boolean oneTimeAutoLoggedIn;
    private LocationManager locationManager;
    
    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getApplicationContext()));
        mContext = this;

        startGpsTracking();
    }

    // start GPS location updates if not already and have permission, set to 1 min interval
    public void startGpsTracking() {
        if (locationManager != null) {
            return;
        }

        boolean hasAccessFineLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean hasAccessCoarseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (!hasAccessFineLocation || !hasAccessCoarseLocation) {
            return;
        }

        locationManager = (LocationManager) FMConwayApp.getContext().getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60 * 1000, 1, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                // location changes are automatically stored in cache and picked up by the getLastKnownLocation() calls throughout the app
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });
    }

    public static FMConwayApp getContext() {
        return mContext;
    }
}
