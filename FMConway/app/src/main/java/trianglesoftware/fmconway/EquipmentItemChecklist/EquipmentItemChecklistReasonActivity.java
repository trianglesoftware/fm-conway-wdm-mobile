package trianglesoftware.fmconway.EquipmentItemChecklist;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentItemChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;

public class EquipmentItemChecklistReasonActivity extends FMConwayActivityBase {
    /*intents*/
    private String EquipmentItemChecklistAnswerID;

    /*other*/
    private EquipmentItemChecklistAnswer equipmentItemChecklistAnswer;
    private EditText reasonEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_equipment_item_checklist_reason);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            EquipmentItemChecklistAnswerID = extras.getString("EquipmentItemChecklistAnswerID", "");
        }

        equipmentItemChecklistAnswer = EquipmentItemChecklistAnswer.getAnswer(EquipmentItemChecklistAnswerID);

        reasonEditText = (EditText) findViewById(R.id.reasonEditText);
        reasonEditText.setText(equipmentItemChecklistAnswer.Reason);
    }

    public void submitReason_Click(View view) {
        equipmentItemChecklistAnswer.Reason = reasonEditText.getText().toString();
        equipmentItemChecklistAnswer.updateReason();
        finish();
    }
}
