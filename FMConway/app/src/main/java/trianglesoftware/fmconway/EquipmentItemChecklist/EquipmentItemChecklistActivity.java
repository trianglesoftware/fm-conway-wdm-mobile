package trianglesoftware.fmconway.EquipmentItemChecklist;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentItemChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class EquipmentItemChecklistActivity extends FMConwayActivity {
    /*intents*/
    private String shiftID;
    private String userID;
    private String loadingSheetEquipmentItemID;
    private String checklistID;

    /*other*/
    private ListView equipmentItemChecklist;
    private EquipmentItemChecklistAdapter equipmentItemChecklistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            userID = extras.getString("UserID", "");
            loadingSheetEquipmentItemID = extras.getString("LoadingSheetEquipmentItemID", "");
            checklistID = extras.getString("ChecklistID", "");
        }

        super.onCreate(savedInstanceState);

        equipmentItemChecklist = (ListView) findViewById(R.id.equipment_item_checklist);
        equipmentItemChecklistAdapter = new EquipmentItemChecklistAdapter(this, getLayoutInflater());
        equipmentItemChecklist.setAdapter(equipmentItemChecklistAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_equipment_item_checklist;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Equipment Checklist";
    }

    @Override
    public void onResume() {
        super.onResume();
        updateEquipmentItemChecklist();
    }
    
    public void updateEquipmentItemChecklist() {
        List<EquipmentItemChecklistAnswer> model = EquipmentItemChecklistAnswer.getAnswers(loadingSheetEquipmentItemID);
        equipmentItemChecklistAdapter.UpdateData(model);
    }

    public void answerYes(View view) {
        int pos = equipmentItemChecklist.getPositionForView(view);
        EquipmentItemChecklistAnswer equipmentItemChecklistAnswer = equipmentItemChecklistAdapter.getItem(pos);

        if (equipmentItemChecklistAnswer.Answer != null && equipmentItemChecklistAnswer.Answer) {
            equipmentItemChecklistAnswer.Answer = null;
        } else {
            equipmentItemChecklistAnswer.Answer = true;
        }

        equipmentItemChecklistAnswer.updateAnswer();
        updateEquipmentItemChecklist();
    }

    public void answerNo(View view) {
        View test = (View) view.getParent();

        int pos = equipmentItemChecklist.getPositionForView(view);
        EquipmentItemChecklistAnswer equipmentItemChecklistAnswer = equipmentItemChecklistAdapter.getItem(pos);
        equipmentItemChecklistAnswer.Answer = false;
        equipmentItemChecklistAnswer.updateAnswer();
        updateEquipmentItemChecklist();

        if (equipmentItemChecklistAnswer.Answer != null && !equipmentItemChecklistAnswer.Answer) {
            Intent intent = new Intent(getApplicationContext(), EquipmentItemChecklistReasonActivity.class);
            intent.putExtra("EquipmentItemChecklistAnswerID", equipmentItemChecklistAnswer.EquipmentItemChecklistAnswerID);
            startActivity(intent);
        }
    }
}
