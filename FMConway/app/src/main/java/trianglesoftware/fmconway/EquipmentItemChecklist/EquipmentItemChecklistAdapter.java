package trianglesoftware.fmconway.EquipmentItemChecklist;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentItemChecklistAnswer;
import trianglesoftware.fmconway.R;

public class EquipmentItemChecklistAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private List<EquipmentItemChecklistAnswer> answers;

    public EquipmentItemChecklistAdapter(Context context, LayoutInflater inflater) {
        this.inflater = inflater;
        answers = new LinkedList<EquipmentItemChecklistAnswer>();
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public EquipmentItemChecklistAnswer getItem(int position) {
        return answers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EquipmentItemChecklistAnswer answer = getItem(position);

        EquipmentItemChecklistAdapter.ViewHolder view;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_checklist_question, null);

            view = new EquipmentItemChecklistAdapter.ViewHolder();
            view.QuestionLabel = (TextView) convertView.findViewById(R.id.text_equipment_type);
            view.YesButton = (Button) convertView.findViewById(R.id.answer_yes);
            view.YesButton.setTag(view);
            view.NoButton = (Button) convertView.findViewById(R.id.answer_no);
            view.NoButton.setTag(view);

            convertView.setTag(view);
        } else {
            view = (EquipmentItemChecklistAdapter.ViewHolder) convertView.getTag();
        }

        view.EquipmentItemChecklistAnswerID = answer.EquipmentItemChecklistAnswerID;
        view.Answer = answer.Answer;

        view.QuestionLabel.setText(answer.Question);
        
        if (view.Answer == null) {
            view.YesButton.setBackgroundColor(Color.parseColor("#d6d7d7"));
            view.NoButton.setBackgroundColor(Color.parseColor("#d6d7d7"));
        } else if (view.Answer) {
            view.YesButton.setBackgroundColor(Color.parseColor("#41ab45"));
            view.NoButton.setBackgroundColor(Color.parseColor("#d6d7d7"));
        } else {
            view.YesButton.setBackgroundColor(Color.parseColor("#d6d7d7"));
            view.NoButton.setBackgroundColor(Color.parseColor("#FF6961"));
        }

        return convertView;
    }

    public void UpdateData(List<EquipmentItemChecklistAnswer> equipmentItemChecklistAnswers) {
        this.answers = equipmentItemChecklistAnswers;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        public String EquipmentItemChecklistAnswerID;
        public Boolean Answer;

        public TextView QuestionLabel;
        public TextView YesButton;
        public Button NoButton;
    }
}
