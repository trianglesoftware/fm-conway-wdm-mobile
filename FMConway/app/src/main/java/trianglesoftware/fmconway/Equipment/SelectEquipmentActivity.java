package trianglesoftware.fmconway.Equipment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Equipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class SelectEquipmentActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String userID;
    private String taskID;
    private String equipmentTypeID;
    private SharedPreferences sp;
    private EquipmentAdapter typeAdapter;
    private boolean isInstallation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
            taskID = extras.getString("TaskID", "");
            equipmentTypeID = extras.getString("EquipmentTypeID","");
            isInstallation = extras.getBoolean("IsInstallation",true);
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView typeList = (ListView) findViewById(R.id.select_equipment_list);
        typeAdapter = new EquipmentAdapter(this, getLayoutInflater());
        typeList.setAdapter(typeAdapter);
        typeList.setOnItemClickListener(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectEquipment");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_equipment;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Equipment";
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        TaskEquipment te = new TaskEquipment();
        te.TaskEquipmentID = UUID.randomUUID().toString();
        te.ShiftID = shiftID;
        te.TaskID = taskID;
        te.EquipmentID = selected;
        te.UserID = userID;
        te.Updated = true;
        te.Quantity = 1;

        TaskEquipment.addTaskEquipment(te);

        finish();
    }

    private void GetData() throws Exception
    {
        List<Equipment> equipment = null;
        
//        if (isInstallation) {
//            equipment = Equipment.GetEquipmentForTypeAndShift(equipmentTypeID, shiftID);
//        } else {
            equipment = Equipment.GetEquipmentForType(equipmentTypeID);
//        }

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < equipment.size(); i++) {
            jsonArray.put(equipment.get(i).getJSONObject());
        }

        typeAdapter.UpdateData(jsonArray);
    }
}
