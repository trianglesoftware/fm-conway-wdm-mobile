package trianglesoftware.fmconway.Equipment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentType;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class SelectEquipmentTypeActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String userID;
    private String taskID;
    private SharedPreferences sp;
    private EquipmentTypeAdapter typeAdapter;
    private boolean isInstallation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
            taskID = extras.getString("TaskID", "");
            isInstallation = extras.getBoolean("IsInstallation", true);
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView typeList = (ListView) findViewById(R.id.select_equipment_type_list);
        typeAdapter = new EquipmentTypeAdapter(this, getLayoutInflater());
        typeList.setAdapter(typeAdapter);
        typeList.setOnItemClickListener(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectEquipmentTypeActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_equipment_type;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Equipment Types";
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Intent equipmentActivity = new Intent(getApplicationContext(), SelectEquipmentActivity.class);
        equipmentActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        equipmentActivity.putExtra("ShiftID", shiftID);
        equipmentActivity.putExtra("TaskID", taskID);
        equipmentActivity.putExtra("EquipmentTypeID", selected);
        equipmentActivity.putExtra("UserID", userID);
        equipmentActivity.putExtra("IsInstallation", isInstallation);

        startActivity(equipmentActivity);

        finish();
    }

    private void GetData() throws Exception
    {
        List<EquipmentType> types = null;
        
//        if (isInstallation) {
//            types = EquipmentType.GetEquipmentTypesForShift(shiftID);
//        } else {
            types = EquipmentType.GetEquipmentTypes();
//        }

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < types.size(); i++) {
            jsonArray.put(types.get(i).getJSONObject());
        }

        typeAdapter.UpdateData(jsonArray);
    }
}
