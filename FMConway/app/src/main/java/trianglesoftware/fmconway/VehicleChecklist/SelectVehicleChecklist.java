package trianglesoftware.fmconway.VehicleChecklist;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;


public class SelectVehicleChecklist extends FMConwayActivity {
    private String shiftID;
    private String vehicleID;
    private String userID;
    private ListView vehicleChecklistList;
    private VehicleChecklistAdapter vehicleChecklistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            vehicleID = extras.getString("VehicleID","");
            userID = extras.getString("UserID","");
        }

        super.onCreate(savedInstanceState);

        vehicleChecklistList = (ListView)findViewById(R.id.select_vehicle_checklist_list);
        vehicleChecklistAdapter = new VehicleChecklistAdapter(this, getLayoutInflater());
        vehicleChecklistList.setAdapter(vehicleChecklistAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectVehicleChecklist");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_vehicle_checklist;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Checklist";
    }

    private void GetData() throws Exception
    {
        List<VehicleChecklist> vehicleChecklists = VehicleChecklist.GetVehicleChecklists(shiftID, vehicleID,true);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < vehicleChecklists.size(); i++) {
            jsonArray.put(vehicleChecklists.get(i).getJSONObject());
        }

        vehicleChecklistAdapter.UpdateData(jsonArray);
    }

    public void answerChecklist(View view)
    {
        int pos = vehicleChecklistList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleChecklistAdapter.getItem(pos);

        //Load Intent with buttons to different parts of the job pack
        Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
        vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
        vehicleChecklistQuestions.putExtra("VehicleChecklistID", selected.optString("VehicleChecklistID"));
        vehicleChecklistQuestions.putExtra("VehicleID", vehicleID);
        vehicleChecklistQuestions.putExtra("UserID", userID);
        startActivity(vehicleChecklistQuestions);
    }
}
