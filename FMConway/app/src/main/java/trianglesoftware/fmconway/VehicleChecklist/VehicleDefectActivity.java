package trianglesoftware.fmconway.VehicleChecklist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswerDefect;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswerDefectPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class VehicleDefectActivity extends FMConwayActivityBase {
    private String vehicleChecklistID;
    private String checklistQuestionID;
    private String shiftID;
    private EditText vehicleDefect;

    private GridView gridView;
    private ArrayList<VehicleChecklistAnswerDefectPhoto> photoList;
    private ImageAdapter adapter;
    private SharedPreferences sp;

    private String filepath;
    private final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_defect);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            vehicleChecklistID = extras.getString("VehicleChecklistID","");
            checklistQuestionID = extras.getString("ChecklistQuestionID","");
            shiftID = extras.getString("ShiftID");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        vehicleDefect = (EditText)findViewById(R.id.editText_VehicleDefect);

        VehicleChecklistAnswerDefect defect = VehicleChecklistAnswerDefect.GetVehicleDefect(vehicleChecklistID, checklistQuestionID);
        if(defect != null)
        {
            vehicleDefect.setText(defect.defect);
        }

        gridView = (GridView)findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = VehicleChecklistAnswerDefectPhoto.getPhotosForDefect(vehicleChecklistID, checklistQuestionID);

        adapter = new ImageAdapter(this, photoList);

        gridView.setAdapter(adapter);
    }

    public void addDefectPhoto(View view)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bitmap bmp = FMConwayUtils.filepathToBitmap(filepath);
                FMConwayUtils.saveBitmap(bmp, filepath, 70, true);

                AddData(filepath);

                photoList = VehicleChecklistAnswerDefectPhoto.getPhotosForDefect(vehicleChecklistID, checklistQuestionID);
                adapter = new ImageAdapter(this, photoList);
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        }
    }

    private void AddData(String imageLocation) {

        LocationManager lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude =  0;
        double latitude =  0;

        if(location != null) {
            longitude =  location.getLongitude();
            latitude =  location.getLatitude();
        }

        VehicleChecklistAnswerDefectPhoto photo = new VehicleChecklistAnswerDefectPhoto();
        photo.vehicleChecklistID = vehicleChecklistID;
        photo.checklistQuestionID = checklistQuestionID;
        photo.longitude = longitude;
        photo.latitude = latitude;
        photo.imageLocation = imageLocation;
        photo.userID = sp.getString("UserID", "");
        photo.time = new Date();
        photo.shiftID = shiftID;

        VehicleChecklistAnswerDefectPhoto.addVehicleChecklistAnswerDefectPhoto(photo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putString("filepath", filepath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
    }

    public void back() {
        finish();
    }

    public void defectSave(View view)
    {
        String defectText = vehicleDefect.getText().toString();

        if (Objects.equals(defectText, ""))
        {
            Toast.makeText(getApplicationContext(), "Please ensure that defect field is completed before proceeding", Toast.LENGTH_LONG).show();
        }
        else {
            VehicleChecklistAnswerDefect defect = VehicleChecklistAnswerDefect.GetVehicleDefect(vehicleChecklistID, checklistQuestionID);

            if (defect == null) {
                defect = new VehicleChecklistAnswerDefect();
                defect.vehicleChecklistID = vehicleChecklistID;
                defect.checklistQuestionID = checklistQuestionID;
                defect.defect = vehicleDefect.getText().toString();
                defect.userID = sp.getString("UserID", "");
                VehicleChecklistAnswerDefect.addVehicleChecklistAnswerDefect(defect);
            } else {
                defect.defect = vehicleDefect.getText().toString();
                VehicleChecklistAnswerDefect.UpdateVehicleDefect(defect);
            }

            finish();
        }
    }
}
