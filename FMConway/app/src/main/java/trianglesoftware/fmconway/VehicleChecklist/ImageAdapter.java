package trianglesoftware.fmconway.VehicleChecklist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswerDefectPhoto;
import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 11/03/2016.
 */
class ImageAdapter extends BaseAdapter {
    private final Context mContext;
    private ArrayList<VehicleChecklistAnswerDefectPhoto> photoList = new ArrayList<>();

    public ImageAdapter(Context c, ArrayList<VehicleChecklistAnswerDefectPhoto> PhotoList) {
        mContext = c;
        this.photoList = PhotoList;
    }

    public int getCount() {
        return photoList.size();
    }

    public Object getItem(int position) {
        return photoList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ImageView imageView = new ImageView(mContext);
        imageView.setImageBitmap(photoList.get(position).imageData);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setRotation(90);
        imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
        imageView.setTag(photoList.get(position).vehicleChecklistDefectPhotoID);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:

                                VehicleChecklistAnswerDefectPhoto.deleteVehicleChecklistPhotoFile(imageView.getTag().toString());
                                VehicleChecklistAnswerDefectPhoto.deleteVehicleChecklistAnswerDefectPhoto(imageView.getTag().toString());

                                photoList.remove(position);

                                Update();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:

                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext , R.style.Base_Theme_AppCompat_Light_Dialog));
                builder.setMessage("Delete Photo?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }
        });
        return imageView;
    }

    private void Update()
    {
        this.notifyDataSetChanged();
    }
}
