package trianglesoftware.fmconway.VehicleChecklist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswer;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Staff.TeamSignOff;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Vehicle.VehicleSignOff;


public class VehicleChecklistActivity extends FMConwayActivity {
    private String vehicleChecklistID;
    private String userID;
    private String shiftID;
    private String vehicleID;
    private ListView vehicleChecklistQuestionList;
    private VehicleChecklistQuestionAdapter vehicleChecklistQuestionAdapter;
    private EditText mileage;
    private boolean startOfShift;
    private boolean noShift;
    private TextView mileageLabel;
    private Button back;
    private Button home;
    private SharedPreferences sp;
    
    private String filepath;
    
    private final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            vehicleChecklistID = extras.getString("VehicleChecklistID","");
            vehicleID = extras.getString("VehicleID","");
            userID = extras.getString("UserID","");
            startOfShift = extras.getBoolean("StartOfShift");
            noShift = extras.getBoolean("NoShift");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        back = (Button) findViewById(R.id.back);
        home = (Button) findViewById(R.id.back_to_home);

        if (noShift)
        {
            back.setVisibility(View.INVISIBLE);
            home.setVisibility(View.INVISIBLE);
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(false);
            }
        });

        vehicleChecklistQuestionList = (ListView)findViewById(R.id.vehicle_checklist_list);
        vehicleChecklistQuestionAdapter = new VehicleChecklistQuestionAdapter(this, getLayoutInflater());
        vehicleChecklistQuestionList.setAdapter(vehicleChecklistQuestionAdapter);

        mileage = (EditText)findViewById(R.id.mileage);
        mileageLabel = (TextView)findViewById(R.id.mileage_label);

        mileage.setSelectAllOnFocus(true);

        ShiftVehicle shiftVehicle = ShiftVehicle.GetShiftVehicle(shiftID, vehicleID, userID);
        if(startOfShift)
        {
            mileageLabel.setText("Enter the start mileage");

            String mileageText = String.valueOf(shiftVehicle.startMileage);

            if (!Objects.equals(mileageText, "0")) {
                mileage.setText(mileageText);
            }
        }
        else
        {
            mileageLabel.setText("Enter the end mileage");

            String mileageText = String.valueOf(shiftVehicle.endMileage);

            if (!Objects.equals(mileageText, "0")) {
                mileage.setText(String.valueOf(shiftVehicle.endMileage));
            }
        }

        refreshCameraIcon();

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "VehicleChecklistActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vehicle_checklist;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Checklist";
    }

    private void GetData() throws Exception
    {
        List<VehicleChecklistAnswer> vehicleChecklistAnswers = VehicleChecklistAnswer.GetVehicleChecklistAnswers(vehicleChecklistID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < vehicleChecklistAnswers.size(); i++) {
            jsonArray.put(vehicleChecklistAnswers.get(i).getJSONObject());
        }

        vehicleChecklistQuestionAdapter.UpdateData(jsonArray);
    }

    public void answerYes(View view)
    {
        int pos = vehicleChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleChecklistQuestionAdapter.getItem(pos);

        VehicleChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), vehicleChecklistID, 1);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "VehicleChecklistActivity");
        }
    }

    public void answerNo(View view)
    {
        int pos = vehicleChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleChecklistQuestionAdapter.getItem(pos);

        VehicleChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), vehicleChecklistID, 0);

        //Load activity that is an empty list with ability to add defects for that checklist answer
        Intent vehicleDefectActivity = new Intent(getApplicationContext(), VehicleDefectActivity.class);
        vehicleDefectActivity.putExtra("VehicleChecklistID", vehicleChecklistID);
        vehicleDefectActivity.putExtra("ChecklistQuestionID", selected.optString("ChecklistQuestionID"));
        vehicleDefectActivity.putExtra("UserID", userID);
        vehicleDefectActivity.putExtra("ShiftID", shiftID);
        startActivity(vehicleDefectActivity);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "VehicleChecklistActivity");
        }
    }

    public void answerNA(View view)
    {
        int pos = vehicleChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleChecklistQuestionAdapter.getItem(pos);

        VehicleChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), vehicleChecklistID, 2);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "VehicleChecklistActivity");
        }
    }
    public void saveClick(View view) {
        if (CheckComplete()) {
            if (startOfShift) {
                ShiftVehicle.ChangeMileage(true, Integer.parseInt(mileage.getText().toString()), 0, shiftID, vehicleID);
                finish();
            } else {
                ShiftVehicle.ChangeMileage(false, 0, Integer.parseInt(mileage.getText().toString()), shiftID, vehicleID);
                finish();

                // Check all vehicle checklists complete
                if (VehicleSignOff.ChecklistsCompleted(shiftID, userID)) {
                    Intent staffSignOff = new Intent(getApplicationContext(), TeamSignOff.class);
                    staffSignOff.putExtra("ShiftID", shiftID);
                    startActivity(staffSignOff);
                }
            }
        }
    }

    private boolean CheckComplete() {
        List<VehicleChecklistAnswer> vehicleChecklistAnswers = VehicleChecklistAnswer.GetVehicleChecklistAnswers(vehicleChecklistID);

        // check all questions answered
        for (int i = 0; i < vehicleChecklistAnswers.size(); i++) {
            if (vehicleChecklistAnswers.get(i).isNew) {
                Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        // check mileage
        String miles = mileage.getText().toString();
        if (TextUtils.isEmpty(miles) || !miles.matches("[0-9]+")) {
            Toast.makeText(getApplicationContext(), "Please enter a mileage.", Toast.LENGTH_LONG).show();
            return false;
        }

        // check photo
        boolean photoAdded = !TextUtils.isEmpty(VehicleChecklist.getPhoto(vehicleChecklistID));
        if (!photoAdded) {
            Toast.makeText(getApplicationContext(), "Please take photo of mileage.", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public void returnToMenu(final boolean menu) {
        User user = User.getUser(sp.getString("UserID", ""));
        final boolean isCrewMember = Objects.equals(user.employeeType, "Crew Member");

        // save mileage
        String miles = mileage.getText().toString();
        int milesInt = 0;
        if (!TextUtils.isEmpty(miles) && miles.matches("[0-9]+")) {
            milesInt = Integer.parseInt(mileage.getText().toString());
        }

        if (startOfShift) {
            ShiftVehicle.ChangeMileage(true, milesInt, 0, shiftID, vehicleID);
        } else {
            ShiftVehicle.ChangeMileage(false, 0, milesInt, shiftID, vehicleID);
        }

        // return
        if (menu) {
            if (Objects.equals(shiftID, "")) {
                Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                noShiftIntent.putExtra("ShiftID", shiftID);
                noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(noShiftIntent);
            } else if (isCrewMember) {
                Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
                startIntent.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(startIntent);
            } else {
                Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startIntent.putExtra("ShiftID", shiftID);
                startIntent.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(startIntent);
            }
        } else {
            finish();
        }
    }

    public void cameraClick(View view) {
        String imageLocation = VehicleChecklist.getPhoto(vehicleChecklistID);
        
        if (!TextUtils.isEmpty(imageLocation)) {
            // if photo exists, overwrite, view or cancel
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(VehicleChecklistActivity.this ,R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("A photo already exists, how would you like to proceed?");
            builder.setPositiveButton("New Photo", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startCameraActivity();
                }
            });
            builder.setNegativeButton("View Existing Photo", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    String imageLocation = VehicleChecklist.getPhoto(vehicleChecklistID);
                    Uri uri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", new File(imageLocation));
                    
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(uri, "image/*");
                    
                    startActivity(intent);
                }
            });
            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                }
            });
            
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {
            // if no photo exists
            startCameraActivity();
        }
    }
    
    private void startCameraActivity() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();
        
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        
        startActivityForResult(intent, REQUEST_CODE);
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File file = new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
        
        return file;
    }
    
    public void refreshCameraIcon() {
        ImageView home = (ImageView) findViewById(R.id.camera);
        
        String imageLocation = VehicleChecklist.getPhoto(vehicleChecklistID);
        if (!TextUtils.isEmpty(imageLocation)) {
            home.setImageResource(R.drawable.camera_green);
        } else {
            home.setImageResource(R.drawable.camera_orange);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String imageLocation = VehicleChecklist.getPhoto(vehicleChecklistID);
                if (!TextUtils.isEmpty(imageLocation)) {
                    File file = new File(imageLocation);
                    file.delete();
                }
                
                Bitmap bmp = FMConwayUtils.filepathToBitmap(filepath);
                bmp = FMConwayUtils.bitmapRotationFix(bmp);
                FMConwayUtils.saveBitmap(bmp, filepath, 70, true);

                VehicleChecklist.updatePhoto(vehicleChecklistID, filepath);

                refreshCameraIcon();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("filepath", filepath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        filepath = savedInstanceState.getString("filepath");
    }
}
