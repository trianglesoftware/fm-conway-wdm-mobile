package trianglesoftware.fmconway.LoadingSheet;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
class LoadingSheetAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public LoadingSheetAdapter(Context context, LayoutInflater inflater) {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        JSONObject jsonObject = getItem(position);

        String loadingSheetEquipmentID = jsonObject.optString("LoadingSheetEquipmentID", "");
        String equipmentName = jsonObject.optString("EquipmentName", "");
        String equipmentType = jsonObject.optString("EquipmentType", "");
        int quantity = jsonObject.optInt("Quantity", 0);
        String vmsOrAsset = jsonObject.optString("VmsOrAsset", "");
        int unansweredQuestions = jsonObject.optInt("UnansweredQuestions", 0);

        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_loading_sheet, null);

            holder = new ViewHolder();
            holder.equipmentNameRowTextView = (TextView) convertView.findViewById(R.id.text_equipment_name);
            holder.equipmentTypeRowTextView = (TextView) convertView.findViewById(R.id.text_equipment_type);
            holder.quantityRowTextView = (TextView) convertView.findViewById(R.id.text_quantity);
            holder.moreInfoRowTextView = (Button) convertView.findViewById(R.id.text_more_info);
            holder.moreInfoRowTextView.setTag(holder);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.loadingSheetEquipmentID = loadingSheetEquipmentID;
        holder.quantity = quantity;
        holder.vmsOrAsset = vmsOrAsset;
        holder.unansweredQuestions = unansweredQuestions;

        holder.equipmentNameRowTextView.setText(equipmentName);
        holder.equipmentTypeRowTextView.setText(equipmentType);
        holder.quantityRowTextView.setText(String.valueOf(quantity));

        if (TextUtils.isEmpty(holder.vmsOrAsset)) {
            holder.moreInfoRowTextView.setVisibility(View.INVISIBLE);
        } else {
            if(holder.unansweredQuestions > 0) {
                holder.moreInfoRowTextView.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_yellow));
                holder.moreInfoRowTextView.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_black));
            } else {
                holder.moreInfoRowTextView.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_blue));
                holder.moreInfoRowTextView.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_light));
            }
        }

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;

        notifyDataSetChanged();
    }

    public class ViewHolder {
        public String loadingSheetEquipmentID;
        public int quantity;
        public String vmsOrAsset;
        public int unansweredQuestions;

        public TextView equipmentNameRowTextView;
        public TextView equipmentTypeRowTextView;
        public TextView quantityRowTextView;
        public Button moreInfoRowTextView;
    }
}
