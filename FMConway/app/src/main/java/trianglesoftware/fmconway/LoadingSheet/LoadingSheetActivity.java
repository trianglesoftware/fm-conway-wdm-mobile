package trianglesoftware.fmconway.LoadingSheet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.Equipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentItemChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentItemChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheet;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipmentItem;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.LoadingSheetEquipment.LoadingSheetEquipmentActivity;
import trianglesoftware.fmconway.LoadingSheetEquipment.SelectNewLoadingSheetEquipDialogActivity;
import trianglesoftware.fmconway.Main.BeforeLeaveActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.Signature;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Vehicle.ConfirmLoadingSheetActivity;


public class LoadingSheetActivity extends FMConwayActivity {
    private String shiftID;
    private String vehicleID;
    private String userID;
    private String loadingSheetID;
    private LoadingSheetAdapter loadingSheetAdapter;
    private SharedPreferences sp;

    private Button loadingSheetConfirm;
    private Button addItems;
    private ListView loadingSheetList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            vehicleID = extras.getString("VehicleID", "");
            userID = extras.getString("UserID", "");
            loadingSheetID = extras.getString("LoadingSheetID", "");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        super.onCreate(savedInstanceState);

        loadingSheetConfirm = (Button) findViewById(R.id.loading_sheet_confirm);
        addItems = (Button) findViewById(R.id.loading_sheet_add_equip);

        loadingSheetList = (ListView) findViewById(R.id.loading_sheet_list);
        loadingSheetAdapter = new LoadingSheetAdapter(this, getLayoutInflater());
        loadingSheetList.setAdapter(loadingSheetAdapter);

        if (loadingSheetID.length() != 0) {
            try {
                GetData();
            } catch (Exception e) {
                ErrorLog.CreateError(e, "LoadingSheetActivity");
            }
        }

        //CheckCompleted();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_loading_sheet;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Loading Sheet";
    }

    @Override
    public void onResume() {
        super.onResume();

        //userID = Integer.parseInt(sp.getString("UserID",""));

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "LoadingSheetActivity");
        }

        CheckCompleted();
    }

    private void CheckCompleted() {
        if (ShiftVehicle.CheckLoadingSheetIsConfirmed(shiftID, vehicleID)) {
            loadingSheetConfirm.setVisibility(View.INVISIBLE);
        } else {
            loadingSheetConfirm.setVisibility(View.VISIBLE);
        }
    }

    private void GetData() throws Exception {
        createEquipmentItemsIfRequired(shiftID, vehicleID);

        List<LoadingSheetEquipment> loadingSheetEquipment = Equipment.GetEquipmentForVehicle(shiftID, vehicleID);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < loadingSheetEquipment.size(); i++) {
            jsonArray.put(loadingSheetEquipment.get(i).getJSONObject());
        }

        loadingSheetAdapter.UpdateData(jsonArray);

        loadingSheetID = LoadingSheet.GetLoadingSheetID(shiftID, vehicleID);
    }

    private void createEquipmentItemsIfRequired(String shiftID, String vehicleID) {
        List<LoadingSheetEquipment> loadingSheetEquipments = LoadingSheetEquipment.createEquipmentItemsIfRequired(shiftID, vehicleID);

        for (int a = 0; a < loadingSheetEquipments.size(); a++) {
            LoadingSheetEquipment loadingSheetEquipment = loadingSheetEquipments.get(a);

            /*not vms or asset*/
            if (TextUtils.isEmpty(loadingSheetEquipment.vmsOrAsset)) {
                continue;
            }

            /*has no quantity*/
            if (loadingSheetEquipment.quantity == 0) {
                continue;
            }

            /*already created items & checklists*/
            if (loadingSheetEquipment.equipmentItems > 0) {
                continue;
            }

            String checklistID;
            if (Objects.equals(loadingSheetEquipment.vmsOrAsset, "Vms")) {
                checklistID = Checklist.getVmsChecklistID();
            } else if (Objects.equals(loadingSheetEquipment.vmsOrAsset, "Asset")) {
                checklistID = Checklist.getPreHireChecklistID();
            } else if (Objects.equals(loadingSheetEquipment.vmsOrAsset, "Battery")) {
                checklistID = Checklist.getPreHireBatteryChecklistID();
            } else {
                continue;
            }

            /*LoadingSheetEquipmentItems*/
            List<LoadingSheetEquipmentItem> loadingSheetEquipmentItems = new LinkedList<LoadingSheetEquipmentItem>();
            for (int i = 1; i <= loadingSheetEquipment.quantity; i++) {
                LoadingSheetEquipmentItem loadingSheetEquipmentItem = new LoadingSheetEquipmentItem();
                loadingSheetEquipmentItem.LoadingSheetEquipmentItemID = UUID.randomUUID().toString();
                loadingSheetEquipmentItem.LoadingSheetEquipmentID = loadingSheetEquipment.loadingSheetEquipmentID;
                loadingSheetEquipmentItem.ItemNumber = i;
                loadingSheetEquipmentItem.ChecklistID = checklistID;
                loadingSheetEquipmentItem.insert();
                loadingSheetEquipmentItems.add(loadingSheetEquipmentItem);
            }

            /*EquipmentItemChecklists*/
            List<EquipmentItemChecklist> equipmentItemChecklists = new LinkedList<EquipmentItemChecklist>();
            for (LoadingSheetEquipmentItem loadingSheetEquipmentItem : loadingSheetEquipmentItems) {
                EquipmentItemChecklist equipmentItemChecklist = new EquipmentItemChecklist();
                equipmentItemChecklist.EquipmentItemChecklistID = UUID.randomUUID().toString();
                equipmentItemChecklist.LoadingSheetEquipmentItemID = loadingSheetEquipmentItem.LoadingSheetEquipmentItemID;
                equipmentItemChecklist.ChecklistID = checklistID;
                equipmentItemChecklist.insert();
                equipmentItemChecklists.add(equipmentItemChecklist);
            }

            /*EquipmentItemChecklistAnswers*/
            List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklistID);
            for (EquipmentItemChecklist equipmentItemChecklist : equipmentItemChecklists) {
                for (ChecklistQuestion checklistQuestion : checklistQuestions) {
                    EquipmentItemChecklistAnswer equipmentItemChecklistAnswer = new EquipmentItemChecklistAnswer();
                    equipmentItemChecklistAnswer.EquipmentItemChecklistAnswerID = UUID.randomUUID().toString();
                    equipmentItemChecklistAnswer.EquipmentItemChecklistID = equipmentItemChecklist.EquipmentItemChecklistID;
                    equipmentItemChecklistAnswer.ChecklistQuestionID = checklistQuestion.checklistQuestionID;
                    equipmentItemChecklistAnswer.Answer = null;
                    equipmentItemChecklistAnswer.Reason = null;
                    equipmentItemChecklistAnswer.insert();
                }
            }
        }
    }

    public void moreInfo_Click(View view) {
        LoadingSheetAdapter.ViewHolder viewHolder = (LoadingSheetAdapter.ViewHolder)view.getTag();
        
        String checklistID;
        if (Objects.equals(viewHolder.vmsOrAsset,"Vms")) {
            checklistID = Checklist.getVmsChecklistID();
        } else if (Objects.equals(viewHolder.vmsOrAsset, "Asset")) {
            checklistID = Checklist.getPreHireChecklistID();
        } else if (Objects.equals(viewHolder.vmsOrAsset, "Battery")) {
            checklistID = Checklist.getPreHireBatteryChecklistID();
        } else {
            Toast.makeText(getApplicationContext(), "Equipment Type not Vms or Asset", Toast.LENGTH_LONG).show();
            return;
        }
        
        Intent intent = new Intent(getApplicationContext(), LoadingSheetEquipmentActivity.class);
        intent.putExtra("ShiftID", shiftID);
        intent.putExtra("UserID", userID);
        intent.putExtra("LoadingSheetEquipmentID", viewHolder.loadingSheetEquipmentID);
        intent.putExtra("Quantity", viewHolder.quantity);
        intent.putExtra("ChecklistID", checklistID);
        startActivity(intent);
    }

    public void confirmLoadingSheetClick(View view) {
        if (!CheckEquipmentItems()) {
            Toast.makeText(getApplicationContext(), "All checklists must be populated. click more info.", Toast.LENGTH_LONG).show();
            return;
        }
        
        ShiftVehicle.CompleteLoadingSheet(shiftID, vehicleID);

        finish();

        try {
            CheckLoadingSheetsConfirmed();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "CheckLoadingSheetsConfirmed");
        }

        if (!Objects.equals(loadingSheetID, "")) {
            //Load intent for signatures
            Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
            signatureIntent.putExtra("ShiftID", shiftID);
            signatureIntent.putExtra("VehicleID", vehicleID);
            signatureIntent.putExtra("UserID", userID);
            signatureIntent.putExtra("LoadingSheetID", loadingSheetID);
            startActivity(signatureIntent);
        }
    }
    
    private boolean CheckEquipmentItems() {
        for (int i = 0; i < loadingSheetList.getChildCount(); i++) { 
            View row = loadingSheetList.getChildAt(i);
            LoadingSheetAdapter.ViewHolder holder = (LoadingSheetAdapter.ViewHolder) row.getTag();
            if (!TextUtils.isEmpty(holder.vmsOrAsset)) {
                if(holder.unansweredQuestions > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private void CheckLoadingSheetsConfirmed() throws Exception {
        int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
        boolean loadingSheetsConfirmed = true;
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
        for (int i = 0; i < vehicles.size(); i++) {
            if (!ShiftVehicle.CheckLoadingSheetIsConfirmed(shiftID, vehicles.get(i).vehicleID)) {
                loadingSheetsConfirmed = false;
            }
        }

        if (loadingSheetsConfirmed) {

//            if (shiftStatus == 4) {
//                Shift.SetCompletionStatus(shiftID, 5);
//            }

            finish();

            Intent startBeforeLeaveActivity = new Intent(getApplicationContext(), BeforeLeaveActivity.class);
            startBeforeLeaveActivity.putExtra("ShiftID", shiftID);
            startBeforeLeaveActivity.putExtra("UserID", userID);
            startActivity(startBeforeLeaveActivity);
            //}
        } else {
            Intent loadingSheetActivity = new Intent(getApplicationContext(), ConfirmLoadingSheetActivity.class);
            loadingSheetActivity.putExtra("ShiftID", shiftID);
            loadingSheetActivity.putExtra("UserID", userID);
            startActivity(loadingSheetActivity);
        }
    }

    public void AddItem(View view){
        loadingSheetID = LoadingSheet.GetLoadingSheetID(shiftID, vehicleID);
        Intent addNewDialog = new Intent(getApplicationContext(), SelectNewLoadingSheetEquipDialogActivity.class);
        addNewDialog.putExtra("ShiftID", shiftID);
        addNewDialog.putExtra("LoadingSheetID", loadingSheetID);
        addNewDialog.putExtra("UserID", userID);
        addNewDialog.putExtra("VehicleID", vehicleID);
        startActivity(addNewDialog);
    }
}
