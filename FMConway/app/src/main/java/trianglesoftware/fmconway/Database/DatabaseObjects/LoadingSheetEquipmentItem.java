package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class LoadingSheetEquipmentItem {
    public String LoadingSheetEquipmentItemID;
    public String LoadingSheetEquipmentID;
    public int ItemNumber;
    public String ChecklistID;
    public String EquipmentName;
    public int UnansweredQuestions;

    public LoadingSheetEquipmentItem() {

    }

    public void insert() {
        ContentValues values = new ContentValues();
        values.put("LoadingSheetEquipmentItemID", LoadingSheetEquipmentItemID);
        values.put("LoadingSheetEquipmentID", LoadingSheetEquipmentID);
        values.put("ItemNumber", ItemNumber);
        values.put("ChecklistID", ChecklistID);
        
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.insertOrThrow("LoadingSheetEquipmentItems", null, values);
    }

    public static List<LoadingSheetEquipmentItem> get(String loadingSheetEquipmentID) {
        List<LoadingSheetEquipmentItem> results = new LinkedList<>();

        String query = "";
        query += "select ";
        query += "lsei.LoadingSheetEquipmentItemID, ";
        query += "lsei.LoadingSheetEquipmentID, ";
        query += "lsei.ItemNumber, ";
        query += "lsei.ChecklistID, ";
        query += "e.Name as EquipmentName, ";
        query += "coalesce(eic.UnansweredQuestions, 0) as UnansweredQuestions ";
        query += "from ";
        query += "LoadingSheetEquipmentItems as lsei ";
        query += "inner join LoadingSheetEquipment as lse on lse.LoadingSheetEquipmentID = lsei.LoadingSheetEquipmentID ";
        query += "inner join Equipment as e on e.EquipmentID = lse.EquipmentID ";
        query += "left join ( ";
        query += "    select ";
        query += "    eic.LoadingSheetEquipmentItemID, ";
        query += "    count(*) as UnansweredQuestions ";
        query += "    from ";
        query += "    EquipmentItemChecklists as eic ";
        query += "    inner join EquipmentItemChecklistAnswers as eica on eica.EquipmentItemChecklistID = eic.EquipmentItemChecklistID ";
        query += "    where ";
        query += "    eica.Answer is null ";
        query += "    group by ";
        query += "    eic.LoadingSheetEquipmentItemID ";
        query += ") as eic on eic.LoadingSheetEquipmentItemID = lsei.LoadingSheetEquipmentItemID ";
        query += "where ";
        query += "lsei.LoadingSheetEquipmentID = ? ";
        query += "order by ";
        query += "lsei.ItemNumber ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{ loadingSheetEquipmentID });
        
        if (cursor.moveToFirst()) {
            do {
                LoadingSheetEquipmentItem a = new LoadingSheetEquipmentItem();
                a.LoadingSheetEquipmentItemID = cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentItemID"));
                a.LoadingSheetEquipmentID = cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentID"));
                a.ItemNumber = cursor.getInt(cursor.getColumnIndex("ItemNumber"));
                a.ChecklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                a.EquipmentName = cursor.getString(cursor.getColumnIndex("EquipmentName"));
                a.UnansweredQuestions = cursor.getInt(cursor.getColumnIndex("UnansweredQuestions"));

                results.add(a);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return results;
    }
    
    public static JSONObject toSend(String shiftID) throws JSONException {
        JSONObject result = new JSONObject();
        JSONArray loadingSheets = new JSONArray();
        JSONArray loadingSheetEquipments = new JSONArray();
        JSONArray loadingSheetEquipmentItems = new JSONArray();
        JSONArray equipmentItemChecklists = new JSONArray();
        JSONArray equipmentItemChecklistAnswers = new JSONArray();

        // LoadingSheets
        String query = "";
        query += "select ";
        query += "a.ActivityID, ls.* ";
        query += "from ";
        query += "LoadingSheets as ls ";
        query += "INNER JOIN JobPacks jp ON ls.ShiftID = jp.ShiftID ";
        query += "INNER JOIN Activities a ON jp.JobPackID = a.JobPackID ";
        query += "where ";
        query += "ls.ShiftID = ? ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{ shiftID });

        if (cursor.moveToFirst()) {
            do {
                JSONObject loadingSheet = new JSONObject();
                loadingSheet.put("LoadingSheetID", cursor.getString(cursor.getColumnIndex("LoadingSheetID")));
                loadingSheet.put("VehicleID", cursor.getString(cursor.getColumnIndex("VehicleID")));
                loadingSheet.put("ShiftID", cursor.getString(cursor.getColumnIndex("ShiftID")));
                loadingSheet.put("ActivityID", cursor.getString(cursor.getColumnIndex("ActivityID")));

                loadingSheets.put(loadingSheet);
            } while (cursor.moveToNext());
        }

        result.put("LoadingSheets", loadingSheets);
        cursor.close();

        // LoadingSheetEquipments
        query = "";
        query += "select ";
        query += "* ";
        query += "from ";
        query += "LoadingSheetEquipment as lse ";
        query += "where ";
        query += "lse.ShiftID = ? ";

        db = Database.MainDB.getWritableDatabase();
        cursor = db.rawQuery(query, new String[]{ shiftID });

        if (cursor.moveToFirst()) {
            do {
                JSONObject loadingSheetEquipment = new JSONObject();
                loadingSheetEquipment.put("LoadingSheetEquipmentID", cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentID")));
                loadingSheetEquipment.put("LoadingSheetID", cursor.getString(cursor.getColumnIndex("LoadingSheetID")));
                loadingSheetEquipment.put("EquipmentID", cursor.getString(cursor.getColumnIndex("EquipmentID")));
                loadingSheetEquipment.put("Quantity", cursor.getInt(cursor.getColumnIndex("Quantity")));
                loadingSheetEquipment.put("SheetOrder", cursor.getInt(cursor.getColumnIndex("SheetOrder")));
                loadingSheetEquipment.put("ShiftID", cursor.getString(cursor.getColumnIndex("ShiftID")));
                loadingSheetEquipment.put("VmsOrAsset", cursor.getString(cursor.getColumnIndex("VmsOrAsset")));

                loadingSheetEquipments.put(loadingSheetEquipment);
            } while (cursor.moveToNext());
        }

        result.put("LoadingSheetEquipments", loadingSheetEquipments);
        cursor.close();

        // LoadingSheetEquipmentItems
        query = "";
        query += "select ";
        query += "lsei.LoadingSheetEquipmentItemID, ";
        query += "lsei.LoadingSheetEquipmentID, ";
        query += "lsei.ItemNumber, ";
        query += "lsei.ChecklistID ";
        query += "from ";
        query += "LoadingSheetEquipment as lse ";
        query += "inner join LoadingSheetEquipmentItems as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "where ";
        query += "lse.ShiftID = ? ";

        db = Database.MainDB.getWritableDatabase();
        cursor = db.rawQuery(query, new String[]{ shiftID });

        if (cursor.moveToFirst()) {
            do {
                JSONObject loadingSheetEquipmentItem = new JSONObject();
                loadingSheetEquipmentItem.put("LoadingSheetEquipmentItemID", cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentItemID")));
                loadingSheetEquipmentItem.put("LoadingSheetEquipmentID", cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentID")));
                loadingSheetEquipmentItem.put("ItemNumber", cursor.getInt(cursor.getColumnIndex("ItemNumber")));
                loadingSheetEquipmentItem.put("ChecklistID", cursor.getString(cursor.getColumnIndex("ChecklistID")));

                loadingSheetEquipmentItems.put(loadingSheetEquipmentItem);
            } while (cursor.moveToNext());
        }

        result.put("LoadingSheetEquipmentItems", loadingSheetEquipmentItems);
        cursor.close();

        // EquipmentItemChecklists
        query = "";
        query += "select ";
        query += "eic.EquipmentItemChecklistID, ";
        query += "eic.LoadingSheetEquipmentItemID, ";
        query += "eic.ChecklistID ";
        query += "from ";
        query += "LoadingSheetEquipment as lse ";
        query += "inner join LoadingSheetEquipmentItems as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "inner join EquipmentItemChecklists as eic on eic.LoadingSheetEquipmentItemID = lsei.LoadingSheetEquipmentItemID ";
        query += "where ";
        query += "lse.ShiftID = ? ";

        cursor = db.rawQuery(query, new String[]{ shiftID });

        if (cursor.moveToFirst()) {
            do {
                JSONObject equipmentItemChecklist = new JSONObject();
                equipmentItemChecklist.put("EquipmentItemChecklistID", cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistID")));
                equipmentItemChecklist.put("LoadingSheetEquipmentItemID", cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentItemID")));
                equipmentItemChecklist.put("ChecklistID", cursor.getString(cursor.getColumnIndex("ChecklistID")));

                equipmentItemChecklists.put(equipmentItemChecklist);
            } while (cursor.moveToNext());
        }
        
        result.put("EquipmentItemChecklists", equipmentItemChecklists);
        cursor.close();

        // EquipmentItemChecklistAnswers
        query = "";
        query += "select ";
        query += "eica.EquipmentItemChecklistAnswerID, ";
        query += "eica.EquipmentItemChecklistID, ";
        query += "eica.ChecklistQuestionID, ";
        query += "eica.Answer, ";
        query += "eica.Reason ";
        query += "from ";
        query += "LoadingSheetEquipment as lse ";
        query += "inner join LoadingSheetEquipmentItems as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "inner join EquipmentItemChecklists as eic on eic.LoadingSheetEquipmentItemID = lsei.LoadingSheetEquipmentItemID ";
        query += "inner join EquipmentItemChecklistAnswers as eica on eica.EquipmentItemChecklistID = eic.EquipmentItemChecklistID ";
        query += "where ";
        query += "lse.ShiftID = ? ";

        cursor = db.rawQuery(query, new String[]{ shiftID });

        if (cursor.moveToFirst()) {
            do {
                JSONObject equipmentItemChecklistAnswer = new JSONObject();
                equipmentItemChecklistAnswer.put("EquipmentItemChecklistAnswerID", cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistAnswerID")));
                equipmentItemChecklistAnswer.put("EquipmentItemChecklistID", cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistID")));
                equipmentItemChecklistAnswer.put("ChecklistQuestionID", cursor.getString(cursor.getColumnIndex("ChecklistQuestionID")));
                equipmentItemChecklistAnswer.put("Answer", cursor.getInt(cursor.getColumnIndex("Answer")) > 0);
                equipmentItemChecklistAnswer.put("Reason", cursor.getString(cursor.getColumnIndex("Reason")));

                equipmentItemChecklistAnswers.put(equipmentItemChecklistAnswer);
            } while (cursor.moveToNext());
        }

        result.put("EquipmentItemChecklistAnswers", equipmentItemChecklistAnswers);
        cursor.close();
        
        return result;
    }

    // LoadingSheetEquipmentItems
    // EquipmentItemChecklists
    // EquipmentItemChecklistAnswers
    public static void deleteForShift(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        
        // EquipmentItemChecklistAnswers
        String query = "";
        query += "delete ";
        query += "from ";
        query += "EquipmentItemChecklistAnswers ";
        query += "where ";
        query += "EquipmentItemChecklistAnswerID in ( ";
        query += "	select ";
        query += "	eica.EquipmentItemChecklistAnswerID ";
        query += "	from ";
        query += "	LoadingSheetEquipment as lse ";
        query += "	inner join LoadingSheetEquipmentItems as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "	inner join EquipmentItemChecklists as eic on eic.LoadingSheetEquipmentItemID = lsei.LoadingSheetEquipmentItemID ";
        query += "	inner join EquipmentItemChecklistAnswers as eica on eica.EquipmentItemChecklistID = eic.EquipmentItemChecklistID ";
        query += "	where ";
        query += "	lse.ShiftID = ? ";
        query += ") ";
        
        db.execSQL(query, new String[] { shiftID });

        // EquipmentItemChecklists
        query = "";
        query += "delete ";
        query += "from ";
        query += "EquipmentItemChecklists ";
        query += "where ";
        query += "EquipmentItemChecklistID in ( ";
        query += "	select ";
        query += "	eic.EquipmentItemChecklistID ";
        query += "	from ";
        query += "	LoadingSheetEquipment as lse ";
        query += "	inner join LoadingSheetEquipmentItems as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "	inner join EquipmentItemChecklists as eic on eic.LoadingSheetEquipmentItemID = lsei.LoadingSheetEquipmentItemID ";
        query += "	where ";
        query += "	lse.ShiftID = ? ";
        query += ") ";

        db.execSQL(query, new String[] { shiftID });

        // LoadingSheetEquipmentItems
        query = "";
        query += "delete ";
        query += "from ";
        query += "LoadingSheetEquipmentItems ";
        query += "where ";
        query += "LoadingSheetEquipmentItemID in ( ";
        query += "	select ";
        query += "	lsei.LoadingSheetEquipmentItemID ";
        query += "	from ";
        query += "	LoadingSheetEquipment as lse ";
        query += "	inner join LoadingSheetEquipmentItems as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "	where ";
        query += "	lse.ShiftID = ? ";
        query += ") ";

        db.execSQL(query, new String[] { shiftID });
    }
    
    // if the task has equipment items of type asset / vms but no associated check, skip upload
    public static boolean hasChecklists(JSONObject loadingSheetEquipmentItemsToSend) throws JSONException {
        boolean result = false;
        JSONArray loadingSheetEquipmentItems = loadingSheetEquipmentItemsToSend.getJSONArray("LoadingSheetEquipmentItems");
        for (int i = 0; i < loadingSheetEquipmentItems.length(); i++) {
            JSONObject loadingSheetEquipmentItem = loadingSheetEquipmentItems.getJSONObject(i);
            boolean hasChecklistID = loadingSheetEquipmentItem.has("ChecklistID");
            if (hasChecklistID) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }
}
