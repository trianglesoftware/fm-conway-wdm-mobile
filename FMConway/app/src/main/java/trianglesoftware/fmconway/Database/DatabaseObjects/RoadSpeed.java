package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class RoadSpeed {
    public String roadSpeedID;
    public String speed;
    public boolean isActive;

    public RoadSpeed() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("RoadSpeedID", this.roadSpeedID);
            obj.put("Speed", this.speed);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "RoadSpeed - getJSONObject");
        }
        return obj;
    }

    protected static RoadSpeed cursorToModel(Cursor c) {
        RoadSpeed roadSpeed = new RoadSpeed();
        roadSpeed.roadSpeedID = c.getString(c.getColumnIndex("RoadSpeedID"));
        roadSpeed.speed = c.getString(c.getColumnIndex("Speed"));
        roadSpeed.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return roadSpeed;
    }

    public static RoadSpeed get(String roadSpeedID) {
        SQLiteDatabase db = Database.MainDB.getReadableDatabase();

        RoadSpeed model = null;
        Cursor c = db.rawQuery("SELECT * FROM RoadSpeeds WHERE RoadSpeedID = ?", new String[] { roadSpeedID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    // includeRoadSpeedID is used to ensure previously selected item will still be in collection even if de-activated
    public static List<RoadSpeed> getLookupItems(String includeRoadSpeedID) {
        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        List<RoadSpeed> models = new LinkedList<>();

        RoadSpeed defaultItem = new RoadSpeed();
        defaultItem.speed = "";
        models.add(defaultItem);

        Cursor c;

        if (includeRoadSpeedID != null) {
            c = db.rawQuery("SELECT * FROM RoadSpeeds WHERE IsActive = 1 or RoadSpeedID = ? ORDER BY Speed", new String[]{includeRoadSpeedID});
        } else {
            c = db.rawQuery("SELECT * FROM RoadSpeeds WHERE IsActive = 1 ORDER BY Speed", null);
        }

        if (c.moveToFirst()) {
            do {
                RoadSpeed model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(RoadSpeed model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("RoadSpeedID", model.roadSpeedID);
            cv.put("Speed", model.speed);
            cv.put("IsActive", model.isActive);

            if (get(model.roadSpeedID) != null) {
                db.update("RoadSpeeds", cv, "RoadSpeedID = ?", new String[]{model.roadSpeedID});
            } else {
                db.insertOrThrow("RoadSpeeds", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "RoadSpeed - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return speed;
    }
}
