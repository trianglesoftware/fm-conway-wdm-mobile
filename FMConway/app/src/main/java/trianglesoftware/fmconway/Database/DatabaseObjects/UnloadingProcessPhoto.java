package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class UnloadingProcessPhoto {

    public String unloadingProcessPhotoID;
    public String unloadingProcessID;
    public String userID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public Date time;
    public String shiftID;
    public String comments;

    public UnloadingProcessPhoto() {

    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("UnloadingProcessPhotoID", this.unloadingProcessPhotoID);
        obj.put("UnloadingProcessID", this.unloadingProcessID);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Time", FMConwayUtils.getDateIsoString(this.time));
        obj.put("Comments", this.comments);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options, 400, 400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
        if (bm != null) {
            try {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            } catch (Exception e) {
                ErrorLog.CreateError(e, "Rotate Photo");
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }

        return obj;
    }

    public static void saveUnloadingProcessPhoto(UnloadingProcessPhoto unloadingProcessPhoto) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("UnloadingProcessPhotoID", unloadingProcessPhoto.unloadingProcessPhotoID);
        values.put("UnloadingProcessID", unloadingProcessPhoto.unloadingProcessID);
        values.put("Longitude", unloadingProcessPhoto.longitude);
        values.put("Latitude", unloadingProcessPhoto.latitude);
        values.put("ImageLocation", unloadingProcessPhoto.imageLocation);
        values.put("UserID", unloadingProcessPhoto.userID);
        values.put("Time", FMConwayUtils.getDateIsoString(unloadingProcessPhoto.time));
        values.put("ShiftID", unloadingProcessPhoto.shiftID);
        values.put("Comments", unloadingProcessPhoto.comments);

        if (checkIfUnloadingProcessPhotoExists(unloadingProcessPhoto.unloadingProcessPhotoID)) {
            db.update("UnloadingProcessPhotos", values, "UnloadingProcessPhotoID = ?", new String[]{unloadingProcessPhoto.unloadingProcessPhotoID});
        } else {
            db.insert("UnloadingProcessPhotos", null, values);
        }
    }

    public static void saveComments(String unloadingProcessPhotoID, String comments) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("Comments", comments);

        db.update("UnloadingProcessPhotos", cv, "UnloadingProcessPhotoID = ?", new String[]{unloadingProcessPhotoID});
    }

    public static void deleteAllUnloadingProcessPhotos() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM UnloadingProcessPhotos");
    }

    public static void deleteUnloadingProcessPhoto(String unloadingProcessPhotoID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM UnloadingProcessPhotos WHERE UnloadingProcessPhotoID = '" + unloadingProcessPhotoID + "'");
    }

    public static void deleteUnloadingProcessPhotoFile(String unloadingProcessPhotoID) {
        try {
            String query = "SELECT * FROM UnloadingProcessPhotos ";
            query += "WHERE UnloadingProcessPhotoID = '" + unloadingProcessPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DeleteUnloadingProcessPhotoFile");
        }
    }

    public static void deleteAllUnloadingProcessPhotosForUser(String userID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM UnloadingProcessPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllUnloadingProcessPhotosForShift(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM UnloadingProcessPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static UnloadingProcessPhoto getUnloadingProcessPhoto(String unloadingProcessPhotoID) {
        UnloadingProcessPhoto unloadingProcessPhoto = null;

        String query = "SELECT * FROM UnloadingProcessPhotos WHERE UnloadingProcessPhotoID = '" + unloadingProcessPhotoID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            unloadingProcessPhoto = new UnloadingProcessPhoto();
            unloadingProcessPhoto.unloadingProcessPhotoID = c.getString(c.getColumnIndex("UnloadingProcessPhotoID"));
            unloadingProcessPhoto.unloadingProcessID = c.getString(c.getColumnIndex("UnloadingProcessID"));
            unloadingProcessPhoto.userID = c.getString(c.getColumnIndex("UserID"));
            unloadingProcessPhoto.longitude = Double.parseDouble(c.getString(c.getColumnIndex("Longitude")));
            unloadingProcessPhoto.latitude = Double.parseDouble(c.getString(c.getColumnIndex("Latitude")));
            unloadingProcessPhoto.time = FMConwayUtils.getCursorDate(c, "Time");
            unloadingProcessPhoto.imageLocation = c.getString(c.getColumnIndex("ImageLocation"));
            unloadingProcessPhoto.shiftID = c.getString(c.getColumnIndex("ShiftID"));
            unloadingProcessPhoto.comments = c.getString(c.getColumnIndex("Comments"));
        }

        c.close();

        return unloadingProcessPhoto;
    }

    public static ArrayList<UnloadingProcessPhoto> getPhotosForUnloadingProcess(String unloadingProcessID) {
        ArrayList<UnloadingProcessPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM UnloadingProcessPhotos WHERE UnloadingProcessID = '" + unloadingProcessID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        UnloadingProcessPhoto data;
        if (cursor.moveToFirst()) {
            do {
                data = new UnloadingProcessPhoto();
                data.unloadingProcessPhotoID = cursor.getString(cursor.getColumnIndex("UnloadingProcessPhotoID"));
                data.unloadingProcessID = cursor.getString(cursor.getColumnIndex("UnloadingProcessID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                data.comments = cursor.getString(cursor.getColumnIndex("Comments"));

                String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageLocation = imageLocation;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options, 150, 150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(
        BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetPhotosToSend(String shiftID) throws Exception {
        JSONArray photos = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT upp.* FROM UnloadingProcessPhotos upp ";
        query += "JOIN UnloadingProcesses up on up.UnloadingProcessID = upp.UnloadingProcessID ";
        query += "JOIN Activities act on act.ActivityID = up.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = act.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    UnloadingProcessPhoto s = new UnloadingProcessPhoto();
                    s.unloadingProcessPhotoID = cursor.getString(cursor.getColumnIndex("UnloadingProcessPhotoID"));
                    s.unloadingProcessID = cursor.getString(cursor.getColumnIndex("UnloadingProcessID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    s.comments = cursor.getString(cursor.getColumnIndex("Comments"));

                    try {
                        String time = cursor.getString(cursor.getColumnIndex("Time"));
                        if (time != null)
                            s.time = format.parse(time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    sObject = s.getJSONObject();

                    photos.put(sObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "GetUnloadingProcessPhotosToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return photos;
    }

    public static boolean checkIfUnloadingProcessPhotoExists(String unloadingProcessPhotoID) {
        String query = "SELECT * FROM UnloadingProcessPhotos WHERE UnloadingProcessPhotoID = '" + unloadingProcessPhotoID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        boolean result = cursor.moveToFirst();

        cursor.close();

        return result;
    }

    public static boolean checkIfPhotosExist(String taskID) {
        String query =
            "select " +
            "upp.* " +
            "from " +
            "UnloadingProcessPhotos as upp " +
            "inner join UnloadingProcesses up on up.UnloadingProcessID = upp.UnloadingProcessID " +
            "inner join Tasks t on t.TaskID = up.TaskID " +
            "where " +
            "t.TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskID });

        boolean result = cursor.moveToFirst();
        
        cursor.close();

        return result;
    }

    public static boolean checkIfPhotosExistForActivity(String activityID) {
        String query = "" +
            "select " +
            "upp.* " +
            "from " +
            "UnloadingProcessPhotos as upp " +
            "inner join UnloadingProcesses up on up.UnloadingProcessID = upp.UnloadingProcessID " +
            "where " +
            "up.ActivityID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { activityID });

        boolean result = cursor.moveToFirst();
        
        cursor.close();

        return result;
    }
}
