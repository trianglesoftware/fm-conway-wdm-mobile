package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class Direction {
    public String directionID;
    public String name;
    public boolean isActive;

    public Direction() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("DirectionID", this.directionID);
            obj.put("Name", this.name);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "Direction - getJSONObject");
        }
        return obj;
    }

    protected static Direction cursorToModel(Cursor c) {
        Direction direction = new Direction();
        direction.directionID = c.getString(c.getColumnIndex("DirectionID"));
        direction.name = c.getString(c.getColumnIndex("Name"));
        direction.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return direction;
    }

    public static Direction get(String directionID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Direction model = null;
        Cursor c = db.rawQuery("SELECT * FROM Directions WHERE DirectionID = ?", new String[] { directionID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    // includeDirectionID is used to ensure previously selected item will still be in collection even if de-activated
    public static List<Direction> getLookupItems(String includeDirectionID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<Direction> models = new LinkedList<>();

        Direction defaultItem = new Direction();
        defaultItem.name = "";
        models.add(defaultItem);

        Cursor c;

        if (includeDirectionID != null) {
            c = db.rawQuery("SELECT * FROM Directions WHERE IsActive = 1 or DirectionID = ? ORDER BY Name", new String[]{includeDirectionID});
        } else {
            c = db.rawQuery("SELECT * FROM Directions WHERE IsActive = 1 ORDER BY Name", null);
        }

        if (c.moveToFirst()) {
            do {
                Direction model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(Direction model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("DirectionID", model.directionID);
            cv.put("Name", model.name);
            cv.put("IsActive", model.isActive);

            if (get(model.directionID) != null) {
                db.update("Directions", cv, "DirectionID = ?", new String[]{model.directionID});
            } else {
                db.insertOrThrow("Directions", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "Direction - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return name;
    }
}
