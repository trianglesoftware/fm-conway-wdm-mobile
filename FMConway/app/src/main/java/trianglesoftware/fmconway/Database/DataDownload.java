package trianglesoftware.fmconway.Database;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import trianglesoftware.fmconway.Database.DatabaseObjects.Accident;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityMethodStatement;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityPriority;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityType;
import trianglesoftware.fmconway.Database.DatabaseObjects.Area;
import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingType;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.Contact;
import trianglesoftware.fmconway.Database.DatabaseObjects.Customer;
import trianglesoftware.fmconway.Database.DatabaseObjects.Depot;
import trianglesoftware.fmconway.Database.DatabaseObjects.Direction;
import trianglesoftware.fmconway.Database.DatabaseObjects.DisposalSite;
import trianglesoftware.fmconway.Database.DatabaseObjects.Equipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentType;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPackDocument;
import trianglesoftware.fmconway.Database.DatabaseObjects.Lane;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckDetail;
import trianglesoftware.fmconway.Database.DatabaseObjects.MethodStatement;
import trianglesoftware.fmconway.Database.DatabaseObjects.Note;
import trianglesoftware.fmconway.Database.DatabaseObjects.Observation;
import trianglesoftware.fmconway.Database.DatabaseObjects.ObservationType;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.RoadSpeed;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftUpdate;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheet;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.StaffRecord;
import trianglesoftware.fmconway.Database.DatabaseObjects.TMRequirement;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipmentAsset;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskType;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswerDefect;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswerDefectPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleType;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleTypeChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.WeatherCondition;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 29/02/2016.
 */
public class DataDownload {
    private String url = "";
    private final int toDownload = 24;
    private final int toDownloadNewShifts = 4;
    private int downloaded = 0;
    private int errors = 0;
    private int shiftUpdatesDownloaded = 0;
    private int shiftUpdatesErrors = 0;
    private final String userID;
    private final String syncDate;
    private final Context ctx;
    private ProgressDialog mDialog;
    private final String encodedHeader;
    private int timeout = 360000;
   // private int longTimeout = 120000;

    public DataDownload(String URL, String UserID, String SyncDate, String EncodedHeader, Context CTX) {
        url = URL;
        userID = UserID;
        syncDate = SyncDate;
        ctx = CTX;
        encodedHeader = EncodedHeader;
    }

    public void DownloadNewData(ProgressDialog Dialog, final DataDownloadListener listener) {
        mDialog = Dialog;

        final DataDownloadProgressListener ddpa = new DataDownloadProgressListener() {
            @Override
            public void onComplete() {
                Log.d("Download","Sub complete " + downloaded + " of " + toDownload);
                if (downloaded >= toDownload) {
                    listener.onComplete(errors > 0);
                    Log.d("Download","Complete");
                }
            }

            @Override
            public void onError(Throwable t) {
                listener.onError();
            }
        };

        ShiftDownloadListener sdpa = new ShiftDownloadListener() {
            @Override
            public void onComplete(List<String> shiftIDs) {
                // VehicleChecklists are propagated within DownloadShifts, wait for it to complete
                DownloadVehicleChecklists(userID, syncDate, ddpa);
                
                Log.d("Download","Sub complete " + downloaded + " of " + toDownload);
                if (downloaded >= toDownload) {
                    listener.onComplete(errors > 0);
                    Log.d("Download","Complete");
                }
            }

            @Override
            public void onError(Throwable t, List<String> shiftIDs) {
                listener.onError();
            }
        };

        // all downloads are async
        DownloadUsers(ddpa);
        DownloadEquipment(ddpa);
        DownloadEquipmentTypes(ddpa);
        DownloadChecklists(ddpa);
        DownloadVehicleTypes(ddpa);
        DownloadVehicles(ddpa);
        DownloadStaff(ddpa);
        DownloadShifts(userID, syncDate, sdpa);
        DownloadBriefingTypes(ddpa);
        DownloadActivityTypes(ddpa);
        //DownloadObservationTypes(ddpa);
        DownloadTaskTypes(ddpa);
        DownloadWeatherConditions(ddpa);
        DownloadJobPacks(userID, syncDate, ddpa);
        DownloadAreas(userID, syncDate, ddpa);
        DownloadActivityPriorities(ddpa);
        DownloadLanes(ddpa);
        DownloadTMRequirements(ddpa);
        DownloadRoadSpeeds(ddpa);
        DownloadDirections(ddpa);
        DownloadDisposalSites(ddpa);
        DownloadCustomers(ddpa);
        DownloadContacts(ddpa);
        DownloadMethodStatements(ddpa);
        DownloadDepots(ddpa);
        DownloadShiftUpdates(ddpa);
    }

    private void DownloadUsers(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadUsers Start");
        client.get(url + "Users", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {

                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    //shouldn't delete all users incase inactive user information is referentially constrained
                    //User.deleteAllUsers();

                    for (int i = 0; i < arr.length(); i++) {
                        User user = new User();
                        user.userID = arr.getJSONObject(i).getString("UserID");
                        user.userTypeID = arr.getJSONObject(i).getString("UserTypeID");
                        user.userName = (arr.getJSONObject(i).getString("Username"));
                        user.depotID = arr.getJSONObject(i).getString("DepotID");
                        //user.userName = arr.getJSONObject(i).getString("Username");
                        //user.setIsStandardUser(arr.getJSONObject(i).getBoolean("IsStandardUser"));
                        user.isStandardUser = true;
                        user.employeeType = arr.getJSONObject(i).getString("EmployeeType");

                        if (User.exists(user.userID)) {
                            User.updateUser(user);
                        } else {
                            User.addUser(user);
                        }
                    }

                    downloaded += 1;

                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    Log.e("DD","DownloadUsers Error");
                    errors++;
                }

                Log.d("DD","DownloadUsers End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Users" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD","DownloadUsers Error");
            }
        });
    }

    private void DownloadStaff(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadStaff Start");
        client.get(url + "Staff", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    //shouldn't delete all staff incase inactive staff is on an active work instruction (referential constraint)
                    //Staff.deleteAllStaff();

                    for (int i = 0; i < arr.length(); i++) {
                        Staff staff = new Staff();
                        staff.staffID = arr.getJSONObject(i).getString("StaffID");
                        staff.forename = arr.getJSONObject(i).getString("Name");
                        staff.surname = arr.getJSONObject(i).getString("Name");
                        staff.depotID = arr.getJSONObject(i).getString("DepotID");
                        staff.staffType = arr.getJSONObject(i).getString("Type");
                        staff.isAgency = arr.getJSONObject(i).getBoolean("IsAgency");

                        if (Staff.exists(staff.staffID)) {
                            Staff.updateStaff(staff);
                        } else {
                            Staff.addStaff(staff);
                        }
                    }

                    downloaded += 1;

                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD","DownloadStaff Error");
                }

                Log.d("DD","DownloadStaff End");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Staff" + statusCode + " " + error.getMessage());
                listener.onError(error);
                Log.e("DD","DownloadStaff Error");
                errors++;
            }
        });
    }

    private void DownloadShifts(final String userID, final String syncDate, final ShiftDownloadListener listener) {
        final List<String> shiftIDs = new LinkedList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadShifts Start");
        client.get(url + "Shifts?userID=" + userID + "&syncDate=" + syncDate, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray arr = new JSONArray();
                    arr = new JSONArray(response);

                    //Shift.deleteAllShifts();
                    //ShiftStaff.deleteAllShiftStaff();
                    //Accident.deleteAllAccidents();
                    //Observation.deleteAllObservations();
                    //Note.deleteAllNotes();
                    //ShiftVehicle.deleteAllShiftVehicles();
                    //LoadingSheet.deleteAllLoadingSheets();
                    //LoadingSheetEquipment.deleteAllLoadingSheetEquipment();

                    // add new shifts
                    for (int i = 0; i < arr.length(); i++) {
                        // add new shift
                        Shift shift = new Shift();
                        LinkedList<ShiftStaff> shiftStaff = new LinkedList<>();
                        LinkedList<Accident> shiftAccidents = new LinkedList<>();
                        LinkedList<Observation> shiftObservations = new LinkedList<>();
                        LinkedList<Note> shiftNotes = new LinkedList<>();
                        LinkedList<ShiftProgress> shiftProgressStatus = new LinkedList<>();
                        LinkedList<ShiftVehicle> shiftVehicles = new LinkedList<>();
                        LinkedList<LoadingSheet> shiftLoadingSheets = new LinkedList<>();
                        LinkedList<LoadingSheetEquipment> shiftLoadingSheetEquipment = new LinkedList<>();

                        Calendar shiftDate = Calendar.getInstance();
                        String shiftDateString = arr.getJSONObject(i).getString("ShiftDate").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                        Long timeInMillis = Long.valueOf(shiftDateString);
                        shiftDate.setTimeInMillis(timeInMillis);

                        shift.shiftID = arr.getJSONObject(i).getString("ShiftID");
                        shiftIDs.add(shift.shiftID);

                        shift.dayShift = arr.getJSONObject(i).getBoolean("DayShift");
                        shift.isCancelled = arr.getJSONObject(i).getBoolean("IsCancelled");
                        shift.userID = arr.getJSONObject(i).getString("UserID");
                        shift.shiftCompletionStatus = arr.getJSONObject(i).getInt("ShiftCompletionStatus");
                        shift.shiftDetails = arr.getJSONObject(i).getString("ShiftDetails");
                        shift.setShiftDate(shiftDate.getTime());
                        shift.shiftTypes = arr.getJSONObject(i).getString("JobTypes");
                        shift.address = arr.getJSONObject(i).getString("Address");
                        shift.trafficManagementActivities = arr.getJSONObject(i).getString("TrafficManagementActivities");
                        shift.srwNumber = arr.getJSONObject(i).getString("SRWNumber");
                        shift.projCode = arr.getJSONObject(i).getString("CustomerProjCode");
                        shift.cancellationReason = "";
                        shift.addressLine1 = arr.getJSONObject(i).getString("AddressLine1");
                        shift.addressLine2 = arr.getJSONObject(i).getString("AddressLine2");
                        shift.borough = arr.getJSONObject(i).getString("Borough");
                        shift.city = arr.getJSONObject(i).getString("City");
                        shift.county = arr.getJSONObject(i).getString("County");
                        shift.postcode = arr.getJSONObject(i).getString("Postcode");
                        shift.updatedDate = FMConwayUtils.parseAspNetDate(arr.getJSONObject(i).getString("UpdatedDate"));

                        final JSONArray shiftStaffJson = arr.getJSONObject(i).getJSONArray("Staff");
                        for (int j = 0; j < shiftStaffJson.length(); j++) {
                            ShiftStaff ss = new ShiftStaff();
                            ss.shiftID = arr.getJSONObject(i).getString("ShiftID");
                            ss.staffID = shiftStaffJson.getJSONObject(j).getString("StaffID");
                            ss.firstAider = shiftStaffJson.getJSONObject(j).getBoolean("FirstAider");

                            Calendar startTime = Calendar.getInstance();
                            if (!shiftStaffJson.getJSONObject(j).getString("StartTime").equals("null")) {
                                String startTimeString = shiftStaffJson.getJSONObject(j).getString("StartTime").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                timeInMillis = Long.valueOf(startTimeString);
                                startTime.setTimeInMillis(timeInMillis);

                                ss.startTime = startTime.getTime();
                            }

                            Calendar endTime = Calendar.getInstance();
                            if (!shiftStaffJson.getJSONObject(j).getString("EndTime").equals("null")) {
                                String endTimeString = shiftStaffJson.getJSONObject(j).getString("EndTime").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                timeInMillis = Long.valueOf(endTimeString);
                                endTime.setTimeInMillis(timeInMillis);

                                ss.endTime = endTime.getTime();
                            }

                            shiftStaff.add(ss);
                            
                            final JSONArray shiftStaffRecordsJson = shiftStaffJson.getJSONObject(j).getJSONArray("StaffRecords");
                            for (int a = 0; a < shiftStaffRecordsJson.length(); a++) {
                                //Check Staff Records if exists
                                String recordID = StaffRecord.GetStaffRecordID(shiftStaffRecordsJson.getJSONObject(a).getString("StaffRecordID"));
                                if (Objects.equals(recordID,"")) {
                                    //Request Document from server
                                    AsyncHttpClient dataClient = new AsyncHttpClient();
                                    dataClient.setTimeout(timeout);
                                    dataClient.addHeader("Authorization", encodedHeader);

                                    final int finalA = a;
                                    final int finalJ = j;
                                    dataClient.get(url + "StaffRecord?recordID=" + shiftStaffRecordsJson.getJSONObject(a).getString("StaffRecordID"), null, new AsyncHttpResponseHandler() {

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                            //Create File on device
                                            // External sdcard location

                                            String timeStamp = new SimpleDateFormat("ddMMyyyy",
                                                    Locale.getDefault()).format(new Date());

                                            File mediaStorageDir = new File(
                                                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "FMConway" + File.separator + timeStamp + File.separator + UUID.randomUUID().toString());

                                            // Create the storage directory if it does not exist
                                            if (!mediaStorageDir.exists()) {
                                                if (!mediaStorageDir.mkdirs()) {
                                                    Toast.makeText(ctx, "Unable to create data directory.", Toast.LENGTH_LONG).show();
                                                }
                                            }

                                            // Create a media file name
                                            try {
                                                //File mediaFile = new File(mediaStorageDir.getPath() + File.separator + shiftStaffRecordsJson.getJSONObject(finalA).getString("RecordName") + shiftStaffRecordsJson.getJSONObject(finalA).getString("ContentType"));
                                                File mediaFile = new File(mediaStorageDir.getPath() + File.separator + shiftStaffRecordsJson.getJSONObject(finalA).getString("RecordName"));

                                                String response = "";
                                                try {
                                                    response = new String(responseBody, "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }

                                                byte[] docData = Base64.decode(response, Base64.DEFAULT);

                                                try {
                                                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mediaFile));
                                                    try {
                                                        bos.write(docData);
                                                        bos.flush();
                                                        bos.close();
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                } catch (FileNotFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                //Create Staff Record
                                                StaffRecord staffRecord = new StaffRecord();
                                                staffRecord.staffRecordID = shiftStaffRecordsJson.getJSONObject(finalA).getString("StaffRecordID");
                                                staffRecord.staffID = shiftStaffJson.getJSONObject(finalJ).getString("StaffID");
                                                staffRecord.recordName = shiftStaffRecordsJson.getJSONObject(finalA).getString("RecordName");
                                                staffRecord.contentType = shiftStaffRecordsJson.getJSONObject(finalA).getString("ContentType");
                                                staffRecord.imageLocation = mediaFile.getPath();
                                                StaffRecord.addStaffRecord(staffRecord);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                            Log.e("FMConway", "Download Shifts - Record Download" + statusCode + " " + error.getMessage());
                                        }
                                    });
                                }
                            }
                        }


                        JSONArray shiftNoteJSON = arr.getJSONObject(i).getJSONArray("Notes");
                        for (int a = 0; a < shiftNoteJSON.length(); a++) {
                            Note note = new Note();
                            note.noteID = shiftNoteJSON.getJSONObject(a).getString("NoteID");
                            note.shiftID = shiftNoteJSON.getJSONObject(a).getString("ShiftID");
                            note.noteDetails = shiftNoteJSON.getJSONObject(a).getString("NoteDetails");
                            note.userID = userID;
                            note.isNew = false;

                            Calendar noteDate = Calendar.getInstance();
                            if (!shiftNoteJSON.getJSONObject(a).getString("NoteDate").equals("null")) {
                                String noteDateString = shiftNoteJSON.getJSONObject(a).getString("NoteDate").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                timeInMillis = Long.valueOf(noteDateString);
                                noteDate.setTimeInMillis(timeInMillis);

                                note.noteDate = noteDate.getTime();
                            }

                            shiftNotes.add(note);
                        }

                        JSONArray vehiclesJson = arr.getJSONObject(i).getJSONArray("ShiftVehicles");
                        for (int j = 0; j < vehiclesJson.length(); j++) {
                            ShiftVehicle jpv = new ShiftVehicle();
                            jpv.shiftID = arr.getJSONObject(i).getString("ShiftID");
                            String staffID = vehiclesJson.getJSONObject(j).getString("StaffID");
                            if (!Objects.equals(staffID, "null")) {
                                jpv.staffID = staffID;
                            }
                            jpv.vehicleID = vehiclesJson.getJSONObject(j).getString("VehicleID");
                            jpv.userID = userID;
                            jpv.startMileage = vehiclesJson.getJSONObject(j).getInt("StartMileage");
                            jpv.endMileage = vehiclesJson.getJSONObject(j).getInt("EndMileage");
                            jpv.loadingSheetCompleted = vehiclesJson.getJSONObject(j).getBoolean("LoadingSheetCompleted");
                            shiftVehicles.add(jpv);

                            JSONArray loadingSheetsJson = vehiclesJson.getJSONObject(j).getJSONArray("LoadingSheets");
                            for (int a = 0; a < loadingSheetsJson.length(); a++) {
                                LoadingSheet ls = new LoadingSheet();
                                ls.loadingSheetID = loadingSheetsJson.getJSONObject(a).getString("LoadingSheetID");
                                ls.vehicleID = vehiclesJson.getJSONObject(j).getString("VehicleID");
                                ls.shiftID = arr.getJSONObject(i).getString("ShiftID");
                                shiftLoadingSheets.add(ls);

                                JSONArray loadingSheetEquipmentJson = loadingSheetsJson.getJSONObject(a).getJSONArray("LoadingSheetEquipment");
                                for (int b = 0; b < loadingSheetEquipmentJson.length(); b++) {
                                    LoadingSheetEquipment lse = new LoadingSheetEquipment();
                                    lse.loadingSheetEquipmentID = loadingSheetEquipmentJson.getJSONObject(b).getString("LoadingSheetEquipmentID");
                                    lse.loadingSheetID = loadingSheetsJson.getJSONObject(a).getString("LoadingSheetID");
                                    lse.equipmentID = loadingSheetEquipmentJson.getJSONObject(b).getString("EquipmentID");
                                    lse.quantity = loadingSheetEquipmentJson.getJSONObject(b).getInt("Quantity");
                                    lse.sheetOrder = loadingSheetEquipmentJson.getJSONObject(b).getInt("SheetOrder");
                                    lse.shiftID = arr.getJSONObject(i).getString("ShiftID");
                                    lse.vmsOrAsset = loadingSheetEquipmentJson.getJSONObject(b).getString("VmsOrAsset");
                                    
                                    // multiple vehicles per shift can cause duplicate loading sheet equipment, check and skip any duplicates
                                    boolean alreadyExists = false;
                                    for (int c = 0; c < shiftLoadingSheetEquipment.size(); c++) {
                                        LoadingSheetEquipment loadingSheetEquipmentCheck = shiftLoadingSheetEquipment.get(c);
                                        if (loadingSheetEquipmentCheck.loadingSheetEquipmentID.equals(lse.loadingSheetEquipmentID)) {
                                            alreadyExists = true;
                                            break;
                                        }
                                    }
                                    
                                    if (!alreadyExists) {
                                        shiftLoadingSheetEquipment.add(lse);
                                    }
                                }
                            }
                        }

                        JSONArray shiftProgressJSON = arr.getJSONObject(i).getJSONArray("ShiftProgress");
                        for (int a = 0; a < shiftProgressJSON.length(); a++) {

                            ShiftProgress shiftProgress = new ShiftProgress();
                            shiftProgress.shiftID = shiftProgressJSON.getJSONObject(a).getString("ShiftID");
                            shiftProgress.progress = shiftProgressJSON.getJSONObject(a).getString("Progress");
                            shiftProgress.sent = false;

                            Calendar time = Calendar.getInstance();
                            if (!shiftProgressJSON.getJSONObject(a).getString("CreatedDate").equals("null")) {
                                String timeString = shiftProgressJSON.getJSONObject(a).getString("CreatedDate").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                Long timeMillis = Long.valueOf(timeString);
                                time.setTimeInMillis(timeMillis);

                                shiftProgress.createdDate = time.getTime();
                                shiftProgress.sent = true;
                            }

                            shiftProgressStatus.add(shiftProgress);

                        }

                        String shiftID = shift.shiftID;
                        String userID = shift.userID;

                        //Shift.deleteShiftForID(shiftID);
                        //ShiftStaff.deleteShiftStaff(shiftID);
                        //ShiftVehicle.deleteShiftVehicles(shiftID);
                        //LoadingSheet.deleteLoadingSheets(shiftID);
                        //LoadingSheetEquipment.deleteLoadingSheetEquipmentForShift(shiftID);
                        //VehicleChecklist.deleteAllVehicleChecklistsForShift(shiftID);
                        //Note.deleteAllNotesForShift(shiftID);
                        //ShiftProgress.deleteShiftProgressForID(shiftID);

                        //Accident.deleteAllAccidents();
                        //Observation.deleteAllObservations();

                        Shift.addShift(shift);

                        // if shift has not started, clear shift staff and shift vehicles
                        if (!ShiftProgress.DoesProgressExist(shiftID, "ChecksCompleted")) {
                            ShiftStaff.deleteShiftStaff(shift.shiftID);
                            ShiftVehicle.deleteShiftVehicles(shift.shiftID);
                        }

                        for (int j = 0; j < shiftStaff.size(); j++) {
                            ShiftStaff.addShiftStaff(shiftStaff.get(j));
                        }
                        for (int j = 0; j < shiftAccidents.size(); j++) {
                            Accident.AddAccident(shiftAccidents.get(j));
                        }
                        for (int j = 0; j < shiftObservations.size(); j++) {
                            Observation.AddObservation(shiftObservations.get(j));
                        }
                        for (int j = 0; j < shiftNotes.size(); j++) {
                            Note.AddNote(shiftNotes.get(j));
                        }
                        for (int j = 0; j < shiftVehicles.size(); j++) {
                            ShiftVehicle.addShiftVehicles(shiftVehicles.get(j));
                        }
                        for (int j = 0; j < shiftLoadingSheets.size(); j++) {
                            LoadingSheet.addLoadingSheet(shiftLoadingSheets.get(j));
                        }
                        for (int j = 0; j < shiftLoadingSheetEquipment.size(); j++) {
                            LoadingSheetEquipment.addLoadingSheetEquipment(shiftLoadingSheetEquipment.get(j));
                        }
                        for (int j = 0; j < shiftProgressStatus.size(); j++) {
                            ShiftProgress.addShiftProgress(shiftProgressStatus.get(j));
                        }
                    }

                    downloaded += 1;
                    listener.onComplete(shiftIDs);
                } catch (Exception e) {

                    errors++;
                    listener.onError(e, shiftIDs);
                    Log.e("DD","DownloadShifts Error");
                    ErrorLog.CreateError(e, "DownloadShifts", response);
                }

                Log.d("DD","DownloadShifts End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Shifts" + statusCode + " " + error.getMessage());
                ErrorLog.CreateError(error, "DownloadShifts");
                errors++;
                listener.onError(error, shiftIDs);
                Log.e("DD","DownloadShifts Error");
            }
        });
    }

    private void DownloadJobPacks(final String userID, final String syncDate, final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadJobPacks Start");
        client.get(url + "JobPacks?userID=" + userID + "&syncDate=" + syncDate, null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = null;
                try {

                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    //JobPack.deleteAllJobPacks();
                    //Briefing.deleteAllBriefings();
                    //BriefingChecklist.deleteAllBriefingChecklists();
                    //Activity.deleteAllActivities();
                    //MaintenanceCheck.deleteAllMaintenanceChecks();
                    //MaintenanceCheckAnswer.deleteAllMaintenanceCheckAnswers();
                    //MaintenanceCheckDetail.deleteAllMaintenanceCheckDetails(); // Not pulled down so don't delete.
                    //MaintenanceCheckDetailPhoto.deleteAllMaintenanceCheckDetailPhotos(); // Not pulled down so don't delete.
                    //Task.deleteAllTasks();

                    for (int i = 0; i < arr.length(); i++) {
                        JobPack jobPack = new JobPack();
                        LinkedList<Briefing> jobPackBriefings = new LinkedList<>();
                        LinkedList<BriefingChecklist> jobPackBriefingChecklists = new LinkedList<>();
                        LinkedList<Activity> jobPackActivities = new LinkedList<>();
                        LinkedList<Task> jobPackTasks = new LinkedList<>();
                        LinkedList<MaintenanceCheck> activityMaintenanceChecks = new LinkedList<>();
                        LinkedList<MaintenanceCheckAnswer> maintenanceCheckAnswers = new LinkedList<>();
                        LinkedList<ActivityMethodStatement> methodStatements = new LinkedList<>();

                        jobPack.jobPackID = arr.getJSONObject(i).getString("JobPackID");
                        jobPack.shiftID = arr.getJSONObject(i).getString("ShiftID");
                        jobPack.jobPackName = arr.getJSONObject(i).getString("JobPackName");

                        // Delete and download

                        String shiftID = arr.getJSONObject(i).getString("ShiftID");

                        /*List<JobPack> jobPacks = JobPack.GetJobPacksForShift(shiftID);
                        for (int k = 0; k<jobPacks.size();k++)
                        {
                            String jobPackID = jobPacks.get(k).jobPackID;

                            Briefing.deleteBriefingsForJobPack(jobPackID);
                            Activity.deleteActivitiesForJobPack(jobPackID);
                        }*/

                        /*List<Activity> activities = Activity.GetActivitiesForShift(shiftID);
                        for (int k = 0; k < activities.size(); k++)
                        {
                            String activityID = activities.get(k).activityID;

                            ActivityMethodStatement.deleteMethodStatementsForActivity(activityID);
                            MaintenanceCheck.deleteMaintenanceChecksForActivity(activityID);
                            MaintenanceCheckDetail.deleteMaintenanceCheckDetailsForActivity(activityID);
                            Task.deleteTasksForActivity(activityID);
                        }*/

                        //BriefingChecklist.deleteBriefingChecklistsForUser(userID);
                        //JobPack.deleteJobPacksForShift(shiftID);

                        final JSONArray documentsJson = arr.getJSONObject(i).getJSONArray("Documents");
                        for (int j = 0; j < documentsJson.length(); j++) {
                            //Check Job Pack Document if exists
                            String recordID = JobPackDocument.GetJobPackDocumentID(documentsJson.getJSONObject(j).getString("JobPackDocumentID"));
                            if (Objects.equals(recordID,"")) {
                                //Request Document from server
                                AsyncHttpClient dataClient = new AsyncHttpClient();
                                dataClient.setTimeout(timeout);
                                dataClient.addHeader("Authorization", encodedHeader);

                                final int finalJ = j;
                                final int finalI = i;
                                final JSONArray finalArr = arr;
                                dataClient.get(url + "Document?documentID=" + documentsJson.getJSONObject(j).getString("JobPackDocumentID") + "&jobPackID=" + documentsJson.getJSONObject(j).getString("JobPackID") + "&briefing=false", null, new AsyncHttpResponseHandler() {

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        //Create File on device
                                        // External sdcard location
                                        String timeStamp = new SimpleDateFormat("ddMMyyyy",
                                                Locale.getDefault()).format(new Date());
                                        File mediaStorageDir = new File(
                                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "FMConway" + File.separator + timeStamp + File.separator + UUID.randomUUID().toString());

                                        // Create the storage directory if it does not exist
                                        if (!mediaStorageDir.exists()) {
                                            if (!mediaStorageDir.mkdirs()) {
                                                Toast.makeText(ctx, "Unable to create data directory.", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        // Create a media file name
                                        try {
                                            String filename = documentsJson.getJSONObject(finalJ).getString("DocumentName");

                                            filename = filename.replace(" ", "_");
                                            filename = filename.replace("&","");

                                            File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                                                    + filename); // + documentsJson.getJSONObject(finalJ).getString("ContentType")

                                            String response = "";
                                            try {
                                                response = new String(responseBody, "UTF-8");
                                            } catch (UnsupportedEncodingException e) {
                                                ErrorLog.CreateError(e, "GetDocument");
                                                return;
                                            }

                                            byte[] docData = Base64.decode(response, Base64.DEFAULT);

                                            try {
                                                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mediaFile));
                                                try {
                                                    bos.write(docData);
                                                    bos.flush();
                                                    bos.close();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (FileNotFoundException e) {
                                                e.printStackTrace();
                                            }
                                            //Create Job Pack Document
                                            JobPackDocument jobPackDocument = new JobPackDocument();
                                            jobPackDocument.jobPackDocumentID = documentsJson.getJSONObject(finalJ).getString("JobPackDocumentID");
                                            jobPackDocument.jobPackID = finalArr.getJSONObject(finalI).getString("JobPackID");
                                            jobPackDocument.documentName = documentsJson.getJSONObject(finalJ).getString("DocumentName");
                                            jobPackDocument.contentType = documentsJson.getJSONObject(finalJ).getString("ContentType");
                                            jobPackDocument.imageLocation = mediaFile.getPath();
                                            JobPackDocument.addJobPackDocument(jobPackDocument);
                                        } catch (JSONException e) {
                                            ErrorLog.CreateError(e, "GetDocument");
                                            Log.e("DD","DownloadJobPacks Error");
                                        }
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        ErrorLog.CreateError(error, "GetDocument");
                                        Log.e("FMConway", "Download Job Pack Documents" + statusCode + " " + error.getMessage());
                                        errors++;
                                    }
                                });
                            }
                        }


                        JSONArray briefingJSON = arr.getJSONObject(i).getJSONArray("Briefings");
                        for (int j = 0; j < briefingJSON.length(); j++) {
                            final Briefing briefing = new Briefing();
                            briefing.briefingID = briefingJSON.getJSONObject(j).getString("BriefingID");
                            briefing.briefingTypeID = briefingJSON.getJSONObject(j).getInt("BriefingTypeID");
                            briefing.jobPackID = arr.getJSONObject(i).getString("JobPackID");
                            briefing.name = briefingJSON.getJSONObject(j).getString("Name");
                            briefing.details = briefingJSON.getJSONObject(j).getString("Details");
                            briefing.documentID = briefingJSON.getJSONObject(j).getString("FileID");
                            briefing.documentName = briefingJSON.getJSONObject(j).getString("DocumentName");
                            briefing.contentType = briefingJSON.getJSONObject(j).getString("ContentType");
                            briefing.userID = userID;

                            if (!Objects.equals(briefing.documentID,"null")) {
                                //Request Document from server
                                AsyncHttpClient dataClient = new AsyncHttpClient();
                                dataClient.setTimeout(timeout);
                                dataClient.addHeader("Authorization", encodedHeader);

                                dataClient.get(url + "Document?documentID=" + briefing.documentID + "&jobPackID=" + briefing.jobPackID + "&briefing=true", null, new AsyncHttpResponseHandler() {

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        //Create File on device
                                        // External sdcard location

                                        String timeStamp = new SimpleDateFormat("ddMMyyyy",
                                                Locale.getDefault()).format(new Date());

                                        File mediaStorageDir = new File(
                                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "FMConway" + File.separator + timeStamp + File.separator + UUID.randomUUID().toString());

                                        // Create the storage directory if it does not exist
                                        if (!mediaStorageDir.exists()) {
                                            if (!mediaStorageDir.mkdirs()) {
                                                Toast.makeText(ctx, "Unable to create data directory.", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        // Create a media file name
                                        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                                                + briefing.documentName); // + briefing.contentType

                                        String response = "";
                                        try {
                                            response = new String(responseBody, "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                        byte[] docData = Base64.decode(response, Base64.DEFAULT);

                                        try {
                                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mediaFile));
                                            try {
                                                bos.write(docData);
                                                bos.flush();
                                                bos.close();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        }
                                        Briefing.SetImageLocation(briefing, mediaFile.getPath());
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                         Log.e("FMConway", "Download Shifts - Record Download" + statusCode + " " + error.getMessage());
                                        errors++;
                                    }
                                });
                            }

                            jobPackBriefings.add(briefing);

                            JSONArray briefingChecklistJSON = briefingJSON.getJSONObject(j).getJSONArray("Checklists");
                            for (int a = 0; a < briefingChecklistJSON.length(); a++) {
                                BriefingChecklist briefingChecklist = new BriefingChecklist();
                                briefingChecklist.briefingID = briefingJSON.getJSONObject(j).getString("BriefingID");
                                briefingChecklist.checklistID = briefingChecklistJSON.getJSONObject(a).getString("ChecklistID");
                                briefingChecklist.userID = userID;
                                briefingChecklist.shiftID = jobPack.shiftID;
                                jobPackBriefingChecklists.add(briefingChecklist);
                            }
                        }

                        JSONArray activityJSON = arr.getJSONObject(i).getJSONArray("Activities");
                        for (int j = 0; j < activityJSON.length(); j++) {
                            Activity activity = new Activity();
                            activity.activityID = activityJSON.getJSONObject(j).getString("ActivityID");
                            activity.activityTypeID = activityJSON.getJSONObject(j).getString("ActivityTypeID");
                            activity.name = activityJSON.getJSONObject(j).getString("Name");
                            activity.customer = activityJSON.getJSONObject(j).getString("Customer");
                            activity.customerPhoneNumber = activityJSON.getJSONObject(j).getString("CustomerPhoneNumber");
                            activity.contract = activityJSON.getJSONObject(j).getString("Contract");
                            activity.contractNumber = activityJSON.getJSONObject(j).getString("ContractNumber");
                            activity.scheme = activityJSON.getJSONObject(j).getString("Scheme");
                            activity.scopeOfWork = activityJSON.getJSONObject(j).getString("ScopeOfWork");
                            activity.areaCallNumber = activityJSON.getJSONObject(j).getString("AreaCallNumber");
                            activity.areaCallProtocol = activityJSON.getJSONObject(j).getString("AreaCallProtocol");
                            activity.maintenanceChecklistReminderInMinutes = activityJSON.getJSONObject(j).getInt("MaintenanceChecklistReminderInMinutes");
                            activity.firstCone = activityJSON.getJSONObject(j).getString("FirstCone");
                            activity.lastCone = activityJSON.getJSONObject(j).getString("LastCone");
                            activity.distance = activityJSON.getJSONObject(j).getDouble("Distance");
                            activity.road = activityJSON.getJSONObject(j).getString("Road");
                            activity.cWayDirection = activityJSON.getJSONObject(j).getString("CWayDirection");

                            Calendar installationTime = Calendar.getInstance();
                            if (!activityJSON.getJSONObject(j).getString("InstallationTime").equals("null")) {
                                String installationTimeString = activityJSON.getJSONObject(j).getString("InstallationTime").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                Long timeInMillis = Long.valueOf(installationTimeString);
                                installationTime.setTimeInMillis(timeInMillis);

                                activity.installationTime = installationTime.getTime();
                            }

                            Calendar removalTime = Calendar.getInstance();
                            if (!activityJSON.getJSONObject(j).getString("RemovalTime").equals("null")) {
                                String removalTimeString = activityJSON.getJSONObject(j).getString("RemovalTime").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                Long timeInMillis = Long.valueOf(removalTimeString);
                                removalTime.setTimeInMillis(timeInMillis);

                                activity.removalTime = removalTime.getTime();
                            }

                            activity.healthAndSafety = activityJSON.getJSONObject(j).getString("HealthAndSafety");
                            activity.worksInstructionNumber = activityJSON.getJSONObject(j).getString("WorksInstructionNumber");
                            activity.worksInstructionComments = activityJSON.getJSONObject(j).getString("WorksInstructionComments");
                            activity.clientReferenceNumber = activityJSON.getJSONObject(j).getString("ClientReferenceNumber");
                            activity.customerID = activityJSON.getJSONObject(j).getString("CustomerID");
                            activity.jobPackID = arr.getJSONObject(i).getString("JobPackID");
                            activity.isOutOfHours = activityJSON.getJSONObject(j).getBoolean("IsOutOfHours");
                            activity.workInstructionStartDate = FMConwayUtils.parseAspNetDate(activityJSON.getJSONObject(j).getString("WorkInstructionStartDate"));
                            activity.workInstructionFinishDate = FMConwayUtils.parseAspNetDate(activityJSON.getJSONObject(j).getString("WorkInstructionFinishDate"));
                            activity.depotID = activityJSON.getJSONObject(j).getString("DepotID");
                            activity.orderNumber = activityJSON.getJSONObject(j).getInt("OrderNumber");
                            jobPackActivities.add(activity);

                            JSONArray activityTaskJSON = activityJSON.getJSONObject(j).getJSONArray("Tasks");
                            for (int a = 0; a < activityTaskJSON.length(); a++) {
                                Task task = new Task();
                                task.taskID = activityTaskJSON.getJSONObject(a).getString("TaskID");
                                task.name = activityTaskJSON.getJSONObject(a).getString("Name");
                                task.taskOrder = activityTaskJSON.getJSONObject(a).getInt("TaskOrder");
                                task.taskTypeID = activityTaskJSON.getJSONObject(a).getInt("TaskTypeID");
                                task.areaID = activityTaskJSON.getJSONObject(a).getString("AreaID");
                                
                                String checklistID = activityTaskJSON.getJSONObject(a).getString("ChecklistID");
                                if (!Objects.equals(checklistID, "null")) {
                                    task.checklistID = checklistID;
                                }
                                
                                task.activityID = activityJSON.getJSONObject(j).getString("ActivityID");
                                task.notes = activityTaskJSON.getJSONObject(a).getString("Notes");
                                task.photosRequired = activityTaskJSON.getJSONObject(a).getBoolean("PhotosRequired");
                                task.showDetails = activityTaskJSON.getJSONObject(a).getBoolean("ShowDetails");
                                task.showClientSignature = activityTaskJSON.getJSONObject(a).getBoolean("ShowClientSignature");
                                task.clientSignatureRequired = activityTaskJSON.getJSONObject(a).getBoolean("ClientSignatureRequired");
                                task.showStaffSignature = activityTaskJSON.getJSONObject(a).getBoolean("ShowStaffSignature");
                                task.staffSignatureRequired = activityTaskJSON.getJSONObject(a).getBoolean("StaffSignatureRequired");
                                task.tag = activityTaskJSON.getJSONObject(a).getString("Tag");
                                task.isMandatory = activityTaskJSON.getJSONObject(a).getBoolean("IsMandatory");

                                String weatherConditionID = activityTaskJSON.getJSONObject(a).getString("WeatherConditionID");
                                if (!Objects.equals(weatherConditionID, "null")) {
                                    task.weatherConditionID = weatherConditionID; 
                                }

                                Calendar taskStart = Calendar.getInstance();
                                if (!activityTaskJSON.getJSONObject(a).getString("StartTime").equals("null")) {
                                    String taskStartString = activityTaskJSON.getJSONObject(a).getString("StartTime").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                    Long timeInMillis = Long.valueOf(taskStartString);
                                    taskStart.setTimeInMillis(timeInMillis);

                                    task.startTime = taskStart.getTime();
                                }

                                Calendar taskEnd = Calendar.getInstance();
                                if (!activityTaskJSON.getJSONObject(a).getString("EndTime").equals("null")) {
                                    String taskEndString = activityTaskJSON.getJSONObject(a).getString("EndTime").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                    Long timeInMillis = Long.valueOf(taskEndString);
                                    taskEnd.setTimeInMillis(timeInMillis);

                                    task.endTime = taskEnd.getTime();
                                }

                                JSONArray taskEquipmentJSON = activityTaskJSON.getJSONObject(a).getJSONArray("TaskEquipment");
                                for (int b = 0; b < taskEquipmentJSON.length(); b++) {
                                    TaskEquipment te = new TaskEquipment();
                                    te.TaskEquipmentID = taskEquipmentJSON.getJSONObject(b).getString("TaskEquipmentID");
                                    te.TaskID = taskEquipmentJSON.getJSONObject(b).getString("TaskID");
                                    te.ShiftID = arr.getJSONObject(i).getString("ShiftID");
                                    te.EquipmentID = taskEquipmentJSON.getJSONObject(b).getString("EquipmentID");
                                    te.Quantity = taskEquipmentJSON.getJSONObject(b).getInt("Quantity");
                                    te.UserID = userID;
                                    te.Updated = false;
                                    TaskEquipment.addTaskEquipment(te);

                                    JSONArray taskEquipmentAssetsJSON = taskEquipmentJSON.getJSONObject(b).getJSONArray("TaskEquipmentAssets");
                                    for (int c = 0; c < taskEquipmentAssetsJSON.length(); c++) {
                                        TaskEquipmentAsset tea = new TaskEquipmentAsset();
                                        tea.TaskEquipmentAssetID = taskEquipmentAssetsJSON.getJSONObject(c).getString("TaskEquipmentAssetID");
                                        tea.TaskEquipmentID = taskEquipmentAssetsJSON.getJSONObject(c).getString("TaskEquipmentID");
                                        tea.ItemNumber = taskEquipmentAssetsJSON.getJSONObject(c).getInt("ItemNumber");
                                        tea.AssetNumber = taskEquipmentAssetsJSON.getJSONObject(c).getString("AssetNumber");
                                        tea.ShiftID = arr.getJSONObject(i).getString("ShiftID");

                                        tea.insertOrUpdate();
                                    }
                                }

                                jobPackTasks.add(task);
                            }

                            JSONArray activityMaintenanceJSON = activityJSON.getJSONObject(j).getJSONArray("MaintenanceChecks");
                            for (int a = 0; a < activityMaintenanceJSON.length(); a++) {
                                MaintenanceCheck maintenanceCheck = new MaintenanceCheck();
                                maintenanceCheck.maintenanceCheckID = activityMaintenanceJSON.getJSONObject(a).getString("MaintenanceCheckID");
                                maintenanceCheck.activityID = activityJSON.getJSONObject(j).getString("ActivityID");
                                maintenanceCheck.description = activityMaintenanceJSON.getJSONObject(a).getString("Description");

                                Calendar time = Calendar.getInstance();
                                if (!activityMaintenanceJSON.getJSONObject(a).getString("Time").equals("null")) {
                                    String timeString = activityMaintenanceJSON.getJSONObject(a).getString("Time").replace("/Date(", "").replace("+0100)/", "").replace("+0000)/", "");
                                    Long timeInMillis = Long.valueOf(timeString);
                                    time.setTimeInMillis(timeInMillis);

                                    maintenanceCheck.setTime(time.getTime());
                                }

                                JSONArray maintenanceJSON = activityMaintenanceJSON.getJSONObject(a).getJSONArray("Answers");
                                for (int b = 0; b < maintenanceJSON.length(); b++) {
                                    MaintenanceCheckAnswer answer = new MaintenanceCheckAnswer();
                                    answer.maintenanceCheckID = activityMaintenanceJSON.getJSONObject(a).getString("MaintenanceCheckID");
                                    answer.answer = maintenanceJSON.getJSONObject(b).getBoolean("Answer");
                                    answer.checklistQuestionID = maintenanceJSON.getJSONObject(b).getString("ChecklistQuestionID");
                                    answer.activityID = activityJSON.getJSONObject(j).getString("ActivityID");

                                    maintenanceCheckAnswers.add(answer);
                                }

                                activityMaintenanceChecks.add(maintenanceCheck);
                            }

                            JSONArray methodStatementsJSON = activityJSON.getJSONObject(j).getJSONArray("MethodStatements");
                            for (int a = 0; a < methodStatementsJSON.length(); a++) {
                                JSONObject methodStatementJSON = methodStatementsJSON.getJSONObject(a);
                                ActivityMethodStatement maintenanceCheck = new ActivityMethodStatement();
                                maintenanceCheck.activityMethodStatementID = methodStatementJSON.getString("ActivityMethodStatementID");
                                maintenanceCheck.activityID = methodStatementJSON.getString("ActivityID");
                                maintenanceCheck.methodStatementID = methodStatementJSON.getString("MethodStatementID");
                                maintenanceCheck.isActive = methodStatementJSON.getBoolean("IsActive");

                                methodStatements.add(maintenanceCheck);
                            }
                        }

                        JobPack.addJobPack(jobPack);
                        for (int j = 0; j < jobPackBriefings.size(); j++) {
                            Briefing.addBriefing(jobPackBriefings.get(j));
                        }
                        for (int j = 0; j < jobPackBriefingChecklists.size(); j++) {
                            BriefingChecklist.addBriefingChecklist(jobPackBriefingChecklists.get(j));
                        }
                        for (int j = 0; j < jobPackActivities.size(); j++) {
                            Activity.addActivity(jobPackActivities.get(j));
                        }
                        for (int j = 0; j < jobPackTasks.size(); j++) {
                            Task.addTask(jobPackTasks.get(j));
                        }
                        for (int j = 0; j < activityMaintenanceChecks.size(); j++) {
                            MaintenanceCheck.AddMaintenanceCheck(activityMaintenanceChecks.get(j));
                        }
                        for (int j = 0; j < maintenanceCheckAnswers.size(); j++) {
                            MaintenanceCheckAnswer.addMaintenanceCheckAnswerDownload(maintenanceCheckAnswers.get(j));
                        }
                        for (int j = 0; j < methodStatements.size(); j++) {
                            ActivityMethodStatement.save(methodStatements.get(j));
                        }
                    }

                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {

                    errors++;
                    listener.onError(e);
                    Log.e("DD","DownloadJobPacks Error");
                    ErrorLog.CreateError(e, "DownloadJobPacks", response );
                }

                Log.d("DD","DownloadJobPacks End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ErrorLog.CreateError(error, "DownloadJobPacks");
                Log.e("FMConway", "Download Job Packs" + statusCode + " " + error.getMessage());
                errors++;
                listener.onError(error);
                Log.e("DD","DownloadJobPacks Error");
            }
        });
    }

    private void DownloadEquipment(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadEquipment Start");
        client.get(url + "Equipment", null, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String response = "";
                    try {
                        response = new String(responseBody, "UTF-8");

                        JSONArray arr = new JSONArray();

                        arr = new JSONArray(response);

                        Equipment.deleteAllEquipment();

                        for (int i = 0; i < arr.length(); i++) {
                            Equipment equipment = new Equipment();
                            equipment.equipmentID = arr.getJSONObject(i).getString("EquipmentID");
                            equipment.name = arr.getJSONObject(i).getString("Name");
                            equipment.equipmentTypeID = arr.getJSONObject(i).getString("EquipmentTypeID");

                            Equipment.addEquipment(equipment);
                        }
                        downloaded += 1;
                        listener.onComplete();
                    } catch (Exception e) {
                        listener.onError(e);
                        errors++;
                        Log.e("DD", "DownloadEquipment Error");
                    }

                    Log.d("DD", "DownloadEquipment End");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.e("FMConway", "Download Equipment" + statusCode + " " + error.getMessage());
                    listener.onError(error);
                    errors++;
                    Log.e("DD", "DownloadEquipment Error");
                }
            }

        );
    }

    private void DownloadEquipmentTypes(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadEquipmentTypes Start");
        client.get(url + "EquipmentTypes", null, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String response = "";
                        try {
                            response = new String(responseBody, "UTF-8");

                            JSONArray arr = new JSONArray();

                            arr = new JSONArray(response);

                            EquipmentType.deleteAllEquipmentTypes();

                            for (int i = 0; i < arr.length(); i++) {
                                EquipmentType equipmentType = new EquipmentType();
                                try {
                                    equipmentType.equipmentTypeID = arr.getJSONObject(i).getString("EquipmentTypeID");
                                    equipmentType.name = arr.getJSONObject(i).getString("Name");
                                    equipmentType.vmsOrAsset = arr.getJSONObject(i).getString("VmsOrAsset");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                EquipmentType.addEquipmentType(equipmentType);
                            }
                            downloaded += 1;
                            listener.onComplete();
                        } catch (Exception e) {
                            listener.onError(e);
                            errors++;
                            Log.e("DD","DownloadEquipmentTypes Error");
                        }

                        Log.d("DD","DownloadEquipmentTypes End");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e("FMConway", "Download Equipment Types" + statusCode + " " + error.getMessage());
                        listener.onError(error);
                        errors++;
                        Log.e("DD","DownloadEquipmentTypes Error");
                    }
                }

        );
    }

    private void DownloadChecklists(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadChecklists Start");
        client.get(url + "Checklists", null, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String response = "";
                    try {
                        response = new String(responseBody, "UTF-8");

                        JSONArray arr = new JSONArray();

                        arr = new JSONArray(response);

                        Checklist.deleteAllChecklists();
                        ChecklistQuestion.deleteAllChecklistQuestions();

                        for (int i = 0; i < arr.length(); i++) {
                            Checklist checklist = new Checklist();
                            LinkedList<ChecklistQuestion> checklistQuestions = new LinkedList<>();
                            checklist.checklistID = arr.getJSONObject(i).getString("ChecklistID");
                            checklist.name = arr.getJSONObject(i).getString("Name");
                            checklist.isMaintenance = arr.getJSONObject(i).getBoolean("IsMaintenance");
                            checklist.isPreHire = arr.getJSONObject(i).getBoolean("IsPreHire");
                            checklist.isVms = arr.getJSONObject(i).getBoolean("IsVms");
                            checklist.isBattery = arr.getJSONObject(i).getBoolean("IsBattery");
                            checklist.isPreHireBattery = arr.getJSONObject(i).getBoolean("IsPreHireBattery");
                            checklist.isPostHireBattery = arr.getJSONObject(i).getBoolean("IsPostHireBattery");
                            checklist.isHighSpeed = arr.getJSONObject(i).getBoolean("IsHighSpeed");
                            checklist.isLowSpeed = arr.getJSONObject(i).getBoolean("IsLowSpeed");
                            checklist.isJettingPermitToWork = arr.getJSONObject(i).getBoolean("IsJettingPermitToWork");
                            checklist.isJettingRiskAssessment = arr.getJSONObject(i).getBoolean("IsJettingRiskAssessment");
                            checklist.isTankeringPermitToWork = arr.getJSONObject(i).getBoolean("IsTankeringPermitToWork");
                            checklist.isTankeringRiskAssessment = arr.getJSONObject(i).getBoolean("IsTankeringRiskAssessment");
                            checklist.isCCTVRiskAssessment = arr.getJSONObject(i).getBoolean("IsCCTVRiskAssessment");
                            checklist.isJettingNewRiskAssessment = arr.getJSONObject(i).getBoolean("IsJettingNewRiskAssessment");

                            JSONArray checklistQuestionsJson = arr.getJSONObject(i).getJSONArray("ChecklistQuestions");
                            for (int j = 0; j < checklistQuestionsJson.length(); j++) {
                                ChecklistQuestion cq = new ChecklistQuestion();
                                cq.checklistQuestionID = checklistQuestionsJson.getJSONObject(j).getString("ChecklistQuestionID");
                                cq.question = checklistQuestionsJson.getJSONObject(j).getString("Question");
                                cq.questionOrder = checklistQuestionsJson.getJSONObject(j).getInt("QuestionOrder");
                                cq.checklistID = arr.getJSONObject(i).getString("ChecklistID");
                                checklistQuestions.add(cq);
                            }

                            Checklist.addChecklist(checklist);
                            for (int j = 0; j < checklistQuestions.size(); j++) {
                                ChecklistQuestion.addChecklistQuestion(checklistQuestions.get(j));
                            }
                        }
                        downloaded += 1;
                        listener.onComplete();

                    } catch (Exception e) {
                        listener.onError(e);
                        errors++;
                        Log.e("DD", "DownloadChecklists Error");
                    }

                    Log.d("DD", "DownloadChecklists End");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.e("FMConway", "Download Checklists" + statusCode + " " + error.getMessage());
                    listener.onError(error);
                    errors++;
                    Log.e("DD", "DownloadChecklists Error");
                }
            }

        );
    }

    private void DownloadVehicleTypes(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadVehicleTypes Start");
        client.get(url + "VehicleTypes", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    VehicleType.deleteAllVehicleTypes();
                    VehicleTypeChecklist.deleteAllVehicleTypeChecklists();

                    for (int i = 0; i < arr.length(); i++) {
                        VehicleType vehicleType = new VehicleType();

                        LinkedList<VehicleTypeChecklist> vehicleTypeChecklists = new LinkedList<>();
                        vehicleType.vehicleTypeID = arr.getJSONObject(i).getString("VehicleTypeID");
                        vehicleType.name = arr.getJSONObject(i).getString("Name");

                        JSONArray vehicleTypeChecklistsJson = arr.getJSONObject(i).getJSONArray("Checklists");
                        for (int j = 0; j < vehicleTypeChecklistsJson.length(); j++) {
                            VehicleTypeChecklist vtc = new VehicleTypeChecklist();
                            vtc.vehicleTypeID = arr.getJSONObject(i).getString("VehicleTypeID");
                            vtc.checklistID = vehicleTypeChecklistsJson.getJSONObject(j).getString("ChecklistID");
                            vehicleTypeChecklists.add(vtc);
                        }

                        VehicleType.addVehicleType(vehicleType);
                        for (int j = 0; j < vehicleTypeChecklists.size(); j++) {
                            VehicleTypeChecklist.addVehicleTypeChecklist(vehicleTypeChecklists.get(j));
                        }
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD","DownloadVehicleTypes Error");
                }

                Log.d("DD","DownloadVehicleTypes End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Vehicle Types" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD","DownloadVehicleTypes End");
            }
        });
    }

    private void DownloadVehicles(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadVehicles Start");
        client.get(url + "Vehicles", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    //shouldn't delete all vehicles incase inactive vehicle is on an active work instruction (referential constraint)
                    //Vehicle.deleteAllVehicles();

                    for (int i = 0; i < arr.length(); i++) {
                        Vehicle vehicle = new Vehicle();
                        vehicle.vehicleID = arr.getJSONObject(i).getString("VehicleID");
                        vehicle.make = arr.getJSONObject(i).getString("Make");
                        vehicle.model = arr.getJSONObject(i).getString("Model");
                        vehicle.registration = arr.getJSONObject(i).getString("Registration");
                        vehicle.depotID = arr.getJSONObject(i).getString("DepotID");
                        vehicle.vehicleTypeID = (arr.getJSONObject(i).getString("VehicleTypeID"));

                        if (Vehicle.exists(vehicle.vehicleID)) {
                            Vehicle.updateVehicle(vehicle);
                        } else {
                            Vehicle.addVehicle(vehicle);
                        }
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD","DownloadVehicles Error");
                }

                Log.d("DD","DownloadVehicles End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Vehicles" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD","DownloadVehicles Error");
            }
        });
    }

    private void DownloadBriefingTypes(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadBriefingTypes Start");
        client.get(url + "BriefingTypes", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    BriefingType.deleteAllBriefingTypes();

                    for (int i = 0; i < arr.length(); i++) {
                        BriefingType briefingType = new BriefingType();
                        try {
                            briefingType.briefingTypeID = arr.getJSONObject(i).getInt("BriefingTypeID");
                            briefingType.name = arr.getJSONObject(i).getString("Name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        BriefingType.addBriefingType(briefingType);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD","DownloadBriefingTypes Error");
                }

                Log.d("DD","DownloadBriefingTypes End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Briefing Types" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD","DownloadBriefingTypes Error");
            }
        });
    }

    private void DownloadActivityTypes(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadActivityTypes Start");
        client.get(url + "ActivityTypes", null, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String response = "";
                        try {
                            response = new String(responseBody, "UTF-8");

                            JSONArray arr = new JSONArray();

                            arr = new JSONArray(response);

                            ActivityType.deleteAllActivityTypes();

                            for (int i = 0; i < arr.length(); i++) {
                                ActivityType activityType = new ActivityType();
                                activityType.activityTypeID = arr.getJSONObject(i).getString("ActivityTypeID");
                                activityType.name = arr.getJSONObject(i).getString("Name");
                                activityType.trafficCountLimit = arr.getJSONObject(i).getInt("TrafficCountLimit");
                                activityType.displayName = arr.getJSONObject(i).getString("DisplayName");

                                ActivityType.addActivityType(activityType);
                            }
                            downloaded += 1;

                            listener.onComplete();

                        } catch (Exception e) {
                            listener.onError(e);
                            errors++;
                            Log.e("DD","DownloadActivityTypes Error");
                        }

                        Log.d("DD","DownloadActivityTypes End");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e("FMConway", "Download Activity Types" + statusCode + " " + error.getMessage());
                        listener.onError(error);
                        errors++;
                        Log.e("DD","DownloadActivityTypes Error");
                    }
                }

        );
    }

    private void DownloadObservationTypes(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadObservationTypes Start");
        client.get(url + "ObservationTypes", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    ObservationType.deleteAllObservationTypes();

                    for (int i = 0; i < arr.length(); i++) {
                        ObservationType observationType = new ObservationType();
                        observationType.observationTypeID = arr.getJSONObject(i).getString("ObservationTypeID");
                        observationType.name = arr.getJSONObject(i).getString("Name");

                        ObservationType.addObservationType(observationType);
                    }
                    downloaded += 1;
                    listener.onComplete();
                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD","DownloadObservationTypes Error");
                }
                Log.d("DD","DownloadObservationTypes End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Observation Types" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD","DownloadObservationTypes Error");
            }
        });
    }

    private void DownloadTaskTypes(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadTaskTypes Start");
        client.get(url + "TaskTypes", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    TaskType.deleteAllTaskTypes();

                    for (int i = 0; i < arr.length(); i++) {
                        TaskType taskType = new TaskType();
                        try {
                            taskType.taskTypeID = arr.getJSONObject(i).getInt("TaskTypeID");
                            taskType.name = arr.getJSONObject(i).getString("Name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        TaskType.addTaskType(taskType);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadTaskTypes Error");
                }

                Log.d("DD", "DownloadTaskTypes End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Task Types" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadTaskTypes Error");
            }
        });
    }

    private void DownloadActivityPriorities(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadActivityPriorities Start");
        client.get(url + "ActivityPriorities", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        ActivityPriority activityPriority = new ActivityPriority();
                        JSONObject obj = array.getJSONObject(i);
                        activityPriority.activityPriorityID = obj.getString("ActivityPriorityID");
                        activityPriority.name = obj.getString("Name");
                        activityPriority.isActive = obj.getBoolean("IsActive");

                        ActivityPriority.save(activityPriority);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadActivityPriorities Error");
                }

                Log.d("DD", "DownloadActivityPriorities End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadActivityPriorities " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadActivityPriorities Error");
            }
        });
    }

    private void DownloadLanes(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadLanes Start");
        client.get(url + "Lanes", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        Lane lane = new Lane();
                        JSONObject obj = array.getJSONObject(i);
                        lane.laneID = obj.getString("LaneID");
                        lane.name = obj.getString("Name");
                        lane.isActive = obj.getBoolean("IsActive");

                        Lane.save(lane);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadLanes Error");
                }

                Log.d("DD", "DownloadLanes End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadLanes " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadLanes Error");
            }
        });
    }

    private void DownloadTMRequirements(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadTMRequirements Start");
        client.get(url + "TMRequirements", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        TMRequirement tmRequirement = new TMRequirement();
                        JSONObject obj = array.getJSONObject(i);
                        tmRequirement.tmRequirementID = obj.getString("TMRequirementID");
                        tmRequirement.name = obj.getString("Name");
                        tmRequirement.isActive = obj.getBoolean("IsActive");

                        TMRequirement.save(tmRequirement);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadTMRequirements Error");
                }

                Log.d("DD", "DownloadTMRequirements End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadTMRequirements " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadTMRequirements Error");
            }
        });
    }

    private void DownloadRoadSpeeds(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadRoadSpeeds Start");
        client.get(url + "RoadSpeeds", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        RoadSpeed roadSpeed = new RoadSpeed();
                        JSONObject obj = array.getJSONObject(i);
                        roadSpeed.roadSpeedID = obj.getString("RoadSpeedID");
                        roadSpeed.speed = obj.getString("Speed");
                        roadSpeed.isActive = obj.getBoolean("IsActive");

                        RoadSpeed.save(roadSpeed);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadRoadSpeeds Error");
                }

                Log.d("DD", "DownloadRoadSpeeds End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadRoadSpeeds " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadRoadSpeeds Error");
            }
        });
    }

    private void DownloadDirections(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadDirections Start");
        client.get(url + "Directions", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        Direction direction = new Direction();
                        JSONObject obj = array.getJSONObject(i);
                        direction.directionID = obj.getString("DirectionID");
                        direction.name = obj.getString("Name");
                        direction.isActive = obj.getBoolean("IsActive");

                        Direction.save(direction);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadDirections Error");
                }

                Log.d("DD", "DownloadDirections End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadDirections " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadDirections Error");
            }
        });
    }

    private void DownloadDisposalSites(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadDisposalSites Start");
        client.get(url + "DisposalSites", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        DisposalSite disposalSite = new DisposalSite();
                        JSONObject obj = array.getJSONObject(i);
                        disposalSite.disposalSiteID = obj.getString("DisposalSiteID");
                        disposalSite.name = obj.getString("Name");
                        disposalSite.isActive = obj.getBoolean("IsActive");

                        DisposalSite.save(disposalSite);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadDisposalSites Error");
                }

                Log.d("DD", "DownloadDisposalSites End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadDisposalSites " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadDisposalSites Error");
            }
        });
    }

    private void DownloadCustomers(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadCustomers Start");
        client.get(url + "Customers", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        Customer customer = new Customer();
                        JSONObject obj = array.getJSONObject(i);
                        customer.customerID = obj.getString("CustomerID");
                        customer.customerName = obj.getString("CustomerName");
                        customer.accountNumber = obj.getString("AccountNumber");
                        customer.customerCode = obj.getString("CustomerCode");
                        customer.phoneNumber = obj.getString("PhoneNumber");
                        customer.isActive = obj.getBoolean("IsActive");

                        Customer.save(customer);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadCustomers Error");
                }

                Log.d("DD", "DownloadCustomers End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadCustomers " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadCustomers Error");
            }
        });
    }

    private void DownloadContacts(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadContacts Start");
        client.get(url + "Contacts", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        Contact contact = new Contact();
                        JSONObject obj = array.getJSONObject(i);
                        contact.contactID = obj.getString("ContactID");
                        contact.customerID = obj.getString("CustomerID");
                        contact.contactName = obj.getString("ContactName");
                        contact.phoneNumber = obj.getString("PhoneNumber");
                        contact.mobileNumber = obj.getString("MobileNumber");
                        contact.isActive = obj.getBoolean("IsActive");

                        Contact.save(contact);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadContacts Error");
                }

                Log.d("DD", "DownloadContacts End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadContacts " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadContacts Error");
            }
        });
    }

    private void DownloadMethodStatements(final DataDownloadProgressListener listener) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(FMConwayApp.getContext());
        Date syncDate = new Date(sp.getLong("MethodStatements-SyncDate", 0));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String dateString = dateFormat.format(syncDate);

        final AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadMethodStatements Start");
        client.get(url + "MethodStatements?syncDate=" + dateString, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        final MethodStatement methodStatement = new MethodStatement();
                        JSONObject obj = array.getJSONObject(i);
                        methodStatement.methodStatementID = obj.getString("MethodStatementID");
                        if (!obj.getString("DocumentID").equals("null")) {
                            methodStatement.documentID = obj.getString("DocumentID");
                        }
                        methodStatement.methodStatementTitle = obj.getString("MethodStatementTitle");
                        methodStatement.documentName = obj.getString("DocumentName");
                        methodStatement.contentType = obj.getString("ContentType");
                        methodStatement.isActive = obj.getBoolean("IsActive");

                        if (methodStatement.documentID != null) {
                            AsyncHttpClient dataClient = new AsyncHttpClient();
                            dataClient.setTimeout(timeout);
                            dataClient.addHeader("Authorization", encodedHeader);

                            dataClient.get(url + "MethodStatementDocument?methodStatementID=" + methodStatement.methodStatementID, null, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    String response = "";
                                    try {
                                        response = new String(responseBody, "UTF-8");
                                    } catch (UnsupportedEncodingException e) {
                                        ErrorLog.CreateError(e, "GetMethodStatementDocument");
                                    }

                                    if (!FMConwayUtils.isNullOrWhitespace(response)) {
                                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "FMConway" + File.separator + UUID.randomUUID().toString());

                                        // Create the storage directory if it does not exist
                                        if (!mediaStorageDir.exists()) {
                                            if (!mediaStorageDir.mkdirs()) {
                                                Toast.makeText(ctx, "Unable to create data directory.", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        String filename = methodStatement.documentName;

                                        filename = filename.replace(" ", "_");
                                        filename = filename.replace("&","");

                                        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename); // + documentsJson.getJSONObject(finalJ).getString("ContentType")

                                        byte[] docData = Base64.decode(response, Base64.DEFAULT);

                                        try {
                                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mediaFile));
                                            try {
                                                bos.write(docData);
                                                bos.flush();
                                                bos.close();
                                            } catch (IOException e) {
                                                ErrorLog.CreateError(e, "GetDocument");
                                            }
                                        } catch (FileNotFoundException e) {
                                            ErrorLog.CreateError(e, "GetDocument");
                                        }

                                        methodStatement.imageLocation = mediaFile.getPath();
                                    }

                                    MethodStatement.save(methodStatement);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    ErrorLog.CreateError(error, "GetMethodStatementDocument");
                                    Log.e("FMConway", "Download Method Statement Document" + statusCode + " " + error.getMessage());
                                    errors++;
                                }
                            });
                        } else {
                            MethodStatement.save(methodStatement);
                        }
                    }

                    downloaded += 1;
                    listener.onComplete();

                    Date date = new Date(System.currentTimeMillis());
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putLong("MethodStatements-SyncDate", date.getTime());
                    editor.apply();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadMethodStatements Error");
                }

                Log.d("DD", "DownloadMethodStatements End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadMethodStatements " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadMethodStatements Error");
            }
        });
    }

    private void DownloadDepots(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadDepots Start");
        client.get(url + "Depots", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {
                        Depot depot = new Depot();
                        JSONObject obj = array.getJSONObject(i);
                        depot.depotID = obj.getString("DepotID");
                        depot.depotName = obj.getString("DepotName");
                        depot.isActive = obj.getBoolean("IsActive");

                        Depot.save(depot);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadDepots Error");
                }

                Log.d("DD", "DownloadDepots End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadDepots " + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadDepots Error");
            }
        });
    }

    private void DownloadWeatherConditions(final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD", "DownloadWeatherConditions Start");
        client.get(url + "WeatherConditions", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    WeatherCondition.deleteAllWeatherConditions();

                    for (int i = 0; i < arr.length(); i++) {
                        WeatherCondition weatherCondition = new WeatherCondition();
                        try {
                            weatherCondition.weatherConditionID = arr.getJSONObject(i).getString("WeatherConditionID");
                            weatherCondition.name = arr.getJSONObject(i).getString("Name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        WeatherCondition.addWeatherCondition(weatherCondition);
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD", "DownloadWeatherConditions Error");
                }

                Log.d("DD", "DownloadWeatherConditions End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Weather Conditions" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD", "DownloadWeatherConditions Error");
            }
        });
    }

    private void DownloadAreas(final String userID, final String syncDate, final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DD","DownloadAreas Start");
        client.get(url + "Areas?userID=" + userID + "&syncDate=" + syncDate, null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    //don't delete all areas if only downloading records since last syncDate
                    //Area.deleteAllAreas();

                    for (int i = 0; i < arr.length(); i++) {
                        Area area = new Area();
                        area.areaID = arr.getJSONObject(i).getString("AreaID");
                        area.name = arr.getJSONObject(i).getString("Name");
                        area.callProtocol = arr.getJSONObject(i).getString("CallProtocol");
                        area.details = arr.getJSONObject(i).getString("Details");
                        area.reference = arr.getJSONObject(i).getString("Reference");
                        area.personsName = arr.getJSONObject(i).getString("PersonsName");
                        area.userID = userID;

                        if (Area.exists(area.areaID)) {
                            Area.updateArea(area);
                        } else {
                            Area.addArea(area);
                        }
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                    Log.e("DD","DownloadAreas Error");
                }

                Log.d("DD","DownloadAreas End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download Areas" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
                Log.e("DD","DownloadAreas Error");
            }
        });
    }

    private void DownloadVehicleChecklists(final String userID, final String syncDate, final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        Log.d("DVC","Start");
        client.get(url + "VehicleChecklists?userID=" + userID + "&syncDate=" + syncDate, null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONArray arr = new JSONArray(new String(responseBody, "UTF-8"));
                    
                    //VehicleChecklist.deleteAllVehicleChecklists();
                    //VehicleChecklistAnswer.deleteAllVehicleChecklistAnswers();
                    //VehicleChecklistAnswerDefect.deleteAllVehicleChecklistAnswerDefects();

                    for (int i = 0; i < arr.length(); i++) {
                        VehicleChecklist vc = new VehicleChecklist();
                        vc.vehicleChecklistID = arr.getJSONObject(i).getString("VehicleChecklistID");
                        vc.shiftID = arr.getJSONObject(i).getString("ShiftID");
                        vc.vehicleID = arr.getJSONObject(i).getString("VehicleID");
                        vc.checklistID = arr.getJSONObject(i).getString("ChecklistID");
                        vc.startOfShift = arr.getJSONObject(i).getBoolean("StartOfShift");
                        vc.adhoc = false;
                        vc.userID = userID;

                        String imageData = arr.getJSONObject(i).getString("ImageData");
                        String imageFilename = arr.getJSONObject(i).getString("ImageFilename");
                        
                        if (!Objects.equals(imageData, "null") && !Objects.equals(imageFilename, "null")) {
                            vc.imageData = imageData;
                            vc.imageFilename = imageFilename;
                        }
                        
                        VehicleChecklist.addVehicleChecklist(vc);

                        final JSONArray answersJSON = arr.getJSONObject(i).getJSONArray("Answers");
                        for (int j = 0; j < answersJSON.length(); j++) {
                            VehicleChecklistAnswer vca = new VehicleChecklistAnswer();

                            vca.checklistQuestionID = answersJSON.getJSONObject(j).getString("ChecklistQuestionID");
                            vca.answer = answersJSON.getJSONObject(j).getInt("Answer");
                            vca.vehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(vc.vehicleID, vc.shiftID, vc.checklistID, vc.startOfShift);
                            vca.userID = userID;

                            if (!VehicleChecklistAnswer.exists(vca.vehicleChecklistID, vca.checklistQuestionID)) {
                                VehicleChecklistAnswer.addVehicleChecklistAnswer(vca);
                            }
                        }

                        final JSONArray defectsJSON = arr.getJSONObject(i).getJSONArray("Defects");
                        for (int q = 0; q < defectsJSON.length(); q++) {
                            VehicleChecklistAnswerDefect vcad = new VehicleChecklistAnswerDefect();

                            vcad.checklistQuestionID = defectsJSON.getJSONObject(q).getString("ChecklistQuestionID");
                            vcad.defect = defectsJSON.getJSONObject(q).getString("Defect");
                            vcad.vehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(vc.vehicleID, vc.shiftID, vc.checklistID, vc.startOfShift);
                            vcad.userID = userID;

                            if (!VehicleChecklistAnswerDefect.exists(vcad.vehicleChecklistID, vcad.checklistQuestionID)) {
                                VehicleChecklistAnswerDefect.addVehicleChecklistAnswerDefect(vcad);
                            }
                        }
                    }
                    downloaded += 1;
                    listener.onComplete();

                } catch (Exception e) {
                    errors++;
                    listener.onError(e);
                    Log.d("DVC","Error");
                }

                Log.d("DVC","End");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download VehicleChecklists" + statusCode + " " + error.getMessage());
                errors++;
                listener.onError(error);
                Log.d("DVC","Error");
            }
        });
    }

    public void DownloadVehiclesOnShiftsToday(final DataDownloadProgressListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);

        client.get(url + "VehiclesOnShifts", null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");

                    JSONArray arr = new JSONArray();

                    arr = new JSONArray(response);

                    for (int i = 0; i < arr.length(); i++) {

                        ShiftVehicle jpv = new ShiftVehicle();
                        jpv.shiftID = arr.getJSONObject(i).getString("ShiftID");
                        String staffID = arr.getJSONObject(i).getString("StaffID");
                        if (!Objects.equals(staffID, "null")) {
                            jpv.staffID = staffID;
                        }
                        jpv.vehicleID = arr.getJSONObject(i).getString("VehicleID");
                        jpv.userID = userID;
                        jpv.startMileage = arr.getJSONObject(i).getInt("StartMileage");
                        jpv.endMileage = arr.getJSONObject(i).getInt("EndMileage");
                        jpv.loadingSheetCompleted = arr.getJSONObject(i).getBoolean("LoadingSheetCompleted");

                        ShiftVehicle.addShiftVehicles(jpv);
                    }

                    listener.onComplete();
                } catch (Exception e) {
                    listener.onError(e);
                    errors++;
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "DownloadVehiclesOnShiftsToday" + statusCode + " " + error.getMessage());
                listener.onError(error);
                errors++;
            }
        });
    }


    public void DownloadShiftUpdates(final DataDownloadProgressListener listener) {
        List<Shift> allShifts = Shift.GetShiftsForUser(userID);
        List<Shift> shifts = new LinkedList<>();

        // remove out of hours shifts
        for (int i = 0; i < allShifts.size(); i++) {
            Shift shift = allShifts.get(i);

            if (shift.isOutOfHours) {
                continue;
            }

            shifts.add(shift);
        }

        final int shiftsCount = shifts.size();

        DataDownloadProgressListener shiftUpdateListener = new DataDownloadProgressListener() {
            @Override
            public void onComplete() {
                ShiftUpdateComplete(shiftsCount, listener, null);
            }

            @Override
            public void onError(Throwable t) {
                ShiftUpdateComplete(shiftsCount, listener, t);
            }
        };

        if (shiftsCount > 0) {
            for (Shift shift : shifts) {
                DownloadShiftUpdate(shift.shiftID, shiftUpdateListener);
            }
        } else {
            downloaded++;
            listener.onComplete();
        }
    }

    // once all shifts have been updated, trigger parent listener
    public void ShiftUpdateComplete(int totalShifts, final DataDownloadProgressListener listener, Throwable t) {
        if ((shiftUpdatesDownloaded + shiftUpdatesErrors) >= totalShifts) {
            if (shiftUpdatesErrors == 0) {
                downloaded++;
                listener.onComplete();
            } else {
                errors++;
                listener.onError(t);
            }
        }
    }

    private void DownloadShiftUpdate(String shiftID, final DataDownloadProgressListener listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);
        client.addHeader("Authorization", encodedHeader);
        client.get(url + "ShiftUpdate?shiftID=" + shiftID, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject a = new JSONObject(new String(responseBody, "UTF-8"));

                    ShiftUpdate shiftUpdate = new ShiftUpdate();
                    shiftUpdate.shiftID = a.getString("ShiftID");
                    shiftUpdate.shiftComplete = a.getBoolean("ShiftComplete");
                    shiftUpdate.isCancelled = a.getBoolean("IsCancelled");
                    shiftUpdate.isActive = a.getBoolean("IsActive");
                    shiftUpdate.isOnShift = false;
                    shiftUpdate.shiftDate = FMConwayUtils.parseAspNetDate(a.getString("ShiftDate"));
                    shiftUpdate.updatedDate = FMConwayUtils.parseAspNetDate(a.getString("UpdatedDate"));
                    shiftUpdate.orderNumber = a.getInt("OrderNumber");
                    shiftUpdate.shiftVehicles = new LinkedList<>();
                    shiftUpdate.vehicleChecklists = new LinkedList<>();

                    // check if shift UserID is still on the shift
                    String shiftUserID = Shift.getShiftUserID(shiftUpdate.shiftID);

                    JSONArray shiftStaffIDs = a.getJSONArray("ShiftStaffs");
                    for (int i = 0; i < shiftStaffIDs.length(); i++) {
                        JSONObject b = shiftStaffIDs.getJSONObject(i);

                        String staffID = b.getString("StaffID");
                        String userID = b.getString("UserID");

                        if (userID.equalsIgnoreCase(shiftUserID)) {
                            shiftUpdate.isOnShift = true;
                        }
                    }

                    JSONArray shiftVehicles = a.getJSONArray("ShiftVehicles");
                    for (int i = 0; i < shiftVehicles.length(); i++) {
                        JSONObject b = shiftVehicles.getJSONObject(i);

                        ShiftVehicle shiftVehicle = new ShiftVehicle();
                        shiftVehicle.shiftID = b.getString("ShiftID");
                        shiftVehicle.vehicleID = b.getString("VehicleID");
                        
                        String staffID = b.getString("StaffID");
                        if (staffID != null && !staffID.equals("null")) {
                            shiftVehicle.staffID = staffID;
                        }
                        
                        shiftVehicle.startMileage = b.getInt("StartMileage");
                        shiftUpdate.shiftVehicles.add(shiftVehicle);
                    }

                    JSONArray vehicleChecklists = a.getJSONArray("VehicleChecklists");
                    for (int i = 0; i < vehicleChecklists.length(); i++) {
                        JSONObject b = vehicleChecklists.getJSONObject(i);

                        VehicleChecklist vehicleChecklist = new VehicleChecklist();
                        vehicleChecklist.vehicleChecklistID = b.getString("VehicleChecklistID");
                        vehicleChecklist.vehicleID = b.getString("VehicleID");
                        vehicleChecklist.checklistID = b.getString("ChecklistID");
                        vehicleChecklist.shiftID = b.getString("ShiftID");
                        vehicleChecklist.startOfShift = b.getBoolean("StartOfShift");

                        String imageData = b.getString("ImageData");
                        String imageFilename = b.getString("ImageFilename");
                        
                        if (!Objects.equals(imageData, "null") && !Objects.equals(imageFilename, "null")) {
                            vehicleChecklist.imageData = imageData;
                            vehicleChecklist.imageFilename = imageFilename;
                        }
                        
                        vehicleChecklist.answers = new LinkedList<>();
                        vehicleChecklist.defects = new LinkedList<>();
                        shiftUpdate.vehicleChecklists.add(vehicleChecklist);

                        JSONArray answers = b.getJSONArray("Answers");
                        for (int j = 0; j < answers.length(); j++) { 
                            JSONObject c = answers.getJSONObject(j);

                            VehicleChecklistAnswer answer = new VehicleChecklistAnswer();
                            answer.checklistQuestionID = c.getString("ChecklistQuestionID");
                            answer.answer = c.getInt("Answer");

                            vehicleChecklist.answers.add(answer);
                        }

                        JSONArray defects = b.getJSONArray("Defects");
                        for (int j = 0; j < defects.length(); j++) {
                            JSONObject c = defects.getJSONObject(j);

                            VehicleChecklistAnswerDefect defect = new VehicleChecklistAnswerDefect();
                            defect.vehicleChecklistID = c.getString("VehicleChecklistID");
                            defect.checklistQuestionID = c.getString("ChecklistQuestionID");
                            defect.defect = c.getString("Defect");
                            defect.userID = userID;
                            defect.photos = new LinkedList<>();

                            JSONArray photos = c.getJSONArray("Photos");
                            for (int k = 0; k < photos.length(); k++) {
                                JSONObject d = photos.getJSONObject(k);

                                VehicleChecklistAnswerDefectPhoto photo = new VehicleChecklistAnswerDefectPhoto();
                                photo.vehicleChecklistDefectPhotoID = d.getString("VehicleChecklistDefectPhotoID");
                                photo.vehicleChecklistID = d.getString("VehicleChecklistID");
                                photo.checklistQuestionID = d.getString("ChecklistQuestionID");
                                photo.userID = userID;
                                photo.longitude = d.getDouble("Longitude");
                                photo.latitude = d.getDouble("Latitude");
                                
                                File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");
                                if (!mediaStorageDir.exists()) {
                                    if (!mediaStorageDir.mkdirs()) {
                                        throw new IOException("Unable to create FM Conway folder within pictures");
                                    }
                                }
                                
                                String filename = d.getString("Filename");
                                filename = filename.replace("/", "").replace(":", "");
                                photo.imageLocation = mediaStorageDir.getPath() + File.separator + filename + ".jpg";
                                
                                String base64 = d.getString("DataString");
                                byte[] byteArray = Base64.decode(base64, Base64.DEFAULT);
                                photo.imageData = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                                
                                photo.time = FMConwayUtils.jsonDateToDate(d.getString("Time"));
                                photo.shiftID = d.getString("ShiftID");

                                defect.photos.add(photo);
                            }

                            vehicleChecklist.defects.add(defect);
                        }
                    }

                    if (shiftUpdate.shiftComplete || shiftUpdate.isCancelled || !shiftUpdate.isActive || !shiftUpdate.isOnShift) {
                        DataSync sync = new DataSync(shiftUpdate.shiftID, userID, url, encodedHeader);
                        sync.deleteShift();
                    } else {
                        shiftUpdate.save();
                    }

                    shiftUpdatesDownloaded++;
                    listener.onComplete();
                } catch (Exception e) {
                    shiftUpdatesErrors++;
                    listener.onError(e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("FMConway", "Download ShiftUpdates " + statusCode + " " + error.getMessage());
                shiftUpdatesErrors++;
                listener.onError(error);
            }
        });
    }

    public void DownloadNewShifts(ProgressDialog Dialog, final DataDownloadListener listener) {
        mDialog = Dialog;

        ShiftDownloadListener shiftListener = new ShiftDownloadListener() {
            @Override
            public void onComplete(final List<String> shiftIDs) {
                if (shiftIDs.size() == 0) {
                    listener.onComplete(false);
                    return;
                }
                
                final DataDownloadListener allPartsCompleteListener = new DataDownloadListener() {
                    @Override
                    public void onComplete(boolean errors) {
                        listener.onComplete(errors);
                    }

                    @Override
                    public void onError() {
                        // deleting shift due to error could potentially scrub a users saved data due to bad connection
                        //DeleteShifts(shiftIDs);
                        listener.onError();
                    }
                };

                DataDownloadProgressListener partsListener = new DataDownloadProgressListener() {
                    @Override
                    public void onComplete() {
                        DownloadNewShiftPart(allPartsCompleteListener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        DownloadNewShiftPart(allPartsCompleteListener);
                    }
                };

                DownloadJobPacks(userID, syncDate, partsListener);
                DownloadAreas(userID, syncDate, partsListener);
                DownloadVehicleChecklists(userID, syncDate, partsListener);
            }

            @Override
            public void onError(Throwable t, List<String> shiftIDs) {
                //DeleteShifts(shiftIDs);
                listener.onError();
            }
        };

        DownloadShifts(userID, syncDate, shiftListener);
    }

    private void DeleteShifts(List<String> shiftIDs) {
        for (String shiftID : shiftIDs) {
            DataSync sync = new DataSync(shiftID, userID, url, encodedHeader);
            sync.deleteShift();
        }
    }

    private void DownloadNewShiftPart(final DataDownloadListener listener) {
        if ((downloaded + errors) >= toDownloadNewShifts) {
            if (errors == 0) {
                listener.onComplete(false);
            } else {
                listener.onError();
            }
        }
    }

    private void DownloadCompleted() {
        mDialog.dismiss();

        //If shift for this user for today load staff intent else message box saying no shift available
        String shiftID = Shift.GetShiftForUser(userID);

        if (!Objects.equals(shiftID,"")) {
            Intent startIntent = new Intent(ctx, StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startIntent.putExtra("ShiftID", shiftID);
            startIntent.putExtra("UserID", userID);
            ctx.startActivity(startIntent);
        } else {

            ShiftVehicle.deleteAllShiftVehicles();
            VehicleChecklist.deleteAllVehicleChecklists();
            VehicleChecklistAnswer.deleteAllVehicleChecklistAnswers();
            VehicleChecklistAnswerDefect.deleteAllVehicleChecklistAnswerDefects();

            DownloadVehiclesOnShiftsToday(new DataDownloadProgressListener() {
                @Override
                public void onComplete() {
                    Intent noShiftIntent = new Intent(ctx, NoShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(noShiftIntent);
                }

                @Override
                public void onError(Throwable t) {

                }
            });

//            Intent noShiftIntent = new Intent(ctx, NoShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            ctx.startActivity(noShiftIntent);
        }
    }


    public interface DataDownloadListener {

        public void onComplete(boolean errors);
        public void onError();
    }

    public interface DataDownloadProgressListener {
        public void onComplete();
        public void onError(Throwable t);
    }

    public interface ShiftDownloadListener {
        public void onComplete(List<String> shiftIDs);
        public void onError(Throwable t, List<String> shiftIDs);
    }
}
