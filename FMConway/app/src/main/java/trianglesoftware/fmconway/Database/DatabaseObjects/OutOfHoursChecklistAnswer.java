package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;

public class OutOfHoursChecklistAnswer {
    public String oohChecklistAnswerID;
    public String oohChecklistID;
    public String checklistQuestionID;
    public int answer;
    public Date time;
    public String reason;
    public boolean isNew;
    public String question;

    public OutOfHoursChecklistAnswer(){
    }

    private String getOutOfHoursChecklistAnswerID() { return this.oohChecklistAnswerID; }
    private String getOutOfHoursChecklistID() { return this.oohChecklistID; }
    private String getChecklistQuestionID() { return this.checklistQuestionID; }
    private int getAnswer() { return this.answer; }
    private String getReason() { return this.reason; }
    public boolean getIsNew() { return this.isNew; }
    public String getQuestion() { return this.question; }

    public void setOutOfHoursChecklistAnswerID(String oohChecklistAnswerID) { this.oohChecklistAnswerID = oohChecklistAnswerID; }
    public void setOutOfHoursChecklistID(String oohChecklistID) { this.oohChecklistID = oohChecklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setAnswer(int answer) { this.answer = answer; }
    public void setReason(String reason) { this.reason = reason; }
    public void setNew(boolean isNew) { this.isNew = isNew; }
    private void setQuestion(String question) { this.question = question; }

    public JSONObject getJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("OOHChecklistAnswerID", this.oohChecklistAnswerID);
        obj.put("OOHChecklistID", this.oohChecklistID);
        obj.put("ChecklistQuestionID", this.checklistQuestionID);
        obj.put("Answer", this.answer);
        obj.put("Reason", this.reason);
        obj.put("IsNew", this.isNew);
        obj.put("Question", this.question);

        if (this.time != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            obj.put("Time", dateFormat.format(this.time));
        }

        return obj;
    }

    //OutOfHours Checklist Answers
    public static String addOOHChecklistAnswer(OutOfHoursChecklistAnswer oohChecklistAnswer)
    {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("OOHChecklistAnswerID", oohChecklistAnswer.oohChecklistAnswerID);
            values.put("OOHChecklistID", oohChecklistAnswer.oohChecklistID);
            values.put("ChecklistQuestionID", oohChecklistAnswer.checklistQuestionID);
            values.put("Reason", oohChecklistAnswer.reason);
            //values.put("Answer", oohChecklistAnswer.getAnswer());

            db.insertOrThrow("OutOfHoursChecklistAnswers", null, values);

            return oohChecklistAnswer.oohChecklistAnswerID;
        } catch (Exception e) {
            ErrorLog.CreateError(e, "addOOHChecklistAnswer");
            Log.e("addOOHChecklistAnswer", e.toString());
            return null;
        }
    }

    public static void deleteOOHChecklistAnswersForCheck(String oohChecklistID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswers WHERE OOHChecklistID = '" + oohChecklistID + "'");
    }

    public static void deleteOOHChecklistAnswersForUser()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswers");
    }

    public static List<OutOfHoursChecklistAnswer> GetOOHChecklistAnswers(String oohChecklistID) {
        List<OutOfHoursChecklistAnswer> oohChecklistAnswers = new LinkedList<>();

        String query = "SELECT vca.*, cq.Question FROM OutOfHoursChecklistAnswers vca ";
        query += "INNER JOIN ChecklistQuestions cq on cq.ChecklistQuestionID = vca.ChecklistQuestionID ";
        query += "WHERE OOHChecklistID = '" + oohChecklistID;
        query += "' ORDER BY cq.QuestionOrder";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        OutOfHoursChecklistAnswer tca;
        if(cursor.moveToFirst()){
            do{
                tca = new OutOfHoursChecklistAnswer();
                tca.oohChecklistAnswerID = cursor.getString(cursor.getColumnIndex("OOHChecklistAnswerID"));
                tca.oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));
                tca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                //tca.setAnswer(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0);
                tca.question = cursor.getString(cursor.getColumnIndex("Question"));
                tca.reason = cursor.getString(cursor.getColumnIndex("Reason"));

                int answerIndex = cursor.getColumnIndex("Answer");
                boolean a = cursor.isNull(answerIndex);

                if (!cursor.isNull(answerIndex)) {
                    tca.answer = Integer.parseInt(cursor.getString(answerIndex));
                }
                else
                {
                    tca.isNew = true;
                }

                int timeIndex = cursor.getColumnIndex("Time");
                if (!cursor.isNull(timeIndex)) {
                    String timeString = cursor.getString(timeIndex);
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    try {
                        tca.time = format.parse(timeString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                oohChecklistAnswers.add(tca);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return oohChecklistAnswers;
    }

    public static OutOfHoursChecklistAnswer GetOOHChecklistAnswer(String oohChecklistAnswerID) {
        OutOfHoursChecklistAnswer tca = null;

        String query = "" +
                "select " +
                "* " +
                "from " +
                "OutOfHoursChecklistAnswers as tca " +
                "where " +
                "tca.OOHChecklistAnswerID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { oohChecklistAnswerID });

        if (cursor.moveToFirst()) {
            tca = new OutOfHoursChecklistAnswer();
            tca.oohChecklistAnswerID = cursor.getString(cursor.getColumnIndex("OOHChecklistAnswerID"));
            tca.oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));
            tca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
            tca.answer = cursor.getInt(cursor.getColumnIndex("Answer"));
            tca.reason = cursor.getString(cursor.getColumnIndex("Reason"));
        }

        cursor.close();

        return tca;
    }

    public static void resetAllAnswers(String oohChecklistID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "" +
                "update " +
                "OutOfHoursChecklistAnswers " +
                "set " +
                "Answer = null " +
                "where " +
                "OOHChecklistID = ? ";

        db.execSQL(query, new String[] { oohChecklistID });
    }

    public static void SetAnswer(String checklistQuestionID, String oohChecklistID, int answer) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        ContentValues contentValues = new ContentValues();
        contentValues.put("Answer", answer);
        contentValues.put("Time", dateFormat.format(now));

        db.update("OutOfHoursChecklistAnswers", contentValues, "OOHChecklistID = ? and ChecklistQuestionID = ?", new String[] { oohChecklistID, checklistQuestionID });
    }

    public static Date GetAnswerTime(String checklistQuestionID, String oohChecklistID) {
        Date time = null;

        String query = "" +
                "select " +
                "Time " +
                "from " +
                "OutOfHoursChecklistAnswers as tca " +
                "where " +
                "tca.OOHChecklistID = ? and " +
                "tca.ChecklistQuestionID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { oohChecklistID, checklistQuestionID });

        if (cursor.moveToFirst()) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String timeString = cursor.getString(cursor.getColumnIndex("Time"));

                if (timeString != null) {
                    time = format.parse(timeString);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        cursor.close();

        return time;
    }

    public static void SetAnswer(String checklistQuestionID, String oohChecklistID, boolean answer) {
        SetAnswer(checklistQuestionID, oohChecklistID, answer ? 1 : 0);
    }

    public static void UpdateReason(String oohChecklistAnswerID, String reason) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("Reason", reason);

        db.update("OutOfHoursChecklistAnswers", cv, "OOHChecklistAnswerID = ?", new String[]{oohChecklistAnswerID});
    }
}
