package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class Depot {
    public String depotID;
    public String depotName;
    public boolean isActive;

    public Depot() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("DepotID", this.depotID);
            obj.put("DepotName", this.depotName);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "Depot - getJSONObject");
        }
        return obj;
    }

    protected static Depot cursorToModel(Cursor c) {
        Depot depot = new Depot();
        depot.depotID = c.getString(c.getColumnIndex("DepotID"));
        depot.depotName = c.getString(c.getColumnIndex("DepotName"));
        depot.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return depot;
    }

    public static Depot get(String depotID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Depot model = null;
        Cursor c = db.rawQuery("SELECT * FROM Depots WHERE DepotID = ?", new String[] { depotID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<Depot> getLookupItems() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<Depot> models = new LinkedList<>();

        Depot defaultItem = new Depot();
        defaultItem.depotName = "";
        models.add(defaultItem);

        Cursor c = db.rawQuery("SELECT * FROM Depots WHERE IsActive = 1 ORDER BY DepotName", null);

        if (c.moveToFirst()) {
            do {
                Depot model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(Depot model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("DepotID", model.depotID);
            cv.put("DepotName", model.depotName);
            cv.put("IsActive", model.isActive);

            if (get(model.depotID) != null) {
                db.update("Depots", cv, "DepotID = ?", new String[]{model.depotID});
            } else {
                db.insertOrThrow("Depots", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "Depot - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return depotName;
    }
}
