package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class BriefingType {
    public int briefingTypeID;
    public String name;

    public BriefingType(){
    }

    public BriefingType(int BriefingTypeID, String Name)
    {
        super();
        this.briefingTypeID = BriefingTypeID;
        this.name = Name;
    }

    private int getBriefingTypeID() { return this.briefingTypeID; }
    private String getName() { return this.name; }

    public void setBriefingTypeID(int briefingTypeID) { this.briefingTypeID = briefingTypeID; }
    public void setName(String name) { this.name = name; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("BriefingTypeID", this.briefingTypeID);
            obj.put("Name", this.name);
        } catch (JSONException e) { }

        return obj;
    }

    //Briefing Types
    public static void addBriefingType(BriefingType briefingType)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BriefingTypeID", briefingType.briefingTypeID);
        values.put("Name", briefingType.name);

        db.insert("BriefingTypes", null, values);
    }

    public static void deleteAllBriefingTypes()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingTypes");
    }
}
