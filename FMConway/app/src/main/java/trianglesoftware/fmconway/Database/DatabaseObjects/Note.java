package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 18/03/2016.
 */
public class Note {
    public String noteID;
    public String shiftID;
    public String userID;
    public String noteDetails;
    public Date noteDate;
    public Boolean isNew;

    public Note() {

    }

    private String getNoteID() {
        return this.noteID;
    }

    private String getShiftID() {
        return this.shiftID;
    }

    private String getUserID() { return this.userID;}
    public Boolean getIsNew() { return this.isNew;}

    public String getNoteDetails() {
        return this.noteDetails;
    }
    private String getNoteDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.noteDate != null){
            return dateFormat.format(this.noteDate);
        }
        else {
            return "";
        }
    }

    public void setNoteID(String noteID) {
        this.noteID = noteID;
    }

    public void setUserID(String userID) { this.userID = userID; }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }
    public void setIsNew(Boolean isNew) { this.isNew = isNew;}

    public void setNoteDetails(String noteDetails) {
        this.noteDetails = noteDetails;
    }
    public void setNoteDate(Date noteDate) {
        this.noteDate = noteDate;
    }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("NoteID", this.noteID);
            obj.put("ShiftID", this.shiftID);
            obj.put("NoteDetails", this.noteDetails);
            obj.put("NoteDate", getNoteDate());
            obj.put("IsNew", getIsNew());

        return obj;
    }

    //Notes
    public static void AddNote(Note note) throws Exception{

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();

        if(!note.isNew)
        {
            values.put("NoteID", note.noteID);
        }
        else
        {
            values.put("NoteID", UUID.randomUUID().toString());
        }
//        else
//        {
//            values.put("NoteID", 0);
//        }

        values.put("ShiftID", note.shiftID);
        values.put("NoteDetails", note.noteDetails);
        values.put("NoteDate", note.getNoteDate());
        values.put("UserID", note.userID);
        values.put("New", note.isNew);

        db.insert("Notes", null, values);
    }

    public static void deleteAllNotes() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Notes");
    }

    public static void deleteAllNotesForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Notes WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllNotesForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Notes WHERE ShiftID = '" + shiftID + "'");
    }

    public static List<Note> GetNotesForShift(String shiftID) {
        List<Note> comments = new LinkedList<>();

        String query = "SELECT * FROM Notes ";
        query += "WHERE ShiftID = '" + shiftID;
        query += "' ORDER BY NoteDate DESC";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Note n;
        if(cursor.moveToFirst()){
            do{
                n = new Note();
                n.noteID = cursor.getString(cursor.getColumnIndex("NoteID"));
                n.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                n.noteDetails = cursor.getString(cursor.getColumnIndex("NoteDetails"));
                try {
                    String noteDate = cursor.getString(cursor.getColumnIndex("NoteDate"));
                    if(noteDate != null)
                        n.setNoteDate(format.parse(noteDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                comments.add(n);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return comments;
    }

    public static Note GetNote(String commentID) {
        String query = "SELECT * FROM Notes ";
        query += "WHERE NoteID = '" + commentID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Note n = null;
        if(cursor.moveToFirst()){
            do{
                n = new Note();
                n.noteID = cursor.getString(cursor.getColumnIndex("NoteID"));
                n.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                n.noteDetails = cursor.getString(cursor.getColumnIndex("NoteDetails"));
                try {
                    String noteDate = cursor.getString(cursor.getColumnIndex("NoteDate"));
                    if(noteDate != null)
                        n.setNoteDate(format.parse(noteDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }while(cursor.moveToNext());
        }

        cursor.close();

        return n;
    }

    public static void UpdateNote(String commentID, String comment) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE Notes SET NoteDetails = '" + comment + "' WHERE NoteID = " + commentID);
        //db.execSQL("UPDATE Notes SET NoteDetails = ? WHERE NoteID = " + commentID, new String[]{comment});

        SQLiteStatement statement = db.compileStatement("UPDATE Notes SET NoteDetails = ? WHERE NoteID = ?");
        statement.bindString(1,comment);
        statement.bindString(2,commentID);
        statement.execute();
        statement.close();
    }

    public static JSONArray GetNotesToSend(String shiftID) throws Exception {
        JSONArray notes = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT n.* FROM Notes n ";
        query += "WHERE n.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        JSONObject nObject;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Note n = new Note();
                    n.noteID = cursor.getString(cursor.getColumnIndex("NoteID"));
                    n.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    n.noteDetails = cursor.getString(cursor.getColumnIndex("NoteDetails"));
                    n.isNew = Integer.parseInt(cursor.getString(cursor.getColumnIndex("New"))) > 0;
                    try {
                        String noteDate = cursor.getString(cursor.getColumnIndex("NoteDate"));
                        if (noteDate != null)
                            n.setNoteDate(format.parse(noteDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    nObject = n.getJSONObject();

                    notes.put(nObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetNotesToSend");
            throw e;
        }
        finally
        {
            cursor.close();
        }

        return notes;
    }
}
