package trianglesoftware.fmconway.Database.DatabaseObjects;

/**
 * Created by Adam.Patrick on 08/05/2017.
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

public class TaskEquipment {
    public String TaskEquipmentID;
    public String ShiftID;
    public String TaskID;
    public String EquipmentID;
    public int Quantity;
    public String UserID;
    public boolean Updated;

    public String VmsOrAsset;
    public int EmptyAssets;
    public int TotalAssets;

    public String Name;
    public String EquipmentType;

    public TaskEquipment()
    {}

    public TaskEquipment(String TaskEquipmentID, String ShiftID, String TaskID, String EquipmentID, int Quantity, String UserID, boolean Updated)
    {
        super();
        this.TaskEquipmentID = TaskEquipmentID;
        this.ShiftID = ShiftID;
        this.TaskID = TaskID;
        this.EquipmentID = EquipmentID;
        this.Quantity = Quantity;
        this.UserID = UserID;
        this.Updated = Updated;
    }

    public JSONObject getJSONObject() throws JSONException
    {
        JSONObject obj = new JSONObject();
        try
        {
            obj.put("TaskEquipmentID", this.TaskEquipmentID);
            obj.put("EquipmentID", this.EquipmentID);
            obj.put("TaskID", this.TaskID);
            obj.put("ShiftID", this.ShiftID);
            obj.put("Quantity", this.Quantity);
            obj.put("Name", this.Name);
            obj.put("EquipmentType", this.EquipmentType);
            obj.put("VmsOrAsset", this.VmsOrAsset);
            obj.put("EmptyAssets", this.EmptyAssets);
            obj.put("TotalAssets", this.TotalAssets);
        }
        catch (JSONException e)
        {
            throw e;
        }

        return obj;
    }

    public static void addTaskEquipment(TaskEquipment equipment)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("EquipmentID", equipment.EquipmentID);
        values.put("TaskEquipmentID", equipment.TaskEquipmentID);
        values.put("ShiftID", equipment.ShiftID);
        values.put("TaskID", equipment.TaskID);
        values.put("Quantity", equipment.Quantity);
        values.put("UserID", equipment.UserID);
        values.put("Updated", equipment.Updated);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(equipment.TaskEquipmentID)) {
            db.insert("TaskEquipment", null, values);
        }
    }

    public static boolean exists(String taskEquipmentID) {
        String query = "select * from TaskEquipment where TaskEquipmentID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskEquipmentID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllTaskEquipment()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        
        db.execSQL("DELETE FROM TaskEquipmentAssets");
        db.execSQL("DELETE FROM TaskEquipment");
    }

    public static void deleteAllTaskEquipment(String taskEquipmentID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskEquipmentAssets WHERE TaskEquipmentID = '" + taskEquipmentID + "'");
        db.execSQL("DELETE FROM TaskEquipment WHERE TaskEquipmentID = '" + taskEquipmentID + "'");
    }

    public static void deleteAllTaskEquipmentForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskEquipmentAssets WHERE ShiftID = '" + shiftID + "'");
        db.execSQL("DELETE FROM TaskEquipment WHERE ShiftID = '" + shiftID + "'");
    }

    public static List<TaskEquipment> GetTaskEquipmentForTask(String taskID)
    {
        List<TaskEquipment> equipment = new LinkedList<>();

        String query = "SELECT e.Name, e.EquipmentID, et.Name AS EquipmentType, te.Quantity, te.TaskEquipmentID, te.ShiftID, te.TaskID, et.VmsOrAsset FROM Equipment e ";
        query += "INNER JOIN EquipmentTypes et on et.EquipmentTypeID = e.EquipmentTypeID ";
        query += "INNER JOIN TaskEquipment te on te.EquipmentID = e.EquipmentID ";
        query += "where te.TaskID = '" + taskID + "' ";
        query += " ORDER BY e.Name";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        TaskEquipment e;
        if(cursor.moveToFirst()){
            do {
                e = new TaskEquipment();
                e.Name = cursor.getString(cursor.getColumnIndex("Name"));
                e.EquipmentType = cursor.getString(cursor.getColumnIndex("EquipmentType"));
                e.Quantity = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Quantity")));
                e.TaskEquipmentID = cursor.getString(cursor.getColumnIndex("TaskEquipmentID"));
                e.EquipmentID = cursor.getString(cursor.getColumnIndex("EquipmentID"));
                e.TaskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                e.ShiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                e.VmsOrAsset = cursor.getString(cursor.getColumnIndex("VmsOrAsset"));
                e.EmptyAssets = emptyTaskEquipmentAssets(e.TaskEquipmentID);
                e.TotalAssets = totalTaskEquipmentAssets(e.TaskEquipmentID);

                equipment.add(e);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return equipment;
    }

    public static int GetTaskEquipmentQuantity(String taskEquipmentID)
    {
        String query = "SELECT * FROM TaskEquipment WHERE TaskEquipmentID = '" + taskEquipmentID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int quantity = 0;
        if(cursor.moveToFirst()){
            do {
                quantity = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Quantity")));

            }while(cursor.moveToNext());
        }

        cursor.close();

        return quantity;
    }

    public static void StoreTaskDetails(String taskEquipmentID, int quantity) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        if (quantity  < 1) {
            db.execSQL("DELETE FROM TaskEquipment WHERE TaskEquipmentID = '" + taskEquipmentID + "'");
        }
        else{
            String quant = String.valueOf(quantity);

            SQLiteStatement statement = db.compileStatement("UPDATE TaskEquipment SET Quantity = ? , Updated = 1 WHERE TaskEquipmentID = ?");
            statement.bindString(1, quant);
            statement.bindString(2, taskEquipmentID);
            statement.execute();
            statement.close();

        }
    }

    public static JSONArray GetTaskEquipmentToSend(String shiftID) throws Exception{
        JSONArray tasks = new JSONArray();

        SQLiteDatabase db = null;
        Cursor tCursor = null;
        Cursor teCursor = null;
        Cursor teaCursor = null;

        String taskQuery = "" +
            "select " +
            "TaskID " +
            "from " +
            "Tasks as t " +
            "inner join Activities act on act.ActivityID = t.ActivityID " +
            "inner join JobPacks jp on jp.JobPackID = act.JobPackID " +
            "where " +
            "jp.ShiftID = ? and " +
            "(t.TaskTypeID = 8 or t.TaskTypeID = 9)";

        String taskEquipmentQuery = "" +
            "select " +
            "te.* " +
            "from " +
            "TaskEquipment as te " +
            "where " +
            "te.TaskID = ?";

        String taskEquipmentAssetsQuery = "" +
            "select " +
            "tea.* " +
            "from " +
            "TaskEquipmentAssets as tea " +
            "where " +
            "tea.TaskEquipmentID = ?";

        try {
            db = Database.MainDB.getWritableDatabase();

            tCursor = db.rawQuery(taskQuery, new String[] { shiftID });
            if (tCursor.moveToFirst()) {
                do {
                    String taskID = tCursor.getString(tCursor.getColumnIndex("TaskID"));
                    
                    JSONObject task = new JSONObject();
                    task.put("TaskID", taskID);
                    
                    // task equipments
                    JSONArray taskEquipments = new JSONArray();
                    teCursor = db.rawQuery(taskEquipmentQuery, new String[] { taskID });
                    if (teCursor.moveToFirst()) {
                        do {
                            String taskEquipmentID = teCursor.getString(teCursor.getColumnIndex("TaskEquipmentID"));

                            JSONObject taskEquipment = new JSONObject();
                            taskEquipment.put("TaskEquipmentID", taskEquipmentID);
                            taskEquipment.put("EquipmentID", teCursor.getString(teCursor.getColumnIndex("EquipmentID")));
                            taskEquipment.put("Quantity", Integer.parseInt(teCursor.getString(teCursor.getColumnIndex("Quantity"))));
                            taskEquipment.put("TaskID", teCursor.getString(teCursor.getColumnIndex("TaskID")));
                            taskEquipment.put("ShiftID", teCursor.getString(teCursor.getColumnIndex("ShiftID")));

                            // task equipment assets
                            JSONArray taskEquipmentAssets = new JSONArray();
                            teaCursor = db.rawQuery(taskEquipmentAssetsQuery, new String[] { taskEquipmentID });
                            if (teaCursor.moveToFirst()) {
                                do {
                                    JSONObject taskEquipmentAsset = new JSONObject();
                                    taskEquipmentAsset.put("TaskEquipmentAssetID", teaCursor.getString(teaCursor.getColumnIndex("TaskEquipmentAssetID")));
                                    taskEquipmentAsset.put("TaskEquipmentID", taskEquipmentID);
                                    taskEquipmentAsset.put("ItemNumber", teaCursor.getInt(teaCursor.getColumnIndex("ItemNumber")));
                                    taskEquipmentAsset.put("AssetNumber", teaCursor.getString(teaCursor.getColumnIndex("AssetNumber")));
                                    taskEquipmentAssets.put(taskEquipmentAsset);
                                } while (teaCursor.moveToNext());
                            }
                            taskEquipment.put("TaskEquipmentAssets", taskEquipmentAssets);

                            taskEquipments.put(taskEquipment);
                        } while (teCursor.moveToNext());
                    }
                    task.put("TaskEquipments", taskEquipments);

                    tasks.put(task);
                } while (tCursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetTaskEquipmentToSend");
            throw e;
        }
        finally {
            if (teaCursor != null) {
                teaCursor.close();
            }
            if (teCursor != null) {
                teCursor.close();
            }
            if (tCursor != null) {
                tCursor.close();
            }
        }

        return tasks;
    }
    
    public static int emptyTaskEquipmentAssets(String taskEquipmentID) {
        String query = "" +
            "select " +
            "count(*) as EmptyAssets " +
            "from " +
            "TaskEquipmentAssets as tea " +
            "where " +
            "tea.TaskEquipmentID = ? and " +
            "tea.AssetNumber is null or AssetNumber = ''";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskEquipmentID });

        int emptyAssets = 0;
        if(cursor.moveToFirst()){
            do {
                emptyAssets = cursor.getInt(cursor.getColumnIndex("EmptyAssets"));
            } while(cursor.moveToNext());
        }

        cursor.close();

        return emptyAssets;
    }

    public static int totalTaskEquipmentAssets(String taskEquipmentID) {
        String query = "" +
            "select " +
            "count(*) as Total " +
            "from " +
            "TaskEquipmentAssets as tea " +
            "where " +
            "tea.TaskEquipmentID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskEquipmentID });

        int count = 0;
        if(cursor.moveToFirst()){
            do {
                count = cursor.getInt(cursor.getColumnIndex("Total"));
            } while(cursor.moveToNext());
        }

        cursor.close();

        return count;
    }
    
    public static void copyTaskEquipmentAndAssets(String installTaskID, String collectionTaskID) {
        String taskEquipmentQuery = "" +
            "select " +
            "* " +
            "from " +
            "TaskEquipment as te " +
            "where " +
            "te.TaskID = ?";

        String taskEquipmentAssetsQuery = "" +
            "select " +
            "* " +
            "from " +
            "TaskEquipmentAssets as tea " +
            "where " +
            "tea.TaskEquipmentID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        
        Cursor teCursor = db.rawQuery(taskEquipmentQuery, new String[] { installTaskID });
        if(teCursor.moveToFirst()){
            do {
                String taskEquipmentID = UUID.randomUUID().toString();
                String installTaskEquipmentID = teCursor.getString(teCursor.getColumnIndex("TaskEquipmentID"));
                
                ContentValues teValues = new ContentValues();
                teValues.put("TaskEquipmentID", taskEquipmentID);
                teValues.put("ShiftID", teCursor.getString(teCursor.getColumnIndex("ShiftID")));
                teValues.put("TaskID", collectionTaskID);
                teValues.put("EquipmentID", teCursor.getString(teCursor.getColumnIndex("EquipmentID")));
                teValues.put("Quantity", teCursor.getInt(teCursor.getColumnIndex("Quantity")));
                teValues.put("UserID", teCursor.getString(teCursor.getColumnIndex("UserID")));
                teValues.put("Updated", teCursor.getInt(teCursor.getColumnIndex("Updated")));
                db.insert("TaskEquipment", null, teValues);

                Cursor teaCursor = db.rawQuery(taskEquipmentAssetsQuery, new String[] { installTaskEquipmentID });
                if (teaCursor.moveToFirst()) {
                    do {
                        ContentValues teaValues = new ContentValues();
                        teaValues.put("TaskEquipmentAssetID", UUID.randomUUID().toString());
                        teaValues.put("TaskEquipmentID", taskEquipmentID);
                        teaValues.put("ItemNumber", teaCursor.getInt(teaCursor.getColumnIndex("ItemNumber")));
                        teaValues.put("AssetNumber", teaCursor.getString(teaCursor.getColumnIndex("AssetNumber")));
                        teaValues.put("ShiftID", teaCursor.getString(teaCursor.getColumnIndex("ShiftID")));
                        db.insert("TaskEquipmentAssets", null, teaValues);
                    } while(teaCursor.moveToNext());
                }
                teaCursor.close();
                
            } while(teCursor.moveToNext());
        }
        teCursor.close();
    }
}
