package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class ActivityRiskAssessment {
    public String activityRiskAssessmentID;
    public String taskID;
    public String roadName;
    public String roadSpeedID;
    public String tmRequirementID;
    public boolean schools;
    public boolean pedestrianCrossings;
    public boolean overheadCables;
    public boolean treeCanopies;
    public boolean waterCourses;
    public boolean adverseWeather;
    public String notes;
    public boolean isActive;
    public Date createdDate;
    public String cleansingLogID;

    public ActivityRiskAssessment() {

    }


    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ActivityRiskAssessmentID", this.activityRiskAssessmentID);
            obj.put("TaskID", this.taskID);
            obj.put("CleansingLogID", this.cleansingLogID);
            obj.put("RoadName", this.roadName);
            obj.put("RoadSpeedID", this.roadSpeedID);
            obj.put("TMRequirementID", this.tmRequirementID);
            obj.put("Schools", this.schools);
            obj.put("PedestrianCrossings", this.pedestrianCrossings);
            obj.put("OverheadCables", this.overheadCables);
            obj.put("TreeCanopies", this.treeCanopies);
            obj.put("WaterCourses", this.waterCourses);
            obj.put("AdverseWeather", this.adverseWeather);
            obj.put("Notes", this.notes);
            obj.put("IsActive", this.isActive);
            obj.put("CreatedDate", FMConwayUtils.getDateIsoString(this.createdDate));
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "ActivityRiskAssessment - getJSONObject");
        }
        return obj;
    }

    protected static ActivityRiskAssessment cursorToModel(Cursor c) {
        ActivityRiskAssessment activityRiskAssessment = new ActivityRiskAssessment();
        activityRiskAssessment.activityRiskAssessmentID = c.getString(c.getColumnIndex("ActivityRiskAssessmentID"));
        activityRiskAssessment.taskID = c.getString(c.getColumnIndex("TaskID"));
        activityRiskAssessment.cleansingLogID = c.getString(c.getColumnIndex("CleansingLogID"));
        activityRiskAssessment.roadName = c.getString(c.getColumnIndex("RoadName"));
        activityRiskAssessment.roadSpeedID = c.getString(c.getColumnIndex("RoadSpeedID"));
        activityRiskAssessment.tmRequirementID = c.getString(c.getColumnIndex("TMRequirementID"));
        activityRiskAssessment.schools = c.getInt(c.getColumnIndex("Schools")) > 0;
        activityRiskAssessment.pedestrianCrossings = c.getInt(c.getColumnIndex("PedestrianCrossings")) > 0;
        activityRiskAssessment.overheadCables = c.getInt(c.getColumnIndex("OverheadCables")) > 0;
        activityRiskAssessment.treeCanopies = c.getInt(c.getColumnIndex("TreeCanopies")) > 0;
        activityRiskAssessment.waterCourses = c.getInt(c.getColumnIndex("WaterCourses")) > 0;
        activityRiskAssessment.adverseWeather = c.getInt(c.getColumnIndex("AdverseWeather")) > 0;
        activityRiskAssessment.notes = c.getString(c.getColumnIndex("Notes"));
        activityRiskAssessment.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;
        activityRiskAssessment.createdDate = FMConwayUtils.getCursorDate(c, "CreatedDate");

        return activityRiskAssessment;
    }

    public static ActivityRiskAssessment get(String activityRiskAssessmentID) {
        SQLiteDatabase db = Database.MainDB.getReadableDatabase();

        ActivityRiskAssessment model = null;
        Cursor c = db.rawQuery("SELECT * FROM ActivityRiskAssessments WHERE ActivityRiskAssessmentID = ?", new String[]{activityRiskAssessmentID});

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<ActivityRiskAssessment> getAllForTask(String taskID) {
        SQLiteDatabase db = Database.MainDB.getReadableDatabase();

        List<ActivityRiskAssessment> models = new LinkedList<>();
        Cursor c = db.rawQuery("SELECT * FROM ActivityRiskAssessments WHERE TaskID = ? AND IsActive = 1", new String[]{taskID});

        if (c.moveToFirst()) {
            do {
                ActivityRiskAssessment model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(ActivityRiskAssessment model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("ActivityRiskAssessmentID", model.activityRiskAssessmentID);
            cv.put("TaskID", model.taskID);
            cv.put("CleansingLogID", model.cleansingLogID);
            cv.put("RoadName", model.roadName);
            cv.put("RoadSpeedID", model.roadSpeedID);
            cv.put("TMRequirementID", model.tmRequirementID);
            cv.put("Schools", model.schools);
            cv.put("PedestrianCrossings", model.pedestrianCrossings);
            cv.put("OverheadCables", model.overheadCables);
            cv.put("TreeCanopies", model.treeCanopies);
            cv.put("WaterCourses", model.waterCourses);
            cv.put("AdverseWeather", model.adverseWeather);
            cv.put("Notes", model.notes);
            cv.put("IsActive", model.isActive);
            cv.put("CreatedDate", FMConwayUtils.getDateIsoString(model.createdDate));

            if (get(model.activityRiskAssessmentID) != null) {
                db.update("ActivityRiskAssessments", cv, "ActivityRiskAssessmentID = ?", new String[]{model.activityRiskAssessmentID});
            } else {
                db.insertOrThrow("ActivityRiskAssessments", null, cv);
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "ActivityRiskAssessment - save");
        }
    }

    public static void updateCleansingLogID(String activityRiskAssessmentID, String cleansingLogID) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            if (activityRiskAssessmentID != null) {
                db.execSQL("UPDATE ActivityRiskAssessments SET CleansingLogID = ? WHERE ActivityRiskAssessmentID = ?", new String[]{cleansingLogID, activityRiskAssessmentID});
            } else {
                db.execSQL("UPDATE ActivityRiskAssessments SET CleansingLogID = NULL WHERE CleansingLogID = ?", new String[]{cleansingLogID});
            }

        } catch (Exception e) {
            ErrorLog.CreateError(e, "ActivityRiskAssessment - updateCleansingLogID");
        }
    }

    public static ActivityRiskAssessment getActivityRiskAssessmentFromCleansingLogID(String cleansingLogID) {
        SQLiteDatabase db = Database.MainDB.getReadableDatabase();

        ActivityRiskAssessment model = new ActivityRiskAssessment();
        Cursor c = db.rawQuery("SELECT * FROM ActivityRiskAssessments WHERE CleansingLogID = ? AND IsActive = 1 LIMIT 1", new String[]{cleansingLogID});

        if (c.moveToFirst()) {
            do {
                model = cursorToModel(c);
            } while (c.moveToNext());
        }

        c.close();
        return model;
    }

    public static List<ActivityRiskAssessment> getLookupItems(String includeActivityRiskAssessmentID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<ActivityRiskAssessment> models = new LinkedList<>();

        ActivityRiskAssessment defaultItem = new ActivityRiskAssessment();
        defaultItem.roadName = "";
        models.add(defaultItem);

        Cursor c;

        if (includeActivityRiskAssessmentID != null) {
            c = db.rawQuery("SELECT * FROM ActivityRiskAssessments WHERE (IsActive = 1 AND CleansingLogID IS NULL) OR CleansingLogID = ? ORDER BY RoadName", new String[]{includeActivityRiskAssessmentID});
        } else {
            c = db.rawQuery("SELECT * FROM ActivityRiskAssessments WHERE IsActive = 1 AND CleansingLogID IS NULL ORDER BY RoadName", null);
        }

        if (c.moveToFirst()) {
            do {
                ActivityRiskAssessment model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    // required for spinner text
    @Override
    public String toString() {
        return roadName;
    }

    public static JSONArray getActivityRiskAssessmentsToSend(String shiftID) throws Exception {
        JSONArray jsonArray = new JSONArray();

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            String query = "SELECT ar.* FROM ActivityRiskAssessments ar ";
            query += "JOIN Tasks t on t.TaskID = ar.TaskID ";
            query += "JOIN Activities a on t.ActivityID = a.ActivityID ";
            query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
            query += "WHERE jp.ShiftID = '" + shiftID + "' AND ar.IsActive = 1";

            db = Database.MainDB.getReadableDatabase();
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {

                    ActivityRiskAssessment activityRiskAssessment = cursorToModel(cursor);
                    JSONObject jsonObject = activityRiskAssessment.getJSONObject();
                    jsonArray.put(jsonObject);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "getActivityRiskAssessmentsToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return jsonArray;
    }

    public static void delete(String activityRiskAssessmentID) {

    }
}

