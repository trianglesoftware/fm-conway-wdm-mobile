package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayApp;

public class ShiftProgressStarted extends ShiftProgress {
    public Date shiftUpdatedDate;
}
