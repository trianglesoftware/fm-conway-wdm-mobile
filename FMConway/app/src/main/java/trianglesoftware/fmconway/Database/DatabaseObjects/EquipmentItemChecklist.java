package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class EquipmentItemChecklist {
    public String EquipmentItemChecklistID;
    public String LoadingSheetEquipmentItemID;
    public String ChecklistID;
    public List<EquipmentItemChecklistAnswer> Answers;

    public EquipmentItemChecklist() {
        
    }

    public void insert() {
        ContentValues values = new ContentValues();
        values.put("EquipmentItemChecklistID", EquipmentItemChecklistID);
        values.put("LoadingSheetEquipmentItemID", LoadingSheetEquipmentItemID);
        values.put("ChecklistID", ChecklistID);

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.insertOrThrow("EquipmentItemChecklists", null, values);
    }

    public static EquipmentItemChecklist get(String loadingSheetEquipmentItemID) {
        EquipmentItemChecklist model = new EquipmentItemChecklist();

        String query = "";
        query += "select ";
        query += "eic.EquipmentItemChecklistID, ";
        query += "eic.LoadingSheetEquipmentItemID, ";
        query += "eic.ChecklistID ";
        query += "from ";
        query += "EquipmentItemChecklists as eic ";
        query += "where ";
        query += "eic.LoadingSheetEquipmentItemID = ? ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { loadingSheetEquipmentItemID });

        if (cursor.moveToFirst()) {
            do {
                model.EquipmentItemChecklistID = cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistID"));
                model.LoadingSheetEquipmentItemID = cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentItemID"));
                model.ChecklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return model;
    }
}
