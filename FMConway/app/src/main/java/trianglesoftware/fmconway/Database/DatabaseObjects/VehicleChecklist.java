package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class VehicleChecklist {
    public String vehicleChecklistID;
    public String vehicleID;
    public String shiftID;
    public String checklistID;
    public String name;
    public boolean startOfShift;
    public boolean adhoc;
    public String userID;
    public String imageLocation;
    public String imageData;
    public String imageFilename;
    public List<VehicleChecklistAnswer> answers;
    public List<VehicleChecklistAnswerDefect> defects;

    public VehicleChecklist(){
    }

    public VehicleChecklist(String VehicleID, String ChecklistID, String ShiftID)
    {
        super();
        this.vehicleID = VehicleID;
        this.checklistID = ChecklistID;
        this.shiftID = ShiftID;
    }

    public String getVehicleChecklistID() { return this.vehicleChecklistID; }
    public String getVehicleID() { return this.vehicleID; }
    public String getChecklistID() { return this.checklistID; }
    public String getShiftID() { return this.shiftID; }
    public String getName() { return this.name; }
    public boolean getStartOfShift() { return this.startOfShift; }
    public boolean getAdHoc() { return this.adhoc;}
    public String getUserID() { return this.userID;}

    private void setVehicleChecklistID(String vehicleChecklistID) { this.vehicleChecklistID = vehicleChecklistID; }
    public void setVehicleID(String vehicleID) { this.vehicleID = vehicleID; }
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }
    public void setShiftID(String shiftID) { this.shiftID = shiftID; }
    private void setName(String name) { this.name = name; }
    public void setStartOfShift(boolean startOfShift) { this.startOfShift = startOfShift; }
    public void setAdhoc(boolean adhoc) { this.adhoc = adhoc; }
    public void setUserID(String userID) { this.userID = userID;}

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();
        obj.put("VehicleChecklistID", this.vehicleChecklistID);
        obj.put("VehicleID", this.vehicleID);
        obj.put("ChecklistID", this.checklistID);
        obj.put("ShiftID", this.shiftID);
        obj.put("Name", this.name);
        obj.put("StartOfShift", this.startOfShift);
        obj.put("ImageData", this.imageData);
        obj.put("ImageFilename", this.imageFilename);

        return obj;
    }

    //Vehicle Checklists
    public static void addVehicleChecklist(VehicleChecklist vehicleChecklist)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleChecklistID", vehicleChecklist.vehicleChecklistID);
        values.put("VehicleID", vehicleChecklist.vehicleID);
        values.put("ChecklistID", vehicleChecklist.checklistID);
        values.put("ShiftID", vehicleChecklist.shiftID);
        values.put("StartOfShift", vehicleChecklist.startOfShift);
        values.put("AdHoc", vehicleChecklist.adhoc);
        values.put("UserID", vehicleChecklist.userID);
        
        if (!TextUtils.isEmpty(vehicleChecklist.imageData) && !TextUtils.isEmpty(vehicleChecklist.imageFilename)) {
            byte[] byteArray = Base64.decode(vehicleChecklist.imageData, Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            String filepath = FMConwayUtils.getPictureFile(vehicleChecklist.imageFilename).getPath();
            FMConwayUtils.saveBitmap(bmp, filepath, 100);
            values.put("ImageLocation", filepath);
        }

        db.insert("VehicleChecklists", null, values);
    }

    public static void deleteAllVehicleChecklists()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklists");
    }

    public static void deleteAllVehicleChecklistsForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswerDefects WHERE UserID = '" + userID + "'");

        db.execSQL("DELETE FROM VehicleChecklistAnswers WHERE UserID = '" + userID + "'");

        db.execSQL("DELETE FROM VehicleChecklists WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllVehicleChecklistsForShift (String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT VehicleChecklistID FROM VehicleChecklists WHERE ShiftID = '" + shiftID + "'";

        Cursor cursor = db.rawQuery(query, null);

        List<String> ids = new LinkedList<>();
        if(cursor.moveToFirst()){
            do{
                String id = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                ids.add(id);
            }while(cursor.moveToNext());
        }
        cursor.close();

        for (int i = 0; i < ids.size(); i++)
        {
            db.execSQL("DELETE FROM VehicleChecklistAnswers WHERE VehicleChecklistID = '" + ids.get(i) + "'");
            db.execSQL("DELETE FROM VehicleChecklistAnswerDefects WHERE VehicleChecklistID = '" + ids.get(i) + "'");
        }

        db.execSQL("DELETE FROM VehicleChecklists WHERE ShiftID = '" + shiftID + "'");
    }

    public static void deleteVehicleChecklist(String vehicleChecklistID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklists WHERE VehicleChecklistID = '" + vehicleChecklistID + "'");
    }

    public static List<VehicleChecklist> GetVehicleChecklists(String shiftID, String vehicleID, boolean startOfShift) {
        List<VehicleChecklist> vehicleChecklists = new LinkedList<>();

        String query = "SELECT c.Name, vc.* FROM VehicleChecklists vc ";
        query += "INNER JOIN Checklists c on c.ChecklistID = vc.ChecklistID ";
        query += "WHERE VehicleID = '" + vehicleID + "' AND ShiftID = '" + shiftID + "' AND StartOfShift = " + (startOfShift ? 1 : 0);

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        VehicleChecklist vc;
        if(cursor.moveToFirst()){
            do{
                vc = new VehicleChecklist();
                vc.vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                vc.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                vc.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                vc.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                vc.name = cursor.getString(cursor.getColumnIndex("Name"));
                vc.startOfShift = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StartOfShift"))) > 0;

                vehicleChecklists.add(vc);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return vehicleChecklists;
    }

    public static String GetVehicleChecklistID(String vehicleID, String shiftID, String checklistID) {
        String query = "SELECT VehicleChecklistID FROM VehicleChecklists where ShiftID = '" + shiftID + "' AND VehicleID = '"+vehicleID+ "' AND ChecklistID = '" +checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String ret = "";

        if(cursor.moveToFirst()){
            do{
                ret = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static String GetVehicleChecklistID(String vehicleID, String shiftID, String checklistID, boolean startOfShift) {
        String query = "SELECT VehicleChecklistID FROM VehicleChecklists where ShiftID = '" + shiftID + "' AND VehicleID = '"+vehicleID+ "' AND ChecklistID = '" +checklistID + "' and StartOfShift = " + (startOfShift ? 1 : 0);

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String ret = "";

        if(cursor.moveToFirst()){
            do{
                ret = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static String GetVehicleChecklistID(String vehicleID, String shiftID, boolean startOfShift) {
        String query = "" +
            "select " +
            "VehicleChecklistID " +
            "from " +
            "VehicleChecklists " +
            "where " +
            "ShiftID = ? and " +
            "VehicleID = ? and " +
            "StartOfShift = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, vehicleID, startOfShift ? "1" : "0" });

        String vehicleChecklistID = null;
        if (cursor.moveToFirst()) {
            vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
        }

        cursor.close();

        return vehicleChecklistID;
    }

    public static boolean CheckIfChecklistCreated(String vehicleID, String shiftID, String checklistID, boolean startOfShift) {
        String query = "SELECT VehicleChecklistID FROM VehicleChecklists where StartOfShift = "+ (startOfShift ? 1 : 0) +" AND ShiftID = '" + shiftID + "' AND VehicleID = '"+vehicleID+ "' AND ChecklistID = '" +checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String vehicleChecklistID = "";

        if(cursor.moveToFirst()){
            do{
                vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return !Objects.equals(vehicleChecklistID,"");
    }

    public static JSONArray GetVehicleChecklistsToSend(String shiftID, String userID) throws  Exception{
        JSONArray vehicleChecklists = new JSONArray();

        String query = "SELECT vc.* FROM VehicleChecklists vc ";
        query += "WHERE vc.ShiftID = '" + shiftID + "'";

        if (Objects.equals(shiftID, ""))
        {
            query = "SELECT vc.* FROM VehicleChecklists vc WHERE vc.AdHoc = 1 AND UserID = '" + userID + "'";
        }

        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor cursorAnswers = null;
        Cursor cursorDefects = null;

        JSONObject vcObject = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    VehicleChecklist vc = new VehicleChecklist();
                    vc.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                    vc.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                    vc.vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                    vc.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    vc.startOfShift = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StartOfShift"))) > 0;
                    vc.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    
                    if (!TextUtils.isEmpty(vc.imageLocation)) {
                        File file = new File(vc.imageLocation);
                        if (file.exists()) {
                            vc.imageFilename = file.getName();
                            vc.imageData = FMConwayUtils.fileToBase64(file);
                        }
                    }

                    vcObject = vc.getJSONObject();

                    try {
                        //Add Answers List & Defects List
                        query = "SELECT vca.* FROM VehicleChecklistAnswers vca ";
                        query += "WHERE vca.VehicleChecklistID = '" + cursor.getString(cursor.getColumnIndex("VehicleChecklistID")) + "'";

                        cursorAnswers = db.rawQuery(query, null);
                        JSONArray vcaObjects = new JSONArray();
                        if (cursorAnswers.moveToFirst()) {
                            do {
                                VehicleChecklistAnswer vca = new VehicleChecklistAnswer();
                                vca.vehicleChecklistID = cursorAnswers.getString(cursorAnswers.getColumnIndex("VehicleChecklistID"));
                                vca.checklistQuestionID = cursorAnswers.getString(cursorAnswers.getColumnIndex("ChecklistQuestionID"));

                                int columnIndex = cursorAnswers.getColumnIndex("Answer");
                                if (!cursorAnswers.isNull(columnIndex)) {
                                    vca.answer = Integer.parseInt(cursorAnswers.getString(cursorAnswers.getColumnIndex("Answer")));
                                }

                                vcaObjects.put(vca.getJSONObjectToSend());

                            } while (cursorAnswers.moveToNext());
                        }
                        try {
                            vcObject.put("Answers", vcaObjects);
                        } catch (JSONException e) {
                            throw e;
                        }

                    }
                    catch (Exception e)
                    {
                        ErrorLog.CreateError(e, "VehicleChecklistAnswers");
                        throw e;
                    }

                    try {
                        query = "SELECT vcd.* FROM VehicleChecklistAnswerDefects vcd ";
                        query += "WHERE vcd.VehicleChecklistID = '" + cursor.getString(cursor.getColumnIndex("VehicleChecklistID")) + "'";
                        cursorDefects = db.rawQuery(query, null);
                        JSONArray vcdObjects = new JSONArray();
                        if (cursorDefects.moveToFirst()) {
                            do {
                                VehicleChecklistAnswerDefect vcd = new VehicleChecklistAnswerDefect();
                                vcd.vehicleChecklistID = cursorDefects.getString(cursorDefects.getColumnIndex("VehicleChecklistID"));
                                vcd.checklistQuestionID = cursorDefects.getString(cursorDefects.getColumnIndex("ChecklistQuestionID"));
                                vcd.defect = cursorDefects.getString(cursorDefects.getColumnIndex("Defect"));

                                vcdObjects.put(vcd.getJSONObject());

                            } while (cursorDefects.moveToNext());
                            try {
                                vcObject.put("Defects", vcdObjects);
                            } catch (JSONException e) {
                                throw e;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorLog.CreateError(e, "VehicleChecklistDefects");
                        throw e;
                    }

                    vehicleChecklists.put(vcObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "GetVehicleChecklistsToSend "+ vcObject.toString());
            throw e;
        }
        finally {
            if (cursor != null) {
                cursor.close();
            }
            if (cursorAnswers != null) {
                cursorAnswers.close();
            }
            if (cursorDefects != null) {
                cursorDefects.close();
            }
        }

        return vehicleChecklists;
    }
    
    public static void updatePhoto(String vehicleChecklistID, String imageLocation) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ImageLocation", imageLocation);

        db.update("VehicleChecklists", values, "VehicleChecklistID = ?", new String[] { vehicleChecklistID });
    }

    public static String getPhoto(String vehicleChecklistID) {
        String imageLocation = null;

        String query = "" +
            "select " +
            "ImageLocation " +
            "from " +
            "VehicleChecklists " +
            "where " +
            "VehicleChecklistID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { vehicleChecklistID });
        
        if (cursor.moveToFirst()) {
            imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
        }

        cursor.close();

        return imageLocation;
    }

    public static String getMileage(String vehicleChecklistID) {
        String imageLocation = null;

        String query = "" +
            "select " +
            "ImageLocation " +
            "from " +
            "VehicleChecklists " +
            "where " +
            "VehicleChecklistID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { vehicleChecklistID });

        if (cursor.moveToFirst()) {
            imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
        }

        cursor.close();

        return imageLocation;
    }
}
