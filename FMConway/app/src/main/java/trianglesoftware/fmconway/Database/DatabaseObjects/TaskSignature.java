package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Adam.Patrick on 16/11/2016.
 */

public class TaskSignature {
    public String taskSignatureID;
    public String taskID;
    public String staffID;
    public String userID;
    public Boolean isClient;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public String shiftID;
    public String printedName;
    public String clientVehicleReg;

    private JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("TaskSignatureID", this.taskSignatureID);
        obj.put("TaskID", this.taskID);
        obj.put("StaffID", this.staffID);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("IsClient", this.isClient);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("PrintedName", this.printedName);
        obj.put("ClientVehicleReg", this.clientVehicleReg);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);

        if (bm != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }

        return obj;
    }

    public static void addTaskSignature(TaskSignature taskSignature)
    {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("TaskSignatureID", taskSignature.taskSignatureID);
            values.put("StaffID", taskSignature.staffID);
            values.put("TaskID", taskSignature.taskID);
            values.put("Longitude", taskSignature.longitude);
            values.put("Latitude", taskSignature.latitude);
            values.put("ImageLocation", taskSignature.imageLocation);
            values.put("UserID", taskSignature.userID);
            values.put("IsClient", taskSignature.isClient);
            values.put("ShiftID", taskSignature.shiftID);
            values.put("PrintedName", taskSignature.printedName);
            values.put("ClientVehicleReg", taskSignature.clientVehicleReg);

            db.insert("TaskSignatures", null, values);
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"AddTaskSignature");
        }
    }

    public static void deleteAllTaskSignaturesForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskSignatures WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllTaskSignaturesForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskSignatures WHERE ShiftID = '" + shiftID + "'");
    }

    public static void deleteTaskSignature(String taskSignatureID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskSignatures WHERE TaskSignatureID = '" + taskSignatureID + "'");
    }

    public static boolean checkStaffSignatureExists(String taskID, String staffID) {
        boolean ret = false;

        String query = "SELECT * FROM TaskSignatures ";
        query += "WHERE TaskID = '" + taskID + "' AND StaffID = '" + staffID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ret = true;
            } while (cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static boolean checkClientSignatureExists(String taskID) {
        boolean ret = false;

        String query = "SELECT * FROM TaskSignatures ";
        query += "WHERE TaskID = '" + taskID + "' AND IsClient = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ret = true;
            } while (cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static JSONArray getSignaturesToSend(String shiftID) throws Exception{
        JSONArray signatures = new JSONArray();

        String query = "SELECT ts.* FROM TaskSignatures ts ";
        query += "JOIN Tasks t on t.TaskID = ts.TaskID ";
        query += "JOIN Activities act on act.ActivityID = t.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = act.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    TaskSignature s = new TaskSignature();
                    s.taskSignatureID = cursor.getString(cursor.getColumnIndex("TaskSignatureID"));
                    s.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                    s.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    int columnIndex = cursor.getColumnIndex("IsClient");
                    if (!cursor.isNull(columnIndex)) {
                        s.isClient = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsClient"))) > 0;
                    }

                    s.printedName = cursor.getString(cursor.getColumnIndex("PrintedName"));
                    s.clientVehicleReg = cursor.getString(cursor.getColumnIndex("ClientVehicleReg"));

                    sObject = s.getJSONObject();

                    signatures.put(sObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetTaskSignaturesToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return signatures;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > 400 || width > 400) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > 400
                    && (halfWidth / inSampleSize) > 400) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
