package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.location.Location;
import android.location.LocationManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Task {
    public String taskID;
    public String name;
    public int taskOrder;
    public int taskTypeID;
    public String areaID;
    public String checklistID;
    public String activityID;
    public Date startTime;
    public Date endTime;
    public String notes;
    public String trafficCount;
    public Boolean photosRequired;
    public Boolean showDetails;
    public Boolean showClientSignature;
    public Boolean clientSignatureRequired;
    public Boolean showStaffSignature;
    public Boolean staffSignatureRequired;
    public String tag;
    public double longitude;
    public double latitude;
    public String weatherConditionID;
    public boolean isMandatory;

    public Task() {
    }

    public String getTaskID() {
        return this.taskID;
    }

    public String getName() {
        return this.name;
    }

    public int getTaskOrder() {
        return this.taskOrder;
    }

    public int getTaskTypeID() {
        return this.taskTypeID;
    }

    public String getAreaID() {
        return this.areaID;
    }

    public String getChecklistID() {
        return this.checklistID;
    }

    public String getActivityID() {
        return this.activityID;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    private String getStartTimeString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if (this.startTime != null) {
            return dateFormat.format(this.startTime);
        } else {
            return "";
        }
    }

    private String getEndTimeString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if (this.endTime != null) {
            return dateFormat.format(this.endTime);
        } else {
            return "";
        }
    }

    private String getNotes() {
        return this.notes;
    }

    public String getTrafficCount() {
        return this.trafficCount;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTaskOrder(int taskOrder) {
        this.taskOrder = taskOrder;
    }

    public void setTaskTypeID(int taskTypeID) {
        this.taskTypeID = taskTypeID;
    }

    public void setAreaID(String areaID) {
        this.areaID = areaID;
    }

    public void setChecklistID(String checklistID) {
        this.checklistID = checklistID;
    }

    public void setActivityID(String activityID) {
        this.activityID = activityID;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setTrafficCount(String trafficCount) {
        this.trafficCount = trafficCount;
    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("TaskID", this.taskID);
        obj.put("Name", this.name);
        obj.put("TaskOrder", this.taskOrder);
        obj.put("TaskTypeID", this.taskTypeID);
        obj.put("AreaID", this.areaID);
        obj.put("ChecklistID", this.checklistID);
        obj.put("ActivityID", this.activityID);
        obj.put("StartTime", getStartTimeString());
        obj.put("EndTime", getEndTimeString());
        obj.put("Notes", this.notes);
        obj.put("IsMandatory", this.isMandatory);

        return obj;
    }

    public JSONObject getJSONObjectToSend() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("TaskID", this.taskID);
        obj.put("Name", this.name);
        obj.put("TaskOrder", this.taskOrder);
        obj.put("TaskTypeID", this.taskTypeID);
        obj.put("AreaID", this.areaID);
        obj.put("ChecklistID", this.checklistID);
        obj.put("ActivityID", this.activityID);
        obj.put("StartTime", getStartTimeString());
        obj.put("EndTime", getEndTimeString());
        obj.put("Notes", this.notes);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("WeatherConditionID", this.weatherConditionID);

        return obj;
    }

    public static boolean exists(String taskID) {
        String query = "" +
                "select " +
                "TaskID " +
                "from " +
                "Tasks " +
                "where " +
                "TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{taskID});

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    //Tasks
    public static void addTask(Task task) {
        if (exists(task.taskID)) {
            return;
        }

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TaskID", task.taskID);
        values.put("Name", task.name);
        values.put("TaskOrder", task.taskOrder);
        values.put("TaskTypeID", task.taskTypeID);
        values.put("AreaID", task.areaID);
        values.put("ChecklistID", task.checklistID);
        values.put("ActivityID", task.activityID);
        values.put("StartTime", task.getStartTimeString());
        values.put("EndTime", task.getEndTimeString());
        values.put("PhotosRequired", task.photosRequired);
        values.put("ShowDetails", task.showDetails);
        values.put("ShowClientSignature", task.showClientSignature);
        values.put("ClientSignatureRequired", task.clientSignatureRequired);
        values.put("ShowStaffSignature", task.showStaffSignature);
        values.put("StaffSignatureRequired", task.staffSignatureRequired);
        values.put("Tag", task.tag);
        values.put("WeatherConditionID", task.weatherConditionID);
        values.put("IsMandatory", task.isMandatory);

        if (task.getNotes() == null) {
            values.put("Notes", "");
        } else {
            values.put("Notes", task.notes);
        }

        try {
            db.insertOrThrow("Tasks", null, values);
        } catch (Exception e) {
            ErrorLog errorLog = new ErrorLog();
            errorLog.description = e.getMessage();
            errorLog.exception = e.toString();
            errorLog.sent = false;

            ErrorLog.AddErrorLog(errorLog);
            throw e;
        }
    }

    public static void deleteAllTasks() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Tasks");
    }

    public static void deleteTasksForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Tasks WHERE ActivityID = '" + activityID + "'");
    }

    public static int GetFirstTaskTypeID(String activityID) {
        String query = "SELECT * FROM Tasks ";
        query += "WHERE TaskOrder = 1 AND ActivityID = '" + activityID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int tt = 0;
        if (cursor.moveToFirst()) {
            do {
                tt = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskTypeID")));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return tt;
    }

    public static void ResetEndTime() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("UPDATE Tasks SET EndTime = NULL");
    }

    // gets next incomplete task for activity
    public static Task GetTaskFromActivity(String activityID) {
        String query = "SELECT * FROM Tasks ";
        query += "WHERE ActivityID = '" + activityID + "' AND (EndTime = '' OR EndTime IS NULL) AND TaskTypeID <> 4 ";
        query += "ORDER BY TaskOrder LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Task t = null;
        if (cursor.moveToFirst()) {
            do {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

                t = new Task();
                t.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                t.taskTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskTypeID")));
                t.taskOrder = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskOrder")));
                t.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                t.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                t.areaID = cursor.getString(cursor.getColumnIndex("AreaID"));
                t.name = cursor.getString(cursor.getColumnIndex("Name"));
                t.notes = cursor.getString(cursor.getColumnIndex("Notes"));
                t.trafficCount = cursor.getString(cursor.getColumnIndex("TrafficCount"));
                t.photosRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PhotosRequired"))) > 0;
                t.showDetails = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowDetails"))) > 0;
                t.showClientSignature = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowClientSignature"))) > 0;
                t.clientSignatureRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ClientSignatureRequired"))) > 0;
                t.showStaffSignature = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowStaffSignature"))) > 0;
                t.staffSignatureRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StaffSignatureRequired"))) > 0;
                t.tag = cursor.getString(cursor.getColumnIndex("Tag"));
                t.weatherConditionID = cursor.getString(cursor.getColumnIndex("WeatherConditionID"));
                t.isMandatory = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsMandatory"))) > 0;

                try {
                    String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                    if (startTime != null)
                        t.setStartTime(format.parse(startTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                    if (endTime != null)
                        t.setStartTime(format.parse(endTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        cursor.close();

        return t;
    }

    public static int GetIncompleteTaskCount(String jobPackID) {
        int count = 0;

        String query = "SELECT TaskID FROM Tasks t JOIN Activities a ON a.ActivityID = t.ActivityID  WHERE a.JobPackID = '" + jobPackID;
        query += "' AND (t.EndTime = '' OR t.EndTime IS NULL) AND t.TaskTypeID <> 4";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                count++;
            } while (cursor.moveToNext());
        }

        cursor.close();

        return count;
    }

    public static Task GetTaskFromActivityAndOrder(String activityID, int taskOrder) {
        String query = "SELECT * FROM Tasks ";
        query += "WHERE ActivityID = '" + activityID + "' AND TaskOrder = " + taskOrder;
        query += " LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Task t = null;
        if (cursor.moveToFirst()) {
            do {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

                t = new Task();
                t.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                t.taskTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskTypeID")));
                t.taskOrder = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskOrder")));
                t.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                t.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                t.areaID = cursor.getString(cursor.getColumnIndex("AreaID"));
                t.name = cursor.getString(cursor.getColumnIndex("Name"));
                t.notes = cursor.getString(cursor.getColumnIndex("Notes"));
                t.trafficCount = cursor.getString(cursor.getColumnIndex("TrafficCount"));
                t.photosRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PhotosRequired"))) > 0;
                t.showDetails = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowDetails"))) > 0;
                t.showClientSignature = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowClientSignature"))) > 0;
                t.clientSignatureRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ClientSignatureRequired"))) > 0;
                t.showStaffSignature = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowStaffSignature"))) > 0;
                t.staffSignatureRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StaffSignatureRequired"))) > 0;
                t.tag = cursor.getString(cursor.getColumnIndex("Tag"));
                t.weatherConditionID = cursor.getString(cursor.getColumnIndex("WeatherConditionID"));
                t.isMandatory = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsMandatory"))) > 0;

                try {
                    String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                    if (startTime != null)
                        t.setStartTime(format.parse(startTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                    if (endTime != null)
                        t.setStartTime(format.parse(endTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        cursor.close();

        return t;
    }

    public static Task GetTask(String taskID) {
        String query = "SELECT * FROM Tasks ";
        query += "WHERE TaskID = '" + taskID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Task t = null;
        if (cursor.moveToFirst()) {
            do {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

                t = new Task();
                t.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                t.taskTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskTypeID")));
                t.taskOrder = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskOrder")));
                t.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                t.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                t.areaID = cursor.getString(cursor.getColumnIndex("AreaID"));
                t.name = cursor.getString(cursor.getColumnIndex("Name"));
                t.notes = cursor.getString(cursor.getColumnIndex("Notes"));
                t.trafficCount = cursor.getString(cursor.getColumnIndex("TrafficCount"));
                t.photosRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PhotosRequired"))) > 0;
                t.showDetails = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowDetails"))) > 0;
                t.showClientSignature = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowClientSignature"))) > 0;
                t.clientSignatureRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ClientSignatureRequired"))) > 0;
                t.showStaffSignature = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShowStaffSignature"))) > 0;
                t.staffSignatureRequired = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StaffSignatureRequired"))) > 0;
                t.tag = cursor.getString(cursor.getColumnIndex("Tag"));
                t.weatherConditionID = cursor.getString(cursor.getColumnIndex("WeatherConditionID"));
                t.isMandatory = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsMandatory"))) > 0;

                try {
                    String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                    if (startTime != null)
                        t.setStartTime(format.parse(startTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                    if (endTime != null)
                        t.setStartTime(format.parse(endTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        cursor.close();

        return t;
    }

    public static void StartTask(String taskID) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String now = dateFormat.format(new Date());

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "select StartTime from Tasks where TaskID = ?";
        Cursor cursor = db.rawQuery(query, new String[]{taskID});

        boolean startTimeExists = false;
        if (cursor.moveToFirst()) {
            try {
                String startTimeString = cursor.getString(cursor.getColumnIndex("StartTime"));
                if (!FMConwayUtils.isNullOrWhitespace(startTimeString)) {
                    Date startTime = dateFormat.parse(startTimeString);
                    startTimeExists = startTime != null;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        // only update StartTime if not started already
        if (!startTimeExists) {
            ContentValues values = new ContentValues();
            values.put("StartTime", now);

            db.update("Tasks", values, "TaskID = ?", new String[]{taskID});
        }
    }

    public static void EndTask(String taskID) {
        // get longitude and latitude
        LocationManager lm = (LocationManager) FMConwayApp.getContext().getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = 0;
        double latitude = 0;
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        EndTask(taskID, longitude, latitude);
    }

    private static void EndTask(String taskID, double longitude, double latitude) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String now = dateFormat.format(new Date());

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "select EndTime from Tasks where TaskID = ?";
        Cursor cursor = db.rawQuery(query, new String[]{taskID});

        boolean endTimeExists = false;
        if (cursor.moveToFirst()) {
            try {
                String endTimeString = cursor.getString(cursor.getColumnIndex("EndTime"));
                if (!FMConwayUtils.isNullOrWhitespace(endTimeString)) {
                    Date endTime = dateFormat.parse(endTimeString);
                    endTimeExists = endTime != null;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        // only update EndTime if not complete already
        if (!endTimeExists) {
            ContentValues values = new ContentValues();
            values.put("EndTime", now);
            values.put("Longitude", longitude);
            values.put("Latitude", latitude);

            db.update("Tasks", values, "TaskID = ?", new String[]{taskID});
        }
    }

    public static int getTaskOrder(String taskID) {
        String query = "" +
                "select " +
                "TaskOrder " +
                "from " +
                "Tasks " +
                "where " +
                "TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{taskID});

        int taskOrder = 0;
        if (cursor.moveToFirst()) {
            taskOrder = cursor.getInt(cursor.getColumnIndex("TaskOrder"));
        }

        cursor.close();

        return taskOrder;
    }

    public static int GetTotalTasksForActivity(String activityID) {
        String query = "SELECT Count(*) as Total FROM Tasks ";
        query += "WHERE ActivityID = '" + activityID + "' AND TaskTypeID <> 4";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int total = 0;
        if (cursor.moveToFirst()) {
            do {
                total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Total")));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return total;
    }

    public static String GetJobPackForTask(String taskID) {
        String query = "SELECT a.JobPackID FROM Tasks t ";
        query += "INNER JOIN Activities a ON a.ActivityID = t.ActivityID ";
        query += "WHERE t.TaskID = '" + taskID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String jobPackID = null;
        if (cursor.moveToFirst()) {
            do {
                jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return jobPackID;
    }

    public static JSONArray GetTasksToSend(String shiftID) throws Exception {
        JSONArray tasks = new JSONArray();

        String query = "SELECT t.* FROM Tasks t ";
        query += "JOIN Activities act on act.ActivityID = t.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = act.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "' AND TaskTypeID <> 4";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            JSONObject tObject;
            if (cursor.moveToFirst()) {
                do {
                    Task t = new Task();
                    t.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                    t.notes = cursor.getString(cursor.getColumnIndex("Notes"));

                    String latitude = cursor.getString(cursor.getColumnIndex("Latitude"));
                    String longitude = cursor.getString(cursor.getColumnIndex("Longitude"));

                    if (latitude != null && longitude != null) {
                        t.latitude = Double.parseDouble(latitude);
                        t.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    }

                    try {
                        String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                        if (startTime != null)
                            t.setStartTime(format.parse(startTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                        if (endTime != null)
                            t.setEndTime(format.parse(endTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    t.weatherConditionID = cursor.getString(cursor.getColumnIndex("WeatherConditionID"));

                    tObject = t.getJSONObjectToSend();

                    tasks.put(tObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "GetTasksToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return tasks;
    }

    // gets all tasks for activity
    public static List<Task> GetTasksForActivity(String activityID) {
        List<Task> tasks = new LinkedList<>();

        String query = "SELECT * FROM Tasks ";
        query += "WHERE ActivityID = '" + activityID + "' AND TaskTypeID <> 4";
        query += " ORDER BY TaskOrder";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Task t;
        if (cursor.moveToFirst()) {
            do {
                t = new Task();
                t.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                t.name = cursor.getString(cursor.getColumnIndex("Name"));
                t.taskTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskTypeID")));
                t.taskOrder = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TaskOrder")));
                t.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                t.isMandatory = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsMandatory"))) > 0;

                String latitude = cursor.getString(cursor.getColumnIndex("Latitude"));
                String longitude = cursor.getString(cursor.getColumnIndex("Longitude"));

                if (latitude != null && longitude != null) {
                    t.latitude = Double.parseDouble(latitude);
                    t.longitude = Double.parseDouble(longitude);
                }

                try {
                    String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                    if (startTime != null)
                        t.startTime = format.parse(startTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                    if (endTime != null)
                        t.endTime = format.parse(endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                tasks.add(t);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return tasks;
    }

    public static void StoreTaskDetails(String taskID, String details) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE Tasks SET Notes = '" + details + "' WHERE TaskID = " + taskID);
        //db.execSQL("UPDATE Tasks SET Notes = ? WHERE TaskID = " + taskID, new String[]{details});

        SQLiteStatement statement = db.compileStatement("UPDATE Tasks SET Notes = ? WHERE TaskID = ?");
        statement.bindString(1, details);
        statement.bindString(2, taskID);
        statement.execute();
        statement.close();
    }

    public static void StoreTrafficCount(String activityID, String count) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE Tasks SET TrafficCount = '" + count + "' WHERE ActivityID = " + activityID);
        db.execSQL("UPDATE Tasks SET TrafficCount = ? WHERE ActivityID = '" + activityID + "'", new String[]{count});
    }

    public static String GetTaskDetails(String taskID) {
        String query = "SELECT Notes FROM Tasks ";
        query += "WHERE TaskID = '" + taskID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String details = "";
        if (cursor.moveToFirst()) {
            do {
                int columnIndex = cursor.getColumnIndex("Notes");
                if (!cursor.isNull(columnIndex)) {
                    details = cursor.getString(cursor.getColumnIndex("Notes"));
                }

            } while (cursor.moveToNext());
        }

        cursor.close();

        if (Objects.equals(details, "null")) {
            details = "";
        }

        return details;
    }

    public static String getEquipmentCollectionTaskIdByActivityID(String activityID) {
        String taskID = null;

        String query = "" +
                "select " +
                "TaskID " +
                "from " +
                "Tasks as t " +
                "inner join TaskTypes as tt on tt.TaskTypeID = t.TaskTypeID " +
                "where " +
                "t.ActivityID = ? and " +
                "tt.Name = 'Equipment Collection' ";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{activityID});

        if (cursor.moveToFirst()) {
            taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
        }

        cursor.close();

        return taskID;
    }

    public static String getWeatherCondition(String taskID) {
        String weatherConditionID = null;

        String query = "" +
                "select " +
                "t.WeatherConditionID " +
                "from " +
                "Tasks as t " +
                "where " +
                "t.TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{taskID});

        if (cursor.moveToFirst()) {
            weatherConditionID = cursor.getString(cursor.getColumnIndex("WeatherConditionID"));
        }

        cursor.close();

        return weatherConditionID;
    }

    public static void updateWeatherCondition(String taskID, String weatherConditionID) {
        ContentValues values = new ContentValues();
        values.put("WeatherConditionID", weatherConditionID);

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.update("Tasks", values, "TaskID = ?", new String[]{taskID});
    }

    public static Task InstallDeinstallTask(String shiftID) {
        Task task = new Task();

        String query =
                "SELECT" +
                        " * " +
                        " FROM" +
                        " Tasks t" +
                        " JOIN Activities a ON t.ActivityID = a.ActivityID " +
                        " JOIN JobPacks j ON a.JobPackID = j.JobPackID" +
                        " WHERE j.ShiftID = '" + shiftID + "'" +
                        " AND t.TaskTypeID = 8"; //install Task

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            task.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
        }

        return task;
    }

    public static Task getChecklistTask(String shiftID) {
        Task task = new Task();

        String query =
                "SELECT" +
                        " t.TaskID " +
                        " FROM" +
                        " Tasks t" +
                        " JOIN Activities a ON t.ActivityID = a.ActivityID " +
                        " JOIN JobPacks j ON a.JobPackID = j.JobPackID" +
                        " WHERE j.ShiftID = '" + shiftID + "'" +
                        " AND t.TaskTypeID = 3"; //checklist Task

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String taskID = null;
        if (cursor.moveToFirst()) {
            taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
        }

        if (taskID != null) {
            task = GetTask(taskID);
        }

        return task;
    }

    public static Task getCleansingLogTask(String shiftID) {
        Task task = new Task();

        String query =
                "SELECT" +
                        " t.TaskID " +
                        " FROM" +
                        " Tasks t" +
                        " JOIN Activities a ON t.ActivityID = a.ActivityID " +
                        " JOIN JobPacks j ON a.JobPackID = j.JobPackID" +
                        " WHERE j.ShiftID = '" + shiftID + "'" +
                        " AND t.TaskTypeID = 14"; //cleansing log Task

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String taskID = null;
        if (cursor.moveToFirst()) {
            taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
        }

        if (taskID != null) {
            task = GetTask(taskID);
        }

        return task;
    }
}
