package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class MaintenanceCheckAnswer {
    public String maintenanceCheckID;
    public String checklistID;
    public String checklistQuestionID;
    public boolean answer;
    public String question;
    public boolean isNew;
    public String activityID;

    public MaintenanceCheckAnswer(){
    }

    private String getMaintenanceCheckID() { return this.maintenanceCheckID; }
    private String getChecklistID() { return this.checklistID; }
    private String getChecklistQuestionID() { return this.checklistQuestionID; }
    private boolean getAnswer() { return this.answer; }
    public boolean getIsNew() { return this.isNew; }
    public String getQuestion() { return this.question; }
    public String getActivityID() { return this.activityID;}

    public void setMaintenanceCheckID(String maintenanceCheckID) { this.maintenanceCheckID = maintenanceCheckID; }
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setAnswer(boolean answer) { this.answer = answer; }
    public void setNew(boolean isNew) { this.isNew = isNew; }
    public void setQuestion(String question) { this.question = question; }
    public void setActivityID(String activityID) { this.activityID = activityID;}

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("MaintenanceCheckID", this.maintenanceCheckID);
            obj.put("ChecklistID", this.checklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Answer", this.answer);
            obj.put("IsNew", this.isNew);
            obj.put("Question", this.question);

        return obj;
    }

    public JSONObject getJSONObjectToSend() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("MaintenanceCheckID", this.maintenanceCheckID);
            obj.put("ChecklistID", this.checklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Answer", this.answer);
            //obj.put("Question", this.question);

        return obj;
    }

    //Maintenance Checklist Answers
    public static void addMaintenanceCheckAnswer(MaintenanceCheckAnswer maintenanceCheckAnswer)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("MaintenanceCheckID", maintenanceCheckAnswer.maintenanceCheckID);
        values.put("ChecklistID", maintenanceCheckAnswer.checklistID);
        values.put("ChecklistQuestionID", maintenanceCheckAnswer.checklistQuestionID);
        values.put("ActivityID", maintenanceCheckAnswer.activityID);
        //values.put("Answer", maintenanceCheckAnswer.getAnswer());

        db.insert("MaintenanceCheckAnswers", null, values);
    }

    public static void addMaintenanceCheckAnswerDownload(MaintenanceCheckAnswer maintenanceCheckAnswer)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("MaintenanceCheckID", maintenanceCheckAnswer.maintenanceCheckID);
        values.put("ChecklistID", maintenanceCheckAnswer.checklistID);
        values.put("ChecklistQuestionID", maintenanceCheckAnswer.checklistQuestionID);
        values.put("Answer", maintenanceCheckAnswer.answer);
        values.put("ActivityID", maintenanceCheckAnswer.activityID);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(maintenanceCheckAnswer.maintenanceCheckID, maintenanceCheckAnswer.checklistID, maintenanceCheckAnswer.checklistQuestionID)) {
            db.insert("MaintenanceCheckAnswers", null, values);
        }
    }

    public static boolean exists(String maintenanceCheckID, String checklistID, String checklistQuestionID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query;
        Cursor cursor;
        if (checklistID != null) {
            query = "select * from MaintenanceCheckAnswers where MaintenanceCheckID = ? and ChecklistID = ? and ChecklistQuestionID = ?";
            cursor = db.rawQuery(query, new String[] { maintenanceCheckID, checklistID, checklistQuestionID });
        } else {
            query = "select * from MaintenanceCheckAnswers where MaintenanceCheckID = ? and ChecklistQuestionID = ?";
            cursor = db.rawQuery(query, new String[] { maintenanceCheckID, checklistQuestionID });
        }

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllMaintenanceCheckAnswers()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckAnswers");
    }

    public static void deleteMaintenanceCheckAnswersForCheck(String maintenanceCheckID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckAnswers WHERE MaintenanceCheckID = '" + maintenanceCheckID + "'");
    }

    public static List<MaintenanceCheckAnswer> GetMaintenanceCheckAnswers(String maintenanceCheckID) {
        List<MaintenanceCheckAnswer> maintenanceCheckAnswers = new LinkedList<>();

        String query = "SELECT mca.*, cq.Question FROM MaintenanceCheckAnswers mca ";
        query += "INNER JOIN ChecklistQuestions cq on cq.ChecklistQuestionID = mca.ChecklistQuestionID ";
        query += "WHERE MaintenanceCheckID = '" + maintenanceCheckID;
        query += "' ORDER BY cq.QuestionOrder";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        MaintenanceCheckAnswer mca;
        if(cursor.moveToFirst()){
            do{
                mca = new MaintenanceCheckAnswer();
                mca.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                mca.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                mca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));

                int columnIndex = cursor.getColumnIndex("Answer");
                if (!cursor.isNull(columnIndex)) {
                    mca.answer = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0;
                }
                else
                {
                    mca.isNew = true;
                }
                mca.question = cursor.getString(cursor.getColumnIndex("Question"));

                maintenanceCheckAnswers.add(mca);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return maintenanceCheckAnswers;
    }


    public static void SetMaintenanceAnswer(String checklistQuestionID, String maintenanceCheckID, boolean answer) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("Update MaintenanceCheckAnswers Set Answer = " + (answer ? 1 : 0) + " WHERE MaintenanceCheckID = '" + maintenanceCheckID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'");
    }

//    public static JSONArray GetMaintenanceCheckAnswersToSend(int shiftID) {
//        JSONArray maintenanceCheckAnswers = new JSONArray();
//
//        String query = "SELECT mca.* FROM MaintenanceCheckAnswers mca ";
//        query += "JOIN MaintenanceChecks mc on mc.MaintenanceCheckID = mca.MaintenanceCheckID ";
//        query += "JOIN Activties a on a.ActivityID = mc.Activity ";
//        query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
//        query += "WHERE jp.ShiftID = " + shiftID;
//
//        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
//        Cursor cursor = db.rawQuery(query, null);
//
//        JSONObject mcaObject;
//        if(cursor.moveToFirst()){
//            do{
//                MaintenanceCheckAnswer mca = new MaintenanceCheckAnswer();
//                mca.setMaintenanceCheckID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"))));
//                mca.setChecklistID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("ChecklistID"))));
//                mca.setChecklistQuestionID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"))));
//                mca.setAnswer(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0);
//
//                mcaObject = mca.getJSONObject();
//
//                maintenanceCheckAnswers.put(mcaObject);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return maintenanceCheckAnswers;
//    }
}
