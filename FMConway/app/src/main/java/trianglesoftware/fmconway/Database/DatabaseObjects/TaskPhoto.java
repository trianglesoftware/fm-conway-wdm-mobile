package trianglesoftware.fmconway.Database.DatabaseObjects;

/**
 * Created by Adam.Patrick on 23/11/2016.
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.Date;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class TaskPhoto {

    public String taskPhotoID;
    public String taskID;
    public String userID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public Date time;
    public String shiftID;
    public String comments;

    public TaskPhoto() {
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if (this.time != null) {
            return dateFormat.format(this.time);
        } else {
            return "";
        }
    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("TaskPhotoID", this.taskPhotoID);
        obj.put("TaskID", this.taskID);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Time", getTime());
        obj.put("Comments", this.comments);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options, 400, 400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
        if (bm != null) {
            try {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            } catch (Exception e) {
                ErrorLog.CreateError(e, "Rotate Photo");
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }
        return obj;
    }

    public static void saveTaskPhoto(TaskPhoto taskPhoto) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TaskPhotoID", taskPhoto.taskPhotoID);
        values.put("TaskID", taskPhoto.taskID);
        values.put("Longitude", taskPhoto.longitude);
        values.put("Latitude", taskPhoto.latitude);
        values.put("ImageLocation", taskPhoto.imageLocation);
        values.put("UserID", taskPhoto.userID);
        values.put("Time", taskPhoto.getTime());
        values.put("ShiftID", taskPhoto.shiftID);
        values.put("Comments", taskPhoto.comments);

        if (checkIfTaskPhotoExists(taskPhoto.taskPhotoID)) {
            db.update("TaskPhotos", values, "TaskPhotoID = ?", new String[]{taskPhoto.taskPhotoID});
        } else {
            db.insert("TaskPhotos", null, values);
        }
    }

    public static void saveComments(String taskPhotoID, String comments) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("Comments", comments);

        db.update("TaskPhotos", cv, "TaskPhotoID = ?", new String[]{taskPhotoID});
    }

    public static void deleteAllTaskPhotos() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskPhotos");
    }

    public static void deleteTaskPhoto(String taskPhotoID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskPhotos WHERE TaskPhotoID = '" + taskPhotoID + "'");
    }

    public static void deleteTaskPhotoFile(String taskPhotoID) {
        try {
            String query = "SELECT * FROM TaskPhotos ";
            query += "WHERE TaskPhotoID = '" + taskPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DeleteTaskPhotoFile");
        }
    }

    public static void deleteAllTaskPhotosForUser(String userID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllTaskPhotosForShift(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static TaskPhoto getTaskPhoto(String taskPhotoID) {
        TaskPhoto taskPhoto = null;

        String query = "SELECT * FROM TaskPhotos WHERE TaskPhotoID = '" + taskPhotoID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            taskPhoto = new TaskPhoto();
            taskPhoto.taskPhotoID = c.getString(c.getColumnIndex("TaskPhotoID"));
            taskPhoto.taskID = c.getString(c.getColumnIndex("TaskID"));
            taskPhoto.userID = c.getString(c.getColumnIndex("UserID"));
            taskPhoto.longitude = Double.parseDouble(c.getString(c.getColumnIndex("Longitude")));
            taskPhoto.latitude = Double.parseDouble(c.getString(c.getColumnIndex("Latitude")));
            taskPhoto.time = FMConwayUtils.getCursorDate(c, "Time");
            taskPhoto.imageLocation = c.getString(c.getColumnIndex("ImageLocation"));
            taskPhoto.shiftID = c.getString(c.getColumnIndex("ShiftID"));
            taskPhoto.comments = c.getString(c.getColumnIndex("Comments"));
        }

        c.close();

        return taskPhoto;
    }

    public static ArrayList<TaskPhoto> getPhotosForTask(String taskID) {
        ArrayList<TaskPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM TaskPhotos WHERE TaskID = '" + taskID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        TaskPhoto data;
        if (cursor.moveToFirst()) {
            do {
                data = new TaskPhoto();
                data.taskPhotoID = cursor.getString(cursor.getColumnIndex("TaskPhotoID"));
                data.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                data.comments = cursor.getString(cursor.getColumnIndex("Comments"));

                String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageLocation = imageLocation;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options, 150, 150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(
        BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetPhotosToSend(String shiftID) throws Exception {
        JSONArray photos = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT ts.* FROM TaskPhotos ts ";
        query += "JOIN Tasks t on t.TaskID = ts.TaskID ";
        query += "JOIN Activities act on act.ActivityID = t.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = act.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    TaskPhoto s = new TaskPhoto();
                    s.taskPhotoID = cursor.getString(cursor.getColumnIndex("TaskPhotoID"));
                    s.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    s.comments = cursor.getString(cursor.getColumnIndex("Comments"));

                    try {
                        String time = cursor.getString(cursor.getColumnIndex("Time"));
                        if (time != null)
                            s.time = format.parse(time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    sObject = s.getJSONObject();

                    photos.put(sObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "GetTaskPhotosToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return photos;
    }

    public static boolean checkIfTaskPhotoExists(String taskPhotoID) {
        String query = "SELECT * FROM TaskPhotos WHERE TaskPhotoID = '" + taskPhotoID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        boolean result = cursor.moveToFirst();

        cursor.close();

        return result;
    }

    public static boolean checkIfPhotosExist(String taskID) {
        String query = "SELECT * FROM TaskPhotos WHERE TaskID = '" + taskID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        boolean result = cursor.moveToFirst();
        
        cursor.close();

        return result;
    }

    public static boolean checkIfPhotosExistForActivity(String activityID) {
        String query = "" +
            "select " +
            "tp.* " +
            "from " +
            "TaskPhotos as tp " +
            "inner join Tasks as t on t.TaskID = tp.TaskID " +
            "where " +
            "t.ActivityID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { activityID });

        boolean result = cursor.moveToFirst();
        
        cursor.close();

        return result;
    }
}
