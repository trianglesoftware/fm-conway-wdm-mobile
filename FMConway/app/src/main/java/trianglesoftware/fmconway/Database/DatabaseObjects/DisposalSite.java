package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class DisposalSite {
    public String disposalSiteID;
    public String name;
    public boolean isActive;

    public DisposalSite() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("DisposalSiteID", this.disposalSiteID);
            obj.put("Name", this.name);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "Disposal Site - getJSONObject");
        }
        return obj;
    }

    protected static DisposalSite cursorToModel(Cursor c) {
        DisposalSite disposalSite = new DisposalSite();
        disposalSite.disposalSiteID = c.getString(c.getColumnIndex("DisposalSiteID"));
        disposalSite.name = c.getString(c.getColumnIndex("Name"));
        disposalSite.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return disposalSite;
    }

    public static DisposalSite get(String disposalSiteID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        DisposalSite model = null;
        Cursor c = db.rawQuery("SELECT * FROM DisposalSites WHERE DisposalSiteID = ?", new String[] { disposalSiteID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<DisposalSite> getAll() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<DisposalSite> models = new LinkedList<>();

        Cursor c = db.rawQuery("SELECT * FROM DisposalSites WHERE IsActive = 1 ORDER BY Name", null);

        if (c.moveToFirst()) {
            do {
                DisposalSite model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(DisposalSite model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("DisposalSiteID", model.disposalSiteID);
            cv.put("Name", model.name);
            cv.put("IsActive", model.isActive);

            if (get(model.disposalSiteID) != null) {
                db.update("DisposalSites", cv, "DisposalSiteID = ?", new String[]{model.disposalSiteID});
            } else {
                db.insertOrThrow("DisposalSites", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "Disposal Site - save");
        }
    }
}
