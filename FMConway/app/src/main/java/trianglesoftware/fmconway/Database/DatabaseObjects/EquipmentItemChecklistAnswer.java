package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class EquipmentItemChecklistAnswer {
    public String EquipmentItemChecklistAnswerID;
    public String EquipmentItemChecklistID;
    public String ChecklistQuestionID;
    public int QuestionOrder;
    public String Question;
    public Boolean Answer;
    public String Reason;

    public EquipmentItemChecklistAnswer() {
        
    }

    public void insert() {
        ContentValues values = new ContentValues();
        values.put("EquipmentItemChecklistAnswerID", EquipmentItemChecklistAnswerID);
        values.put("EquipmentItemChecklistID", EquipmentItemChecklistID);
        values.put("ChecklistQuestionID", ChecklistQuestionID);
        values.put("Answer", booleanToInteger(Answer));

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.insertOrThrow("EquipmentItemChecklistAnswers", null, values);
    }
    
    public void updateAnswer() {
        ContentValues values = new ContentValues();
        values.put("Answer", booleanToInteger(Answer));

        if (Answer == null || Answer) {
            values.put("Reason", (String)null);
        }

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.update("EquipmentItemChecklistAnswers", values, "EquipmentItemChecklistAnswerID = ?", new String[]{ EquipmentItemChecklistAnswerID });
    }

    public void updateReason() {
        ContentValues values = new ContentValues();
        values.put("Reason", TextUtils.isEmpty(Reason) ? null : Reason);

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.update("EquipmentItemChecklistAnswers", values, "EquipmentItemChecklistAnswerID = ?", new String[]{ EquipmentItemChecklistAnswerID });
    }

    public static EquipmentItemChecklistAnswer getAnswer(String equipmentItemChecklistAnswerID) {
        EquipmentItemChecklistAnswer model = new EquipmentItemChecklistAnswer();

        String query = "";
        query += "select ";
        query += "eica.EquipmentItemChecklistAnswerID, ";
        query += "eica.EquipmentItemChecklistID, ";
        query += "eica.ChecklistQuestionID, ";
        query += "eica.Answer, ";
        query += "eica.Reason ";
        query += "from ";
        query += "EquipmentItemChecklistAnswers as eica ";
        query += "where ";
        query += "eica.EquipmentItemChecklistAnswerID = ? ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { equipmentItemChecklistAnswerID });

        if (cursor.moveToFirst()) {
            do {
                model.EquipmentItemChecklistAnswerID = cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistAnswerID"));
                model.EquipmentItemChecklistID = cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistID"));
                model.ChecklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                model.Reason = cursor.getString(cursor.getColumnIndex("Reason"));

                int answerColumnIndex = cursor.getColumnIndex("Answer");
                if (!cursor.isNull(answerColumnIndex)) {
                    model.Answer = cursor.getInt(answerColumnIndex) > 0;
                } else {
                    model.Answer = null;
                }
            } while (cursor.moveToNext());
        }

        cursor.close();

        return model;
    }

    public static List<EquipmentItemChecklistAnswer> getAnswers(String loadingSheetEquipmentItemID) {
        List<EquipmentItemChecklistAnswer> results = new LinkedList<>();

        String query = "";
        query += "select ";
        query += "eica.EquipmentItemChecklistAnswerID, ";
        query += "eica.EquipmentItemChecklistID, ";
        query += "eica.ChecklistQuestionID, ";
        query += "cq.QuestionOrder, ";
        query += "cq.Question, ";
        query += "eica.Answer, ";
        query += "eica.Reason ";
        query += "from ";
        query += "EquipmentItemChecklists as eic ";
        query += "inner join EquipmentItemChecklistAnswers as eica on eica.EquipmentItemChecklistID = eic.EquipmentItemChecklistID ";
        query += "inner join ChecklistQuestions as cq on cq.ChecklistQuestionID = eica.ChecklistQuestionID ";
        query += "where ";
        query += "eic.LoadingSheetEquipmentItemID = ? ";
        query += "order by ";
        query += "cq.QuestionOrder ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { loadingSheetEquipmentItemID });

        if (cursor.moveToFirst()) {
            do {
                EquipmentItemChecklistAnswer model = new EquipmentItemChecklistAnswer();
                model.EquipmentItemChecklistAnswerID = cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistAnswerID"));
                model.EquipmentItemChecklistID = cursor.getString(cursor.getColumnIndex("EquipmentItemChecklistID"));
                model.ChecklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                model.QuestionOrder = cursor.getInt(cursor.getColumnIndex("QuestionOrder"));
                model.Question = cursor.getString(cursor.getColumnIndex("Question"));
                model.Reason = cursor.getString(cursor.getColumnIndex("Reason"));

                int answerColumnIndex = cursor.getColumnIndex("Answer");
                if (!cursor.isNull(answerColumnIndex)) {
                    model.Answer = cursor.getInt(answerColumnIndex) > 0;
                } else {
                    model.Answer = null;
                }

                results.add(model);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return results;
    }
    
    public Boolean integerToBoolean(Integer integer) {
        if (integer == null) {
            return null;
        } else {
            return integer > 0 ? true : false;
        }
    }
    
    public Integer booleanToInteger(Boolean bool) {
        if (bool == null) {
            return null;
        } else {
            return bool ? 1 : 0;
        }
    }
}
