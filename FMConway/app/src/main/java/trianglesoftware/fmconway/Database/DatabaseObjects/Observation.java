package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 18/03/2016.
 */
public class Observation {
    public String observationID;
    public String shiftID;
    //public String observationTypeID;
    public String userID;
    public String peopleInvolved;
    public String location;
    public String observationDescription;
    public String causeOfProblem;
    public Date observationDate;
    //public String chevronDepot;
    public boolean isNew;

    public Observation() {
    }

    private String getObservationID() {
        return this.observationID;
    }
    private String getShiftID() {
        return this.shiftID;
    }
    public String getUserID() { return this.userID;}
//    public int getObservationTypeID() {
//        return this.observationTypeID;
//    }
    public String getPeopleInvolved() {
        return this.peopleInvolved;
    }
    public String getLocation() {
        return this.location;
    }
    public String getObservationDescription() {
        return this.observationDescription;
    }
    public String getCauseOfProblem() {
        return this.causeOfProblem;
    }
//    public String getChevronDepot() {
//        return this.chevronDepot;
//    }
    public boolean getIsNew() {
        return this.isNew;
    }
    private String getObservationDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.observationDate != null){
            return dateFormat.format(this.observationDate);
        }
        else {
            return "";
        }
    }

    public void setObservationID(String observationID) {
        this.observationID = observationID;
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }
    public void setUserID(String userID) { this.userID = userID;}

//    public void setObservationTypeID(int observationTypeID) {
//        this.observationTypeID = observationTypeID;
//    }

    public void setPeopleInvolved(String peopleInvolved) {
        this.peopleInvolved = peopleInvolved;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public void setObservationDescription(String observationDescription) {
        this.observationDescription = observationDescription;
    }
    public void setCauseOfProblem(String causeOfProblem) {
        this.causeOfProblem = causeOfProblem;
    }
    public void setObservationDate(Date observationDate) {
        this.observationDate = observationDate;
    }
//    public void setChevronDepot(String chevronDepot) {
//        this.chevronDepot = chevronDepot;
//    }
    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("ObservationID", this.observationID);
            obj.put("ShiftID", this.shiftID);
            //obj.put("ObservationTypeID", this.observationTypeID);
            obj.put("PeopleInvolved", this.peopleInvolved);
            obj.put("Location", this.location);
            obj.put("ObservationDescription", this.observationDescription);
            obj.put("CauseOfProblem", this.causeOfProblem);
            //obj.put("ChevronDepot", this.chevronDepot);
            obj.put("ObservationDate", getObservationDate());
            obj.put("IsNew", this.isNew);

        return obj;
    }

    //Observations
    public static void AddObservation(Observation observation) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();

        if(!observation.isNew)
        {
            values.put("ObservationID", observation.observationID);
        }
        else
        {
            values.put("ObservationID", UUID.randomUUID().toString());
        }

        if(observation.shiftID != null)
        {
            values.put("ShiftID", observation.shiftID);
        }

        //values.put("ObservationTypeID", observation.getObservationTypeID());
        values.put("PeopleInvolved", observation.peopleInvolved);
        values.put("Location", observation.location);
        values.put("ObservationDescription", observation.observationDescription);
        values.put("CauseOfProblem", observation.causeOfProblem);
        //values.put("ChevronDepot", observation.getChevronDepot());
        values.put("ObservationDate", observation.getObservationDate());
        values.put("IsNew", observation.isNew);
        values.put("UserID", observation.userID);

        db.insert("Observations", null, values);
    }

    public static void deleteAllObservations() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Observations");
    }

    public static void deleteAllObservationsForUser(String userID) {

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = null;

        try {


            String query = "SELECT * FROM ObservationPhotos WHERE UserID = '" + userID + "'";

            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())
            {
                do{
                    String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    try {
                        File file = new File(imageLocation);
                        file.delete();
                    }
                    catch (Exception e)
                    {
                        ErrorLog.CreateError(e, "DeleteObsevationPhoto");
                    }

                }while(cursor.moveToNext());
            }

            db.execSQL("DELETE FROM ObservationPhotos WHERE UserID = '" + userID + "'");

            db.execSQL("DELETE FROM Observations WHERE UserID = '" + userID + "'");


        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DeleteObservationsForUser");
        }
        finally {

            if (cursor != null)
            {
                cursor.close();
            }
        }
    }

    public static List<Observation> GetObservationsForShift(String shiftID, String userID) {
        List<Observation> observations = new LinkedList<>();

        String query = "SELECT * FROM Observations ";
        query += "WHERE ShiftID = '" + shiftID;
        query += "' ORDER BY ObservationDate DESC";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Observation o;
        if(cursor.moveToFirst()){
            do{
                o = new Observation();
                o.observationID = cursor.getString(cursor.getColumnIndex("ObservationID"));
                o.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                //o.setObservationTypeID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("ObservationTypeID"))));
                o.observationDescription = cursor.getString(cursor.getColumnIndex("ObservationDescription"));
                o.causeOfProblem = cursor.getString(cursor.getColumnIndex("CauseOfProblem"));
                o.peopleInvolved = cursor.getString(cursor.getColumnIndex("PeopleInvolved"));
                o.location = cursor.getString(cursor.getColumnIndex("Location"));
                //o.setChevronDepot(cursor.getString(cursor.getColumnIndex("ChevronDepot")));
                try {
                    String observationDate = cursor.getString(cursor.getColumnIndex("ObservationDate"));
                    if(observationDate != null)
                        o.setObservationDate(format.parse(observationDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                observations.add(o);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return observations;
    }

    public static Observation GetObservation(String observationID) {
        String query = "SELECT * FROM Observations ";
        query += "WHERE ObservationID = '" + observationID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Observation o = null;
        if(cursor.moveToFirst()){
            do{
                o = new Observation();
                o.observationID = cursor.getString(cursor.getColumnIndex("ObservationID"));
                o.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                //o.setObservationTypeID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("ObservationTypeID"))));
                o.observationDescription = cursor.getString(cursor.getColumnIndex("ObservationDescription"));
                o.location = cursor.getString(cursor.getColumnIndex("Location"));
                o.peopleInvolved = cursor.getString(cursor.getColumnIndex("PeopleInvolved"));
                o.causeOfProblem = cursor.getString(cursor.getColumnIndex("CauseOfProblem"));
                //o.setChevronDepot(cursor.getString(cursor.getColumnIndex("ChevronDepot")));
                try {
                    String observationDate = cursor.getString(cursor.getColumnIndex("ObservationDate"));
                    if(observationDate != null)
                        o.setObservationDate(format.parse(observationDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }while(cursor.moveToNext());
        }

        cursor.close();

        return o;
    }

    public static void UpdateObservation(Observation observation) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ObservationID", observation.observationID);
        //values.put("ObservationTypeID", observation.getObservationTypeID());
        values.put("ShiftID", observation.shiftID);
        values.put("CauseOfProblem", observation.causeOfProblem);
        values.put("ObservationDescription", observation.observationDescription);
        values.put("Location", observation.location);
        values.put("PeopleInvolved", observation.peopleInvolved);
        //values.put("ChevronDepot", observation.getChevronDepot());

        db.update("Observations", values, "ObservationID = ?", new String[]{String.valueOf(observation.observationID)});
    }

    public static JSONArray GetObservationsToSend(String shiftID, String userID) throws Exception{
        JSONArray observations = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT o.* FROM Observations o ";
        query += "WHERE o.ShiftID = '" + shiftID + "' AND UserID = '" + userID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor cursorPhotos = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject oObject;
            if (cursor.moveToFirst()) {
                do {
                    Observation o = new Observation();
                    o.observationID = cursor.getString(cursor.getColumnIndex("ObservationID"));
                    o.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    o.observationDescription = cursor.getString(cursor.getColumnIndex("ObservationDescription"));
                    o.location = cursor.getString(cursor.getColumnIndex("Location"));
                    o.peopleInvolved = cursor.getString(cursor.getColumnIndex("PeopleInvolved"));
                    o.causeOfProblem = cursor.getString(cursor.getColumnIndex("CauseOfProblem"));
                    //o.setChevronDepot(cursor.getString(cursor.getColumnIndex("ChevronDepot")));
                    //o.setObservationTypeID(Integer.parseInt(cursor.getString(cursor.getColumnIndex("ObservationTypeID"))));
                    o.isNew = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsNew"))) > 0;
                    try {
                        String observationDate = cursor.getString(cursor.getColumnIndex("ObservationDate"));
                        if (observationDate != null)
                            o.setObservationDate(format.parse(observationDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    oObject = o.getJSONObject();

                    query = "SELECT p.* FROM ObservationPhotos p ";
                    query += "WHERE p.ObservationID = '" + cursor.getString(cursor.getColumnIndex("ObservationID")) + "'";

                    cursorPhotos = db.rawQuery(query, null);
                    JSONArray opObjects = new JSONArray();

                    if (cursorPhotos.moveToFirst()) {
                        do {
                            ObservationPhoto p = new ObservationPhoto();
                            p.observationID = cursorPhotos.getString(cursorPhotos.getColumnIndex("ObservationID"));
                            p.latitude = Double.parseDouble(cursorPhotos.getString(cursorPhotos.getColumnIndex("Latitude")));
                            p.longitude = Double.parseDouble(cursorPhotos.getString(cursorPhotos.getColumnIndex("Longitude")));
                            p.imageLocation = cursorPhotos.getString(cursorPhotos.getColumnIndex("ImageLocation"));

                            try {
                                String time = cursorPhotos.getString(cursorPhotos.getColumnIndex("Time"));
                                if (time != null)
                                    p.time = format.parse(time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            opObjects.put(p.getJSONObject());

                        } while (cursorPhotos.moveToNext());

                        try {
                            oObject.put("Photos", opObjects);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    cursorPhotos.close();

                    observations.put(oObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetObservationsToSend");
            throw e;
        }
        finally {
            cursor.close();
            if (cursorPhotos != null)
            {
                cursorPhotos.close();
            }
        }


        return observations;
    }

    public static String GetLatestObservation() {
        String query = "SELECT ObservationID FROM Observations ";
        query += "Order By ObservationDate Desc LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String id = "";
        if(cursor.moveToFirst()){
            do{
                id = cursor.getString(cursor.getColumnIndex("ObservationID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return id;
    }
}
