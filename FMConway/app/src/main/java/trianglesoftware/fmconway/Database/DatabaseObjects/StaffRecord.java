package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class StaffRecord {
    public String staffRecordID;
    public String staffID;
    public String recordName;
    public String contentType;
    public String imageLocation;

    public StaffRecord(){
    }

    private String getStaffID() { return this.staffID; }
    private String getStaffRecordID() { return this.staffRecordID; }
    private String getRecordName() { return this.recordName; }
    private String getContentType() { return this.contentType; }
    public String getImageLocation() { return this.imageLocation; }

    public void setStaffID(String staffID) { this.staffID = staffID; }
    public void setStaffRecordID(String staffRecordID) { this.staffRecordID = staffRecordID; }
    public void setRecordName(String recordName) { this.recordName = recordName; }
    public void setContentType(String contentType) { this.contentType = contentType; }
    public void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("StaffID", this.staffID);
            obj.put("StaffRecordID", this.staffRecordID);
            obj.put("RecordName", this.recordName);
            obj.put("ContentType", this.contentType);
            obj.put("ImageLocation", this.imageLocation);

        return obj;
    }

    //Staff Records
    public static void addStaffRecord(StaffRecord staffRecord)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("StaffRecordID", staffRecord.staffRecordID);
        values.put("StaffID", staffRecord.staffID);
        values.put("RecordName", staffRecord.recordName);
        values.put("ContentType", staffRecord.contentType);
        values.put("ImageLocation", staffRecord.imageLocation);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(staffRecord.staffRecordID)) {
            db.insert("StaffRecords", null, values);
        }
    }

    public static boolean exists(String staffRecordID) {
        String query = "select * from StaffRecords where StaffRecordID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { staffRecordID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllStaffRecords()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM StaffRecords");
    }

    public static String GetStaffRecordID(String staffRecordID) {
        String query = "SELECT StaffRecordID FROM StaffRecords where StaffRecordID = '" + staffRecordID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String ret = "";

        if(cursor.moveToFirst()){
            do{
                ret = cursor.getString(cursor.getColumnIndex("StaffRecordID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static StaffRecord GetStaffRecord(String staffRecordID) {
        String query = "SELECT * FROM StaffRecords where StaffRecordID = '" + staffRecordID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        StaffRecord sr = null;
        if(cursor.moveToFirst()){
            do{
                sr = new StaffRecord();
                sr.staffRecordID = cursor.getString(cursor.getColumnIndex("StaffRecordID"));
                sr.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                sr.recordName = cursor.getString(cursor.getColumnIndex("RecordName"));
                sr.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                sr.contentType = cursor.getString(cursor.getColumnIndex("ContentType"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return sr;
    }

    public static List<StaffRecord> GetStaffRecords(String staffID) {
        List<StaffRecord> staffRecords = new LinkedList<>();

        String query = "SELECT * FROM StaffRecords ";
        query += "WHERE StaffID = '" + staffID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        StaffRecord sr;
        if(cursor.moveToFirst()){
            do{
                sr = new StaffRecord();
                sr.staffRecordID = cursor.getString(cursor.getColumnIndex("StaffRecordID"));
                sr.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                sr.recordName = cursor.getString(cursor.getColumnIndex("RecordName"));
                sr.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                sr.contentType = cursor.getString(cursor.getColumnIndex("ContentType"));

                staffRecords.add(sr);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return staffRecords;
    }
}
