package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 01/03/2016.
 */
public class Shift {
    public String shiftID;
    public boolean dayShift;
    public String userID;
    public int shiftCompletionStatus;
    public boolean isCancelled;
    public Date shiftDate;
    public String shiftDetails;
    public String progress;
    public String activityType;
    public String shiftTypes;
    public String address;
    public String trafficManagementActivities;
    public String srwNumber;
    public String cancellationReason;
    public String projCode;
    public String addressLine1;
    public String addressLine2;
    public String borough;
    public String city;
    public String county;
    public String postcode;
    public boolean isOutOfHours; // not in shift entity
    public int orderNumber; // not in shift entity
    public Date updatedDate;

    public Shift() {
    }

    public Shift(String ShiftID, boolean DayShift, String UserID, Date ShiftDate, int ShiftCompletionStatus) {
        super();
        this.shiftID = ShiftID;
        this.dayShift = DayShift;
        this.userID = UserID;
        this.shiftDate = ShiftDate;
        this.shiftCompletionStatus = ShiftCompletionStatus;
    }

    public String getShiftID() {
        return this.shiftID;
    }

    public String getUserID() {
        return this.userID;
    }

    private int getShiftCompletionStatus() {
        return this.shiftCompletionStatus;
    }

    private Boolean getDayShift() {
        return this.dayShift;
    }

    private Boolean getIsCancelled() {
        return this.isCancelled;
    }

    public Date getShiftDate() {
        return this.shiftDate;
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if (this.shiftDate != null) {
            return dateFormat.format(this.shiftDate);
        } else {
            return "";
        }
    }

    private String getShiftAdapterDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        if (this.shiftDate != null) {
            return dateFormat.format(this.shiftDate);
        } else {
            return "";
        }
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }

    public void setDayShift(boolean dayShift) {
        this.dayShift = dayShift;
    }

    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setShiftCompletionStatus(int shiftCompletionStatus) {
        this.shiftCompletionStatus = shiftCompletionStatus;
    }

    public void setShiftDate(Date shiftDate) {
        this.shiftDate = shiftDate;
    }

    public JSONObject getJSONObject() {
        String progressTitleCase = FMConwayUtils.splitCamelCase(this.progress);

        JSONObject obj = new JSONObject();
        try {
            obj.put("ShiftID", this.shiftID);
            obj.put("DayShift", this.dayShift);
            obj.put("UserID", this.userID);
            obj.put("ShiftDate", this.shiftDate);
            obj.put("ShiftCompletionStatus", this.shiftCompletionStatus);
            obj.put("ShiftDetails", this.shiftDetails + " - Start Date: " + getShiftAdapterDateTime() + " - Progress: " + progressTitleCase);
            obj.put("Progress", this.progress);
            obj.put("ActivityType", this.activityType);
            obj.put("ShiftTypes", this.shiftTypes);
            obj.put("Address", this.address);
            obj.put("TrafficManagementActivities", this.trafficManagementActivities);
            obj.put("SRWNumber", this.srwNumber);
            obj.put("ProjCode", this.projCode);
            obj.put("AddressLine1", this.addressLine1);
            obj.put("AddressLine2", this.addressLine2);
            obj.put("Borough", this.borough);
            obj.put("City", this.city);
            obj.put("County", this.county);
            obj.put("Postcode", this.postcode);
        } catch (JSONException e) {
        }

        return obj;
    }

    // Shifts
    public static void addShift(Shift shift) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ShiftID", shift.shiftID);
        //values.put("DayShift", shift.dayShift);
        values.put("UserID", shift.userID);
        values.put("ShiftCompletionStatus", shift.shiftCompletionStatus);
        values.put("IsCancelled", shift.isCancelled);
        values.put("ShiftDetails", shift.shiftDetails);
        values.put("ShiftDate", shift.getTime());
        values.put("ShiftTypes", shift.shiftTypes);
        values.put("Address", shift.address);
        values.put("TrafficManagementActivities", shift.trafficManagementActivities);
        values.put("SRWNumber", shift.srwNumber);
        values.put("CancellationReason", shift.cancellationReason);
        values.put("ProjCode", shift.projCode);
        values.put("AddressLine1", shift.addressLine1);
        values.put("AddressLine2", shift.addressLine2);
        values.put("Borough", shift.borough);
        values.put("City", shift.city);
        values.put("County", shift.county);
        values.put("Postcode", shift.postcode);
        values.put("UpdatedDate", FMConwayUtils.getDateIsoString(shift.updatedDate));

        db.insert("Shifts", null, values);
    }

    public static void deleteAllShifts() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Shifts");
    }

    public static void deleteAllShiftsForUser(String userID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Shifts WHERE UserID = '" + userID + "'");
    }

    public static void deleteShiftForID(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Shifts WHERE ShiftID = '" + shiftID + "'");
    }

    public static String GetShiftForUser(String userID) {
        String query = "SELECT ShiftID FROM Shifts WHERE UserID = '" + userID + "' LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String shiftID = "";
        if (cursor.moveToFirst()) {
            do {
                shiftID = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return shiftID;
    }

    public static List<Shift> GetShiftsForUser(String userID) {
        List<Shift> shifts = new LinkedList<>();

        String query = "" +
                "select " +
                "s.*, " +
                "at.DisplayName as ActivityType, " +
                "a.IsOutOfHours, " +
                "a.OrderNumber " +
                "from " +
                "Shifts as s " +
                "inner join JobPacks as jp on jp.ShiftID = s.ShiftID " +
                "inner join Activities as a on a.JobPackID = jp.JobPackID " +
                "inner join ActivityTypes as at on at.ActivityTypeID = a.ActivityTypeID " +
                "where " +
                "s.UserID = ? " +
                "order by " +
                "s.ShiftDate";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{userID});

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Shift shift;
        if (cursor.moveToFirst()) {
            do {
                shift = new Shift();
                shift.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                shift.userID = userID;
                shift.shiftDetails = cursor.getString(cursor.getColumnIndex("ShiftDetails"));
                try {
                    String time = cursor.getString(cursor.getColumnIndex("ShiftDate"));
                    if (time != null) {
                        shift.shiftDate = format.parse(time);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                shift.activityType = cursor.getString(cursor.getColumnIndex("ActivityType"));
                shift.shiftTypes = cursor.getString(cursor.getColumnIndex("ShiftTypes"));
                shift.address = cursor.getString(cursor.getColumnIndex("Address"));
                shift.trafficManagementActivities = cursor.getString(cursor.getColumnIndex("TrafficManagementActivities"));
                shift.srwNumber = cursor.getString(cursor.getColumnIndex("SRWNumber"));
                shift.projCode = cursor.getString((cursor.getColumnIndex("ProjCode")));
                shift.addressLine1 = cursor.getString(cursor.getColumnIndex("AddressLine1"));
                shift.addressLine2 = cursor.getString(cursor.getColumnIndex("AddressLine2"));
                shift.borough = cursor.getString(cursor.getColumnIndex("Borough"));
                shift.city = cursor.getString(cursor.getColumnIndex("City"));
                shift.county = cursor.getString(cursor.getColumnIndex("County"));
                shift.postcode = cursor.getString(cursor.getColumnIndex("Postcode"));
                shift.isOutOfHours = cursor.getInt(cursor.getColumnIndex("IsOutOfHours")) > 0;
                shift.orderNumber = cursor.getInt(cursor.getColumnIndex("OrderNumber"));
                shift.updatedDate = new Date(cursor.getLong(cursor.getColumnIndex("UpdatedDate")));

                shifts.add(shift);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return shifts;
    }

    public static String GetShiftID(String briefingID) {
        String query = "SELECT jp.ShiftID FROM Briefings b ";
        query += "INNER JOIN JobPacks jp on jp.JobPackID = b.JobPackID ";
        query += "WHERE b.BriefingID = '" + briefingID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String shiftID = "";
        if (cursor.moveToFirst()) {
            do {
                shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return shiftID;
    }

    public static String GetShiftIDFromTask(String taskID) {
        String shiftID = null;

        String query = "" +
                "select " +
                "s.ShiftID " +
                "from " +
                "Tasks as t " +
                "inner join Activities as a on a.ActivityID = t.ActivityID " +
                "inner join JobPacks as jp on jp.JobPackID = a.JobPackID " +
                "inner join Shifts as s on s.ShiftID = jp.ShiftID " +
                "where " +
                "t.TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{taskID});

        if (cursor.moveToFirst()) {
            shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
        }

        cursor.close();

        return shiftID;
    }

    public static String GetShiftIDFromJobPack(String jobPackID) {
        String query = "SELECT ShiftID FROM JobPacks WHERE JobPackID = '" + jobPackID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String shiftID = "";

        if (cursor.moveToFirst()) {
            do {
                shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return shiftID;
    }

    public static Date GetShiftDate(String shiftID) {
        String query = "SELECT ShiftDate FROM Shifts WHERE ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Date shiftDate = null;
        if (cursor.moveToFirst()) {
            try {
                String shiftDateString = cursor.getString(cursor.getColumnIndex("ShiftDate"));
                if (shiftDateString != null) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    shiftDate = format.parse(shiftDateString);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        cursor.close();

        return shiftDate;
    }

    public static int GetShiftCompletionStatus(String shiftID) throws Exception {
        // the shift completion status is a bit all over of the place but here's what i've found by debugging
        // 0 = default
        // 1 = people confirmed
        // 2 = when enter brief
        // 3 = vehicles confirmed
        // 4 = not used (previously used when vehicle checklists completed)
        // 5 = people confirmed (previously included all staff signed brief too)

        String query = "SELECT ShiftCompletionStatus FROM Shifts ";
        query += "WHERE ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int shiftCompletionStatus = 0;
        if (cursor.moveToFirst()) {
            do {
                shiftCompletionStatus = Integer.parseInt(cursor.getString(cursor.getColumnIndex("ShiftCompletionStatus")));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return shiftCompletionStatus;
    }

    public static void SetCompletionStatus(String shiftID, int status) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("Update Shifts set ShiftCompletionStatus = " + status + " WHERE ShiftID = '" + shiftID + "'");
    }

    public static void CancelShift(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("UPDATE Shifts SET IsCancelled = 1 WHERE ShiftID = '" + shiftID + "'");
    }

    public static void UpdateCancellationReason(String shiftID, String cancellationReason) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("CancellationReason", cancellationReason);

        db.update("Shifts", contentValues, "ShiftID = ?", new String[]{shiftID});
    }

    public static String GetCancellationReason(String shiftID) {
        String query = "" +
                "select " +
                "CancellationReason " +
                "from " +
                "Shifts " +
                "where " +
                "ShiftID = ?";

        String cancellationReason = "";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{shiftID});

        if (cursor.moveToFirst()) {
            cancellationReason = cursor.getString(cursor.getColumnIndex("CancellationReason"));
        }

        cursor.close();

        return cancellationReason;
    }

    public static boolean CheckIfCancelled(String shiftID) {
        String query = "SELECT IsCancelled FROM Shifts ";
        query += "WHERE ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        boolean cancelled = false;

        db = Database.MainDB.getWritableDatabase();
        cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                cancelled = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsCancelled"))) > 0;
            } while (cursor.moveToNext());
        }

        cursor.close();

        return cancelled;
    }

    public static void updateShiftDate(String shiftID, Date shiftDate) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ShiftDate", FMConwayUtils.getDateIsoString(shiftDate));

        db.update("Shifts", contentValues, "ShiftID = ?", new String[]{shiftID});
    }

    public static Date getShiftUpdatedDate(String shiftID) {
        Date updatedDate = null;
        String query = "select UpdatedDate from Shifts where ShiftID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{shiftID});

        if (cursor.moveToFirst()) {
            updatedDate = FMConwayUtils.getCursorDate(cursor, "UpdatedDate");
        }

        cursor.close();

        return updatedDate;
    }

    public static void saveShiftUpdatedDate(String shiftID, Date updatedDate) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("UpdatedDate", FMConwayUtils.getDateIsoString(updatedDate));

        db.update("Shifts", contentValues, "ShiftID = ?", new String[]{shiftID});
    }

    public static String getShiftUserID(String shiftID) {
        String userID = null;
        String query = "select UserID from Shifts where ShiftID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{shiftID});

        if (cursor.moveToFirst()) {
            userID = cursor.getString(cursor.getColumnIndex("UserID"));
        }

        cursor.close();

        return userID;
    }
}
