package trianglesoftware.fmconway.Database.DatabaseObjects;

/**
 * Created by Adam.Patrick on 28/02/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayApp;

public class ShiftProgress {
    public String shiftID;
    public boolean sent;
    public String progress;
    public Date createdDate;

    public ShiftProgress()
    {}

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.createdDate != null){
            return dateFormat.format(this.createdDate);
        }
        else {
            return "";
        }
    }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("ShiftID", this.shiftID);
            obj.put("Sent", this.sent);
            obj.put("Progress", this.progress);
            obj.put("CreatedDate", this.getTime());
        } catch (JSONException e) { }

        return obj;
    }

    public static void addShiftProgress(ShiftProgress shiftProgress)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ShiftID", shiftProgress.shiftID);
        values.put("Progress", shiftProgress.progress);
        values.put("CreatedDate", shiftProgress.getTime());
        values.put("Sent", shiftProgress.sent);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(shiftProgress.shiftID, shiftProgress.progress)) {
            db.insert("ShiftProgress", null, values);
        }
    }

    public static boolean exists(String shiftID, String progress) {
        String query = "select * from ShiftProgress where ShiftID = ? and Progress = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, progress });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteShiftProgressForID(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ShiftProgress WHERE ShiftID = '" + shiftID + "'");
    }

    public static void deleteShiftProgress()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ShiftProgress");
    }

    public static ShiftProgress GetCurrentShiftProgress(String shiftID)
    {
        String query = "SELECT * FROM ShiftProgress ";
        query += "WHERE ShiftID = '" + shiftID + "' ORDER BY CreatedDate DESC LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        ShiftProgress sp = new ShiftProgress();
        if(cursor.moveToFirst()){
            do{
                sp.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                sp.progress = cursor.getString(cursor.getColumnIndex("Progress"));
                try {
                    String createdDate = cursor.getString(cursor.getColumnIndex("CreatedDate"));
                    if(createdDate != null)
                        sp.createdDate = (format.parse(createdDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int columnIndex = cursor.getColumnIndex("Sent");
                if (!cursor.isNull(columnIndex)) {
                    sp.sent = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Sent"))) > 0;
                }
                else
                {
                    sp.sent = false;
                }
            }while(cursor.moveToNext());
        }

        cursor.close();

        return sp;
    }

    public static JSONArray GetShiftProgressToSend()
    {
        JSONArray shiftProgresses = new JSONArray();

        String query = "SELECT * FROM ShiftProgress ";
        query += "WHERE Sent = 0 AND CreatedDate IS NOT NULL AND CreatedDate > '2000/01/01'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        JSONObject spObject;
        if(cursor.moveToFirst()){
            do{
                ShiftProgress sp = new ShiftProgress();
                sp.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                sp.progress = cursor.getString(cursor.getColumnIndex("Progress"));
                try {
                    String createdDate = cursor.getString(cursor.getColumnIndex("CreatedDate"));
                    if(createdDate != null)
                        sp.createdDate = (format.parse(createdDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int columnIndex = cursor.getColumnIndex("Sent");
                if (!cursor.isNull(columnIndex)) {
                    sp.sent = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Sent"))) > 0;
                }
                else
                {
                    sp.sent = false;
                }

                spObject = sp.getJSONObject();
                shiftProgresses.put(spObject);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return shiftProgresses;
    }

    public static void SetAsSent() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("UPDATE ShiftProgress SET Sent = 1 WHERE CreatedDate IS NOT NULL AND CreatedDate > '2000/01/01'");
    }

    public static void SetShiftProgressAsSent(String shiftID, String progress) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("UPDATE ShiftProgress SET Sent = 1 WHERE CreatedDate IS NOT NULL AND CreatedDate > '2000/01/01' AND ShiftID = '" + shiftID + "' AND Progress = '" + progress + "'");
    }

    public static void SetShiftProgress(String shiftID, String progress)
    {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            Date date = new Date();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            db.execSQL("UPDATE ShiftProgress SET CreatedDate = '" + dateFormat.format(date) + "', Sent = 0 WHERE ShiftID = '" + shiftID + "' AND Progress = '" + progress + "'");
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SetShiftProgress");
        }
    }

    public static void saveShiftProgress(ShiftProgress shiftProgress) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ShiftID", shiftProgress.shiftID);
        values.put("Progress", shiftProgress.progress);
        values.put("CreatedDate", shiftProgress.getTime());
        values.put("Sent", shiftProgress.sent);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(shiftProgress.shiftID, shiftProgress.progress)) {
            db.insert("ShiftProgress", null, values);
        } else {
            db.update("ShiftProgress", values, "ShiftID = ? and Progress = ?", new String[] { shiftProgress.shiftID, shiftProgress.progress });
        }
    }

    // ShiftProgress records will always exist but may or may not have a CreatedDate, this is what this function checks
    public static boolean DoesProgressExist(String shiftID, String progress)
    {
        boolean exists = false;

        try
        {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            String query = "SELECT * FROM ShiftProgress WHERE ShiftID = '" + shiftID + "' AND Progress = '" + progress +
                    "' AND CreatedDate IS NOT NULL AND CreatedDate > '2000/01/01'";

            Cursor cursor = db.rawQuery(query, null);

            if(cursor.moveToFirst()){
                do{
                    exists = true;
                }while(cursor.moveToNext());
            }

            cursor.close();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DoesProgressExist");
        }

        return exists;
    }

    public static void SyncProgress() {
        Context context = FMConwayApp.getContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            String userName = (sp.getString("Username", ""));
            String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
            String url = context.getResources().getString(R.string.QUERY_URL);
            DataSync sync = new DataSync("", sp.getString("UserID", ""), url, encodedHeader);

            sync.PushShiftProgress(new DataSync.PushDataListener() {
                @Override
                public void onComplete(boolean errors) {
                    //if (!errors) {
                    //    ShiftProgress.SetAsSent();
                    //}
                }

                @Override
                public void onError(Throwable t) {
                    //ErrorLog.CreateError(t, "SyncProgress");
                }
            });
        }
    }

    public static void PushShiftProgressStarted(ShiftProgressStarted shiftProgress, final DataSync.PushDataListenerProgressResponse listener) {
        Context context = FMConwayApp.getContext();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String userName = (sp.getString("Username", ""));
        String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        String url = context.getResources().getString(R.string.QUERY_URL);
        DataSync sync = new DataSync("", sp.getString("UserID", ""), url, encodedHeader);

        sync.PushShiftProgressStarted(shiftProgress, listener);
    }
}
