package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class Customer {
    public String customerID;
    public String customerName;
    public String accountNumber;
    public String customerCode;
    public String phoneNumber;
    public boolean isActive;

    public Customer() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("CustomerID", this.customerID);
            obj.put("CustomerName", this.customerName);
            obj.put("AccountNumber", this.accountNumber);
            obj.put("CustomerCode", this.customerCode);
            obj.put("PhoneNumber", this.phoneNumber);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "Customer - getJSONObject");
        }
        return obj;
    }

    protected static Customer cursorToModel(Cursor c) {
        Customer contact = new Customer();
        contact.customerID = c.getString(c.getColumnIndex("CustomerID"));
        contact.customerName = c.getString(c.getColumnIndex("CustomerName"));
        contact.accountNumber = c.getString(c.getColumnIndex("AccountNumber"));
        contact.customerCode = c.getString(c.getColumnIndex("CustomerCode"));
        contact.phoneNumber = c.getString(c.getColumnIndex("PhoneNumber"));
        contact.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return contact;
    }

    public static Customer get(String customerID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Customer model = null;
        Cursor c = db.rawQuery("SELECT * FROM Customers WHERE CustomerID = ?", new String[] { customerID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<Customer> getAllForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<Customer> models = new LinkedList<>();

        Cursor c = db.rawQuery("SELECT c.* FROM Customers as c INNER JOIN Activities as a on a.CustomerID = c.CustomerID WHERE c.IsActive = 1 AND a.ActivityID = ? ORDER BY CustomerName", new String[]{activityID});

        if (c.moveToFirst()) {
            do {
                Customer model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static List<Customer> getLookupItems() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<Customer> models = new LinkedList<>();

        Customer defaultItem = new Customer();
        defaultItem.customerName = "";
        models.add(defaultItem);

        Cursor c = db.rawQuery("SELECT * FROM Customers WHERE IsActive = 1 ORDER BY CustomerName", null);

        if (c.moveToFirst()) {
            do {
                Customer model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(Customer model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("CustomerID", model.customerID);
            cv.put("CustomerName", model.customerName);
            cv.put("AccountNumber", model.accountNumber);
            cv.put("CustomerCode", model.customerCode);
            cv.put("PhoneNumber", model.phoneNumber);
            cv.put("IsActive", model.isActive);

            if (get(model.customerID) != null) {
                db.update("Customers", cv, "CustomerID = ?", new String[]{model.customerID});
            } else {
                db.insertOrThrow("Customers", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "Customer - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return customerName;
    }
}
