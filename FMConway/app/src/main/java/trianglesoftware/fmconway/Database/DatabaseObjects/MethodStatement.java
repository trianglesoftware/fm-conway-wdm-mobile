package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class MethodStatement {
    public String methodStatementID;
    public String documentID;
    public String methodStatementTitle;
    public String imageLocation;
    public String documentName;
    public String contentType;
    public boolean isActive;

    public MethodStatement() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("MethodStatementID", this.methodStatementID);
            obj.put("DocumentID", this.documentID);
            obj.put("MethodStatementTitle", this.methodStatementTitle);
            obj.put("ImageLocation", this.imageLocation);
            obj.put("DocumentName", this.documentName);
            obj.put("ContentType", this.contentType);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "MethodStatement - getJSONObject");
        }
        return obj;
    }

    protected static MethodStatement cursorToModel(Cursor c) {
        MethodStatement methodStatement = new MethodStatement();
        methodStatement.methodStatementID = c.getString(c.getColumnIndex("MethodStatementID"));
        methodStatement.documentID = c.getString(c.getColumnIndex("DocumentID"));
        methodStatement.methodStatementTitle = c.getString(c.getColumnIndex("MethodStatementTitle"));
        methodStatement.imageLocation = c.getString(c.getColumnIndex("ImageLocation"));
        methodStatement.documentName = c.getString(c.getColumnIndex("DocumentName"));
        methodStatement.contentType = c.getString(c.getColumnIndex("ContentType"));
        methodStatement.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return methodStatement;
    }

    public static MethodStatement get(String methodStatementID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        MethodStatement model = null;
        Cursor c = db.rawQuery("SELECT * FROM MethodStatements WHERE MethodStatementID = ?", new String[] { methodStatementID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<MethodStatement> getAll() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<MethodStatement> models = new LinkedList<>();

        Cursor c = db.rawQuery("SELECT * FROM MethodStatements WHERE IsActive = 1 ORDER BY MethodStatementTitle", null);

        if (c.moveToFirst()) {
            do {
                MethodStatement model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    /*public static List<MethodStatement> getAllForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<MethodStatement> models = new LinkedList<>();

        Cursor c = db.rawQuery("SELECT co.* FROM Contacts as co INNER JOIN Customers as c on c.CustomerID = co.CustomerID INNER JOIN Activities as a on a.CustomerID = c.CustomerID WHERE co.IsActive = 1 AND a.ActivityID = ? ORDER BY ContactName", new String[]{activityID});

        if (c.moveToFirst()) {
            do {
                MethodStatement model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }*/

    public static void save(MethodStatement model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("MethodStatementID", model.methodStatementID);
            cv.put("DocumentID", model.documentID);
            cv.put("MethodStatementTitle", model.methodStatementTitle);
            cv.put("ImageLocation", model.imageLocation);
            cv.put("DocumentName", model.documentName);
            cv.put("ContentType", model.contentType);
            cv.put("IsActive", model.isActive);

            if (get(model.methodStatementID) != null) {
                db.update("MethodStatements", cv, "MethodStatementID = ?", new String[]{model.methodStatementID});
            } else {
                db.insertOrThrow("MethodStatements", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "MethodStatement - save");
        }
    }

}
