package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Checklist {
    public String checklistID;
    public String name;
    public boolean isMaintenance;
    public boolean isPreHire;
    public boolean isVms;
    public boolean isBattery;
    public boolean isPreHireBattery;
    public boolean isPostHireBattery;
    public boolean isHighSpeed;
    public boolean isLowSpeed;
    public boolean isJettingPermitToWork;
    public boolean isJettingRiskAssessment;
    public boolean isJettingNewRiskAssessment;
    public boolean isTankeringPermitToWork;
    public boolean isTankeringRiskAssessment;
    public boolean isCCTVRiskAssessment;

    public Checklist() {
    }

    public Checklist(String ChecklistID, String Name) {
        super();
        this.checklistID = ChecklistID;
        this.name = Name;
    }

    public String getChecklistID() {
        return this.checklistID;
    }

    private String getName() {
        return this.name;
    }

    private boolean getIsMaintenance() {
        return this.isMaintenance;
    }

    private boolean getIsPreHire() {
        return this.isPreHire;
    }

    private boolean getIsVms() {
        return this.isVms;
    }

    public void setChecklistID(String checklistID) {
        this.checklistID = checklistID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaintenance(boolean isMaintenance) {
        this.isMaintenance = isMaintenance;
    }

    public void setPreHire(boolean isPreHire) {
        this.isPreHire = isPreHire;
    }

    public void setVms(boolean isVms) {
        this.isVms = isVms;
    }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("ChecklistID", this.checklistID);
            obj.put("Name", this.name);
            obj.put("IsMaintenance", this.isMaintenance);
            obj.put("IsPreHire", this.isPreHire);
            obj.put("IsVms", this.isVms);
            obj.put("IsBattery", this.isBattery);
            obj.put("IsPreHireBattery", this.isPreHireBattery);
            obj.put("IsPreHireBattery", this.isPostHireBattery);
            obj.put("IsHighSpeed", this.isHighSpeed);
            obj.put("IsLowSpeed", this.isLowSpeed);
            obj.put("IsJettingPermitToWork", this.isJettingPermitToWork);
            obj.put("IsJettingRiskAssessment", this.isJettingRiskAssessment);
            obj.put("IsTankeringPermitToWork", this.isTankeringPermitToWork);
            obj.put("IsTankeringRiskAssessment", this.isTankeringRiskAssessment);
            obj.put("IsCCTVRiskAssessment", this.isCCTVRiskAssessment);
            obj.put("IsJettingNewRiskAssessment", this.isJettingNewRiskAssessment);
        } catch (JSONException e) {
        }

        return obj;
    }

    // Checklists
    public static void addChecklist(Checklist checklist) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ChecklistID", checklist.checklistID);
        values.put("Name", checklist.name);
        values.put("IsMaintenance", checklist.isMaintenance);
        values.put("IsPreHire", checklist.isPreHire);
        values.put("IsVms", checklist.isVms);
        values.put("IsBattery", checklist.isBattery);
        values.put("IsPreHireBattery", checklist.isPreHireBattery);
        values.put("IsPostHireBattery", checklist.isPostHireBattery);
        values.put("IsHighSpeed", checklist.isHighSpeed);
        values.put("IsLowSpeed", checklist.isLowSpeed);
        values.put("IsJettingPermitToWork", checklist.isJettingPermitToWork);
        values.put("IsJettingRiskAssessment", checklist.isJettingRiskAssessment);
        values.put("IsTankeringPermitToWork", checklist.isTankeringPermitToWork);
        values.put("IsTankeringRiskAssessment", checklist.isTankeringRiskAssessment);
        values.put("IsCCTVRiskAssessment", checklist.isCCTVRiskAssessment);
        values.put("IsJettingNewRiskAssessment", checklist.isJettingNewRiskAssessment);

        db.insert("Checklists", null, values);
    }

    public static void deleteAllChecklists() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Checklists");
    }

    public static List<Checklist> GetChecklistsForVehicle(String vehicleID) {
        List<Checklist> checklists = new LinkedList<>();

        String query = "SELECT c.* FROM Checklists c ";
        query += "INNER JOIN VehicleTypeChecklists vtc on vtc.ChecklistID = c.ChecklistID ";
        query += "INNER JOIN Vehicles v on v.VehicleTypeID = vtc.VehicleTypeID ";
        query += "WHERE v.VehicleID = '" + vehicleID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                c = new Checklist();
                c.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                c.name = cursor.getString(cursor.getColumnIndex("Name"));
                c.isMaintenance = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsMaintenance"))) > 0;
                c.isPreHire = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsPreHire"))) > 0;
                c.isBattery = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsBattery"))) > 0;
                c.isVms = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsVms"))) > 0;

                checklists.add(c);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklists;
    }

    public static String getPreHireChecklistID() {
        String checklistID = null;
        String query = "select c.ChecklistID from Checklists as c where c.isPreHire = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static String getVmsChecklistID() {
        String checklistID = null;
        String query = "select c.ChecklistID from Checklists as c where c.isVms = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static String getBatteryChecklistID() {
        String checklistID = null;
        String query = "select c.ChecklistID from Checklists as c where c.IsBattery = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static String getJettingNewRiskAssessmentID() {
        String checklistID = null;
        String query = "select c.ChecklistID from Checklists as c where c.IsJettingNewRiskAssessment = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static String getPreHireBatteryChecklistID() {
        String checklistID = null;
        String query = "select c.ChecklistID from Checklists as c where c.IsPreHireBattery = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static String getPostHireBatteryChecklistID() {
        String checklistID = null;
        String query = "select c.ChecklistID from Checklists as c where c.IsPostHireBattery = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist c;
        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static boolean isRiskAssessmentChecklist(String checklistID) {
        boolean isRiskAssessment = false;

        String query = "" +
                "select " +
                "c.ChecklistID " +
                "from " +
                "Checklists as c " +
                "where " +
                "(c.isHighSpeed = 1 or c.isLowSpeed = 1 or c.IsPostHireBattery) and " +
                "c.ChecklistID = ? ";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{checklistID});
        isRiskAssessment = cursor.moveToFirst();

        cursor.close();

        return isRiskAssessment;
    }
    public static boolean isBatteryChecklist(String checklistID) {
        boolean isBatteryCheck = false;

        String query = "" +
                "select " +
                "c.ChecklistID " +
                "from " +
                "Checklists as c " +
                "where " +
                "(c.IsPostHireBattery) and " +
                "c.ChecklistID = ? ";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{checklistID});
        isBatteryCheck = cursor.moveToFirst();

        cursor.close();

        return isBatteryCheck;
    }

    public static List<String> getRAChecklistNames() {
        List<String> list = new ArrayList<>();
        String query = "SELECT Name FROM Checklists WHERE IsHighSpeed = 1 OR IsLowSpeed = 1";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                String checklist = cursor.getString(cursor.getColumnIndex("Name"));
                list.add(checklist);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return list;
    }

    public static String getRAChecklistFromName(String name) {
        String query = "SELECT ChecklistID FROM Checklists WHERE Name = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{name});

        String checklistID = null;

        if (cursor.moveToFirst()) {
            do {
                checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return checklistID;
    }

    public static Checklist getChecklist(String checklistID) {
        String query = "SELECT c.* FROM Checklists c ";
        query += "WHERE c.ChecklistID = '" + checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist checklist = null;
        if (cursor.moveToFirst()) {
            checklist = new Checklist();
            checklist.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            checklist.name = cursor.getString(cursor.getColumnIndex("Name"));
            checklist.isMaintenance = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsMaintenance"))) > 0;
            checklist.isPreHire = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsPreHire"))) > 0;
            checklist.isVms = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsVms"))) > 0;
            checklist.isBattery = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsBattery"))) > 0;
            checklist.isPreHireBattery = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsPreHireBattery"))) > 0;
            checklist.isPostHireBattery = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsPostHireBattery"))) > 0;
            checklist.isHighSpeed = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsHighSpeed"))) > 0;
            checklist.isLowSpeed = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsLowSpeed"))) > 0;
            checklist.isJettingPermitToWork = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsJettingPermitToWork"))) > 0;
            checklist.isJettingRiskAssessment = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsJettingRiskAssessment"))) > 0;
            checklist.isTankeringPermitToWork = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsTankeringPermitToWork"))) > 0;
            checklist.isTankeringRiskAssessment = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsTankeringRiskAssessment"))) > 0;
            checklist.isCCTVRiskAssessment = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsCCTVRiskAssessment"))) > 0;
            checklist.isJettingNewRiskAssessment = Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsJettingNewRiskAssessment"))) > 0;
        }

        cursor.close();

        return checklist;
    }
}
