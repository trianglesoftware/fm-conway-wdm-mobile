package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 18/03/2016.
 */
public class MaintenanceCheck {
    public String maintenanceCheckID;
    public String activityID;
    public String description;
    public Date time;

    public MaintenanceCheck() {
    }

    public String getMaintenanceCheckID() {
        return this.maintenanceCheckID;
    }

    private String getActivityID() {
        return this.activityID;
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.time != null){
            return dateFormat.format(this.time);
        }
        else {
            return "";
        }
    }

    public String getDescription() { return this.description; }

    public void setMaintenanceCheckID(String maintenanceCheckID) {
        this.maintenanceCheckID = maintenanceCheckID;
    }

    public void setActivityID(String activityID) {
        this.activityID = activityID;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("MaintenanceCheckID", this.maintenanceCheckID);
            obj.put("ActivityID", this.activityID);
            obj.put("Description", this.description);

            obj.put("Time", getTime());

        return obj;
    }

    //Maintenance Checks
    public static void AddMaintenanceCheck(MaintenanceCheck maintenanceCheck) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();

        if (maintenanceCheck.maintenanceCheckID != null) {
            values.put("MaintenanceCheckID", maintenanceCheck.maintenanceCheckID);
        } else {
            values.put("MaintenanceCheckID", UUID.randomUUID().toString());
        }

        values.put("ActivityID", maintenanceCheck.activityID);
        values.put("Time", maintenanceCheck.getTime());
        values.put("Description", maintenanceCheck.description);

        db.insert("MaintenanceChecks", null, values);
    }

    public static void deleteAllMaintenanceChecks() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceChecks");
    }

    public static void deleteMaintenanceChecksForActivity(String activityID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckAnswers WHERE ActivityID = '" + activityID + "'");

        db.execSQL("DELETE FROM MaintenanceChecks WHERE ActivityID = '" + activityID + "'");
    }

    public static void deleteMaintenanceCheck(int maintenanceCheckID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceChecks WHERE MaintenanceCheckID = " + maintenanceCheckID);
    }

    public static boolean checkMaintenanceCheckExists(String activityID) {
        boolean ret = false;

        String query = "SELECT * FROM MaintenanceChecks ";
        query += "WHERE ActivityID = '" + activityID + "'";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            ret = true;
        }
        cursor.close();

        return ret;
    }

    public static List<MaintenanceCheck> GetMaintenanceChecksForActivity(String activityID) {
        List<MaintenanceCheck> maintenanceChecks = new LinkedList<>();

        String query = "SELECT * FROM MaintenanceChecks ";
        query += "WHERE ActivityID = '" + activityID;
        query += "' ORDER BY Time DESC";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        MaintenanceCheck mc;
        if(cursor.moveToFirst()){
            do{
                mc = new MaintenanceCheck();
                mc.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                mc.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                mc.description = cursor.getString(cursor.getColumnIndex("Description"));
                try {
                    String time = cursor.getString(cursor.getColumnIndex("Time"));
                    if(time != null)
                        mc.setTime(format.parse(time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                maintenanceChecks.add(mc);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return maintenanceChecks;
    }

    public static MaintenanceCheck GetMaintenanceCheck(String maintenanceCheckID) {
        String query = "SELECT * FROM MaintenanceChecks ";
        query += "WHERE MaintenanceCheckID = '" + maintenanceCheckID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        MaintenanceCheck mc = null;
        if(cursor.moveToFirst()){
            do{
                mc = new MaintenanceCheck();
                mc.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                mc.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                mc.description = cursor.getString(cursor.getColumnIndex("Description"));
                try {
                    String time = cursor.getString(cursor.getColumnIndex("Time"));
                    if(time != null)
                        mc.setTime(format.parse(time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }while(cursor.moveToNext());
        }

        cursor.close();

        return mc;
    }

    public static void UpdateMaintenanceCheck(String maintenanceCheckID, String description) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE MaintenanceChecks SET Description = '" + description + "' WHERE MaintenanceCheckID = " + maintenanceCheckID);
        //db.execSQL("UPDATE MaintenanceChecks SET Description = ? WHERE MaintenanceCheckID = " + maintenanceCheckID, new String[]{description});

        SQLiteStatement statement = db.compileStatement("UPDATE MaintenanceChecks SET Description = ? WHERE MaintenanceCheckID = ?");
        statement.bindString(1,description);
        statement.bindString(2,maintenanceCheckID);
        statement.execute();
        statement.close();
    }

    public static JSONArray GetMaintenanceChecksToSend(String shiftID) throws Exception {
        JSONArray maintenanceChecks = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT mc.* FROM MaintenanceChecks mc ";
        query += "JOIN Activities a on a.ActivityID = mc.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor cursorAnswers = null;
        Cursor cursorDetails = null;

        JSONObject aObject;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    MaintenanceCheck mc = new MaintenanceCheck();
                    mc.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                    mc.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                    mc.description = cursor.getString(cursor.getColumnIndex("Description"));
                    try {
                        String time = cursor.getString(cursor.getColumnIndex("Time"));
                        if (time != null)
                            mc.setTime(format.parse(time));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    aObject = mc.getJSONObject();

                    //Add Answers List
                    query = "SELECT mca.* FROM MaintenanceCheckAnswers mca ";
                    query += "WHERE mca.MaintenanceCheckID = '" + cursor.getString(cursor.getColumnIndex("MaintenanceCheckID")) + "'";

                    cursorAnswers = db.rawQuery(query, null);
                    JSONArray mcaObjects = new JSONArray();
                    if (cursorAnswers.moveToFirst()) {
                        do {
                            MaintenanceCheckAnswer mca = new MaintenanceCheckAnswer();
                            mca.maintenanceCheckID = cursorAnswers.getString(cursorAnswers.getColumnIndex("MaintenanceCheckID"));
                            mca.checklistQuestionID = cursorAnswers.getString(cursorAnswers.getColumnIndex("ChecklistQuestionID"));

                            int columnIndex = cursorAnswers.getColumnIndex("Answer");
                            if (!cursorAnswers.isNull(columnIndex)) {
                                mca.answer = Integer.parseInt(cursorAnswers.getString(cursorAnswers.getColumnIndex("Answer"))) > 0;
                            }

                            mcaObjects.put(mca.getJSONObjectToSend());

                        } while (cursorAnswers.moveToNext());

                    }
                    aObject.put("Answers", mcaObjects);

                    //Add Details List
                    query = "SELECT mcd.* FROM MaintenanceCheckDetails mcd ";
                    query += "WHERE mcd.MaintenanceCheckID = '" + cursor.getString(cursor.getColumnIndex("MaintenanceCheckID")) + "'";

                    cursorDetails = db.rawQuery(query, null);
                    JSONArray mcdObjects = new JSONArray();
                    if (cursorDetails.moveToFirst()) {
                        do {
                            MaintenanceCheckDetail mcd = new MaintenanceCheckDetail();
                            mcd.maintenanceCheckID = cursorDetails.getString(cursorDetails.getColumnIndex("MaintenanceCheckID"));
                            mcd.checklistQuestionID = cursorDetails.getString(cursorDetails.getColumnIndex("ChecklistQuestionID"));
                            mcd.description = cursorDetails.getString(cursorDetails.getColumnIndex("Description"));

                            mcdObjects.put(mcd.getJSONObject());

                        } while (cursorDetails.moveToNext());

                    }
                    aObject.put("Details", mcdObjects);

                    maintenanceChecks.put(aObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetMaintenanceChecksToSend");
            throw e;
        }
        finally {
            cursor.close();
            if (cursorAnswers != null) {
                cursorAnswers.close();
            }
            if (cursorDetails != null) {
                cursorDetails.close();
            }
        }

        return maintenanceChecks;
    }

    public static MaintenanceCheck GetLatestCheck() {
        String query = "SELECT MaintenanceCheckID FROM MaintenanceChecks ";
        query += "ORDER BY Time DESC LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        MaintenanceCheck mc = null;
        if(cursor.moveToFirst()){
            do{
                mc = new MaintenanceCheck();
                mc.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return mc;
    }

    // ID can be activityID or taskID (by setting 2nd param)
    public static boolean CheckLatestTime(String ID, boolean activity) {
        String activityID;
        if (activity) {
            activityID = ID;
        } else {
            activityID = Activity.getActivityIdByTaskID(ID);
        }

        String query = "";
        query += "SELECT Time FROM MaintenanceChecks mc ";
        query += "WHERE mc.ActivityID = ? ";
        query += "ORDER BY Time DESC LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { activityID });

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date latest = null;
        if (cursor.moveToFirst()) {
            do {
                try {
                    String time = cursor.getString(cursor.getColumnIndex("Time"));
                    if (time != null)
                        latest = format.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        cursor.close();
        
        int remindIn = Activity.getMaintenanceChecklistReminderInMinutes(activityID);
        
        if (remindIn > 0) {
            if (latest != null) {
                Date now = new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(now);
                calendar.add(Calendar.MINUTE, -remindIn);

                if (calendar.getTime().after(latest)) {
                    return false; // reminder period elapsed
                } else {
                    return true; // no checklist necessary atm
                }
            } else {
                return false; // first entry due
            }
        } else {
            return true; // no reminder (remindIn = 0 is inf)
        }
    }
}
