package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class ShiftVehicle {
    public String shiftID;
    public String staffID;
    public String vehicleID;
    public String userID;
    public boolean loadingSheetCompleted;
    public int startMileage;
    public int endMileage;
    public boolean notOnShift;
    public String reasonNotOn;

    public ShiftVehicle(){
    }

    public ShiftVehicle(String ShiftID, String VehicleID)
    {
        super();
        this.shiftID = ShiftID;
        this.vehicleID = VehicleID;
    }

    public String getShiftID() { return this.shiftID; }
    public String getVehicleID() { return this.vehicleID; }
    public String getStaffID() { return this.staffID;}
    public String getUserID() {return this.userID;}
    public boolean isLoadingSheetCompleted() { return this.loadingSheetCompleted; }
    public int getStartMileage() { return this.startMileage; }
    public int getEndMileage() { return this.endMileage; }
    public boolean getNotOnShift() { return this.notOnShift; }
    public String getReasonNotOn() { return this.reasonNotOn; }

    public void setShiftID(String shiftID) { this.shiftID = shiftID; }
    public void setStaffID(String staffID) { this.staffID = staffID; }
    public void setVehicleID(String vehicleID) { this.vehicleID = vehicleID; }
    public void setUserID(String userID) { this.userID = userID;}
    public void setLoadingSheetCompleted(boolean loadingSheetCompleted) { this.loadingSheetCompleted = loadingSheetCompleted; }
    public void setStartMileage(int startMileage) { this.startMileage = startMileage; }
    public void setEndMileage(int endMileage) { this.endMileage = endMileage; }
    public void setNotOnShift(boolean notOnShift) {
        this.notOnShift = notOnShift;
    }
    public void setReasonNotOn(String reasonNotOn) {
        this.reasonNotOn = reasonNotOn;
    }

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("ShiftID", this.shiftID);
            obj.put("VehicleID", this.vehicleID);
            obj.put("StaffID", this.staffID);
            obj.put("LoadingSheetCompleted", this.loadingSheetCompleted);
            obj.put("StartMileage", this.startMileage);
            obj.put("EndMileage", this.endMileage);
            obj.put("NotOnShift", this.notOnShift);
            obj.put("ReasonNotOn", this.reasonNotOn);


        return obj;
    }

    // Job Pack Vehicles
    public static void addShiftVehicles(ShiftVehicle shiftVehicle)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ShiftID", shiftVehicle.shiftID);
        values.put("VehicleID", shiftVehicle.vehicleID);
        values.put("StaffID", shiftVehicle.staffID);
        values.put("UserID", shiftVehicle.userID);
        values.put("StartMileage", shiftVehicle.startMileage);
        values.put("EndMileage", shiftVehicle.endMileage);
        values.put("LoadingSheetCompleted", shiftVehicle.loadingSheetCompleted);
        values.put("NotOnShift", shiftVehicle.notOnShift);
        values.put("ReasonNotOn", shiftVehicle.reasonNotOn);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(shiftVehicle.shiftID, shiftVehicle.vehicleID)) {
            db.insert("ShiftVehicle", null, values);
        }
    }

    public static boolean exists(String shiftID, String vehicleID) {
        String query = "select * from ShiftVehicle where ShiftID = ? and VehicleID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, vehicleID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllShiftVehicles()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM ShiftVehicle");
    }

    public static void deleteShiftVehiclesForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ShiftVehicle WHERE UserID = '" + userID + "'");
    }

    public static void deleteShiftVehicles(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ShiftVehicle WHERE ShiftID = '" + shiftID + "'");
    }

    public static boolean CheckLoadingSheetIsConfirmed(String shiftID, String vehicleID) {
        String query = "SELECT LoadingSheetCompleted FROM ShiftVehicle where ShiftID = '" + shiftID + "' AND VehicleID = '" + vehicleID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        boolean ret = false;

        if(cursor.moveToFirst()){
            do{
                ret = cursor.getInt(cursor.getColumnIndex("LoadingSheetCompleted")) > 0;
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static void CompleteLoadingSheet(String shiftID, String vehicleID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("Update ShiftVehicle Set LoadingSheetCompleted = 1 Where ShiftID = '" + shiftID + "' AND VehicleID = '" + vehicleID + "'");
    }

    public static List<Vehicle> GetVehiclesForShift(String shiftID, String userID)
    {
        List<Vehicle> vehicles = new LinkedList<>();

        String query = "SELECT v.*, s.Forename, s.Surname, sv.StaffID, sv.StartMileage FROM Vehicles v INNER JOIN ShiftVehicle sv on sv.VehicleID = v.VehicleID LEFT JOIN Staff s on s.StaffID = sv.StaffID where sv.NotOnShift = 0 AND sv.ShiftID = '" + shiftID + "' AND sv.UserID = '" + userID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Vehicle v;
        if(cursor.moveToFirst()){
            do{
                v = new Vehicle();
                v.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                v.make = cursor.getString(cursor.getColumnIndex("Make"));
                v.model = cursor.getString(cursor.getColumnIndex("Model"));
                v.registration = cursor.getString(cursor.getColumnIndex("Registration"));
                v.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                v.vehicleTypeID = cursor.getString(cursor.getColumnIndex("VehicleTypeID"));
                v.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                v.startMileage = cursor.getInt(cursor.getColumnIndex("StartMileage"));

                String forename = cursor.getString(cursor.getColumnIndex("Forename"));
                String surname = cursor.getString(cursor.getColumnIndex("Surname"));
                
                // we don't have forename & surname in backend only full name
                // both forename = full name and surname = full name so just use either
                if (!TextUtils.isEmpty(forename)) { 
                    v.driver = forename;
                } else if (!TextUtils.isEmpty(surname)) {
                    v.driver = surname;
                } else {
                    v.driver = "";
                }

                vehicles.add(v);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return vehicles;
    }

    public static List<Vehicle> GetVehiclesForToday(String userID)
    {
        List<Vehicle> vehicles = new LinkedList<>();

        String query = "SELECT v.*, s.Forename, s.Surname, sv.StaffID FROM Vehicles v INNER JOIN ShiftVehicle sv on sv.VehicleID = v.VehicleID LEFT JOIN Staff s on s.StaffID = sv.StaffID where sv.NotOnShift = 0 AND sv.UserID = '" + userID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Vehicle v;
        if(cursor.moveToFirst()){
            do{
                v = new Vehicle();
                v.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                v.make = cursor.getString(cursor.getColumnIndex("Make"));
                v.model = cursor.getString(cursor.getColumnIndex("Model"));
                v.registration = cursor.getString(cursor.getColumnIndex("Registration"));
                v.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                v.vehicleTypeID = cursor.getString(cursor.getColumnIndex("VehicleTypeID"));

                String forename = cursor.getString(cursor.getColumnIndex("Forename"));
                String surname = cursor.getString(cursor.getColumnIndex("Surname"));

                if (forename == null || surname == null)
                {
                    forename = "";
                    surname = "";
                }

                v.driver = forename + ' ' + surname;

                vehicles.add(v);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return vehicles;
    }

    public static ShiftVehicle GetShiftVehicle(String shiftID, String vehicleID, String userID) {

        String query = "SELECT * FROM ShiftVehicle where ShiftID = '" + shiftID + "' AND VehicleID = '" + vehicleID + "' AND UserID = '" + userID + "'";

        if (Objects.equals(shiftID, ""))
        {
            query = "SELECT * FROM ShiftVehicle where VehicleID = '" + vehicleID + "' AND UserID = '" + userID + "'";
        }

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ShiftVehicle sv = null;
        if(cursor.moveToFirst()){
            do{
                sv = new ShiftVehicle();
                sv.startMileage = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StartMileage")));
                sv.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                sv.endMileage = Integer.parseInt(cursor.getString(cursor.getColumnIndex("EndMileage")));
                sv.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                sv.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return sv;
    }

    public static void ChangeMileage(boolean setStartMileage, int startMileage, int endMileage, String shiftID, String vehicleID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        if(setStartMileage)
        {
            db.execSQL("UPDATE ShiftVehicle SET StartMileage = " + startMileage + " WHERE ShiftID = '" + shiftID + "' AND VehicleID = '" + vehicleID + "'");
        }
        else{
            db.execSQL("UPDATE ShiftVehicle SET EndMileage = " + endMileage + " WHERE ShiftID = '" + shiftID + "' AND VehicleID = '" + vehicleID + "'");
        }
    }

    public static JSONObject GetShiftVehiclesToSend(String shiftID, String userID, boolean restrictedUpdate) throws Exception{
        JSONObject model = new JSONObject();
        model.put("RestrictedUpdate", restrictedUpdate);
        
        JSONArray shiftVehicles = new JSONArray();
        model.put("ShiftVehicles", shiftVehicles);

        String query = "SELECT sv.* FROM ShiftVehicle sv ";
        query += "WHERE sv.ShiftID = '" + shiftID + "' AND sv.VehicleID is not null and sv.UserID = '" + userID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject jpvObject;
            if (cursor.moveToFirst()) {
                do {
                    ShiftVehicle jpv = new ShiftVehicle();
                    jpv.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    jpv.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                    jpv.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                    jpv.startMileage = Integer.parseInt(cursor.getString(cursor.getColumnIndex("StartMileage")));
                    jpv.endMileage = Integer.parseInt(cursor.getString(cursor.getColumnIndex("EndMileage")));
                    jpv.loadingSheetCompleted = Integer.parseInt(cursor.getString(cursor.getColumnIndex("LoadingSheetCompleted"))) > 0;
                    jpv.notOnShift = Integer.parseInt(cursor.getString(cursor.getColumnIndex("NotOnShift"))) > 0;
                    jpv.reasonNotOn = cursor.getString(cursor.getColumnIndex("ReasonNotOn"));
                    
                    jpvObject = jpv.getJSONObject();
                    shiftVehicles.put(jpvObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetShiftVehiclesToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return model;
    }

    public static void UpdateShiftVehicle(ShiftVehicle currentShiftVehicle) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ShiftID", currentShiftVehicle.shiftID);
        values.put("VehicleID", currentShiftVehicle.vehicleID);
        values.put("NotOnShift", currentShiftVehicle.notOnShift);
        values.put("ReasonNotOn", currentShiftVehicle.reasonNotOn);

        db.update("ShiftVehicle", values, "ShiftID = ? and VehicleID = ?", new String[]{currentShiftVehicle.getShiftID(), currentShiftVehicle.getVehicleID()});
    }
    
    public static String getStaffID(String shiftID, String vehicleID) {
        String staffID = null;
        
        String query = "" + 
            "select " +
            "StaffID " +
            "from " +
            "ShiftVehicle " +
            "where " +
            "ShiftID = ? and VehicleID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, vehicleID });
        
        if (cursor.moveToFirst()) { 
            staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
        }
        
        cursor.close();
        
        return staffID;
    }
    
    public static void setStaffID(String shiftID, String vehicleID, String staffID) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("StaffID", staffID);
        
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.update("ShiftVehicle", contentValues, "ShiftID = ? and VehicleID = ?", new String[] { shiftID, vehicleID });
    }
}
