package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class Lane {
    public String laneID;
    public String name;
    public boolean isActive;

    public Lane() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("LaneID", this.laneID);
            obj.put("Name", this.name);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "Lane - getJSONObject");
        }
        return obj;
    }

    protected static Lane cursorToModel(Cursor c) {
        Lane lane = new Lane();
        lane.laneID = c.getString(c.getColumnIndex("LaneID"));
        lane.name = c.getString(c.getColumnIndex("Name"));
        lane.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return lane;
    }

    public static Lane get(String laneID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Lane model = null;
        Cursor c = db.rawQuery("SELECT * FROM Lanes WHERE LaneID = ?", new String[] { laneID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    // includeLaneID is used to ensure previously selected item will still be in collection even if de-activated
    public static List<Lane> getLookupItems(String includeLaneID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<Lane> models = new LinkedList<>();

        Lane defaultItem = new Lane();
        defaultItem.name = "";
        models.add(defaultItem);

        Cursor c;

        if (includeLaneID != null) {
            c = db.rawQuery("SELECT * FROM Lanes WHERE IsActive = 1 or LaneID = ? ORDER BY Name", new String[]{includeLaneID});
        } else {
            c = db.rawQuery("SELECT * FROM Lanes WHERE IsActive = 1 ORDER BY Name", null);
        }

        if (c.moveToFirst()) {
            do {
                Lane model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(Lane model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("LaneID", model.laneID);
            cv.put("Name", model.name);
            cv.put("IsActive", model.isActive);

            if (get(model.laneID) != null) {
                db.update("Lanes", cv, "LaneID = ?", new String[]{model.laneID});
            } else {
                db.insertOrThrow("Lanes", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "Lane - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return name;
    }
}
