package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class BriefingChecklistAnswer {
    public String briefingID;
    public String checklistID;
    public String checklistQuestionID;
    public boolean answer;
    public boolean isNew;
    public String question;
    public String userID;
    public String shiftID;

    public BriefingChecklistAnswer(){
    }

    private String getBriefingID() { return this.briefingID; }
    private String getChecklistID() { return this.checklistID; }
    private String getChecklistQuestionID() { return this.checklistQuestionID; }
    private boolean getAnswer() { return this.answer; }
    public boolean getIsNew() { return this.isNew; }
    public String getQuestion() { return this.question; }
    public String getUserID() { return this.userID;}

    public void setBriefingID(String briefingID) { this.briefingID = briefingID; }
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setAnswer(boolean answer) { this.answer = answer; }
    public void setNew(boolean isNew) { this.isNew = isNew; }
    public void setUserID(String userID) { this.userID = userID;}
    private void setQuestion(String question) { this.question = question; }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("BriefingID", this.briefingID);
            obj.put("ChecklistID", this.checklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Answer", this.answer);
            obj.put("IsNew", this.isNew);
            obj.put("Question", this.question);

        return obj;
    }

    public JSONObject getJSONObjectToSend() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("BriefingID", this.briefingID);
            obj.put("ChecklistID", this.checklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Answer", this.answer);
            obj.put("Question", this.question);

        return obj;
    }

    //Briefing Checklist Answers
    public static void addBriefingChecklistAnswer(BriefingChecklistAnswer briefingChecklistAnswer)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BriefingID", briefingChecklistAnswer.briefingID);
        values.put("ChecklistID", briefingChecklistAnswer.checklistID);
        values.put("ChecklistQuestionID", briefingChecklistAnswer.checklistQuestionID);
        values.put("UserID", briefingChecklistAnswer.userID);
        values.put("ShiftID", briefingChecklistAnswer.shiftID);
        //values.put("Answer", briefingChecklistAnswer.getAnswer());

        db.insert("BriefingChecklistAnswers", null, values);
    }

    public static void deleteAllBriefingChecklistAnswers()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklistAnswers");
    }

    public static List<BriefingChecklistAnswer> GetBriefingChecklistAnswers(String briefingID) {
        List<BriefingChecklistAnswer> briefingChecklistAnswers = new LinkedList<>();

        String query = "SELECT bca.*, cq.Question FROM BriefingChecklistAnswers bca ";
        query += "INNER JOIN ChecklistQuestions cq on cq.ChecklistQuestionID = bca.ChecklistQuestionID ";
        query += "WHERE BriefingID = '" + briefingID;
        query += "' ORDER BY cq.QuestionOrder";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        BriefingChecklistAnswer bca;
        if(cursor.moveToFirst()){
            do{
                bca = new BriefingChecklistAnswer();
                bca.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                bca.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                bca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));

                int columnIndex = cursor.getColumnIndex("Answer");
                if (!cursor.isNull(columnIndex)) {
                    bca.answer = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0;
                }
                else
                {
                    bca.isNew = true;
                }

                //bca.setAnswer(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0);
                bca.question = cursor.getString(cursor.getColumnIndex("Question"));

                briefingChecklistAnswers.add(bca);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return briefingChecklistAnswers;
    }


    public static void SetToolboxAnswer(String checklistQuestionID, String briefingID, boolean answer) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("Update BriefingChecklistAnswers Set Answer = " + (answer ? 1 : 0) + " WHERE BriefingID = '" + briefingID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'");
    }

    public static boolean CheckIfBriefingChecklistCreated(String briefingID, String checklistID) {
        boolean ret = false;

        String query = "SELECT * FROM BriefingChecklistAnswers ";
        query += "WHERE BriefingID = '" + briefingID + "' AND ChecklistID = '" + checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                ret = true;
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static JSONArray GetBriefingChecklistAnswersToSend(String shiftID) throws Exception {
        JSONArray briefingChecklistAnswers = new JSONArray();

        String query = "SELECT bca.* FROM BriefingChecklistAnswers bca ";
        query += "JOIN Briefings b on b.BriefingID = bca.BriefingID ";
        query += "JOIN JobPacks jp on jp.JobPackID = b.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject bcaObject;
            if (cursor.moveToFirst()) {
                do {
                    BriefingChecklistAnswer bca = new BriefingChecklistAnswer();
                    bca.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                    bca.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                    bca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                    bca.answer = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0;

                    bcaObject = bca.getJSONObjectToSend();

                    briefingChecklistAnswers.put(bcaObject);
                } while (cursor.moveToNext());
            }

        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetBriefingChecklistAnswersToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return briefingChecklistAnswers;
    }
}
