package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class JobPackDocument {
    public String jobPackDocumentID;
    public String jobPackID;
    public String documentName;
    public String contentType;
    public String imageLocation;

    public JobPackDocument(){
    }

    private String getJobPackDocumentID() { return this.jobPackDocumentID; }
    private String getJobPackID() { return this.jobPackID; }
    private String getDocumentName() { return this.documentName; }
    private String getContentType() { return this.contentType; }
    public String getImageLocation() { return this.imageLocation; }

    public void setJobPackDocumentID(String jobPackDocumentID) { this.jobPackDocumentID = jobPackDocumentID; }
    public void setJobPackID(String jobPackID) { this.jobPackID = jobPackID; }
    public void setDocumentName(String documentName) { this.documentName = documentName; }
    public void setContentType(String contentType) { this.contentType = contentType; }
    public void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("JobPackDocumentID", this.jobPackDocumentID);
            obj.put("JobPackID", this.jobPackID);
            obj.put("DocumentName", this.documentName);
            obj.put("ContentType", this.contentType);
            obj.put("ImageLocation", this.imageLocation);

        return obj;
    }

    //Job Pack Documents
    public static void addJobPackDocument(JobPackDocument jobPackDocument)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("JobPackDocumentID", jobPackDocument.jobPackDocumentID);
        values.put("JobPackID", jobPackDocument.jobPackID);
        values.put("DocumentName", jobPackDocument.documentName);
        values.put("ContentType", jobPackDocument.contentType);
        values.put("ImageLocation", jobPackDocument.imageLocation);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(jobPackDocument.jobPackDocumentID)) {
            db.insert("JobPackDocuments", null, values);
        }
    }

    public static boolean exists(String jobPackDocumentID) {
        String query = "select * from JobPackDocuments where JobPackDocumentID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { jobPackDocumentID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllJobPackDocuments()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM JobPackDocuments");
    }

    public static void deleteJobPackDocumentsForJobPack(String jobPackID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM JobPackDocuments WHERE JobPackID = '" + jobPackID + "'");
    }

    public static String GetJobPackDocumentID(String jobPackDocumentID) {
        String query = "SELECT JobPackDocumentID FROM JobPackDocuments where JobPackDocumentID = '" + jobPackDocumentID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String ret = "";

        if(cursor.moveToFirst()){
            do{
                ret = cursor.getString(cursor.getColumnIndex("JobPackDocumentID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static JobPackDocument GetJobPackDocument(String jobPackDocumentID) {
        String query = "SELECT * FROM JobPackDocuments where JobPackDocumentID = '" + jobPackDocumentID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        JobPackDocument jpd = null;
        if(cursor.moveToFirst()){
            do{
                jpd = new JobPackDocument();
                jpd.jobPackDocumentID = cursor.getString(cursor.getColumnIndex("JobPackDocumentID"));
                jpd.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                jpd.documentName = cursor.getString(cursor.getColumnIndex("DocumentName"));
                jpd.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                jpd.contentType = cursor.getString(cursor.getColumnIndex("ContentType"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return jpd;
    }

    public static List<JobPackDocument> GetJobPackDocuments(String jobPackID) {
        List<JobPackDocument> jobPackDocuments = new LinkedList<>();

        String query = "SELECT * FROM JobPackDocuments ";
        query += "WHERE JobPackID = '" + jobPackID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        JobPackDocument jpd;
        if(cursor.moveToFirst()){
            do{
                jpd = new JobPackDocument();
                jpd.jobPackDocumentID = cursor.getString(cursor.getColumnIndex("JobPackDocumentID"));
                jpd.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                jpd.documentName = cursor.getString(cursor.getColumnIndex("DocumentName"));
                jpd.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                jpd.contentType = cursor.getString(cursor.getColumnIndex("ContentType"));

                jobPackDocuments.add(jpd);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return jobPackDocuments;
    }
}
