package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class ActivityType {
    public String activityTypeID;
    public String name;
    public int trafficCountLimit;
    public String displayName;

    public ActivityType(){
    }

    private String getActivityTypeID() { return this.activityTypeID; }
    private String getName() { return this.name; }
    private int getTrafficCountLimit() { return this.trafficCountLimit; }
    private String getDisplayName() { return this.displayName; }

    public void setActivityTypeID(String activityTypeID) { this.activityTypeID = activityTypeID; }
    public void setName(String name) { this.name = name; }
    public void setTrafficCountLimit(int trafficCountLimit) { this.trafficCountLimit = trafficCountLimit; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("ActivityTypeID", this.activityTypeID);
            obj.put("Name", this.name);
            obj.put("TrafficCountLimit", this.trafficCountLimit);
            obj.put("DisplayName", this.displayName);
        } catch (JSONException e) { }

        return obj;
    }

    protected static ActivityType cursorToModel(Cursor c) {
        ActivityType activityType = new ActivityType();
        activityType.activityTypeID = c.getString(c.getColumnIndex("ActivityTypeID"));
        activityType.name = c.getString(c.getColumnIndex("Name"));
        activityType.displayName = c.getString(c.getColumnIndex("DisplayName"));

        return activityType;
    }

    //Activity Types
    public static void addActivityType(ActivityType activityType)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ActivityTypeID", activityType.activityTypeID);
        values.put("Name", activityType.name);
        values.put("TrafficCountLimit", 0);
        values.put("DisplayName", activityType.displayName);

        db.insert("ActivityTypes", null, values);
    }

    public static void deleteAllActivityTypes()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ActivityTypes");
    }

    public static int GetTrafficCountLimit(int taskID) {
        String query = "SELECT at.TrafficCountLimit FROM Tasks t ";
        query += "JOIN Activities a on a.ActivityID = t.ActivityID ";
        query += "JOIN ActivityTypes at on at.ActivityTypeID = a.ActivityTypeID ";
        query += "WHERE t.TaskID = '" + taskID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int limit = 0;
        if(cursor.moveToFirst()){
            do{
                limit = Integer.parseInt(cursor.getString(cursor.getColumnIndex("TrafficCountLimit")));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return limit;
    }

    public static List<ActivityType> getLookupItems() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<ActivityType> models = new LinkedList<>();

        ActivityType defaultItem = new ActivityType();
        defaultItem.name = "";
        models.add(defaultItem);

        Cursor c = db.rawQuery("SELECT * FROM ActivityTypes ORDER BY Name", null);

        if (c.moveToFirst()) {
            do {
                ActivityType model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    // required for spinner text
    @Override
    public String toString() {
        return displayName;
    }
}
