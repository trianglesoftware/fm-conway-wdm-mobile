package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

public class ActivityPriority {
    public String activityPriorityID;
    public String name;
    public boolean isActive;

    public ActivityPriority() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ActivityPriorityID", this.activityPriorityID);
            obj.put("Name", this.name);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "ActivityPriority - getJSONObject");
        }
        return obj;
    }

    protected static ActivityPriority cursorToModel(Cursor c) {
        ActivityPriority activityPriority = new ActivityPriority();
        activityPriority.activityPriorityID = c.getString(c.getColumnIndex("ActivityPriorityID"));
        activityPriority.name = c.getString(c.getColumnIndex("Name"));
        activityPriority.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return activityPriority;
    }

    public static ActivityPriority get(String activityPriorityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ActivityPriority model = null;
        Cursor c = db.rawQuery("SELECT * FROM ActivityPriorities WHERE ActivityPriorityID = ?", new String[] { activityPriorityID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    // includeActivityPriorityID is used to ensure previously selected item will still be in collection even if de-activated
    public static List<ActivityPriority> getLookupItems(String includeActivityPriorityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<ActivityPriority> models = new LinkedList<>();

        ActivityPriority defaultItem = new ActivityPriority();
        defaultItem.name = "";
        models.add(defaultItem);

        Cursor c;

        if (includeActivityPriorityID != null) {
            c = db.rawQuery("SELECT * FROM ActivityPriorities WHERE IsActive = 1 or ActivityPriorityID = ? ORDER BY Name", new String[]{includeActivityPriorityID});
        } else {
            c = db.rawQuery("SELECT * FROM ActivityPriorities WHERE IsActive = 1 ORDER BY Name", null);
        }

        if (c.moveToFirst()) {
            do {
                ActivityPriority model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(ActivityPriority model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("ActivityPriorityID", model.activityPriorityID);
            cv.put("Name", model.name);
            cv.put("IsActive", model.isActive);

            if (get(model.activityPriorityID) != null) {
                db.update("ActivityPriorities", cv, "ActivityPriorityID = ?", new String[]{model.activityPriorityID});
            } else {
                db.insertOrThrow("ActivityPriorities", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "ActivityPriority - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return name;
    }
}
