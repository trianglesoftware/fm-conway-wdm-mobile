package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class BriefingChecklist {
    public String briefingID;
    public String checklistID;
    public String userID;
    public String shiftID;

    public BriefingChecklist(){
    }

    public BriefingChecklist(String BriefingID, String ChecklistID)
    {
        super();
        this.briefingID = BriefingID;
        this.checklistID = ChecklistID;
    }

    private String getBriefingID() { return this.briefingID; }
    private String getChecklistID() { return this.checklistID; }
    public String getUserID() { return this.userID;}

    public void setBriefingID(String briefingID) { this.briefingID = briefingID; }
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }
    public void setUserID(String userID) { this.userID = userID;}

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("BriefingID", this.briefingID);
            obj.put("ChecklistID", this.checklistID);
        } catch (JSONException e) { }

        return obj;
    }

    //Briefing Checklists
    public static void addBriefingChecklist(BriefingChecklist briefingChecklist)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BriefingID", briefingChecklist.briefingID);
        values.put("ChecklistID", briefingChecklist.checklistID);
        values.put("UserID", briefingChecklist.userID);
        values.put("ShiftID", briefingChecklist.shiftID);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(briefingChecklist.briefingID, briefingChecklist.checklistID)) {
            db.insert("BriefingChecklists", null, values);
        }
    }

    public static boolean exists(String briefingID, String checklistID) {
        String query = "select * from BriefingChecklists where BriefingID = ? and ChecklistID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { briefingID, checklistID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllBriefingChecklists()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklists");
    }

    public static void deleteBriefingChecklistsForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklistAnswers WHERE UserID = '" + userID + "'");

        db.execSQL("DELETE FROM BriefingChecklists WHERE UserID = '" + userID + "'");
    }

    public static void deleteBriefingChecklistsForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklistAnswers WHERE ShiftID = '" + shiftID + "'");

        db.execSQL("DELETE FROM BriefingChecklists WHERE ShiftID = '" + shiftID + "'");
    }

    public static Checklist GetBriefingChecklist(String briefingID) {
        String query = "SELECT * FROM BriefingChecklists ";
        query += "WHERE BriefingID = '" + briefingID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Checklist checklist = null;
        if(cursor.moveToFirst()){
            do{
                checklist = new Checklist();
                checklist.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return checklist;
    }
}
