package trianglesoftware.fmconway.Database;

import android.content.Context;

/**
 * Created by Jamie.Dobson on 15/03/2016.
 */
public class Database {
    public static DatabaseHelper MainDB;

    public Database()
    {

    }

    public Database(Context ctx)
    {
        initialiseDatabase(ctx);
    }

    private void initialiseDatabase(Context ctx)
    {
        MainDB = new DatabaseHelper(ctx);
    }
}
