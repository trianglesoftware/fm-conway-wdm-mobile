package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Area {
    public String areaID;
    public String userID;
    public String name;
    public String callProtocol;
    public String personsName;
    public String reference;
    public String details;

    public Area(){
    }

    public String getAreaID() { return this.areaID; }
    public String getUserID() { return this.userID;}
    public String getName() { return this.name; }
    public String getCallProtocol() { return this.callProtocol; }
    public String getDetails() { return this.details; }
    public String getPersonsName() { return this.personsName; }
    public String getReference() { return this.reference; }

    public void setAreaID(String areaID) { this.areaID = areaID; }
    public void setUserID(String userID) { this.userID = userID;}
    public void setName(String name) { this.name = name; }
    public void setCallProtocol(String callProtocol) { this.callProtocol = callProtocol; }
    public void setDetails(String details) { this.details = details; }
    public void setPersonsName(String personsName) { this.personsName = personsName; }
    public void setReference(String reference) { this.reference = reference; }

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("AreaID", this.areaID);
            obj.put("Name", this.name);
            obj.put("CallProtocol", this.callProtocol);
            obj.put("Details", this.details);
            obj.put("PersonsName", this.personsName);
            obj.put("Reference", this.reference);

        return obj;
    }

    //Areas
    public static void addArea(Area area)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("AreaID", area.areaID);
        values.put("Name", area.name);
        values.put("CallProtocol", area.callProtocol);
        values.put("Details", area.details);
        values.put("PersonsName", area.personsName);
        values.put("Reference", area.reference);
        values.put("UserID", area.userID);

        db.insert("Areas", null, values);
    }

    public static void updateArea(Area area)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("AreaID", area.areaID);
        values.put("Name", area.name);
        values.put("CallProtocol", area.callProtocol);
        values.put("Details", area.details);
        values.put("PersonsName", area.personsName);
        values.put("Reference", area.reference);
        values.put("UserID", area.userID);

        db.update("Areas", values, "AreaID = ?", new String[]{area.areaID});
    }

    public static boolean exists(String areaID) {
        String query = "select * from Areas where AreaID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { areaID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllAreas()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Areas");
    }

    public static void deleteAreasForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Areas WHERE UserID = '" + userID + "'");
    }

    public static Area GetArea(String areaID) {
        String query = "SELECT * FROM Areas ";
        query += "WHERE AreaID = '" + areaID +"'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Area a = null;
        if(cursor.moveToFirst()){
            do{
                a = new Area();
                a.areaID = cursor.getString(cursor.getColumnIndex("AreaID"));
                a.name = cursor.getString(cursor.getColumnIndex("Name"));
                a.callProtocol = cursor.getString(cursor.getColumnIndex("CallProtocol"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return a;
    }

    public static void UpdateDetails(String areaID, String details, String personsName, String reference) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

//        db.execSQL("UPDATE Areas SET Details = ?, PersonsName = ?, Reference = ? WHERE AreaID = " + areaID,
//                new String[]{details,personsName,reference});

        SQLiteStatement statement = db.compileStatement("UPDATE Areas SET Details = ?, PersonsName = ?, Reference = ? WHERE AreaID = ?");
        statement.bindString(1,details);
        statement.bindString(2,personsName);
        statement.bindString(3,reference);
        statement.bindString(4,areaID);
        statement.execute();
        statement.close();
    }

    public static JSONArray GetAreasToSend(String shiftID) throws Exception {
        JSONArray areas = new JSONArray();

        String query = "SELECT a.* FROM Areas a ";
        query += "JOIN Tasks t on t.AreaID = a.AreaID ";
        query += "JOIN Activities act on act.ActivityID = t.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = act.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject aObject;
            if (cursor.moveToFirst()) {
                do {
                    Area a = new Area();
                    a.areaID = cursor.getString(cursor.getColumnIndex("AreaID"));
                    a.details = cursor.getString(cursor.getColumnIndex("Details"));
                    a.personsName = cursor.getString(cursor.getColumnIndex("PersonsName"));
                    a.reference = cursor.getString(cursor.getColumnIndex("Reference"));

                    aObject = a.getJSONObject();

                    areas.put(aObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetAreasToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return areas;
    }

    public static String GetAreaReference(String activityID) {
        String query = "SELECT a.Reference FROM Areas a ";
        query += "JOIN Tasks t on t.AreaID = a.AreaID ";
        query += "WHERE t.ActivityID = '" + activityID + "' AND a.Reference IS NOT NULL AND a.Reference <> ''";
        query += "LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String a = "";
        if(cursor.moveToFirst()){
            do{
                a = cursor.getString(cursor.getColumnIndex("Reference"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return a;
    }
}
