package trianglesoftware.fmconway.Database;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import trianglesoftware.fmconway.Database.DatabaseObjects.Accident;
import trianglesoftware.fmconway.Database.DatabaseObjects.AccidentPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityMethodStatement;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityRiskAssessment;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivitySignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Area;
import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLogPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.Contact;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPackDocument;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipmentItem;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckDetail;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckDetailPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.ObservationPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHours;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursChecklistAnswerPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgressStarted;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheet;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Note;
import trianglesoftware.fmconway.Database.DatabaseObjects.Observation;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswerPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcess;
import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcessPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswerDefectPhoto;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 04/04/2016.
 */
public class DataSync {
    private final String shiftID;
    private final String userID;
    private boolean finalSyncAtEnd;
    private final String encodedHeader;
    private final String queryURL;
    private boolean onFinalSyncCompleteShift = true;
    private int timeout = 360000;

    public DataSync(String ShiftID, String UserID, String QueryURL, String EncodedHeader) {
        shiftID = ShiftID;
        userID = UserID;
        encodedHeader = EncodedHeader;
        queryURL = QueryURL;
    }

    int _processes = 18;
    int _completedProcesses = 0;
    int _AandOProcesses = 2;
    int _completedAandOProcesses = 0;
    int _errors = 0;

    public void deleteShift() {
        _completedProcesses = 999;
        _errors = 0;
        finalSyncAtEnd = true;
        onFinalSyncCompleteShift = false;

        onRequestComplete(new DataSync.PushDataListener() {
            @Override
            public void onComplete(boolean errors) {
            }

            @Override
            public void onError(Throwable t) {
                ErrorLog.CreateError(t, "DeleteShift");
            }
        });
    }

    private synchronized void onRequestComplete(PushDataListener listener) {

        if (++_completedProcesses >= _processes) {

            if (_errors == 0) {
                // must be captured before shift is deleted
                boolean isCancelled = Shift.CheckIfCancelled(shiftID);

                // Shift and user dependent data
                Shift.deleteShiftForID(shiftID);
                ShiftStaff.deleteShiftStaff(shiftID);
                ShiftVehicle.deleteShiftVehicles(shiftID);
                LoadingSheet.deleteLoadingSheets(shiftID);
                LoadingSheetEquipmentItem.deleteForShift(shiftID);
                LoadingSheetEquipment.deleteLoadingSheetEquipmentForShift(shiftID);
                VehicleChecklist.deleteAllVehicleChecklistsForShift(shiftID);
                Note.deleteAllNotesForShift(shiftID);
                //Area.deleteAreasForUser(userID);
                TaskChecklist.deleteTaskChecklistsForShift(shiftID);
                TaskChecklistAnswer.deleteTaskChecklistAnswersForShift(shiftID);
                TaskEquipment.deleteAllTaskEquipmentForShift(shiftID);

                List<Activity> activities = Activity.GetActivitiesForShift(shiftID);
                for (int i = 0; i < activities.size(); i++) {
                    String activityID = activities.get(i).activityID;

                    ActivityMethodStatement.deleteMethodStatementsForActivity(activityID);
                    MaintenanceCheck.deleteMaintenanceChecksForActivity(activityID);
                    MaintenanceCheckDetail.deleteMaintenanceCheckDetailsForActivity(activityID);
                    CleansingLog.deleteCleansingLogsForActivity(activityID);
                    UnloadingProcess.deleteUnloadingProcessesForActivity(activityID);
                    Task.deleteTasksForActivity(activityID);
                }

                // JobPack tables
                List<JobPack> jobPacks = JobPack.GetJobPacksForShift(shiftID);
                for (int i = 0; i < jobPacks.size(); i++) {
                    String jobPackID = jobPacks.get(i).jobPackID;

                    Briefing.deleteBriefingsForJobPack(jobPackID);
                    Activity.deleteActivitiesForJobPack(jobPackID);

                    if (finalSyncAtEnd) {
                        JobPackDocument.deleteJobPackDocumentsForJobPack(jobPackID);

                        // only successfully completed shifts run this (skipped for cancelled shifts)
                        if (onFinalSyncCompleteShift && !isCancelled) {
                            if (!ShiftProgress.DoesProgressExist(shiftID, "Completed")) {
                                ShiftProgress.SetShiftProgress(shiftID, "Completed");
                                ShiftProgress.SyncProgress();
                            }

                            AsyncHttpClient client = new AsyncHttpClient();
                            client.setTimeout(timeout);
                            client.addHeader("Authorization", encodedHeader);
                            client.get(queryURL + "CompleteShift?shiftID=" + shiftID, null, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    Log.e("Chevron", "ShiftCompleted");
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    Log.e("Chevron", "Complete Shift Failed" + statusCode + " " + error.getMessage());
                                    ErrorLog.CreateError(error, "CompleteShift");
                                }
                            });
                        }
                    }

                }

                BriefingChecklist.deleteBriefingChecklistsForShift(shiftID);
                JobPack.deleteJobPacksForShift(shiftID);

                if (finalSyncAtEnd) {
                    Accident.deleteAllAccidentsForUser(userID);
                    Observation.deleteAllObservationsForUser(userID);
                    BriefingChecklistSignature.deleteAllBriefingChecklistSignaturesForShift(shiftID);
                    ActivitySignature.deleteActivitySignaturesForShiftID(shiftID);
                    VehicleChecklistAnswerDefectPhoto.deleteAllVehicleChecklistAnswerDefectPhotosForShift(shiftID);
                    LoadingSheetSignature.deleteLoadingSheetSignaturesForShift(shiftID);
                    MaintenanceCheckDetailPhoto.deleteMaintenanceCheckDetailPhotosForShift(shiftID);
                    TaskSignature.deleteAllTaskSignaturesForShift(shiftID);
                    UnloadingProcessPhoto.deleteAllUnloadingProcessPhotosForShift(shiftID);
                    CleansingLogPhoto.deleteAllCleansingLogPhotosForShift(shiftID);
                    TaskPhoto.deleteAllTaskPhotosForShift(shiftID);
                    /*OutOfHours.deleteAllOutOfHoursForUser(userID);*/
                }

                // Delete Shift and User independent data
//                ActivityType.deleteAllActivityTypes();
//                BriefingType.deleteAllBriefingTypes();
//                Checklist.deleteAllChecklists();
//                ChecklistQuestion.deleteAllChecklistQuestions();
//                Equipment.deleteAllEquipment();
//                EquipmentType.deleteAllEquipmentTypes();
//
//                Staff.deleteAllStaff();
//                TaskType.deleteAllTaskTypes();
//                User.deleteAllUsers();
//                Vehicle.deleteAllVehicles();
//                VehicleType.deleteAllVehicleTypes();

            } else {
                try {
                    JSONArray errorLogs = ErrorLog.GetErrorLogsToSend();
                    Params urlParams = new Params(queryURL + "PushError", errorLogs.toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!
                            ErrorLog.deleteErrorLogs();
                        }

                        @Override
                        public void onError(Throwable t) {

                        }

                    }).execute(urlParams).get();
                } catch (Exception e) {
                    ErrorLog.CreateError(e, "SendErrorLogsAfterFailedSync");
                }
            }

            listener.onComplete(_errors > 0);

        }
    }

    private synchronized void onRequestError(Throwable t, PushDataListener listener, String from) {

        ErrorLog.CreateError(t, from);
        _errors++;

        if (++_completedProcesses >= _processes)
            listener.onComplete(_errors > 0);

    }

    public void PushData(boolean finalSync, final PushDataListener listener) {

        finalSyncAtEnd = finalSync;

        Params urlParams;

        try {

            JSONArray loadingSheetSignaturesToSend = new JSONArray();
            JSONArray briefingSignaturesToSend = new JSONArray();
            JSONArray activitySignaturesToSend = new JSONArray();
            JSONArray defectPhotosToSend = new JSONArray();
            JSONArray accidentsToSend = new JSONArray();
            JSONArray observationsToSend = new JSONArray();
            JSONArray maintenanceCheckPhotos = new JSONArray();
            JSONArray taskChecklistAnswerPhotos = new JSONArray();
            JSONArray unloadingProcessPhotos = new JSONArray();
            JSONArray cleansingLogPhotos = new JSONArray();
            JSONArray taskPhotos = new JSONArray();
            JSONArray taskSignatures = new JSONArray();
            JSONArray taskEquipment = new JSONArray();
            //JSONArray outOfHoursToSend = new JSONArray();
            //JSONArray outOfHoursChecklistsToSend = new JSONArray();
            //JSONArray outOfHoursChecklistPhotosToSend = new JSONArray();


            if (finalSync) {

                //_processes++; // Complete Shift
                maintenanceCheckPhotos = MaintenanceCheckDetailPhoto.GetMaintenanceCheckDetailPhotosToSend(shiftID);
                _processes += maintenanceCheckPhotos.length();

                taskChecklistAnswerPhotos = TaskChecklistAnswerPhoto.GetTaskChecklistPhotosToSend(shiftID);
                _processes += taskChecklistAnswerPhotos.length();

                loadingSheetSignaturesToSend = LoadingSheetSignature.GetSignaturesToSend(shiftID);
                _processes += loadingSheetSignaturesToSend.length();

                briefingSignaturesToSend = BriefingChecklistSignature.GetSignaturesToSend(shiftID);
                _processes += briefingSignaturesToSend.length();

                activitySignaturesToSend = ActivitySignature.GetSignaturesToSend(shiftID);
                _processes += activitySignaturesToSend.length();

                defectPhotosToSend = VehicleChecklistAnswerDefectPhoto.GetDefectPhotosToSend(shiftID, userID);
                _processes += defectPhotosToSend.length();

                accidentsToSend = Accident.GetAccidentsToSend(shiftID, userID);
                _processes += accidentsToSend.length();

                observationsToSend = Observation.GetObservationsToSend(shiftID, userID);
                _processes += observationsToSend.length();

                unloadingProcessPhotos = UnloadingProcessPhoto.GetPhotosToSend(shiftID);
                _processes += unloadingProcessPhotos.length();

                cleansingLogPhotos = CleansingLogPhoto.GetPhotosToSend(shiftID);
                _processes += cleansingLogPhotos.length();

                taskPhotos = TaskPhoto.GetPhotosToSend(shiftID);
                _processes += taskPhotos.length();

                taskSignatures = TaskSignature.getSignaturesToSend(shiftID);
                _processes += taskSignatures.length();

                /*outOfHoursToSend = OutOfHours.GetOutOfHoursToSend();
                _processes += outOfHoursToSend.length();*/

                //outOfHoursChecklistsToSend = OutOfHoursChecklist.GetOOHChecklistsToSend();
                //_processes += outOfHoursChecklistsToSend.length();

                //outOfHoursChecklistPhotosToSend = OutOfHoursChecklistAnswerPhoto.GetOOHChecklistPhotosToSend();
                //_processes += outOfHoursChecklistPhotosToSend.length();
            }

            //Error Logs
            JSONArray errorLogs = ErrorLog.GetErrorLogsToSend();
            urlParams = new Params(queryURL + "PushError", errorLogs.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!
                    ErrorLog.deleteErrorLogs();
                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - ErrorLogs");
                }
            }).execute(urlParams).get();

            //Push OutOfHours (New WorkInstruction)
            if (OutOfHours.IsOutOfHoursShift(shiftID)) {
                JSONObject outOfHoursToSend = OutOfHours.GetOutOfHoursToSend(shiftID);
                urlParams = new Params(queryURL + "PushOutOfHours", outOfHoursToSend.toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        onRequestComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        // if OutOfHours fails, set _completedProcess and force an early return
                        _completedProcesses = 999;
                        onRequestError(t, listener, "DataSync - OutOfHours");
                    }
                }).execute(urlParams).get();

                if (_completedProcesses >= _processes) {
                    return;
                }
            } else {
                onRequestComplete(listener);
            }

            //Push ShiftProgress
            JSONArray shiftProgressToSend = ShiftProgress.GetShiftProgressToSend();
            urlParams = new Params(queryURL + "UpdateShiftProgress", shiftProgressToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgressResponse() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("UpdateShiftProgressResult");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject shiftProgressResult = jsonArray.getJSONObject(i);
                            String shiftID = shiftProgressResult.getString("ShiftID");
                            String progress = shiftProgressResult.getString("Progress");
                            boolean saved = shiftProgressResult.getBoolean("Saved");

                            if (saved) {
                                ShiftProgress.SetShiftProgressAsSent(shiftID, progress);
                            }
                        }

                        onRequestComplete(listener);
                    } catch (Exception ex) {
                        onRequestError(ex, listener, "DataSync - ShiftProgressInner");
                    }
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - ShiftProgress");
                }
            }).execute(urlParams).get();

            //Push Then Delete - Deletes need to be removed from DataDownload for these items
            //Shift Staff
            JSONArray shiftStaffToSend = ShiftStaff.GetShiftStaffToSend(shiftID);
            urlParams = new Params(queryURL + "PushShiftStaff", shiftStaffToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - ShiftStaff");
                }

            }).execute(urlParams).get();

            //Shift Vehicles
            JSONObject shiftVehiclesToSend = ShiftVehicle.GetShiftVehiclesToSend(shiftID, userID, false);
            urlParams = new Params(queryURL + "PushShiftVehicles", shiftVehiclesToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - ShiftVehicles");
                }

            }).execute(urlParams).get();

            //Vehicle Checklists
            //Vehicle Checklist Answers
            //Vehicle Checklist Answer Defects
            JSONArray vehicleChecklistsToSend = VehicleChecklist.GetVehicleChecklistsToSend(shiftID, userID);
            urlParams = new Params(queryURL + "PushVehicleChecklists", vehicleChecklistsToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - VehicleChecklists");
                }
            }).execute(urlParams).get();

            //Loading Sheet Equipment Items
            //Equipment Item Checklists
            //Equipment Item Checklist Answers

            JSONObject loadingSheetEquipmentItemsToSend = LoadingSheetEquipmentItem.toSend(shiftID);
            if (LoadingSheetEquipmentItem.hasChecklists(loadingSheetEquipmentItemsToSend)) {
                urlParams = new Params(queryURL + "PushEquipmentItems", loadingSheetEquipmentItemsToSend.toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        //Only delete if successful!
                        onRequestComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        onRequestError(t, listener, "DataSync - EquipmentItems");
                    }
                }).execute(urlParams).get();
            } else {
                onRequestComplete(listener);
            }

            //Tasks
            JSONArray tasksToSend = Task.GetTasksToSend(shiftID);
            urlParams = new Params(queryURL + "PushTasks", tasksToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - Tasks");
                }

            }).execute(urlParams).get();

            //Task equipment
            JSONArray taskEquipmentToSend = TaskEquipment.GetTaskEquipmentToSend(shiftID);
            urlParams = new Params(queryURL + "PushTaskEquipment", taskEquipmentToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - TaskEquipment");
                }
            }).execute(urlParams).get();

            //Task Checklist
            //Task Checklist Answers
            JSONArray taskChecklistsToSend = TaskChecklist.GetTaskChecklistsToSend(shiftID);
            urlParams = new Params(queryURL + "PushTaskChecklists", taskChecklistsToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - TaskChecklists");
                }

            }).execute(urlParams).get();

            //Areas
            JSONArray areasToSend = Area.GetAreasToSend(shiftID);
            urlParams = new Params(queryURL + "PushAreas", areasToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - Areas");
                }

            }).execute(urlParams).get();

            //Notes
            JSONArray notesToSend = Note.GetNotesToSend(shiftID);
            urlParams = new Params(queryURL + "PushNotes", notesToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - Notes");
                }

            }).execute(urlParams).get();

            //Maintenance Checks
            JSONArray maintenanceChecksToSend = MaintenanceCheck.GetMaintenanceChecksToSend(shiftID);
            urlParams = new Params(queryURL + "PushMaintenanceChecks", maintenanceChecksToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - MaintenanceChecks");
                }

            }).execute(urlParams).get();

            //Maintenance Check Photos
            //JSONArray maintenanceCheckPhotos = MaintenanceCheckDetailPhoto.GetMaintenanceCheckDetailPhotosToSend(shiftID);
            //urlParams = new Params(queryURL + "PushMaintenanceCheckDetailPhoto", maintenanceCheckPhotos.toString(), encodedHeader);
            //new PushData(new PushDataListenerProgress() {
            //    @Override
            //    public void onComplete() {
            //        //Only delete if successful!
            //
            //        onRequestComplete(listener);
            //    }
            //
            //    @Override
            //    public void onError(Throwable t) {
            //        onRequestError(t, listener);
            //    }
            //
            //}).execute(urlParams).get();

            //Activity Details
            JSONObject activityDetailsToSend = Activity.getActivityDetailsToSend(shiftID);
            urlParams = new Params(queryURL + "PushActivityDetails", activityDetailsToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - ActivityDetails");
                }
            }).execute(urlParams).get();

            //Maintenance Check Photos
            //JSONArray maintenanceCheckPhotos = MaintenanceCheckDetailPhoto.GetMaintenanceCheckDetailPhotosToSend(shiftID);
            //urlParams = new Params(queryURL + "PushMaintenanceCheckDetailPhoto", maintenanceCheckPhotos.toString(), encodedHeader);
            //new PushData(new PushDataListenerProgress() {
            //    @Override
            //    public void onComplete() {
            //        //Only delete if successful!
            //
            //        onRequestComplete(listener);
            //    }
            //
            //    @Override
            //    public void onError(Throwable t) {
            //        onRequestError(t, listener);
            //    }
            //
            //}).execute(urlParams).get();

            //Cleansing Logs
            JSONArray cleansingLogsToSend = CleansingLog.getCleansingLogsToSend(shiftID);
            urlParams = new Params(queryURL + "PushCleansingLogs", cleansingLogsToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - CleansingLogs");
                }
            }).execute(urlParams).get();

            //Activity Details
            JSONArray activityRiskAssessmentsToSend = ActivityRiskAssessment.getActivityRiskAssessmentsToSend(shiftID);
            urlParams = new Params(queryURL + "PushActivityRiskAssessments", activityRiskAssessmentsToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - ActivityDetails");
                }
            }).execute(urlParams).get();

            //Unloading Processes
            JSONArray unloadingProcessesToSend = UnloadingProcess.getUnloadingProcessesToSend(shiftID);
            urlParams = new Params(queryURL + "PushUnloadingProcesses", unloadingProcessesToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!

                    onRequestComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    onRequestError(t, listener, "DataSync - UnloadingProcesses");
                }
            }).execute(urlParams).get();

            //UpdateShiftStatus
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(timeout);
            client.addHeader("Authorization", encodedHeader);
            client.get(queryURL + "UpdateShiftStatus?shiftID=" + shiftID + "&completionStatus=" + Shift.GetShiftCompletionStatus(shiftID), null, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Log.e("Chevron", "Shift Status Updated");
                    onRequestComplete(listener);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.e("Chevron", "Update Shift Status Failed" + statusCode + " " + error.getMessage());
                    ErrorLog.CreateError(error, "UpdateShiftCompletionStatus");
                    onRequestError(error, listener, "DataSync - UpdateShiftStatus");
                }
            });

            //
            // Final sync
            //

            if (finalSync) {

//                if (maintenanceCheckPhotos.length()>0) {
//                    urlParams = new Params(queryURL + "PushMaintenanceCheckDetailPhoto", maintenanceCheckPhotos.toString(), encodedHeader);
//                    new PushData(new PushDataListenerProgress() {
//                        @Override
//                        public void onComplete() {
//                            //Only delete if successful!
//
//                            onRequestComplete(listener);
//                        }
//
//                        @Override
//                        public void onError(Throwable t) {
//                            onRequestError(t, listener);
//                        }
//
//                    }).execute(urlParams).get();
//                }

                for (int i = 0; i < maintenanceCheckPhotos.length(); i++) {
                    final String id = maintenanceCheckPhotos.getJSONObject(i).getString("MaintenanceCheckDetailPhotoID");
                    urlParams = new Params(queryURL + "PushMaintenanceCheckDetailPhoto", maintenanceCheckPhotos.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            MaintenanceCheckDetailPhoto.deleteMaintenanceCheckDetailPhoto(id);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - MaintenanceCheckPhoto");
                        }

                    }).execute(urlParams).get();
                }

                for (int i = 0; i < taskChecklistAnswerPhotos.length(); i++) {
                    final String id = taskChecklistAnswerPhotos.getJSONObject(i).getString("TaskChecklistAnswerPhotoID");
                    urlParams = new Params(queryURL + "PushTaskChecklistAnswerPhoto", taskChecklistAnswerPhotos.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            TaskChecklistAnswerPhoto.deleteTaskChecklistAnswerPhoto(id);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - TaskChecklistAnswerPhotos");
                        }

                    }).execute(urlParams).get();
                }

                for (int i = 0; i < loadingSheetSignaturesToSend.length(); i++) {
                    final String lid = loadingSheetSignaturesToSend.getJSONObject(i).getString("LoadingSheetID");
                    urlParams = new Params(queryURL + "PushLoadingSheetSignature", loadingSheetSignaturesToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            LoadingSheetSignature.deleteLoadingSheetSignature(lid);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - LoadingSheetSignatures");
                        }

                    }).execute(urlParams).get();
                }
                //Briefing Signatures

                for (int i = 0; i < briefingSignaturesToSend.length(); i++) {
                    final String briefingID = briefingSignaturesToSend.getJSONObject(i).getString("BriefingID");
                    final String staffID = briefingSignaturesToSend.getJSONObject(i).getString("StaffID");
                    urlParams = new Params(queryURL + "PushBriefingSignature", briefingSignaturesToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            BriefingChecklistSignature.deleteBriefingChecklistSignature(briefingID, staffID);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - BriefingSignatures");
                        }

                    }).execute(urlParams).get();
                }
                //Activity Signatures

                for (int i = 0; i < activitySignaturesToSend.length(); i++) {
                    final String activityID = activitySignaturesToSend.getJSONObject(i).getString("ActivityID");
                    final String staffID = activitySignaturesToSend.getJSONObject(i).getString("StaffID");
                    urlParams = new Params(queryURL + "PushActivitySignature", activitySignaturesToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            ActivitySignature.deleteActivitySignature(activityID, staffID);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - ActivitySignatures");
                        }

                    }).execute(urlParams).get();
                }
                //Vehicle Checklist Answer Defect Photos

                for (int i = 0; i < defectPhotosToSend.length(); i++) {

                    final String id = defectPhotosToSend.getJSONObject(i).getString("VehicleChecklistDefectPhotoID");

                    urlParams = new Params(queryURL + "PushDefectPhoto", defectPhotosToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            VehicleChecklistAnswerDefectPhoto.deleteVehicleChecklistAnswerDefectPhoto(id);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - DefectPhotos");
                        }

                    }).execute(urlParams).get();
                }

                // Task photos
                for (int i = 0; i < taskPhotos.length(); i++) {

                    final String id = taskPhotos.getJSONObject(i).getString("TaskPhotoID");
                    urlParams = new Params(queryURL + "PushTaskPhoto", taskPhotos.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            TaskPhoto.deleteTaskPhoto(id);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - TaskPhotos");
                        }

                    }).execute(urlParams).get();
                }

                // Unloading process photos
                for (int i = 0; i < unloadingProcessPhotos.length(); i++) {

                    final String id = unloadingProcessPhotos.getJSONObject(i).getString("UnloadingProcessPhotoID");
                    urlParams = new Params(queryURL + "PushUnloadingProcessPhoto", unloadingProcessPhotos.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            UnloadingProcessPhoto.deleteUnloadingProcessPhoto(id);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - UnloadingProcessPhotos");
                        }

                    }).execute(urlParams).get();
                }

                // Cleansing log photos
                for (int i = 0; i < cleansingLogPhotos.length(); i++) {

                    final String id = cleansingLogPhotos.getJSONObject(i).getString("CleansingLogPhotoID");
                    urlParams = new Params(queryURL + "PushCleansingLogPhoto", cleansingLogPhotos.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            CleansingLogPhoto.deleteCleansingLogPhoto(id);
                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - CleansingLogPhotos");
                        }

                    }).execute(urlParams).get();
                }

                // Task signatures
                for (int i = 0; i < taskSignatures.length(); i++) {

                    final String taskSignatureID = taskSignatures.getJSONObject(i).getString("TaskSignatureID");

                    urlParams = new Params(queryURL + "PushTaskSignature", taskSignatures.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            TaskSignature.deleteTaskSignature(taskSignatureID);

                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - TaskSignatures");
                        }

                    }).execute(urlParams).get();
                }


                //Accidents

                for (int i = 0; i < accidentsToSend.length(); i++) {

                    final String accidentID = accidentsToSend.getJSONObject(i).getString("AccidentID");

                    urlParams = new Params(queryURL + "PushAccidents", accidentsToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            AccidentPhoto.deleteAccidentPhoto(accidentID);

                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - Accidents");
                        }

                    }).execute(urlParams).get();

                }
                //Observations

                for (int i = 0; i < observationsToSend.length(); i++) {

                    final String observationID = observationsToSend.getJSONObject(i).getString("ObservationID");

                    urlParams = new Params(queryURL + "PushObservations", observationsToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            ObservationPhoto.deleteObservationPhoto(observationID);

                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - Observations");
                        }

                    }).execute(urlParams).get();
                }

                //OutOfHours

                /*for (int i = 0; i < outOfHoursToSend.length(); i++) {

                    final String outOfHoursID = outOfHoursToSend.getJSONObject(i).getString("OutOfHoursID");

                    urlParams = new Params(queryURL + "PushOutOfHours", outOfHoursToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!

                            OutOfHoursPhoto.deleteAllOutOfHoursPhotos(outOfHoursID);

                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - OutOfHours");
                        }

                    }).execute(urlParams).get();
                }*/

                //OutOfHoursChecklists

                /*for (int i = 0; i < outOfHoursChecklistsToSend.length(); i++) {

                    final String oohChecklistID = outOfHoursChecklistsToSend.getJSONObject(i).getString("OOHChecklistID");

                    urlParams = new Params(queryURL + "PushOutOfHoursChecklists", outOfHoursChecklistsToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!
                            OutOfHoursChecklistAnswer.deleteOOHChecklistAnswersForCheck(oohChecklistID);
                            OutOfHoursChecklist.deleteOOHChecklist(oohChecklistID);

                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - OutOfHoursChecklists");
                        }

                    }).execute(urlParams).get();
                }*/

                //OutOfHoursChecklistsPhotos

                /*for (int i = 0; i < outOfHoursChecklistPhotosToSend.length(); i++) {

                    final String oohChecklistAnswerPhotoID = outOfHoursChecklistPhotosToSend.getJSONObject(i).getString("OOHChecklistAnswerPhotoID");

                    urlParams = new Params(queryURL + "PushOutOfHoursChecklistPhotos", outOfHoursChecklistPhotosToSend.getJSONObject(i).toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!
                            OutOfHoursChecklistAnswerPhoto.deleteOOHChecklistAnswerPhoto(oohChecklistAnswerPhotoID);

                            onRequestComplete(listener);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onRequestError(t, listener, "DataSync - OutOfHoursChecklistPhotos");
                        }

                    }).execute(urlParams).get();
                }*/

                // Has the shift been cancelled?

                if (Shift.CheckIfCancelled(shiftID)) {
                    JSONObject obj = new JSONObject();
                    obj.put("ShiftID", shiftID);
                    obj.put("CancellationReason", Shift.GetCancellationReason(shiftID));

                    urlParams = new Params(queryURL + "PushCancelShift", obj.toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            Log.e("FMConway", "Cancel Shift");
                        }

                        @Override
                        public void onError(Throwable t) {
                            Log.e("FMConway", "Shift Set Cancellation Failed " + t.getMessage());
                            ErrorLog.CreateError(t, "Cancel Shift");
                        }
                    }).execute(urlParams).get();
                }


            }


        } catch (Exception e) {
            ErrorLog.CreateError(e, "Failed to Push Data");
            listener.onError(e);
        }
    }

    // shiftID should be "" when called from NoShiftActivity but has shiftID when called from SelectShiftActivity
    public void PushAandO(final PushDataListener listener) {
        Params urlParams;

        try {
            //Accidents
            JSONArray accidentsToSend = Accident.GetAccidentsToSend(shiftID, userID);
            JSONArray observationsToSend = Observation.GetObservationsToSend(shiftID, userID);
            JSONArray vehicleChecklistsToSend = VehicleChecklist.GetVehicleChecklistsToSend(shiftID, userID);
            JSONArray errorLogs = ErrorLog.GetErrorLogsToSend();
            JSONArray defectPhotosToSend = VehicleChecklistAnswerDefectPhoto.GetDefectPhotosToSend(shiftID, userID);
            //JSONArray outOfHoursToSend = OutOfHours.GetOutOfHoursToSend();
            //JSONArray outOfHoursChecklistsToSend = OutOfHoursChecklist.GetOOHChecklistsToSend();
            //JSONArray outOfHoursChecklistPhotosToSend = OutOfHoursChecklistAnswerPhoto.GetOOHChecklistPhotosToSend();

            _AandOProcesses = _AandOProcesses +
                    accidentsToSend.length() +
                    observationsToSend.length() +
                    defectPhotosToSend.length();
                    //outOfHoursToSend.length() +
                    //outOfHoursChecklistsToSend.length() +
                    //outOfHoursChecklistPhotosToSend.length();

            JSONObject shiftVehiclesToSend = null;
            if (!TextUtils.isEmpty(shiftID)) {
                _AandOProcesses += 1;
                shiftVehiclesToSend = ShiftVehicle.GetShiftVehiclesToSend(shiftID, userID, true);
            }

            // Push Error Logs
            urlParams = new Params(queryURL + "PushError", errorLogs.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    //Only delete if successful!
                    CheckPushAandOComplete(listener);
                    ErrorLog.deleteErrorLogs();
                }

                @Override
                public void onError(Throwable t) {
                    ErrorLog.CreateError(t, "PushAandOErrors");
                    _errors++;
                    CheckPushAandOComplete(listener);
                }

            }).execute(urlParams).get();

            //Push Accidents
            for (int i = 0; i < accidentsToSend.length(); i++) {
                urlParams = new Params(queryURL + "PushAccidents", accidentsToSend.getJSONObject(i).toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushAandOAccidents");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }

            // Push Observations
            for (int i = 0; i < observationsToSend.length(); i++) {
                urlParams = new Params(queryURL + "PushObservations", observationsToSend.getJSONObject(i).toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushAandOObservations");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }

            //Push Out Of Hours
            /*for (int i = 0; i < outOfHoursToSend.length(); i++) {
                urlParams = new Params(queryURL + "PushOutOfHours", outOfHoursToSend.getJSONObject(i).toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushAandOOutOfHours");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }*/

            //Push Out Of Hours Checklists
            /*for (int i = 0; i < outOfHoursChecklistsToSend.length(); i++) {
                urlParams = new Params(queryURL + "PushOutOfHoursChecklists", outOfHoursChecklistsToSend.getJSONObject(i).toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushAandOOutOfHoursChecklists");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }*/

            //Push Out Of Hours Checklist Photos
            /*for (int i = 0; i < outOfHoursChecklistPhotosToSend.length(); i++) {
                urlParams = new Params(queryURL + "PushOutOfHoursChecklistPhotos", outOfHoursChecklistPhotosToSend.getJSONObject(i).toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushAandOOutOfHoursChecklistPhotos");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }*/

            // Push VehicleChecklists
            urlParams = new Params(queryURL + "PushVehicleChecklists", vehicleChecklistsToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgress() {
                @Override
                public void onComplete() {
                    CheckPushAandOComplete(listener);
                }

                @Override
                public void onError(Throwable t) {
                    ErrorLog.CreateError(t, "PushAandOVehicles");
                    _errors++;
                    CheckPushAandOComplete(listener);

                }
            }).execute(urlParams).get();

            //Shift Vehicles - if has shiftID push nominated drivers and start / end millage
            if (!TextUtils.isEmpty(shiftID)) {
                urlParams = new Params(queryURL + "PushShiftVehicles", shiftVehiclesToSend.toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushShiftVehicles");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }

            // Push DefectPhoto
            for (int i = 0; i < defectPhotosToSend.length(); i++) {
                urlParams = new Params(queryURL + "PushDefectPhoto", defectPhotosToSend.getJSONObject(i).toString(), encodedHeader);
                new PushData(new PushDataListenerProgress() {
                    @Override
                    public void onComplete() {
                        CheckPushAandOComplete(listener);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushAandODefectPhotos");
                        _errors++;
                        CheckPushAandOComplete(listener);
                    }
                }).execute(urlParams).get();
            }

        } catch (Exception e) {
            listener.onError(e);
        }
    }

    private void CheckPushAandOComplete(PushDataListener listener) {
        if (++_completedAandOProcesses >= _AandOProcesses) {
            if (_errors > 0) {
                // Send new error logs
                try {
                    JSONArray errorLogs = ErrorLog.GetErrorLogsToSend();
                    Params urlParams = new Params(queryURL + "PushError", errorLogs.toString(), encodedHeader);
                    new PushData(new PushDataListenerProgress() {
                        @Override
                        public void onComplete() {
                            //Only delete if successful!
                            ErrorLog.deleteErrorLogs();
                        }

                        @Override
                        public void onError(Throwable t) {
                            ErrorLog.CreateError(t, "PushAandOErrors");
                            _errors++;
                        }

                    }).execute(urlParams).get();
                } catch (Exception e) {
                    ErrorLog.CreateError(e, "PushAandOErrorsCatch");
                }
                // Inform user of an error.
            } else {
                // Delete for user
                Accident.deleteAllAccidentsForUser(userID);
                Observation.deleteAllObservationsForUser(userID);

                //OutOfHours.deleteAllOutOfHoursForUser(userID);
                //OutOfHoursChecklist.deleteAllOOHChecklists();
                //OutOfHoursChecklistAnswer.deleteOOHChecklistAnswersForUser();
                //OutOfHoursChecklistAnswerPhoto.deleteOOHChecklistAnswerPhotosForUser(userID);

                if (TextUtils.isEmpty(shiftID)) {
                    VehicleChecklist.deleteAllVehicleChecklistsForUser(userID);
                    VehicleChecklistAnswerDefectPhoto.deleteAllVehicleChecklistAnswerDefectPhotosForUser(userID);
                    ShiftVehicle.deleteShiftVehiclesForUser(userID);
                }

                listener.onComplete(_errors > 0);
            }

        }
    }

    public void PushShiftProgress(final PushDataListener listener) {
        Params urlParams;

        try {
            JSONArray shiftProgressToSend = ShiftProgress.GetShiftProgressToSend();

            if (shiftProgressToSend.length() > 0) {
                //Push ShiftProgress
                urlParams = new Params(queryURL + "UpdateShiftProgress", shiftProgressToSend.toString(), encodedHeader);
                new PushData(new PushDataListenerProgressResponse() {
                    @Override
                    public void onComplete(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("UpdateShiftProgressResult");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject shiftProgressResult = jsonArray.getJSONObject(i);
                                String shiftID = shiftProgressResult.getString("ShiftID");
                                String progress = shiftProgressResult.getString("Progress");
                                boolean saved = shiftProgressResult.getBoolean("Saved");

                                if (saved) {
                                    ShiftProgress.SetShiftProgressAsSent(shiftID, progress);
                                }
                            }

                            listener.onComplete(_errors > 0);
                        } catch (Exception ex) {
                            ErrorLog.CreateError(ex, "PushShiftProgress-Inner");
                            listener.onComplete(true);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        ErrorLog.CreateError(t, "PushShiftProgress");
                        _errors++;
                        listener.onComplete(_errors > 0);
                    }
                }).execute(urlParams).get();
            } else {
                listener.onComplete(true);
            }
        } catch (Exception e) {
            listener.onError(e);
        }
    }

    public void PushShiftProgressStarted(final ShiftProgressStarted shiftProgressStarted, final PushDataListenerProgressResponse listener) {
        Params urlParams;

        try {
            JSONObject shiftProgressToSend = shiftProgressStarted.getJSONObject();
            shiftProgressToSend.put("ShiftUpdatedDate", FMConwayUtils.getDateIsoString(shiftProgressStarted.shiftUpdatedDate));

            //Push ShiftProgressStarted
            urlParams = new Params(queryURL + "UpdateShiftProgressStarted", shiftProgressToSend.toString(), encodedHeader);
            new PushData(new PushDataListenerProgressResponse() {
                @Override
                public void onComplete(String response) {
                    listener.onComplete(response);
                }

                @Override
                public void onError(Throwable t) {
                    listener.onError(t);
                }
            }).execute(urlParams).get();
        } catch (Exception e) {
            listener.onError(e);
        }
    }


    public interface PushDataListenerProgress {
        public void onComplete();
        public void onError(Throwable t);
    }

    public interface PushDataListenerProgressResponse {
        public void onComplete(String response);
        public void onError(Throwable t);
    }

    public interface PushDataListener {
        public void onComplete(boolean errors);
        public void onError(Throwable t);
    }
}

class Params {
    final String url;
    final String jsonData;
    final String encodedHeader;

    Params(String Url, String JsonData, String EncodedHeader) {
        this.url = Url;
        this.jsonData = JsonData;
        this.encodedHeader = EncodedHeader;
    }
}

class PushData extends AsyncTask<Params, Void, Integer> {

    private final DataSync.PushDataListenerProgress _listener;
    private final DataSync.PushDataListenerProgressResponse _responseListener;

    public PushData(DataSync.PushDataListenerProgress listener) {
        _listener = listener;
        _responseListener = null;
    }

    public PushData(DataSync.PushDataListenerProgressResponse responseListener) {
        _listener = null;
        _responseListener = responseListener;
    }

    public PushData() {
        _listener = null;
        _responseListener = null;
    }

    @Override
    protected Integer doInBackground(Params... params) {

        try {
            StringBuilder sb = new StringBuilder();
            URL url;
            HttpURLConnection urlConn;
            url = new URL(params[0].url);

            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            //urlConn.setRequestProperty("Content-Type", "applicat ion/json");
            urlConn.setRequestProperty("Authorization", params[0].encodedHeader);

            urlConn.connect();

            String str = params[0].jsonData.toString();
            byte[] outputInBytes = str.getBytes("UTF-8");
            OutputStream os = urlConn.getOutputStream();
            os.write(outputInBytes);
            os.close();

            int HttpResult = urlConn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(), "utf-8"));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();

                String response = sb.toString();
                int countProperties = response.length() - response.replace(":", "").length();

                if (response.contains("true") && countProperties == 1) {
                    //Delete file from device
                    JSONObject json = new JSONObject(params[0].jsonData);
                    String imageLocation = json.getString("ImageLocation");
                    File file = new File(imageLocation);
                    file.delete();

                } else if (response.contains("PushAccidentsResult")) {
                    String result = response;
                    result = result.replace("{\"PushAccidentsResult\":", "").replace("}\n", "");
                    return Integer.parseInt(result);
                } else if (response.contains("PushObservationsResult")) {
                    String result = response;
                    result = result.replace("{\"PushObservationsResult\":", "").replace("}\n", "");
                    return Integer.parseInt(result);
                }

                if (_listener != null) {
                    _listener.onComplete();
                }

                if (_responseListener != null) {
                    _responseListener.onComplete(response);
                }

            } else {
                System.out.println(urlConn.getResponseMessage());
                ErrorLog.CreateError(new Throwable(urlConn.getResponseMessage()), "PushData Failed");

                if (_listener != null) {
                    _listener.onError(new Throwable(urlConn.getResponseMessage()));
                }

                if (_responseListener != null) {
                    _responseListener.onError(new Throwable(urlConn.getResponseMessage()));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            ErrorLog.CreateError(e, "PushData Failed");
            if (_listener != null) {
                _listener.onError(e);
            }

            if (_responseListener != null) {
                _responseListener.onError(e);
            }
        }

        return null;
    }

}
