package trianglesoftware.fmconway.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;

/**
 * Created by Jamie.Dobson on 25/02/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 9;
    public static final String DATABASE_NAME = "FMConwayDB";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Users (UserID TEXT PRIMARY KEY, UserTypeID TEXT, EmployeeType TEXT, DepotID TEXT, Username TEXT, IsStandardUser INTEGER)");
        db.execSQL("CREATE TABLE Shifts (ShiftID TEXT PRIMARY KEY, DayShift INTEGER, UserID TEXT, ShiftDate DATETIME, ShiftCompletionStatus INTEGER, IsCancelled INTEGER, ShiftDetails TEXT, ShiftTypes TEXT, Address TEXT, TrafficManagementActivities TEXT, CancellationReason Text, SRWNumber TEXT, ProjCode TEXT, AddressLine1 TEXT, AddressLine2 TEXT, Borough TEXT, City TEXT, County TEXT, Postcode TEXT, UpdatedDate DATETIME)");
        db.execSQL("CREATE TABLE Staff (StaffID TEXT PRIMARY KEY, Forename TEXT, Surname TEXT, DepotID TEXT, StaffType TEXT, IsAgency INTEGER)");
        db.execSQL("CREATE TABLE ShiftStaff (StaffID TEXT, ShiftID TEXT, NotOnShift INTEGER, ReasonNotOn TEXT, StartTime DATETIME, EndTime DATETIME, FirstAider INTEGER, WorkedHours INTEGER, WorkedMins INTEGER, BreakHours INTEGER, BreakMins INTEGER, TravelHours INTEGER, TravelMins INTEGER, Ipv INTEGER, PaidBreak INTEGER, OnCall INTEGER, AgencyEmployeeName TEXT)");
        db.execSQL("CREATE TABLE StaffRecords (StaffRecordID INTEGER, StaffID, ImageLocation TEXT, RecordName TEXT, ContentType TEXT)");
        db.execSQL("CREATE TABLE JobPacks (JobPackID TEXT PRIMARY KEY, ShiftID TEXT, JobPackName TEXT)");
        db.execSQL("CREATE TABLE JobPackDocuments (JobPackDocumentID TEXT, JobPackID TEXT, ImageLocation TEXT, DocumentName TEXT, ContentType TEXT)");
        db.execSQL("CREATE TABLE ShiftVehicle (ShiftID TEXT, VehicleID TEXT, StaffID TEXT, LoadingSheetCompleted INTEGER, StartMileage INTEGER, EndMileage INTEGER, NotOnShift INTEGER, ReasonNotOn TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE Equipment (EquipmentID TEXT PRIMARY KEY, Name TEXT, EquipmentTypeID TEXT)");
        db.execSQL("CREATE TABLE EquipmentTypes (EquipmentTypeID TEXT PRIMARY KEY, Name TEXT, VmsOrAsset TEXT)");
        db.execSQL("CREATE TABLE Checklists (ChecklistID TEXT PRIMARY KEY, Name TEXT, IsMaintenance INTEGER, IsPreHire INTEGER, IsVms INTEGER, IsBattery INTEGER, IsPreHireBattery INTEGER, IsPostHireBattery INTEGER, IsHighSpeed INTEGER, IsLowSpeed INTEGER, IsJettingPermitToWork INTEGER, IsJettingRiskAssessment INTEGER, IsTankeringPermitToWork INTEGER, IsTankeringRiskAssessment INTEGER, IsCCTVRiskAssessment INTEGER, IsJettingNewRiskAssessment INTEGER)");
        db.execSQL("CREATE TABLE ChecklistQuestions (ChecklistQuestionID TEXT, Question TEXT, QuestionOrder INTEGER, ChecklistID TEXT)");
        db.execSQL("CREATE TABLE VehicleTypes (VehicleTypeID TEXT PRIMARY KEY, Name TEXT)");
        db.execSQL("CREATE TABLE VehicleTypeChecklists (VehicleTypeID TEXT, ChecklistID TEXT)");
        db.execSQL("CREATE TABLE Vehicles (VehicleID TEXT PRIMARY KEY, Make TEXT, Model TEXT, Registration TEXT, DepotID TEXT, VehicleTypeID TEXT)");
        db.execSQL("CREATE TABLE LoadingSheets (LoadingSheetID TEXT PRIMARY KEY, VehicleID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE LoadingSheetEquipment (LoadingSheetEquipmentID TEXT PRIMARY KEY, LoadingSheetID TEXT, EquipmentID TEXT, Quantity INTEGER, SheetOrder INTEGER, ShiftID TEXT, VmsOrAsset TEXT)");
        db.execSQL("CREATE TABLE LoadingSheetEquipmentItems (LoadingSheetEquipmentItemID TEXT PRIMARY KEY, LoadingSheetEquipmentID TEXT, ItemNumber INTEGER, ChecklistID TEXT)");
        db.execSQL("CREATE TABLE EquipmentItemChecklists (EquipmentItemChecklistID TEXT PRIMARY KEY, LoadingSheetEquipmentItemID TEXT, ChecklistID TEXT)");
        db.execSQL("CREATE TABLE EquipmentItemChecklistAnswers (EquipmentItemChecklistAnswerID TEXT PRIMARY KEY, EquipmentItemChecklistID TEXT, ChecklistQuestionID TEXT, Answer INTEGER, Reason TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE Briefings (BriefingID TEXT PRIMARY KEY, BriefingTypeID INTEGER, JobPackID TEXT, Name TEXT, Details TEXT, DocumentID TEXT, ImageLocation TEXT, DocumentName TEXT, ContentType TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE BriefingChecklists (BriefingID TEXT, ChecklistID TEXT, UserID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE BriefingTypes (BriefingTypeID INTEGER PRIMARY KEY, Name TEXT)");
        db.execSQL("CREATE TABLE ActivityTypes (ActivityTypeID TEXT PRIMARY KEY, Name TEXT, TrafficCountLimit INTEGER, DisplayName TEXT)");
        db.execSQL("CREATE TABLE ObservationTypes (ObservationTypeID INTEGER PRIMARY KEY, Name TEXT)");
        db.execSQL("CREATE TABLE TaskTypes (TaskTypeID INTEGER PRIMARY KEY, Name TEXT)");
        db.execSQL("CREATE TABLE Activities (ActivityID TEXT PRIMARY KEY, ActivityTypeID TEXT, Name TEXT, Customer TEXT, CustomerPhoneNumber TEXT, Contract TEXT, ContractNumber TEXT, Scheme TEXT, ScopeOfWork TEXT, AreaCallNumber TEXT, AreaCallProtocol TEXT, FirstCone TEXT, LastCone TEXT, Distance REAL, Road TEXT, CWayDirection TEXT, InstallationTime DATE, RemovalTime DATE, HealthAndSafety TEXT, WorksInstructionNumber TEXT, JobPackID TEXT, WorksInstructionComments TEXT, MaintenanceChecklistReminderInMinutes INTEGER, SiteName TEXT, ClientReferenceNumber TEXT, ActivityPriorityID TEXT, Operator TEXT, CustomerID TEXT, IsOutOfHours INTEGER, WorkInstructionStartDate DATETIME, WorkInstructionFinishDate DATETIME, DepotID TEXT, OrderNumber INTEGER)");
        db.execSQL("CREATE TABLE Tasks (TaskID TEXT PRIMARY KEY, Name TEXT, TaskOrder INTEGER, TaskTypeID INTEGER, AreaID TEXT, ChecklistID TEXT, ActivityID TEXT, StartTime DATETIME, EndTime DATETIME, Notes TEXT, TrafficCount TEXT, PhotosRequired INTEGER, ShowDetails INTEGER, ShowClientSignature INTEGER, ClientSignatureRequired INTEGER, ShowStaffSignature INTEGER, StaffSignatureRequired INTEGER, Tag TEXT, Longitude REAL, Latitude REAL, WeatherConditionID Text, IsMandatory INTEGER)");
        db.execSQL("CREATE TABLE Areas (AreaID TEXT PRIMARY KEY, Name TEXT, CallProtocol TEXT, Details TEXT, PersonsName TEXT, Reference TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE LoadingSheetSignatures (ShiftID TEXT, VehicleID TEXT, LoadingSheetID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, ImageLocation TEXT, Time DATE)");
        db.execSQL("CREATE TABLE VehicleChecklists (VehicleChecklistID TEXT PRIMARY KEY, VehicleID TEXT, ChecklistID TEXT, ShiftID TEXT, StartOfShift INTEGER, AdHoc INTEGER, ImageLocation TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE VehicleChecklistAnswers (VehicleChecklistID TEXT, ChecklistQuestionID TEXT, Answer INTEGER, UserID TEXT)");
        db.execSQL("CREATE TABLE VehicleChecklistAnswerDefects (VehicleChecklistID TEXT, ChecklistQuestionID TEXT, Defect TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE VehicleChecklistAnswerDefectPhotos (VehicleChecklistDefectPhotoID TEXT, VehicleChecklistID TEXT, ChecklistQuestionID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE BriefingChecklistAnswers (BriefingID TEXT, ChecklistID TEXT, ChecklistQuestionID TEXT, Answer TEXT, UserID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE BriefingChecklistSignatures (BriefingID TEXT, StaffID TEXT, Longitude REAL, Latitude REAL, ImageLocation TEXT, BriefingStart DATE, BriefingEnd DATE, UserID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE TaskChecklists (TaskChecklistID TEXT PRIMARY KEY, TaskID TEXT, ChecklistID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE TaskChecklistAnswers (TaskChecklistAnswerID TEXT PRIMARY KEY, TaskChecklistID TEXT, ChecklistQuestionID TEXT, Answer INTEGER, Reason TEXT, ShiftID TEXT, Time DATE)");
        db.execSQL("CREATE TABLE TaskChecklistAnswerPhotos (TaskChecklistAnswerPhotoID TEXT, TaskChecklistAnswerID TEXT, ImageLocation TEXT, Longitude REAL, Latitude REAL, ActivityID TEXT, ShiftID TEXT, UserID TEXT, Time DATE)");
        db.execSQL("CREATE TABLE Accidents (IsNew INTEGER, AccidentID TEXT PRIMARY KEY, ShiftID TEXT, AccidentDetails TEXT, AccidentDate DATE, Location TEXT, Registrations TEXT, PersonReporting TEXT, ChevronDepot TEXT, PeopleInvolved TEXT, Witnesses TEXT, ActionTaken TEXT, UserID TEXT)");
        db.execSQL("CREATE TABLE Observations (IsNew INTEGER, ObservationID TEXT PRIMARY KEY, ShiftID TEXT, PeopleInvolved TEXT, Location TEXT, ObservationDescription TEXT, CauseOfProblem TEXT, ObservationDate DATE, UserID TEXT)");
        db.execSQL("CREATE TABLE Notes (NoteID TEXT PRIMARY KEY, ShiftID TEXT, NoteDetails TEXT, NoteDate DATE, UserID TEXT, New INTEGER)");
        db.execSQL("CREATE TABLE MaintenanceChecks (MaintenanceCheckID TEXT PRIMARY KEY, Time DATE, Description TEXT, ActivityID TEXT)");
        db.execSQL("CREATE TABLE MaintenanceCheckAnswers (MaintenanceCheckID TEXT, ChecklistID TEXT, ChecklistQuestionID TEXT, Answer INTEGER, ActivityID TEXT)");
        db.execSQL("CREATE TABLE ActivitySignatures (ActivityID TEXT, StaffID TEXT, Longitude REAL, Latitude REAL, ImageLocation TEXT, UserID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE AccidentPhotos (AccidentPhotoID TEXT, AccidentID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT)");
        db.execSQL("CREATE TABLE ObservationPhotos (ObservationPhotoID TEXT, ObservationID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT)");
        db.execSQL("CREATE TABLE MaintenanceCheckDetails (MaintenanceCheckID TEXT, ChecklistQuestionID TEXT, Description TEXT, ActivityID TEXT)");
        db.execSQL("CREATE TABLE MaintenanceCheckDetailPhotos (MaintenanceCheckDetailPhotoID TEXT, MaintenanceCheckID TEXT, ChecklistQuestionID TEXT, Longitude REAL, Latitude REAL, ImageLocation TEXT, ActivityID TEXT, UserID TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE ErrorLog (LogID TEXT PRIMARY KEY, Description TEXT, Exception TEXT, Sent INTEGER)");
        db.execSQL("CREATE TABLE TaskEquipment (TaskEquipmentID TEXT, ShiftID TEXT, TaskID TEXT, EquipmentID TEXT, Quantity INTEGER, UserID TEXT, Updated INTEGER)");
        db.execSQL("CREATE TABLE TaskEquipmentAssets (TaskEquipmentAssetID TEXT PRIMARY KEY, TaskEquipmentID TEXT, ItemNumber INTEGER, AssetNumber TEXT, ShiftID TEXT)");
        db.execSQL("CREATE TABLE TaskSignatures (TaskSignatureID TEXT, TaskID TEXT, StaffID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, ImageLocation TEXT, IsClient INTEGER, ShiftID TEXT, PrintedName TEXT, ClientVehicleReg TEXT)");
        db.execSQL("CREATE TABLE TaskPhotos (TaskPhotoID TEXT, TaskID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT, ShiftID TEXT, Comments TEXT)");
        db.execSQL("CREATE TABLE ShiftProgress (ShiftID TEXT, Progress TEXT, CreatedDate DATE, Sent INTEGER)");
        db.execSQL("CREATE TABLE WeatherConditions (WeatherConditionID TEXT, Name TEXT)");
        db.execSQL("CREATE TABLE OutOfHours (OutOfHoursID TEXT, Customer TEXT, TMRequested TEXT, Location TEXT, ArriveOnSite DATETIME, Equipment TEXT, JobInstalled DATETIME, LeaveSite DATETIME, IsNew INTEGER, UserID TEXT)");
        db.execSQL("CREATE TABLE OutOfHoursPhotos (OOHPhotoID TEXT, OutOfHoursID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT)");
        db.execSQL("CREATE TABLE OutOfHoursChecklists (OOHChecklistID TEXT, OutOfHoursID TEXT, ChecklistID TEXT)");
        db.execSQL("CREATE TABLE OutOfHoursChecklistAnswers (OOHChecklistAnswerID TEXT, OOHChecklistID TEXT, ChecklistQuestionID TEXT, Answer INTEGER, Reason TEXT, Time Date)");
        db.execSQL("CREATE TABLE OutOfHoursChecklistAnswerPhotos (OOHChecklistAnswerPhotoID TEXT, OOHChecklistAnswerID TEXT, ImageLocation TEXT, Longitude REAL, Latitude REAL, UserID TEXT, Time DATE)");
        db.execSQL("CREATE TABLE CleansingLogs (CleansingLogID TEXT PRIMARY KEY, TaskID TEXT, ActivityID TEXT, Road TEXT, Section TEXT, LaneID TEXT, DirectionID TEXT, StartMarker TEXT, EndMarker TEXT, GulliesCleaned REAL, GulliesMissed REAL, CatchpitsCleaned REAL, CatchpitsMissed REAL, Channels REAL, SlotDrains REAL, Comments TEXT, CreatedDate DATETIME, Defects REAL)");
        db.execSQL("CREATE TABLE CleansingLogPhotos (CleansingLogPhotoID TEXT PRIMARY KEY, CleansingLogID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT, ShiftID TEXT, Comments TEXT)");
        db.execSQL("CREATE TABLE ActivityPriorities (ActivityPriorityID TEXT PRIMARY KEY, Name TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE Lanes (LaneID TEXT PRIMARY KEY, Name TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE Directions (DirectionID TEXT PRIMARY KEY, Name TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE UnloadingProcesses (UnloadingProcessID TEXT PRIMARY KEY, TaskID TEXT, ActivityID TEXT, DepartToDisposalSiteDate DATETIME, ArriveAtDisposalSiteDate DATETIME, DisposalSiteID TEXT, WasteTicketNumber TEXT, VolumeOfWasteDisposed REAL, Comments TEXT, LeaveDisposalSiteDate DATETIME, CreatedDate DATETIME)");
        db.execSQL("CREATE TABLE UnloadingProcessPhotos (UnloadingProcessPhotoID TEXT PRIMARY KEY, UnloadingProcessID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT, ShiftID TEXT, Comments TEXT)");
        db.execSQL("CREATE TABLE DisposalSites (DisposalSiteID TEXT PRIMARY KEY, Name TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE Contacts (ContactID TEXT PRIMARY KEY, CustomerID TEXT, ContactName TEXT, PhoneNumber TEXT, MobileNumber TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE Customers (CustomerID TEXT PRIMARY KEY, CustomerName TEXT, AccountNumber TEXT, CustomerCode TEXT, PhoneNumber TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE MethodStatements (MethodStatementID TEXT PRIMARY KEY, DocumentID TEXT, MethodStatementTitle TEXT, ImageLocation TEXT, DocumentName TEXT, ContentType TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE Depots (DepotID TEXT PRIMARY KEY, DepotName TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE ActivityMethodStatements (ActivityMethodStatementID TEXT PRIMARY KEY, ActivityID TEXT, MethodStatementID TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE TMRequirements (TMRequirementID TEXT PRIMARY KEY, Name TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE RoadSpeeds (RoadSpeedID TEXT PRIMARY KEY, Speed TEXT, IsActive INTEGER)");
        db.execSQL("CREATE TABLE ActivityRiskAssessments (ActivityRiskAssessmentID TEXT PRIMARY KEY, TaskID TEXT, CleansingLogID TEXT, RoadName TEXT, RoadSpeedID TEXT, TMRequirementID TEXT, Schools INTEGER, PedestrianCrossings INTEGER, OverheadCables INTEGER, TreeCanopies INTEGER, WaterCourses INTEGER, AdverseWeather INTEGER, Notes TEXT, IsActive INTEGER, CreatedDate DATETIME)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.beginTransaction();
        try {
            // examples
            //db.execSQL("CREATE TABLE StaffRecords (StaffRecordID INTEGER, StaffID, ImageLocation TEXT, RecordName TEXT, ContentType TEXT)");
            //db.execSQL("ALTER TABLE Tasks ADD COLUMN ShowDetails INTEGER");
            //db.execSQL("DROP TABLE ShiftVehicle");

            if (columnExists(db, "CleansingLogs", "StartMarker") && columnType(db, "CleansingLogs", "StartMarker").equals("REAL")) {
                modifyColumnType(db, "CleansingLogs", "StartMarker", "TEXT");
            }

            if (columnExists(db, "CleansingLogs", "EndMarker") && columnType(db, "CleansingLogs", "EndMarker").equals("REAL")) {
                modifyColumnType(db, "CleansingLogs", "EndMarker", "TEXT");
            }

            if (!tableExists(db, "CleansingLogPhotos")) {
                db.execSQL("CREATE TABLE CleansingLogPhotos (CleansingLogPhotoID TEXT PRIMARY KEY, CleansingLogID TEXT, UserID TEXT, Longitude REAL, Latitude REAL, Time DATE, ImageLocation TEXT, ShiftID TEXT, Comments TEXT)");
            }

            if (!columnExists(db, "Activities", "ContractNumber")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN ContractNumber TEXT");
            }

            if (!tableExists(db, "Contacts")) {
                db.execSQL("CREATE TABLE Contacts (ContactID TEXT PRIMARY KEY, ActivityID TEXT, ContactName TEXT, PhoneNumber TEXT, MobileNumber TEXT, IsActive INTEGER)");
            }

            if (!columnExists(db, "Activities", "CustomerPhoneNumber")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN CustomerPhoneNumber TEXT");
            }

            if (!columnExists(db, "Tasks", "IsMandatory")) {
                db.execSQL("ALTER TABLE Tasks ADD COLUMN IsMandatory INTEGER DEFAULT 1");
            }

            if (!columnExists(db, "Contacts", "ActivityID")) {
                dropColumn(db, "Contacts", "ActivityID");
            }

            if (!columnExists(db, "Contacts", "CustomerID")) {
                db.execSQL("ALTER TABLE Contacts ADD COLUMN CustomerID TEXT");
            }

            if (!tableExists(db, "Customers")) {
                db.execSQL("CREATE TABLE Customers (CustomerID TEXT PRIMARY KEY, CustomerName TEXT, AccountNumber TEXT, CustomerCode TEXT, PhoneNumber TEXT, IsActive INTEGER)");
            }

            if (!columnExists(db, "Activities", "CustomerID")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN CustomerID TEXT");
            }

            if (!tableExists(db, "MethodStatements")) {
                db.execSQL("CREATE TABLE MethodStatements (MethodStatementID TEXT PRIMARY KEY, DocumentID TEXT, MethodStatementTitle TEXT, ImageLocation TEXT, DocumentName TEXT, ContentType TEXT, IsActive INTEGER)");
            }

            if (!tableExists(db, "Depots")) {
                db.execSQL("CREATE TABLE Depots (DepotID TEXT PRIMARY KEY, DepotName TEXT, IsActive INTEGER)");
            }

            if (!columnExists(db, "Activities", "IsOutOfHours")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN IsOutOfHours INTEGER");
            }

            if (!columnExists(db, "Activities", "WorkInstructionStartDate")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN WorkInstructionStartDate DATETIME");
            }

            if (!columnExists(db, "Activities", "WorkInstructionFinishDate")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN WorkInstructionFinishDate DATETIME");
            }

            if (!columnExists(db, "Activities", "DepotID")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN DepotID TEXT");
            }

            if (!columnExists(db, "Shifts", "AddressLine1")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN AddressLine1 TEXT");
            }

            if (!columnExists(db, "Shifts", "AddressLine2")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN AddressLine2 TEXT");
            }

            if (!columnExists(db, "Shifts", "Borough")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN Borough TEXT");
            }

            if (!columnExists(db, "Shifts", "City")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN City TEXT");
            }

            if (!columnExists(db, "Shifts", "County")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN County TEXT");
            }

            if (!columnExists(db, "Shifts", "Postcode")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN Postcode TEXT");
            }

            if (!tableExists(db, "ActivityMethodStatements")) {
                db.execSQL("CREATE TABLE ActivityMethodStatements (ActivityMethodStatementID TEXT PRIMARY KEY, ActivityID TEXT, MethodStatementID TEXT, IsActive INTEGER)");
            }

            if (!columnExists(db, "Activities", "OrderNumber")) {
                db.execSQL("ALTER TABLE Activities ADD COLUMN OrderNumber INTEGER");
            }

            if (!columnExists(db, "Shifts", "UpdatedDate")) {
                db.execSQL("ALTER TABLE Shifts ADD COLUMN UpdatedDate DATETIME");
            }

            if (!columnExists(db, "Checklists", "IsJettingNewRiskAssessment")) {
                db.execSQL("ALTER TABLE Checklists ADD COLUMN IsJettingNewRiskAssessment INTEGER");
            }

            if(!tableExists(db, "TMRequirements")){
                db.execSQL("CREATE TABLE TMRequirements (TMRequirementID TEXT PRIMARY KEY, Name TEXT, IsActive INTEGER)");
            }

            if(!tableExists(db, "RoadSpeeds")){
                db.execSQL("CREATE TABLE RoadSpeeds (RoadSpeedID TEXT PRIMARY KEY, Speed TEXT, IsActive INTEGER)");
            }

            if(!tableExists(db, "ActivityRiskAssessments")){
                db.execSQL("CREATE TABLE ActivityRiskAssessments (ActivityRiskAssessmentID TEXT PRIMARY KEY, TaskID TEXT, CleansingLogID TEXT, RoadName TEXT, RoadSpeedID TEXT, TMRequirementID TEXT, Schools INTEGER, PedestrianCrossings INTEGER, OverheadCables INTEGER, TreeCanopies INTEGER, WaterCourses INTEGER, AdverseWeather INTEGER, Notes TEXT, IsActive INTEGER, CreatedDate DATETIME)");
            }

            if (!columnExists(db, "CleansingLogs", "Defects")) {
                db.execSQL("ALTER TABLE CleansingLogs ADD COLUMN Defects REAL");
            }

            if(!columnExists(db, "ActivityTypes", "DisplayName")) {
                db.execSQL("ALTER TABLE ActivityTypes ADD COLUMN DisplayName TEXT");
            }

            db.setTransactionSuccessful();

        } catch (Exception ex) {
            ErrorLog.CreateError(ex, "DatabaseHelper - onUpgrade");
            throw ex;
        } finally {
            db.endTransaction();
        }
    }

    public boolean tableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        boolean found = false;
        try {
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    found = true;
                }

            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DatabaseHelper - tableExists");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return found;
    }

    public boolean columnExists(SQLiteDatabase db, String tableName, String column) {
        boolean exists = false;

        String query = "PRAGMA table_info(" + tableName + ")";
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    if (name.equals(column)) {
                        exists = true;
                        break;
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DatabaseHelper - columnExists");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return exists;
    }

    public void dropColumn(SQLiteDatabase db, String tableName, String column) {
        dropColumns(db, tableName, Collections.singletonList(column));
    }

    public void dropColumns(SQLiteDatabase db, String tableName, Collection<String> columnsToRemove) {
        List<String> columnNames = new ArrayList<>();
        List<String> columnNamesWithType = new ArrayList<>();
        List<String> primaryKeys = new ArrayList<>();
        String query = "pragma table_info(" + tableName + ");";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String columnName = cursor.getString(cursor.getColumnIndex("name"));

            if (columnsToRemove.contains(columnName)) {
                continue;
            }

            String columnType = cursor.getString(cursor.getColumnIndex("type"));
            boolean isNotNull = cursor.getInt(cursor.getColumnIndex("notnull")) == 1;
            boolean isPk = cursor.getInt(cursor.getColumnIndex("pk")) == 1;

            columnNames.add(columnName);
            String tmp = "`" + columnName + "` " + columnType + " ";
            if (isNotNull) {
                tmp += " NOT NULL ";
            }

            int defaultValueType = cursor.getType(cursor.getColumnIndex("dflt_value"));
            if (defaultValueType == Cursor.FIELD_TYPE_STRING) {
                tmp += " DEFAULT " + "\"" + cursor.getString(cursor.getColumnIndex("dflt_value")) + "\" ";
            } else if (defaultValueType == Cursor.FIELD_TYPE_INTEGER) {
                tmp += " DEFAULT " + cursor.getInt(cursor.getColumnIndex("dflt_value")) + " ";
            } else if (defaultValueType == Cursor.FIELD_TYPE_FLOAT) {
                tmp += " DEFAULT " + cursor.getFloat(cursor.getColumnIndex("dflt_value")) + " ";
            }
            columnNamesWithType.add(tmp);
            if (isPk) {
                primaryKeys.add("`" + columnName + "`");
            }
        }
        cursor.close();

        String columnNamesSeparated = TextUtils.join(", ", columnNames);
        if (primaryKeys.size() > 0) {
            columnNamesWithType.add("PRIMARY KEY(" + TextUtils.join(", ", primaryKeys) + ")");
        }
        String columnNamesWithTypeSeparated = TextUtils.join(", ", columnNamesWithType);

        db.execSQL("ALTER TABLE " + tableName + " RENAME TO " + tableName + "_old;");
        db.execSQL("CREATE TABLE " + tableName + " (" + columnNamesWithTypeSeparated + ");");
        db.execSQL("INSERT INTO " + tableName + " (" + columnNamesSeparated + ") SELECT " + columnNamesSeparated + " FROM " + tableName + "_old;");
        db.execSQL("DROP TABLE " + tableName + "_old;");
    }

    public String columnType(SQLiteDatabase db, String tableName, String column) {
        String columnType = null;

        String query = "PRAGMA table_info(" + tableName + ")";
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    if (name.equals(column)) {
                        columnType = cursor.getString(cursor.getColumnIndex("type"));
                        break;
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DatabaseHelper - columnType");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return columnType;
    }

    public void modifyColumnType(SQLiteDatabase db, String tableName, String column, String type) {
        String columnTemp = column + "_modify_column_temp";
        db.execSQL("ALTER TABLE " + tableName + " ADD " + columnTemp + " " + type);
        db.execSQL("UPDATE " + tableName + " SET " + columnTemp + " = CAST(" + column + " as " + type + ")");
        dropColumn(db, tableName, column);
        db.execSQL("ALTER TABLE " + tableName + " ADD " + column + " " + type);
        db.execSQL("UPDATE " + tableName + " SET " + column + " = " + columnTemp);
        dropColumn(db, tableName, columnTemp);
    }
}
