package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class LoadingSheet {
    public String loadingSheetID;
    public String vehicleID;
    public String shiftID;

    public LoadingSheet(){
    }

    public LoadingSheet(String LoadingSheetID, String VehicleID, String ShiftID)
    {
        super();
        this.loadingSheetID = LoadingSheetID;
        this.vehicleID = VehicleID;
        this.shiftID = ShiftID;
    }

    private String getLoadingSheetID() { return this.loadingSheetID; }
    private String getVehicleID() { return this.vehicleID; }
    private String getShiftID() { return this.shiftID; }

    public void setLoadingSheetID(String loadingSheetID) { this.loadingSheetID = loadingSheetID; }
    public void setVehicleID(String vehicleID) { this.vehicleID = vehicleID; }
    public void setShiftID(String shiftID) { this.shiftID = shiftID; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("LoadingSheetID", this.loadingSheetID);
            obj.put("VehicleID", this.vehicleID);
            obj.put("ShiftID", this.shiftID);
        } catch (JSONException e) { }

        return obj;
    }

    // Loading Sheets
    public static void addLoadingSheet(LoadingSheet loadingSheet)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("LoadingSheetID", loadingSheet.loadingSheetID);
        values.put("VehicleID", loadingSheet.vehicleID);
        values.put("ShiftID", loadingSheet.shiftID);

        db.insert("LoadingSheets", null, values);
    }

    public static void deleteAllLoadingSheets()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheets");
    }

    public static void deleteLoadingSheets(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheetEquipment WHERE ShiftID = '" + shiftID + "'");

        db.execSQL("DELETE FROM LoadingSheets WHERE ShiftID = '" + shiftID + "'");
    }

    public static String GetLoadingSheetID(String shiftID, String vehicleID)
    {
        String query = "SELECT LoadingSheetID FROM LoadingSheets WHERE ShiftID = '" + shiftID + "' AND VehicleID = '" + vehicleID + "'";

        SQLiteDatabase db =null;
        Cursor cursor = null;
        String loadingSheetID = "";

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    loadingSheetID = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetLoadingSheetID");
        }
        finally
        {
            cursor.close();
        }

        return loadingSheetID;
    }

    public static JSONObject toSend(String shiftID) throws JSONException {
        JSONObject model = new JSONObject();

        JSONArray loadingSheets = new JSONArray();
        model.put("LoadingSheets", loadingSheets);

        String query = "SELECT * FROM LoadingSheets" +
                " WHERE ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject jpvObject;
            if (cursor.moveToFirst()) {
                do {
                    LoadingSheet ls = new LoadingSheet();
                    ls.loadingSheetID = cursor.getString(cursor.getColumnIndex("LoadingSheetID"));
                    ls.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                    ls.shiftID = shiftID;


                    jpvObject = ls.getJSONObject();
                    loadingSheets.put(jpvObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "GetLoadingSheetsToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return model;
    }

}
