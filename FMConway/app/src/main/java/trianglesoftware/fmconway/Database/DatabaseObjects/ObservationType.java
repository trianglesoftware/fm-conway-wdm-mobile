package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class ObservationType {
    public String observationTypeID;
    public String name;

    public ObservationType(){
    }

    public ObservationType(String ObservationTypeID, String Name)
    {
        super();
        this.observationTypeID = ObservationTypeID;
        this.name = Name;
    }

    private String getObservationTypeID() { return this.observationTypeID; }
    private String getName() { return this.name; }

    public void setObservationTypeID(String observationTypeID) { this.observationTypeID = observationTypeID; }
    public void setName(String name) { this.name = name; }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("ObservationTypeID", this.observationTypeID);
            obj.put("Name", this.name);

        return obj;
    }

    //Observation Types
    public static void addObservationType(ObservationType observationType)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ObservationTypeID", observationType.observationTypeID);
        values.put("Name", observationType.name);

        db.insert("ObservationTypes", null, values);
    }

    public static void deleteAllObservationTypes()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ObservationTypes");
    }

    public static List<ObservationType> GetObservationTypes() {
        List<ObservationType> observationTypes = new LinkedList<>();

        String query = "SELECT * FROM ObservationTypes";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ObservationType ot;
        if(cursor.moveToFirst()){
            do{
                ot = new ObservationType();
                ot.observationTypeID = cursor.getString(cursor.getColumnIndex("ObservationTypeID"));
                ot.name = cursor.getString(cursor.getColumnIndex("Name"));

                observationTypes.add(ot);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return observationTypes;
    }
}
