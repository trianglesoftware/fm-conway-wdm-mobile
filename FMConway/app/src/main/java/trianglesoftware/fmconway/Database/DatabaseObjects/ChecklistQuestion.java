package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class ChecklistQuestion {
    public String checklistQuestionID;
    public String question;
    public int questionOrder;
    public String checklistID;

    public ChecklistQuestion(){
    }

    public ChecklistQuestion(String ChecklistQuestionID, String Question, int QuestionOrder, String ChecklistID)
    {
        super();
        this.checklistQuestionID = ChecklistQuestionID;
        this.question = Question;
        this.questionOrder = QuestionOrder;
        this.checklistID = ChecklistID;
    }

    public String getChecklistQuestionID() { return this.checklistQuestionID; }
    private String getQuestion() { return this.question; }
    private int getQuestionOrder() { return this.questionOrder; }
    private String getChecklistID() { return this.checklistID; }

    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setQuestion(String question) { this.question = question; }
    public void setQuestionOrder(int questionOrder) { this.questionOrder = questionOrder; }
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Question", this.question);
            obj.put("QuestionOrder", this.questionOrder);
            obj.put("ChecklistID", this.checklistID);
        } catch (JSONException e) { }

        return obj;
    }

    // Checklist Questions
    public static void addChecklistQuestion(ChecklistQuestion checklistQuestion)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ChecklistQuestionID", checklistQuestion.checklistQuestionID);
        values.put("Question", checklistQuestion.question);
        values.put("QuestionOrder", checklistQuestion.questionOrder);
        values.put("ChecklistID", checklistQuestion.checklistID);

        db.insert("ChecklistQuestions", null, values);
    }

    public static void deleteAllChecklistQuestions()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ChecklistQuestions");
    }

    public static List<ChecklistQuestion> GetChecklistQuestionsForChecklist(String checklistID) {
        List<ChecklistQuestion> checklistQuestions = new LinkedList<>();

        String query = "SELECT * FROM ChecklistQuestions ";
        query += "WHERE ChecklistID = '" + checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ChecklistQuestion cq;
        if(cursor.moveToFirst()){
            do{
                cq = new ChecklistQuestion();
                cq.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                cq.question = cursor.getString(cursor.getColumnIndex("Question"));
                cq.questionOrder = Integer.parseInt(cursor.getString(cursor.getColumnIndex("QuestionOrder")));
                cq.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));

                checklistQuestions.add(cq);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return checklistQuestions;
    }

    public static List<ChecklistQuestion> GetChecklistQuestionsForMaintenanceChecklist() {
        List<ChecklistQuestion> checklistQuestions = new LinkedList<>();

        String query = "SELECT * FROM ChecklistQuestions cq ";
        query += "JOIN Checklists c on c.ChecklistID = cq.ChecklistID ";
        query += "WHERE c.IsMaintenance = 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ChecklistQuestion cq;
        if(cursor.moveToFirst()){
            do{
                cq = new ChecklistQuestion();
                cq.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                cq.question = cursor.getString(cursor.getColumnIndex("Question"));
                cq.questionOrder = Integer.parseInt(cursor.getString(cursor.getColumnIndex("QuestionOrder")));
                cq.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));

                checklistQuestions.add(cq);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return checklistQuestions;
    }
}
