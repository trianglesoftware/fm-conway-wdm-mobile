package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;


public class OutOfHours {
    public String activityID;
    public String jobTypeID;
    public String jobPackID;
    public String depotID;
    public String customerID;
    public Date startDate;
    public Date finishDate;
    public String shiftID;
    public String addressLine1;
    public String addressLine2;
    public String borough;
    public String city;
    public String county;
    public String postcode;

    public List<OutOfHoursBriefing> briefings;
    public List<OutOfHoursMethodStatement> methodStatements;
    public List<OutOfHoursShiftStaff> shiftStaffs;
    public List<OutOfHoursShiftVehicle> shiftVehicles;
    public List<OutOfHoursTask> tasks;

    public JSONObject getJSONObject() throws JSONException {
        JSONObject outOfHoursJSON = new JSONObject();

        // OutOfHours
        outOfHoursJSON.put("ActivityID", this.activityID);
        outOfHoursJSON.put("JobTypeID", this.jobTypeID);
        outOfHoursJSON.put("JobPackID", this.jobPackID);
        outOfHoursJSON.put("DepotID", this.depotID);
        outOfHoursJSON.put("CustomerID", this.customerID);
        outOfHoursJSON.put("StartDate", FMConwayUtils.getDateIsoString(this.startDate));
        outOfHoursJSON.put("FinishDate", FMConwayUtils.getDateIsoString(this.finishDate));
        outOfHoursJSON.put("ShiftID", this.shiftID);
        outOfHoursJSON.put("AddressLine1", this.addressLine1);
        outOfHoursJSON.put("AddressLine2", this.addressLine2);
        outOfHoursJSON.put("Borough", this.borough);
        outOfHoursJSON.put("City", this.city);
        outOfHoursJSON.put("County", this.county);
        outOfHoursJSON.put("Postcode", this.postcode);

        // Briefings
        JSONArray briefings = new JSONArray();
        for (OutOfHoursBriefing briefing : this.briefings) {
            JSONObject briefingJSON = new JSONObject();
            briefingJSON.put("BriefingID", briefing.briefingID);
            briefingJSON.put("BriefingTypeID", briefing.briefingTypeID);
            briefingJSON.put("Name", briefing.name);
            briefingJSON.put("FileID", briefing.fileID);

            briefings.put(briefingJSON);
        }
        outOfHoursJSON.put("Briefings", briefings);

        // MethodStatements
        JSONArray methodStatements = new JSONArray();
        for (OutOfHoursMethodStatement methodStatement : this.methodStatements) {
            JSONObject methodStatementJSON = new JSONObject();
            methodStatementJSON.put("ActivityMethodStatementID", methodStatement.activityMethodStatementID);
            methodStatementJSON.put("MethodStatementID", methodStatement.methodStatementID);

            methodStatements.put(methodStatementJSON);
        }
        outOfHoursJSON.put("MethodStatements", methodStatements);

        // ShiftStaffs
        JSONArray shiftStaffs = new JSONArray();
        for (OutOfHoursShiftStaff shiftStaff : this.shiftStaffs) {
            JSONObject shiftStaffJSON = new JSONObject();
            shiftStaffJSON.put("EmployeeID", shiftStaff.employeeID);

            shiftStaffs.put(shiftStaffJSON);
        }
        outOfHoursJSON.put("ShiftStaffs", shiftStaffs);

        // ShiftVehicles
        JSONArray shiftVehicles = new JSONArray();
        for (OutOfHoursShiftVehicle shiftVehicle : this.shiftVehicles) {
            JSONObject shiftVehicleJSON = new JSONObject();
            shiftVehicleJSON.put("VehicleID", shiftVehicle.vehicleID);

            shiftVehicles.put(shiftVehicleJSON);
        }
        outOfHoursJSON.put("ShiftVehicles", shiftVehicles);

        // Tasks
        JSONArray tasks = new JSONArray();
        for (OutOfHoursTask task : this.tasks) {
            JSONObject taskJSON = new JSONObject();
            taskJSON.put("TaskID", task.taskID);
            taskJSON.put("Name", task.name);
            taskJSON.put("TaskTypeID", task.taskTypeID);
            taskJSON.put("TaskOrder", task.taskOrder);
            taskJSON.put("ChecklistID", task.checklistID);
            taskJSON.put("PhotosRequired", task.photosRequired);
            taskJSON.put("ShowDetails", task.showDetails);
            taskJSON.put("ShowClientSignature", task.showClientSignature);
            taskJSON.put("ClientSignatureRequired", task.clientSignatureRequired);
            taskJSON.put("ShowStaffSignature", task.showStaffSignature);
            taskJSON.put("StaffSignatureRequired", task.staffSignatureRequired);
            taskJSON.put("Tag", task.tag);
            taskJSON.put("IsMandatory", task.isMandatory);

            tasks.put(taskJSON);
        }
        outOfHoursJSON.put("Tasks", tasks);

        return outOfHoursJSON;
    }

    public static boolean IsOutOfHoursShift(String shiftID) {
        boolean isOutOfHours = false;

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        String query = "select a.IsOutOfHours from Activities as a INNER JOIN JobPacks as jp on jp.JobPackID = a.JobPackID where jp.ShiftID = ?";
        Cursor cursor = db.rawQuery(query, new String[] { shiftID });

        if (cursor.moveToFirst()) {
            isOutOfHours = cursor.getInt(cursor.getColumnIndex("IsOutOfHours")) > 0;
        }

        return isOutOfHours;
    }

    public static JSONObject GetOutOfHoursToSend(String shiftID) throws Exception {
        OutOfHours ooh = new OutOfHours();

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        // OutOfHours
        String query = "select * from JobPacks where ShiftID = ?";
        Cursor cursor = db.rawQuery(query, new String[] { shiftID });

        cursor.moveToFirst();
        ooh.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
        ooh.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
        cursor.close();

        query = "select * from Activities where JobPackID = ?";
        cursor = db.rawQuery(query, new String[] { ooh.jobPackID });

        cursor.moveToFirst();
        ooh.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
        ooh.jobTypeID = cursor.getString(cursor.getColumnIndex("ActivityTypeID"));
        ooh.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
        ooh.customerID = cursor.getString(cursor.getColumnIndex("CustomerID"));
        ooh.startDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
        ooh.finishDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionFinishDate");
        cursor.close();

        query = "select * from Shifts where ShiftID = ?";
        cursor = db.rawQuery(query, new String[] { ooh.shiftID });

        cursor.moveToFirst();
        ooh.addressLine1 = cursor.getString(cursor.getColumnIndex("AddressLine1"));
        ooh.addressLine2 = cursor.getString(cursor.getColumnIndex("AddressLine2"));
        ooh.borough = cursor.getString(cursor.getColumnIndex("Borough"));
        ooh.city = cursor.getString(cursor.getColumnIndex("City"));
        ooh.county = cursor.getString(cursor.getColumnIndex("County"));
        ooh.postcode = cursor.getString(cursor.getColumnIndex("Postcode"));
        cursor.close();

        // Briefings
        query = "select * from Briefings where JobPackID = ?";
        cursor = db.rawQuery(query, new String[] { ooh.jobPackID });

        ooh.briefings = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                OutOfHoursBriefing briefing = new OutOfHoursBriefing();
                briefing.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                briefing.briefingTypeID = cursor.getString(cursor.getColumnIndex("BriefingTypeID"));
                briefing.name = cursor.getString(cursor.getColumnIndex("Name"));
                briefing.fileID = cursor.getString(cursor.getColumnIndex("DocumentID"));

                ooh.briefings.add(briefing);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // MethodStatements
        query = "select * from ActivityMethodStatements where ActivityID = ?";
        cursor = db.rawQuery(query, new String[] { ooh.activityID });

        ooh.methodStatements = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                OutOfHoursMethodStatement methodStatement = new OutOfHoursMethodStatement();
                methodStatement.activityMethodStatementID = cursor.getString(cursor.getColumnIndex("ActivityMethodStatementID"));
                methodStatement.methodStatementID = cursor.getString(cursor.getColumnIndex("MethodStatementID"));

                ooh.methodStatements.add(methodStatement);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // ShiftStaff
        query = "select * from ShiftStaff where ShiftID = ?";
        cursor = db.rawQuery(query, new String[] { ooh.shiftID });

        ooh.shiftStaffs = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                OutOfHoursShiftStaff shiftStaff = new OutOfHoursShiftStaff();
                shiftStaff.employeeID = cursor.getString(cursor.getColumnIndex("StaffID"));

                ooh.shiftStaffs.add(shiftStaff);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // ShiftVehicles
        query = "select * from ShiftVehicle where ShiftID = ? and VehicleID is not null";
        cursor = db.rawQuery(query, new String[] { ooh.shiftID });

        ooh.shiftVehicles = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                OutOfHoursShiftVehicle shiftVehicle = new OutOfHoursShiftVehicle();
                shiftVehicle.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));

                ooh.shiftVehicles.add(shiftVehicle);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // Tasks
        query = "select * from Tasks where ActivityID = ?";
        cursor = db.rawQuery(query, new String[] { ooh.activityID });

        ooh.tasks = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                OutOfHoursTask task = new OutOfHoursTask();
                task.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                task.name = cursor.getString(cursor.getColumnIndex("Name"));
                task.taskTypeID = cursor.getInt(cursor.getColumnIndex("TaskTypeID"));
                task.taskOrder = cursor.getInt(cursor.getColumnIndex("TaskOrder"));
                task.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                task.photosRequired = cursor.getInt(cursor.getColumnIndex("PhotosRequired")) > 0;
                task.showDetails = cursor.getInt(cursor.getColumnIndex("ShowDetails")) > 0;
                task.showClientSignature = cursor.getInt(cursor.getColumnIndex("ShowClientSignature")) > 0;
                task.clientSignatureRequired = cursor.getInt(cursor.getColumnIndex("ClientSignatureRequired")) > 0;
                task.showStaffSignature = cursor.getInt(cursor.getColumnIndex("ShowStaffSignature")) > 0;
                task.staffSignatureRequired = cursor.getInt(cursor.getColumnIndex("StaffSignatureRequired")) > 0;
                task.tag = cursor.getString(cursor.getColumnIndex("Tag"));
                task.isMandatory = cursor.getInt(cursor.getColumnIndex("IsMandatory")) > 0;

                ooh.tasks.add(task);
            } while (cursor.moveToNext());
        }
        cursor.close();

        JSONObject outOfHoursJSON = ooh.getJSONObject();

        return outOfHoursJSON;
    }

}

class OutOfHoursBriefing {
    public String briefingID;
    public String briefingTypeID;
    public String name;
    public String fileID;
}

class OutOfHoursMethodStatement {
    public String activityMethodStatementID;
    public String methodStatementID;
}

class OutOfHoursShiftStaff {
    public String employeeID;
}

class OutOfHoursShiftVehicle {
    public String vehicleID;
}

class OutOfHoursTask {
    public String taskID;
    public String name;
    public int taskTypeID;
    public int taskOrder;
    public String checklistID;
    public boolean photosRequired;
    public boolean showDetails;
    public boolean showClientSignature;
    public boolean clientSignatureRequired;
    public boolean showStaffSignature;
    public boolean staffSignatureRequired;
    public String tag;
    public boolean isMandatory;
}
