package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class TaskType {
    public int taskTypeID;
    public String name;

    public TaskType() {

    }

    public TaskType(int TaskTypeID, String Name) {
        super();
        this.taskTypeID = TaskTypeID;
        this.name = Name;
    }

    private int getTaskTypeID() {
        return this.taskTypeID;
    }

    private String getName() {
        return this.name;
    }

    public void setTaskTypeID(int taskTypeID) {
        this.taskTypeID = taskTypeID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("TaskTypeID", this.taskTypeID);
            obj.put("Name", this.name);
        } catch (JSONException e) {
        }

        return obj;
    }

    protected static TaskType cursorToModel(Cursor c) {
        TaskType taskType = new TaskType();
        taskType.taskTypeID = c.getInt(c.getColumnIndex("TaskTypeID"));
        taskType.name = c.getString(c.getColumnIndex("Name"));

        return taskType;
    }

    public static TaskType get(int taskTypeID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        TaskType model = null;
        Cursor c = db.rawQuery("SELECT * FROM TaskTypes", null);

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<TaskType> getAll() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        List<TaskType> models = new LinkedList<>();
        Cursor c = db.rawQuery("SELECT * FROM TaskTypes", null);

        if (c.moveToFirst()) {
            do {
                TaskType model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void addTaskType(TaskType taskType) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TaskTypeID", taskType.taskTypeID);
        values.put("Name", taskType.name);

        db.insert("TaskTypes", null, values);
    }

    public static void deleteAllTaskTypes() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskTypes");
    }

}
