package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;

public class TaskChecklistAnswerPhoto {
    public String taskChecklistAnswerPhotoID;
    public String taskChecklistAnswerID;
    public String imageLocation;
    public double longitude;
    public double latitude;
    public String activityID;
    public String shiftID;
    public String userID;
    public Date time;
    
    public Bitmap imageData;

    public TaskChecklistAnswerPhoto() {}

    private JSONObject getJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();

        obj.put("TaskChecklistAnswerPhotoID", this.taskChecklistAnswerPhotoID);
        obj.put("TaskChecklistAnswerID", this.taskChecklistAnswerID);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("ActivityID", this.activityID );
        obj.put("ShiftID", this.shiftID );
        obj.put("UserID", this.userID);
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        obj.put("Time", dateFormat.format(this.time));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options,400,400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
        if (bm != null) {

            try {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            }
            catch (Exception e)
            {
                ErrorLog.CreateError(e, "Rotate Photo");
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }

        return obj;
    }

    public static void addTaskChecklistAnswerPhoto(TaskChecklistAnswerPhoto taskChecklistAnswerPhoto)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TaskChecklistAnswerPhotoID", taskChecklistAnswerPhoto.taskChecklistAnswerPhotoID);
        values.put("TaskChecklistAnswerID", taskChecklistAnswerPhoto.taskChecklistAnswerID);
        values.put("ImageLocation", taskChecklistAnswerPhoto.imageLocation);
        values.put("Longitude", taskChecklistAnswerPhoto.longitude);
        values.put("Latitude", taskChecklistAnswerPhoto.latitude);
        values.put("ActivityID", taskChecklistAnswerPhoto.activityID);
        values.put("ShiftID", taskChecklistAnswerPhoto.shiftID);
        values.put("UserID", taskChecklistAnswerPhoto.userID);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String time = dateFormat.format(taskChecklistAnswerPhoto.time);
        values.put("Time", time);

        db.insert("TaskChecklistAnswerPhotos", null, values);
    }

    public static void deleteAllTaskChecklistAnswerPhotos()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswerPhotos");
    }

    public static void deleteTaskChecklistAnswerPhoto(String taskChecklistAnswerPhotoID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswerPhotos WHERE TaskChecklistAnswerPhotoID = '" + taskChecklistAnswerPhotoID + "'");
    }

    public static void deleteTaskChecklistAnswerPhotoFile(String taskChecklistAnswerPhotoID)
    {
        try {
            String query = "SELECT * FROM TaskChecklistAnswerPhotos ";
            query += "WHERE TaskChecklistAnswerPhotoID = '" + taskChecklistAnswerPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DeleteTaskChecklistAnswerPhotoFile");
        }
    }

    public static void deleteTaskChecklistAnswerPhotosForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswerPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteTaskChecklistAnswerPhotosForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswerPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static void deleteTaskChecklistAnswerPhotosForAnswer(String taskChecklistAnswerID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswerPhotos WHERE TaskChecklistAnswerID = '" + taskChecklistAnswerID + "'");
    }

    public static ArrayList<TaskChecklistAnswerPhoto> getPhotosForTaskChecklistAnswer(String taskChecklistAnswerID) {
        ArrayList<TaskChecklistAnswerPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM TaskChecklistAnswerPhotos WHERE TaskChecklistAnswerID = '" + taskChecklistAnswerID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        
        if(cursor.moveToFirst()) {
            do {
                TaskChecklistAnswerPhoto data = new TaskChecklistAnswerPhoto();
                data.taskChecklistAnswerPhotoID = cursor.getString(cursor.getColumnIndex("TaskChecklistAnswerPhotoID"));
                data.taskChecklistAnswerID = cursor.getString(cursor.getColumnIndex("TaskChecklistAnswerID"));
                data.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));;
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                data.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));;
                data.shiftID  = cursor.getString(cursor.getColumnIndex("ShiftID"));;
                data.userID  = cursor.getString(cursor.getColumnIndex("UserID"));;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(data.imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options,150,150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(data.imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetTaskChecklistPhotosToSend(String shiftID) throws Exception {
        JSONArray photos = new JSONArray();

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            String query = "SELECT * FROM TaskChecklistAnswerPhotos mtap ";
            query += "WHERE mtap.ShiftID = '" + shiftID + "'";

            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject pObject;
            if (cursor.moveToFirst()) {
                do {
                    TaskChecklistAnswerPhoto p = new TaskChecklistAnswerPhoto();
                    p.taskChecklistAnswerPhotoID = cursor.getString(cursor.getColumnIndex("TaskChecklistAnswerPhotoID"));
                    p.taskChecklistAnswerID = cursor.getString(cursor.getColumnIndex("TaskChecklistAnswerID"));
                    p.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    p.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    p.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    p.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                    p.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    p.userID = cursor.getString(cursor.getColumnIndex("UserID"));

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String time = cursor.getString(cursor.getColumnIndex("Time"));
                    p.time = format.parse(time);

                    pObject = p.getJSONObject();

                    photos.put(pObject);
                } while (cursor.moveToNext());
            }
        }
        catch( Exception e)
        {
            ErrorLog.CreateError(e, "GetTaskCheclistPhotosToSend");
            throw e;
        }
        finally
        {
            cursor.close();
        }

        return photos;
    }
}
