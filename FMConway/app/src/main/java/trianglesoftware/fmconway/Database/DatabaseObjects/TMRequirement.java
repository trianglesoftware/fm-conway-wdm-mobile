package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class TMRequirement {
    public String tmRequirementID;
    public String name;
    public boolean isActive;

    public TMRequirement() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("TMRequirementID", this.tmRequirementID);
            obj.put("Name", this.name);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "TMRequirement - getJSONObject");
        }
        return obj;
    }

    protected static TMRequirement cursorToModel(Cursor c) {
        TMRequirement tmRequirement = new TMRequirement();
        tmRequirement.tmRequirementID = c.getString(c.getColumnIndex("TMRequirementID"));
        tmRequirement.name = c.getString(c.getColumnIndex("Name"));
        tmRequirement.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return tmRequirement;
    }

    public static TMRequirement get(String tmRequirementID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        TMRequirement model = null;
        Cursor c = db.rawQuery("SELECT * FROM TMRequirements WHERE TMRequirementID = ?", new String[] { tmRequirementID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    // includeTMRequirementID is used to ensure previously selected item will still be in collection even if de-activated
    public static List<TMRequirement> getLookupItems(String includeTMRequirementID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<TMRequirement> models = new LinkedList<>();

        TMRequirement defaultItem = new TMRequirement();
        defaultItem.name = "";
        models.add(defaultItem);

        Cursor c;

        if (includeTMRequirementID != null) {
            c = db.rawQuery("SELECT * FROM TMRequirements WHERE IsActive = 1 or TMRequirementID = ? ORDER BY Name", new String[]{includeTMRequirementID});
        } else {
            c = db.rawQuery("SELECT * FROM TMRequirements WHERE IsActive = 1 ORDER BY Name", null);
        }

        if (c.moveToFirst()) {
            do {
                TMRequirement model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(TMRequirement model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("TMRequirementID", model.tmRequirementID);
            cv.put("Name", model.name);
            cv.put("IsActive", model.isActive);

            if (get(model.tmRequirementID) != null) {
                db.update("TMRequirements", cv, "TMRequirementID = ?", new String[]{model.tmRequirementID});
            } else {
                db.insertOrThrow("TMRequirements", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "TMRequirement - save");
        }
    }

    // required for spinner text
    @Override
    public String toString() {
        return name;
    }
}
