package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class ActivitySignature {
    public String activityID;
    public String staffID;
    public String userID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public String shiftID;

    public ActivitySignature(){
    }

    private String getActivityID() { return this.activityID; }
    private String getStaffID() { return this.staffID; }
    private String getUserID() {return this.userID;}
    private double getLongitude() { return this.longitude; }
    private double getLatitude() { return this.latitude; }
    private String getImageLocation() { return this.imageLocation; }

    public void setActivityID(String activityID) { this.activityID = activityID; }
    public void setStaffID(String staffID) { this.staffID = staffID; }
    public void setUserID(String userID) {this.userID = userID;}
    public void setLongitude(double longitude) { this.longitude = longitude; }
    public void setLatitude(double latitude) { this.latitude = latitude; }
    public void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("ActivityID", this.activityID);
            obj.put("StaffID", this.staffID);
            obj.put("Longitude", this.longitude);
            obj.put("Latitude", this.latitude);
            obj.put("ImageLocation", this.imageLocation);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.imageLocation, options);

            options.inSampleSize = calculateInSampleSize(options);
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);

            if (bm != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                obj.put("DataString", encodedImage);
            }
            else
            {
                ErrorLog log = new ErrorLog();
                log.exception = "Could not decode file. Image location: " + this.imageLocation ;
                log.description = "ActivitySignature";
                log.setSent(false);

                ErrorLog.AddErrorLog(log);
            }

        return obj;
    }

    //Activity Signatures
    public static void addActivitySignature(ActivitySignature activitySignature)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ActivityID", activitySignature.activityID);
        values.put("StaffID", activitySignature.staffID);
        values.put("Longitude", activitySignature.longitude);
        values.put("Latitude", activitySignature.latitude);
        values.put("ImageLocation", activitySignature.imageLocation);
        values.put("UserID", activitySignature.userID);
        values.put("ShiftID", activitySignature.shiftID);

        db.insert("ActivitySignatures", null, values);
    }

    public static void deleteActivitySignature(String activityID, String staffID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ActivitySignatures WHERE ActivityID = '" + activityID + "' AND StaffID = '" + staffID + "'");
    }

    public static void deleteActivitySignaturesForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ActivitySignatures WHERE UserID = '" + userID + "'");
    }

    public static void deleteActivitySignaturesForShiftID(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ActivitySignatures WHERE ShiftID = '" + shiftID + "'");
    }

    public static boolean CheckActivtySignatureExists(String activityID, String staffID) {
        boolean ret = false;

        String query = "SELECT * FROM ActivitySignatures ";
        query += "WHERE ActivityID = '" + activityID + "' AND StaffID = '" + staffID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                ret = true;
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static JSONArray GetSignaturesToSend(String shiftID) throws Exception{
        JSONArray signatures = new JSONArray();

        String query = "SELECT s.* FROM ActivitySignatures s ";
        query += "JOIN Activities a on a.ActivityID = s.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    ActivitySignature s = new ActivitySignature();
                    s.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                    s.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    sObject = s.getJSONObject();

                    signatures.put(sObject);
                } while (cursor.moveToNext());
            }

        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetActivitySignaturesToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return signatures;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > 400 || width > 400) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > 400
                    && (halfWidth / inSampleSize) > 400) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
