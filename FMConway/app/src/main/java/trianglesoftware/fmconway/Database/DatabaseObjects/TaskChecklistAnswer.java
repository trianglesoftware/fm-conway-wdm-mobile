package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class TaskChecklistAnswer {
    public String taskChecklistAnswerID;
    public String taskChecklistID;
    public String checklistQuestionID;
    public int answer;
    public Date time;
    public String reason;
    public boolean isNew;
    public String question;
    public String shiftID;

    public TaskChecklistAnswer(){
    }

    private String getTaskChecklistAnswerID() { return this.taskChecklistAnswerID; }
    private String getTaskChecklistID() { return this.taskChecklistID; }
    private String getChecklistQuestionID() { return this.checklistQuestionID; }
    private int getAnswer() { return this.answer; }
    private String getReason() { return this.reason; }
    public boolean getIsNew() { return this.isNew; }
    public String getQuestion() { return this.question; }
    public String getShiftID() {return this.shiftID;}

    public void setTaskChecklistAnswerID(String taskChecklistAnswerID) { this.taskChecklistAnswerID = taskChecklistAnswerID; }
    public void setTaskChecklistID(String taskChecklistID) { this.taskChecklistID = taskChecklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setAnswer(int answer) { this.answer = answer; }
    public void setReason(String reason) { this.reason = reason; }
    public void setNew(boolean isNew) { this.isNew = isNew; }
    private void setQuestion(String question) { this.question = question; }
    public void setShiftID(String shiftID) { this.shiftID = shiftID;}

    public JSONObject getJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("TaskChecklistAnswerID", this.taskChecklistAnswerID);
        obj.put("TaskChecklistID", this.taskChecklistID);
        obj.put("ChecklistQuestionID", this.checklistQuestionID);
        obj.put("Answer", this.answer);
        obj.put("Reason", this.reason);
        obj.put("IsNew", this.isNew);
        obj.put("Question", this.question);

        if (this.time != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            obj.put("Time", dateFormat.format(this.time));
        }

        return obj;
    }

    //Task Checklist Answers
    public static void addTaskChecklistAnswer(TaskChecklistAnswer taskChecklistAnswer)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TaskChecklistAnswerID", taskChecklistAnswer.taskChecklistAnswerID);
        values.put("TaskChecklistID", taskChecklistAnswer.taskChecklistID);
        values.put("ChecklistQuestionID", taskChecklistAnswer.checklistQuestionID);
        values.put("ShiftID", taskChecklistAnswer.shiftID);
        values.put("Reason", taskChecklistAnswer.reason);
        //values.put("Answer", taskChecklistAnswer.getAnswer());

        db.insert("TaskChecklistAnswers", null, values);
    }

//    public static void addTaskChecklistAnswerDownload(TaskChecklistAnswer vehicleChecklistAnswer)
//    {
//        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("TaskChecklistID", vehicleChecklistAnswer.getTaskChecklistID());
//        values.put("ChecklistQuestionID", vehicleChecklistAnswer.getChecklistQuestionID());
//        values.put("Answer", vehicleChecklistAnswer.getAnswer());
//
//        db.insert("TaskChecklistAnswers", null, values);
//    }

    public static void deleteAllTaskChecklistAnswersForCheck(String taskChecklistID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswers WHERE TaskChecklistID = '" + taskChecklistID + "'");
    }

    public static void deleteTaskChecklistAnswersForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM TaskChecklistAnswers WHERE ShiftID = '" + shiftID + "'");
    }

    public static List<TaskChecklistAnswer> GetTaskChecklistAnswers(String taskChecklistID) {
        List<TaskChecklistAnswer> taskChecklistAnswers = new LinkedList<>();

        String query = "SELECT vca.*, cq.Question FROM TaskChecklistAnswers vca ";
        query += "INNER JOIN ChecklistQuestions cq on cq.ChecklistQuestionID = vca.ChecklistQuestionID ";
        query += "WHERE TaskChecklistID = '" + taskChecklistID;
        query += "' ORDER BY cq.QuestionOrder";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        TaskChecklistAnswer tca;
        if(cursor.moveToFirst()){
            do{
                tca = new TaskChecklistAnswer();
                tca.taskChecklistAnswerID = cursor.getString(cursor.getColumnIndex("TaskChecklistAnswerID"));
                tca.taskChecklistID = cursor.getString(cursor.getColumnIndex("TaskChecklistID"));
                tca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                //tca.setAnswer(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0);
                tca.question = cursor.getString(cursor.getColumnIndex("Question"));
                tca.reason = cursor.getString(cursor.getColumnIndex("Reason"));

                int answerIndex = cursor.getColumnIndex("Answer");
                if (!cursor.isNull(answerIndex)) {
                    tca.answer = Integer.parseInt(cursor.getString(answerIndex));
                }
                else
                {
                    tca.isNew = true;
                }

                int timeIndex = cursor.getColumnIndex("Time");
                if (!cursor.isNull(timeIndex)) {
                    String timeString = cursor.getString(timeIndex);
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    try {
                        tca.time = format.parse(timeString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                taskChecklistAnswers.add(tca);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return taskChecklistAnswers;
    }

    public static TaskChecklistAnswer GetTaskChecklistAnswer(String taskChecklistAnswerID) {
        TaskChecklistAnswer tca = null;

        String query = "" +
            "select " +
            "* " +
            "from " +
            "TaskChecklistAnswers as tca " +
            "where " +
            "tca.TaskChecklistAnswerID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskChecklistAnswerID });

        if (cursor.moveToFirst()) {
            tca = new TaskChecklistAnswer();
            tca.taskChecklistAnswerID = cursor.getString(cursor.getColumnIndex("TaskChecklistAnswerID"));
            tca.taskChecklistID = cursor.getString(cursor.getColumnIndex("TaskChecklistID"));
            tca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
            tca.answer = cursor.getInt(cursor.getColumnIndex("Answer"));
            tca.reason = cursor.getString(cursor.getColumnIndex("Reason"));
            tca.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
        }

        cursor.close();

        return tca;
    }

    public static void resetAllAnswers(String taskChecklistID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        
        String query = "" +
            "update " +
            "TaskChecklistAnswers " +
            "set " +
            "Answer = null " +
            "where " +
            "TaskChecklistID = ? ";

        db.execSQL(query, new String[] { taskChecklistID });
    }

    public static void SetAnswer(String checklistQuestionID, String taskChecklistID, int answer) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        
        ContentValues contentValues = new ContentValues();
        contentValues.put("Answer", answer);
        contentValues.put("Time", dateFormat.format(now));
        
        db.update("TaskChecklistAnswers", contentValues, "TaskChecklistID = ? and ChecklistQuestionID = ?", new String[] { taskChecklistID, checklistQuestionID });
    }

    public static Date GetAnswerTime(String checklistQuestionID, String taskChecklistID) {
        Date time = null;

        String query = "" +
            "select " +
            "Time " +
            "from " +
            "TaskChecklistAnswers as tca " +
            "where " +
            "tca.TaskChecklistID = ? and " +
            "tca.ChecklistQuestionID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskChecklistID, checklistQuestionID });

        if (cursor.moveToFirst()) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String timeString = cursor.getString(cursor.getColumnIndex("Time"));
                
                if (timeString != null) {
                    time = format.parse(timeString);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        cursor.close();

        return time;
    }

    public static void SetAnswer(String checklistQuestionID, String taskChecklistID, boolean answer) {
        SetAnswer(checklistQuestionID, taskChecklistID, answer ? 1 : 0);
    }

    public static void UpdateReason(String taskChecklistAnswerID, String reason) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("Reason", reason);

        db.update("TaskChecklistAnswers", cv, "TaskChecklistAnswerID = ?", new String[]{taskChecklistAnswerID});
    }
}
