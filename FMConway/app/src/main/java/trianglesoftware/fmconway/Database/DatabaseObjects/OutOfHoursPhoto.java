package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

public class OutOfHoursPhoto {
    public String outOfHoursID;
    public String oohPhotoID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public String userID;
    public Date time;

    private String getoutOfHoursID
            () {
        return this.outOfHoursID
                ;
    }

    private double getLongitude() {
        return this.longitude;
    }

    private double getLatitude() {
        return this.latitude;
    }

    private String getImageLocation() {
        return this.imageLocation;
    }

    public Bitmap getImageData() {
        return this.imageData;
    }

    public String getUserID() {
        return this.userID;
    }

    public String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if (this.time != null) {
            return dateFormat.format(this.time);
        } else {
            return "";
        }
    }

    public void setoutOfHoursID(String outOfHoursID) {
        this.outOfHoursID = outOfHoursID;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    private void setImageData(Bitmap imageData) {
        this.imageData = imageData;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("OutOfHoursID", this.outOfHoursID);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Time", getTime());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options, 400, 400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
        if (bm != null) {

            try {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            } catch (Exception e) {
                ErrorLog.CreateError(e, "Rotate Photo");
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }
        return obj;
    }

    //OutOfHours Photos
    public static void addOutOfHoursPhoto(OutOfHoursPhoto outOfHoursPhoto) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("OOHPhotoID", UUID.randomUUID().toString());
        values.put("OutOfHoursID", outOfHoursPhoto.outOfHoursID);
        values.put("Longitude", outOfHoursPhoto.longitude);
        values.put("Latitude", outOfHoursPhoto.latitude);
        values.put("ImageLocation", outOfHoursPhoto.imageLocation);
        values.put("UserID", outOfHoursPhoto.userID);
        values.put("Time", outOfHoursPhoto.getTime());

        db.insert("OutOfHoursPhotos", null, values);
    }

    public static void deleteAllOutOfHoursPhotos(String outOfHoursID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM OutOfHoursPhotos WHERE outOfHoursID = '" + outOfHoursID + "'");
    }

    public static void deleteOutOfHoursPhotoFile(String oohPhotoID) {
        try {
            String query = "SELECT * FROM OutOfHoursPhotos ";
            query += "WHERE oohPhotoID = '" + oohPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DeleteOutOfHoursPhotoFile");
        }
    }

    public static void deleteOutOfHoursPhoto(String oohPhotoID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM OutOfHoursPhotos WHERE oohPhotoID = '" + oohPhotoID + "'");
    }

    public static ArrayList<OutOfHoursPhoto> getPhotosForOutOfHours(String outOfHoursID) {
        ArrayList<OutOfHoursPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM OutOfHoursPhotos WHERE outOfHoursID = '" + outOfHoursID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        OutOfHoursPhoto data;
        if (cursor.moveToFirst()) {
            do {
                data = new OutOfHoursPhoto();
                data.oohPhotoID = cursor.getString(cursor.getColumnIndex("OOHPhotoID"));
                data.outOfHoursID = cursor.getString(cursor.getColumnIndex("OutOfHoursID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));

                String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageLocation = imageLocation;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options, 150, 150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetOutOfHoursPhotosToSend(String outOfHoursID, String newoutOfHoursID) throws Exception {
        JSONArray photos = new JSONArray();

        String query = "SELECT p.* FROM OutOfHoursPhotos p ";
        query += "WHERE p.OutOfHoursID = '" + outOfHoursID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject pObject;
            if (cursor.moveToFirst()) {
                do {
                    OutOfHoursPhoto p = new OutOfHoursPhoto();
                    p.outOfHoursID = outOfHoursID;
                    p.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    p.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    p.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    pObject = p.getJSONObject();

                    photos.put(pObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            throw e;
        } finally {
            cursor.close();
        }

        return photos;
    }

    public static int GetOutOfHoursPhotosToSendCount(String outOfHoursID) {

        String query = "SELECT p.* FROM OutOfHoursPhotos p ";
        query += "WHERE p.OutOfHoursID = '" + outOfHoursID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int count = 0;
        if (cursor.moveToFirst()) {
            do {
                count++;
            } while (cursor.moveToNext());
        }

        cursor.close();

        return count;
    }
}
