package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 25/02/2016.
 */
public class User {
    public String userID;
    public String userTypeID;
    public String depotID;
    public String userName;
    public Boolean isStandardUser;
    public String employeeType;

    public User(){
    }

    public User(String UserID, String UserTypeID, String DepotID, String Username, Boolean IsStandardUser)
    {
        super();
        this.userID = UserID;
        this.userTypeID = UserTypeID;
        this.userName = Username;
        this.depotID = DepotID;
        this.isStandardUser = IsStandardUser;
    }

    private String getUserID() { return this.userID; }
    private String getUserTypeID() { return this.userTypeID; }
    private String getDepotID() { return this.depotID; }
    private String getUserName() { return this.userName; }
    private Boolean getIsStandardUser() { return this.isStandardUser; }

    public void setUserID(String userID) { this.userID = userID; }
    public void setUserTypeID(String userTypeID) { this.userTypeID = userTypeID; }
    public void setDepotID(String depotID) { this.depotID = depotID; }
    public void setUserName(String userName) { this.userName = userName; }
    public void setIsStandardUser(Boolean isStandardUser) { this.isStandardUser = isStandardUser; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("UserID", this.userID);
            obj.put("UserTypeID", this.userTypeID);
            obj.put("DepotID", this.depotID);
            obj.put("Username", this.userName);
            obj.put("IsStandardUser", this.isStandardUser);
            obj.put("EmployeeType", this.employeeType);
        } catch (JSONException e) { }

        return obj;
    }

    // Users
    public static void addUser(User user)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("UserID", user.userID);
        values.put("UserTypeID", user.userTypeID);
        values.put("DepotID", user.depotID);
        values.put("Username", user.userName);
        values.put("IsStandardUser", user.isStandardUser);
        values.put("EmployeeType", user.employeeType);

        db.insert("Users", null, values);
    }

    public static void updateUser(User user) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("UserID", user.userID);
        values.put("UserTypeID", user.userTypeID);
        values.put("DepotID", user.depotID);
        values.put("Username", user.userName);
        values.put("IsStandardUser", user.isStandardUser);
        values.put("EmployeeType", user.employeeType);

        db.update("Users", values, "UserID = ?", new String[]{user.userID});
    }

    public static boolean exists(String userID) {
        String query = "select * from Users where UserID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { userID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllUsers()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Users");
    }
    
    public static User getUser(String userID) {
        User user = null;

        String query = "" +
            "select " +
            "* " +
            "from " +
            "Users as u " +
            "where " +
            "u.UserID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{userID});

        if (cursor.moveToFirst()) {
            user = new User();
            user.userID = cursor.getString(cursor.getColumnIndex("UserID"));
            user.userTypeID = cursor.getString(cursor.getColumnIndex("UserTypeID"));
            user.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
            user.userName = cursor.getString(cursor.getColumnIndex("Username"));
            user.isStandardUser = cursor.getInt(cursor.getColumnIndex("IsStandardUser")) > 0;
            user.employeeType = cursor.getString(cursor.getColumnIndex("EmployeeType"));
        }

        cursor.close();

        return user;
    }

    public static List<User> getAllUsers()
    {
        List<User> users = new LinkedList<>();

        String query = "SELECT * FROM Users";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        User user;
        if(cursor.moveToFirst()){
            do{
                user = new User();
                user.userID = cursor.getString(cursor.getColumnIndex("UserID"));
                user.userTypeID = cursor.getString(cursor.getColumnIndex("UserTypeID"));
                user.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                user.userName = cursor.getString(cursor.getColumnIndex("Username"));
                user.isStandardUser = cursor.getInt(cursor.getColumnIndex("IsStandardUser")) > 0;
                user.employeeType = cursor.getString(cursor.getColumnIndex("EmployeeType"));

                users.add(user);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return users;
    }
}
