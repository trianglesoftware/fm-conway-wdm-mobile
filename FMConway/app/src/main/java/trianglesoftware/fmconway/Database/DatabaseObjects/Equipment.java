package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Equipment {
    public String equipmentID;
    public String name;
    public String equipmentTypeID;

    public Equipment(){
    }

    public Equipment(String EquipmentID, String Name, String EquipmentTypeID)
    {
        super();
        this.equipmentID = EquipmentID;
        this.name = Name;
        this.equipmentTypeID = EquipmentTypeID;
    }

    private String getEquipmentID() { return this.equipmentID; }
    public String getName() { return this.name; }
    private String getEquipmentTypeID() { return this.equipmentTypeID; }

    public void setEquipmentID(String equipmentID) { this.equipmentID = equipmentID; }
    public void setName(String name) { this.name = name; }
    public void setEquipmentTypeID(String equipmentTypeID) { this.equipmentTypeID = equipmentTypeID; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("EquipmentID", this.equipmentID);
            obj.put("Name", this.name);
            obj.put("EquipmentTypeID", this.equipmentTypeID);
        } catch (JSONException e) { }

        return obj;
    }

    // Equipment
    public static void addEquipment(Equipment equipment)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("EquipmentID", equipment.equipmentID);
        values.put("Name", equipment.name);
        values.put("EquipmentTypeID", equipment.equipmentTypeID);

        db.insert("Equipment", null, values);
    }

    public static void deleteAllEquipment()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Equipment");
    }

    public static List<LoadingSheetEquipment> GetEquipmentForVehicle(String shiftID, String vehicleID)
    {
        List<LoadingSheetEquipment> equipment = new LinkedList<>();

        String query = "";
        query += "select ";
        query += "lse.LoadingSheetEquipmentID, ";
        query += "e.Name, ";
        query += "et.Name as EquipmentType, ";
        query += "lse.Quantity, ";
        query += "et.VmsOrAsset, ";
        query += "coalesce(lsei.UnansweredQuestions, 0) as UnansweredQuestions ";
        query += "from ";
        query += "Equipment as e ";
        query += "inner join EquipmentTypes as et on et.EquipmentTypeID = e.EquipmentTypeID ";
        query += "inner join LoadingSheetEquipment as lse on lse.EquipmentID = e.EquipmentID ";
        query += "inner join LoadingSheets as ls on ls.LoadingSheetID = lse.LoadingSheetID ";
        query += "left join ( ";
        query += "	select ";
        query += "	lsei.LoadingSheetEquipmentID, ";
        query += "	count(*) as UnansweredQuestions ";
        query += "	from ";
        query += "	LoadingSheetEquipmentItems as lsei ";
        query += "	inner join EquipmentItemChecklists as eic on eic.LoadingSheetEquipmentItemID = lsei.LoadingSheetEquipmentItemID ";
        query += "	inner join EquipmentItemChecklistAnswers as eica on eica.EquipmentItemChecklistID = eic.EquipmentItemChecklistID ";
        query += "	where ";
        query += "	eica.Answer is null ";
        query += "	group by ";
        query += "	lsei.LoadingSheetEquipmentID ";
        query += ") as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "where ";
        query += "ls.ShiftID = ? and ls.VehicleID = ? ";
        query += "order by ";
        query += "lse.SheetOrder ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, vehicleID });

        LoadingSheetEquipment e;
        if(cursor.moveToFirst()){
            do {
                e = new LoadingSheetEquipment();
                e.loadingSheetEquipmentID = cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentID"));
                e.equipmentName = cursor.getString(cursor.getColumnIndex("Name"));
                e.equipmentType = cursor.getString(cursor.getColumnIndex("EquipmentType"));
                e.quantity = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Quantity")));
                e.vmsOrAsset = cursor.getString(cursor.getColumnIndex("VmsOrAsset"));
                e.unansweredQuestions = cursor.getInt(cursor.getColumnIndex("UnansweredQuestions"));

                equipment.add(e);
            } while(cursor.moveToNext());
        }

        cursor.close();

        return equipment;
    }

    public static List<Equipment> GetEquipmentForType(String equipmentTypeID)
    {
        List<Equipment> equipments = new LinkedList<>();

        String query = "SELECT e.* FROM Equipment e";
        query += " WHERE e.EquipmentTypeID = '" + equipmentTypeID + "'";
        query += " ORDER BY e.Name";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Equipment e;
        if(cursor.moveToFirst()){
            do {
                e = new Equipment();
                e.name = cursor.getString(cursor.getColumnIndex("Name"));
                e.equipmentID = cursor.getString(cursor.getColumnIndex("EquipmentID"));
                e.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));

                equipments.add(e);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return equipments;
    }

    public static List<Equipment> GetEquipmentForTypeAndShift(String equipmentTypeID, String shiftID)
    {
        List<Equipment> equipments = new LinkedList<>();

        String query = "SELECT e.* FROM Equipment e";
        query += " INNER JOIN LoadingSheetEquipment lse ON lse.EquipmentID = e.EquipmentID";
        query += " WHERE e.EquipmentTypeID = '" + equipmentTypeID + "'";
        query += " AND lse.ShiftID = '" + shiftID + "'";
        query += " ORDER BY e.Name";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Equipment e;
        if(cursor.moveToFirst()){
            do {
                e = new Equipment();
                e.name = cursor.getString(cursor.getColumnIndex("Name"));
                e.equipmentID = cursor.getString(cursor.getColumnIndex("EquipmentID"));
                e.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));

                equipments.add(e);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return equipments;
    }

    public static Equipment GetEquipmentFromName(String equipmentName)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT * FROM Equipment where Name = '" + equipmentName + "'";

        Cursor cursor = db.rawQuery(query, null);

        Equipment equipment = new Equipment();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            equipment.equipmentID = cursor.getString(cursor.getColumnIndex("EquipmentID"));
            equipment.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));
            cursor.close();
        }

        return equipment;
    }

    public static List<Equipment> GetAllEquipment(String searchTerm) {

        List<Equipment> equipments = new LinkedList<>();

        // select query
        String query = "SELECT * FROM Equipment e WHERE e.Name LIKE '%" + searchTerm + "%'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Equipment e;
        if(cursor.moveToFirst()){
            do{
                e = new Equipment();
                e.name = cursor.getString(cursor.getColumnIndex("Name"));
                e.equipmentID = cursor.getString(cursor.getColumnIndex("EquipmentID"));
                e.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));

                equipments.add(e);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return equipments;
    }

}
