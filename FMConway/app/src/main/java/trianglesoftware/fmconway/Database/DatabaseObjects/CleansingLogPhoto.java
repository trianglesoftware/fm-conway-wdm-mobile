package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class CleansingLogPhoto {

    public String cleansingLogPhotoID;
    public String cleansingLogID;
    public String userID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public Date time;
    public String shiftID;
    public String comments;

    public CleansingLogPhoto() {

    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("CleansingLogPhotoID", this.cleansingLogPhotoID);
        obj.put("CleansingLogID", this.cleansingLogID);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Time", FMConwayUtils.getDateIsoString(this.time));
        obj.put("Comments", this.comments);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options, 400, 400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
        if (bm != null) {
            try {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            } catch (Exception e) {
                ErrorLog.CreateError(e, "Rotate Photo");
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }

        return obj;
    }

    public static void saveCleansingLogPhoto(CleansingLogPhoto cleansingLogPhoto) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("CleansingLogPhotoID", cleansingLogPhoto.cleansingLogPhotoID);
        values.put("CleansingLogID", cleansingLogPhoto.cleansingLogID);
        values.put("Longitude", cleansingLogPhoto.longitude);
        values.put("Latitude", cleansingLogPhoto.latitude);
        values.put("ImageLocation", cleansingLogPhoto.imageLocation);
        values.put("UserID", cleansingLogPhoto.userID);
        values.put("Time", FMConwayUtils.getDateIsoString(cleansingLogPhoto.time));
        values.put("ShiftID", cleansingLogPhoto.shiftID);
        values.put("Comments", cleansingLogPhoto.comments);

        if (checkIfCleansingLogPhotoExists(cleansingLogPhoto.cleansingLogPhotoID)) {
            db.update("CleansingLogPhotos", values, "CleansingLogPhotoID = ?", new String[]{cleansingLogPhoto.cleansingLogPhotoID});
        } else {
            db.insert("CleansingLogPhotos", null, values);
        }
    }

    public static void saveComments(String cleansingLogPhotoID, String comments) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("Comments", comments);

        db.update("CleansingLogPhotos", cv, "CleansingLogPhotoID = ?", new String[]{cleansingLogPhotoID});
    }

    public static void deleteAllCleansingLogPhotos() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM CleansingLogPhotos");
    }

    public static void deleteCleansingLogPhoto(String cleansingLogPhotoID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM CleansingLogPhotos WHERE CleansingLogPhotoID = '" + cleansingLogPhotoID + "'");
    }

    public static void deleteCleansingLogPhotoFile(String cleansingLogPhotoID) {
        try {
            String query = "SELECT * FROM CleansingLogPhotos ";
            query += "WHERE CleansingLogPhotoID = '" + cleansingLogPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "DeleteCleansingLogPhotoFile");
        }
    }

    public static void deleteAllCleansingLogPhotosForUser(String userID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM CleansingLogPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllCleansingLogPhotosForShift(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM CleansingLogPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static CleansingLogPhoto getCleansingLogPhoto(String cleansingLogPhotoID) {
        CleansingLogPhoto cleansingLogPhoto = null;

        String query = "SELECT * FROM CleansingLogPhotos WHERE CleansingLogPhotoID = '" + cleansingLogPhotoID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            cleansingLogPhoto = new CleansingLogPhoto();
            cleansingLogPhoto.cleansingLogPhotoID = c.getString(c.getColumnIndex("CleansingLogPhotoID"));
            cleansingLogPhoto.cleansingLogID = c.getString(c.getColumnIndex("CleansingLogID"));
            cleansingLogPhoto.userID = c.getString(c.getColumnIndex("UserID"));
            cleansingLogPhoto.longitude = Double.parseDouble(c.getString(c.getColumnIndex("Longitude")));
            cleansingLogPhoto.latitude = Double.parseDouble(c.getString(c.getColumnIndex("Latitude")));
            cleansingLogPhoto.time = FMConwayUtils.getCursorDate(c, "Time");
            cleansingLogPhoto.imageLocation = c.getString(c.getColumnIndex("ImageLocation"));
            cleansingLogPhoto.shiftID = c.getString(c.getColumnIndex("ShiftID"));
            cleansingLogPhoto.comments = c.getString(c.getColumnIndex("Comments"));
        }

        c.close();

        return cleansingLogPhoto;
    }

    public static ArrayList<CleansingLogPhoto> getPhotosForCleansingLog(String cleansingLogID) {
        ArrayList<CleansingLogPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM CleansingLogPhotos WHERE CleansingLogID = '" + cleansingLogID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        CleansingLogPhoto data;
        if (cursor.moveToFirst()) {
            do {
                data = new CleansingLogPhoto();
                data.cleansingLogPhotoID = cursor.getString(cursor.getColumnIndex("CleansingLogPhotoID"));
                data.cleansingLogID = cursor.getString(cursor.getColumnIndex("CleansingLogID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                data.comments = cursor.getString(cursor.getColumnIndex("Comments"));

                String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageLocation = imageLocation;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options, 150, 150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(
        BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetPhotosToSend(String shiftID) throws Exception {
        JSONArray photos = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT clp.* FROM CleansingLogPhotos clp ";
        query += "JOIN CleansingLogs cl on cl.CleansingLogID = clp.CleansingLogID ";
        query += "JOIN Activities act on act.ActivityID = cl.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = act.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    CleansingLogPhoto s = new CleansingLogPhoto();
                    s.cleansingLogPhotoID = cursor.getString(cursor.getColumnIndex("CleansingLogPhotoID"));
                    s.cleansingLogID = cursor.getString(cursor.getColumnIndex("CleansingLogID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    s.comments = cursor.getString(cursor.getColumnIndex("Comments"));

                    try {
                        String time = cursor.getString(cursor.getColumnIndex("Time"));
                        if (time != null)
                            s.time = format.parse(time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    sObject = s.getJSONObject();

                    photos.put(sObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "GetCleansingLogPhotosToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return photos;
    }

    public static boolean checkIfCleansingLogPhotoExists(String cleansingLogPhotoID) {
        String query = "SELECT * FROM CleansingLogPhotos WHERE CleansingLogPhotoID = '" + cleansingLogPhotoID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        boolean result = cursor.moveToFirst();

        cursor.close();

        return result;
    }

    public static boolean checkIfPhotosExist(String taskID) {
        String query =
            "select " +
            "clp.* " +
            "from " +
            "CleansingLogPhotos as clp " +
            "inner join CleansingLogs cl on cl.CleansingLogID = clp.CleansingLogID " +
            "inner join Tasks t on t.TaskID = cl.TaskID " +
            "where " +
            "t.TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskID });

        boolean result = cursor.moveToFirst();
        
        cursor.close();

        return result;
    }

    public static boolean checkIfPhotosExistForActivity(String activityID) {
        String query = "" +
            "select " +
            "clp.* " +
            "from " +
            "CleansingLogPhotos as clp " +
            "inner join CleansingLogs cl on cl.CleansingLogID = clp.CleansingLogID " +
            "where " +
            "cl.ActivityID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { activityID });

        boolean result = cursor.moveToFirst();
        
        cursor.close();

        return result;
    }
}
