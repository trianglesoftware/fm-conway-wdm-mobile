package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class TaskChecklist {
    public String taskChecklistID;
    public String taskID;
    public String shiftID;
    public String checklistID;
    public String name;

    public TaskChecklist(){
    }

    public String getTaskChecklistID() { return this.taskChecklistID; }
    private String getTaskID() { return this.taskID; }
    private String getChecklistID() { return this.checklistID; }
    public String getShiftID() { return this.shiftID;}
    public String getName() { return this.name; }

    private void setTaskChecklistID(String taskChecklistID) { this.taskChecklistID = taskChecklistID; }
    public void setTaskID(String taskID) { this.taskID = taskID; }
    public void setShiftID(String shiftID) { this.shiftID = shiftID;}
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }
    public void setName(String name) { this.name = name; }

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("TaskChecklistID", this.taskChecklistID);
            obj.put("TaskID", this.taskID);
            obj.put("ChecklistID", this.checklistID);
            obj.put("Name", this.name);

        return obj;
    }

    //Task Checklists
    public static void addTaskChecklist(TaskChecklist taskChecklist)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TaskChecklistID", taskChecklist.taskChecklistID);
        values.put("TaskID", taskChecklist.taskID);
        values.put("ChecklistID", taskChecklist.checklistID);
        values.put("ShiftID", taskChecklist.shiftID);

        db.insert("TaskChecklists", null, values);
    }

    public static void deleteAllTaskChecklists()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskChecklists");
    }

    public static void deleteTaskChecklist(String taskChecklistID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskChecklists WHERE TaskChecklistID = '" + taskChecklistID + "'");
    }

    public static void deleteTaskChecklistsForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM TaskChecklists WHERE ShiftID = '" + shiftID + "'");
    }

    public static boolean CheckIfChecklistCreated(String taskID, String checklistID) {
        String query = "SELECT TaskChecklistID FROM TaskChecklists where TaskID = '"+taskID+ "' AND ChecklistID = '" +checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String taskChecklistID = "";

        if(cursor.moveToFirst()){
            do{
                taskChecklistID = cursor.getString(cursor.getColumnIndex("TaskChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return !Objects.equals(taskChecklistID, "");
    }

    public static TaskChecklist GetTaskChecklist(String taskID, String checklistID) {
        String query = "SELECT * FROM TaskChecklists where TaskID = '"+taskID+ "' AND ChecklistID = '" +checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        TaskChecklist taskChecklist = null;

        if(cursor.moveToFirst()){
            do{
                taskChecklist = new TaskChecklist();
                taskChecklist.taskChecklistID = cursor.getString(cursor.getColumnIndex("TaskChecklistID"));
                taskChecklist.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                taskChecklist.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return taskChecklist;
    }

    public static JSONArray GetTaskChecklistsToSend(String shiftID) throws Exception{
        JSONArray taskChecklists = new JSONArray();

        String query = "SELECT tc.* FROM TaskChecklists tc ";
        query += "JOIN Tasks t on t.TaskID = tc.TaskID ";
        query += "JOIN Activities a on a.ActivityID = t.ActivityID ";
        query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor cursorAnswers = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject tcObject;
            if (cursor.moveToFirst()) {
                do {
                    TaskChecklist tc = new TaskChecklist();
                    tc.taskID = cursor.getString(cursor.getColumnIndex("TaskID"));
                    tc.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                    tc.taskChecklistID = cursor.getString(cursor.getColumnIndex("TaskChecklistID"));

                    tcObject = tc.getJSONObject();

                    //Add Answers List
                    query = "SELECT tca.* FROM TaskChecklistAnswers tca ";
                    query += "WHERE tca.TaskChecklistID = '" + cursor.getString(cursor.getColumnIndex("TaskChecklistID")) + "'";

                    cursorAnswers = db.rawQuery(query, null);
                    JSONArray tcaObjects = new JSONArray();
                    if (cursorAnswers.moveToFirst()) {
                        do {
                            TaskChecklistAnswer tca = new TaskChecklistAnswer();
                            tca.taskChecklistAnswerID = cursorAnswers.getString(cursorAnswers.getColumnIndex("TaskChecklistAnswerID"));
                            tca.taskChecklistID = cursorAnswers.getString(cursorAnswers.getColumnIndex("TaskChecklistID"));
                            tca.checklistQuestionID = cursorAnswers.getString(cursorAnswers.getColumnIndex("ChecklistQuestionID"));
                            tca.reason = cursorAnswers.getString(cursorAnswers.getColumnIndex("Reason"));

                            int answerIndex = cursorAnswers.getColumnIndex("Answer");
                            if (!cursorAnswers.isNull(answerIndex)) {
                                tca.answer = Integer.parseInt(cursorAnswers.getString(answerIndex));
                            }

                            int timeIndex = cursorAnswers.getColumnIndex("Time");
                            if (!cursorAnswers.isNull(timeIndex)) {
                                String timeString = cursorAnswers.getString(timeIndex);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                tca.time = format.parse(timeString);
                            } else {
                                tca.time = new Date();
                            }

                            tcaObjects.put(tca.getJSONObject());

                        } while (cursorAnswers.moveToNext());
                    }
                    try {
                        tcObject.put("Answers", tcaObjects);
                    } catch (JSONException e) {
                        throw e;
                    }

                    taskChecklists.put(tcObject);

                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetTaskChecklistsToSend");
            throw e;
        }
        finally {
            cursor.close();
            if (cursorAnswers != null) {
                cursorAnswers.close();
            }
        }

        return taskChecklists;
    }
}
