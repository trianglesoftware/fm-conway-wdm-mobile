package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class BriefingChecklistSignature {
    public String briefingID;
    public String staffID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Date briefingStart;
    public Date briefingEnd;
    public String userID;
    public String shiftID;

    public BriefingChecklistSignature(){
    }

    private String getBriefingID() { return this.briefingID; }
    private String getStaffID() { return this.staffID; }
    private double getLongitude() { return this.longitude; }
    private double getLatitude() { return this.latitude; }
    private String getImageLocation() { return this.imageLocation; }
    private Date getBriefingStart() { return this.briefingStart; }
    private Date getBriefingEnd() { return this.briefingEnd; }
    private String getUserID() { return this.userID;}
    private String getBriefingStartString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.briefingStart != null){
            return dateFormat.format(this.briefingStart);
        }
        else {
            return "";
        }
    }
    private String getBriefingEndString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.briefingEnd != null){
            return dateFormat.format(this.briefingEnd);
        }
        else {
            return "";
        }
    }

    public void setBriefingID(String briefingID) { this.briefingID = briefingID; }
    public void setStaffID(String staffID) { this.staffID = staffID; }
    public void setLongitude(double longitude) { this.longitude = longitude; }
    public void setLatitude(double latitude) { this.latitude = latitude; }
    public void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }
    public void setBriefingStart(Date briefingStart) { this.briefingStart = briefingStart; }
    public void setBriefingEnd(Date briefingEnd) { this.briefingEnd = briefingEnd; }
    public void setUserID(String userID) {this.userID = userID;}

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("BriefingID", this.briefingID);
            obj.put("StaffID", this.staffID);
            obj.put("Longitude", this.longitude);
            obj.put("Latitude", this.latitude);
            obj.put("ImageLocation", this.imageLocation);
            obj.put("BriefingStart", getBriefingStartString());
            obj.put("BriefingEnd", getBriefingEndString());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.imageLocation, options);

            options.inSampleSize = calculateInSampleSize(options);
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);

            if (bm != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                obj.put("DataString", encodedImage);
            }



        return obj;
    }

    //Briefing Checklist Signatures
    public static void addBriefingChecklistSignature(BriefingChecklistSignature briefingChecklistSignature)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BriefingID", briefingChecklistSignature.briefingID);
        values.put("StaffID", briefingChecklistSignature.staffID);
        values.put("Longitude", briefingChecklistSignature.longitude);
        values.put("Latitude", briefingChecklistSignature.latitude);
        values.put("ImageLocation", briefingChecklistSignature.imageLocation);
        values.put("UserID", briefingChecklistSignature.userID);
        values.put("ShiftID", briefingChecklistSignature.shiftID);

        db.insert("BriefingChecklistSignatures", null, values);
    }

    public static void deleteBriefingChecklistSignature(String briefingID, String staffID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklistSignatures WHERE BriefingID = '" + briefingID + "' AND StaffID = '" + staffID + "'");
    }

    public static void deleteAllBriefingChecklistSignaturesForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklistSignatures WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllBriefingChecklistSignaturesForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM BriefingChecklistSignatures WHERE ShiftID = '" + shiftID + "'");
    }

    public static boolean CheckStaffSignatureExists(String briefingID, String staffID) {
        boolean ret = false;

        String query = "SELECT * FROM BriefingChecklistSignatures ";
        query += "WHERE BriefingID = '" + briefingID + "' AND StaffID = '" + staffID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                ret = true;
            }while(cursor.moveToNext());
        }

        cursor.close();

        return ret;
    }

    public static JSONArray GetSignaturesToSend(String shiftID) throws Exception{
        JSONArray signatures = new JSONArray();

        String query = "SELECT s.* FROM BriefingChecklistSignatures s ";
        query += "JOIN Briefings b on b.BriefingID = s.BriefingID ";
        query += "JOIN JobPacks jp on jp.JobPackID = b.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    BriefingChecklistSignature s = new BriefingChecklistSignature();
                    s.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                    s.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    try {
                        String briefingStart = cursor.getString(cursor.getColumnIndex("BriefingStart"));
                        if (briefingStart != null)
                            s.setBriefingStart(format.parse(briefingStart));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        String briefingEnd = cursor.getString(cursor.getColumnIndex("BriefingEnd"));
                        if (briefingEnd != null)
                            s.setBriefingEnd(format.parse(briefingEnd));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    sObject = s.getJSONObject();

                    signatures.put(sObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetBriefingSignaturesToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return signatures;
    }

    public static void UpdateSignature(String briefingID, String staffID, String briefingStart, String briefingEnd) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE BriefingChecklistSignatures SET BriefingStart = '" + briefingStart + "', BriefingEnd = '"+ briefingEnd +"' WHERE BriefingID = " + briefingID + " AND StaffID = " + staffID);

        //db.execSQL("UPDATE BriefingChecklistSignatures SET BriefingStart = ?, BriefingEnd = ? WHERE BriefingID = " + briefingID + " AND StaffID = " + staffID, new String[]{briefingStart, briefingEnd});

        SQLiteStatement statement = db.compileStatement("UPDATE BriefingChecklistSignatures SET BriefingStart = ?, BriefingEnd = ? WHERE BriefingID = ? AND StaffID = ?");
        statement.bindString(1,briefingStart);
        statement.bindString(2,briefingEnd);
        statement.bindString(3,briefingID);
        statement.bindString(4,staffID);
        statement.execute();
        statement.close();
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > 400 || width > 400) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > 400
                    && (halfWidth / inSampleSize) > 400) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
