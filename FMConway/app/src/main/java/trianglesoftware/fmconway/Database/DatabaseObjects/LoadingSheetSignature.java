package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class LoadingSheetSignature {
    public String loadingSheetID;
    public String shiftID;
    public String vehicleID;
    public String userID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Date time;

    public LoadingSheetSignature(){
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.time != null){
            return dateFormat.format(this.time);
        }
        else {
            return "";
        }
    }

    private JSONObject getJSONObject() throws Exception{

        JSONObject obj = new JSONObject();

            obj.put("LoadingSheetID", this.loadingSheetID);
            obj.put("ShiftID", this.shiftID);
            obj.put("VehicleID", this.vehicleID);
            obj.put("UserID", this.userID);
            obj.put("Longitude", this.longitude);
            obj.put("Latitude", this.latitude);
            obj.put("ImageLocation", this.imageLocation);
            obj.put("Time", getTime());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.imageLocation, options);

            options.inSampleSize = calculateInSampleSize(options);
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
            if (bm != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                obj.put("DataString", encodedImage);
            }


        return obj;
    }

    //Loading Sheet Signature
    public static void addLoadingSheetSignature(LoadingSheetSignature loadingSheetSignature)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("LoadingSheetID", loadingSheetSignature.loadingSheetID);
        values.put("ShiftID", loadingSheetSignature.shiftID);
        values.put("VehicleID", loadingSheetSignature.vehicleID);
        values.put("UserID", loadingSheetSignature.userID);
        values.put("Longitude", loadingSheetSignature.longitude);
        values.put("Latitude", loadingSheetSignature.latitude);
        values.put("ImageLocation", loadingSheetSignature.imageLocation);
        values.put("Time", loadingSheetSignature.getTime());

        db.insert("LoadingSheetSignatures", null, values);
    }

    public static void deleteLoadingSheetSignature(String loadingSheetID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheetSignatures WHERE LoadingSheetID = '" + loadingSheetID + "'");
    }

    public static void deleteLoadingSheetSignaturesForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheetSignatures WHERE UserID = '" + userID + "'");
    }

    public static void deleteLoadingSheetSignaturesForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheetSignatures WHERE ShiftID = '" + shiftID + "'");
    }

    public static JSONArray GetSignaturesToSend(String shiftID) throws Exception{
        JSONArray signatures = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT s.* FROM LoadingSheetSignatures s ";
        query += "WHERE s.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject sObject;
            if (cursor.moveToFirst()) {
                do {
                    LoadingSheetSignature s = new LoadingSheetSignature();
                    s.loadingSheetID = cursor.getString(cursor.getColumnIndex("LoadingSheetID"));
                    s.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    s.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                    s.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    s.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    s.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    try {
                        String time = cursor.getString(cursor.getColumnIndex("Time"));
                        if (time != null)
                            s.time = format.parse(time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    sObject = s.getJSONObject();

                    signatures.put(sObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetLoadingSheetSignaturesToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return signatures;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > 400 || width > 400) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > 400
                    && (halfWidth / inSampleSize) > 400) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
