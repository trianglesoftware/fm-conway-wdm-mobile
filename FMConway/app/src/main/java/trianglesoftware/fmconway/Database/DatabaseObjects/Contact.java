package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class Contact {
    public String contactID;
    public String customerID;
    public String contactName;
    public String phoneNumber;
    public String mobileNumber;
    public boolean isActive;

    public Contact() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ContactID", this.contactID);
            obj.put("CustomerID", this.customerID);
            obj.put("ContactName", this.contactName);
            obj.put("PhoneNumber", this.phoneNumber);
            obj.put("MobileNumber", this.mobileNumber);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "Contact - getJSONObject");
        }
        return obj;
    }

    protected static Contact cursorToModel(Cursor c) {
        Contact contact = new Contact();
        contact.contactID = c.getString(c.getColumnIndex("ContactID"));
        contact.customerID = c.getString(c.getColumnIndex("CustomerID"));
        contact.contactName = c.getString(c.getColumnIndex("ContactName"));
        contact.phoneNumber = c.getString(c.getColumnIndex("PhoneNumber"));
        contact.mobileNumber = c.getString(c.getColumnIndex("MobileNumber"));
        contact.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return contact;
    }

    public static Contact get(String contactID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        Contact model = null;
        Cursor c = db.rawQuery("SELECT * FROM Contacts WHERE ContactID = ?", new String[] { contactID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<Contact> getAllForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        List<Contact> models = new LinkedList<>();

        Cursor c = db.rawQuery("SELECT co.* FROM Contacts as co INNER JOIN Customers as c on c.CustomerID = co.CustomerID INNER JOIN Activities as a on a.CustomerID = c.CustomerID WHERE co.IsActive = 1 AND a.ActivityID = ? ORDER BY ContactName", new String[]{activityID});

        if (c.moveToFirst()) {
            do {
                Contact model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(Contact model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("ContactID", model.contactID);
            cv.put("CustomerID", model.customerID);
            cv.put("ContactName", model.contactName);
            cv.put("PhoneNumber", model.phoneNumber);
            cv.put("MobileNumber", model.mobileNumber);
            cv.put("IsActive", model.isActive);

            if (get(model.contactID) != null) {
                db.update("Contacts", cv, "ContactID = ?", new String[]{model.contactID});
            } else {
                db.insertOrThrow("Contacts", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "Contact - save");
        }
    }

}
