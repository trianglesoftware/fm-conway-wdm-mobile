package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.Date;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class ObservationPhoto {
    public String observationID;
    public String observationPhotoID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public String userID;
    public Date time;

    public ObservationPhoto(){
    }

    private String getObservationID() { return this.observationID; }
    private double getLongitude() { return this.longitude; }
    private double getLatitude() { return this.latitude; }
    private String getImageLocation() { return this.imageLocation; }
    public Bitmap getImageData() { return this.imageData; }
    public String getUserID() {return this.userID;}

    public String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.time != null){
            return dateFormat.format(this.time);
        }
        else {
            return "";
        }
    }

    public void setObservationID(String observationID) { this.observationID = observationID; }
    public void setLongitude(double longitude) { this.longitude = longitude; }
    public void setLatitude(double latitude) { this.latitude = latitude; }
    public void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }
    private void setImageData(Bitmap imageData) {
        this.imageData = imageData;
    }
    public void setUserID(String userID) {this.userID = userID;}

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("ObservationID", this.observationID);
            obj.put("Longitude", this.longitude);
            obj.put("Latitude", this.latitude);
            obj.put("ImageLocation", this.imageLocation);

            obj.put("Time", getTime());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.imageLocation, options);

            options.inSampleSize = calculateInSampleSize(options,400,400);
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
            if (bm != null) {

                try {
                    Matrix matrix = new Matrix();

                    matrix.postRotate(90);

                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                }
                catch (Exception e)
                {
                    ErrorLog.CreateError(e, "Rotate Photo");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                obj.put("DataString", encodedImage);
            }
        return obj;
    }

    //Observation Photos
    public static void addObservationPhoto(ObservationPhoto observationPhoto)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ObservationPhotoID", UUID.randomUUID().toString());
        values.put("ObservationID", observationPhoto.observationID);
        values.put("Longitude", observationPhoto.longitude);
        values.put("Latitude", observationPhoto.latitude);
        values.put("ImageLocation", observationPhoto.imageLocation);
        values.put("UserID", observationPhoto.userID);
        values.put("Time", observationPhoto.getTime());

        db.insert("ObservationPhotos", null, values);
    }

    public static void deleteAllObservationPhotos(String observationID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ObservationPhotos WHERE ObservationID = '" + observationID + "'");
    }

    public static void deleteObservationPhotoFile(String observationPhotoID)
    {
        try {
            String query = "SELECT * FROM ObservationPhotos ";
            query += "WHERE ObservationPhotoID = '" + observationPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DeleteObservationPhotoFile");
        }
    }

    public static void deleteObservationPhoto(String observationPhotoID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ObservationPhotos WHERE ObservationPhotoID = '" + observationPhotoID + "'");
    }

    public static ArrayList<ObservationPhoto> getPhotosForObservation(String observationID) {
        ArrayList<ObservationPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM ObservationPhotos WHERE ObservationID = '" + observationID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ObservationPhoto data;
        if(cursor.moveToFirst()) {
            do {
                data = new ObservationPhoto();
                data.observationPhotoID = cursor.getString(cursor.getColumnIndex("ObservationPhotoID"));
                data.observationID = cursor.getString(cursor.getColumnIndex("ObservationID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));

                String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageLocation = imageLocation;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options,150,150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetObservationPhotosToSend(String observationID, String newObservationID) throws Exception {
        JSONArray photos = new JSONArray();

        String query = "SELECT p.* FROM ObservationPhotos p ";
        query += "WHERE p.ObservationID = '" + observationID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject pObject;
            if (cursor.moveToFirst()) {
                do {
                    ObservationPhoto p = new ObservationPhoto();
                    p.observationID = newObservationID;
                    p.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    p.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    p.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    pObject = p.getJSONObject();

                    photos.put(pObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally {
            cursor.close();
        }

        return photos;
    }

    public static int GetObservationPhotosToSendCount(String observationID) {

        String query = "SELECT p.* FROM ObservationPhotos p ";
        query += "WHERE p.ObservationID = '" + observationID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int count = 0;
        if(cursor.moveToFirst()){
            do{
                count++;
            }while(cursor.moveToNext());
        }

        cursor.close();

        return count;
    }
}
