package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 04/03/2016.
 */
public class JobPack {
    public String jobPackID;
    public String shiftID;
    public String jobPackName;

    public JobPack(){
    }

    public JobPack(String JobPackID, String ShiftID, String JobPackName)
    {
        super();
        this.jobPackID = JobPackID;
        this.shiftID = ShiftID;
        this.jobPackName = JobPackName;
    }

    public String getJobPackID() { return this.jobPackID; }
    private String getShiftID() { return this.shiftID; }
    private String getJobPackName() { return this.jobPackName; }

    public void setJobPackID(String jobPackID) { this.jobPackID = jobPackID; }
    public void setShiftID(String shiftID) { this.shiftID = shiftID; }
    public void setJobPackName(String jobPackName) { this.jobPackName = jobPackName; }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("JobPackID", this.jobPackID);
            obj.put("ShiftID", this.shiftID);
            obj.put("JobPackName", this.jobPackName);

        return obj;
    }

    // Job Packs
    public static void addJobPack(JobPack jobPack)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("JobPackID", jobPack.jobPackID);
        values.put("ShiftID", jobPack.shiftID);
        values.put("JobPackName", jobPack.jobPackName);

        db.insert("JobPacks", null, values);
    }

    public static void deleteAllJobPacks()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM JobPacks");
    }

    public static void deleteJobPacksForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM JobPacks WHERE ShiftID = '" + shiftID + "'");
    }

    public static List<JobPack> getAllJobPacks()
    {
        List<JobPack> jobPacks = new LinkedList<>();

        String query = "SELECT * FROM JobPacks";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        JobPack jobPack;
        if(cursor.moveToFirst()){
            do{
                jobPack = new JobPack();
                jobPack.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                jobPack.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                jobPack.jobPackName = cursor.getString(cursor.getColumnIndex("JobPackName"));

                jobPacks.add(jobPack);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return jobPacks;
    }

    public static List<JobPack> GetJobPacksForShift(String shiftID)
    {
        List<JobPack> jobPacks = new LinkedList<>();

        String query = "SELECT * FROM JobPacks " +
                "s where ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        JobPack jp;
        if(cursor.moveToFirst()){
            do{
                jp = new JobPack();
                jp.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                jp.shiftID = shiftID;
                jp.jobPackName = cursor.getString(cursor.getColumnIndex("JobPackName"));

                jobPacks.add(jp);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return jobPacks;
    }

    public static String GetJobPackName(String jobPackID) {
        String query = "SELECT * FROM JobPacks where JobPackID = '" + jobPackID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String jobPackName = "";
        if(cursor.moveToFirst()){
            do{
                jobPackName = cursor.getString(cursor.getColumnIndex("JobPackName"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return jobPackName;
    }
}
