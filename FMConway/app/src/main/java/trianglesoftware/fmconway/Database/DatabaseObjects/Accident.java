package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 18/03/2016.
 */
public class Accident {
    public String accidentID;
    public String shiftID;
    public String accidentDetails;
    public Date accidentDate;
    public String location;
    public String registrations;
    public String personReporting;
    public String chevronDepot;
    public String peopleInvolved;
    public String witnesses;
    public String actionTaken;
    public boolean isNew;
    public String userID;

    public Accident() {
    }

    private String getAccidentID() {
        return this.accidentID;
    }

    private String getShiftID() {
        return this.shiftID;
    }

    public String getAccidentDetails() {
        return this.accidentDetails;
    }
    public String getLocation() {
        return this.location;
    }
    public String getRegistrations() {
        return this.registrations;
    }
    public String getPersonReporting() {
        return this.personReporting;
    }
    public String getChevronDepot() {
        return this.chevronDepot;
    }
    public String getPeopleInvolved() {
        return this.peopleInvolved;
    }
    public String getWitnesses() {
        return this.witnesses;
    }
    public String getActionTaken() {
        return this.actionTaken;
    }
    public boolean getIsNew() {
        return this.isNew;
    }
    public String getUserID() { return this.userID;}

    private String getAccidentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.accidentDate != null){
            return dateFormat.format(this.accidentDate);
        }
        else {
            return "";
        }
    }

    public void setAccidentID(String accidentID) {
        this.accidentID = accidentID;
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }

    public void setAccidentDetails(String accidentDetails) {
        this.accidentDetails = accidentDetails;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public void setRegistrations(String registrations) {
        this.registrations = registrations;
    }
    public void setPersonReporting(String personReporting) {
        this.personReporting = personReporting;
    }
    public void setChevronDepot(String chevronDepot) {
        this.chevronDepot = chevronDepot;
    }
    public void setPeopleInvolved(String peopleInvolved) {
        this.peopleInvolved = peopleInvolved;
    }
    public void setWitnesses(String witnesses) {
        this.witnesses = witnesses;
    }
    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }
    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }
    public void setUserID(String userID) { this.userID = userID;}

    public void setAccidentDate(Date accidentDate) {
        this.accidentDate = accidentDate;
    }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("AccidentID", this.accidentID);
            obj.put("ShiftID", this.shiftID);
            obj.put("AccidentDetails", this.accidentDetails);
            obj.put("Location", this.location);
            obj.put("Registrations", this.registrations);
            obj.put("PersonReporting", this.personReporting);
            //obj.put("ChevronDepot", this.chevronDepot);
            obj.put("PeopleInvolved", this.peopleInvolved);
            obj.put("Witnesses", this.witnesses);
            obj.put("ActionTaken", this.actionTaken);
            obj.put("IsNew", this.isNew);

            obj.put("AccidentDate", getAccidentDate());

        return obj;
    }

    //Accidents
    public static void AddAccident(Accident accident) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();

        if(!accident.isNew)
        {
            values.put("AccidentID", accident.accidentID);
        }
        else
        {
            values.put("AccidentID", UUID.randomUUID().toString());
        }

        if(accident.shiftID != null)
        {
            values.put("ShiftID", accident.shiftID);
        }
        else
        {
            values.put("ShiftID", "00000000-0000-0000-0000-000000000000");
        }
        values.put("AccidentDetails", accident.accidentDetails);
        values.put("AccidentDate", accident.getAccidentDate());
        values.put("Location", accident.location);
        values.put("Registrations", accident.registrations);
        values.put("PersonReporting", accident.personReporting);
        //values.put("ChevronDepot", accident.getChevronDepot());
        values.put("PeopleInvolved", accident.peopleInvolved);
        values.put("Witnesses", accident.witnesses);
        values.put("ActionTaken", accident.actionTaken);
        values.put("IsNew", accident.isNew);
        values.put("UserID", accident.userID);

        db.insert("Accidents", null, values);
    }

    public static void deleteAllAccidents() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Accidents");
    }

    public static void deleteAllAccidentsForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = null;
        try {
            String query = "SELECT * FROM AccidentPhotos WHERE UserID = '" + userID + "'";

            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    try {
                        File file = new File(imageLocation);
                        file.delete();
                    }
                    catch (Exception e)
                    {}

                } while (cursor.moveToNext());
            }

            db.execSQL("DELETE FROM AccidentPhotos WHERE UserID = '" + userID + "'");

            db.execSQL("DELETE FROM Accidents WHERE UserID = '" + userID + "'");


        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"DeleteAccidentsForUser");
        }
        finally
        {

            if (cursor != null)
            {
                cursor.close();
            }
        }
    }

    public static List<Accident> GetAccidentsForShift(String shiftID, String userID) {
        List<Accident> accidents = new LinkedList<>();

        String query = "SELECT * FROM Accidents ";
        query += "WHERE ShiftID = '" + shiftID + "' AND UserID = '" + userID + "'";
        query += " ORDER BY AccidentDate DESC";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Accident a;
        if(cursor.moveToFirst()){
            do{
                a = new Accident();
                a.accidentID = cursor.getString(cursor.getColumnIndex("AccidentID"));
                a.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                a.accidentDetails = cursor.getString(cursor.getColumnIndex("AccidentDetails"));
                a.location = cursor.getString(cursor.getColumnIndex("Location"));
                a.registrations = cursor.getString(cursor.getColumnIndex("Registrations"));
                a.personReporting = cursor.getString(cursor.getColumnIndex("PersonReporting"));
                //a.setChevronDepot(cursor.getString(cursor.getColumnIndex("ChevronDepot")));
                a.peopleInvolved = cursor.getString(cursor.getColumnIndex("PeopleInvolved"));
                a.witnesses = cursor.getString(cursor.getColumnIndex("Witnesses"));
                a.actionTaken = cursor.getString(cursor.getColumnIndex("ActionTaken"));
                try {
                    String noteDate = cursor.getString(cursor.getColumnIndex("AccidentDate"));
                    if(noteDate != null)
                        a.setAccidentDate(format.parse(noteDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                accidents.add(a);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return accidents;
    }

    public static Accident GetAccident(String accidentID) {
        String query = "SELECT * FROM Accidents ";
        query += "WHERE AccidentID = '" + accidentID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Accident a = null;
        if(cursor.moveToFirst()){
            do{
                a = new Accident();
                a.accidentID = cursor.getString(cursor.getColumnIndex("AccidentID"));
                a.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                a.accidentDetails = cursor.getString(cursor.getColumnIndex("AccidentDetails"));
                a.location = cursor.getString(cursor.getColumnIndex("Location"));
                a.registrations = cursor.getString(cursor.getColumnIndex("Registrations"));
                a.personReporting = cursor.getString(cursor.getColumnIndex("PersonReporting"));
                //a.setChevronDepot(cursor.getString(cursor.getColumnIndex("ChevronDepot")));
                a.peopleInvolved = cursor.getString(cursor.getColumnIndex("PeopleInvolved"));
                a.witnesses = cursor.getString(cursor.getColumnIndex("Witnesses"));
                a.actionTaken = cursor.getString(cursor.getColumnIndex("ActionTaken"));
                try {
                    String noteDate = cursor.getString(cursor.getColumnIndex("AccidentDate"));
                    if(noteDate != null)
                        a.setAccidentDate(format.parse(noteDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }while(cursor.moveToNext());
        }

        cursor.close();

        return a;
    }

    public static void UpdateAccident(Accident accident) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("AccidentID", accident.accidentID);
        values.put("AccidentDetails", accident.accidentDetails);
        values.put("ShiftID", accident.shiftID);
        values.put("Location", accident.location);
        values.put("Registrations", accident.registrations);
        values.put("PersonReporting", accident.personReporting);
        //values.put("ChevronDepot", accident.getChevronDepot());
        values.put("PeopleInvolved", accident.peopleInvolved);
        values.put("Witnesses", accident.witnesses);
        values.put("ActionTaken", accident.actionTaken);

        db.update("Accidents", values, "AccidentID = ?", new String[]{String.valueOf(accident.accidentID)});
    }

    public static JSONArray GetAccidentsToSend(String shiftID, String userID) throws Exception {
        JSONArray accidents = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT a.* FROM Accidents a ";
        query += "WHERE a.ShiftID = '" + shiftID + "' AND UserID = '" + userID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor cursorPhotos = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject aObject;
            if (cursor.moveToFirst()) {
                do {
                    Accident a = new Accident();
                    a.accidentID = cursor.getString(cursor.getColumnIndex("AccidentID"));
                    a.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    a.accidentDetails = cursor.getString(cursor.getColumnIndex("AccidentDetails"));
                    a.location = cursor.getString(cursor.getColumnIndex("Location"));
                    a.registrations = cursor.getString(cursor.getColumnIndex("Registrations"));
                    a.personReporting = cursor.getString(cursor.getColumnIndex("PersonReporting"));
                    //a.setChevronDepot(cursor.getString(cursor.getColumnIndex("ChevronDepot")));
                    a.peopleInvolved = cursor.getString(cursor.getColumnIndex("PeopleInvolved"));
                    a.witnesses = cursor.getString(cursor.getColumnIndex("Witnesses"));
                    a.actionTaken = cursor.getString(cursor.getColumnIndex("ActionTaken"));
                    a.setIsNew(Integer.parseInt(cursor.getString(cursor.getColumnIndex("IsNew"))) > 0);
                    try {
                        String accidentDate = cursor.getString(cursor.getColumnIndex("AccidentDate"));
                        if (accidentDate != null)
                            a.setAccidentDate(format.parse(accidentDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    aObject = a.getJSONObject();

                    // Add photos
                    query = "SELECT p.* FROM AccidentPhotos p ";
                    query += "WHERE p.AccidentID = '" + cursor.getString(cursor.getColumnIndex("AccidentID")) + "'";

                    cursorPhotos = db.rawQuery(query, null);
                    JSONArray apObjects = new JSONArray();
                    if (cursorPhotos.moveToFirst()) {
                        do {
                            AccidentPhoto p = new AccidentPhoto();
                            p.accidentID = cursorPhotos.getString(cursorPhotos.getColumnIndex("AccidentID"));
                            p.latitude = Double.parseDouble(cursorPhotos.getString(cursorPhotos.getColumnIndex("Latitude")));
                            p.longitude = Double.parseDouble(cursorPhotos.getString(cursorPhotos.getColumnIndex("Longitude")));
                            p.imageLocation = cursorPhotos.getString(cursorPhotos.getColumnIndex("ImageLocation"));

                            try {
                                String time = cursorPhotos.getString(cursorPhotos.getColumnIndex("Time"));
                                if (time != null)
                                    p.time = format.parse(time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            apObjects.put(p.getJSONObject());

                        } while (cursorPhotos.moveToNext());

                        try {
                            aObject.put("Photos", apObjects);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    cursorPhotos.close();

                    accidents.put(aObject);
                } while (cursor.moveToNext());
            }

        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetAccidentsToSend");
            throw e;
        }
        finally {
            cursor.close();
            if (cursorPhotos != null)
            {
                cursorPhotos.close();
            }
        }

        return accidents;
    }

    public static String GetLatestAccidentID() {
        String query = "SELECT AccidentID FROM Accidents ";
        query += "Order By AccidentDate Desc LIMIT 1";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String id = "";
        if(cursor.moveToFirst()){
            do{
                id = cursor.getString(cursor.getColumnIndex("AccidentID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return id;
    }
}
