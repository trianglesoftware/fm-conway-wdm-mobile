package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class VehicleTypeChecklist {
    public String vehicleTypeID;
    public String checklistID;

    public static final String Entity = "VehicleTypeChecklists";
    public static final String COL_VehicleTypeID = "VehicleTypeID";
    public static final String COL_ChecklistID = "ChecklistID";

    public VehicleTypeChecklist() {
    }

    public VehicleTypeChecklist(String VehicleTypeID, String ChecklistID) {
        super();
        this.vehicleTypeID = VehicleTypeID;
        this.checklistID = ChecklistID;
    }

    private String getVehicleTypeID() {
        return this.vehicleTypeID;
    }

    private String getChecklistID() {
        return this.checklistID;
    }

    public void setVehicleTypeID(String vehicleTypeID) {
        this.vehicleTypeID = vehicleTypeID;
    }

    public void setChecklistID(String checklistID) {
        this.checklistID = checklistID;
    }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("VehicleTypeID", this.vehicleTypeID);
        obj.put("ChecklistID", this.checklistID);

        return obj;
    }

    // Vehicle Type Checklists
    public static void addVehicleTypeChecklist(VehicleTypeChecklist vehicleTypeChecklist) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("VehicleTypeID", vehicleTypeChecklist.vehicleTypeID);
        values.put("ChecklistID", vehicleTypeChecklist.checklistID);

        db.insert("VehicleTypeChecklists", null, values);
//        try {
//            ContentValues values = new ContentValues();
//            values.put("VehicleTypeID", vehicleTypeChecklist.getVehicleTypeID());
//            values.put("ChecklistID", vehicleTypeChecklist.getChecklistID());
//
//            int vid = vehicleTypeChecklist.getVehicleTypeID();
//            int cid = vehicleTypeChecklist.getChecklistID();
//
//            if (exists(vid, cid))
//                db.update("VehicleTypeChecklists", values, COL_VehicleTypeID + " = ? AND " + COL_ChecklistID + " = ?",
//                        new String[]{String.valueOf(vid), String.valueOf(cid)});
//            else
//                db.insert("VehicleTypeChecklists", null, values);
//
//        } finally {
//        }

    }

    private static boolean exists(String vid, String cid) {

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + Entity + " WHERE " + COL_VehicleTypeID + " = ? AND " + COL_ChecklistID + " = ?", new String[]{String.valueOf(vid), String.valueOf(cid)});
        try {

            if (c.moveToNext())
                return true;

        } finally {
            c.close();
        }

        return false;
    }

    public static void deleteAllVehicleTypeChecklists() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM " + Entity);
    }
}
