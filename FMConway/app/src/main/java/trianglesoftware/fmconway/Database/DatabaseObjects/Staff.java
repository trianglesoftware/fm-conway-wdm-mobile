package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 01/03/2016.
 */
public class Staff {
    public String staffID;
    public String forename;
    public String surname;
    public String depotID;
    public String staffType;
    public boolean firstAider;
    public boolean isAgency;
    public String agencyEmployeeName;

    public Staff(){
    }

    public Staff(String StaffID, String Forename, String Surname, String DepotID)
    {
        super();
        this.staffID = StaffID;
        this.forename = Forename;
        this.surname = Surname;
        this.depotID = DepotID;
    }

    public String getStaffID() { return this.staffID; }
    public String getForename() { return this.forename; }
    public String getSurname() { return this.surname; }
    private String getDepotID() { return this.depotID; }
    public String getStaffType() { return this.staffType; }

    public void setStaffID(String staffID) { this.staffID = staffID; }
    public void setForename(String forename) { this.forename = forename; }
    public void setSurname(String surname) { this.surname = surname; }
    public void setDepotID(String depotID) { this.depotID = depotID; }
    public void setStaffType(String staffType) { this.staffType = staffType; }
    public void setFirstAider(boolean firstAider) { this.firstAider = firstAider; }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();
        obj.put("StaffID", this.staffID);
        obj.put("Forename", this.forename);
        obj.put("Surname", this.surname);
        obj.put("DepotID", this.depotID);
        obj.put("StaffType", this.staffType);
        obj.put("FirstAider", this.firstAider);
        obj.put("IsAgency", this.isAgency);
        obj.put("AgencyEmployeeName", this.agencyEmployeeName);

        return obj;
    }

    // Staff
    public static void addStaff(Staff staff)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("StaffID", staff.staffID);
        values.put("Forename", staff.forename);
        values.put("Surname", staff.surname);
        values.put("DepotID", staff.depotID);
        values.put("StaffType", staff.staffType);
        values.put("IsAgency", staff.isAgency);

        db.insert("Staff", null, values);
    }

    public static void updateStaff(Staff staff)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("StaffID", staff.staffID);
        values.put("Forename", staff.forename);
        values.put("Surname", staff.surname);
        values.put("DepotID", staff.depotID);
        values.put("StaffType", staff.staffType);
        values.put("IsAgency", staff.isAgency);

        db.update("Staff", values, "StaffID = ?", new String[]{staff.staffID});
    }

    public static boolean exists(String staffID) {
        String query = "select * from Staff where StaffID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { staffID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllStaff()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Staff");
    }

    public static List<Staff> GetStaffForShift(String shiftID)
    {
        List<Staff> staff = new LinkedList<>();

        String query = "SELECT s.*, ss.FirstAider, ss.AgencyEmployeeName FROM Staff s INNER JOIN ShiftStaff ss on ss.StaffID = s.StaffID where ss.NotOnShift = 0 AND ss.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Staff s;
        if(cursor.moveToFirst()){
            do{
                s = new Staff();
                s.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                s.forename = cursor.getString(cursor.getColumnIndex("Forename"));
                s.surname = cursor.getString(cursor.getColumnIndex("Surname"));
                s.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                s.staffType = cursor.getString(cursor.getColumnIndex("StaffType"));
                s.firstAider = Integer.parseInt(cursor.getString(cursor.getColumnIndex("FirstAider"))) > 0;
                s.isAgency = cursor.getInt(cursor.getColumnIndex("IsAgency")) > 0;
                s.agencyEmployeeName = cursor.getString(cursor.getColumnIndex("AgencyEmployeeName"));

                staff.add(s);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return staff;
    }

    public static boolean haveAllAgencyEmployeesBeenNamedForShift(String shiftID) {
        String query = "" + 
            "select " +
            "s.StaffID " +
            "from " +
            "ShiftStaff as ss " +
            "inner join Staff as s on s.StaffID = ss.StaffID " +
            "where " +
            "ss.ShiftID = ? and " +
            "s.IsAgency = 1 and " +
            "(ss.AgencyEmployeeName is null or ss.AgencyEmployeeName = '') ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID });

        boolean haveAllAgencyEmployeesBeenNamed = true;
        if (cursor != null && cursor.moveToFirst()) {
            haveAllAgencyEmployeesBeenNamed = false;
            cursor.close();
        }
        
        return haveAllAgencyEmployeesBeenNamed;
    }
    
    public static void updateAgencyEmployeeName(String shiftID, String staffID, String agencyEmployeeName) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        
        ContentValues values = new ContentValues();
        values.put("AgencyEmployeeName", agencyEmployeeName);
        db.update("ShiftStaff", values, "ShiftID = ? and StaffID = ?", new String[]{ shiftID, staffID });
    }

    public static List<Staff> GetStaffNotOnShift(String searchTerm) {

        List<Staff> staff = new LinkedList<>();

        // select query
        String query = "";
        query += "SELECT s.* FROM Staff s";
        query += " WHERE Forename LIKE '%" + searchTerm + "%'";
        query += " ORDER BY Forename";
        query += " LIMIT 0,5";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Staff s;
        if(cursor.moveToFirst()){
            do{
                s = new Staff();
                s.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                s.forename = cursor.getString(cursor.getColumnIndex("Forename"));
                s.surname = cursor.getString(cursor.getColumnIndex("Surname"));
                s.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                s.staffType = cursor.getString(cursor.getColumnIndex("StaffType"));

                staff.add(s);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return staff;
    }

    public static Staff GetStaffFromName(String staffName)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT StaffID FROM Staff where Forename = '" + staffName + "'" + " OR Surname = '" + staffName + "'";

        Cursor cursor = db.rawQuery(query, null);

        Staff staff = new Staff();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            staff.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
            cursor.close();
        }

        return staff;
    }

    public static String GetStaffName(String staffID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT Forename FROM Staff where StaffID = '" + staffID + "'";

        Cursor cursor = db.rawQuery(query, null);

        String staffName = "";
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            staffName = (cursor.getString(cursor.getColumnIndex("Forename")));
            cursor.close();
        }

        return staffName;
    }
}
