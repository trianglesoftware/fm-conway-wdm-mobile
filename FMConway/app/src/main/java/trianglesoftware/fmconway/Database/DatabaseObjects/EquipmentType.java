package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class EquipmentType {
    public String equipmentTypeID;
    public String name;
    public String vmsOrAsset;

    public EquipmentType(){
    }

    public EquipmentType(String EquipmentTypeID, String Name, String VmsOrAsset)
    {
        super();
        this.equipmentTypeID = EquipmentTypeID;
        this.name = Name;
        this.vmsOrAsset = VmsOrAsset;
    }

    private String getEquipmentTypeID() { return this.equipmentTypeID; }
    private String getName() { return this.name; }
    private String getVmsOrAsset() { return this.vmsOrAsset; }

    public void setEquipmentTypeID(String equipmentTypeID) { this.equipmentTypeID = equipmentTypeID; }
    public void setName(String name) { this.name = name; }
    public void setVmsOrAsset(String vmsOrAsset) { this.vmsOrAsset = vmsOrAsset; }


    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("EquipmentTypeID", this.equipmentTypeID);
            obj.put("Name", this.name);
            obj.put("VmsOrAsset", this.vmsOrAsset);
        } catch (JSONException e) { }

        return obj;
    }

    // Equipment Types
    public static void addEquipmentType(EquipmentType equipmentType)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("EquipmentTypeID", equipmentType.equipmentTypeID);
        values.put("Name", equipmentType.name);
        values.put("VmsOrAsset", equipmentType.vmsOrAsset);

        db.insert("EquipmentTypes", null, values);
}

    public static void deleteAllEquipmentTypes()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM EquipmentTypes");
    }

    public static List<EquipmentType> GetEquipmentTypes()
    {
        List<EquipmentType> types = new LinkedList<>();

        String query = "" +
            "select " +
            "et.Name, " +
            "et.EquipmentTypeID " +
            "from " +
            "EquipmentTypes as et " +
            "inner join Equipment as e on e.EquipmentTypeID = et.EquipmentTypeID " +
            "group by " +
            "et.Name, et.EquipmentTypeID " +
            "order by " +
            "et.Name";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        EquipmentType e;
        if (cursor.moveToFirst()){
            do {
                e = new EquipmentType();
                e.name = cursor.getString(cursor.getColumnIndex("Name"));
                e.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));

                types.add(e);
            } while (cursor.moveToNext());
        }
        
        cursor.close();

        return types;
    }

    public static List<EquipmentType> GetEquipmentTypesForShift(String shiftID)
    {
        List<EquipmentType> types = new LinkedList<>();
        
        String query = "" +
            "select " +
            "et.Name, " +
            "et.EquipmentTypeID " +
            "from " +
            "EquipmentTypes as et " +
            "inner join Equipment as e on e.EquipmentTypeID = et.EquipmentTypeID " +
            "inner join LoadingSheetEquipment lse on lse.EquipmentID = e.EquipmentID " +
            "where " +
            "lse.ShiftID = ? " +
            "group by " +
            "et.Name, et.EquipmentTypeID " +
            "order by " +
            "et.Name";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID });

        EquipmentType e;
        if(cursor.moveToFirst()){
            do {
                e = new EquipmentType();
                e.name = cursor.getString(cursor.getColumnIndex("Name"));
                e.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));

                types.add(e);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return types;
    }

    public static EquipmentType getVmsOrAsset(String equipmentTypeID) {
        EquipmentType result = new EquipmentType();
        String query = "SELECT * FROM EquipmentTypes WHERE EquipmentTypeID = '" + equipmentTypeID + "'" ;

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            result.equipmentTypeID = cursor.getString(cursor.getColumnIndex("EquipmentTypeID"));
            result.name = cursor.getString(cursor.getColumnIndex("Name"));
            result.vmsOrAsset = cursor.getString(cursor.getColumnIndex("VmsOrAsset"));
        }
        return result;
    }

}
