package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class UnloadingProcess {
    public String unloadingProcessID;
    public String taskID;
    public String activityID;
    public Date departToDisposalSiteDate;
    public Date arriveAtDisposalSiteDate;
    public String disposalSiteID;
    public String disposalSiteName;
    public String wasteTicketNumber;
    public Double volumeOfWasteDisposed;
    public String comments;
    public Date leaveDisposalSiteDate;
    public Date createdDate;

    public UnloadingProcess() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("UnloadingProcessID", this.unloadingProcessID);
            obj.put("TaskID", this.taskID);
            obj.put("ActivityID", this.activityID);
            obj.put("DepartToDisposalSiteDate", FMConwayUtils.getDateIsoString(this.departToDisposalSiteDate));
            obj.put("ArriveAtDisposalSiteDate", FMConwayUtils.getDateIsoString(this.arriveAtDisposalSiteDate));
            obj.put("DisposalSiteID", this.disposalSiteID);
            obj.put("DisposalSiteName", this.disposalSiteName);
            obj.put("WasteTicketNumber", this.wasteTicketNumber);
            obj.put("VolumeOfWasteDisposed", this.volumeOfWasteDisposed);
            obj.put("Comments", this.comments);
            obj.put("LeaveDisposalSiteDate", FMConwayUtils.getDateIsoString(this.leaveDisposalSiteDate));
            obj.put("CreatedDate", FMConwayUtils.getDateIsoString(this.createdDate));
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "UnloadingProcess - getJSONObject");
        }
        return obj;
    }

    protected static UnloadingProcess cursorToModel(Cursor c) {
        UnloadingProcess unloadingProcess = new UnloadingProcess();
        unloadingProcess.unloadingProcessID = c.getString(c.getColumnIndex("UnloadingProcessID"));
        unloadingProcess.taskID = c.getString(c.getColumnIndex("TaskID"));
        unloadingProcess.activityID = c.getString(c.getColumnIndex("ActivityID"));
        unloadingProcess.departToDisposalSiteDate = FMConwayUtils.getCursorDate(c, "DepartToDisposalSiteDate");
        unloadingProcess.arriveAtDisposalSiteDate = FMConwayUtils.getCursorDate(c, "ArriveAtDisposalSiteDate");
        unloadingProcess.disposalSiteID = c.getString(c.getColumnIndex("DisposalSiteID"));
        unloadingProcess.disposalSiteName = c.getString(c.getColumnIndex("DisposalSiteName"));
        unloadingProcess.wasteTicketNumber = c.getString(c.getColumnIndex("WasteTicketNumber"));
        unloadingProcess.volumeOfWasteDisposed = FMConwayUtils.getDouble(c, "VolumeOfWasteDisposed");
        unloadingProcess.comments = c.getString(c.getColumnIndex("Comments"));
        unloadingProcess.leaveDisposalSiteDate = FMConwayUtils.getCursorDate(c, "LeaveDisposalSiteDate");
        unloadingProcess.createdDate = FMConwayUtils.getCursorDate(c, "CreatedDate");

        return unloadingProcess;
    }

    public static UnloadingProcess get(String unloadingProcessID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        UnloadingProcess model = null;
        Cursor c = db.rawQuery("SELECT up.*, ds.Name as DisposalSiteName FROM UnloadingProcesses up LEFT JOIN DisposalSites ds on ds.DisposalSiteID = up.DisposalSiteID WHERE UnloadingProcessID = ?", new String[]{unloadingProcessID});

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<UnloadingProcess> getAllForTask(String taskID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        List<UnloadingProcess> models = new LinkedList<>();
        Cursor c = db.rawQuery("SELECT up.*, ds.Name as DisposalSiteName FROM UnloadingProcesses up LEFT JOIN DisposalSites ds on ds.DisposalSiteID = up.DisposalSiteID WHERE TaskID = ? ORDER BY CreatedDate", new String[]{taskID});

        if (c.moveToFirst()) {
            do {
                UnloadingProcess model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(UnloadingProcess model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("UnloadingProcessID", model.unloadingProcessID);
            cv.put("TaskID", model.taskID);
            cv.put("ActivityID", model.activityID);
            cv.put("DepartToDisposalSiteDate", FMConwayUtils.getDateIsoString(model.departToDisposalSiteDate));
            cv.put("ArriveAtDisposalSiteDate", FMConwayUtils.getDateIsoString(model.arriveAtDisposalSiteDate));
            cv.put("DisposalSiteID", model.disposalSiteID);
            cv.put("WasteTicketNumber", model.wasteTicketNumber);
            cv.put("VolumeOfWasteDisposed", model.volumeOfWasteDisposed);
            cv.put("Comments", model.comments);
            cv.put("LeaveDisposalSiteDate", FMConwayUtils.getDateIsoString(model.leaveDisposalSiteDate));
            cv.put("CreatedDate", FMConwayUtils.getDateIsoString(model.createdDate));

            if (get(model.unloadingProcessID) != null) {
                db.update("UnloadingProcesses", cv, "UnloadingProcessID = ?", new String[]{model.unloadingProcessID});
            } else {
                db.insertOrThrow("UnloadingProcesses", null, cv);
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "UnloadingProcess - save");
        }
    }

    public static int delete(String unloadingProcessID) {
        ArrayList<UnloadingProcessPhoto> unloadingProcessPhotos = UnloadingProcessPhoto.getPhotosForUnloadingProcess(unloadingProcessID);
        for (UnloadingProcessPhoto unloadingProcessPhoto : unloadingProcessPhotos) {
            UnloadingProcessPhoto.deleteUnloadingProcessPhotoFile(unloadingProcessPhoto.unloadingProcessPhotoID);
            UnloadingProcessPhoto.deleteUnloadingProcessPhoto(unloadingProcessPhoto.unloadingProcessPhotoID);
        }

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        return db.delete("UnloadingProcesses", "UnloadingProcessID = ?", new String[]{unloadingProcessID});
    }

    public static int deleteUnloadingProcessesForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        return db.delete("UnloadingProcesses", "ActivityID = ?", new String[]{activityID});
    }

    public static JSONArray getUnloadingProcessesToSend(String shiftID) throws Exception {
        JSONArray jsonArray = new JSONArray();

        SQLiteDatabase db = null;
        Cursor c = null;

        try {
            String query = "SELECT up.*, ds.Name as DisposalSiteName FROM UnloadingProcesses up ";
            query += "LEFT JOIN DisposalSites ds on ds.DisposalSiteID = up.DisposalSiteID ";
            query += "JOIN Activities a on a.ActivityID = up.ActivityID ";
            query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
            query += "WHERE jp.ShiftID = '" + shiftID + "'";

            db = Database.MainDB.getWritableDatabase();
            c = db.rawQuery(query, null);

            if (c.moveToFirst()) {
                do {
                    UnloadingProcess unloadingProcess = cursorToModel(c);
                    JSONObject jsonObject = unloadingProcess.getJSONObject();
                    jsonArray.put(jsonObject);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "getUnloadingProcessesToSend");
            throw e;
        } finally {
            c.close();
        }

        return jsonArray;
    }

}
