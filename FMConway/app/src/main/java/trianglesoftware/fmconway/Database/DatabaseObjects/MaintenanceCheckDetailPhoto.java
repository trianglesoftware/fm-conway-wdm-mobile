package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Adam.Patrick on 01/09/2016.
 */
public class MaintenanceCheckDetailPhoto {
    public String maintenanceCheckID;
    public String maintenanceCheckDetailPhotoID;
    public String checklistQuestionID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public String activityID;
    public String userID;
    public String shiftID;

    public MaintenanceCheckDetailPhoto() {}

    private String getMaintenanceCheckID() { return this.maintenanceCheckID;}
    private String getChecklistQuestionID() { return this.checklistQuestionID;}
    private double getLongitude() { return this.longitude;}
    private double getLatitude() { return this.latitude;}
    private String getImageLocation() { return this.imageLocation;}
    public Bitmap getImageData() { return this.imageData;}
    public String getActivityID() {return this.activityID;}
    public String getUserID() { return this.userID;}

    public void setMaintenanceCheckID(String maintenanceCheckID) {this.maintenanceCheckID = maintenanceCheckID;}
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID;}
    public void setLongitude(double longitude) {this.longitude = longitude;}
    public void setLatitude(double latitude) {this.latitude = latitude;}
    public void setImageLocation(String imageLocation) {this.imageLocation = imageLocation;}
    public void setImageData(Bitmap imageData) {this.imageData = imageData;}
    public void setActivityID(String activityID) { this.activityID = activityID;}
    public void setUserID(String userID) {this.userID = userID;}

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("MaintenanceCheckDetailPhotoID", this.maintenanceCheckDetailPhotoID);
            obj.put("MaintenanceCheckID", this.maintenanceCheckID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Longitude", this.longitude);
            obj.put("Latitude", this.latitude);
            obj.put("ImageLocation", this.imageLocation);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.imageLocation, options);

            options.inSampleSize = calculateInSampleSize(options,400,400);
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
            if (bm != null) {

                try {
                    Matrix matrix = new Matrix();

                    matrix.postRotate(90);

                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                }
                catch (Exception e)
                {
                    ErrorLog.CreateError(e, "Rotate Photo");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                obj.put("DataString", encodedImage);
            }


        return obj;
    }

    public static void addMaintenanceCheckDetailPhoto(MaintenanceCheckDetailPhoto maintenanceCheckDetailPhoto)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("MaintenanceCheckDetailPhotoID", UUID.randomUUID().toString());
        values.put("MaintenanceCheckID", maintenanceCheckDetailPhoto.maintenanceCheckID);
        values.put("ChecklistQuestionID", maintenanceCheckDetailPhoto.checklistQuestionID);
        values.put("Longitude", maintenanceCheckDetailPhoto.longitude);
        values.put("Latitude", maintenanceCheckDetailPhoto.latitude);
        values.put("ImageLocation", maintenanceCheckDetailPhoto.imageLocation);
        values.put("ActivityID", maintenanceCheckDetailPhoto.activityID);
        values.put("UserID", maintenanceCheckDetailPhoto.userID);
        values.put("ShiftID", maintenanceCheckDetailPhoto.shiftID);

        db.insert("MaintenanceCheckDetailPhotos", null, values);
    }

    public static void deleteAllMaintenanceCheckDetailPhotos()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetailPhotos");
    }

    public static void deleteMaintenanceCheckDetailPhoto(String maintenanceCheckDetailPhotoID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetailPhotos WHERE MaintenanceCheckDetailPhotoID = '" + maintenanceCheckDetailPhotoID + "'");
    }

    public static void deleteMaintenanceCheckDetailPhotoFile(String maintenanceCheckDetailPhotoID)
    {
        try {
            String query = "SELECT * FROM MaintenanceCheckDetailPhotos ";
            query += "WHERE MaintenanceCheckDetailPhotoID = '" + maintenanceCheckDetailPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DeleteMaintenanceCheckDetailPhotoFile");
        }
    }

    public static void deleteMaintenanceCheckDetailPhotosForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetailPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteMaintenanceCheckDetailPhotosForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetailPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static void deleteMaintenanceCheckDetailPhotosForCheck(String maintenanceCheckID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetailPhotos WHERE MaintenanceCheckID = '" + maintenanceCheckID + "'");
    }

    public static ArrayList<MaintenanceCheckDetailPhoto> getPhotosForMaintenanceCheckDetail(String maintenanceCheckID, String checklistQuestionID) {
        ArrayList<MaintenanceCheckDetailPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM MaintenanceCheckDetailPhotos WHERE MaintenanceCheckID = '" + maintenanceCheckID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        MaintenanceCheckDetailPhoto data;
        if(cursor.moveToFirst()) {
            do {
                data = new MaintenanceCheckDetailPhoto();

                data.maintenanceCheckDetailPhotoID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckDetailPhotoID"));
                data.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                data.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));

                String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageLocation = imageLocation;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options,150,150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetMaintenanceCheckDetailPhotosToSend(String shiftID) throws Exception {
        JSONArray photos = new JSONArray();

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            String query = "SELECT * FROM MaintenanceCheckDetailPhotos mcdp ";
            query += "JOIN MaintenanceChecks mc ON mc.MaintenanceCheckID = mcdp.MaintenanceCheckID ";
            query += "JOIN Activities a on a.ActivityID = mc.ActivityID ";
            query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
            query += "WHERE jp.ShiftID = '" + shiftID + "'";

            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject pObject;
            if (cursor.moveToFirst()) {
                do {
                    MaintenanceCheckDetailPhoto p = new MaintenanceCheckDetailPhoto();
                    p.maintenanceCheckDetailPhotoID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckDetailPhotoID"));
                    p.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                    p.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                    p.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    p.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    p.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    pObject = p.getJSONObject();

                    photos.put(pObject);
                } while (cursor.moveToNext());
            }
        }
        catch( Exception e)
        {
            ErrorLog.CreateError(e, "GetMaintenanceCheckDetailPhotosToSend");
            throw e;
        }
        finally
        {
            cursor.close();
        }

        return photos;
    }
}
