package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class ShiftUpdate {
    public String shiftID;
    public boolean shiftComplete;
    public boolean isCancelled;
    public boolean isActive;
    public boolean isOnShift;
    public Date shiftDate;
    public Date updatedDate;
    public Integer orderNumber;
    public List<ShiftVehicle> shiftVehicles;
    public List<VehicleChecklist> vehicleChecklists;

    public void save() {
        // skip shiftDate update if null (shiftDate is null when save() comes from propagateVehicleChecklistsIfPossible())
        if (shiftDate != null) {
            Shift.updateShiftDate(shiftID, shiftDate);
        }

        if (updatedDate != null) {
            Shift.saveShiftUpdatedDate(shiftID, updatedDate);
        }

        if (orderNumber != null) {
            Activity.updateOrderNumber(shiftID, orderNumber);
        }

        try {
            // skip ShiftVehicle and VehicleChecklists updates if vehicle checklist has been confirmed
            int shiftCompletionStatus = Shift.GetShiftCompletionStatus(shiftID);
            if (shiftCompletionStatus >= 3) {
                return;
            }
        } catch (Exception e) {
            Toast.makeText(FMConwayApp.getContext(), "An error occurred within GetShiftCompletionStatus", Toast.LENGTH_SHORT).show();
            return;
        }

        // update ShiftVehicle
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        for (ShiftVehicle shiftVehicle : shiftVehicles) {
            ContentValues values = new ContentValues();
            values.put("StaffID", shiftVehicle.staffID);
            values.put("StartMileage", shiftVehicle.startMileage);

            db.update("ShiftVehicle", values, "ShiftID = ? and VehicleID = ?", new String[] { shiftVehicle.shiftID, shiftVehicle.vehicleID });
        }

        // update VehicleChecklists
        for (VehicleChecklist vehicleChecklist : vehicleChecklists) {
            CreateVehicleChecklistIfRequired(vehicleChecklist);

            // VehicleChecklistID stored locally may not always equal that sent from the service
            String vehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(vehicleChecklist.vehicleID, shiftID, vehicleChecklist.checklistID, true);

            // photo
            if (!TextUtils.isEmpty(vehicleChecklist.imageFilename)) {
                if (!FMConwayUtils.isNullOrWhitespace(vehicleChecklist.imageLocation)) {
                    VehicleChecklist.updatePhoto(vehicleChecklistID, vehicleChecklist.imageLocation);
                } else if (!FMConwayUtils.isNullOrWhitespace(vehicleChecklist.imageData)) {
                    Bitmap bmp = FMConwayUtils.base64ToBmp(vehicleChecklist.imageData);
                    String filepath = FMConwayUtils.getPictureFile(vehicleChecklist.imageFilename).getPath();
                    FMConwayUtils.saveBitmap(bmp, filepath, 100);

                    VehicleChecklist.updatePhoto(vehicleChecklistID, filepath);
                }
            }

            // answers
            for (VehicleChecklistAnswer answer : vehicleChecklist.answers) {
                ContentValues values = new ContentValues();
                values.put("Answer", answer.answer);

                db.update("VehicleChecklistAnswers", values, "VehicleChecklistID = ? and ChecklistQuestionID = ?", new String[] { vehicleChecklistID, answer.checklistQuestionID });
            }

            // answer defects
            for (VehicleChecklistAnswerDefect defect : vehicleChecklist.defects) {
                if (!VehicleChecklistAnswerDefect.exists(vehicleChecklistID, defect.checklistQuestionID)) {
                    VehicleChecklistAnswerDefect.addVehicleChecklistAnswerDefect(defect);
                } else {
                    ContentValues values = new ContentValues();
                    values.put("Defect", defect.defect);

                    db.update("VehicleChecklistAnswerDefects", values, "VehicleChecklistID = ? and ChecklistQuestionID = ?", new String[] { vehicleChecklistID, defect.checklistQuestionID });
                }

                for (VehicleChecklistAnswerDefectPhoto photo : defect.photos) {
                    if (!VehicleChecklistAnswerDefectPhoto.exists(photo.vehicleChecklistDefectPhotoID)) {
                        FMConwayUtils.saveBitmap(photo.imageData, photo.imageLocation, 100);
                        VehicleChecklistAnswerDefectPhoto.addVehicleChecklistAnswerDefectPhoto(photo);
                    }
                }
            }

        }
    }

    private void CreateVehicleChecklistIfRequired(VehicleChecklist vehicleChecklist) {
        if (!VehicleChecklist.CheckIfChecklistCreated(vehicleChecklist.vehicleID, shiftID, vehicleChecklist.checklistID, true)) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(FMConwayApp.getContext());

            VehicleChecklist vc = new VehicleChecklist();
            vc.vehicleChecklistID = vehicleChecklist.vehicleChecklistID;
            vc.vehicleID = vehicleChecklist.vehicleID;
            vc.checklistID = vehicleChecklist.checklistID;
            vc.shiftID = shiftID;
            vc.startOfShift = true;
            vc.adhoc = false;
            vc.userID = sp.getString("UserID", "");
            VehicleChecklist.addVehicleChecklist(vc);

            List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(vehicleChecklist.checklistID);
            for (int j = 0; j < checklistQuestions.size(); j++) {
                VehicleChecklistAnswer vca = new VehicleChecklistAnswer();
                vca.vehicleChecklistID = vc.vehicleChecklistID;
                vca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                vca.userID = sp.getString("UserID", "");
                //vca.setAnswer(true);
                VehicleChecklistAnswer.addVehicleChecklistAnswerNew(vca);
            }
        }
    }

    // copy a vehicles morning checklist from the same vehicle on another shift, on the same day
    // this is required when multiple shifts are completed in same day but not pushed / synced with the service
    public static void propagateVehicleChecklistsIfPossible(String shiftID, String vehicleID) {
        List<Checklist> checklists = Checklist.GetChecklistsForVehicle(vehicleID);

        // if vehicle type has no checklist
        if (checklists.size() == 0) {
            return;
        }

        Checklist checklist = checklists.get(0);

        boolean checklistCreated = VehicleChecklist.CheckIfChecklistCreated(vehicleID, shiftID, checklist.checklistID, true);

        if (checklistCreated) {
            return;
        }

        Calendar today = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String todayString = dateFormat.format(today.getTime());

        // note exclude the current shiftID here
        String query = "" +
            "select " +
            "sv.StaffID, " +
            "sv.StartMileage, " +
            "vc.VehicleChecklistID, " +
            "vc.VehicleID, " +
            "vc.ChecklistID, " +
            "vc.ShiftID, " +
            "vc.StartOfShift, " +
            "vc.AdHoc, " +
            "vc.ImageLocation, " +
            "vc.UserID " +
            "from " +
            "ShiftVehicle as sv " +
            "join Shifts as s on s.ShiftID = sv.ShiftID " +
            "join VehicleChecklists as vc on vc.VehicleID = sv.VehicleID and vc.ShiftID = sv.ShiftID and vc.StartOfShift = 1 " +
            "where " +
            "s.ShiftID != ? and " +
            "sv.VehicleID = ? and " +
            "date(s.ShiftDate) = ? and " +
            "s.ShiftCompletionStatus >= 3";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, vehicleID, todayString });

        ShiftUpdate shiftUpdate = null;

        if (cursor.moveToFirst()) {
            shiftUpdate = new ShiftUpdate();
            shiftUpdate.shiftID = shiftID;
            shiftUpdate.shiftComplete = false;
            shiftUpdate.isCancelled = false;
            shiftUpdate.isActive = true;
            shiftUpdate.shiftDate = null; // ignored in save()
            shiftUpdate.updatedDate = null; // ignored in save()
            shiftUpdate.orderNumber = null; // ignored in save()
            shiftUpdate.shiftVehicles = new LinkedList<>();
            shiftUpdate.vehicleChecklists = new LinkedList<>();

            ShiftVehicle shiftVehicle = new ShiftVehicle();
            shiftVehicle.shiftID = shiftID;
            shiftVehicle.vehicleID = vehicleID;
            shiftVehicle.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
            shiftVehicle.startMileage = cursor.getInt(cursor.getColumnIndex("StartMileage"));
            shiftUpdate.shiftVehicles.add(shiftVehicle);

            String vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
            String imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

            VehicleChecklist vehicleChecklist = new VehicleChecklist();
            vehicleChecklist.vehicleChecklistID = UUID.randomUUID().toString();
            vehicleChecklist.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
            vehicleChecklist.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            vehicleChecklist.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
            vehicleChecklist.startOfShift = cursor.getInt(cursor.getColumnIndex("StartOfShift")) > 0 ? true : false;
            vehicleChecklist.adhoc = cursor.getInt(cursor.getColumnIndex("AdHoc")) > 0 ? true : false;
            vehicleChecklist.userID = cursor.getString(cursor.getColumnIndex("UserID"));

            File file = FMConwayUtils.getOutputMediaFile();
            Bitmap bmp = FMConwayUtils.filepathToBitmap(imageLocation);
            FMConwayUtils.saveBitmap(bmp, file.getPath(), 100);
            vehicleChecklist.imageLocation = file.getPath();
            vehicleChecklist.imageFilename = file.getName();
            shiftUpdate.vehicleChecklists.add(vehicleChecklist);

            vehicleChecklist.answers = new LinkedList<>();
            vehicleChecklist.defects = new LinkedList<>();

            List<VehicleChecklistAnswer> answers = VehicleChecklistAnswer.GetVehicleChecklistAnswers(vehicleChecklistID);
            for (VehicleChecklistAnswer answer : answers) {
                VehicleChecklistAnswer a = new VehicleChecklistAnswer();
                a.vehicleChecklistID = vehicleChecklist.vehicleChecklistID;
                a.checklistQuestionID = answer.checklistQuestionID;
                a.answer = answer.answer;
                a.isNew = answer.isNew;
                a.question = answer.question;
                vehicleChecklist.answers.add(a);

                VehicleChecklistAnswerDefect defect = VehicleChecklistAnswerDefect.GetVehicleDefect(vehicleChecklistID, answer.checklistQuestionID);
                if (defect != null) {
                    VehicleChecklistAnswerDefect d = new VehicleChecklistAnswerDefect();
                    d.vehicleChecklistID = vehicleChecklist.vehicleChecklistID;
                    d.checklistQuestionID = a.checklistQuestionID;
                    d.defect = defect.defect;
                    d.userID = defect.userID;

                    d.photos = new LinkedList<>();

                    ArrayList<VehicleChecklistAnswerDefectPhoto> photos = VehicleChecklistAnswerDefectPhoto.getPhotosForDefect(vehicleChecklistID, answer.checklistQuestionID);
                    for (VehicleChecklistAnswerDefectPhoto photo : photos) {
                        VehicleChecklistAnswerDefectPhoto p = new VehicleChecklistAnswerDefectPhoto();
                        p.vehicleChecklistDefectPhotoID = UUID.randomUUID().toString();
                        p.vehicleChecklistID = vehicleChecklist.vehicleChecklistID;
                        p.checklistQuestionID = a.checklistQuestionID;
                        p.userID = photo.userID;
                        p.longitude = photo.longitude;
                        p.latitude = photo.latitude;

                        File defectPhotoFile = FMConwayUtils.getOutputMediaFile();
                        p.imageData = photo.imageData;
                        p.imageLocation = defectPhotoFile.getPath();

                        p.time = photo.time;
                        p.shiftID = shiftID;

                        d.photos.add(p);
                    }

                    vehicleChecklist.defects.add(d);
                }
            }
        }

        cursor.close();

        if (shiftUpdate != null) {
            shiftUpdate.save();
        }
    }
}
