package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class CleansingLog {
    public String cleansingLogID;
    public String taskID;
    public String activityID;
    public String road;
    public String section;
    public String laneID;
    public String directionID;
    public String startMarker;
    public String endMarker;
    public Double gulliesCleaned;
    public Double gulliesMissed;
    public Double catchpitsCleaned;
    public Double catchpitsMissed;
    public Double channels;
    public Double slotDrains;
    public Double defects;
    public String comments;
    public Date createdDate;

    public CleansingLog() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("CleansingLogID", this.cleansingLogID);
            obj.put("TaskID", this.taskID);
            obj.put("ActivityID", this.activityID);
            obj.put("Road", this.road);
            obj.put("Section", this.section);
            obj.put("LaneID", this.laneID);
            obj.put("DirectionID", this.directionID);
            obj.put("StartMarker", this.startMarker);
            obj.put("EndMarker", this.endMarker);
            obj.put("GulliesCleaned", this.gulliesCleaned);
            obj.put("GulliesMissed", this.gulliesMissed);
            obj.put("CatchpitsCleaned", this.catchpitsCleaned);
            obj.put("CatchpitsMissed", this.catchpitsMissed);
            obj.put("Channels", this.channels);
            obj.put("SlotDrains", this.slotDrains);
            obj.put("Defects", this.defects);
            obj.put("Comments", this.comments);
            obj.put("CreatedDate", FMConwayUtils.getDateIsoString(this.createdDate));
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "CleansingLog - getJSONObject");
        }
        return obj;
    }

    protected static CleansingLog cursorToModel(Cursor c) {
        CleansingLog cleansingLog = new CleansingLog();
        cleansingLog.cleansingLogID = c.getString(c.getColumnIndex("CleansingLogID"));
        cleansingLog.taskID = c.getString(c.getColumnIndex("TaskID"));
        cleansingLog.activityID = c.getString(c.getColumnIndex("ActivityID"));
        cleansingLog.road = c.getString(c.getColumnIndex("Road"));
        cleansingLog.section = c.getString(c.getColumnIndex("Section"));
        cleansingLog.laneID = c.getString(c.getColumnIndex("LaneID"));
        cleansingLog.directionID = c.getString(c.getColumnIndex("DirectionID"));
        cleansingLog.startMarker = c.getString(c.getColumnIndex("StartMarker"));
        cleansingLog.endMarker = c.getString(c.getColumnIndex("EndMarker"));
        cleansingLog.gulliesCleaned = FMConwayUtils.getDouble(c, "GulliesCleaned");
        cleansingLog.gulliesMissed = FMConwayUtils.getDouble(c, "GulliesMissed");
        cleansingLog.catchpitsCleaned = FMConwayUtils.getDouble(c, "CatchpitsCleaned");
        cleansingLog.catchpitsMissed = FMConwayUtils.getDouble(c, "CatchpitsMissed");
        cleansingLog.channels = FMConwayUtils.getDouble(c, "Channels");
        cleansingLog.slotDrains = FMConwayUtils.getDouble(c, "SlotDrains");
        cleansingLog.defects = FMConwayUtils.getDouble(c, "Defects");
        cleansingLog.comments = c.getString(c.getColumnIndex("Comments"));
        cleansingLog.createdDate = FMConwayUtils.getCursorDate(c, "CreatedDate");

        return cleansingLog;
    }

    public static CleansingLog get(String cleansingLogID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        CleansingLog model = null;
        Cursor c = db.rawQuery("SELECT * FROM CleansingLogs WHERE CleansingLogID = ?", new String[]{cleansingLogID});

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static List<CleansingLog> getAllForTask(String taskID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        List<CleansingLog> models = new LinkedList<>();
        Cursor c = db.rawQuery("SELECT * FROM CleansingLogs WHERE TaskID = ? ORDER BY CreatedDate", new String[]{taskID});

        if (c.moveToFirst()) {
            do {
                CleansingLog model = cursorToModel(c);
                models.add(model);
            } while (c.moveToNext());
        }

        c.close();
        return models;
    }

    public static void save(CleansingLog model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("CleansingLogID", model.cleansingLogID);
            cv.put("TaskID", model.taskID);
            cv.put("ActivityID", model.activityID);
            cv.put("Road", model.road);
            cv.put("Section", model.section);
            cv.put("LaneID", model.laneID);
            cv.put("DirectionID", model.directionID);
            cv.put("StartMarker", model.startMarker);
            cv.put("EndMarker", model.endMarker);
            cv.put("GulliesCleaned", model.gulliesCleaned);
            cv.put("GulliesMissed", model.gulliesMissed);
            cv.put("CatchpitsCleaned", model.catchpitsCleaned);
            cv.put("CatchpitsMissed", model.catchpitsMissed);
            cv.put("Channels", model.channels);
            cv.put("SlotDrains", model.slotDrains);
            cv.put("Defects", model.defects);
            cv.put("Comments", model.comments);
            cv.put("CreatedDate", FMConwayUtils.getDateIsoString(model.createdDate));

            if (get(model.cleansingLogID) != null) {
                db.update("CleansingLogs", cv, "CleansingLogID = ?", new String[]{model.cleansingLogID});
            } else {
                db.insertOrThrow("CleansingLogs", null, cv);
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "CleansingLog - save");
        }
    }

    public static int delete(String cleansingLogID) {
        ArrayList<CleansingLogPhoto> cleansingLogPhotos = CleansingLogPhoto.getPhotosForCleansingLog(cleansingLogID);
        for (CleansingLogPhoto cleansingLogPhoto : cleansingLogPhotos) {
            CleansingLogPhoto.deleteCleansingLogPhotoFile(cleansingLogPhoto.cleansingLogPhotoID);
            CleansingLogPhoto.deleteCleansingLogPhoto(cleansingLogPhoto.cleansingLogPhotoID);
        }

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        return db.delete("CleansingLogs", "CleansingLogID = ?", new String[]{cleansingLogID});
    }

    public static int deleteCleansingLogsForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        return db.delete("CleansingLogs", "ActivityID = ?", new String[]{activityID});
    }

    public static JSONArray getCleansingLogsToSend(String shiftID) throws Exception {
        JSONArray jsonArray = new JSONArray();

        SQLiteDatabase db = null;
        Cursor c = null;

        try {
            String query = "SELECT c.* FROM CleansingLogs c ";
            query += "JOIN Activities a on a.ActivityID = c.ActivityID ";
            query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
            query += "WHERE jp.ShiftID = '" + shiftID + "'";

            db = Database.MainDB.getWritableDatabase();
            c = db.rawQuery(query, null);

            if (c.moveToFirst()) {
                do {
                    CleansingLog cleansingLog = cursorToModel(c);
                    JSONObject jsonObject = cleansingLog.getJSONObject();
                    jsonArray.put(jsonObject);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "getCleansingLogsToSend");
            throw e;
        } finally {
            c.close();
        }

        return jsonArray;
    }

}
