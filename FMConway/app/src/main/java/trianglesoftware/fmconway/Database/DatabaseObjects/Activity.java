package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Activity {
    public String activityID;
    public String activityTypeID;
    public String name;
    public String customer;
    public String customerPhoneNumber;
    public String contract;
    public String contractNumber;
    public String scheme;
    public String scopeOfWork;
    public String areaCallNumber;
    public String areaCallProtocol;
    public int maintenanceChecklistReminderInMinutes;
    public String firstCone;
    public String lastCone;
    public double distance;
    public String road;
    public String cWayDirection;
    public Date installationTime;
    public Date removalTime;
    public String healthAndSafety;
    public String worksInstructionNumber;
    public String worksInstructionComments;
    public String jobPackID;
    public String siteName;
    public String clientReferenceNumber;
    public String customerID;
    public String activityPriorityID;
    public String operator;
    public boolean isOutOfHours;
    public Date workInstructionStartDate;
    public Date workInstructionFinishDate;
    public String depotID;
    public int orderNumber;

    public Activity(){
    }

    public String getActivityID() { return this.activityID; }
    private String getActivityTypeID() { return this.activityTypeID; }
    private String getName() { return this.name; }
    private String getCustomer() { return this.customer; }
    private String getCustomerPhoneNumber() { return this.customerPhoneNumber; }
    private String getContract() { return this.contract; }
    private String getContractNumber() { return this.contractNumber; }
    private String getScheme() { return this.scheme; }
    private String getScopeOfWork() { return this.scopeOfWork; }
    public String getAreaCallNumber() { return this.areaCallNumber;}
    private String getAreaCallProtocol() { return this.areaCallProtocol;}
    private int getMaintenanceChecklistReminderInMinutes() { return this.maintenanceChecklistReminderInMinutes;}
    public String getFirstCone() { return this.firstCone;}
    public String getLastCone() { return this.lastCone;}
    public double getDistance() { return this.distance;}
    public String getRoad() { return this.road;}
    public String getcWayDirection() { return this.cWayDirection;}
    public String getHealthAndSafety() { return this.healthAndSafety;}

    public String getInstallationTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.installationTime != null){
            return dateFormat.format(this.installationTime);
        }
        else {
            return "";
        }
    }

    public String getRemovalTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.removalTime != null){
            return dateFormat.format(this.removalTime);
        }
        else {
            return "";
        }
    }

    public String getWorksInstructionNumber() { return this.worksInstructionNumber;}
    public String getWorksInstructionComments() { return this.worksInstructionComments;}

    private String getJobPackID() { return this.jobPackID; }

    public void setActivityID(String activityID) { this.activityID = activityID; }
    public void setActivityTypeID(String activityTypeID) { this.activityTypeID = activityTypeID; }
    public void setName(String name) { this.name = name; }
    public void setCustomer(String customer) { this.customer = customer; }
    public void setCustomerPhoneNumber(String customerPhoneNumber) { this.customerPhoneNumber = customerPhoneNumber; }
    public void setContract(String contract) { this.contract = contract; }
    public void setContractNumber(String contractNumber) { this.contractNumber = contractNumber; }
    public void setScheme(String scheme) { this.scheme = scheme; }
    public void setScopeOfWork(String scopeOfWork) { this.scopeOfWork = scopeOfWork; }
    public void setAreaCallNumber(String areaCallNumber) {this.areaCallNumber = areaCallNumber;}
    public void setAreaCallProtocol(String areaCallProtocol) {this.areaCallProtocol = areaCallProtocol;}
    public void setMaintenanceChecklistReminderInMinutes(int maintenanceChecklistReminderInMinutes) {this.maintenanceChecklistReminderInMinutes = maintenanceChecklistReminderInMinutes;}
    public void setFirstCone(String firstCone) { this.firstCone = firstCone;}
    public void setLastCone(String lastCone) { this.lastCone = lastCone;}
    public void setDistance(double distance) { this.distance = distance;}
    public void setRoad(String road) { this.road = road;}
    public void setcWayDirection(String cWayDirection) { this.cWayDirection = cWayDirection;}
    public void setHealthAndSafety(String healthAndSafety) {this.healthAndSafety = healthAndSafety;}
    public void setWorksInstructionNumber(String worksInstructionNumber) { this.worksInstructionNumber = worksInstructionNumber;}
    public void setWorksInstructionComments(String worksInstructionComments) { this.worksInstructionComments = worksInstructionComments;}
    public void setInstallationTime(Date installationTime) { this.installationTime = installationTime;}
    public void setRemovalTime(Date removalTime) { this.removalTime = removalTime;}
    public void setJobPackID(String jobPackID) { this.jobPackID = jobPackID; }

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("ActivityID", this.activityID);
        obj.put("ActivityTypeID", this.activityTypeID);
        obj.put("Name", this.name);
        obj.put("Customer", this.customer);
        obj.put("CustomerPhoneNumber", this.customerPhoneNumber);
        obj.put("Contract", this.contract);
        obj.put("ContractNumber", this.contractNumber);
        obj.put("Scheme", this.scheme);
        obj.put("ScopeOfWork", this.scopeOfWork);
        obj.put("AreaCallNumber", this.areaCallNumber);
        obj.put("AreaCallProtocol", this.areaCallProtocol);
        obj.put("MaintenanceChecklistReminderInMinutes", this.maintenanceChecklistReminderInMinutes);
        obj.put("FirstCone", this.firstCone);
        obj.put("LastCone", this.lastCone);
        obj.put("Distance", this.distance);
        obj.put("Road", this.road);
        obj.put("CWayDirection", this.cWayDirection);
        obj.put("InstallationTime", getInstallationTime());
        obj.put("RemovalTime", getRemovalTime());
        obj.put("HealthAndSafety", this.healthAndSafety);
        obj.put("WorksInstructionNumber", this.worksInstructionNumber);
        obj.put("WorksInstructionComments", this.worksInstructionComments);
        obj.put("JobPackID", this.jobPackID);
        obj.put("SiteName", this.siteName);
        obj.put("ClientReferenceNumber", this.clientReferenceNumber);
        obj.put("CustomerID", this.customerID);
        obj.put("ActivityPriorityID", this.activityPriorityID);
        obj.put("Operator", this.operator);
        obj.put("IsOutOfHours", this.isOutOfHours);
        obj.put("WorkInstructionStartDate", this.workInstructionStartDate);
        obj.put("WorkInstructionFinishDate", this.workInstructionFinishDate);
        obj.put("DepotID", this.depotID);
        obj.put("OrderNumber", this.orderNumber);

        return obj;
    }

    //Activities
    public static void addActivity(Activity activity)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ActivityID", activity.activityID);
        values.put("ActivityTypeID", activity.activityTypeID);
        values.put("Name", activity.name);
        values.put("Customer", activity.customer);
        values.put("CustomerPhoneNumber", activity.customerPhoneNumber);
        values.put("Contract", activity.contract);
        values.put("ContractNumber", activity.contractNumber);
        values.put("Scheme", activity.scheme);
        values.put("ScopeOfWork", activity.scopeOfWork);
        values.put("AreaCallNumber", activity.areaCallNumber);
        values.put("AreaCallProtocol", activity.areaCallProtocol);
        values.put("MaintenanceChecklistReminderInMinutes", activity.maintenanceChecklistReminderInMinutes);
        values.put("FirstCone", activity.firstCone);
        values.put("LastCone", activity.lastCone);
        values.put("Distance", activity.distance);
        values.put("Road", activity.road);
        values.put("CWayDirection", activity.cWayDirection);
        values.put("InstallationTime", activity.getInstallationTime());
        values.put("RemovalTime", activity.getRemovalTime());
        values.put("HealthAndSafety", activity.healthAndSafety);
        values.put("WorksInstructionNumber", activity.worksInstructionNumber);
        values.put("WorksInstructionComments", activity.worksInstructionComments);
        values.put("JobPackID", activity.jobPackID);
        values.put("SiteName", activity.siteName);
        values.put("ClientReferenceNumber", activity.clientReferenceNumber);
        values.put("CustomerID", activity.customerID);
        values.put("ActivityPriorityID", activity.activityPriorityID);
        values.put("Operator", activity.operator);
        values.put("IsOutOfHours", activity.isOutOfHours);
        values.put("WorkInstructionStartDate", FMConwayUtils.getDateIsoString(activity.workInstructionStartDate));
        values.put("WorkInstructionFinishDate", FMConwayUtils.getDateIsoString(activity.workInstructionFinishDate));
        values.put("DepotID", activity.depotID);
        values.put("OrderNumber", activity.orderNumber);

        db.insert("Activities", null, values);
    }

    public static void deleteAllActivities()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Activities");
    }

    public static void deleteActivitiesForJobPack(String jobPackID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Activities WHERE JobPackID = '" + jobPackID + "'");
    }

    public static List<Activity> GetActivitiesForJobPack(String jobPackID) {
        List<Activity> activities = new LinkedList<>();

        String query = "SELECT * FROM Activities ";
        query += "WHERE JobPackID = '" + jobPackID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Activity a;
        if(cursor.moveToFirst()){
            do{
                a = new Activity();
                a.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                a.activityTypeID = cursor.getString(cursor.getColumnIndex("ActivityTypeID"));
                a.name = cursor.getString(cursor.getColumnIndex("Name"));
                a.customer = cursor.getString(cursor.getColumnIndex("Customer"));
                a.customerPhoneNumber = cursor.getString(cursor.getColumnIndex("CustomerPhoneNumber"));
                a.contract = cursor.getString(cursor.getColumnIndex("Contract"));
                a.contractNumber = cursor.getString(cursor.getColumnIndex("ContractNumber"));
                a.scheme = cursor.getString(cursor.getColumnIndex("Scheme"));
                a.scopeOfWork = cursor.getString(cursor.getColumnIndex("ScopeOfWork"));
                a.areaCallNumber = cursor.getString(cursor.getColumnIndex("AreaCallNumber"));
                a.areaCallProtocol = cursor.getString(cursor.getColumnIndex("AreaCallProtocol"));
                a.maintenanceChecklistReminderInMinutes = cursor.getInt(cursor.getColumnIndex("MaintenanceChecklistReminderInMinutes"));
                a.firstCone = cursor.getString(cursor.getColumnIndex("FirstCone"));
                a.lastCone = cursor.getString(cursor.getColumnIndex("LastCone"));
                a.distance = cursor.getDouble(cursor.getColumnIndex("Distance"));
                a.road = cursor.getString(cursor.getColumnIndex("Road"));
                a.cWayDirection = cursor.getString(cursor.getColumnIndex("CWayDirection"));

                try {
                    String installationTime = cursor.getString(cursor.getColumnIndex("InstallationTime"));
                    if(installationTime != null)
                        a.setInstallationTime(format.parse(installationTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    String removalTime = cursor.getString(cursor.getColumnIndex("RemovalTime"));
                    if (removalTime != null)
                    {
                        a.setRemovalTime(format.parse(removalTime));
                    }
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }

                a.healthAndSafety = cursor.getString(cursor.getColumnIndex("HealthAndSafety"));
                a.worksInstructionNumber = cursor.getString(cursor.getColumnIndex("WorksInstructionNumber"));
                a.worksInstructionComments = cursor.getString(cursor.getColumnIndex("WorksInstructionComments"));
                a.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                a.siteName = cursor.getString(cursor.getColumnIndex("SiteName"));
                a.clientReferenceNumber = cursor.getString(cursor.getColumnIndex("ClientReferenceNumber"));
                a.customerID = cursor.getString(cursor.getColumnIndex("CustomerID"));
                a.activityPriorityID = cursor.getString(cursor.getColumnIndex("ActivityPriorityID"));
                a.operator = cursor.getString(cursor.getColumnIndex("Operator"));
                a.isOutOfHours = cursor.getInt(cursor.getColumnIndex("IsOutOfHours")) > 0;
                a.workInstructionStartDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
                a.workInstructionFinishDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
                a.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));

                activities.add(a);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return activities;
    }

    public static String GetScopeOfWork(String activityID, String shiftID) {
        String query = "SELECT * FROM Activities ";
        query += "WHERE ActivityID = '" + activityID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String scope = "";
        if(cursor.moveToFirst()){
            do{
                scope = cursor.getString(cursor.getColumnIndex("Contract"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        query = "SELECT * FROM Shifts ";
        query += " WHERE ShiftID = '" + shiftID + "'";

        db = Database.MainDB.getWritableDatabase();
        cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                scope += "\nProject Code - " + cursor.getString(cursor.getColumnIndex("ProjCode"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return scope;
    }

    public static Activity GetActivity(String activityID)
    {
        String query = "SELECT * FROM Activities ";
        query += "WHERE ActivityID = '" + activityID + "'";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Activity a = null;

        if(cursor.moveToFirst()){
            do{
                a = new Activity();
                a.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                a.activityTypeID = cursor.getString(cursor.getColumnIndex("ActivityTypeID"));
                a.name = cursor.getString(cursor.getColumnIndex("Name"));
                a.customer = cursor.getString(cursor.getColumnIndex("Customer"));
                a.customerPhoneNumber = cursor.getString(cursor.getColumnIndex("CustomerPhoneNumber"));
                a.contract = cursor.getString(cursor.getColumnIndex("Contract"));
                a.contractNumber = cursor.getString(cursor.getColumnIndex("ContractNumber"));
                a.scheme = cursor.getString(cursor.getColumnIndex("Scheme"));
                a.scopeOfWork = cursor.getString(cursor.getColumnIndex("ScopeOfWork"));
                a.areaCallNumber = cursor.getString(cursor.getColumnIndex("AreaCallNumber"));
                a.areaCallProtocol = cursor.getString(cursor.getColumnIndex("AreaCallProtocol"));
                a.maintenanceChecklistReminderInMinutes = cursor.getInt(cursor.getColumnIndex("MaintenanceChecklistReminderInMinutes"));
                a.firstCone = cursor.getString(cursor.getColumnIndex("FirstCone"));
                a.lastCone = cursor.getString(cursor.getColumnIndex("LastCone"));
                a.distance = cursor.getDouble(cursor.getColumnIndex("Distance"));
                a.road = cursor.getString(cursor.getColumnIndex("Road"));
                a.cWayDirection = cursor.getString(cursor.getColumnIndex("CWayDirection"));

                try {
                    String installationTime = cursor.getString(cursor.getColumnIndex("InstallationTime"));
                    if(installationTime != null)
                        a.setInstallationTime(format.parse(installationTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    String removalTime = cursor.getString(cursor.getColumnIndex("RemovalTime"));
                    if (removalTime != null)
                    {
                        a.setRemovalTime(format.parse(removalTime));
                    }
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }

                a.healthAndSafety = cursor.getString(cursor.getColumnIndex("HealthAndSafety"));
                a.worksInstructionNumber = cursor.getString(cursor.getColumnIndex("WorksInstructionNumber"));
                a.worksInstructionComments = cursor.getString(cursor.getColumnIndex("WorksInstructionComments"));
                a.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                a.siteName = cursor.getString(cursor.getColumnIndex("SiteName"));
                a.clientReferenceNumber = cursor.getString(cursor.getColumnIndex("ClientReferenceNumber"));
                a.customerID = cursor.getString(cursor.getColumnIndex("CustomerID"));
                a.activityPriorityID = cursor.getString(cursor.getColumnIndex("ActivityPriorityID"));
                a.operator = cursor.getString(cursor.getColumnIndex("Operator"));
                a.isOutOfHours = cursor.getInt(cursor.getColumnIndex("IsOutOfHours")) > 0;
                a.workInstructionStartDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
                a.workInstructionFinishDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
                a.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                a.orderNumber = cursor.getInt(cursor.getColumnIndex("OrderNumber"));

            }while(cursor.moveToNext());
        }

        cursor.close();

        return a;
    }

    public static String GetAreaCallProtocolForActivity(String activityID)
    {
        String query = "SELECT AreaCallProtocol FROM Activities WHERE ActivityID = '" + activityID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String protocol = "";
        if(cursor.moveToFirst())
        {
            do{
                protocol = cursor.getString(cursor.getColumnIndex("AreaCallProtocol"));
            }while(cursor.moveToNext());
        }

        return protocol;
    }

    public static int getMaintenanceChecklistReminderInMinutes(String activityID)
    {
        String query = "SELECT MaintenanceChecklistReminderInMinutes FROM Activities WHERE ActivityID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { activityID });

        int maintenanceChecklistReminderInMinutes = 0;
        if (cursor.moveToFirst())
        {
            maintenanceChecklistReminderInMinutes = cursor.getInt(cursor.getColumnIndex("MaintenanceChecklistReminderInMinutes"));
        }

        return maintenanceChecklistReminderInMinutes;
    }

    public static List<Activity> GetActivitiesForShift(String shiftID) {
        List<Activity> activities = new LinkedList<>();

        String query = "SELECT * FROM Activities a ";
        query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Activity a;
        if(cursor.moveToFirst()){
            do{
                a = new Activity();
                a.activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));
                a.activityTypeID = cursor.getString(cursor.getColumnIndex("ActivityTypeID"));
                a.name = cursor.getString(cursor.getColumnIndex("Name"));
                a.customer = cursor.getString(cursor.getColumnIndex("Customer"));
                a.customerPhoneNumber = cursor.getString(cursor.getColumnIndex("CustomerPhoneNumber"));
                a.contract = cursor.getString(cursor.getColumnIndex("Contract"));
                a.contractNumber = cursor.getString(cursor.getColumnIndex("ContractNumber"));
                a.scheme = cursor.getString(cursor.getColumnIndex("Scheme"));
                a.scopeOfWork = cursor.getString(cursor.getColumnIndex("ScopeOfWork"));
                a.worksInstructionNumber = cursor.getString(cursor.getColumnIndex("WorksInstructionNumber"));
                a.worksInstructionComments = cursor.getString(cursor.getColumnIndex("WorksInstructionComments"));
                a.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                a.isOutOfHours = cursor.getInt(cursor.getColumnIndex("IsOutOfHours")) > 0;
                a.workInstructionStartDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
                a.workInstructionFinishDate = FMConwayUtils.getCursorDate(cursor, "WorkInstructionStartDate");
                a.depotID = cursor.getString(cursor.getColumnIndex("DepotID"));
                a.orderNumber = cursor.getInt(cursor.getColumnIndex("OrderNumber"));

                activities.add(a);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return activities;
    }

    public static String getActivityIdByTaskID(String taskID) {
        String query = "" +
            "select " +
            "ActivityID " +
            "from " +
            "Tasks as t " +
            "where " +
            "t.TaskID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskID });

        cursor.moveToFirst();
        String activityID = cursor.getString(cursor.getColumnIndex("ActivityID"));

        cursor.close();

        return activityID;
    }

    public static String getActivityTypeByTaskID(String taskID) {
        String activityType ="";
        String query = "" +
                "select " +
                "at.Name " +
                "from " +
                "ActivityTypes as at " +
                "join Activities as a on a.ActivityTypeID = at.ActivityTypeID " +
                "join Tasks as t on t.ActivityID = a.ActivityID where t.taskID = ?";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { taskID });

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            activityType = cursor.getString(cursor.getColumnIndex("Name"));
            cursor.close();
        }

        return activityType;
    }

    public static String getActivityTypeByShift(String shiftID) {
        String activityType = "";

        try {
            String query = "select at.Name from Shifts s " +
                    "INNER JOIN JobPacks jp ON jp.ShiftID = s.ShiftID " +
                    "INNER JOIN Activities a ON a.JobPackID = jp.JobPackID " +
                    "INNER JOIN ActivityTypes at ON at.ActivityTypeID = a.ActivityTypeID " +
                    "WHERE s.ShiftID = ?";

            SQLiteDatabase db = Database.MainDB.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{shiftID});

            if (cursor.moveToFirst()) {
                activityType = cursor.getString(cursor.getColumnIndex("Name"));
            }

            cursor.close();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "getActivityTypeFromShift");
        }

        return activityType;
    }


    public static void saveActivityDetails(Activity model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("SiteName", model.siteName);
            cv.put("ClientReferenceNumber", model.clientReferenceNumber);
            cv.put("ActivityPriorityID", model.activityPriorityID);
            cv.put("Operator", model.operator);

            db.update("Activities", cv, "ActivityID = ?", new String[]{model.activityID});
        } catch (Exception e) {
            ErrorLog.CreateError(e, "saveActivityDetails");
        }
    }

    public static JSONObject getActivityDetailsToSend(String shiftID) throws Exception {
        JSONObject activityDetail = new JSONObject();

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            String query = "SELECT a.* FROM Activities a ";
            query += "JOIN JobPacks jp on jp.JobPackID = a.JobPackID ";
            query += "WHERE jp.ShiftID = '" + shiftID + "'";

            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    activityDetail.put("ActivityID", cursor.getString(cursor.getColumnIndex("ActivityID")));
                    activityDetail.put("SiteName", cursor.getString(cursor.getColumnIndex("SiteName")));
                    activityDetail.put("ClientReferenceNumber", cursor.getString(cursor.getColumnIndex("ClientReferenceNumber")));
                    activityDetail.put("ActivityPriorityID", cursor.getString(cursor.getColumnIndex("ActivityPriorityID")));
                    activityDetail.put("Operator", cursor.getString(cursor.getColumnIndex("Operator")));
                } while (cursor.moveToNext());
            }
        }
        catch( Exception e)
        {
            ErrorLog.CreateError(e, "getActivityDetailsToSend");
            throw e;
        }
        finally
        {
            cursor.close();
        }

        return activityDetail;
    }

    public static void updateOrderNumber(String shiftID, Integer orderNumber) {
        Activity activity = GetActivitiesForShift(shiftID).get(0);

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("OrderNumber", orderNumber);

        db.update("Activities", contentValues, "ActivityID = ?", new String[] { activity.activityID });
    }
}
