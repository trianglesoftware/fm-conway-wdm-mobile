package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Briefing {
    public String briefingID;
    public String name;
    public String details;
    public int briefingTypeID;
    public String jobPackID;
    public String documentID;
    public String imageLocation;
    public String documentName;
    public String contentType;
    public String userID;

    public Briefing(){
    }

    public String getBriefingID() { return this.briefingID; }
    public String getName() { return this.name; }
    public String getDetails() { return this.details; }
    public int getBriefingTypeID() { return this.briefingTypeID; }
    private String getJobPackID() { return this.jobPackID; }
    public String getDocumentID() { return this.documentID; }
    public String getImageLocation() { return this.imageLocation; }
    public String getDocumentName() { return this.documentName; }
    public String getContentType() { return this.contentType; }

    public void setBriefingID(String briefingID) { this.briefingID = briefingID; }
    public void setName(String name) { this.name = name; }
    public void setDetails(String details) { this.details = details; }
    public void setBriefingTypeID(int briefingTypeID) { this.briefingTypeID = briefingTypeID; }
    public void setJobPackID(String jobPackID) { this.jobPackID = jobPackID; }
    public void setDocumentID(String documentID) { this.documentID = documentID; }
    private void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }
    public void setDocumentName(String documentName) { this.documentName = documentName; }
    public void setContentType(String contentType) { this.contentType = contentType; }

    public JSONObject getJSONObject() throws  JSONException{

        JSONObject obj = new JSONObject();

            obj.put("BriefingID", this.briefingID);
            obj.put("Name", this.name);
            obj.put("Details", this.details);
            obj.put("BriefingTypeID", this.briefingTypeID);
            obj.put("JobPackID", this.jobPackID);
            obj.put("DocumentID", this.documentID);
            obj.put("ImageLocation", this.imageLocation);
            obj.put("DocumentName", this.documentName);
            obj.put("ContentType", this.contentType);


        return obj;
    }

    //Briefings
    public static void addBriefing(Briefing briefing)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BriefingID", briefing.briefingID);
        values.put("BriefingTypeID", briefing.briefingTypeID);
        values.put("Name", briefing.name);
        values.put("Details", briefing.details);
        values.put("JobPackID", briefing.jobPackID);
        values.put("DocumentID", briefing.documentID);
        values.put("DocumentName", briefing.documentName);
        values.put("ContentType", briefing.contentType);
        values.put("UserID", briefing.userID);

        db.insert("Briefings", null, values);
    }

    public static void deleteAllBriefings()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Briefings");
    }

    public static void deleteBriefingsForJobPack(String jobPackID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Briefings WHERE JobPackID = '" + jobPackID + "'");
    }

    public static void deleteBriefingsForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Briefings WHERE UserID = '" + userID + "'");
    }

    public static List<Briefing> GetBriefingsForJobPack(String jobPackID) {
        List<Briefing> briefings = new LinkedList<>();

        String query = "SELECT * FROM Briefings ";
        query += "WHERE JobPackID = '" + jobPackID + "' AND BriefingTypeID <> 2 AND BriefingTypeID <> 1 AND BriefingTypeID <> 5";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Briefing b;
        if(cursor.moveToFirst()){
            do{
                b = new Briefing();
                b.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                b.briefingTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("BriefingTypeID")));
                b.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                b.name = cursor.getString(cursor.getColumnIndex("Name"));
                b.details = cursor.getString(cursor.getColumnIndex("Details"));

                briefings.add(b);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return briefings;
    }


    public static int GetBriefingType(String briefingID) {
        String query = "SELECT * FROM Briefings ";
        query += "WHERE BriefingID = '" + briefingID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int bt = 0;
        if(cursor.moveToFirst()){
            do{
                bt = Integer.parseInt(cursor.getString(cursor.getColumnIndex("BriefingTypeID")));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return bt;
    }

    public static Briefing GetBriefing(String briefingID) {
        String query = "SELECT * FROM Briefings ";
        query += "WHERE BriefingID = '" + briefingID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Briefing b = null;
        if(cursor.moveToFirst()){
            do{
                b = new Briefing();
                b.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                b.briefingTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("BriefingTypeID")));
                b.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                b.name = cursor.getString(cursor.getColumnIndex("Name"));
                b.details = cursor.getString(cursor.getColumnIndex("Details"));
                b.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                b.contentType = cursor.getString(cursor.getColumnIndex("ContentType"));

            }while(cursor.moveToNext());
        }

        cursor.close();

        return b;
    }
//
//    public static String GetHealthAndSafetyBriefing(int jobPackID) {
//        String query = "SELECT * FROM Briefings ";
//        query += "WHERE BriefingTypeID = 5 AND JobPackID = " + jobPackID; // Health and safety type = 5?
//
//        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
//        Cursor cursor = db.rawQuery(query, null);
//
//        String hs = "";
//        if(cursor.moveToFirst()){
//            do{
//                hs = cursor.getString(cursor.getColumnIndex("Details"));
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return hs;
//    }

    public static void SetImageLocation(Briefing briefing, String path) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE Briefings SET ImageLocation = '"+path+"' WHERE BriefingID = " + briefing.getBriefingID());

        //db.execSQL("UPDATE Briefings SET ImageLocation = ? WHERE BriefingID = " + briefing.getBriefingID(), new String[]{path});

        SQLiteStatement statement = db.compileStatement("UPDATE Briefings SET ImageLocation = ? WHERE BriefingID = ?");
        statement.bindString(1,path);
        statement.bindString(2,String.valueOf(briefing.briefingID));
        statement.execute();
        statement.close();
    }

    public static List<Briefing> GetBriefingsForShift(String shiftID) {
        List<Briefing> briefings = new LinkedList<>();

        String query = "SELECT * FROM Briefings b ";
        query += "JOIN JobPacks jp on jp.JobPackID = b.JobPackID ";
        query += "WHERE jp.ShiftID = '" + shiftID + "' AND BriefingTypeID <> 2 AND BriefingTypeID <> 1 AND BriefingTypeID <> 5";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Briefing b;
        if(cursor.moveToFirst()){
            do{
                b = new Briefing();
                b.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                b.briefingTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("BriefingTypeID")));
                b.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                b.name = cursor.getString(cursor.getColumnIndex("Name"));
                b.details = cursor.getString(cursor.getColumnIndex("Details"));

                briefings.add(b);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return briefings;
    }

    public static List<Briefing> GetRAMSForJobPack(String jobPackID) {
        List<Briefing> briefings = new LinkedList<>();

        String query = "SELECT * FROM Briefings ";
        query += "WHERE JobPackID = '" + jobPackID + "' AND (BriefingTypeID = 2 OR BriefingTypeID = 5)";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Briefing b;
        if(cursor.moveToFirst()){
            do{
                b = new Briefing();
                b.briefingID = cursor.getString(cursor.getColumnIndex("BriefingID"));
                b.briefingTypeID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("BriefingTypeID")));
                b.jobPackID = cursor.getString(cursor.getColumnIndex("JobPackID"));
                b.name = cursor.getString(cursor.getColumnIndex("Name"));
                b.details = cursor.getString(cursor.getColumnIndex("Details"));

                briefings.add(b);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return briefings;
    }
}
