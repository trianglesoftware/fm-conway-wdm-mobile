package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class VehicleType {
    public String vehicleTypeID;
    public String name;

    public VehicleType(){
    }

    public VehicleType(String VehicleTypeID, String Name)
    {
        super();
        this.vehicleTypeID = VehicleTypeID;
        this.name = Name;
    }

    private String getVehicleTypeID() { return this.vehicleTypeID; }
    private String getName() { return this.name; }

    public void setVehicleTypeID(String vehicleTypeID) { this.vehicleTypeID = vehicleTypeID; }
    public void setName(String name) { this.name = name; }

    public JSONObject getJSONObject() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("VehicleTypeID", this.vehicleTypeID);
            obj.put("Name", this.name);
        } catch (JSONException e) { }

        return obj;
    }

    // Vehicle Types
    public static void addVehicleType(VehicleType vehicleType)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleTypeID", vehicleType.vehicleTypeID);
        values.put("Name", vehicleType.name);

        db.insert("VehicleTypes", null, values);
    }

    public static void deleteAllVehicleTypes()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleTypes");
    }
}
