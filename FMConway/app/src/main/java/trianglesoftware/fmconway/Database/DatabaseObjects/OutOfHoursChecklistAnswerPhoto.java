package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;

public class OutOfHoursChecklistAnswerPhoto {
    public String oohChecklistAnswerPhotoID;
    public String oohChecklistAnswerID;
    public String imageLocation;
    public double longitude;
    public double latitude;
    public String userID;
    public Date time;

    public Bitmap imageData;

    public OutOfHoursChecklistAnswerPhoto() {}

    private JSONObject getJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();

        obj.put("OOHChecklistAnswerPhotoID", this.oohChecklistAnswerPhotoID);
        obj.put("OOHChecklistAnswerID", this.oohChecklistAnswerID);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("UserID", this.userID);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        obj.put("Time", dateFormat.format(this.time));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.imageLocation, options);

        options.inSampleSize = calculateInSampleSize(options,400,400);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(this.imageLocation, options);
        if (bm != null) {

            try {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            }
            catch (Exception e)
            {
                ErrorLog.CreateError(e, "Rotate Photo");
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            obj.put("DataString", encodedImage);
        }

        return obj;
    }

    public static void addOOHChecklistAnswerPhoto(OutOfHoursChecklistAnswerPhoto oohChecklistAnswerPhoto)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("OOHChecklistAnswerPhotoID", oohChecklistAnswerPhoto.oohChecklistAnswerPhotoID);
        values.put("OOHChecklistAnswerID", oohChecklistAnswerPhoto.oohChecklistAnswerID);
        values.put("ImageLocation", oohChecklistAnswerPhoto.imageLocation);
        values.put("Longitude", oohChecklistAnswerPhoto.longitude);
        values.put("Latitude", oohChecklistAnswerPhoto.latitude);
        values.put("UserID", oohChecklistAnswerPhoto.userID);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String time = dateFormat.format(oohChecklistAnswerPhoto.time);
        values.put("Time", time);

        db.insert("OutOfHoursChecklistAnswerPhotos", null, values);
    }

    public static void deleteAllOOHChecklistAnswerPhotos()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswerPhotos");
    }

    public static void deleteOOHChecklistAnswerPhoto(String oohChecklistAnswerPhotoID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswerPhotos WHERE OOHChecklistAnswerPhotoID = '" + oohChecklistAnswerPhotoID + "'");
    }

    public static void deleteOOHChecklistAnswerPhotoFile(String oohChecklistAnswerPhotoID)
    {
        try {
            String query = "SELECT * FROM OOHChecklistAnswerPhotos ";
            query += "WHERE OOHChecklistAnswerPhotoID = '" + oohChecklistAnswerPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DeleteOOHChecklistAnswerPhotoFile");
        }
    }

    public static void deleteOOHChecklistAnswerPhotosForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswerPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteOOHChecklistAnswerPhotosForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswerPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static void deleteOOHChecklistAnswerPhotosForAnswer(String oohChecklistAnswerID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM OutOfHoursChecklistAnswerPhotos WHERE OOHChecklistAnswerID = '" + oohChecklistAnswerID + "'");
    }

    public static ArrayList<OutOfHoursChecklistAnswerPhoto> getPhotosForOOHChecklistAnswer(String oohChecklistAnswerID) {
        ArrayList<OutOfHoursChecklistAnswerPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM OutOfHoursChecklistAnswerPhotos WHERE OOHChecklistAnswerID = '" + oohChecklistAnswerID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()) {
            do {
                OutOfHoursChecklistAnswerPhoto data = new OutOfHoursChecklistAnswerPhoto();
                data.oohChecklistAnswerPhotoID = cursor.getString(cursor.getColumnIndex("OOHChecklistAnswerPhotoID"));
                data.oohChecklistAnswerID = cursor.getString(cursor.getColumnIndex("OOHChecklistAnswerID"));
                data.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));;
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                data.userID  = cursor.getString(cursor.getColumnIndex("UserID"));;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(data.imageLocation, options);

                options.inSampleSize = calculateInSampleSize(options,150,150);
                options.inJustDecodeBounds = false;

                Bitmap bitmap = BitmapFactory.decodeFile(data.imageLocation, options);
                data.imageData = bitmap;

                photos.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return photos;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONArray GetOOHChecklistPhotosToSend() throws Exception {
        JSONArray photos = new JSONArray();

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            String query = "SELECT * FROM OutOfHoursChecklistAnswerPhotos mtap ";

            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject pObject;
            if (cursor.moveToFirst()) {
                do {
                    OutOfHoursChecklistAnswerPhoto p = new OutOfHoursChecklistAnswerPhoto();
                    p.oohChecklistAnswerPhotoID = cursor.getString(cursor.getColumnIndex("OOHChecklistAnswerPhotoID"));
                    p.oohChecklistAnswerID = cursor.getString(cursor.getColumnIndex("OOHChecklistAnswerID"));
                    p.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                    p.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    p.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    p.userID = cursor.getString(cursor.getColumnIndex("UserID"));

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String time = cursor.getString(cursor.getColumnIndex("Time"));
                    p.time = format.parse(time);

                    pObject = p.getJSONObject();

                    photos.put(pObject);
                } while (cursor.moveToNext());
            }
        }
        catch( Exception e)
        {
            ErrorLog.CreateError(e, "GetOOHChecklistPhotosToSend");
            throw e;
        }
        finally
        {
            cursor.close();
        }

        return photos;
    }
}
