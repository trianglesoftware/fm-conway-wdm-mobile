package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 01/03/2016.
 */
public class ShiftStaff {
    public String shiftID;
    public String staffID;
    public String staffFullname;
    public boolean notOnShift;
    public String reasonNotOn;
    public Date startTime;
    public Date endTime;
    public boolean firstAider;
    public String agencyEmployeeName;

    public int workedHours;
    public int workedMins;
    public int breakHours;
    public int breakMins;
    public int travelHours;
    public int travelMins;

    public boolean ipv;
    public boolean onCall;
    public boolean paidBreak;

    public ShiftStaff() {

    }

    public JSONObject getJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("ShiftID", this.shiftID);
        obj.put("StaffID", this.staffID);
        obj.put("AgencyEmployeeName", this.agencyEmployeeName);
        obj.put("FirstAider", this.firstAider);

        String forename = "";
        String surname = "";
        String staffType = "";

        String query = "SELECT Forename, Surname, StaffType FROM Staff where StaffID = '" + this.staffID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                forename = cursor.getString(cursor.getColumnIndex("Forename"));
                surname = cursor.getString(cursor.getColumnIndex("Surname"));
                staffType = cursor.getString(cursor.getColumnIndex("StaffType"));
            }
        }

        cursor.close();

        obj.put("Forename", forename);
        obj.put("Surname", surname);
        obj.put("NotOnShift", this.notOnShift);
        obj.put("ReasonNotOn", this.reasonNotOn);
        obj.put("StartTime", FMConwayUtils.dateToString(this.startTime, "yyyy-MM-dd HH:mm:ss", ""));
        obj.put("EndTime", FMConwayUtils.dateToString(this.endTime, "yyyy-MM-dd HH:mm:ss", ""));
        obj.put("StaffType", staffType);
        obj.put("WorkedHours", workedHours);
        obj.put("WorkedMins", workedMins);
        obj.put("BreakHours", breakHours);
        obj.put("BreakMins", breakMins);
        obj.put("TravelHours", travelHours);
        obj.put("TravelMins", travelMins);
        obj.put("IPV", ipv);
        obj.put("OnCall", onCall);
        obj.put("PaidBreak", paidBreak);

        return obj;
    }

    // Shift Staff
    public static void addShiftStaff(ShiftStaff shiftStaff) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("StaffID", shiftStaff.staffID);
        values.put("ShiftID", shiftStaff.shiftID);
        values.put("AgencyEmployeeName", "");
        values.put("NotOnShift", 0);
        values.put("ReasonNotOn", "");
        values.put("StartTime", FMConwayUtils.dateToString(shiftStaff.startTime, "yyyy-MM-dd HH:mm:ss", ""));
        values.put("EndTime", FMConwayUtils.dateToString(shiftStaff.endTime, "yyyy-MM-dd HH:mm:ss", ""));
        values.put("FirstAider", false);
        values.put("WorkedHours", 0);
        values.put("WorkedMins", 0);
        values.put("BreakHours", 0);
        values.put("BreakMins", 0);
        values.put("TravelHours", 0);
        values.put("TravelMins", 0);
        values.put("Ipv", false);
        values.put("PaidBreak", false);
        values.put("OnCall", false);

        // table has no primary key, so check if record exists to avoid inserting duplicates
        if (!exists(shiftStaff.shiftID, shiftStaff.staffID)) {
            db.insert("ShiftStaff", null, values);
        }
    }

    public static boolean exists(String shiftID, String staffID) {
        String query = "select * from ShiftStaff where ShiftID = ? and StaffID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, staffID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllShiftStaff() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ShiftStaff");
    }

    public static void deleteShiftStaff(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ShiftStaff WHERE ShiftID = '" + shiftID + "'");
    }

    public static ShiftStaff GetShiftStaff(String shiftID, String staffID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT * FROM ShiftStaff where StaffID = '" + staffID + "' AND ShiftID = '" + shiftID + "'";

        Cursor cursor = db.rawQuery(query, null);

        ShiftStaff shiftStaff = new ShiftStaff();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            shiftStaff.shiftID = shiftID;
            shiftStaff.staffID = staffID;
            shiftStaff.notOnShift = cursor.getInt(cursor.getColumnIndex("NotOnShift")) > 0;
            shiftStaff.reasonNotOn = cursor.getString(cursor.getColumnIndex("ReasonNotOn"));
            shiftStaff.firstAider = Integer.parseInt(cursor.getString(cursor.getColumnIndex("FirstAider"))) > 0;
            shiftStaff.onCall = Integer.parseInt(cursor.getString(cursor.getColumnIndex("OnCall"))) > 0;
            shiftStaff.ipv = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Ipv"))) > 0;
            shiftStaff.paidBreak = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PaidBreak"))) > 0;
            shiftStaff.workedHours = cursor.getInt(cursor.getColumnIndex("WorkedHours"));
            shiftStaff.workedMins = cursor.getInt(cursor.getColumnIndex("WorkedMins"));
            shiftStaff.breakHours = cursor.getInt(cursor.getColumnIndex("BreakHours"));
            shiftStaff.breakMins = cursor.getInt(cursor.getColumnIndex("BreakMins"));
            shiftStaff.travelHours = cursor.getInt(cursor.getColumnIndex("TravelHours"));
            shiftStaff.travelMins = cursor.getInt(cursor.getColumnIndex("TravelMins"));
            shiftStaff.agencyEmployeeName = cursor.getString(cursor.getColumnIndex("AgencyEmployeeName"));
            cursor.close();
        }

        return shiftStaff;
    }

    public static void UpdateShiftStaff(ShiftStaff shiftStaff) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ShiftID", shiftStaff.shiftID);
        values.put("StaffID", shiftStaff.staffID);
        values.put("NotOnShift", shiftStaff.notOnShift);
        values.put("ReasonNotOn", shiftStaff.reasonNotOn);
        values.put("WorkedHours", shiftStaff.workedHours);
        values.put("WorkedMins", shiftStaff.workedMins);
        values.put("BreakHours", shiftStaff.breakHours);
        values.put("BreakMins", shiftStaff.breakMins);
        values.put("TravelHours", shiftStaff.travelHours);
        values.put("TravelMins", shiftStaff.travelMins);
        values.put("Ipv", shiftStaff.ipv);
        values.put("PaidBreak", shiftStaff.paidBreak);
        values.put("OnCall", shiftStaff.onCall);

        db.update("ShiftStaff", values, "ShiftID = ? and StaffID = ?", new String[] { shiftStaff.shiftID, shiftStaff.staffID });
    }


    public static JSONArray GetShiftStaffToSend(String shiftID) throws Exception {
        JSONArray shiftStaff = new JSONArray();
        //List<ShiftStaff> shiftStaff = new LinkedList<ShiftStaff>();

        String query = "SELECT * FROM ShiftStaff ";
        query += "WHERE ShiftID = '" + shiftID + "'";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            JSONObject ssObject;
            if (cursor.moveToFirst()) {
                do {
                    ShiftStaff ss = new ShiftStaff();
                    ss.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                    ss.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                    ss.agencyEmployeeName = cursor.getString(cursor.getColumnIndex("AgencyEmployeeName"));
                    ss.notOnShift = Integer.parseInt(cursor.getString(cursor.getColumnIndex("NotOnShift"))) > 0;
                    ss.firstAider = Integer.parseInt(cursor.getString(cursor.getColumnIndex("FirstAider"))) > 0;
                    ss.reasonNotOn = cursor.getString(cursor.getColumnIndex("ReasonNotOn"));
                    ss.onCall = Integer.parseInt(cursor.getString(cursor.getColumnIndex("OnCall"))) > 0;
                    ss.ipv = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Ipv"))) > 0;
                    ss.paidBreak = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PaidBreak"))) > 0;
                    ss.workedHours = cursor.getInt(cursor.getColumnIndex("WorkedHours"));
                    ss.workedMins = cursor.getInt(cursor.getColumnIndex("WorkedMins"));
                    ss.breakHours = cursor.getInt(cursor.getColumnIndex("BreakHours"));
                    ss.breakMins = cursor.getInt(cursor.getColumnIndex("BreakMins"));
                    ss.travelHours = cursor.getInt(cursor.getColumnIndex("TravelHours"));
                    ss.travelMins = cursor.getInt(cursor.getColumnIndex("TravelMins"));

                    try {
                        String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                        if (startTime != null)
                            ss.startTime = format.parse(startTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                        if (endTime != null)
                            ss.endTime = format.parse(endTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    ssObject = ss.getJSONObject();
                    shiftStaff.put(ssObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "GetShiftStaffToSend");
            throw e;
        } finally {
            cursor.close();
        }

        return shiftStaff;
    }

    public static void SetStartTime(String staffID, String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        db.execSQL("UPDATE ShiftStaff SET StartTime = ? WHERE StaffID = '" + staffID + "' AND ShiftID = '" + shiftID + "'", new String[] { date });
    }

    public static void SetEndTime(String staffID, String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        db.execSQL("UPDATE ShiftStaff SET EndTime = ? WHERE StaffID = '" + staffID + "' AND ShiftID = '" + shiftID + "'", new String[] { date });
    }

    public static List<ShiftStaff> GetShiftStaff(String shiftID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "" +
            "select " +
            "ss.*, " +
            "s.Forename " +
            "from " +
            "ShiftStaff as ss " +
            "inner join Staff as s on s.StaffID = ss.StaffID " +
            "where " +
            "ss.NotOnShift = 0 and " +
            "ss.ShiftID = ?";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        List<ShiftStaff> shiftStaff = new LinkedList<>();

        Cursor cursor = db.rawQuery(query, new String[] { shiftID });
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    ShiftStaff ss = new ShiftStaff();
                    ss.shiftID = shiftID;
                    ss.staffID = cursor.getString(cursor.getColumnIndex("StaffID"));
                    ss.staffFullname = cursor.getString(cursor.getColumnIndex("Forename"));
                    ss.notOnShift = cursor.getInt(cursor.getColumnIndex("NotOnShift")) > 0;
                    ss.reasonNotOn = cursor.getString(cursor.getColumnIndex("ReasonNotOn"));
                    ss.firstAider = Integer.parseInt(cursor.getString(cursor.getColumnIndex("FirstAider"))) > 0;
                    ss.onCall = Integer.parseInt(cursor.getString(cursor.getColumnIndex("OnCall"))) > 0;
                    ss.ipv = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Ipv"))) > 0;
                    ss.paidBreak = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PaidBreak"))) > 0;
                    ss.workedHours = cursor.getInt(cursor.getColumnIndex("WorkedHours"));
                    ss.workedMins = cursor.getInt(cursor.getColumnIndex("WorkedMins"));
                    ss.breakHours = cursor.getInt(cursor.getColumnIndex("BreakHours"));
                    ss.breakMins = cursor.getInt(cursor.getColumnIndex("BreakMins"));
                    ss.travelHours = cursor.getInt(cursor.getColumnIndex("TravelHours"));
                    ss.travelMins = cursor.getInt(cursor.getColumnIndex("TravelMins"));

                    try {
                        String startTime = cursor.getString(cursor.getColumnIndex("StartTime"));
                        if (startTime != null)
                            ss.startTime = format.parse(startTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        String endTime = cursor.getString(cursor.getColumnIndex("EndTime"));
                        if (endTime != null)
                            ss.endTime = format.parse(endTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    shiftStaff.add(ss);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();

        return shiftStaff;
    }
}
