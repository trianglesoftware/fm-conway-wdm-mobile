package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class TaskEquipmentAsset {
    public String TaskEquipmentAssetID;
    public String TaskEquipmentID;
    public int ItemNumber;
    public String AssetNumber;
    public String ShiftID;

    private ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put("TaskEquipmentAssetID", TaskEquipmentAssetID);
        values.put("TaskEquipmentID", TaskEquipmentID);
        values.put("ItemNumber", ItemNumber);
        values.put("AssetNumber", AssetNumber);
        values.put("ShiftID", ShiftID);
        return values;
    }

    public void insertOrUpdate(){
        if(exists()){
            update();
        }else{
            insert();
        }
    }

    public boolean exists() {
        if(TaskEquipmentAssetID == null){
            return false;
        }
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + "TaskEquipmentAssets" + " WHERE " + "TaskEquipmentAssetID" + " = ?", new String[]{String.valueOf(TaskEquipmentAssetID)});
        boolean res = cur.moveToFirst();
        cur.close();

        return res;
    }

    public void insert() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.insert("TaskEquipmentAssets", null, getContentValues());
    }

    public void update() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.update("TaskEquipmentAssets", getContentValues(), "TaskEquipmentAssetID = ?", new String[] { TaskEquipmentAssetID });
    }

    public void delete() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.delete("TaskEquipmentAssets", "TaskEquipmentAssetID = ?", new String[] { TaskEquipmentAssetID });
    }

    public static TaskEquipmentAsset single(String taskEquipmentAssetID) {
        return where("TaskEquipmentAssetID", taskEquipmentAssetID).get(0);
    }

    public static List<TaskEquipmentAsset> byTaskEquipment(String taskEquipmentID) {
        return where("TaskEquipmentID", taskEquipmentID);
    }

    private static List<TaskEquipmentAsset> where(String... args) {
        List<TaskEquipmentAsset> results = new LinkedList<>();

        List<String> listArgs = new LinkedList<>();
        String whereClause = "where ";
        boolean a = false;
        for (int b = 0; b < args.length; b++) {
            if (a) {
                listArgs.add(args[b]);
                continue;
            }
            whereClause += args[b] + " = ? ";
            if (b + 2 < args.length) {
                whereClause += " and ";
            }
            a = !a;
        }
        
        String orderBy = " order by ItemNumber";

        String query = "" +
            "select " +
            "tea.TaskEquipmentAssetID, " +
            "tea.TaskEquipmentID, " +
            "tea.ItemNumber, " +
            "tea.AssetNumber, " +
            "tea.ShiftID " +
            "from " +
            "TaskEquipmentAssets as tea " +
            whereClause + orderBy;

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String[] arrayArgs = new String[listArgs.size()];
        arrayArgs = listArgs.toArray(arrayArgs);
        Cursor cursor = db.rawQuery(query, arrayArgs);

        if (cursor.moveToFirst()) {
            do {
                TaskEquipmentAsset model = new TaskEquipmentAsset();
                model.TaskEquipmentAssetID = cursor.getString(cursor.getColumnIndex("TaskEquipmentAssetID"));
                model.TaskEquipmentID = cursor.getString(cursor.getColumnIndex("TaskEquipmentID"));
                model.ItemNumber = cursor.getInt(cursor.getColumnIndex("ItemNumber"));
                model.AssetNumber = cursor.getString(cursor.getColumnIndex("AssetNumber"));
                model.ShiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                results.add(model);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return results;
    }

}