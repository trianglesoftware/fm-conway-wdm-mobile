package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class LoadingSheetEquipment {
    public String loadingSheetEquipmentID;
    public String loadingSheetID;
    public String equipmentID;
    public int quantity;
    public String vmsOrAsset;
    public int unansweredQuestions;
    public int sheetOrder;
    public String shiftID;

    public String equipmentName;
    public String equipmentType;
    public int equipmentItems;

    public LoadingSheetEquipment(){
    }

    public LoadingSheetEquipment(String LoadingSheetEquipmentID, String LoadingSheetID, String EquipmentID, int Quantity, int SheetOrder, String VmsOrAsset)
    {
        super();
        this.loadingSheetEquipmentID = LoadingSheetEquipmentID;
        this.loadingSheetID = LoadingSheetID;
        this.equipmentID = EquipmentID;
        this.quantity = Quantity;
        this.sheetOrder = SheetOrder;
        this.vmsOrAsset = VmsOrAsset;
    }

    private String getLoadingSheetEquipmentID() { return this.loadingSheetEquipmentID; }
    private String getLoadingSheetID() { return this.loadingSheetID; }
    private String getEquipmentID() { return this.equipmentID; }
    private int getQuantity() { return this.quantity; }
    private int getSheetOrder() { return this.sheetOrder; }
    private String getShiftID() {return this.shiftID;}
    public String getEquipmentName() { return this.equipmentName; }
    public String getEquipmentType() { return this.equipmentType; }
    public String getVmsOrAsset() { return this.vmsOrAsset; }
    public int getUnansweredQuestions() { return this.unansweredQuestions; }

    public void setLoadingSheetEquipmentID(String loadingSheetEquipmentID) { this.loadingSheetEquipmentID = loadingSheetEquipmentID; }
    public void setLoadingSheetID(String loadingSheetID) { this.loadingSheetID = loadingSheetID; }
    public void setEquipmentID(String equipmentID) { this.equipmentID = equipmentID; }
    public void setQuantity(int quantity) { this.quantity = quantity; }
    public void setSheetOrder(int sheetOrder) { this.sheetOrder = sheetOrder; }
    public void setShiftID(String shiftID) { this.shiftID = shiftID;}
    public void setEquipmentName(String equipmentName) { this.equipmentName = equipmentName; }
    public void setEquipmentType(String equipmentType) { this.equipmentType = equipmentType; }
    public void setVmsOrAsset(String vmsOrAsset) { this.vmsOrAsset = vmsOrAsset; }
    public void setUnansweredQuestions() { this.unansweredQuestions = unansweredQuestions; }

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();
        obj.put("LoadingSheetEquipmentID", this.loadingSheetEquipmentID);
        obj.put("LoadingSheetID", this.loadingSheetID);
        obj.put("EquipmentID", this.equipmentID);
        obj.put("Quantity", this.quantity);
        obj.put("SheetOrder", this.sheetOrder);
        obj.put("EquipmentName", this.equipmentName);
        obj.put("EquipmentType", this.equipmentType);
        obj.put("VmsOrAsset", this.vmsOrAsset);
        obj.put("UnansweredQuestions", this.unansweredQuestions);

        return obj;
    }

    // Loading Sheet Equipment
    public static void addLoadingSheetEquipment(LoadingSheetEquipment loadingSheetEquipment)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("LoadingSheetEquipmentID", loadingSheetEquipment.loadingSheetEquipmentID);
        values.put("LoadingSheetID", loadingSheetEquipment.loadingSheetID);
        values.put("EquipmentID", loadingSheetEquipment.equipmentID);
        values.put("Quantity", loadingSheetEquipment.quantity);
        values.put("SheetOrder", loadingSheetEquipment.sheetOrder);
        values.put("ShiftID", loadingSheetEquipment.shiftID);
        values.put("VmsOrAsset", loadingSheetEquipment.vmsOrAsset);

        db.insert("LoadingSheetEquipment", null, values);
    }

    public static void deleteAllLoadingSheetEquipment()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheetEquipment");
    }

    public static void deleteLoadingSheetEquipmentForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM LoadingSheetEquipment WHERE ShiftID = '" + shiftID + "'");
    }

    public static List<LoadingSheetEquipment> createEquipmentItemsIfRequired(String shiftID, String vehicleID) {
        List<LoadingSheetEquipment> results = new LinkedList<>();

        String query = "";
        query += "select ";
        query += "lse.LoadingSheetEquipmentID, ";
        query += "lse.Quantity, ";
        query += "et.VmsOrAsset, ";
        query += "coalesce(lsei.EquipmentItems, 0) as EquipmentItems ";
        query += "from ";
        query += "Equipment as e ";
        query += "inner join EquipmentTypes as et on et.EquipmentTypeID = e.EquipmentTypeID ";
        query += "inner join LoadingSheetEquipment as lse on lse.EquipmentID = e.EquipmentID ";
        query += "inner join LoadingSheets as ls on ls.LoadingSheetID = lse.LoadingSheetID ";
        query += "left join ( ";
        query += "  select ";
        query += "  LoadingSheetEquipmentID, ";
        query += "  count(*) as EquipmentItems ";
        query += "  from ";
        query += "  LoadingSheetEquipmentItems as lsei ";
        query += "  group by ";
        query += "  lsei.LoadingSheetEquipmentID ";
        query += ") as lsei on lsei.LoadingSheetEquipmentID = lse.LoadingSheetEquipmentID ";
        query += "where ";
        query += "ls.ShiftID = ? and ls.VehicleID = ? ";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { shiftID, vehicleID });

        if(cursor.moveToFirst()){
            do {
                LoadingSheetEquipment a = new LoadingSheetEquipment();
                a.loadingSheetEquipmentID = cursor.getString(cursor.getColumnIndex("LoadingSheetEquipmentID"));
                a.quantity = cursor.getInt(cursor.getColumnIndex("Quantity"));
                a.vmsOrAsset = cursor.getString(cursor.getColumnIndex("VmsOrAsset"));
                a.equipmentItems = cursor.getInt(cursor.getColumnIndex("EquipmentItems"));

                results.add(a);
            } while(cursor.moveToNext());
        }

        cursor.close();

        return results;
    }

    public static Integer GetNextSheetOrder(String loadingSheetID){
        Integer res = 1;
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT MAX(SheetOrder) FROM LoadingSheetEquipment WHERE LoadingSheetID = '" + loadingSheetID + "'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()) {
            res = cursor.getInt(0) + 1;
        }

        return res;
    }

}
