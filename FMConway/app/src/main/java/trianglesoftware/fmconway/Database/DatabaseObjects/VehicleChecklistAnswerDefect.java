package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class VehicleChecklistAnswerDefect {
    public String vehicleChecklistID;
    public String checklistQuestionID;
    public String defect;
    public String userID;
    public List<VehicleChecklistAnswerDefectPhoto> photos;

    public VehicleChecklistAnswerDefect(){
    }

    public VehicleChecklistAnswerDefect(String VehicleChecklistID, String ChecklistQuestionID, String Defect)
    {
        super();
        this.vehicleChecklistID = VehicleChecklistID;
        this.checklistQuestionID = ChecklistQuestionID;
        this.defect = Defect;
    }

    private String getVehicleChecklistID() { return this.vehicleChecklistID; }
    private String getChecklistQuestionID() { return this.checklistQuestionID; }
    public String getDefect() { return this.defect; }
    public String getUserID() { return this.userID;}

    public void setVehicleChecklistID(String vehicleChecklistID) { this.vehicleChecklistID = vehicleChecklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setDefect(String defect) { this.defect = defect; }
    public void setUserID(String userID) { this.userID = userID;}

    public JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("VehicleChecklistID", this.vehicleChecklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Defect", this.defect);

        return obj;
    }

    //Vehicle Checklist Answer Defects
    public static void addVehicleChecklistAnswerDefect(VehicleChecklistAnswerDefect vehicleChecklistAnswerDefect)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleChecklistID", vehicleChecklistAnswerDefect.vehicleChecklistID);
        values.put("ChecklistQuestionID", vehicleChecklistAnswerDefect.checklistQuestionID);
        values.put("Defect", vehicleChecklistAnswerDefect.defect);
        values.put("UserID", vehicleChecklistAnswerDefect.userID);

        db.insert("VehicleChecklistAnswerDefects", null, values);
    }

    public static void deleteAllVehicleChecklistAnswerDefects()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswerDefects");
    }

    public static VehicleChecklistAnswerDefect GetVehicleDefect(String vehicleChecklistID, String checklistQuestionID) {
        String query = "SELECT * FROM VehicleChecklistAnswerDefects ";
        query += "WHERE VehicleChecklistID = '" + vehicleChecklistID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        VehicleChecklistAnswerDefect vcad = null;
        if(cursor.moveToFirst()){
            do{
                vcad = new VehicleChecklistAnswerDefect();
                vcad.vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                vcad.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                vcad.defect = cursor.getString(cursor.getColumnIndex("Defect"));
                vcad.userID = cursor.getString(cursor.getColumnIndex("UserID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return vcad;
    }

    public static void UpdateVehicleDefect(VehicleChecklistAnswerDefect defect) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleChecklistID", defect.vehicleChecklistID);
        values.put("ChecklistQuestionID", defect.checklistQuestionID);
        values.put("Defect", defect.defect);

        db.update("VehicleChecklistAnswerDefects", values, "VehicleChecklistID = ? and ChecklistQuestionID = ?", new String[]{defect.vehicleChecklistID, defect.checklistQuestionID});
    }
    
    public static boolean exists(String vehicleChecklistID, String checklistQuestionID) {
        String query = "" +
            "select " +
            "* " +
            "from " +
            "VehicleChecklistAnswerDefects " +
            "where " +
            "VehicleChecklistID = ? and ChecklistQuestionID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { vehicleChecklistID, checklistQuestionID });
        
        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }
}
