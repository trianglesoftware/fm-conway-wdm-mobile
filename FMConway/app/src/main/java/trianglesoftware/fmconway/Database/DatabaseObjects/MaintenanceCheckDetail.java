package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Adam.Patrick on 31/08/2016.
 */
public class MaintenanceCheckDetail {
    public String maintenanceCheckID;
    public String checklistQuestionID;
    public String description;
    public String activityID;

    public MaintenanceCheckDetail() {}

    public String getMaintenanceCheckID() { return  this.maintenanceCheckID;}
    public String getChecklistQuestionID() { return this.checklistQuestionID;}
    public String getDescription() { return this.description;}
    public String getActivityID() {return this.activityID;}

    public void setMaintenanceCheckID(String maintenanceCheckID) {this.maintenanceCheckID = maintenanceCheckID;}
    public void setChecklistQuestionID(String checklistQuestionID) {this.checklistQuestionID = checklistQuestionID;}
    public void setDescription(String description) { this.description = description;}
    public void setActivityID(String activityID) { this.activityID = activityID;}

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("MaintenanceCheckID", this.maintenanceCheckID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Description", this.description);

        return obj;
    }

    public static void AddMaintenanceCheckDetail(MaintenanceCheckDetail maintenanceCheckDetail) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();

        if(maintenanceCheckDetail.maintenanceCheckID != null)
        {
            values.put("MaintenanceCheckID", maintenanceCheckDetail.maintenanceCheckID);
        }
        else
        {
            values.put("MaintenanceCheckID", UUID.randomUUID().toString());
        }

        values.put("ChecklistQuestionID", maintenanceCheckDetail.checklistQuestionID);
        values.put("Description", maintenanceCheckDetail.description);
        values.put("ActivityID", maintenanceCheckDetail.activityID);

        db.insert("MaintenanceCheckDetails", null, values);
    }

    public static void deleteAllMaintenanceCheckDetails() {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetails");
    }

    public static void deleteMaintenanceCheckDetailsForActivity(String activityID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetails WHERE ActivityID = '" + activityID + "'");
    }

    public static void deleteMaintenanceCheckDetailForCheck(String maintenanceCheckID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM MaintenanceCheckDetails WHERE MaintenanceCheckID = '" + maintenanceCheckID + "'");
    }

    public static void UpdateMaintenanceCheckDetails(String maintenanceCheckID, String checklistQuestionID, String description) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        //db.execSQL("UPDATE MaintenanceCheckDetails SET Description = '" + description + "' WHERE MaintenanceCheckID = " + maintenanceCheckID + " AND ChecklistQuestionID = " + checklistQuestionID);
        //db.execSQL("UPDATE MaintenanceCheckDetails SET Description = ? WHERE MaintenanceCheckID = " + maintenanceCheckID + " AND ChecklistQuestionID = " + checklistQuestionID, new String[]{description});

        SQLiteStatement statement = db.compileStatement("UPDATE MaintenanceCheckDetails SET Description = ? WHERE MaintenanceCheckID = ? AND ChecklistQuestionID = ?");
        statement.bindString(1,description);
        statement.bindString(2,maintenanceCheckID);
        statement.bindString(3,checklistQuestionID);
        statement.execute();
        statement.close();
    }

    public static MaintenanceCheckDetail GetMaintenanceCheckDetail(String maintenanceCheckID, String checklistQuestionID)
    {
        MaintenanceCheckDetail detail = null;

        String query = "SELECT * FROM MaintenanceCheckDetails WHERE MaintenanceCheckID = '" + maintenanceCheckID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do{
                detail = new MaintenanceCheckDetail();
                detail.maintenanceCheckID = cursor.getString(cursor.getColumnIndex("MaintenanceCheckID"));
                detail.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                detail.description = cursor.getString(cursor.getColumnIndex("Description"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return detail;
    }
}
