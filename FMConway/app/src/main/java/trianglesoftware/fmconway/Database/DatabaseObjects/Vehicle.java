package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 07/03/2016.
 */
public class Vehicle {
    public String vehicleID;
    public String make;
    public String model;
    public String registration;
    public String depotID;
    public String vehicleTypeID;
    public String staffID;
    public int startMileage;
    public String driver;

    public Vehicle(){
    }

    public Vehicle(String VehicleID, String Make, String Model, String Registration, String DepotID, String VehicleTypeID)
    {
        super();
        this.vehicleID = VehicleID;
        this.make = Make;
        this.model = Model;
        this.registration = Registration;
        this.depotID = DepotID;
        this.vehicleTypeID = VehicleTypeID;
    }

    public String getVehicleID() { return this.vehicleID; }
    public String getMake() { return this.make; }
    public String getModel() { return this.model; }
    public String getRegistration() { return this.registration; }
    public String getDepotID() { return this.depotID; }
    public String getVehicleTypeID() { return this.vehicleTypeID; }
    public String getDriver() { return this.driver;}

    public void setVehicleID(String vehicleID) { this.vehicleID = vehicleID; }
    public void setMake(String make) { this.make = make; }
    public void setModel(String model) { this.model = model; }
    public void setRegistration(String registration) { this.registration = registration; }
    public void setDepotID(String depotID) { this.depotID = depotID; }
    public void setVehicleTypeID(String vehicleTypeID) { this.vehicleTypeID = vehicleTypeID; }
    public void setDriver(String driver) { this.driver = driver;}

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();
        obj.put("VehicleID", this.vehicleID);
        obj.put("Make", this.make);
        obj.put("Model", this.model);
        obj.put("Registration", this.registration);
        obj.put("DepotID", this.depotID);
        obj.put("VehicleTypeID", this.vehicleTypeID);
        obj.put("Driver", this.driver);
        obj.put("StaffID", this.staffID);

        return obj;
    }

    // Vehicles
    public static void addVehicle(Vehicle vehicle)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleID", vehicle.vehicleID);
        values.put("Make", vehicle.make);
        values.put("Model", vehicle.model);
        values.put("Registration", vehicle.registration);
        values.put("DepotID", vehicle.depotID);
        values.put("VehicleTypeID", vehicle.vehicleTypeID);

        db.insert("Vehicles", null, values);
    }

    public static void updateVehicle(Vehicle vehicle) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleID", vehicle.vehicleID);
        values.put("Make", vehicle.make);
        values.put("Model", vehicle.model);
        values.put("Registration", vehicle.registration);
        values.put("DepotID", vehicle.depotID);
        values.put("VehicleTypeID", vehicle.vehicleTypeID);

        db.update("Vehicles", values, "VehicleID = ?", new String[]{vehicle.vehicleID});
    }

    public static boolean exists(String vehicleID) {
        String query = "select * from Vehicles where VehicleID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { vehicleID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void deleteAllVehicles()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM Vehicles");
    }

    public static Vehicle GetVehicleFromName(String vehicleName) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        String query = "SELECT VehicleID FROM Vehicles where Make || ' ' || Model || ' ' || Registration = '" + vehicleName + "'";

        Cursor cursor = db.rawQuery(query, null);

        Vehicle vehicle = new Vehicle();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            vehicle.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
            cursor.close();
        }

        return vehicle;
    }

    public static List<Vehicle> GetVehiclesNotOnShift(String searchTerm) {
        List<Vehicle> vehicle = new LinkedList<>();

        // select query
        String query = "";
        query += "SELECT v.* FROM Vehicles v";
        query += " WHERE Make || ' ' || Model || ' ' || Registration LIKE '%" + searchTerm + "%'";
        query += " ORDER BY Make, Model, Registration";
        query += " LIMIT 0,5";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Vehicle v;
        if(cursor.moveToFirst()){
            do{
                v = new Vehicle();
                v.vehicleID = cursor.getString(cursor.getColumnIndex("VehicleID"));
                v.make = cursor.getString(cursor.getColumnIndex("Make"));
                v.model = cursor.getString(cursor.getColumnIndex("Model"));
                v.registration = cursor.getString(cursor.getColumnIndex("Registration"));

                vehicle.add(v);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return vehicle;
    }
}
