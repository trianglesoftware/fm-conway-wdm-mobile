package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class WeatherCondition {
    public String weatherConditionID;
    public String name;

    public JSONObject getJSONObject() throws Exception {
        JSONObject obj = new JSONObject();
        obj.put("WeatherConditionID", this.weatherConditionID);
        obj.put("Name", this.name);
        return obj;
    }
    
    public static void addWeatherCondition(WeatherCondition weatherCondition)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("WeatherConditionID", weatherCondition.weatherConditionID);
        values.put("Name", weatherCondition.name);
        
        db.insert("WeatherConditions", null, values);
    }

    public static void deleteAllWeatherConditions()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM WeatherConditions");
    }

    public static List<WeatherCondition> getAllWeatherConditions() {
        List<WeatherCondition> results = new LinkedList<>();

        String query = "" +
            "select " +
            "* " +
            "from " +
            "WeatherConditions as wc";

        SQLiteDatabase db = Database.MainDB.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        
        if (cursor.moveToFirst()) {
            do {
                WeatherCondition model = new WeatherCondition();
                model.weatherConditionID = cursor.getString(cursor.getColumnIndex("WeatherConditionID"));
                model.name = cursor.getString(cursor.getColumnIndex("Name"));
                results.add(model);
            } while (cursor.moveToNext());
        }

        cursor.close();
        
        return results;
    }
}
