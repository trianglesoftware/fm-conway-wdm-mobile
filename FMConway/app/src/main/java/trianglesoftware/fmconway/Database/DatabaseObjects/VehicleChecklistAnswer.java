package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class VehicleChecklistAnswer {
    public String vehicleChecklistID;
    public String checklistQuestionID;
    public int answer;
    public boolean isNew;
    public String question;
    public String userID;

    public VehicleChecklistAnswer(){
    }

    public VehicleChecklistAnswer(String VehicleChecklistID, String ChecklistQuestionID, int Answer)
    {
        super();
        this.vehicleChecklistID = VehicleChecklistID;
        this.checklistQuestionID = ChecklistQuestionID;
        this.answer = Answer;
    }

    private String getVehicleChecklistID() { return this.vehicleChecklistID; }
    private String getChecklistQuestionID() { return this.checklistQuestionID; }
    public int getAnswer() { return this.answer; }
    public boolean getIsNew() { return this.isNew; }
    public String getQuestion() { return this.question; }
    public String getUserID() { return this.userID;}

    public void setVehicleChecklistID(String vehicleChecklistID) { this.vehicleChecklistID = vehicleChecklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setAnswer(int answer) { this.answer = answer; }
    public void setNew(boolean isNew) { this.isNew = isNew; }
    private void setQuestion(String question) { this.question = question; }
    public void setUserID(String userID) {this.userID = userID;}

    public JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

            obj.put("VehicleChecklistID", this.vehicleChecklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Answer", this.answer);
            obj.put("Question", this.question);
            obj.put("IsNew", this.isNew);

        return obj;
    }

    public JSONObject getJSONObjectToSend() throws JSONException{

        JSONObject obj = new JSONObject();

            obj.put("VehicleChecklistID", this.vehicleChecklistID);
            obj.put("ChecklistQuestionID", this.checklistQuestionID);
            obj.put("Answer", this.answer);
            obj.put("Question", this.question);


        return obj;
    }

    //Vehicle Checklist Answers
    public static void addVehicleChecklistAnswer(VehicleChecklistAnswer vehicleChecklistAnswer)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleChecklistID", vehicleChecklistAnswer.vehicleChecklistID);
        values.put("ChecklistQuestionID", vehicleChecklistAnswer.checklistQuestionID);
        values.put("Answer", vehicleChecklistAnswer.answer);
        values.put("UserID", vehicleChecklistAnswer.userID);

        db.insert("VehicleChecklistAnswers", null, values);
    }

    public static boolean exists(String vehicleChecklistID, String checklistQuestionID) {
        String query = "select * from VehicleChecklistAnswers where VehicleChecklistID = ? and ChecklistQuestionID = ?";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { vehicleChecklistID, checklistQuestionID });

        boolean exists = cursor.moveToFirst();

        cursor.close();

        return exists;
    }

    public static void addVehicleChecklistAnswerNew(VehicleChecklistAnswer vehicleChecklistAnswer)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("VehicleChecklistID", vehicleChecklistAnswer.vehicleChecklistID);
        values.put("ChecklistQuestionID", vehicleChecklistAnswer.checklistQuestionID);
        values.put("UserID", vehicleChecklistAnswer.userID);
        //values.put("Answer", vehicleChecklistAnswer.getAnswer());

        db.insert("VehicleChecklistAnswers", null, values);
    }

    public static void deleteAllVehicleChecklistAnswers()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswers");
    }

    public static void deleteVehicleChecklistAnswers(String vehicleChecklistID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswers WHERE VehicleChecklistID = '" + vehicleChecklistID + "'");
    }

    public static List<VehicleChecklistAnswer> GetVehicleChecklistAnswers(String vehicleChecklistID) {
        List<VehicleChecklistAnswer> vehicleChecklistAnswers = new LinkedList<>();

        String query = "SELECT vca.*, cq.Question FROM VehicleChecklistAnswers vca ";
        query += "INNER JOIN ChecklistQuestions cq on cq.ChecklistQuestionID = vca.ChecklistQuestionID ";
        query += "WHERE VehicleChecklistID = '" + vehicleChecklistID;
        query += "' ORDER BY cq.QuestionOrder";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        VehicleChecklistAnswer vca;
        if(cursor.moveToFirst()){
            do{
                vca = new VehicleChecklistAnswer();
                vca.vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                vca.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));

                int columnIndex = cursor.getColumnIndex("Answer");
                if (!cursor.isNull(columnIndex)) {
                    vca.answer = Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer")));
                }
                else
                {
                    vca.isNew = true;
                }

                //vca.setAnswer(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Answer"))) > 0);
                vca.question = cursor.getString(cursor.getColumnIndex("Question"));

                vehicleChecklistAnswers.add(vca);
            }while(cursor.moveToNext());
        }

        cursor.close();

        return vehicleChecklistAnswers;
    }

    public static void SetAnswer(String checklistQuestionID, String vehicleChecklistID, int answer) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("Update VehicleChecklistAnswers Set Answer = " + answer + " WHERE VehicleChecklistID = '" + vehicleChecklistID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'");
    }
}
