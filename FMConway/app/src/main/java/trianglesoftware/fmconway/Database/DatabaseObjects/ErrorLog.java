package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayApp;

/**
 * Created by Adam.Patrick on 06/09/2016.
 */
public class ErrorLog {
    public String logID;
    public String description;
    public String exception;
    public boolean sent;

    public String getLogID() { return this.logID;}
    public String getDescription() { return this.description;}
    public String getException() { return this.exception;}
    public boolean getSent() { return this.sent;}

    public void setLogID(String logID) { this.logID = logID;}
    public void setDescription(String description) {this.description = description;}
    public void setException(String exception) {this.exception = exception;}
    public void setSent(boolean sent) {this.sent = sent;}

//    public ErrorLog(String description, String exception, boolean sent)
//    {
//
//    }

    public JSONObject getJSONObject() throws  JSONException{

        JSONObject obj = new JSONObject();

            obj.put("Description", this.description);
            obj.put("Exception", this.exception);

        return obj;
    }
    
    public static void AddSimpleLog(String text)
    {
        try {
            // add to log file
            File fLog = getAlbumStorageDir(FMConwayApp.getContext().getPackageName());
            File f = new File(fLog, "log.txt");

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

            FileWriter fw = new FileWriter(f, true);
            fw.append("=====================================\r\n\r\n");
            fw.append("Date: " + sdf.format(new Date()) + "\r\n\r\n");
            fw.append("Description: " + text + "\r\n\r\n");
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void AddErrorLog(ErrorLog error)
    {
        try {
            // add to local database
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put("LogID", UUID.randomUUID().toString());
            values.put("Description", error.description);
            values.put("Exception", error.exception);
            values.put("Sent", error.sent);

            db.insert("ErrorLog", null, values);

            // add to log file
            File fLog = getAlbumStorageDir(FMConwayApp.getContext().getPackageName());
            File f = new File(fLog, "log.txt");

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

            FileWriter fw = new FileWriter(f, true);
            fw.append("=====================================\r\n\r\n");
            fw.append("Date: " + sdf.format(new Date()) + "\r\n\r\n");
            fw.append("Description: " + error.description + "\r\n\r\n");
            fw.append("Exception: " + "\r\n" + error.exception + "\r\n\r\n");
            fw.close();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), albumName);

        if (!file.mkdirs()) {
            Log.e("Log Export", "Directory not created");
        }

        return file;
    }

    public static void deleteErrorLogs()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM ErrorLog");
    }

    public static JSONArray GetErrorLogsToSend() throws Exception{
        JSONArray logs = new JSONArray();

        String query = "SELECT * FROM ErrorLog ";
        query += "WHERE Sent = 0";

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject eObject;
            if (cursor.moveToFirst()) {
                do {
                    ErrorLog e = new ErrorLog();
                    e.description = cursor.getString(cursor.getColumnIndex("Description"));
                    e.exception = cursor.getString(cursor.getColumnIndex("Exception"));

                    eObject = e.getJSONObject();

                    logs.put(eObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetErrorLogsToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return logs;
    }

    public static void CreateError(Throwable e, String from, String extraInfo){
        String LINE_SEPARATOR = "\n";

        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        if(e != null) {
            errorReport.append(e.toString());
        }
        if(extraInfo != null){
            errorReport.append("\n");
            errorReport.append(extraInfo);
        }
        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK_INT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(BuildConfig.VERSION_NAME);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);

        ErrorLog error = new ErrorLog();

        error.logID = UUID.randomUUID().toString();
        error.description = from;
        error.exception = errorReport.toString();

        Log.e("Error",errorReport.toString());

        ErrorLog.AddErrorLog(error);
    }

    public static void CreateError(Throwable e, String from)
    {
        CreateError(e, from, null);
    }

}
