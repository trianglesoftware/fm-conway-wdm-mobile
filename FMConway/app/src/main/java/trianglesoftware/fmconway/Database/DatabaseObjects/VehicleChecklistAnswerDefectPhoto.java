package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.Date;

import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 09/03/2016.
 */
public class VehicleChecklistAnswerDefectPhoto {
    public String vehicleChecklistDefectPhotoID;
    public String vehicleChecklistID;
    public String checklistQuestionID;
    public String userID;
    public double longitude;
    public double latitude;
    public String imageLocation;
    public Bitmap imageData;
    public Date time;
    public String shiftID;

    public VehicleChecklistAnswerDefectPhoto(){
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        if(this.time != null){
            return dateFormat.format(this.time);
        }
        else {
            return "";
        }
    }

    public void setVehicleChecklistID(String vehicleChecklistID) { this.vehicleChecklistID = vehicleChecklistID; }
    public void setChecklistQuestionID(String checklistQuestionID) { this.checklistQuestionID = checklistQuestionID; }
    public void setUserID(String userID) { this.userID = userID; }
    public void setLongitude(double longitude) { this.longitude = longitude; }
    public void setLatitude(double latitude) { this.latitude = latitude; }
    public void setImageLocation(String imageLocation) { this.imageLocation = imageLocation; }
    private void setImageData(Bitmap imageData) {
        this.imageData = imageData;
    }

    private JSONObject getJSONObject() throws JSONException{

        JSONObject obj = new JSONObject();
        obj.put("VehicleChecklistDefectPhotoID", this.vehicleChecklistDefectPhotoID);
        obj.put("VehicleChecklistID", this.vehicleChecklistID);
        obj.put("ChecklistQuestionID", this.checklistQuestionID);
        obj.put("UserID", this.userID);
        obj.put("Longitude", this.longitude);
        obj.put("Latitude", this.latitude);
        obj.put("ImageLocation", this.imageLocation);
        obj.put("Time", getTime());

        if (!TextUtils.isEmpty(this.imageLocation)) {
            Bitmap bm = FMConwayUtils.filepathToBitmap(this.imageLocation);
            if (bm != null) {
                bm = FMConwayUtils.bitmapRotationFix(bm);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                byte[] byteArray = baos.toByteArray();

                String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                obj.put("DataString", base64);
            }
        }

        return obj;
    }

    //Vehicle Checklist Answer Defect Photos
    public static void addVehicleChecklistAnswerDefectPhoto(VehicleChecklistAnswerDefectPhoto vehicleChecklistAnswerDefectPhoto)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ContentValues values = new ContentValues();

        // new photos will have empty ID, where as photos pulled from service will have an ID
        if (TextUtils.isEmpty(vehicleChecklistAnswerDefectPhoto.vehicleChecklistDefectPhotoID)) {
            vehicleChecklistAnswerDefectPhoto.vehicleChecklistDefectPhotoID = UUID.randomUUID().toString();
        }
        
        values.put("VehicleChecklistDefectPhotoID", vehicleChecklistAnswerDefectPhoto.vehicleChecklistDefectPhotoID);
        values.put("VehicleChecklistID", vehicleChecklistAnswerDefectPhoto.vehicleChecklistID);
        values.put("ChecklistQuestionID", vehicleChecklistAnswerDefectPhoto.checklistQuestionID);
        values.put("UserID", vehicleChecklistAnswerDefectPhoto.userID);
        values.put("Longitude", vehicleChecklistAnswerDefectPhoto.longitude);
        values.put("Latitude", vehicleChecklistAnswerDefectPhoto.latitude);
        values.put("ImageLocation", vehicleChecklistAnswerDefectPhoto.imageLocation);
        values.put("Time", vehicleChecklistAnswerDefectPhoto.getTime());
        values.put("ShiftID", vehicleChecklistAnswerDefectPhoto.shiftID);

        db.insert("VehicleChecklistAnswerDefectPhotos", null, values);
    }

    public static void deleteVehicleChecklistAnswerDefectPhoto(String vehicleChecklistDefectPhotoID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswerDefectPhotos WHERE VehicleChecklistDefectPhotoID = '" + vehicleChecklistDefectPhotoID + "'");
    }

    public static void deleteVehicleChecklistPhotoFile(String vehicleChecklistDefectPhotoID)
    {
        try {
            String query = "SELECT * FROM VehicleChecklistAnswerDefectPhotos ";
            query += "WHERE VehicleChecklistDefectPhotoID = '" + vehicleChecklistDefectPhotoID + "'";

            SQLiteDatabase db = Database.MainDB.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            String imageLocation = "";
            if (cursor.moveToFirst()) {
                do {
                    imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                } while (cursor.moveToNext());
            }

            cursor.close();

            if (!Objects.equals(imageLocation, "")) {
                File file = new File(imageLocation);
                file.delete();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "DeleteVehicleChecklistPhotoFile");
        }
    }

    public static void deleteAllVehicleChecklistAnswerDefectPhotosForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswerDefectPhotos WHERE UserID = '" + userID + "'");
    }

    public static void deleteAllVehicleChecklistAnswerDefectPhotosForShift(String shiftID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM VehicleChecklistAnswerDefectPhotos WHERE ShiftID = '" + shiftID + "'");
    }

    public static ArrayList<VehicleChecklistAnswerDefectPhoto> getPhotosForDefect(String vehicleChecklistID, String checklistQuestionID) {
        ArrayList<VehicleChecklistAnswerDefectPhoto> photos = new ArrayList<>();

        String query = "SELECT * FROM VehicleChecklistAnswerDefectPhotos WHERE VehicleChecklistID = '" + vehicleChecklistID + "' AND ChecklistQuestionID = '" + checklistQuestionID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        VehicleChecklistAnswerDefectPhoto data;
        if(cursor.moveToFirst()) {
            do {
                data = new VehicleChecklistAnswerDefectPhoto();
                data.vehicleChecklistDefectPhotoID = cursor.getString(cursor.getColumnIndex("VehicleChecklistDefectPhotoID"));
                data.vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                data.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                data.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                data.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                data.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));
                data.imageData = FMConwayUtils.filepathToBitmap(data.imageLocation);

                try {
                    String time = cursor.getString(cursor.getColumnIndex("Time"));
                    if (time != null) {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        data.time = format.parse(time);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                
                data.shiftID = cursor.getString(cursor.getColumnIndex("ShiftID"));
                data.userID = cursor.getString(cursor.getColumnIndex("UserID"));

                photos.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return photos;
    }

    public static JSONArray GetDefectPhotosToSend(String shiftID, String userID) throws Exception{
        JSONArray photos = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String query = "SELECT p.*, vc.ShiftID, vc.VehicleID, vc.ChecklistID FROM VehicleChecklistAnswerDefectPhotos p ";
        query += "JOIN VehicleChecklists vc on vc.VehicleChecklistID = p.VehicleChecklistID ";
        query += "WHERE vc.ShiftID = '" + shiftID + "' AND vc.UserID = '" + userID + "'";

        if(Objects.equals(shiftID,""))
        {
            query = "SELECT p.*, vc.ShiftID, vc.VehicleID, vc.ChecklistID FROM VehicleChecklistAnswerDefectPhotos p ";
            query += "JOIN VehicleChecklists vc on vc.VehicleChecklistID = p.VehicleChecklistID ";
            query += "WHERE vc.UserID = '" + userID + "'";
        }

        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject pObject;
            if (cursor.moveToFirst()) {
                do {
                    VehicleChecklistAnswerDefectPhoto p = new VehicleChecklistAnswerDefectPhoto();
                    p.vehicleChecklistDefectPhotoID = cursor.getString(cursor.getColumnIndex("VehicleChecklistDefectPhotoID"));
                    p.vehicleChecklistID = cursor.getString(cursor.getColumnIndex("VehicleChecklistID"));
                    p.checklistQuestionID = cursor.getString(cursor.getColumnIndex("ChecklistQuestionID"));
                    p.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Latitude")));
                    p.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex("Longitude")));
                    p.imageLocation = cursor.getString(cursor.getColumnIndex("ImageLocation"));

                    try {
                        String time = cursor.getString(cursor.getColumnIndex("Time"));
                        if (time != null)
                            p.time = format.parse(time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    pObject = p.getJSONObject();

                    try {
                        pObject.put("VehicleID", cursor.getString(cursor.getColumnIndex("VehicleID")));
                        pObject.put("ShiftID", cursor.getString(cursor.getColumnIndex("ShiftID")));
                        pObject.put("ChecklistID", cursor.getString(cursor.getColumnIndex("ChecklistID")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    photos.put(pObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"GetDefectPhotosToSend");
            throw e;
        }
        finally {
            cursor.close();
        }

        return photos;
    }

    public static boolean exists(String vehicleChecklistDefectPhotoID)
    {
        String query = "" +
            "select " +
            "VehicleChecklistDefectPhotoID " +
            "from " +
            "VehicleChecklistAnswerDefectPhotos " +
            "where " +
            "VehicleChecklistDefectPhotoID = ?";
    
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { vehicleChecklistDefectPhotoID });
        
        boolean exists = cursor.moveToFirst();
    
        cursor.close();
        
        return exists;
    }
}
