package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.Database;

public class ActivityMethodStatement {
    public String activityMethodStatementID;
    public String activityID;
    public String methodStatementID;
    public boolean isActive;

    public ActivityMethodStatement() {

    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ActivityMethodStatementID", this.activityMethodStatementID);
            obj.put("ActivityID", this.activityID);
            obj.put("MethodStatementID", this.methodStatementID);
            obj.put("IsActive", this.isActive);
        } catch (JSONException e) {
            ErrorLog.CreateError(e, "ActivityMethodStatement - getJSONObject");
        }
        return obj;
    }

    protected static ActivityMethodStatement cursorToModel(Cursor c) {
        ActivityMethodStatement activityMethodStatement = new ActivityMethodStatement();
        activityMethodStatement.activityMethodStatementID = c.getString(c.getColumnIndex("ActivityMethodStatementID"));
        activityMethodStatement.activityID = c.getString(c.getColumnIndex("ActivityID"));
        activityMethodStatement.methodStatementID = c.getString(c.getColumnIndex("MethodStatementID"));
        activityMethodStatement.isActive = c.getInt(c.getColumnIndex("IsActive")) > 0;

        return activityMethodStatement;
    }

    public static ActivityMethodStatement get(String activityMethodStatementID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ActivityMethodStatement model = null;
        Cursor c = db.rawQuery("SELECT * FROM ActivityMethodStatements WHERE ActivityMethodStatementID = ?", new String[] { activityMethodStatementID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static ActivityMethodStatement getAllForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        ActivityMethodStatement model = null;
        Cursor c = db.rawQuery("SELECT * FROM ActivityMethodStatements WHERE ActivityID = ?", new String[] { activityID });

        if (c.moveToFirst()) {
            model = cursorToModel(c);
        }

        c.close();
        return model;
    }

    public static void save(ActivityMethodStatement model) {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("ActivityMethodStatementID", model.activityMethodStatementID);
            cv.put("ActivityID", model.activityID);
            cv.put("MethodStatementID", model.methodStatementID);
            cv.put("IsActive", model.isActive);

            if (get(model.activityMethodStatementID) != null) {
                db.update("ActivityMethodStatements", cv, "ActivityMethodStatementID = ?", new String[]{model.activityMethodStatementID});
            } else {
                db.insertOrThrow("ActivityMethodStatements", null, cv);
            }
        }
        catch (Exception e) {
            ErrorLog.CreateError(e, "ActivityMethodStatement - save");
        }
    }

    public static void deleteMethodStatementsForActivity(String activityID) {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        db.execSQL("DELETE FROM ActivityMethodStatements WHERE ActivityID = '" + activityID + "'");
    }

}
