package trianglesoftware.fmconway.Database.DatabaseObjects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.Database;

public class OutOfHoursChecklist {
    public String oohChecklistID;
    public String outOfHoursID;
    public String checklistID;
    public String name;

    public OutOfHoursChecklist(){
    }

    public String getOOHChecklistID() { return this.oohChecklistID; }
    private String getOutOfHoursID() { return this.outOfHoursID; }
    private String getChecklistID() { return this.checklistID; }
    public String getName() { return this.name; }

    private void setOOHChecklistID(String oohChecklistID) { this.oohChecklistID = oohChecklistID; }
    public void setOutOfHoursID(String outOfHoursID) { this.outOfHoursID = outOfHoursID; }
    public void setChecklistID(String checklistID) { this.checklistID = checklistID; }
    public void setName(String name) { this.name = name; }

    private JSONObject getJSONObject() throws JSONException {

        JSONObject obj = new JSONObject();

        obj.put("OOHChecklistID", this.oohChecklistID);
        obj.put("OutOfHoursID", this.outOfHoursID);
        obj.put("ChecklistID", this.checklistID);
        obj.put("Name", this.name);

        return obj;
    }

    //OOH Checklists
    public static String addOOHChecklist(OutOfHoursChecklist oohChecklist)
    {
        try {
            SQLiteDatabase db = Database.MainDB.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("OOHChecklistID", oohChecklist.oohChecklistID);
            values.put("OutOfHoursID", oohChecklist.outOfHoursID);
            values.put("ChecklistID", oohChecklist.checklistID);

            db.insertOrThrow("OutOfHoursChecklists", null, values);

            return oohChecklist.oohChecklistID;
        } catch (Exception e) {
            ErrorLog.CreateError(e, "addOOHChecklist");
            Log.e("AddOOHChecklist", e.toString());
            return null;
        }
    }

    public static void deleteAllOOHChecklists()
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM OutOfHoursChecklists");
    }

    public static void deleteOOHChecklist(String oohChecklistID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM OutOfHoursChecklists WHERE OOHChecklistID = '" + oohChecklistID + "'");
    }

    public static void deleteOOHChecklistForOutOfHours(String outOfHoursID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM OutOfHoursChecklists WHERE OutOfHoursID = '" + outOfHoursID + "'");
    }

    public static void deleteOOHChecklistsForUser(String userID)
    {
        SQLiteDatabase db = Database.MainDB.getWritableDatabase();

        db.execSQL("DELETE FROM OutOfHoursChecklists WHERE UserID = '" + userID + "'");
    }

    public static boolean CheckIfChecklistCreated(String outOfHoursID, String checklistID) {
        String query = "SELECT OOHChecklistID FROM OutOfHoursChecklists where OutOfHoursID = '"+outOfHoursID+ "' AND ChecklistID = '" +checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String oohChecklistID = "";

        if(cursor.moveToFirst()){
            do{
                oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return !Objects.equals(oohChecklistID, "");
    }


    public static boolean CheckIfChecklistCreated(String outOfHoursID) {
        String query = "SELECT OOHChecklistID FROM OutOfHoursChecklists where OutOfHoursID = '"+outOfHoursID+ "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String oohChecklistID = "";

        if(cursor.moveToFirst()){
            do{
                oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return !Objects.equals(oohChecklistID, "");
    }

    public static OutOfHoursChecklist GetOOHChecklist(String outOfHoursID, String checklistID) {
        String query = "SELECT * FROM OutOfHoursChecklists where OutOfHoursID = '"+ outOfHoursID+ "' AND ChecklistID = '" +checklistID + "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        OutOfHoursChecklist oohChecklist = null;

        if(cursor.moveToFirst()){
            do{
                oohChecklist = new OutOfHoursChecklist();
                oohChecklist.oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));
                oohChecklist.outOfHoursID = cursor.getString(cursor.getColumnIndex("OutOfHoursID"));
                oohChecklist.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return oohChecklist;
    }

    public static OutOfHoursChecklist GetOOHChecklist(String outOfHoursID) {
        String query = "SELECT * FROM OutOfHoursChecklists where OutOfHoursID = '"+ outOfHoursID+ "'";

        SQLiteDatabase db = Database.MainDB.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        OutOfHoursChecklist oohChecklist = null;

        if(cursor.moveToFirst()){
            do{
                oohChecklist = new OutOfHoursChecklist();
                oohChecklist.oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));
                oohChecklist.outOfHoursID = cursor.getString(cursor.getColumnIndex("OutOfHoursID"));
                oohChecklist.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
            }while(cursor.moveToNext());
        }

        cursor.close();

        return oohChecklist;
    }

    public static JSONArray GetOOHChecklistsToSend() throws Exception{
        JSONArray oohChecklists = new JSONArray();

        String query = "SELECT tc.* FROM OutOfHoursChecklists tc ";
        query += "JOIN OutOfHours t on t.OutOfHoursID = tc.OutOfHoursID ";

        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor cursorAnswers = null;

        try {
            db = Database.MainDB.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            JSONObject tcObject;
            if (cursor.moveToFirst()) {
                do {
                    OutOfHoursChecklist tc = new OutOfHoursChecklist();
                    tc.outOfHoursID = cursor.getString(cursor.getColumnIndex("OutOfHoursID"));
                    tc.checklistID = cursor.getString(cursor.getColumnIndex("ChecklistID"));
                    tc.oohChecklistID = cursor.getString(cursor.getColumnIndex("OOHChecklistID"));

                    tcObject = tc.getJSONObject();

                    //Add Answers List
                    query = "SELECT tca.* FROM OutOfHoursChecklistAnswers tca ";
                    query += "WHERE tca.OOHChecklistID = '" + cursor.getString(cursor.getColumnIndex("OOHChecklistID")) + "'";

                    cursorAnswers = db.rawQuery(query, null);
                    JSONArray tcaObjects = new JSONArray();
                    if (cursorAnswers.moveToFirst()) {
                        do {
                            OutOfHoursChecklistAnswer tca = new OutOfHoursChecklistAnswer();
                            tca.oohChecklistAnswerID = cursorAnswers.getString(cursorAnswers.getColumnIndex("OOHChecklistAnswerID"));
                            tca.oohChecklistID = cursorAnswers.getString(cursorAnswers.getColumnIndex("OOHChecklistID"));
                            tca.checklistQuestionID = cursorAnswers.getString(cursorAnswers.getColumnIndex("ChecklistQuestionID"));
                            tca.reason = cursorAnswers.getString(cursorAnswers.getColumnIndex("Reason"));

                            int answerIndex = cursorAnswers.getColumnIndex("Answer");
                            if (!cursorAnswers.isNull(answerIndex)) {
                                tca.answer = Integer.parseInt(cursorAnswers.getString(answerIndex));
                            }

                            int timeIndex = cursorAnswers.getColumnIndex("Time");
                            if (!cursorAnswers.isNull(timeIndex)) {
                                String timeString = cursorAnswers.getString(timeIndex);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                tca.time = format.parse(timeString);
                            } else {
                                tca.time = new Date();
                            }

                            tcaObjects.put(tca.getJSONObject());

                        } while (cursorAnswers.moveToNext());

                        try {
                            tcObject.put("Answers", tcaObjects);
                        } catch (JSONException e) {
                            throw e;
                        }
                    }
                    //cursorAnswers.close();

                    oohChecklists.put(tcObject);
                } while (cursor.moveToNext());
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetOOHChecklistsToSend");
            throw e;
        }
        finally {
            cursor.close();
            if (cursorAnswers != null) {
                cursorAnswers.close();
            }
        }

        return oohChecklists;
    }
}
