package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.R;


class CleansingLogsAdapter extends BaseAdapter {
    public List<CleansingLog> items;
    private Context context;
    private LayoutInflater inflater;

    public CleansingLogsAdapter(Context context, LayoutInflater inflater) {
        this.items = new LinkedList<>();
        this.context = context;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public CleansingLog getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            view = inflater.inflate(R.layout.row_cleansing_log, null);

            holder = new ViewHolder();
            holder.numberTextView = view.findViewById(R.id.number);
            holder.roadTextView = view.findViewById(R.id.road);
            holder.sectionTextView = view.findViewById(R.id.section);
            holder.removeButton = view.findViewById(R.id.removeButton);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        CleansingLog cleansingLog = items.get(position);

        holder.numberTextView.setText(String.valueOf(position + 1));

        if (cleansingLog.road != null) {
            holder.roadTextView.setText(cleansingLog.road);
        }

        if (cleansingLog.section != null) {
            holder.sectionTextView.setText(cleansingLog.section);
        }

        return view;
    }

    public static class ViewHolder {
        public TextView numberTextView;
        public TextView roadTextView;
        public TextView sectionTextView;
        public Button removeButton;
    }
}
