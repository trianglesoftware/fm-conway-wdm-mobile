package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 11/03/2016.
 */
class TaskChecklistQuestionAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;
    public boolean isRiskAssessmentChecklist;
    public boolean isBatteryChecklist;
    public boolean isYesNoNaChecklist;

    public TaskChecklistQuestionAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (isBatteryChecklist) {
            // Battery Checklist
            BatteryCheckViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_riskassessment_checklist_question, null);
                convertView.findViewById(R.id.low).setVisibility(View.GONE);

                holder = new BatteryCheckViewHolder();
                holder.question = (TextView) convertView.findViewById(R.id.question);
                holder.yes = (Button) convertView.findViewById(R.id.medium);
                holder.yes.setTag(1);
                holder.yes.setText("Yes");
                holder.no = (Button) convertView.findViewById(R.id.high);
                holder.no.setText("No");
                holder.no.setTag(0);

                convertView.setTag(holder);
            } else {
                holder = (BatteryCheckViewHolder) convertView.getTag();
            }

            JSONObject jsonObject = getItem(position);

            String checklistQuestion = jsonObject.optString("Question", "");
            String checklistQuestionID = jsonObject.optString("ChecklistQuestionID", "");
            int answer = jsonObject.optInt("Answer", 0);
            boolean isNew = jsonObject.optBoolean("IsNew", false);

            holder.question.setTag(checklistQuestionID);
            holder.question.setText(checklistQuestion);

            if (isNew) {
                holder.no.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.yes.setBackgroundColor(Color.parseColor("#d6d7d7"));
            } else {
                if (answer == 0) {
                    holder.yes.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.no.setBackgroundColor(Color.parseColor("#FF0103"));
                } else if (answer == 1) {
                    holder.no.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.yes.setBackgroundColor(Color.parseColor("#41ab45"));
                }
            }
            return convertView;
        } else if (isRiskAssessmentChecklist) {
            // Risk Assessment Checklist
            RiskAssessmentViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_riskassessment_checklist_question, null);

                holder = new RiskAssessmentViewHolder();
                holder.question = (TextView) convertView.findViewById(R.id.question);
                holder.low = (Button) convertView.findViewById(R.id.low);
                holder.low.setTag(2);
                holder.medium = (Button) convertView.findViewById(R.id.medium);
                holder.medium.setTag(1);
                holder.high = (Button) convertView.findViewById(R.id.high);
                holder.high.setTag(0);

                convertView.setTag(holder);
            } else {
                holder = (RiskAssessmentViewHolder) convertView.getTag();
            }

            JSONObject jsonObject = getItem(position);

            String checklistQuestion = jsonObject.optString("Question", "");
            String checklistQuestionID = jsonObject.optString("ChecklistQuestionID", "");
            int answer = jsonObject.optInt("Answer", 0);
            boolean isNew = jsonObject.optBoolean("IsNew", false);

            holder.question.setTag(checklistQuestionID);
            holder.question.setText(checklistQuestion);

            if (isNew) {
                holder.low.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.medium.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.high.setBackgroundColor(Color.parseColor("#d6d7d7"));
            } else {
                if (answer == 0) {
                    holder.low.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.medium.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.high.setBackgroundColor(Color.parseColor("#41ab45"));
                } else if (answer == 1) {
                    holder.low.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.medium.setBackgroundColor(Color.parseColor("#41ab45"));
                    holder.high.setBackgroundColor(Color.parseColor("#d6d7d7"));
                } else if (answer == 2) {
                    holder.low.setBackgroundColor(Color.parseColor("#41ab45"));
                    holder.medium.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.high.setBackgroundColor(Color.parseColor("#d6d7d7"));
                }
            }

            return convertView;
        } else if (isYesNoNaChecklist) {
            // YesNoNa Extended Checklist
            ExtendedViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_checklist_question_extended, null);

                holder = new ExtendedViewHolder();
                holder.question = (TextView) convertView.findViewById(R.id.question);
                holder.yes = (Button) convertView.findViewById(R.id.answer_yes);
                holder.yes.setText("Yes");
                holder.yes.setTag(1);
                holder.no = (Button) convertView.findViewById(R.id.answer_no);
                holder.no.setText("No");
                holder.no.setTag(0);
                holder.na = (Button) convertView.findViewById(R.id.answer_na);
                holder.na.setText("NA");
                holder.na.setTag(2);

                convertView.setTag(holder);
            } else {
                holder = (ExtendedViewHolder) convertView.getTag();
            }

            JSONObject jsonObject = getItem(position);

            String checklistQuestion = jsonObject.optString("Question", "");
            String checklistQuestionID = jsonObject.optString("ChecklistQuestionID", "");
            int answer = jsonObject.optInt("Answer", 0);
            boolean isNew = jsonObject.optBoolean("IsNew", false);

            holder.question.setTag(checklistQuestionID);
            holder.question.setText(checklistQuestion);

            if (isNew) {
                holder.yes.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.no.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.na.setBackgroundColor(Color.parseColor("#d6d7d7"));
            } else {
                if (answer == 0) {
                    holder.yes.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.no.setBackgroundColor(Color.parseColor("#FF0103"));
                    holder.na.setBackgroundColor(Color.parseColor("#d6d7d7"));
                } else if (answer == 1) {
                    holder.yes.setBackgroundColor(Color.parseColor("#41ab45"));
                    holder.no.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.na.setBackgroundColor(Color.parseColor("#d6d7d7"));
                } else if (answer == 2) {
                    holder.yes.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.no.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.na.setBackgroundColor(Color.parseColor("#8f8f8f"));
                }
            }

            return convertView;
        } else {
            // Standard Checklist
            StandardViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_checklist_question, null);

                holder = new StandardViewHolder();
                holder.middleRowTextView = (TextView) convertView.findViewById(R.id.text_equipment_type);
                holder.answerYes = (Button) convertView.findViewById(R.id.answer_yes);
                holder.answerNo = (Button) convertView.findViewById(R.id.answer_no);

                convertView.setTag(holder);
            } else {
                holder = (StandardViewHolder) convertView.getTag();
            }

            JSONObject jsonObject = getItem(position);

            String checklistQuestion = jsonObject.optString("Question", "");
            String checklistQuestionID = jsonObject.optString("ChecklistQuestionID", "");
            int answer = jsonObject.optInt("Answer", 0);
            boolean isNew = jsonObject.optBoolean("IsNew", false);

            holder.middleRowTextView.setTag(checklistQuestionID);
            holder.middleRowTextView.setText(checklistQuestion);

            if (isNew) {
                holder.answerNo.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.answerYes.setBackgroundColor(Color.parseColor("#d6d7d7"));
            } else {
                if (answer == 1) {
                    holder.answerYes.setBackgroundColor(Color.parseColor("#41ab45"));
                    holder.answerNo.setBackgroundColor(Color.parseColor("#d6d7d7"));
                } else {
                    holder.answerYes.setBackgroundColor(Color.parseColor("#d6d7d7"));
                    holder.answerNo.setBackgroundColor(Color.parseColor("#FF6961"));
                }
            }

            return convertView;
        }
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class StandardViewHolder {
        public TextView middleRowTextView;
        public Button answerYes;
        public Button answerNo;
    }

    public static class RiskAssessmentViewHolder {
        public TextView question;
        public Button low;
        public Button medium;
        public Button high;
        public Button no;
    }

    public static class BatteryCheckViewHolder {
        public TextView question;
        public Button yes;
        public Button no;
    }

    public static class ExtendedViewHolder {
        public TextView question;
        public Button yes;
        public Button no;
        public Button na;
    }
}
