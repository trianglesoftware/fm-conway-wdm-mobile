package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipmentAsset;
import trianglesoftware.fmconway.R;

public class TaskEquipmentAssetAdapter extends BaseAdapter {
    public List<TaskEquipmentAsset> Entities;
    public int QuantityRequired;
    
    private final LayoutInflater inflater;

    public TaskEquipmentAssetAdapter(Context context, LayoutInflater inflater) {
        this.inflater = inflater;
        Entities = new LinkedList<TaskEquipmentAsset>();
    }

    @Override
    public int getCount() {
        return Entities.size();
    }

    @Override
    public TaskEquipmentAsset getItem(int position) {
        return Entities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TaskEquipmentAsset e = getItem(position);
        
        ViewHolder viewHolder;
        if (convertView == null) { 
            convertView = inflater.inflate(R.layout.row_task_equipment_asset, null);

            viewHolder = new ViewHolder();
            viewHolder.ItemNumber = (TextView) convertView.findViewById(R.id.text_item_number);
            viewHolder.AssetNumber = (EditText) convertView.findViewById(R.id.text_asset_number);
            viewHolder.DeleteRow = (ImageView) convertView.findViewById(R.id.button_delete_row);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.Entity = e;
        viewHolder.ItemNumber.setText(String.valueOf(e.ItemNumber));
        viewHolder.AssetNumber.setText(e.AssetNumber);
        
        if (Entities.size() > QuantityRequired) {
            viewHolder.DeleteRow.setVisibility(View.VISIBLE);
        } else {
            viewHolder.DeleteRow.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    public void UpdateData(List<TaskEquipmentAsset> entities) {
        this.Entities = entities;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        public TaskEquipmentAsset Entity;
        public TextView ItemNumber;
        public EditText AssetNumber;
        public ImageView DeleteRow;
    }
}
