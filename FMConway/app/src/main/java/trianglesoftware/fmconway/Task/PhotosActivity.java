package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.util.TextUtils;
import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskSignature;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Utilities.Signature;

public class PhotosActivity extends FMConwayActivity {
    //intents
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private int taskCount;
    private boolean showDetails;
    private boolean photosRequired;
    private boolean showClientSignature;
    private boolean clientSignatureRequired;
    private String tag;

    //other
    private String url;
    private Task task;
    private TextView scopeDetails;
    private TextView workInstructionComments;
    private Button clientSignatureButton;
    private Button taskCompleteButton;
    private Button maintenanceButton;
    private Button addPhotoButton;
    private SharedPreferences sp;
    private boolean photosOnlyManditoryIfNoPhotosTaken;
    private boolean showNotes;
    private boolean notesRequired;

    private String filepath;
    private String retakeTaskPhotoID;
    private final int REQUEST_CODE = 10;

    private GridView gridView;
    private ArrayList<TaskPhoto> photoList;
    private TaskImageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            jobPackID = extras.getString("JobPackID", "");
            taskID = extras.getString("TaskID", "");
            taskCount = extras.getInt("TaskCount");
            showDetails = extras.getBoolean("ShowDetails");
            photosRequired = extras.getBoolean("PhotosRequired");
            showClientSignature = extras.getBoolean("ShowClientSignature");
            clientSignatureRequired = extras.getBoolean("ClientSignatureRequired");
            tag = extras.getString("Tag", "");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        url = this.getResources().getString(R.string.QUERY_URL);

        task = Task.GetTask(taskID);

        workInstructionComments = (TextView) findViewById(R.id.workinstruction_comments);
        clientSignatureButton = (Button) findViewById(R.id.client_signature_button);

        // show details
        if (showDetails) {
            Activity activity = Activity.GetActivity(task.activityID);
            workInstructionComments.setText(activity.getWorksInstructionComments());
            if (isNullOrWhiteSpace(activity.getWorksInstructionComments())) {
                ScrollView commentsScroller = (ScrollView) findViewById(R.id.workinstruction_comments_scroller);
                commentsScroller.setVisibility(View.GONE);
            }
        } else {
            LinearLayout taskDetailsContainer = (LinearLayout) findViewById(R.id.task_details_container);
            taskDetailsContainer.setVisibility(View.GONE);
        }

        // show client signature
        if (!showClientSignature) {
            clientSignatureButton.setVisibility(View.GONE);
        }

        gridView = (GridView) findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = TaskPhoto.getPhotosForTask(taskID);

        adapter = new TaskImageAdapter(this, photoList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoEditDialog(view);
            }
        });

        gridView.setAdapter(adapter);

        scopeDetails = (TextView) findViewById(R.id.scope_description);
        scopeDetails.setText(trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetScopeOfWork(task.activityID, shiftID));

        taskCompleteButton = (Button) findViewById(R.id.task_complete_button);

        taskCompleteButton.setText(task.name);

        maintenanceButton = (Button) findViewById(R.id.maintenance_button);

        addPhotoButton = (Button) findViewById(R.id.add_photo);

        if (!TextUtils.isEmpty(tag)) {
            String[] split = tag.split(Pattern.quote("|"));
            for (int i = 0; i < split.length; i++) {
                if (Objects.equals(split[i], "PhotosOnlyManditoryIfNoPhotosTaken")) {
                    photosOnlyManditoryIfNoPhotosTaken = true;
                }
                if (Objects.equals(split[i], "ShowNotes")) {
                    showNotes = true;
                }
                if (Objects.equals(split[i], "NotesRequired")) {
                    notesRequired = true;
                }
            }
        }

        if (showNotes) {
            TextView notes = (TextView) findViewById(R.id.notes);
            notes.setText(Task.GetTaskDetails(taskID));
        } else {
            LinearLayout notesContainer = (LinearLayout) findViewById(R.id.notes_container);
            notesContainer.setVisibility(View.GONE);
        }

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_photos;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Task";
    }

    private void photoEditDialog(final View imageView) {
        View photoDialogView = getLayoutInflater().inflate(R.layout.photo_dialog, null);
        final int position = gridView.getPositionForView(imageView);
        final TaskPhoto taskPhoto = photoList.get(position);

        final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog))
                .setView(photoDialogView)
                .setCancelable(true)
                .create();

        photoDialogView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FMConwayUtils.confirmDialog(PhotosActivity.this, "Delete photo and comment, are you sure?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TaskPhoto.deleteTaskPhotoFile(imageView.getTag().toString());
                        TaskPhoto.deleteTaskPhoto(imageView.getTag().toString());
                        photoList.remove(position);
                        adapter.update(photoList);
                        dialog.dismiss();
                    }
                });
            }
        });

        photoDialogView.findViewById(R.id.view_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", new File(taskPhoto.imageLocation));

                String extension = MimeTypeMap.getFileExtensionFromUrl(taskPhoto.imageLocation);
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileIntent.setDataAndType(uri, type);

                startActivity(fileIntent);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.retake_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = getOutputMediaFile();
                Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                filepath = file.getPath();
                retakeTaskPhotoID = taskPhoto.taskPhotoID;

                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                // start the image capture Intent
                startActivityForResult(intent, REQUEST_CODE);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.edit_comments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectActivity = new Intent(PhotosActivity.this, PhotoCommentActivity.class);
                selectActivity.putExtra("TaskPhotoID", taskPhoto.taskPhotoID);
                startActivity(selectActivity);
                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static boolean isNullOrWhiteSpace(String string) {
        return string == null || string.trim().isEmpty();
    }

    private void MaintenanceDue() {
        if (!MaintenanceCheck.CheckLatestTime(taskID, false)) {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        } else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MaintenanceDue();

        //if (photosRequired && !TaskPhoto.CheckIfPhotosExist(task.taskID)) {
        //    addPhotoButton.setBackgroundColor(Color.parseColor("#41ab45"));
        //    taskCompleteButton.setEnabled(false);
        //    taskCompleteButton.setBackgroundColor(Color.parseColor("#808080"));
        //} else {
        //    addPhotoButton.setBackgroundColor(Color.parseColor("#00215D"));
        //    taskCompleteButton.setEnabled(true);
        //    taskCompleteButton.setBackgroundColor(Color.parseColor("#41ab45"));
        //}
    }

    public void taskCompleted(View view) {
        // check client signature if required
        if (clientSignatureRequired && !TaskSignature.checkClientSignatureExists(taskID)) {
            Toast.makeText(getApplicationContext(), "Clients signature is required", Toast.LENGTH_SHORT).show();
            return;
        }

        // check custom photos required condition
        if (photosOnlyManditoryIfNoPhotosTaken) {
            if (!TaskPhoto.checkIfPhotosExistForActivity(task.activityID)) {
                Toast.makeText(getApplicationContext(), "At least one photo is required", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            if (photosRequired && !TaskPhoto.checkIfPhotosExist(task.taskID)) {
                Toast.makeText(getApplicationContext(), "At least one photo is required", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (showNotes && notesRequired) {
            TextView notes = findViewById(R.id.notes);
            if (FMConwayUtils.isNullOrWhitespace(notes.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Notes is a required field", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Continue");
        alertDialog.setMessage("Please confirm you have completed this task.");
        alertDialog.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (showNotes) {
                    TextView notes = findViewById(R.id.notes);
                    Task.StoreTaskDetails(task.taskID, notes.getText().toString());
                }

                Task.EndTask(task.taskID);

                /*if (Objects.equals(task.name, "Arrive on Site")) {
                    ShiftProgress.SetShiftProgress(shiftID, "TasksStarted");
                    ShiftProgress.SyncProgress();
                }*/

                /*if (Objects.equals(task.name, "Leave Site")) {
                    ShiftProgress.SetShiftProgress(shiftID, "Completed");
                    ShiftProgress.SyncProgress();
                }*/

                finish();

                /*if (checkIfFromClosureInstalled()) {
                    if (task.taskOrder < taskCount) {
                        //Load Next Activity
                        task = Task.GetTaskFromActivity(task.activityID);

                        TaskSelection(task, taskCount, jobPackID);
                    }

                    Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
                    selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
                    selectMaintenanceActivity.putExtra("ShiftID", shiftID);
                    selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(selectMaintenanceActivity);
                    finish();
                } else {
                    if (task.taskOrder < taskCount) {
                        //Load Next Activity
                        task = Task.GetTaskFromActivity(task.activityID);

                        TaskSelection(task, taskCount, jobPackID);
                    }
                }*/
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void photoClick(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();
        retakeTaskPhotoID = null;

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    public void clientClick(View view) {
        if (!TaskSignature.checkClientSignatureExists(taskID)) {
            Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
            signatureIntent.putExtra("TaskID", taskID);
            signatureIntent.putExtra("Client", "Client");
            signatureIntent.putExtra("LoggedIn", sp.getString("UserID", ""));
            startActivity(signatureIntent);
        }
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view) {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }

    public boolean checkIfFromClosureInstalled() {
        try {
            int previousTaskOrder = task.taskOrder - 1;
            String activityID = task.activityID;

            Task previousTask = Task.GetTaskFromActivityAndOrder(activityID, previousTaskOrder);
            if (previousTask != null) {
                String name = previousTask.getName();

                if (Objects.equals(name, "Closure Installed")) {
                    return true;
                }
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "CheckIfFromClosureInstalled");
        }

        return false;
    }

    private static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-" + timeStamp + ".jpg");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (retakeTaskPhotoID != null) {
                    TaskPhoto taskPhoto = savePhoto(retakeTaskPhotoID, filepath);
                } else {
                    TaskPhoto taskPhoto = savePhoto(null, filepath);

                    Intent selectActivity = new Intent(getApplicationContext(), PhotoCommentActivity.class);
                    selectActivity.putExtra("TaskPhotoID", taskPhoto.taskPhotoID);
                    startActivity(selectActivity);
                }

                photoList = TaskPhoto.getPhotosForTask(taskID);
                adapter.update(photoList);
            }
        }
    }

    private TaskPhoto savePhoto(String taskPhotoID, String imageLocation) {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude = 0;
        double latitude = 0;

        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        TaskPhoto taskPhoto = null;

        if (taskPhotoID != null) {
            taskPhoto = TaskPhoto.getTaskPhoto(taskPhotoID);
        } else {
            taskPhoto = new TaskPhoto();
            taskPhoto.taskPhotoID = UUID.randomUUID().toString();
        }

        taskPhoto.taskID = taskID;
        taskPhoto.longitude = longitude;
        taskPhoto.latitude = latitude;
        taskPhoto.imageLocation = imageLocation;
        taskPhoto.userID = sp.getString("UserID", "");
        taskPhoto.time = new Date();
        taskPhoto.shiftID = shiftID;

        TaskPhoto.saveTaskPhoto(taskPhoto);

        return taskPhoto;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation changes
        outState.putString("filepath", filepath);
        outState.putString("retakeTaskPhotoID", retakeTaskPhotoID);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
        retakeTaskPhotoID = savedInstanceState.getString("retakeTaskPhotoID");
    }
}
