package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcess;
import trianglesoftware.fmconway.R;


class UnloadingProcessesAdapter extends BaseAdapter {
    public List<UnloadingProcess> items;
    private Context context;
    private LayoutInflater inflater;

    public UnloadingProcessesAdapter(Context context, LayoutInflater inflater) {
        this.items = new LinkedList<>();
        this.context = context;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public UnloadingProcess getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            view = inflater.inflate(R.layout.row_unloading_process_log, null);

            holder = new ViewHolder();
            holder.numberTextView = view.findViewById(R.id.number);
            holder.wasteTickerNumberTextView = view.findViewById(R.id.waste_ticket_number);
            holder.statusTextView = view.findViewById(R.id.status);
            holder.removeButton = view.findViewById(R.id.removeButton);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        UnloadingProcess unloadingProcess = items.get(position);

        holder.numberTextView.setText(String.valueOf(position + 1));
        holder.wasteTickerNumberTextView.setText(unloadingProcess.wasteTicketNumber != null ? unloadingProcess.wasteTicketNumber : "");
        holder.statusTextView.setText(unloadingProcess.leaveDisposalSiteDate == null ? "Incomplete" : "Complete");

        return view;
    }

    public static class ViewHolder {
        public TextView numberTextView;
        public TextView wasteTickerNumberTextView;
        public TextView statusTextView;
        public Button removeButton;
    }
}
