package trianglesoftware.fmconway.Task;

import static trianglesoftware.fmconway.Utilities.FMConwayUtils.getOutputMediaFile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewCompat;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcess;
import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcessPhoto;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class UnloadingProcessActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private String userID;
    private String unloadingProcessID;
    private int stage;

    private Task task;
    private Activity activity;
    private String jobType;
    private UnloadingProcess unloadingProcess;

    private String filepath;
    private String retakeUnloadingProcessPhotoID;

    private SharedPreferences sp;

    // inputs
    private LinearLayout unloadingProcessContainer;
    private LinearLayout departForDisposalContainer;
    private Button departForDisposalButton;
    private LinearLayout arrivedAtDisposalContainer;
    private Button arrivedAtDisposalButton;

    private EditText disposalSiteEditText;
    private EditText wasteTicketNumberEditText;
    private EditText volumeOfWasteDisposedEditText;
    private EditText commentsEditText;
    private GridView gridView;
    private ArrayList<UnloadingProcessPhoto> photosList;
    private UnloadingProcessPhotoAdapter photosAdapter;

    // request codes
    private int disposalSiteActivityRequestCode = 1;
    private int photoRequestCode = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID", "");
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        userID = extras.getString("UserID", "");
        unloadingProcessID = extras.getString("UnloadingProcessID", null);

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        task = Task.GetTask(taskID);
        activity = Activity.GetActivity(task.activityID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        if (unloadingProcessID != null) {
            unloadingProcess = UnloadingProcess.get(unloadingProcessID);
        }

        // inputs
        unloadingProcessContainer = findViewById(R.id.unloading_process_container);
        departForDisposalContainer = findViewById(R.id.depart_for_disposal_container);
        departForDisposalButton = findViewById(R.id.depart_for_disposal_button);
        arrivedAtDisposalContainer = findViewById(R.id.arrived_at_disposal_container);
        arrivedAtDisposalButton = findViewById(R.id.arrived_at_disposal_button);

        disposalSiteEditText = findViewById(R.id.disposal_site);
        wasteTicketNumberEditText = findViewById(R.id.waste_ticket_number);
        volumeOfWasteDisposedEditText = findViewById(R.id.volume_of_waste_disposed);
        commentsEditText = findViewById(R.id.comments);

        gridView = (GridView)findViewById(R.id.photo_gridView);
        ViewCompat.setNestedScrollingEnabled(gridView,true);
        photosList = new ArrayList<>();

        if (unloadingProcess != null) {
            photosList = UnloadingProcessPhoto.getPhotosForUnloadingProcess(unloadingProcess.unloadingProcessID);
        }

        photosAdapter = new UnloadingProcessPhotoAdapter(this, photosList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoEditDialog(view);
            }
        });

        gridView.setAdapter(photosAdapter);

        // set values
        if (unloadingProcess != null) {
            // trim trailing 0's
            DecimalFormat decimalFormat = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            decimalFormat.setMaximumFractionDigits(340); //340 = DecimalFormat.DOUBLE_FRACTION_DIGITS

            if (unloadingProcess.disposalSiteID != null) {
                disposalSiteEditText.setTag(unloadingProcess.disposalSiteID);
                disposalSiteEditText.setText(unloadingProcess.disposalSiteName);
            }

            if (unloadingProcess.wasteTicketNumber != null) {
                wasteTicketNumberEditText.setText(unloadingProcess.wasteTicketNumber);
            }

            if (unloadingProcess.volumeOfWasteDisposed != null) {
                volumeOfWasteDisposedEditText.setText(decimalFormat.format(unloadingProcess.volumeOfWasteDisposed));
            }

            if (unloadingProcess.comments != null) {
                commentsEditText.setText(unloadingProcess.comments);
            }
        }

        // events
        disposalSiteEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent disposalSiteActivity = new Intent(getApplicationContext(), DisposalSiteActivity.class);
                disposalSiteActivity.putExtra("ShiftID", shiftID);
                disposalSiteActivity.putExtra("DisposalSiteID", disposalSiteEditText.getTag() != null ? disposalSiteEditText.getTag().toString() : null);
                startActivityForResult(disposalSiteActivity, disposalSiteActivityRequestCode);
            }
        });

        // set layout
        unloadingProcessContainer.setVisibility(View.GONE);
        departForDisposalContainer.setVisibility(View.GONE);
        arrivedAtDisposalContainer.setVisibility(View.GONE);

        refreshStageLayout();
    }

    @Override
    public void onResume() {
        super.onResume();

        // for when activity is destroyed & recreated (usually when camera rotates)
        if (unloadingProcess == null && unloadingProcessID != null) {
            unloadingProcess = UnloadingProcess.get(unloadingProcessID);
            refreshStageLayout();

            photosList = UnloadingProcessPhoto.getPhotosForUnloadingProcess(unloadingProcessID);
            photosAdapter.update(photosList);

            getWindow().getDecorView().findViewById(R.id.top_layout).clearFocus();
        }
    }

    private void refreshStageLayout() {
        if (unloadingProcess == null || unloadingProcess.departToDisposalSiteDate == null) {
            stage = 0;
            departForDisposalContainer.setVisibility(View.VISIBLE);
            arrivedAtDisposalContainer.setVisibility(View.GONE);
            unloadingProcessContainer.setVisibility(View.GONE);
        } else if (unloadingProcess.arriveAtDisposalSiteDate == null) {
            stage = 1;
            departForDisposalContainer.setVisibility(View.GONE);
            arrivedAtDisposalContainer.setVisibility(View.VISIBLE);
            unloadingProcessContainer.setVisibility(View.GONE);
        } else {
            stage = 2;
            departForDisposalContainer.setVisibility(View.GONE);
            arrivedAtDisposalContainer.setVisibility(View.GONE);
            unloadingProcessContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_unloading_process_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Unloading Process";
    }

    public void photoClick(View view) {
        if (unloadingProcess == null) {
            Toast.makeText(getApplicationContext(), "Unloading process must first be saved before photos can be added.", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();
        retakeUnloadingProcessPhotoID = null;

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, photoRequestCode);
    }

    private void photoEditDialog(final View imageView) {
        View photoDialogView = getLayoutInflater().inflate(R.layout.photo_dialog, null);
        final int position = gridView.getPositionForView(imageView);
        final UnloadingProcessPhoto unloadingProcessPhoto = photosList.get(position);

        final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog))
                .setView(photoDialogView)
                .setCancelable(true)
                .create();

        photoDialogView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FMConwayUtils.confirmDialog(UnloadingProcessActivity.this, "Delete photo and comment, are you sure?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UnloadingProcessPhoto.deleteUnloadingProcessPhotoFile(imageView.getTag().toString());
                        UnloadingProcessPhoto.deleteUnloadingProcessPhoto(imageView.getTag().toString());
                        photosList.remove(position);
                        photosAdapter.update(photosList);
                        dialog.dismiss();
                    }
                });
            }
        });

        photoDialogView.findViewById(R.id.view_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", new File(unloadingProcessPhoto.imageLocation));

                String extension = MimeTypeMap.getFileExtensionFromUrl(unloadingProcessPhoto.imageLocation);
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileIntent.setDataAndType(uri, type);

                startActivity(fileIntent);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.retake_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = getOutputMediaFile();
                Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                filepath = file.getPath();
                retakeUnloadingProcessPhotoID = unloadingProcessPhoto.unloadingProcessPhotoID;

                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                // start the image capture Intent
                startActivityForResult(intent, photoRequestCode);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.edit_comments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectActivity = new Intent(UnloadingProcessActivity.this, PhotoCommentActivity.class);
                selectActivity.putExtra("UnloadingProcessPhotoID", unloadingProcessPhoto.unloadingProcessPhotoID);
                startActivity(selectActivity);
                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void departForDisposalClicked(View view) {
        String buttonText = departForDisposalButton.getText().toString();

        FMConwayUtils.confirmDialog(this, buttonText + "?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                save();
            }
        });
    }

    public void arrivedAtDisposalClicked(View view) {
        String buttonText = arrivedAtDisposalButton.getText().toString();

        FMConwayUtils.confirmDialog(this, buttonText + "?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                save();
            }
        });
    }

    public void saveClick(View view) {
        if (!validate()) {
            return;
        }

        FMConwayUtils.confirmDialog(this, "Save this record, are you sure?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                save();
            }
        });
    }

    public boolean validate() {
        Object disposalSiteID = disposalSiteEditText.getTag();
        if (disposalSiteID == null) {
            Toast.makeText(getApplicationContext(), "Disposal Site is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        String wasteTicketNumber = wasteTicketNumberEditText.getText().toString();
        if (FMConwayUtils.isNullOrWhitespace(wasteTicketNumber)) {
            Toast.makeText(getApplicationContext(), "Waste Ticket Number is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        String volumeOfWasteDiposed = volumeOfWasteDisposedEditText.getText().toString();
        if (!FMConwayUtils.isNumber(volumeOfWasteDiposed)) {
            Toast.makeText(getApplicationContext(), "Volume of Waste Disposed is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        if (photosAdapter.getCount() == 0) {
            Toast.makeText(getApplicationContext(), "At least one photo is required", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public void save() {
        if (unloadingProcess == null) {
            unloadingProcess = new UnloadingProcess();
            unloadingProcess.unloadingProcessID = UUID.randomUUID().toString();
            unloadingProcess.taskID = taskID;
            unloadingProcess.activityID = activity.activityID;
            unloadingProcess.createdDate = new Date();

            unloadingProcessID = unloadingProcess.unloadingProcessID;
        }

        if (stage == 0) {
            unloadingProcess.departToDisposalSiteDate = new Date();
        } else if (stage == 1) {
            unloadingProcess.arriveAtDisposalSiteDate = new Date();
        } else if (stage == 2) {
            unloadingProcess.disposalSiteID = disposalSiteEditText.getTag().toString();
            unloadingProcess.wasteTicketNumber = wasteTicketNumberEditText.getText().toString();
            unloadingProcess.volumeOfWasteDisposed = Double.parseDouble(volumeOfWasteDisposedEditText.getText().toString());
            unloadingProcess.comments = commentsEditText.getText().toString();

            if (unloadingProcess.leaveDisposalSiteDate == null) {
                unloadingProcess.leaveDisposalSiteDate = new Date();
            }
        }

        UnloadingProcess.save(unloadingProcess);

        if (stage == 0 || stage == 1) {
            refreshStageLayout();
        } else if (stage == 2) {
            finish();
        }
    }

    private UnloadingProcessPhoto savePhoto(String unloadingProcessPhotoID, String imageLocation) {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude = 0;
        double latitude = 0;

        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        UnloadingProcessPhoto unloadingProcessPhoto = null;

        if (unloadingProcessPhotoID != null) {
            unloadingProcessPhoto = UnloadingProcessPhoto.getUnloadingProcessPhoto(unloadingProcessPhotoID);
        } else {
            unloadingProcessPhoto = new UnloadingProcessPhoto();
            unloadingProcessPhoto.unloadingProcessPhotoID = UUID.randomUUID().toString();
        }

        unloadingProcessPhoto.unloadingProcessID = unloadingProcessID;
        unloadingProcessPhoto.longitude = longitude;
        unloadingProcessPhoto.latitude = latitude;
        unloadingProcessPhoto.imageLocation = imageLocation;
        unloadingProcessPhoto.userID = sp.getString("UserID", "");
        unloadingProcessPhoto.time = new Date();
        unloadingProcessPhoto.shiftID = shiftID;

        UnloadingProcessPhoto.saveUnloadingProcessPhoto(unloadingProcessPhoto);

        return unloadingProcessPhoto;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == disposalSiteActivityRequestCode) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();

                String disposalSiteID = extras.getString("DisposalSiteID", null);
                String disposalSiteName = extras.getString("DisposalSiteName", "");

                disposalSiteEditText.setTag(disposalSiteID);
                disposalSiteEditText.setText(disposalSiteName);
            }
        } else if (requestCode == photoRequestCode) {
            if (resultCode == RESULT_OK) {
                if (retakeUnloadingProcessPhotoID != null) {
                    savePhoto(retakeUnloadingProcessPhotoID, filepath);
                } else {
                    UnloadingProcessPhoto unloadingProcessPhoto = savePhoto(null, filepath);

                    Intent selectActivity = new Intent(getApplicationContext(), PhotoCommentActivity.class);
                    selectActivity.putExtra("UnloadingProcessPhotoID", unloadingProcessPhoto.unloadingProcessPhotoID);
                    startActivity(selectActivity);
                }

                photosList = UnloadingProcessPhoto.getPhotosForUnloadingProcess(unloadingProcessID);
                photosAdapter.update(photosList);
            }
        }
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation changes
        outState.putString("filepath", filepath);
        outState.putString("retakeUnloadingProcessPhotoID", retakeUnloadingProcessPhotoID);
        outState.putString("unloadingProcessID", unloadingProcessID);

        Object disposalSiteObject = disposalSiteEditText.getTag();
        if (disposalSiteObject != null) {
            outState.putString("disposalSiteID", disposalSiteObject.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
        retakeUnloadingProcessPhotoID = savedInstanceState.getString("retakeUnloadingProcessPhotoID");
        unloadingProcessID = savedInstanceState.getString("unloadingProcessID");
        disposalSiteEditText.setTag(savedInstanceState.getString("disposalSiteID", null));
    }
}
