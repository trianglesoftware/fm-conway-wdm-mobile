package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.JobPack.SelectJobPackActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class FreeTextTaskActivity extends FMConwayActivity {
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private int taskCount;
    private String userID;
    private trianglesoftware.fmconway.Database.DatabaseObjects.Activity activity;
    private Task task;
    private TextView scopeDetails;
    private Button taskCompleteButton;
    private Button maintenanceButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            jobPackID = extras.getString("JobPackID","");
            taskID = extras.getString("TaskID","");
            taskCount = extras.getInt("TaskCount");
            userID = extras.getString("UserID","");
        }

        super.onCreate(savedInstanceState);
        task = Task.GetTask(taskID);
        //activity = trianglesoftware.chevron.Database.DatabaseObjects.Activity.GetActivity(task.getActivityID());

        scopeDetails = (TextView)findViewById(R.id.scope_description);
        scopeDetails.setText(trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetScopeOfWork(task.activityID, shiftID));

        taskCompleteButton = (Button)findViewById(R.id.task_complete_button);

        taskCompleteButton.setText(task.name);

        if (task.name.length() > 20)
        {
            taskCompleteButton.setTextSize(30);
        }

        maintenanceButton = (Button)findViewById(R.id.maintenance_button);
        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_free_text_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Task";
    }

    private void MaintenanceDue()
    {
        if(!MaintenanceCheck.CheckLatestTime(taskID, false))
        {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        }
        else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        MaintenanceDue();
    }

    public void taskCompleted(View view)
    {
        String message = "Please confirm you have completed this task.";
        
        if (Objects.equals(task.tag, "SiteClearedConfirmation")) { 
            message = "Have you made sure everything is clear?";
        }

        // Check that the mandatory maintenance check has been completed
        boolean requireMaintenanceCheck = false;
        /*if (task.name.equals("Leave Site")) {
            String activityID = Activity.getActivityIdByTaskID(taskID);
            String activityType = Activity.getActivityTypeByTaskID(taskID);
            requireMaintenanceCheck = !(MaintenanceCheck.checkMaintenanceCheckExists(activityID)) && !(activityType.toUpperCase().contains("BATTERY"))
                                        && !(activityType.toUpperCase().contains("DE-INSTALL"));
        }*/

        if (requireMaintenanceCheck) {

            Toast.makeText(getApplicationContext(), "A Maintenance Check is required before continuing", Toast.LENGTH_LONG).show();

        } else {

            //Confirm box first
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Continue");
            alertDialog.setMessage(message);

            alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Task.EndTask(task.taskID);
                    finish();

                    /*if (task.taskOrder < taskCount) {
                        //Load Next Activity
                        task = Task.GetTaskFromActivity(task.activityID);

                        TaskSelection(task, taskCount, jobPackID);
                    } else if (task.taskOrder == taskCount) {
                        try {
                            List<JobPack> jobPacks = SelectJobPackActivity.GetData(shiftID);

                            boolean redirectToStart = false;

                            if (jobPacks.size() == 1) // At end of the only activity so redirect to menu
                            {
                                redirectToStart = true;
                            } else // Check all other job packs and their activities
                            {
                                int count = 0;
                                for (int i = 0; i < jobPacks.size(); i++) {
                                    count += Task.GetIncompleteTaskCount(jobPacks.get(i).jobPackID);
                                }

                                if (count == 0) // All activities are complete
                                {
                                    redirectToStart = true;
                                }
                            }

                            if (redirectToStart) {
                                Intent startMenuActivity = new Intent(getApplicationContext(), StartActivity.class);
                                startMenuActivity.putExtra("ShiftID", shiftID);
                                startMenuActivity.putExtra("UserID", userID);
                                startActivity(startMenuActivity);
                            }
                        } catch (Exception e) {
                            ErrorLog.CreateError(e, "FreeTextTaskActivityTaskCompleted");
                        }
                    }*/
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }

    }

    public void drawingClick(View view)
    {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view)
    {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view)
    {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }

//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }
}
