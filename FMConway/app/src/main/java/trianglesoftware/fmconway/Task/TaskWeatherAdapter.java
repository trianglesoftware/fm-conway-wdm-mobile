package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.WeatherCondition;
import trianglesoftware.fmconway.R;

public class TaskWeatherAdapter extends BaseAdapter { 
    public List<WeatherCondition> Entities;
    public String WeatherConditionID;
    private final LayoutInflater inflater;

    public TaskWeatherAdapter(Context context, LayoutInflater inflater)
    {
        this.inflater = inflater;
        Entities = new LinkedList<>();
    }

    @Override
    public int getCount() {
        return Entities.size();
    }

    @Override
    public WeatherCondition getItem(int position) {
        return Entities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.row_weather, null);

            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        WeatherCondition weatherCondition = getItem(position);
        holder.name.setText(weatherCondition.name);
        
        if (Objects.equals(weatherCondition.weatherConditionID, WeatherConditionID)) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView name;
        public CheckBox checkbox;
    }
}
