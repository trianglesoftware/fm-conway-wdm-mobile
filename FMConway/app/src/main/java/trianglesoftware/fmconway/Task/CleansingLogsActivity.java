package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Comment.CommentActivity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityRiskAssessment;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Direction;
import trianglesoftware.fmconway.Database.DatabaseObjects.Lane;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class CleansingLogsActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private int taskCount;
    private String userID;

    private Task task;
    private Activity activity;
    private String jobType;

    // inputs
    private ListView cleansingLogsList;
    private CleansingLogsAdapter cleansingLogsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID", "");
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        taskCount = extras.getInt("TaskCount");
        userID = extras.getString("UserID", "");

        super.onCreate(savedInstanceState);

        task = Task.GetTask(taskID);
        activity = Activity.GetActivity(task.activityID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        cleansingLogsList = findViewById(R.id.cleansing_logs_list);
        cleansingLogsAdapter = new CleansingLogsAdapter(this, getLayoutInflater());
        cleansingLogsList.setAdapter(cleansingLogsAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cleansing_logs_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Cleansing Logs";
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void refreshList() {
        List<CleansingLog> cleansingLogs = CleansingLog.getAllForTask(taskID);
        cleansingLogsAdapter.items = cleansingLogs;
        cleansingLogsAdapter.notifyDataSetChanged();
    }

    // note the validation performed here and in CleansingLogActivity validate() must match
    public boolean validate() {
        for (int i = 0; i < cleansingLogsAdapter.items.size(); i++) {
            CleansingLog cleansingLog = cleansingLogsAdapter.items.get(i);
            int number = i + 1;

            if (jobType.equals("Jetting New")) {

                if (ActivityRiskAssessment.getActivityRiskAssessmentFromCleansingLogID(cleansingLog.cleansingLogID) == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Road is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

            } else {

                if (FMConwayUtils.isNullOrWhitespace(cleansingLog.road)) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Road is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (FMConwayUtils.isNullOrWhitespace(cleansingLog.section)) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Section is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.laneID == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Lane is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.directionID == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Direction is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (FMConwayUtils.isNullOrWhitespace(cleansingLog.startMarker)) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Start Marker is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (FMConwayUtils.isNullOrWhitespace(cleansingLog.endMarker)) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - End Marker is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.gulliesCleaned == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Gullies Cleaned is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.gulliesMissed == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Gullies Missed is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.catchpitsCleaned == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Catchpits Cleaned is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.catchpitsMissed == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Catchpits Missed is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.channels == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Channels is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (cleansingLog.slotDrains == null) {
                    Toast.makeText(getApplicationContext(), "Cleansing Log " + number + " - Slot Drains is a required field", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }

        return true;
    }

    public void save() {
        Task.EndTask(task.taskID);

        // load next activity
        /*if (task.taskOrder < taskCount) {
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/

        finish();
    }

    public void addCleansingLogClick(View view) {
        Intent cleansingLogActivity = new Intent(getApplicationContext(), CleansingLogActivity.class);
        cleansingLogActivity.putExtra("JobPackID", jobPackID);
        cleansingLogActivity.putExtra("ShiftID", shiftID);
        cleansingLogActivity.putExtra("TaskID", taskID);
        cleansingLogActivity.putExtra("UserID", userID);
        startActivity(cleansingLogActivity);
    }

    public void confirmClick(View view) {
        if (!validate()) {
            return;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Continue");
        alertDialog.setMessage("Please confirm you have completed this task.");

        alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                save();
            }
        });

        alertDialog.setNegativeButton("Cancel", null);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void rowClick(View view) {
        int pos = cleansingLogsList.getPositionForView(view);
        CleansingLog cleansingLog = cleansingLogsAdapter.getItem(pos);

        Intent cleansingLogActivity = new Intent(getApplicationContext(), CleansingLogActivity.class);
        cleansingLogActivity.putExtra("JobPackID", jobPackID);
        cleansingLogActivity.putExtra("ShiftID", shiftID);
        cleansingLogActivity.putExtra("TaskID", taskID);
        cleansingLogActivity.putExtra("UserID", userID);
        cleansingLogActivity.putExtra("CleansingLogID", cleansingLog.cleansingLogID);
        startActivity(cleansingLogActivity);
    }

    public void removeClick(View view) {
        int pos = cleansingLogsList.getPositionForView(view);
        final CleansingLog cleansingLog = cleansingLogsAdapter.getItem(pos);

        FMConwayUtils.confirmDialog(this, "Delete this record, are you sure?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                CleansingLog.delete(cleansingLog.cleansingLogID);

                if (jobType.equals("Jetting New")) {
                    ActivityRiskAssessment.updateCleansingLogID(null, cleansingLog.cleansingLogID);
                }

                refreshList();
            }
        });
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

}
