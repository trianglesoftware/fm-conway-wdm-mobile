package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Contact;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

class TaskDisplayContactAdapter extends BaseAdapter {
    private List<Contact> items;
    private final LayoutInflater mInflater;

    public TaskDisplayContactAdapter(Context context, LayoutInflater inflater) {
        items = new LinkedList<>();
        mInflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Contact getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_task_display_contact, null);

            holder = new ViewHolder();
            holder.contactNameTextView = convertView.findViewById(R.id.contact_name);
            holder.phoneNumberTextView = convertView.findViewById(R.id.phone_number);
            holder.mobileNumberTextView = convertView.findViewById(R.id.mobile_number);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Contact contact = getItem(position);

        if (!FMConwayUtils.isNullOrWhitespace(contact.contactName)) {
            holder.contactNameTextView.setText(contact.contactName);
        } else {
            holder.contactNameTextView.setVisibility(View.GONE);
        }

        if (!FMConwayUtils.isNullOrWhitespace(contact.phoneNumber)) {
            holder.phoneNumberTextView.setText("P. " + contact.phoneNumber);
        } else {
            holder.phoneNumberTextView.setVisibility(View.GONE);
        }

        if (!FMConwayUtils.isNullOrWhitespace(contact.mobileNumber)) {
            holder.mobileNumberTextView.setText("M. " + contact.mobileNumber);
        } else {
            holder.mobileNumberTextView.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void UpdateData(List<Contact> contacts) {
        items.clear();
        items.addAll(contacts);
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView contactNameTextView;
        public TextView phoneNumberTextView;
        public TextView mobileNumberTextView;
    }
}
