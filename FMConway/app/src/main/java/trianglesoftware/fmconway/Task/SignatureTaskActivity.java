package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import trianglesoftware.fmconway.Database.DatabaseHelper;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskSignature;
import trianglesoftware.fmconway.JobPack.SelectJobPackActivity;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Utilities.Signature;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SignatureTaskActivity extends FMConwayActivity {
    private String taskChecklistID;
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private int taskCount;
    private String userID;
    private ListView taskChecklistQuestionList;
    private TaskChecklistQuestionAdapter taskChecklistQuestionAdapter;
    private DatabaseHelper db;
    private Button nextTaskButton;
    private Task task;
    private TextView scopeDetails;
    private EditText amendments;
    private Button home;
    private Button back;
    private SharedPreferences sp;
    
    private boolean showClientSignature;
    private boolean clientSignatureRequired;
    private boolean showStaffSignature;
    private boolean staffSignatureRequired;
    private boolean showAmendments;
    private boolean showChecklist;
    private boolean showPrintedName;
    private boolean showClientVehicleReg;
    
    private Button clientSignatureButton;
    private Button staffSignatureButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            jobPackID = extras.getString("JobPackID", "");
            taskChecklistID = extras.getString("TaskChecklistID", "");
            taskID = extras.getString("TaskID", "");
            taskCount = extras.getInt("TaskCount");
            userID = extras.getString("UserID", "");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        taskChecklistQuestionList = (ListView) findViewById(R.id.task_signature_checklist_list);
        taskChecklistQuestionAdapter = new TaskChecklistQuestionAdapter(this, getLayoutInflater());
        taskChecklistQuestionList.setAdapter(taskChecklistQuestionAdapter);

        task = Task.GetTask(taskID);
        
        showClientSignature = task.showClientSignature;
        clientSignatureRequired = task.clientSignatureRequired;
        showStaffSignature = task.showStaffSignature;
        staffSignatureRequired = task.staffSignatureRequired;
        
        if (!TextUtils.isEmpty(task.tag)) {
            String[] args;
            if (task.tag.contains("|")) {
                args = task.tag.split(Pattern.quote("|"));
            } else {
                args = new String[] { task.tag };
            }
            for (int i = 0; i < args.length; i++) {
                if (Objects.equals(args[i], "ShowAmendments")) {
                    showAmendments = true;
                } else if (Objects.equals(args[i], "ShowChecklist")) {
                    showChecklist = true;
                } else if (Objects.equals(args[i], "ShowPrintedName")) {
                    showPrintedName = true;
                } else if (Objects.equals(args[i], "ShowClientVehicleReg")) {
                    showClientVehicleReg = true;
                }
            }
        }

        nextTaskButton = (Button) findViewById(R.id.next_button);
        home = (Button) findViewById(R.id.back_to_home);
        back = (Button) findViewById(R.id.back);
        amendments = (EditText) findViewById(R.id.amendments);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(false);
            }
        });

        scopeDetails = (TextView) findViewById(R.id.scope_description);
        scopeDetails.setText(Activity.GetScopeOfWork(task.activityID, shiftID));

        amendments.setText(Task.GetTaskDetails(task.taskID));

        if (task.getStartTime() != null) {
            nextTaskButton.setEnabled(true);
            taskChecklistQuestionList.setVisibility(View.VISIBLE);
        } else {
            taskChecklistQuestionList.setVisibility(View.INVISIBLE);
        }

        if (!showAmendments) { 
            TextView amendmentsLabel = (TextView) findViewById(R.id.amendments_label);
            amendmentsLabel.setVisibility(View.GONE);
            amendments.setVisibility(View.GONE);
        }
        
        if (!showChecklist) {
            TextView checklistLabel = (TextView) findViewById(R.id.task_signature_checklist_label);
            checklistLabel.setVisibility(View.GONE);
            taskChecklistQuestionList.setVisibility(View.GONE);
        }

        clientSignatureButton = (Button) findViewById(R.id.client_signature_button);
        staffSignatureButton = (Button) findViewById(R.id.staff_signature_button);
        
        if (!showClientSignature) {
            clientSignatureButton.setVisibility(View.GONE);
        }

        if (!showStaffSignature) {
            staffSignatureButton.setVisibility(View.GONE);
        }

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskChecklistActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.signature_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Checklist";
    }


    @Override
    public void onResume() {
        super.onResume();
        
        if (showClientSignature) {
            if (TaskSignature.checkClientSignatureExists(taskID)) {
                clientSignatureButton.setBackgroundColor(getResources().getColor(R.color.fmconway_green));
                clientSignatureButton.setTextColor(getResources().getColor(R.color.fmconway_text_black));
            } else if (clientSignatureRequired) {
                clientSignatureButton.setBackgroundColor(getResources().getColor(R.color.fmconway_yellow));
                clientSignatureButton.setTextColor(getResources().getColor(R.color.fmconway_text_black));
            } else {
                clientSignatureButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
                clientSignatureButton.setTextColor(getResources().getColor(R.color.fmconway_text_light));
            }
        }
        
        if (showStaffSignature) {
            if (CheckStaffSignaturesExist()) {
                staffSignatureButton.setBackgroundColor(getResources().getColor(R.color.fmconway_green));
                staffSignatureButton.setTextColor(getResources().getColor(R.color.fmconway_text_black));
            } else if (staffSignatureRequired) {
                staffSignatureButton.setBackgroundColor(getResources().getColor(R.color.fmconway_yellow));
                staffSignatureButton.setTextColor(getResources().getColor(R.color.fmconway_text_black));
            } else {
                staffSignatureButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
                staffSignatureButton.setTextColor(getResources().getColor(R.color.fmconway_text_light));
            }
        }
    }

    private void GetData() throws Exception {
        if (!FMConwayUtils.isNullOrWhitespace(taskChecklistID)) {
            List<TaskChecklistAnswer> taskChecklistAnswers = TaskChecklistAnswer.GetTaskChecklistAnswers(taskChecklistID);

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < taskChecklistAnswers.size(); i++) {
                jsonArray.put(taskChecklistAnswers.get(i).getJSONObject());
            }

            taskChecklistQuestionAdapter.UpdateData(jsonArray);
        }
    }

    public void answerYes(View view) {
        int pos = taskChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = taskChecklistQuestionAdapter.getItem(pos);

        TaskChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), taskChecklistID, true);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "SignatureChecklistActivity");
        }
    }

    public void answerNo(View view) {
        int pos = taskChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = taskChecklistQuestionAdapter.getItem(pos);

        TaskChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), taskChecklistID, false);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "SignatureChecklistActivity");
        }
    }

    //public void startTask(View view)
    //{
    //    Task.StartTask(task.getTaskID());
    //    startTaskButton.setEnabled(false);
    //    finishTaskButton.setEnabled(true);
    //    taskChecklistQuestionList.setVisibility(View.VISIBLE);
    //}

    public void nextTask(View view) {
        if (showChecklist && !CheckComplete()) {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding.", Toast.LENGTH_LONG).show();
            return;
        }
        
        if (clientSignatureRequired && !TaskSignature.checkClientSignatureExists(taskID)) {
            Toast.makeText(getApplicationContext(),"Please add client signature", Toast.LENGTH_LONG).show();
            return;
        }
        
        if (staffSignatureRequired && !CheckStaffSignaturesExist()) {
            Toast.makeText(getApplicationContext(),"Please add staff signatures", Toast.LENGTH_LONG).show();
            return;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Continue");
        alertDialog.setMessage("Please confirm you have completed this task.");
        alertDialog.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                nextTaskContinue();
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }
    
    private void nextTaskContinue() {
        Task.EndTask(task.taskID);
        Task.StoreTaskDetails(task.taskID, amendments.getText().toString());
        finish();

        /*if (task.getTaskOrder() < taskCount) {
            //Load Next Activity
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        } else if (task.getTaskOrder() == taskCount) {
            try {
                List<JobPack> jobPacks = SelectJobPackActivity.GetData(shiftID);

                boolean redirectToStart = false;

                if (jobPacks.size() == 1) // At end of the only activity so redirect to menu
                {
                    redirectToStart = true;
                } else // Check all other job packs and their activities
                {
                    int count = 0;
                    for (int i = 0; i < jobPacks.size(); i++) {
                        count += Task.GetIncompleteTaskCount(jobPacks.get(i).jobPackID);
                    }

                    if (count == 0) // All activities are complete
                    {
                        redirectToStart = true;
                    }
                }

                if (redirectToStart) {
                    Intent startMenuActivity = new Intent(getApplicationContext(), StartActivity.class);
                    startMenuActivity.putExtra("ShiftID", shiftID);
                    startMenuActivity.putExtra("UserID", userID);
                    startActivity(startMenuActivity);
                }
            } catch (Exception e) {
                ErrorLog.CreateError(e, "SignatureTaskActivityTaskCompleted");
            }
        }*/
    }

    public void clientClick(View view)
    {
        if(!TaskSignature.checkClientSignatureExists(taskID))
        {
            //Load Signature Intent for every staff member in shift
            Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
            signatureIntent.putExtra("TaskID", taskID);
            signatureIntent.putExtra("Client", "Client");
            signatureIntent.putExtra("LoggedIn", sp.getString("UserID", ""));
            signatureIntent.putExtra("ShowPrintedName", showPrintedName);
            signatureIntent.putExtra("ShowClientVehicleReg", showClientVehicleReg);
            startActivity(signatureIntent);
        }
    }

    public void staffClick(View view)
    {
        List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
        //int staffSigned = 0;
        for (int i = 0; i < staffToSign.size(); i++)
        {
            if(!TaskSignature.checkStaffSignatureExists(taskID, staffToSign.get(i).staffID))
            {
                //Load Signature Intent for every staff member in shift
                Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
                signatureIntent.putExtra("TaskID", taskID);
                signatureIntent.putExtra("StaffID", staffToSign.get(i).staffID);
                signatureIntent.putExtra("LoggedIn", sp.getString("UserID", ""));
                signatureIntent.putExtra("ShowPrintedName", false);
                signatureIntent.putExtra("ShowClientVehicleReg", false);
                startActivity(signatureIntent);
            }
        }
    }

    public void taskClick(View view)
    {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }



    private boolean CheckComplete()
    {
        if (FMConwayUtils.isNullOrWhitespace(taskChecklistID)) {
            return true;
        }
        
        List<TaskChecklistAnswer> taskChecklistAnswers = TaskChecklistAnswer.GetTaskChecklistAnswers(taskChecklistID);

        boolean complete = true;

        for(int i = 0; i < taskChecklistAnswers.size(); i++) {
            complete = !taskChecklistAnswers.get(i).isNew;
        }

        return complete;
    }

    private boolean CheckStaffSignaturesExist()
    {
        List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
        //int staffSigned = 0;
        for (int i = 0; i < staffToSign.size(); i++)
        {
            if(!TaskSignature.checkStaffSignatureExists(taskID, staffToSign.get(i).staffID))
            {
                return false;
            }
        }

        return true;
    }

    @Override
    public void onBackPressed()
    {
        if (CheckComplete() & TaskSignature.checkClientSignatureExists(taskID) && CheckStaffSignaturesExist()) {

            finish();
        }
        else
        {
            if (!FMConwayUtils.isNullOrWhitespace(taskChecklistID)) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                TaskChecklistAnswer.deleteAllTaskChecklistAnswersForCheck(taskChecklistID);
                                TaskChecklist.deleteTaskChecklist(taskChecklistID);

                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:

                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
                builder.setMessage("Continue to cancel checklist?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
            }  else {
                finish();
            }
        }
    }

    private void returnToMenu(final boolean menu)
    {
        if (CheckComplete()) {

            if (TaskSignature.checkClientSignatureExists(taskID) && CheckStaffSignaturesExist()) {
                if (menu) {
                    if (Objects.equals(shiftID, "")) {
                        Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                        noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        noShiftIntent.putExtra("ShiftID", shiftID);
                        noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(noShiftIntent);
                    } else {
                        Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startIntent.putExtra("ShiftID", shiftID);
                        startIntent.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(startIntent);
                    }
                } else {
                    finish();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Please add signatures", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            //Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();

            if (!FMConwayUtils.isNullOrWhitespace(taskChecklistID)) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                TaskChecklistAnswer.deleteAllTaskChecklistAnswersForCheck(taskChecklistID);
                                TaskChecklist.deleteTaskChecklist(taskChecklistID);

                                if (menu) {
                                    if (Objects.equals(shiftID, "")) {
                                        Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                                        noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        noShiftIntent.putExtra("ShiftID", shiftID);
                                        noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                                        startActivity(noShiftIntent);
                                    } else {
                                        Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                                        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startIntent.putExtra("ShiftID", shiftID);
                                        startIntent.putExtra("UserID", sp.getString("UserID", ""));
                                        startActivity(startIntent);
                                    }
                                }

                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:

                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
                builder.setMessage("Continue to cancel checklist?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

            } else {
                if (menu) {
                    if (Objects.equals(shiftID, "")) {
                        Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                        noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        noShiftIntent.putExtra("ShiftID", shiftID);
                        noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(noShiftIntent);
                    } else {
                        Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startIntent.putExtra("ShiftID", shiftID);
                        startIntent.putExtra("UserID", sp.getString("UserID", ""));
                        startActivity(startIntent);
                    }
                }

                finish();
            }
        }
    }
}
