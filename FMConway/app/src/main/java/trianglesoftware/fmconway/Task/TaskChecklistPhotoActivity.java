package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswerPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class TaskChecklistPhotoActivity extends FMConwayActivityBase {
    private String taskChecklistAnswerID;
    private String activityID;
    private String shiftID;
    
    private EditText reason;
    private TextView reasonHeader;
    private SharedPreferences sp;
    private TaskChecklistAnswer taskChecklistAnswer;
    private GridView gridView;
    private ArrayList<TaskChecklistAnswerPhoto> photoList;
    private TaskChecklistPhotoAdapter adapter;

    private String filepath;
    private final int REQUEST_CODE = 10;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_task_checklist_photo);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskChecklistAnswerID = extras.getString("TaskChecklistAnswerID","");
            activityID = extras.getString("ActivityID","");
            shiftID = extras.getString("ShiftID");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        taskChecklistAnswer = TaskChecklistAnswer.GetTaskChecklistAnswer(taskChecklistAnswerID);
        
        reason = (EditText) findViewById(R.id.task_checklist_reason);
        reason.setText(taskChecklistAnswer.reason);

        reasonHeader = (TextView) findViewById(R.id.txt_reason);

        //ToDo set reason description title
        //reasonText.setText("Test");

        gridView = (GridView)findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = TaskChecklistAnswerPhoto.getPhotosForTaskChecklistAnswer(taskChecklistAnswerID);
        adapter = new TaskChecklistPhotoAdapter(this, photoList);

        gridView.setAdapter(adapter);
    }

    public void submitReason(View view) {
        if (photoList.size() == 0) {
            Toast.makeText(getApplicationContext(), "At least one photo is required", Toast.LENGTH_LONG).show();
            return;
        }

        TaskChecklistAnswer.UpdateReason(taskChecklistAnswerID, reason.getText().toString());
        finish();
    }

    public void addTaskChecklistPhoto(View view)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        
        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();
        
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    private static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                AddData(filepath);

                photoList = TaskChecklistAnswerPhoto.getPhotosForTaskChecklistAnswer(taskChecklistAnswerID);
                adapter = new TaskChecklistPhotoAdapter(this, photoList);
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        }
    }

    private void AddData(String imageLocation) {
        LocationManager lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude =  0;
        double latitude =  0;

        if(location != null) {
            longitude =  location.getLongitude();
            latitude =  location.getLatitude();
        }

        TaskChecklistAnswerPhoto photo = new TaskChecklistAnswerPhoto();
        photo.taskChecklistAnswerPhotoID = UUID.randomUUID().toString();
        photo.taskChecklistAnswerID = taskChecklistAnswerID;
        photo.imageLocation = imageLocation;
        photo.longitude = longitude;
        photo.latitude = latitude;
        photo.activityID = activityID;
        photo.shiftID = shiftID;
        photo.userID = sp.getString("UserID", "");
        photo.time = new Date();

        TaskChecklistAnswerPhoto.addTaskChecklistAnswerPhoto(photo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation changes
        outState.putString("filepath", filepath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
    }

}
