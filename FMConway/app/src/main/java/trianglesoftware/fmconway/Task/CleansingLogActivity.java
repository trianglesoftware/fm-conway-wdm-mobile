package trianglesoftware.fmconway.Task;

import static trianglesoftware.fmconway.Utilities.FMConwayUtils.getOutputMediaFile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewCompat;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityRiskAssessment;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLogPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.Direction;
import trianglesoftware.fmconway.Database.DatabaseObjects.Lane;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class CleansingLogActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private String userID;
    private String cleansingLogID;
    private String activityRiskAssessmentID;

    private Task task;
    private Activity activity;
    private String jobType;
    private CleansingLog cleansingLog;

    private String filepath;
    private String retakeCleansingLogPhotoID;

    private SharedPreferences sp;

    // inputs
    private EditText roadEditText;
    private EditText sectionEditText;
    private Spinner laneSpinner;
    private Spinner directionSpinner;
    private EditText startMarkerEditText;
    private EditText endMarkerEditText;
    private EditText gulliesCleanedEditText;
    private EditText gulliesMissedEditText;
    private EditText catchpitsCleanedEditText;
    private EditText catchpitsMissedEditText;
    private EditText channelsEditText;
    private EditText slotDrainsEditText;
    private EditText commentsEditText;
    private Spinner activityRiskAssessmentSpinner;
    private EditText defectsEditText;

    private TextView sectionLabel;
    private TextView laneLabel;
    private TextView directionLabel;
    private TextView startLabel;
    private TextView endLabel;
    private TextView slotLabel;
    private TextView defectsLabel;

    private GridView gridView;
    private ArrayList<CleansingLogPhoto> photosList;
    private CleansingLogPhotoAdapter photosAdapter;

    // request codes
    private int photoRequestCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID", "");
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        userID = extras.getString("UserID", "");
        cleansingLogID = extras.getString("CleansingLogID", null);
        activityRiskAssessmentID = extras.getString("ActivityRiskAssessmentID", null);

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        task = Task.GetTask(taskID);
        activity = Activity.GetActivity(task.activityID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        if (cleansingLogID != null) {
            cleansingLog = CleansingLog.get(cleansingLogID);
        }

        // inputs
        roadEditText = findViewById(R.id.road);
        sectionEditText = findViewById(R.id.section);
        laneSpinner = findViewById(R.id.lane);
        directionSpinner = findViewById(R.id.direction);
        startMarkerEditText = findViewById(R.id.start_marker);
        endMarkerEditText = findViewById(R.id.end_marker);
        gulliesCleanedEditText = findViewById(R.id.gullies_cleaned);
        gulliesMissedEditText = findViewById(R.id.gullies_missed);
        catchpitsCleanedEditText = findViewById(R.id.catchpits_cleaned);
        catchpitsMissedEditText = findViewById(R.id.catchpits_missed);
        channelsEditText = findViewById(R.id.channels);
        slotDrainsEditText = findViewById(R.id.slot_drains);
        commentsEditText = findViewById(R.id.comments);
        activityRiskAssessmentSpinner = findViewById(R.id.spinner_road);
        defectsEditText = findViewById(R.id.defects);

        //labels
        sectionLabel = findViewById(R.id.section_label);
        laneLabel = findViewById(R.id.lane_label);
        directionLabel = findViewById(R.id.direction_label);
        startLabel = findViewById(R.id.start_marker_label);
        endLabel = findViewById(R.id.end_marker_label);
        slotLabel = findViewById(R.id.slot_drains_label);
        defectsLabel= findViewById(R.id.defects_label);

        List<Lane> lanes = Lane.getLookupItems(cleansingLog != null ? cleansingLog.laneID : null);
        ArrayAdapter<Lane> laneAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, lanes);
        laneSpinner.setAdapter(laneAdapter);

        List<Direction> directions = Direction.getLookupItems(cleansingLog != null ? cleansingLog.directionID : null);
        ArrayAdapter<Direction> directionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, directions);
        directionSpinner.setAdapter(directionAdapter);

        gridView = (GridView) findViewById(R.id.photo_gridView);
        ViewCompat.setNestedScrollingEnabled(gridView, true);
        photosList = new ArrayList<>();

        if (cleansingLog != null) {
            photosList = CleansingLogPhoto.getPhotosForCleansingLog(cleansingLog.cleansingLogID);
        }

        photosAdapter = new CleansingLogPhotoAdapter(this, photosList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoEditDialog(view);
            }
        });

        gridView.setAdapter(photosAdapter);

        List<ActivityRiskAssessment> activityRiskAssessments = ActivityRiskAssessment.getLookupItems(cleansingLog != null ? cleansingLog.cleansingLogID : null);
        if (jobType.equals("Jetting New")) {
            activityRiskAssessmentSpinner.setVisibility(View.VISIBLE);
            roadEditText.setVisibility(View.GONE);

            ArrayAdapter<ActivityRiskAssessment> activityRiskAssessmentsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, activityRiskAssessments);
            activityRiskAssessmentSpinner.setAdapter(activityRiskAssessmentsAdapter);

            //set initial value when coming from activity road activity
            for (int i = 0; i < activityRiskAssessments.size(); i++) {
                if (Objects.equals(activityRiskAssessments.get(i).activityRiskAssessmentID, activityRiskAssessmentID)) {
                    activityRiskAssessmentSpinner.setSelection(i);
                }
            }

            sectionLabel.setVisibility(View.GONE);
            laneLabel.setVisibility(View.GONE);
            directionLabel.setVisibility(View.GONE);
            startLabel.setVisibility(View.GONE);
            endLabel.setVisibility(View.GONE);
            slotLabel.setVisibility(View.GONE);
            defectsLabel.setVisibility(View.VISIBLE);

            sectionEditText.setVisibility(View.GONE);
            laneSpinner.setVisibility(View.GONE);
            directionSpinner.setVisibility(View.GONE);
            startMarkerEditText.setVisibility(View.GONE);
            endMarkerEditText.setVisibility(View.GONE);
            slotDrainsEditText.setVisibility(View.GONE);
            defectsEditText.setVisibility(View.VISIBLE);
        }

        // set values
        if (cleansingLog != null) {
            // trim trailing 0's
            DecimalFormat decimalFormat = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            decimalFormat.setMaximumFractionDigits(340); //340 = DecimalFormat.DOUBLE_FRACTION_DIGITS

            if (cleansingLog.road != null) {
                roadEditText.setText(cleansingLog.road);
            }

            if (jobType.equals("Jetting New")) {
                ActivityRiskAssessment activityRiskAssessment = ActivityRiskAssessment.getActivityRiskAssessmentFromCleansingLogID(cleansingLogID);

                for (int i = 0; i < activityRiskAssessments.size(); i++) {
                    if (Objects.equals(activityRiskAssessments.get(i).activityRiskAssessmentID, activityRiskAssessment.activityRiskAssessmentID)) {
                        activityRiskAssessmentSpinner.setSelection(i);
                    }
                }
            }

            if (cleansingLog.section != null) {
                sectionEditText.setText(cleansingLog.section);
            }

            if (cleansingLog.laneID != null) {
                for (int i = 0; i < lanes.size(); i++) {
                    if (Objects.equals(lanes.get(i).laneID, cleansingLog.laneID)) {
                        laneSpinner.setSelection(i);
                    }
                }
            }

            if (cleansingLog.directionID != null) {
                for (int i = 0; i < directions.size(); i++) {
                    if (Objects.equals(directions.get(i).directionID, cleansingLog.directionID)) {
                        directionSpinner.setSelection(i);
                    }
                }
            }

            if (cleansingLog.startMarker != null) {
                startMarkerEditText.setText(cleansingLog.startMarker);
            }

            if (cleansingLog.endMarker != null) {
                endMarkerEditText.setText(cleansingLog.endMarker);
            }

            if (cleansingLog.gulliesCleaned != null) {
                gulliesCleanedEditText.setText(decimalFormat.format(cleansingLog.gulliesCleaned));
            }

            if (cleansingLog.gulliesMissed != null) {
                gulliesMissedEditText.setText(decimalFormat.format(cleansingLog.gulliesMissed));
            }

            if (cleansingLog.catchpitsCleaned != null) {
                catchpitsCleanedEditText.setText(decimalFormat.format(cleansingLog.catchpitsCleaned));
            }

            if (cleansingLog.catchpitsMissed != null) {
                catchpitsMissedEditText.setText(decimalFormat.format(cleansingLog.catchpitsMissed));
            }

            if (cleansingLog.channels != null) {
                channelsEditText.setText(decimalFormat.format(cleansingLog.channels));
            }

            if (cleansingLog.slotDrains != null) {
                slotDrainsEditText.setText(decimalFormat.format(cleansingLog.slotDrains));
            }

            if (cleansingLog.comments != null) {
                commentsEditText.setText(cleansingLog.comments);
            }
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cleansing_log_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Cleansing Log";
    }

    @Override
    public void onResume() {
        super.onResume();

        // for when activity is destroyed & recreated (usually when camera rotates)
        if (cleansingLog == null && cleansingLogID != null) {
            cleansingLog = CleansingLog.get(cleansingLogID);

            photosList = CleansingLogPhoto.getPhotosForCleansingLog(cleansingLogID);
            photosAdapter.update(photosList);

            getWindow().getDecorView().findViewById(R.id.top_layout).clearFocus();
        }
    }

    public void photoClick(View view) {
        // cleansing log must be saved if hasn't already for photos / comments to be saved regardless of validation
        // additional validation will be added to list confirm button to ensure all cleansing logs are valid
        if (cleansingLog == null) {
            cleansingLog = save();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();
        retakeCleansingLogPhotoID = null;

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, photoRequestCode);
    }

    private void photoEditDialog(final View imageView) {
        View photoDialogView = getLayoutInflater().inflate(R.layout.photo_dialog, null);
        final int position = gridView.getPositionForView(imageView);
        final CleansingLogPhoto cleansingLogPhoto = photosList.get(position);

        final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog))
                .setView(photoDialogView)
                .setCancelable(true)
                .create();

        photoDialogView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FMConwayUtils.confirmDialog(CleansingLogActivity.this, "Delete photo and comment, are you sure?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CleansingLogPhoto.deleteCleansingLogPhotoFile(imageView.getTag().toString());
                        CleansingLogPhoto.deleteCleansingLogPhoto(imageView.getTag().toString());
                        photosList.remove(position);
                        photosAdapter.update(photosList);
                        dialog.dismiss();
                    }
                });
            }
        });

        photoDialogView.findViewById(R.id.view_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", new File(cleansingLogPhoto.imageLocation));

                String extension = MimeTypeMap.getFileExtensionFromUrl(cleansingLogPhoto.imageLocation);
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileIntent.setDataAndType(uri, type);

                startActivity(fileIntent);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.retake_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = getOutputMediaFile();
                Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                filepath = file.getPath();
                retakeCleansingLogPhotoID = cleansingLogPhoto.cleansingLogPhotoID;

                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                // start the image capture Intent
                startActivityForResult(intent, photoRequestCode);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.edit_comments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectActivity = new Intent(CleansingLogActivity.this, PhotoCommentActivity.class);
                selectActivity.putExtra("CleansingLogPhotoID", cleansingLogPhoto.cleansingLogPhotoID);
                startActivity(selectActivity);
                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    // note the validation performed here and in CleansingLogsActivity validate() must match
    public boolean validate() {
        if (jobType.equals("Jetting New")) {

            ActivityRiskAssessment road = (ActivityRiskAssessment) activityRiskAssessmentSpinner.getSelectedItem();
            if (road.activityRiskAssessmentID == null) {
                Toast.makeText(getApplicationContext(), "Road is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

        } else {

            String road = roadEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(road)) {
                Toast.makeText(getApplicationContext(), "Road is a required field", Toast.LENGTH_LONG).show();
                return false;
            }


            String section = sectionEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(section)) {
                Toast.makeText(getApplicationContext(), "Section is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            Lane lane = (Lane) laneSpinner.getSelectedItem();
            if (lane.laneID == null) {
                Toast.makeText(getApplicationContext(), "Lane is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            Direction direction = (Direction) directionSpinner.getSelectedItem();
            if (direction.directionID == null) {
                Toast.makeText(getApplicationContext(), "Direction is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String startMarker = startMarkerEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(startMarker)) {
                Toast.makeText(getApplicationContext(), "Start Marker is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String endMarker = endMarkerEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(endMarker)) {
                Toast.makeText(getApplicationContext(), "End Marker is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String gulliesCleaned = gulliesCleanedEditText.getText().toString();
            if (!FMConwayUtils.isNumber(gulliesCleaned)) {
                Toast.makeText(getApplicationContext(), "Gullies Cleaned is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String gulliesMissed = gulliesMissedEditText.getText().toString();
            if (!FMConwayUtils.isNumber(gulliesMissed)) {
                Toast.makeText(getApplicationContext(), "Gullies Missed is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String catchpitsCleaned = catchpitsCleanedEditText.getText().toString();
            if (!FMConwayUtils.isNumber(catchpitsCleaned)) {
                Toast.makeText(getApplicationContext(), "Catchpits Cleaned is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String catchpitsMissed = catchpitsMissedEditText.getText().toString();
            if (!FMConwayUtils.isNumber(catchpitsMissed)) {
                Toast.makeText(getApplicationContext(), "Catchpits Missed is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String channels = channelsEditText.getText().toString();
            if (!FMConwayUtils.isNumber(channels)) {
                Toast.makeText(getApplicationContext(), "Channels is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String slotDrains = slotDrainsEditText.getText().toString();
            if (!FMConwayUtils.isNumber(slotDrains)) {
                Toast.makeText(getApplicationContext(), "Slot Drains is a required field", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    public CleansingLog save() {
        if (cleansingLogID == null) {
            cleansingLog = new CleansingLog();
            cleansingLog.cleansingLogID = UUID.randomUUID().toString();
            cleansingLog.taskID = taskID;
            cleansingLog.activityID = activity.activityID;
            cleansingLog.createdDate = new Date();

            cleansingLogID = cleansingLog.cleansingLogID;
        }

        cleansingLog.road = roadEditText.getText().toString();
        cleansingLog.section = sectionEditText.getText().toString();

        Lane lane = (Lane) laneSpinner.getSelectedItem();
        cleansingLog.laneID = lane.laneID;

        Direction direction = (Direction) directionSpinner.getSelectedItem();
        cleansingLog.directionID = direction.directionID;

        cleansingLog.startMarker = startMarkerEditText.getText().toString();
        cleansingLog.endMarker = endMarkerEditText.getText().toString();
        cleansingLog.gulliesCleaned = FMConwayUtils.parseDouble(gulliesCleanedEditText.getText().toString(), null);
        cleansingLog.gulliesMissed = FMConwayUtils.parseDouble(gulliesMissedEditText.getText().toString(), null);
        cleansingLog.catchpitsCleaned = FMConwayUtils.parseDouble(catchpitsCleanedEditText.getText().toString(), null);
        cleansingLog.catchpitsMissed = FMConwayUtils.parseDouble(catchpitsMissedEditText.getText().toString(), null);
        cleansingLog.channels = FMConwayUtils.parseDouble(channelsEditText.getText().toString(), null);
        cleansingLog.slotDrains = FMConwayUtils.parseDouble(slotDrainsEditText.getText().toString(), null);
        cleansingLog.defects = FMConwayUtils.parseDouble(defectsEditText.getText().toString(), null);
        cleansingLog.comments = commentsEditText.getText().toString();

        if (jobType.equals("Jetting New")) {
            ActivityRiskAssessment road = (ActivityRiskAssessment) activityRiskAssessmentSpinner.getSelectedItem();
            ActivityRiskAssessment.updateCleansingLogID(road.activityRiskAssessmentID, cleansingLogID);

            cleansingLog.road = road.roadName;
        }

        CleansingLog.save(cleansingLog);

        return cleansingLog;
    }

    public void saveClick(View view) {
        if (!validate()) {
            return;
        }

        FMConwayUtils.confirmDialog(this, "Save this record, are you sure?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                save();
                finish();
            }
        });
    }

    private CleansingLogPhoto savePhoto(String cleansingLogPhotoID, String imageLocation) {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude = 0;
        double latitude = 0;

        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        CleansingLogPhoto cleansingLogPhoto = null;

        if (cleansingLogPhotoID != null) {
            cleansingLogPhoto = CleansingLogPhoto.getCleansingLogPhoto(cleansingLogPhotoID);
        } else {
            cleansingLogPhoto = new CleansingLogPhoto();
            cleansingLogPhoto.cleansingLogPhotoID = UUID.randomUUID().toString();
        }

        cleansingLogPhoto.cleansingLogID = cleansingLogID;
        cleansingLogPhoto.longitude = longitude;
        cleansingLogPhoto.latitude = latitude;
        cleansingLogPhoto.imageLocation = imageLocation;
        cleansingLogPhoto.userID = sp.getString("UserID", "");
        cleansingLogPhoto.time = new Date();
        cleansingLogPhoto.shiftID = shiftID;

        CleansingLogPhoto.saveCleansingLogPhoto(cleansingLogPhoto);

        return cleansingLogPhoto;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == photoRequestCode) {
            if (resultCode == RESULT_OK) {
                if (retakeCleansingLogPhotoID != null) {
                    CleansingLogPhoto cleansingLogPhoto = savePhoto(retakeCleansingLogPhotoID, filepath);
                    //FMConwayUtils.debugImageCompression(this, cleansingLogPhoto.imageLocation);

                } else {
                    CleansingLogPhoto cleansingLogPhoto = savePhoto(null, filepath);
                    //FMConwayUtils.debugImageCompression(this, cleansingLogPhoto.imageLocation);

                    Intent selectActivity = new Intent(getApplicationContext(), PhotoCommentActivity.class);
                    selectActivity.putExtra("CleansingLogPhotoID", cleansingLogPhoto.cleansingLogPhotoID);
                    startActivity(selectActivity);
                }

                photosList = CleansingLogPhoto.getPhotosForCleansingLog(cleansingLogID);
                photosAdapter.update(photosList);
            }
        }
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation changes
        outState.putString("filepath", filepath);
        outState.putString("retakeCleansingLogPhotoID", retakeCleansingLogPhotoID);
        outState.putString("cleansingLogID", cleansingLogID);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
        retakeCleansingLogPhotoID = savedInstanceState.getString("retakeCleansingLogPhotoID");
        cleansingLogID = savedInstanceState.getString("cleansingLogID");
    }
}
