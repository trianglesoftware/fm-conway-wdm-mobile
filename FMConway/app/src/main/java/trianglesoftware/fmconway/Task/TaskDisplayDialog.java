package trianglesoftware.fmconway.Task;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;

public class TaskDisplayDialog extends FMConwayActivityBase {
    private String activityID;
    private ListView taskList;
    private TaskAdapter taskAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_display_dialog);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            activityID = extras.getString("ActivityID","");
        }

        ListView taskList = (ListView) findViewById(R.id.select_task_list);
        taskAdapter = new TaskAdapter(this, getLayoutInflater());
        taskList.setAdapter(taskAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "TaskDisplayDialog");
        }
    }

    private void GetData() throws Exception
    {
        List<Task> tasks = Task.GetTasksForActivity(activityID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < tasks.size(); i++) {
            jsonArray.put(tasks.get(i).getJSONObject());
        }

        taskAdapter.UpdateData(jsonArray);
    }

    public void backClick(View view)
    {
        finish();
    }
}
