package trianglesoftware.fmconway.Task;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.WeatherCondition;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class TaskWeatherActivity extends FMConwayActivity {
    //intent
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private int taskCount;

    //controls
    private ListView list;
    private TaskWeatherAdapter adapter;

    //other
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobPackID = extras.getString("JobPackID", "");
            shiftID = extras.getString("ShiftID", "");
            taskID = extras.getString("TaskID", "");
            taskCount = extras.getInt("TaskCount");
        }

        super.onCreate(savedInstanceState);

        task = Task.GetTask(taskID);

        list = (ListView) findViewById(R.id.weathercondition_list);
        adapter = new TaskWeatherAdapter(this, getLayoutInflater());
        list.setAdapter(adapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_task_weather;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Weather";
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskEquipmentActivity");
        }
    }

    private void GetData() {
        adapter.Entities = WeatherCondition.getAllWeatherConditions();
        adapter.WeatherConditionID = Task.getWeatherCondition(taskID);
        adapter.notifyDataSetChanged();
    }

    public void rowClick(View view) {
        String oldWeatherConditionID = Task.getWeatherCondition(taskID);

        int position = list.getPositionForView(view);
        WeatherCondition weatherCondition = adapter.getItem(position);

        String newWeatherConditionID = null;
        if (!Objects.equals(weatherCondition.weatherConditionID, oldWeatherConditionID)) {
            newWeatherConditionID = weatherCondition.weatherConditionID;
        }

        Task.updateWeatherCondition(taskID, newWeatherConditionID);
        GetData();
    }

    public void confirmClick(View view) {
        String weatherConditionID = Task.getWeatherCondition(taskID);
        if (weatherConditionID == null) {
            Toast.makeText(getApplicationContext(), "Please set a weather condition", Toast.LENGTH_SHORT).show();
            return;
        }

        taskFinish();
    }

    private void taskFinish() {
        Task.EndTask(taskID);
        finish();

        /*if (task.taskOrder < taskCount) {
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/
    }
}
