package trianglesoftware.fmconway.Task;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityRiskAssessment;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.RoadSpeed;
import trianglesoftware.fmconway.Database.DatabaseObjects.TMRequirement;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class ActivityRiskAssessmentActivity extends FMConwayActivity {
    private String shiftID;
    private String taskID;
    private String userID;
    private ActivityRiskAssessment activityRiskAssessment;
    private String activityRiskAssessmentID;
    private String jobPackID;

    private Task task;

    private Spinner speedSpinner;
    private Spinner tmReqsSpinner;

    private EditText roadNameEditText;
    private CheckBox schoolsCheckbox;
    private CheckBox treeCanopiesCheckbox;
    private CheckBox pedestrianCrossingsCheckbox;
    private CheckBox waterCoursesCheckbox;
    private CheckBox overheadCablesCheckbox;
    private CheckBox adverseWeatherCheckbox;
    private EditText notesEditText;
    private Button deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        userID = extras.getString("UserID", "");
        activityRiskAssessmentID = extras.getString("ActivityRiskAssessmentID", null);
        jobPackID = extras.getString("JobPackID", "");

        super.onCreate(savedInstanceState);

        if (activityRiskAssessmentID != null) {
            activityRiskAssessment = ActivityRiskAssessment.get(activityRiskAssessmentID);
        }

        task = Task.GetTask(taskID);

        //Inputs
        speedSpinner = findViewById(R.id.road_speed);
        tmReqsSpinner = findViewById(R.id.tm_requirements);
        roadNameEditText = findViewById(R.id.road_name);
        schoolsCheckbox = findViewById(R.id.checkbox_schools);
        treeCanopiesCheckbox = findViewById(R.id.checkbox_tree_canopies);
        pedestrianCrossingsCheckbox = findViewById(R.id.checkbox_pedestrian_crossings);
        waterCoursesCheckbox = findViewById(R.id.checkbox_water_courses);
        overheadCablesCheckbox = findViewById(R.id.checkbox_overhead_cables);
        adverseWeatherCheckbox = findViewById(R.id.checkbox_adverse_weather);
        notesEditText = findViewById(R.id.road_notes);
        deleteButton = findViewById(R.id.delete);

        List<RoadSpeed> speeds = RoadSpeed.getLookupItems(activityRiskAssessment != null ? activityRiskAssessment.roadSpeedID : null);
        ArrayAdapter<RoadSpeed> laneAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, speeds);
        speedSpinner.setAdapter(laneAdapter);

        List<TMRequirement> tmReqs = TMRequirement.getLookupItems(activityRiskAssessment != null ? activityRiskAssessment.tmRequirementID : null);
        ArrayAdapter<TMRequirement> directionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, tmReqs);
        tmReqsSpinner.setAdapter(directionAdapter);

        if (activityRiskAssessmentID != null) {

            deleteButton.setVisibility(View.VISIBLE);

            roadNameEditText.setText(activityRiskAssessment.roadName);

            if (activityRiskAssessment.roadSpeedID != null) {
                for (int i = 0; i < speeds.size(); i++) {
                    if (Objects.equals(speeds.get(i).roadSpeedID, activityRiskAssessment.roadSpeedID)) {
                        speedSpinner.setSelection(i);
                    }
                }
            }

            if (activityRiskAssessment.tmRequirementID != null) {
                for (int i = 0; i < tmReqs.size(); i++) {
                    if (Objects.equals(tmReqs.get(i).tmRequirementID, activityRiskAssessment.tmRequirementID)) {
                        tmReqsSpinner.setSelection(i);
                    }
                }
            }

            schoolsCheckbox.setChecked(activityRiskAssessment.schools);
            treeCanopiesCheckbox.setChecked(activityRiskAssessment.treeCanopies);
            pedestrianCrossingsCheckbox.setChecked(activityRiskAssessment.pedestrianCrossings);
            waterCoursesCheckbox.setChecked(activityRiskAssessment.waterCourses);
            overheadCablesCheckbox.setChecked(activityRiskAssessment.overheadCables);
            adverseWeatherCheckbox.setChecked(activityRiskAssessment.adverseWeather);

            notesEditText.setText(activityRiskAssessment.notes);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_activity_risk_assessment;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Risk Assessment";
    }

    // note the validation performed here and in CleansingLogsActivity validate() must match
    public boolean validate() {
        String road = roadNameEditText.getText().toString();
        if (FMConwayUtils.isNullOrWhitespace(road)) {
            Toast.makeText(getApplicationContext(), "Road name is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        RoadSpeed speed = (RoadSpeed) speedSpinner.getSelectedItem();
        if (speed.roadSpeedID == null) {
            Toast.makeText(getApplicationContext(), "Road Speed is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        TMRequirement tmReq = (TMRequirement) tmReqsSpinner.getSelectedItem();
        if (tmReq.tmRequirementID == null) {
            Toast.makeText(getApplicationContext(), "TM Requirement is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public ActivityRiskAssessment save() {
        if (activityRiskAssessmentID == null) {
            activityRiskAssessment = new ActivityRiskAssessment();
            activityRiskAssessment.activityRiskAssessmentID = UUID.randomUUID().toString();
            activityRiskAssessment.taskID = taskID;
            activityRiskAssessment.createdDate = new Date();

            activityRiskAssessmentID = activityRiskAssessment.activityRiskAssessmentID;
        }

        activityRiskAssessment.roadName = roadNameEditText.getText().toString();
        activityRiskAssessment.notes = notesEditText.getText().toString();

        RoadSpeed speed = (RoadSpeed) speedSpinner.getSelectedItem();
        activityRiskAssessment.roadSpeedID = speed.roadSpeedID;

        TMRequirement tmReqs = (TMRequirement) tmReqsSpinner.getSelectedItem();
        activityRiskAssessment.tmRequirementID = tmReqs.tmRequirementID;

        activityRiskAssessment.schools = schoolsCheckbox.isChecked();
        activityRiskAssessment.treeCanopies = treeCanopiesCheckbox.isChecked();
        activityRiskAssessment.pedestrianCrossings = pedestrianCrossingsCheckbox.isChecked();
        activityRiskAssessment.waterCourses = waterCoursesCheckbox.isChecked();
        activityRiskAssessment.overheadCables = overheadCablesCheckbox.isChecked();
        activityRiskAssessment.adverseWeather = adverseWeatherCheckbox.isChecked();
        activityRiskAssessment.isActive = true;

        ActivityRiskAssessment.save(activityRiskAssessment);

        return activityRiskAssessment;
    }

    public void saveClick(View view) {
        if (!validate()) {
            return;
        }

        if(!schoolsCheckbox.isChecked() && !treeCanopiesCheckbox.isChecked() && !pedestrianCrossingsCheckbox.isChecked() && !waterCoursesCheckbox.isChecked()
                && !overheadCablesCheckbox.isChecked() && !adverseWeatherCheckbox.isChecked()) {

            FMConwayUtils.confirmDialog(this, "No hazards have been selected, is this correct?", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    save();
                    finish();
                }
            });

        } else {
            save();
            finish();
        }
    }

    public void deleteClick(View view) {
        FMConwayUtils.confirmDialog(this, "Are you sure you want to remove this record? This will also remove any associated Cleansing Log", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(activityRiskAssessment.cleansingLogID != null) {
                    CleansingLog.delete(activityRiskAssessment.cleansingLogID);
                }

                ActivityRiskAssessment.delete(activityRiskAssessmentID);
                finish();
            }
        });
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

}
