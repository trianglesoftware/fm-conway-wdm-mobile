package trianglesoftware.fmconway.Task;

/**
 * Created by Adam.Patrick on 23/11/2016.
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLogPhoto;

class CleansingLogPhotoAdapter extends  BaseAdapter {
    private final Context mContext;
    private ArrayList<CleansingLogPhoto> photoList = new ArrayList<>();
    private View.OnClickListener onClickListener;

    public CleansingLogPhotoAdapter(Context c, ArrayList<CleansingLogPhoto> PhotoList, View.OnClickListener onClickListener) {
        this.mContext = c;
        this.photoList = PhotoList;
        this.onClickListener = onClickListener;
    }

    public int getCount() {
        return photoList.size();
    }

    public Object getItem(int position) {
        return photoList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        final CleansingLogPhoto cleansingLogPhoto = photoList.get(position);

        final ImageView imageView = new ImageView(mContext);
        imageView.setImageBitmap(cleansingLogPhoto.imageData);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setRotation(90);
        imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
        imageView.setTag(cleansingLogPhoto.cleansingLogPhotoID);
        imageView.setOnClickListener(onClickListener);

        return imageView;
    }

    public void update(ArrayList<CleansingLogPhoto> photoList) {
        this.photoList = photoList;
        this.notifyDataSetChanged();
    }
}
