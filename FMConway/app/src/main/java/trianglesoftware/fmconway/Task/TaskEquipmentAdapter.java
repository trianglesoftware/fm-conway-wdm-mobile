package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.R;


class TaskEquipmentAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;
    public String taskID;
    private List<TaskEquipment> taskEquipment;

    public TaskEquipmentAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_task_equipment, null);

            holder = new ViewHolder();
            holder.leftRowTextView = (TextView)convertView.findViewById(R.id.text_leftrow);
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_middlerow);
            holder.rightRowTextView = (EditText)convertView.findViewById(R.id.text_rightrow);
            holder.assetsButton = (Button) convertView.findViewById(R.id.assets);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String equipmentName = jsonObject.optString("Name", "");
        String equipmentType = jsonObject.optString("EquipmentType", "");
        String taskEquipmentID = jsonObject.optString("TaskEquipmentID", "");
        int quantity = jsonObject.optInt("Quantity", 0);
        boolean isVmsOrAsset = !jsonObject.optString("VmsOrAsset", "").isEmpty();
        int emptyAssets = jsonObject.optInt("EmptyAssets", 0);
        int totalAssets = jsonObject.optInt("TotalAssets", 0);

        holder.middleRowTextView.setTag(taskEquipmentID);

        holder.leftRowTextView.setText(equipmentName);
        holder.middleRowTextView.setText(equipmentType);
        holder.rightRowTextView.setText(String.valueOf(quantity));
        
        if (isVmsOrAsset) {
            holder.assetsButton.setVisibility(View.VISIBLE);
            
            if (quantity == totalAssets && emptyAssets == 0) {
                holder.assetsButton.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_blue));
                holder.assetsButton.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_light));
            } else {
                holder.assetsButton.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_yellow));
                holder.assetsButton.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_black));
            }
        } else {
            holder.assetsButton.setVisibility(View.INVISIBLE);
        }

        holder.rightRowTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                {
                    String taskEquipmentID = holder.middleRowTextView.getTag().toString();

                    int quantity = 0;
                    if (!Objects.equals(holder.rightRowTextView.getText().toString(), "")) {
                        quantity = Integer.valueOf(holder.rightRowTextView.getText().toString());
                    }

                    TaskEquipment.StoreTaskDetails(taskEquipmentID, quantity);
                }
            }
        });

        holder.rightRowTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    String taskEquipmentID = holder.middleRowTextView.getTag().toString();

                    int quantity = 0;
                    if (!Objects.equals(holder.rightRowTextView.getText().toString(), "")) {
                        quantity = Integer.valueOf(holder.rightRowTextView.getText().toString());
                    }

                    TaskEquipment.StoreTaskDetails(taskEquipmentID, quantity);
                    try {
                        GetData();
                    }
                    catch (Exception e)
                    {
                        ErrorLog.CreateError(e, "TaskEquipmentAdapter");
                    }
                }
                return false;
            }
        });

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;

        notifyDataSetChanged();
    }

    private void GetData() throws Exception
    {
        taskEquipment = TaskEquipment.GetTaskEquipmentForTask(taskID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < taskEquipment.size(); i++) {
            jsonArray.put(taskEquipment.get(i).getJSONObject());
        }

        UpdateData(jsonArray);
    }

    public static class ViewHolder {
        public TextView leftRowTextView;
        public TextView middleRowTextView;
        public TextView rightRowTextView;
        public Button assetsButton;
    }
}
