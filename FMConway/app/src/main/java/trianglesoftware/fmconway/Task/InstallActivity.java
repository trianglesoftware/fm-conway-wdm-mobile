package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class InstallActivity extends FMConwayActivity {
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private int taskCount;
    private Task task;
    private TextView taskInstructions;
    private Button startTaskButton;
    private Button finishTaskButton;
    private TextView scopeDetails;
    private Button maintenanceButton;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            jobPackID = extras.getString("JobPackID", "");
            taskID = extras.getString("TaskID", "");
            taskCount = extras.getInt("TaskCount");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        task = Task.GetTask(taskID);

        taskInstructions = (TextView) findViewById(R.id.taskInstructions);
        startTaskButton = (Button) findViewById(R.id.start_button);
        finishTaskButton = (Button) findViewById(R.id.finish_button);

        taskInstructions.setText(task.name);

        scopeDetails = (TextView) findViewById(R.id.scope_description);
        scopeDetails.setText(Activity.GetScopeOfWork(task.activityID, shiftID));

        if (task.getStartTime() != null) {
            startTaskButton.setEnabled(false);
            finishTaskButton.setEnabled(true);
        } else {
            startTaskButton.setEnabled(true);
            finishTaskButton.setEnabled(false);
        }

        maintenanceButton = (Button) findViewById(R.id.maintenance_button);

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_install;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Install";
    }

    private void MaintenanceDue() {
        if (!MaintenanceCheck.CheckLatestTime(taskID, false)) {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        } else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MaintenanceDue();
    }

    public void startTask(View view) {
        Task.StartTask(task.taskID);
        startTaskButton.setEnabled(false);
        finishTaskButton.setEnabled(true);
        taskInstructions.setVisibility(View.VISIBLE);
    }

    public void finishTask(View view) {
        Task.EndTask(task.taskID);
        finish();

        /*if (task.taskOrder < taskCount) {
            //Load Next Activity
            task = Task.GetTaskFromActivity(task.activityID);

            TaskSelection(task, taskCount, jobPackID);
        }*/
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view) {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }

//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }
}
