package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Briefing.SelectBriefingActivity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivitySignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.Contact;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.Signature;

public class TaskDisplayActivity extends FMConwayActivity {
    private String shiftID;
    private String activityID;
    private String jobPackID;
    private String userID;
    private trianglesoftware.fmconway.Database.DatabaseObjects.Activity activity;
    private String activityName;

    private SharedPreferences sp;
    private boolean signaturesActivityLaunched;

    private TextView clientReferenceNumberTextView;
    private TextView customerTextView;
    private TextView customerPhoneNumberTextView;

    private ListView contactsList;
    private TaskDisplayContactAdapter contactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityID = extras.getString("ActivityID", "");
            jobPackID = extras.getString("JobPackID", "");
            shiftID = extras.getString("ShiftID", "");
            activityName = extras.getString("ActivityName");
            userID = extras.getString("UserID", "");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        activity = trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetActivity(activityID);

        clientReferenceNumberTextView = findViewById(R.id.client_reference_number);
        customerTextView = findViewById(R.id.customer_name);
        customerPhoneNumberTextView = findViewById(R.id.customer_phone_number);

        if (activity.clientReferenceNumber != null) {
            clientReferenceNumberTextView.setText(activity.clientReferenceNumber);
        }

        if (activity.customer != null) {
            customerTextView.setText(activity.customer);
        }

        if (activity.customerPhoneNumber != null) {
            customerPhoneNumberTextView.setText(activity.customerPhoneNumber);
        }

        contactsList = (ListView) findViewById(R.id.contacts_list);
        ViewCompat.setNestedScrollingEnabled(contactsList,true);
        contactsAdapter = new TaskDisplayContactAdapter(this, getLayoutInflater());
        contactsList.setAdapter(contactsAdapter);

        LinearLayout healthAndSafetyLayout = (LinearLayout) findViewById(R.id.health_and_safety_layout);

        // crew member restrictions
        User user = User.getUser(sp.getString("UserID", ""));
        if (Objects.equals(user.employeeType, "Crew Member")) {
            healthAndSafetyLayout.setVisibility(View.GONE);
        }

        // health and safety complete
        boolean hasConfirmedOrNoBriefings = SelectBriefingActivity.CheckBriefingsConfirmed(shiftID) || Briefing.GetBriefingsForShift(shiftID).size() == 0;

        if (hasConfirmedOrNoBriefings) {
            Button healthAndSafetyButton = findViewById(R.id.health_and_safety_button);
            healthAndSafetyButton.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.fmconway_blue));
            healthAndSafetyButton.setTextColor(getApplicationContext().getResources().getColor(R.color.fmconway_text_light));
        }

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskDisplayActivity");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        UpdateShiftCompletionStatus();

        // when resume from signatures activity if all signed and health & safety complete finish
        if (signaturesActivityLaunched) {
            signaturesActivityLaunched = false;

            boolean allSignaturesSigned = true;
            List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
            for (int i = 0; i < staffToSign.size(); i++) {
                if (!ActivitySignature.CheckActivtySignatureExists(activityID, staffToSign.get(i).staffID)) {
                    allSignaturesSigned = false;
                }
            }

            if (allSignaturesSigned) {
                boolean hasConfirmedOrNoBriefings = SelectBriefingActivity.CheckBriefingsConfirmed(shiftID) || Briefing.GetBriefingsForShift(shiftID).size() == 0;
                if (hasConfirmedOrNoBriefings) {
                    finish();
                }
            }
        }
    }

    // ShiftCompletionStatus is also updated in StartActivity restore but as health and safety has been moved from main navigation to this activity
    // we should not finish this activity when signed and allow the user to complete the health safety (which requires shift progress to be updated)
    private void UpdateShiftCompletionStatus() {
        // shift activity complete
        List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
        List<trianglesoftware.fmconway.Database.DatabaseObjects.Activity> activities = trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetActivitiesForShift(shiftID);
        int staffSigned = 0;
        boolean allSigned = true;
        for (int i = 0; i < staffToSign.size(); i++) {
            for (int j = 0; j < activities.size(); j++) {
                if (ActivitySignature.CheckActivtySignatureExists(activities.get(j).activityID, staffToSign.get(i).staffID)) {
                    staffSigned += 1;
                }
            }
        }
        if (staffSigned < (staffToSign.size() * activities.size())) {
            allSigned = false;
        }

        // people complete
        List<ShiftStaff> shiftStaff = ShiftStaff.GetShiftStaff(shiftID);
        boolean staffConfirmed = true;
        for (int i = 0; i < shiftStaff.size(); i++) {
            if (shiftStaff.get(i).startTime == null) {
                staffConfirmed = false;
            }
        }

        if (staffConfirmed) {
            Shift.SetCompletionStatus(shiftID, 5);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_task_display;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return activityName;
    }

    private void GetData() throws Exception {
        List<Contact> contacts = Contact.getAllForActivity(activityID);

        // disable focus to stop ScrollView from scrolling listview into view when populated
        contactsList.setFocusable(false);
        contactsAdapter.UpdateData(contacts);
        contactsList.setFocusable(true);
    }

    public void healthAndSafetyClick(View view) {
        try {
            boolean bypassChecks = getResources().getBoolean(R.bool.bypass_health_and_safety_checks);
            int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);

            if (bypassChecks || shiftStatus > 4) { // >3
                boolean loadingSheetsConfirmed = true;

                if (bypassChecks || loadingSheetsConfirmed) {
                    //Load list of job packs then go from there to briefings
                    /*Intent selectJobActivity = new Intent(getApplicationContext(), SelectJobPackActivity.class);
                    selectJobActivity.putExtra("ShiftID", shiftID);
                    selectJobActivity.putExtra("UserID", userID);
                    selectJobActivity.putExtra("FromButton", 1);
                    startActivity(selectJobActivity);*/

                    JobPack jobPack = JobPack.GetJobPacksForShift(shiftID).get(0);

                    Intent selectBriefingActivity = new Intent(getApplicationContext(), SelectBriefingActivity.class);
                    selectBriefingActivity.putExtra("JobPackID", jobPack.jobPackID);
                    selectBriefingActivity.putExtra("ShiftID", shiftID);
                    selectBriefingActivity.putExtra("UserID", userID);
                    startActivity(selectBriefingActivity);
                } else {
                    Toast.makeText(getApplicationContext(), "Please view and confirm all loading sheets before proceeding", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Please confirm staff members in the people section", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "SafetyFirstClick");
        }
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void confirmClick(View view) {
        boolean allSignaturesSigned = true;

        List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
        for (int i = 0; i < staffToSign.size(); i++) {
            if (!ActivitySignature.CheckActivtySignatureExists(activityID, staffToSign.get(i).staffID)) {
                allSignaturesSigned = false;

                //Load Signature Intent for every staff member in shift
                Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
                signatureIntent.putExtra("ActivityID", activityID);
                signatureIntent.putExtra("StaffID", staffToSign.get(i).staffID);
                signatureIntent.putExtra("LoggedIn", sp.getString("UserID", ""));
                signatureIntent.putExtra("ShiftID", shiftID);
                startActivity(signatureIntent);

                signaturesActivityLaunched = true;
            }
        }

        if (allSignaturesSigned) {
            boolean hasConfirmedOrNoBriefings = SelectBriefingActivity.CheckBriefingsConfirmed(shiftID) || Briefing.GetBriefingsForShift(shiftID).size() == 0;
            if (hasConfirmedOrNoBriefings) {
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Shift Activity already confirmed & signed.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
