package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class TrafficCountActivity extends FMConwayActivity {
    private String taskID;
    private int taskCount;
    private String shiftID;
    private String jobPackID;
    private Task task;
    private TextView timeRemaining;
    private TextView calculatedValue;
    private TextView calculatedValueList;
    private EditText trafficCount;
    private Button nextTaskButton;
    private Button saveCountButton;
    private CountDownTimer timer;
    private LinearLayout timeLayout;
    private LinearLayout detailsLayout;
    private TextView scopeDetails;
    private Button maintenanceButton;
    private Button startTimerButton;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            jobPackID = extras.getString("JobPackID","");
            taskID = extras.getString("TaskID","");
            taskCount = extras.getInt("TaskCount");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        task = Task.GetTask(taskID);

        trafficCount = (EditText)findViewById(R.id.traffic_count);
        timeRemaining = (TextView)findViewById(R.id.timer);
        calculatedValue = (TextView)findViewById(R.id.calculated_value);
        calculatedValueList = (TextView)findViewById(R.id.list_calculated_value);
        nextTaskButton = (Button)findViewById(R.id.next_button);
        saveCountButton = (Button)findViewById(R.id.saveCount_button);
        startTimerButton = (Button)findViewById(R.id.startTimer);

        timeLayout = (LinearLayout)findViewById(R.id.timer_layout);
        detailsLayout = (LinearLayout)findViewById(R.id.detail_layout);

        scopeDetails = (TextView)findViewById(R.id.scope_description);

        scopeDetails.setText(Activity.GetScopeOfWork(task.activityID, shiftID));

        //String calc = (String) calculatedValue.getText();
        //if(Objects.equals(calc,"")) {
        //    nextTaskButton.setEnabled(false);
        //    trafficCount.setEnabled(false);
        //    saveCountButton.setEnabled(false);
        //}
        //else{
        //    timeLayout.setVisibility(View.INVISIBLE);
        //    detailsLayout.setVisibility(View.INVISIBLE);
        //}

        String existingDetails = Task.GetTaskDetails(taskID);

        //if (!Objects.equals(existingDetails, ""))
        //{
        //    nextTaskButton.setEnabled(true);
        //}


        nextTaskButton.setEnabled(true);
        //nextTaskButton.setBackgroundColor(Color.parseColor("#41ab45"));

        //trafficCount.setEnabled(true);
        timer = new CountDownTimer(180000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = (millisUntilFinished / 1000);
                int totalMins = (int)(seconds / 60);
                int totalSeconds = (int)(seconds - (totalMins * 60));
                timeRemaining.setText("0"+totalMins + ":" + (totalSeconds < 10 ? "0" + totalSeconds : totalSeconds));
            }

            @Override
            public void onFinish() {
                trafficCount.setEnabled(true);
                saveCountButton.setEnabled(true);
                timeRemaining.setText("00:00");
                saveCountButton.setBackgroundColor(getResources().getColor(R.color.fmconway_green));
            }
        };

        trafficCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tCountString = trafficCount.getText().toString();
                if(!tCountString.equals("")) {
                    long tCount = Long.parseLong(tCountString);

                    if (tCount > 10000)
                    {
                        Toast.makeText(getApplicationContext(),"Please enter a traffic count below 10000.",Toast.LENGTH_LONG).show();
                        calculatedValue.setText("");
                    }
                    else {
                        tCount = tCount * 20;
                        calculatedValue.setText(String.valueOf(tCount));
                    }
                }
                else
                {
                    calculatedValue.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        maintenanceButton = (Button)findViewById(R.id.maintenance_button);

        updateTrafficCountList();

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_traffic_count;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Traffic Count";
    }

    private void MaintenanceDue()
    {
        if(!MaintenanceCheck.CheckLatestTime(taskID, false))
        {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        }
        else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        MaintenanceDue();
    }

    public void startTimer(View view)
    {
        timer.start();
        trafficCount.setEnabled(true);
        saveCountButton.setEnabled(true);
        startTimerButton.setEnabled(false);
    }

    public void resetTimer(View view)
    {
        saveExistingCount();

        startTimerButton.setEnabled(true);

        timer.cancel();
        trafficCount.setText("");
        calculatedValue.setText("");
        trafficCount.setEnabled(false);
        saveCountButton.setEnabled(false);
        timeRemaining.setText("03:00");
        saveCountButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));

        timer = new CountDownTimer(180000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = (millisUntilFinished / 1000);
                int totalMins = (int)(seconds / 60);
                int totalSeconds = (int)(seconds - (totalMins * 60));
                timeRemaining.setText("0"+totalMins + ":" + (totalSeconds < 10 ? "0" + totalSeconds : totalSeconds));
            }

            @Override
            public void onFinish() {
                saveCountButton.setEnabled(true);
                saveCountButton.setBackgroundColor(getResources().getColor(R.color.fmconway_green));
                timeRemaining.setText("00:00");
                trafficCount.setEnabled(true);
            }
        };
        updateTrafficCountList();
    }

//    public void startTask(View view)
//    {
//        Task.StartTask(task.getTaskID());
//        startTaskButton.setEnabled(false);
//        finishTaskButton.setEnabled(true);
//        timeLayout.setVisibility(View.VISIBLE);
//        detailsLayout.setVisibility(View.VISIBLE);
//    }

    public void saveCount(View view)
    {
        if (!Objects.equals(calculatedValue.getText().toString(), "")) {
            String existingDetails = Task.GetTaskDetails(taskID);

            Task.StoreTrafficCount(task.activityID, calculatedValue.getText().toString());

            if (Objects.equals(existingDetails, "")) {
                Task.StoreTaskDetails(task.taskID, "Traffic Count Value: " + calculatedValue.getText().toString());
            } else // Already exists so append with new count
            {
                String newDetails = existingDetails + ". Next Traffic Count Value: " + calculatedValue.getText().toString();

                Task.StoreTaskDetails(task.taskID, newDetails);
            }

            nextTaskButton.setBackgroundColor(getResources().getColor(R.color.fmconway_green));

            Toast.makeText(getApplicationContext(), "Traffic count saved", Toast.LENGTH_LONG).show();

            //nextTaskButton.setEnabled(true);
            //nextTaskButton.setBackgroundColor(Color.parseColor("#41ab45"));
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please enter a traffic count value", Toast.LENGTH_LONG).show();
        }

        String countString = "";
        if (!Objects.equals(Task.GetTaskDetails(taskID), ""))  {
            String[] countList = Task.GetTaskDetails(taskID).split("\\.");

            for (String item : countList) {
                countString = countString + item + System.getProperty("line.separator");
            }
            updateTrafficCountList();
        }
    }

    public void saveExistingCount()
    {
        if (!Objects.equals(calculatedValue.getText().toString(), "")) {
            String existingDetails = Task.GetTaskDetails(taskID);

            Task.StoreTrafficCount(task.activityID, calculatedValue.getText().toString());

            if (Objects.equals(existingDetails, "")) {
                Task.StoreTaskDetails(task.taskID, "Traffic Count Value: " + calculatedValue.getText().toString());

                Toast.makeText(getApplicationContext(), "Traffic count saved.", Toast.LENGTH_LONG).show();
            } else // Already exists so append with new count
            {
                String newDetails = existingDetails + ". Next Traffic Count Value: " + calculatedValue.getText().toString();

                Task.StoreTaskDetails(task.taskID, newDetails);

                Toast.makeText(getApplicationContext(), "Additional traffic count saved.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void nextTask(View view)
    {
        String existingDetails = Task.GetTaskDetails(taskID);
        
        if(Objects.equals(existingDetails, "")) {

            Toast.makeText(getApplicationContext(), "Please complete and save the Traffic Count", Toast.LENGTH_LONG).show();

//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setCancelable(true);
//            builder.setTitle("Confirmation");
//            builder.setMessage("Are you sure you want to skip the traffic count?");
//            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Task.StoreTrafficCount(task.activityID, "0");
//                    Task.StoreTaskDetails(task.getTaskID(), "Traffic Count Skipped");
//                    nextTaskContinue();
//                }
//            });
//            builder.setNegativeButton("Cancel",null);
//
//            AlertDialog dialog = builder.create();
//            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            dialog.show();
        } else {
            nextTaskContinue();
        }
    }
    
    public void nextTaskContinue() {
        Task.EndTask(task.taskID);
        finish();

        //Load Next Activity
        /*if (task.getTaskOrder() < taskCount) {
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/
    }

    public void drawingClick(View view)
    {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view)
    {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID",""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view)
    {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }
//
//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }

    public void updateTrafficCountList(){
        String countString = "";
        if (!Objects.equals(Task.GetTaskDetails(taskID), ""))  {
            String[] countList = Task.GetTaskDetails(taskID).split("\\.");

            for (String item : countList) {
                countString = countString + item + System.getProperty("line.separator");
            }
            calculatedValueList.setText(countString.trim());
        }
    }

}
