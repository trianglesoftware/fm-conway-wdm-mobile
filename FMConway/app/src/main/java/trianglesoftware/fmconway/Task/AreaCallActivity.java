package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Area;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class AreaCallActivity extends FMConwayActivity {
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private int taskCount;
    private Task task;
    private Area area;
    private EditText details;
    private EditText personsName;
    private EditText reference;
    private Button nextTaskButton;
    private TextView scopeDetails;
    private Button maintenanceButton;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            jobPackID = extras.getString("JobPackID","");
            shiftID = extras.getString("ShiftID","");
            taskID = extras.getString("TaskID","");
            taskCount = extras.getInt("TaskCount");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        task = Task.GetTask(taskID);
        area = Area.GetArea(task.areaID);

        TextView areaText = (TextView) findViewById(R.id.area);
        TextView callProtocol = (TextView) findViewById(R.id.call_protocol);
        TextView trafficCount = (TextView) findViewById(R.id.traffic_count) ;
        details = (EditText)findViewById(R.id.details_area);
        personsName = (EditText)findViewById(R.id.person_area);
        reference = (EditText)findViewById(R.id.reference_area);
        nextTaskButton = (Button)findViewById(R.id.next_button);

        trafficCount.setText("Traffic count per hour: " + task.trafficCount); // Test storing traffic count in db

        if (area != null) {
            areaText.setText(area.name);
        }
        //callProtocol.setText(area.getCallProtocol());
        String callProtocolText = Activity.GetAreaCallProtocolForActivity(task.getActivityID());

        if (callProtocolText != "" || callProtocolText != null) {
            callProtocol.setText(callProtocolText);
        }
        else
        {
            callProtocol.setVisibility(View.INVISIBLE);
        }

        reference.setText(Area.GetAreaReference(task.activityID));

        scopeDetails = (TextView)findViewById(R.id.scope_description);
        scopeDetails.setText(Activity.GetScopeOfWork(task.activityID, shiftID));

        if(task.getStartTime() != null) {
            nextTaskButton.setEnabled(true);
            details.setEnabled(true);
        }
        else{
            details.setEnabled(false);
        }

        maintenanceButton = (Button)findViewById(R.id.maintenance_button);

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_area_call;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Area Call";
    }

    private void MaintenanceDue()
    {
        if(!MaintenanceCheck.CheckLatestTime(taskID, false))
        {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        }
        else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        MaintenanceDue();
    }

//    public void startTask(View view)
//    {
//        Task.StartTask(task.getTaskID());
//        startTaskButton.setEnabled(false);
//        finishTaskButton.setEnabled(true);
//        details.setEnabled(true);
//    }

    public void nextTask(View view)
    {
        if (checkIfComplete()) {
            Task.EndTask(task.taskID);
            Area.UpdateDetails(area.areaID, details.getText().toString(), personsName.getText().toString(), reference.getText().toString());
            finish();

            if (checkIfFromClosureInstalled()) {
                if (task.taskOrder < taskCount) {
                    //Load Next Activity
                    task = Task.GetTaskFromActivity(task.activityID);

                    TaskSelection(task, taskCount, jobPackID);
                }

                Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
                selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
                selectMaintenanceActivity.putExtra("ShiftID", shiftID);
                selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(selectMaintenanceActivity);
                finish();
            }
            else {
                if (task.taskOrder < taskCount) {
                    //Load Next Activity
                    task = Task.GetTaskFromActivity(task.activityID);

                    TaskSelection(task, taskCount, jobPackID);
                }
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please ensure that all fields are completed before proceeding", Toast.LENGTH_LONG).show();
        }
    }

    public boolean checkIfComplete()
    {
        if (Objects.equals(personsName.getText().toString(), "") || Objects.equals(reference.getText().toString(), ""))
        {
            return false;
        }
        else {
            return true;
        }
    }

    public boolean checkIfFromClosureInstalled()
    {
        try {
            int previousTaskOrder = task.taskOrder - 1;
            String activityID = task.activityID;

            Task previousTask = Task.GetTaskFromActivityAndOrder(activityID, previousTaskOrder);
            if (previousTask != null) {
                String name = previousTask.getName();

                if (Objects.equals(name, "Closure Installed"))
                //if (Objects.equals(name,"Check Site (Post Install).  Then carry out a maintenance check"))
                {
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "CheckIfFromClosureInstalled");
        }

        return false;
    }

    public void drawingsClick(View view)
    {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view)
    {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view)
    {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }
//
//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }
}
