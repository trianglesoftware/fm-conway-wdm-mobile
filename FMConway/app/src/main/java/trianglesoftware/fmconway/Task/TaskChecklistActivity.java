package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseHelper;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskChecklistAnswer;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class TaskChecklistActivity extends FMConwayActivity {
    private String taskChecklistID;
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private int taskCount;
    private String userID;
    private String jobType;
    private ListView taskChecklistQuestionList;
    private TaskChecklistQuestionAdapter taskChecklistQuestionAdapter;
    private DatabaseHelper db;
    private Button nextTaskButton;
    private Task task;
    private TextView scopeDetails;
    private TextView messageTextVew;
    private Button riskAssessmentButton;
    private Button maintenanceButton;
    private Button home;
    private Button back;
    private SharedPreferences sp;

    private String checklistID;
    private boolean isRiskAssessmentChecklist;
    private boolean isBatteryChecklist;

    boolean yesHasReason;
    boolean noHasReason;
    boolean naHasReason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            jobPackID = extras.getString("JobPackID", "");
            taskChecklistID = extras.getString("TaskChecklistID", "");
            checklistID = extras.getString("ChecklistID", "");
            taskID = extras.getString("TaskID", "");
            taskCount = extras.getInt("TaskCount");
            userID = extras.getString("UserID", "");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        Checklist checklist = Checklist.getChecklist(checklistID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        isRiskAssessmentChecklist = Checklist.isRiskAssessmentChecklist(checklistID);
        isBatteryChecklist = Checklist.isBatteryChecklist(checklistID);

        taskChecklistQuestionList = (ListView) findViewById(R.id.task_checklist_list);
        taskChecklistQuestionAdapter = new TaskChecklistQuestionAdapter(this, getLayoutInflater());

        //taskChecklistQuestionAdapter.isRiskAssessmentChecklist = isRiskAssessmentChecklist;
        //taskChecklistQuestionAdapter.isBatteryChecklist = isBatteryChecklist;

        if (checklist.isJettingPermitToWork || checklist.isJettingRiskAssessment ||
                checklist.isTankeringPermitToWork || checklist.isTankeringRiskAssessment ||
                checklist.isCCTVRiskAssessment || checklist.isJettingNewRiskAssessment) {
            taskChecklistQuestionAdapter.isYesNoNaChecklist = true;
            noHasReason = true;
        }

        taskChecklistQuestionList.setAdapter(taskChecklistQuestionAdapter);

        task = Task.GetTask(taskID);

        nextTaskButton = (Button) findViewById(R.id.next_button);
        home = (Button) findViewById(R.id.back_to_home);
        back = (Button) findViewById(R.id.back);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(false);
            }
        });

        scopeDetails = (TextView) findViewById(R.id.scope_description);
        scopeDetails.setText(Activity.GetScopeOfWork(task.activityID, shiftID));

        if (task.getStartTime() != null) {
            nextTaskButton.setEnabled(true);
            taskChecklistQuestionList.setVisibility(View.VISIBLE);
        } else {
            taskChecklistQuestionList.setVisibility(View.INVISIBLE);
        }

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskChecklistActivity");
        }

        messageTextVew = findViewById(R.id.message);
        riskAssessmentButton = findViewById(R.id.riskAssessmentButton);

        if (checklist.isJettingRiskAssessment || checklist.isTankeringRiskAssessment || checklist.isCCTVRiskAssessment) {
            messageTextVew.setVisibility(View.VISIBLE);
            riskAssessmentButton.setEnabled(false);
        } else {
            messageTextVew.setVisibility(View.GONE);
            riskAssessmentButton.setEnabled(true);
        }

        maintenanceButton = (Button) findViewById(R.id.maintenance_button);

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_task_checklist;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Checklist";
    }

    private void MaintenanceDue() {
        if (!MaintenanceCheck.CheckLatestTime(taskID, false)) {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        } else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MaintenanceDue();
    }

    private void GetData() throws Exception {
        List<TaskChecklistAnswer> taskChecklistAnswers = TaskChecklistAnswer.GetTaskChecklistAnswers(taskChecklistID);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < taskChecklistAnswers.size(); i++) {
            jsonArray.put(taskChecklistAnswers.get(i).getJSONObject());
        }

        taskChecklistQuestionAdapter.UpdateData(jsonArray);
    }

    //Standard checklist answers
    public void answerYes(View view) {
        int pos = taskChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = taskChecklistQuestionAdapter.getItem(pos);

        TaskChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), taskChecklistID, true);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskChecklistActivity");
        }
    }

    public void answerNo(View view) {
        int pos = taskChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = taskChecklistQuestionAdapter.getItem(pos);

        TaskChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), taskChecklistID, false);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskChecklistActivity");
        }
    }

    //RiskAssessment checklist answers
    public void riskAssessmentAnswer(View view) {
        int pos = taskChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = taskChecklistQuestionAdapter.getItem(pos);

        String taskChecklistAnswerID = selected.optString("TaskChecklistAnswerID");
        int answer = (int) view.getTag();

        TaskChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), taskChecklistID, answer);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskChecklistActivity");
        }

//        if (answer == 0) {
        // answer = high
        Intent intent = new Intent(getApplicationContext(), TaskChecklistPhotoActivity.class);
        intent.putExtra("TaskChecklistAnswerID", taskChecklistAnswerID);
        intent.putExtra("ActivityID", task.activityID);
        intent.putExtra("ShiftID", shiftID);

        startActivity(intent);
//        } else {
        // answer = low or medium
//            TaskChecklistAnswer.UpdateReason(taskChecklistAnswerID, "");
//
//            List<TaskChecklistAnswerPhoto> photos = TaskChecklistAnswerPhoto.getPhotosForTaskChecklistAnswer(taskChecklistAnswerID);
//            for (int i = 0; i < photos.size(); i++) {
//                TaskChecklistAnswerPhoto.deleteTaskChecklistAnswerPhotoFile(photos.get(i).taskChecklistAnswerPhotoID);
//                TaskChecklistAnswerPhoto.deleteTaskChecklistAnswerPhoto(photos.get(i).taskChecklistAnswerPhotoID);
//            }
//        }
    }

    // YesNoNa Extended Checklist Answer
    public void extendedAnswer(View view) {
        int pos = taskChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = taskChecklistQuestionAdapter.getItem(pos);

        String taskChecklistAnswerID = selected.optString("TaskChecklistAnswerID");
        int answer = (int) view.getTag();

        TaskChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), taskChecklistID, answer);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "TaskChecklistActivity");
        }

        if ((yesHasReason && answer == 1) || (noHasReason && answer == 0) || (naHasReason && answer == 2)) {
            Intent intent = new Intent(getApplicationContext(), TaskChecklistPhotoActivity.class);
            intent.putExtra("TaskChecklistAnswerID", taskChecklistAnswerID);
            intent.putExtra("ActivityID", task.activityID);
            intent.putExtra("ShiftID", shiftID);

            startActivity(intent);
        }
    }

    public void nextTask(View view) {
        if (!CheckComplete()) {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding.", Toast.LENGTH_LONG).show();
            return;
        }

        if (isRiskAssessmentChecklist) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Confirmation");
            //builder.setMessage("");
            builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    nextTaskContinue();
                }
            });
            builder.setNegativeButton("Not Proceed", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //if you reset all answers, all photos and reasons will need deleting too
                    //TaskChecklistAnswer.resetAllAnswers(taskChecklistID);
                    //
                    //try {
                    //    GetData();
                    //}
                    //catch (Exception e) {
                    //    ErrorLog.CreateError(e, "TaskChecklistActivity");
                    //}
                }
            });

            AlertDialog dialog = builder.create();
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.show();

            return;
        }

        nextTaskContinue();
    }

    private void nextTaskContinue() {
        Task.EndTask(task.taskID);

        finish();

        /*if (task.getTaskOrder() < taskCount) {
            //Load Next Activity
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view) {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }
//
//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }

    private boolean CheckComplete() {
        List<TaskChecklistAnswer> taskChecklistAnswers = TaskChecklistAnswer.GetTaskChecklistAnswers(taskChecklistID);

        boolean complete = true;

        for (int i = 0; i < taskChecklistAnswers.size(); i++) {
            complete = !taskChecklistAnswers.get(i).isNew;
        }

        return complete;
    }

    @Override
    public void onBackPressed() {
        if (CheckComplete()) {

            finish();
        } else {
            //Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            TaskChecklistAnswer.deleteAllTaskChecklistAnswersForCheck(taskChecklistID);
                            TaskChecklist.deleteTaskChecklist(taskChecklistID);

                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("Continue to cancel checklist?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }

    private void returnToMenu(final boolean menu) {
        if (CheckComplete()) {

            if (menu) {
                if (Objects.equals(shiftID, "")) {
                    Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                    noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    noShiftIntent.putExtra("ShiftID", shiftID);
                    noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(noShiftIntent);
                } else {
                    Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                    startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startIntent.putExtra("ShiftID", shiftID);
                    startIntent.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(startIntent);
                }
            } else {
                finish();
            }
        } else {
            //Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            TaskChecklistAnswer.deleteAllTaskChecklistAnswersForCheck(taskChecklistID);
                            TaskChecklist.deleteTaskChecklist(taskChecklistID);

                            if (menu) {
                                if (Objects.equals(shiftID, "")) {
                                    Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                                    noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    noShiftIntent.putExtra("ShiftID", shiftID);
                                    noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                                    startActivity(noShiftIntent);
                                } else {
                                    Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                                    startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startIntent.putExtra("ShiftID", shiftID);
                                    startIntent.putExtra("UserID", sp.getString("UserID", ""));
                                    startActivity(startIntent);
                                }
                            } else {
                                finish();
                            }

                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("Continue to cancel checklist?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }
}
