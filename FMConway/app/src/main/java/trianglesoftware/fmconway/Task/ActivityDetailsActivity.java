package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityPriority;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class ActivityDetailsActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private int taskCount;
    private String userID;

    private Task task;
    private Activity activity;
    private String jobType;

    // inputs
    private TextView siteNameLabel;
    private EditText siteNameEditText;
    private TextView clientReferenceNumberLabel;
    private EditText clientReferenceNumberEditText;
    private TextView activityPriorityLabel;
    private Spinner activityPrioritySpinner;
    private TextView operatorLabel;
    private EditText operatorEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID", "");
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        taskCount = extras.getInt("TaskCount");
        userID = extras.getString("UserID", "");

        super.onCreate(savedInstanceState);

        task = Task.GetTask(taskID);
        activity = Activity.GetActivity(task.activityID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        // inputs
        siteNameLabel = findViewById(R.id.site_name_label);
        siteNameEditText = findViewById(R.id.site_name);
        clientReferenceNumberLabel = findViewById(R.id.client_reference_number_label);
        clientReferenceNumberEditText = findViewById(R.id.client_reference_number);
        activityPriorityLabel = findViewById(R.id.activity_priority_label);
        activityPrioritySpinner = findViewById(R.id.activity_priority);
        operatorLabel = findViewById(R.id.operator_label);
        operatorEditText = findViewById(R.id.operator);

        // activity details is a generic editor which can be customised for any workflow
        // hide all input fields as default
        siteNameLabel.setVisibility(View.GONE);
        siteNameEditText.setVisibility(View.GONE);
        clientReferenceNumberLabel.setVisibility(View.GONE);
        clientReferenceNumberEditText.setVisibility(View.GONE);
        activityPriorityLabel.setVisibility(View.GONE);
        activityPrioritySpinner.setVisibility(View.GONE);
        operatorLabel.setVisibility(View.GONE);
        operatorEditText.setVisibility(View.GONE);

        // jetting and tankering workflow
        if (jobType.equals("Jetting") || jobType.equals("Tankering")) {
            siteNameLabel.setVisibility(View.VISIBLE);
            siteNameEditText.setVisibility(View.VISIBLE);
            clientReferenceNumberLabel.setVisibility(View.VISIBLE);
            clientReferenceNumberEditText.setVisibility(View.VISIBLE);
            activityPriorityLabel.setVisibility(View.VISIBLE);
            activityPrioritySpinner.setVisibility(View.VISIBLE);
            operatorLabel.setVisibility(View.VISIBLE);
            operatorEditText.setVisibility(View.VISIBLE);

            List<ActivityPriority> activityPriorities = ActivityPriority.getLookupItems(activity.activityPriorityID);
            ArrayAdapter<ActivityPriority> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, activityPriorities);
            activityPrioritySpinner.setAdapter(adapter);

            // set values
            if (activity.siteName != null) {
                siteNameEditText.setText(activity.siteName);
            }

            if (activity.clientReferenceNumber != null) {
                clientReferenceNumberEditText.setText(activity.clientReferenceNumber);
            }

            if (activity.activityPriorityID != null) {
                for (int i = 0; i < activityPriorities.size(); i++) {
                    if (Objects.equals(activityPriorities.get(i).activityPriorityID, activity.activityPriorityID)) {
                        activityPrioritySpinner.setSelection(i);
                    }
                }
            }

            if (activity.operator != null) {
                operatorEditText.setText(activity.operator);
            }
        }

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_activity_details_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Activity Details";
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public boolean validate() {
        if (jobType.equals("Jetting") || jobType.equals("Tankering")) {
            String siteName = siteNameEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(siteName)) {
                Toast.makeText(getApplicationContext(), "Site Name is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            ActivityPriority activityPriority = (ActivityPriority) activityPrioritySpinner.getSelectedItem();
            if (activityPriority.activityPriorityID == null) {
                Toast.makeText(getApplicationContext(), "Activity Priority is a required field", Toast.LENGTH_LONG).show();
                return false;
            }

            String operator = operatorEditText.getText().toString();
            if (FMConwayUtils.isNullOrWhitespace(operator)) {
                Toast.makeText(getApplicationContext(), "Operator is a required field", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    public void save() {
        if (jobType.equals("Jetting") || jobType.equals("Tankering")) {
            Activity activity = Activity.GetActivity(task.activityID);

            activity.siteName = siteNameEditText.getText().toString();
            activity.clientReferenceNumber = clientReferenceNumberEditText.getText().toString();

            ActivityPriority activityPriority = (ActivityPriority) activityPrioritySpinner.getSelectedItem();
            activity.activityPriorityID = activityPriority.activityPriorityID;

            activity.operator = operatorEditText.getText().toString();

            Activity.saveActivityDetails(activity);
        }

        Task.EndTask(task.taskID);

        // load next activity
        /*if (task.taskOrder < taskCount) {
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/

        finish();
    }

    public void confirmClick(View view) {
        if (!validate()) {
            return;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Continue");
        alertDialog.setMessage("Please confirm you have completed this task.");

        alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                save();
            }
        });

        alertDialog.setNegativeButton("Cancel", null);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }
}
