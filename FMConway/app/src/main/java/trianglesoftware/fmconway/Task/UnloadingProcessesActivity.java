package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcess;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class UnloadingProcessesActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private int taskCount;
    private String userID;

    private Task task;
    private Activity activity;
    private String jobType;

    // inputs
    private ListView unloadingProcessesList;
    private UnloadingProcessesAdapter unloadingProcessesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID", "");
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        taskCount = extras.getInt("TaskCount");
        userID = extras.getString("UserID", "");

        super.onCreate(savedInstanceState);

        task = Task.GetTask(taskID);
        activity = Activity.GetActivity(task.activityID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        unloadingProcessesList = findViewById(R.id.unloading_processes_list);
        unloadingProcessesAdapter = new UnloadingProcessesAdapter(this, getLayoutInflater());
        unloadingProcessesList.setAdapter(unloadingProcessesAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_unloading_processes_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Unloading Processes";
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void refreshList() {
        List<UnloadingProcess> unloadingProcesses = UnloadingProcess.getAllForTask(taskID);
        unloadingProcessesAdapter.items = unloadingProcesses;
        unloadingProcessesAdapter.notifyDataSetChanged();
    }

    public boolean validate() {
        // any incomplete unloading processes must be finished
        for (UnloadingProcess unloadingProcess : unloadingProcessesAdapter.items) {
            if (unloadingProcess.leaveDisposalSiteDate == null) {
                Toast.makeText(getApplicationContext(), "Any incomplete unloading processes must be finished", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    public void save() {
        Task.EndTask(task.taskID);

        // load next activity
        /*if (task.taskOrder < taskCount) {
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/

        finish();
    }

    public void addUnloadingProcessClick(View view) {
        // any incomplete unloading processes must be finished
        for (UnloadingProcess unloadingProcess : unloadingProcessesAdapter.items) {
            if (unloadingProcess.leaveDisposalSiteDate == null) {
                Toast.makeText(getApplicationContext(), "Incomplete unloading processes must be finished before adding another", Toast.LENGTH_LONG).show();
                return;
            }
        }

        Intent unloadingProcessActivity = new Intent(getApplicationContext(), UnloadingProcessActivity.class);
        unloadingProcessActivity.putExtra("JobPackID", jobPackID);
        unloadingProcessActivity.putExtra("ShiftID", shiftID);
        unloadingProcessActivity.putExtra("TaskID", taskID);
        unloadingProcessActivity.putExtra("UserID", userID);
        startActivity(unloadingProcessActivity);
    }

    public void confirmClick(View view) {
        if (!validate()) {
            return;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Continue");
        alertDialog.setMessage("Please confirm you have completed this task.");

        alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                save();
            }
        });

        alertDialog.setNegativeButton("Cancel", null);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void rowClick(View view) {
        int pos = unloadingProcessesList.getPositionForView(view);
        UnloadingProcess unloadingProcess = unloadingProcessesAdapter.getItem(pos);

        Intent unloadingProcessActivity = new Intent(getApplicationContext(), UnloadingProcessActivity.class);
        unloadingProcessActivity.putExtra("JobPackID", jobPackID);
        unloadingProcessActivity.putExtra("ShiftID", shiftID);
        unloadingProcessActivity.putExtra("TaskID", taskID);
        unloadingProcessActivity.putExtra("UserID", userID);
        unloadingProcessActivity.putExtra("UnloadingProcessID", unloadingProcess.unloadingProcessID);
        startActivity(unloadingProcessActivity);
    }

    public void removeClick(View view) {
        int pos = unloadingProcessesList.getPositionForView(view);
        final UnloadingProcess unloadingProcess = unloadingProcessesAdapter.getItem(pos);

        FMConwayUtils.confirmDialog(this, "Delete this record, are you sure?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                UnloadingProcess.delete(unloadingProcess.unloadingProcessID);
                refreshList();
            }
        });
    }

    public void drawingClick(View view)
    {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view)
    {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

}
