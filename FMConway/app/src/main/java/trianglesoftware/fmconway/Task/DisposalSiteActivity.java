package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.DisposalSite;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class DisposalSiteActivity extends FMConwayActivity {
    private String shiftID;
    private String selectedDisposalSiteID;
    List<DisposalSiteAdapter.Item> allItems = new LinkedList<>();

    // inputs
    private ListView disposalSiteListView;
    private DisposalSiteAdapter disposalSiteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        shiftID = extras.getString("ShiftID", "");
        selectedDisposalSiteID = extras.getString("DisposalSiteID", "");

        super.onCreate(savedInstanceState);

        // listview
        disposalSiteAdapter = new DisposalSiteAdapter(this, getLayoutInflater());
        disposalSiteListView = findViewById(R.id.disposal_sites_list);
        disposalSiteListView.setAdapter(disposalSiteAdapter);

        // search
        ((SearchView)findViewById(R.id.search_view)).setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filter(s);
                return true;
            }
        });

        findViewById(R.id.search_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchView)findViewById(R.id.search_view)).onActionViewExpanded();
            }
        });

        // load
        load();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_disposal_sites;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Disposal Sites";
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void load() {
        allItems.clear();
        disposalSiteAdapter.items.clear();

        List<DisposalSite> disposalSites = DisposalSite.getAll();
        for (DisposalSite disposalSite : disposalSites) {
            DisposalSiteAdapter.Item item = new DisposalSiteAdapter.Item();
            item.disposalSite = disposalSite;
            item.checked = disposalSite.disposalSiteID.equals(selectedDisposalSiteID);
            item.visible = true;

            allItems.add(item);
            disposalSiteAdapter.items.add(item);
        }

        disposalSiteAdapter.notifyDataSetChanged();
    }

    private void filter(String search) {
        disposalSiteAdapter.items.clear();

        for (DisposalSiteAdapter.Item item : allItems) {
            item.visible = FMConwayUtils.isNullOrWhitespace(search) || item.disposalSite.name.toLowerCase().contains(search.toLowerCase());
            if (item.visible) {
                disposalSiteAdapter.items.add(item);
            }
        }

        disposalSiteAdapter.notifyDataSetChanged();
    }

    public void rowClick(View view) {
        int position = disposalSiteListView.getPositionForView(view);
        DisposalSiteAdapter.Item selectedItem = disposalSiteAdapter.getItem(position);

        disposalSiteAdapter.items.clear();

        for (DisposalSiteAdapter.Item item : allItems) {
            if (item.disposalSite.disposalSiteID.equals(selectedItem.disposalSite.disposalSiteID)) {
                item.checked = !item.checked;
            } else {
                item.checked = false;
            }

            if (item.visible) {
                disposalSiteAdapter.items.add(item);
            }
        }

        disposalSiteAdapter.notifyDataSetChanged();
    }

    public void confirmClick(View view) {
        DisposalSiteAdapter.Item selectedItem = null;
        for (DisposalSiteAdapter.Item item : allItems) {
            if (item.checked && item.visible) {
                selectedItem = item;
                break;
            }
        }

        Intent intent = new Intent();
        intent.putExtra("DisposalSiteID", selectedItem != null ? selectedItem.disposalSite.disposalSiteID : null);
        intent.putExtra("DisposalSiteName", selectedItem != null ? selectedItem.disposalSite.name : null);

        setResult(RESULT_OK, intent);

        finish();
    }

}
