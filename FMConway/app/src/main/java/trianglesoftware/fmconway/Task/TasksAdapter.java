package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

class TasksAdapter extends BaseAdapter {
    private List<Item> items;
    private final LayoutInflater mInflater;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public TasksAdapter(LayoutInflater inflater) {
        items = new LinkedList<>();
        mInflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_task, null);

            viewHolder = new ViewHolder();
            viewHolder.title = convertView.findViewById(R.id.idName);
            viewHolder.isMandatory = convertView.findViewById(R.id.idMandatory);
            viewHolder.isCompleted = convertView.findViewById(R.id.idCompleted);
            viewHolder.date = convertView.findViewById(R.id.idDate);
            viewHolder.complete = convertView.findViewById(R.id.idChipComplete);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Item item = getItem(position);

        // set data
        viewHolder.title.setText(item.name);

        if (item.mandatory) {
            viewHolder.isMandatory.setImageResource(R.drawable.circle_checked);
        } else {
            viewHolder.isMandatory.setImageResource(R.drawable.circle_unchecked);
        }

        viewHolder.isCompleted.setChecked(item.completed);

        if (item.date != null) {
            viewHolder.date.setVisibility(View.VISIBLE);
            viewHolder.date.setText(FMConwayUtils.getLongDateStringWithoutSeconds(item.date));
        } else {
            viewHolder.date.setVisibility(View.GONE);
        }

        if (item.completed) {
            viewHolder.complete.setBackgroundResource(R.drawable.circle_button_complete);
            viewHolder.complete.setText("Completed");
        } else {
            viewHolder.complete.setBackgroundResource(R.drawable.circle_button_incomplete);
            viewHolder.complete.setText("Incomplete");
        }

        return convertView;
    }

    public void updateData(List<Item> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView title;
        public TextView date;
        public ImageView isMandatory;
        public CheckBox isCompleted;
        public Button complete;
    }

    public static class Item {
        public String taskID;
        public String name;
        public String type;
        public Date date;
        public int order;
        public boolean completed;
        public boolean mandatory;
        public double longitude;
        public double latitude;
    }
}
