package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class FreeTextDetailActivity extends FMConwayActivity {
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private String url;
    private int taskCount;
    private Task task;
    private TextView scopeDetails;
    private Button taskCompleteButton;
    private Button maintenanceButton;
    private EditText details;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            jobPackID = extras.getString("JobPackID","");
            taskID = extras.getString("TaskID","");
            taskCount = extras.getInt("TaskCount");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        url = this.getResources().getString(R.string.QUERY_URL);

        task = Task.GetTask(taskID);

        scopeDetails = (TextView)findViewById(R.id.scope_description);
        scopeDetails.setText(trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetScopeOfWork(task.activityID, shiftID));

        taskCompleteButton = (Button)findViewById(R.id.task_complete_button);

        taskCompleteButton.setText(task.name);

        maintenanceButton = (Button)findViewById(R.id.maintenance_button);

        details = (EditText)findViewById(R.id.taskDetails);

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_free_text_detail;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Task";
    }

    private void MaintenanceDue()
    {
        if(!MaintenanceCheck.CheckLatestTime(taskID, false))
        {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        }
        else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        MaintenanceDue();
    }

    public void taskCompleted(View view)
    {
        if (!Objects.equals(details.getText().toString(), "")) {
            Task.EndTask(task.taskID);
            Task.StoreTaskDetails(task.taskID, details.getText().toString());

            /*if (Objects.equals(task.name, "Leave Site"))
            {
                ShiftProgress.SetShiftProgress(shiftID, "Completed");
                ShiftProgress.SyncProgress();
            }*/

            finish();

            /*if (checkIfFromClosureInstalled()) {
                if (task.taskOrder < taskCount) {
                    //Load Next Activity
                    task = Task.GetTaskFromActivity(task.activityID);

                    TaskSelection(task, taskCount, jobPackID);
                }

                Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
                selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
                selectMaintenanceActivity.putExtra("ShiftID", shiftID);
                selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(selectMaintenanceActivity);
                finish();
            }
            else
            {
                if (task.taskOrder < taskCount) {
                    //Load Next Activity
                    task = Task.GetTaskFromActivity(task.activityID);

                    TaskSelection(task, taskCount, jobPackID);
                }
            }*/
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please enter the details.", Toast.LENGTH_SHORT).show();
        }
    }

    public void drawingClick(View view)
    {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view)
    {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view)
    {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }

//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }

    public boolean checkIfFromClosureInstalled()
    {
        try {
            int previousTaskOrder = task.taskOrder - 1;
            String activityID = task.activityID;

            Task previousTask = Task.GetTaskFromActivityAndOrder(activityID, previousTaskOrder);
            if (previousTask != null) {
                String name = previousTask.getName();

                if (Objects.equals(name, "Closure Installed"))
                //if (Objects.equals(name,"Check Site (Post Install).  Then carry out a maintenance check"))
                {
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "CheckIfFromClosureInstalled");
        }

        return false;
    }
}
