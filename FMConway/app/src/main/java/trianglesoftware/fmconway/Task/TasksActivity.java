package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskType;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.Enums.TaskTypeEnum;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;

public class TasksActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String activityID;

    private GridView tasksList;
    private TasksAdapter tasksAdapter;

    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID");
        shiftID = extras.getString("ShiftID");
        activityID = extras.getString("ActivityID");

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        tasksAdapter = new TasksAdapter(getLayoutInflater());
        tasksList = findViewById(R.id.idGridView);
        tasksList.setAdapter(tasksAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_tasks;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Workflow";
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        List<Task> tasks = Task.GetTasksForActivity(activityID);

        List<TasksAdapter.Item> items = new ArrayList<>();
        for (Task task : tasks) {
            TasksAdapter.Item item = new TasksAdapter.Item();
            item.taskID = task.taskID;

            if (TaskTypeEnum.get(task.taskTypeID) == TaskTypeEnum.Checklist) {
                Checklist checklist = Checklist.getChecklist(task.checklistID);
                if (checklist != null) {
                    item.name = checklist.name;
                } else {
                    item.name = task.name;
                }
            } else {
                item.name = task.name;
            }

            item.type = TaskType.get(task.taskTypeID).name;
            item.completed = task.endTime != null;
            item.order = task.taskOrder;
            item.mandatory = task.isMandatory;
            item.date = task.endTime;
            item.longitude = task.longitude;
            item.latitude = task.latitude;

            items.add(item);
        }

        tasksAdapter.updateData(items);
    }

    public void taskClick(View view) {
        int position = tasksList.getPositionForView(view);
        TasksAdapter.Item item = tasksAdapter.getItem(position);

        Task task = Task.GetTask(item.taskID);
        int taskCount = Task.GetTotalTasksForActivity(activityID);

        TaskSelection(task, taskCount, jobPackID);
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

}
