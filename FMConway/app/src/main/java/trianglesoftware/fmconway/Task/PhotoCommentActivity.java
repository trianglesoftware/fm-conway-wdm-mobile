package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

import java.util.Date;
import java.util.Objects;

import trianglesoftware.fmconway.Comment.SelectCommentActivity;
import trianglesoftware.fmconway.Database.DatabaseObjects.CleansingLogPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Note;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.UnloadingProcessPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;


public class PhotoCommentActivity extends FMConwayActivityBase {
    private SharedPreferences sp;

    private String taskPhotoID;
    private String unloadingProcessPhotoID;
    private String cleansingLogPhotoID;

    private EditText commentEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_photo_comment);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        Bundle extras = getIntent().getExtras();
        taskPhotoID = extras.getString("TaskPhotoID", null);
        unloadingProcessPhotoID = extras.getString("UnloadingProcessPhotoID", null);
        cleansingLogPhotoID = extras.getString("CleansingLogPhotoID", null);

        commentEditText = (EditText) findViewById(R.id.comment_detail);

        if (taskPhotoID != null) {
            TaskPhoto taskPhoto = TaskPhoto.getTaskPhoto(taskPhotoID);
            if (taskPhoto.comments != null) {
                commentEditText.setText(taskPhoto.comments);
            }
        } else if (unloadingProcessPhotoID != null) {
            UnloadingProcessPhoto unloadingProcessPhoto = UnloadingProcessPhoto.getUnloadingProcessPhoto(unloadingProcessPhotoID);
            if (unloadingProcessPhoto.comments != null) {
                commentEditText.setText(unloadingProcessPhoto.comments);
            }
        } else if (cleansingLogPhotoID != null) {
            CleansingLogPhoto cleansingLogPhoto = CleansingLogPhoto.getCleansingLogPhoto(cleansingLogPhotoID);
            if (cleansingLogPhoto.comments != null) {
                commentEditText.setText(cleansingLogPhoto.comments);
            }
        }
    }

    public void submitComment(View view) {
        if (taskPhotoID != null) {
            TaskPhoto.saveComments(taskPhotoID, commentEditText.getText().toString());
        } else if (unloadingProcessPhotoID != null) {
            UnloadingProcessPhoto.saveComments(unloadingProcessPhotoID, commentEditText.getText().toString());
        } else if (cleansingLogPhotoID != null) {
            CleansingLogPhoto.saveComments(cleansingLogPhotoID, commentEditText.getText().toString());
        }

        finish();
    }
}
