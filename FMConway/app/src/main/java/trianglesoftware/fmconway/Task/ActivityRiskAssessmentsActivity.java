package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityPriority;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityRiskAssessment;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class ActivityRiskAssessmentsActivity extends FMConwayActivity {
    private String jobPackID;
    private String shiftID;
    private String taskID;
    private int taskCount;
    private String userID;

    private Task task;
    private Activity activity;
    private String jobType;

    // inputs
    private Button confirmButton;
    private Button addRiskAssessmentButton;
    private ListView roadList;
    private ActivityRiskAssessmentAdapter activityRiskAssessmentAdapter;
    private LinearLayout activityRiskAssessmentHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        jobPackID = extras.getString("JobPackID", "");
        shiftID = extras.getString("ShiftID", "");
        taskID = extras.getString("TaskID", "");
        taskCount = extras.getInt("TaskCount");
        userID = extras.getString("UserID", "");

        super.onCreate(savedInstanceState);

        task = Task.GetTask(taskID);
        activity = Activity.GetActivity(task.activityID);
        jobType = Activity.getActivityTypeByTaskID(taskID);

        // inputs

        confirmButton = findViewById(R.id.confirm);
        addRiskAssessmentButton = findViewById(R.id.addRiskAssessment);
        roadList = findViewById(R.id.road_list);
        activityRiskAssessmentHeader = findViewById(R.id.activity_risk_assessment_header);

        activityRiskAssessmentAdapter = new ActivityRiskAssessmentAdapter(this, getLayoutInflater());
        roadList.setAdapter(activityRiskAssessmentAdapter);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_activity_risk_assessment_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Activity Details";
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshList();
    }

    public void refreshList() {
        List<ActivityRiskAssessment> roads = ActivityRiskAssessment.getAllForTask(taskID);
        activityRiskAssessmentAdapter.items = roads;
        activityRiskAssessmentAdapter.notifyDataSetChanged();
    }

    public boolean validate() {

        if(activityRiskAssessmentAdapter.items.size() == 0 ) {
            Toast.makeText(getApplicationContext(), "Please add at least one risk assessment", Toast.LENGTH_LONG).show();
            return false;
        }

        for (int i = 0; i < activityRiskAssessmentAdapter.items.size(); i++) {
            if (activityRiskAssessmentAdapter.getItem(i).cleansingLogID == null) {
                Toast.makeText(getApplicationContext(), activityRiskAssessmentAdapter.getItem(i).roadName + " does not have a cleansing log", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    public void save() {
        Task.EndTask(task.taskID);

        finish();
    }

    public void confirmClick(View view) {
        if (!validate()) {
            return;
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
        alertDialog.setTitle("Continue");
        alertDialog.setMessage("Please confirm you have completed this task.");

        alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                save();
            }
        });

        alertDialog.setNegativeButton("Cancel", null);

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", userID);
        startActivity(selectMaintenanceActivity);
    }

    public void addRiskAssessmentClick(View view) {
        Intent activityRiskAssessmentActivity = new Intent(getApplicationContext(), ActivityRiskAssessmentActivity.class);
        activityRiskAssessmentActivity.putExtra("ShiftID", shiftID);
        activityRiskAssessmentActivity.putExtra("UserID", userID);
        activityRiskAssessmentActivity.putExtra("TaskID", taskID);
        activityRiskAssessmentActivity.putExtra("JobPackID", jobPackID);
        startActivity(activityRiskAssessmentActivity);
    }

    public void rowClick(View view) {
        Intent activityCleansingLogActivity = new Intent(getApplicationContext(), CleansingLogActivity.class);

        int pos = roadList.getPositionForView(view);
        ActivityRiskAssessment activityRiskAssessment = activityRiskAssessmentAdapter.getItem(pos);

        Task task = Task.getCleansingLogTask(shiftID);

        activityCleansingLogActivity.putExtra("ShiftID", shiftID);
        activityCleansingLogActivity.putExtra("UserID", userID);
        activityCleansingLogActivity.putExtra("TaskID", task.taskID);
        activityCleansingLogActivity.putExtra("ActivityRiskAssessmentID", activityRiskAssessment.activityRiskAssessmentID);
        activityCleansingLogActivity.putExtra("CleansingLogID", activityRiskAssessment.cleansingLogID);
        startActivity(activityCleansingLogActivity);
    }

    public void editClick(View view) {
        Intent activityRiskAssessmentActivity = new Intent(getApplicationContext(), ActivityRiskAssessmentActivity.class);

        int pos = roadList.getPositionForView(view);
        ActivityRiskAssessment activityRiskAssessment = activityRiskAssessmentAdapter.getItem(pos);

        activityRiskAssessmentActivity.putExtra("ShiftID", shiftID);
        activityRiskAssessmentActivity.putExtra("UserID", userID);
        activityRiskAssessmentActivity.putExtra("TaskID", taskID);
        activityRiskAssessmentActivity.putExtra("JobPackID", jobPackID);
        activityRiskAssessmentActivity.putExtra("ActivityRiskAssessmentID", activityRiskAssessment.activityRiskAssessmentID);
        startActivity(activityRiskAssessmentActivity);
    }
}