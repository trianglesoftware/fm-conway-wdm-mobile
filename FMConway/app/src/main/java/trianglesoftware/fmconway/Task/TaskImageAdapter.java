package trianglesoftware.fmconway.Task;

/**
 * Created by Adam.Patrick on 23/11/2016.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

import trianglesoftware.fmconway.Database.DatabaseObjects.TaskPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

class TaskImageAdapter extends  BaseAdapter {
    private final Context mContext;
    private ArrayList<TaskPhoto> photoList = new ArrayList<>();
    private View.OnClickListener onClickListener;

    public TaskImageAdapter(Context c, ArrayList<TaskPhoto> PhotoList, View.OnClickListener onClickListener) {
        this.mContext = c;
        this.photoList = PhotoList;
        this.onClickListener = onClickListener;
    }

    public int getCount() {
        return photoList.size();
    }

    public Object getItem(int position) {
        return photoList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        final TaskPhoto taskPhoto = photoList.get(position);

        final ImageView imageView = new ImageView(mContext);
        imageView.setImageBitmap(taskPhoto.imageData);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setRotation(90);
        imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
        imageView.setTag(taskPhoto.taskPhotoID);
        imageView.setOnClickListener(onClickListener);

        return imageView;
    }

    public void update(ArrayList<TaskPhoto> photoList) {
        this.photoList = photoList;
        this.notifyDataSetChanged();
    }
}
