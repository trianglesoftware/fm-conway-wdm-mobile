package trianglesoftware.fmconway.Task;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipmentAsset;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class TaskEquipmentAssetActivity extends FMConwayActivity {
    //intents
    public String shiftID;
    public String taskEquipmentID;
    public int quantityRequired;
    
    //other
    public TaskEquipmentAssetAdapter adapter;
    public ListView list;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID");
            taskEquipmentID = extras.getString("TaskEquipmentID");
            quantityRequired = extras.getInt("QuantityRequired");
        }

        super.onCreate(savedInstanceState);

        adapter = new TaskEquipmentAssetAdapter(this, getLayoutInflater());
        adapter.QuantityRequired = quantityRequired;
        list = (ListView) findViewById(R.id.task_equipment_asset_list);
        list.setAdapter(adapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_equipment_task_asset;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Task Equipment Assets";
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }
    
    public void getData() {
        adapter.Entities = TaskEquipmentAsset.byTaskEquipment(taskEquipmentID);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();

        for (int i = 0; i < list.getChildCount(); i++) {
            TaskEquipmentAssetAdapter.ViewHolder viewHolder = (TaskEquipmentAssetAdapter.ViewHolder) list.getChildAt(i).getTag();
            viewHolder.Entity.AssetNumber = viewHolder.AssetNumber.getText().toString();
            viewHolder.Entity.update();
        }
    }
    
    public void deleteRow_Click(View view) {
        int position = list.getPositionForView(view);
        TaskEquipmentAsset deleteTaskEquipmentAsset = adapter.getItem(position);
        
        // update item numbers of other assets
        int counter = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            TaskEquipmentAsset taskEquipmentAsset = adapter.getItem(i);
            if (taskEquipmentAsset.TaskEquipmentAssetID.equals(deleteTaskEquipmentAsset.TaskEquipmentAssetID)) {
                continue;
            }

            counter++;
            taskEquipmentAsset.ItemNumber = counter;
            taskEquipmentAsset.update();
        }
        
        // delete asset
        deleteTaskEquipmentAsset.delete();
        getData();
    }
    
}
