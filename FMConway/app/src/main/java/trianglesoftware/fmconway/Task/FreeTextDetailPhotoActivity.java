package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskPhoto;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class FreeTextDetailPhotoActivity extends FMConwayActivity {
    private String shiftID;
    private String jobPackID;
    private String taskID;
    private String url;
    private int taskCount;
    private Task task;
    private TextView scopeDetails;
    private TextView workInstructionComments;
    private Button taskCompleteButton;
    private Button maintenanceButton;
    private Button addPhotoButton;
    private EditText details;
    private SharedPreferences sp;

    private String filepath;
    private String retakeTaskPhotoID;
    private final int REQUEST_CODE = 10;

    private GridView gridView;
    private ArrayList<TaskPhoto> photoList;
    private TaskImageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            jobPackID = extras.getString("JobPackID","");
            taskID = extras.getString("TaskID","");
            taskCount = extras.getInt("TaskCount");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        url = this.getResources().getString(R.string.QUERY_URL);

        task = Task.GetTask(taskID);

        Activity activity = Activity.GetActivity(task.activityID);
        workInstructionComments = (TextView)findViewById(R.id.workinstruction_comments);
        workInstructionComments.setText(activity.getWorksInstructionComments());

        if (isNullOrWhiteSpace(activity.getWorksInstructionComments())) {
            ScrollView commentsScroller = (ScrollView) findViewById(R.id.workinstruction_comments_scroller);
            commentsScroller.setVisibility(View.GONE);
        }

        gridView = (GridView)findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = TaskPhoto.getPhotosForTask(taskID);

        adapter = new TaskImageAdapter(this, photoList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoEditDialog(view);
            }
        });

        gridView.setAdapter(adapter);

        scopeDetails = (TextView)findViewById(R.id.scope_description);
        scopeDetails.setText(trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetScopeOfWork(task.activityID, shiftID));

        taskCompleteButton = (Button)findViewById(R.id.task_complete_button);

        taskCompleteButton.setText(task.name);

        maintenanceButton = (Button)findViewById(R.id.maintenance_button);

        addPhotoButton = (Button)findViewById(R.id.add_photo);

        details = (EditText)findViewById(R.id.taskDetails);

        MaintenanceDue();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_free_text_detail_photo;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Task";
    }

    private void photoEditDialog(final View imageView) {
        View photoDialogView = getLayoutInflater().inflate(R.layout.photo_dialog, null);
        final int position = gridView.getPositionForView(imageView);
        final TaskPhoto taskPhoto = photoList.get(position);

        final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog))
                .setView(photoDialogView)
                .setCancelable(true)
                .create();

        photoDialogView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FMConwayUtils.confirmDialog(FreeTextDetailPhotoActivity.this, "Delete photo and comment, are you sure?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TaskPhoto.deleteTaskPhotoFile(imageView.getTag().toString());
                        TaskPhoto.deleteTaskPhoto(imageView.getTag().toString());
                        photoList.remove(position);
                        adapter.update(photoList);
                        dialog.dismiss();
                    }
                });
            }
        });

        photoDialogView.findViewById(R.id.view_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", new File(taskPhoto.imageLocation));

                String extension = MimeTypeMap.getFileExtensionFromUrl(taskPhoto.imageLocation);
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileIntent.setDataAndType(uri, type);

                startActivity(fileIntent);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.retake_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = getOutputMediaFile();
                Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                filepath = file.getPath();
                retakeTaskPhotoID = taskPhoto.taskPhotoID;

                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                // start the image capture Intent
                startActivityForResult(intent, REQUEST_CODE);

                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.edit_comments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectActivity = new Intent(FreeTextDetailPhotoActivity.this, PhotoCommentActivity.class);
                selectActivity.putExtra("TaskPhotoID", taskPhoto.taskPhotoID);
                startActivity(selectActivity);
                dialog.dismiss();
            }
        });

        photoDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static boolean isNullOrWhiteSpace(String string)
    {
        return string == null || string.trim().isEmpty();
    }

    private void MaintenanceDue()
    {
        if(!MaintenanceCheck.CheckLatestTime(taskID, false))
        {
            maintenanceButton.setText("Maintenance Due");
            maintenanceButton.setBackgroundColor(Color.parseColor("#FF6961"));
        }
        else {
            maintenanceButton.setText("Maintenance");
            maintenanceButton.setBackgroundColor(getResources().getColor(R.color.fmconway_blue));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        MaintenanceDue();

        if(!TaskPhoto.checkIfPhotosExist(task.taskID))
        {
            addPhotoButton.setBackgroundColor(Color.parseColor("#41ab45"));
            taskCompleteButton.setEnabled(false);
            taskCompleteButton.setBackgroundColor(Color.parseColor("#808080"));
        }
        else
        {
            addPhotoButton.setBackgroundColor(Color.parseColor("#00215D"));
            taskCompleteButton.setEnabled(true);
            taskCompleteButton.setBackgroundColor(Color.parseColor("#41ab45"));
        }
    }

    public void taskCompleted(View view)
    {
        if (!Objects.equals(details.getText().toString(), "")) {
            Task.EndTask(task.taskID);
            Task.StoreTaskDetails(task.taskID, details.getText().toString());

            /*if (Objects.equals(task.name,"Arrive on Site")) {
                ShiftProgress.SetShiftProgress(shiftID, "TasksStarted");
                ShiftProgress.SyncProgress();
            }*/

            /*if (Objects.equals(task.name, "Leave Site")) {
                ShiftProgress.SetShiftProgress(shiftID, "Completed");
                ShiftProgress.SyncProgress();
            }*/

            finish();

            /*if (checkIfFromClosureInstalled()) {
                if (task.taskOrder < taskCount) {
                    //Load Next Activity
                    task = Task.GetTaskFromActivity(task.activityID);

                    TaskSelection(task, taskCount, jobPackID);
                }

                Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
                selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
                selectMaintenanceActivity.putExtra("ShiftID", shiftID);
                selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(selectMaintenanceActivity);
                finish();
            }
            else
            {
                if (task.taskOrder < taskCount) {
                    //Load Next Activity
                    task = Task.GetTaskFromActivity(task.activityID);

                    TaskSelection(task, taskCount, jobPackID);
                }
            }*/
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please enter the details.", Toast.LENGTH_SHORT).show();
        }
    }

    public void photoClick(View view)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();
        retakeTaskPhotoID = null;

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    public void drawingClick(View view)
    {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view)
    {
        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", task.activityID);
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

    public void taskClick(View view)
    {
        Intent taskDisplayDialog = new Intent(getApplicationContext(), TaskDisplayDialog.class);
        taskDisplayDialog.putExtra("ActivityID", task.activityID);
        startActivity(taskDisplayDialog);
    }

//    public void ramsClick(View view)
//    {
////        Intent ramsIntent = new Intent(getApplicationContext(), RamsActivity.class);
////        ramsIntent.putExtra("JobPackID", jobPackID);
////        ramsIntent.putExtra("ShiftID", shiftID);
////        startActivity(ramsIntent);
//
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
//        startActivity(browserIntent);
//    }

    public boolean checkIfFromClosureInstalled()
    {
        try {
            int previousTaskOrder = task.taskOrder - 1;
            String activityID = task.activityID;

            Task previousTask = Task.GetTaskFromActivityAndOrder(activityID, previousTaskOrder);
            if (previousTask != null) {
                String name = previousTask.getName();

                if (Objects.equals(name, "Closure Installed"))
                //if (Objects.equals(name,"Check Site (Post Install).  Then carry out a maintenance check"))
                {
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "CheckIfFromClosureInstalled");
        }

        return false;
    }

    private static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (retakeTaskPhotoID != null) {
                    TaskPhoto taskPhoto = savePhoto(retakeTaskPhotoID, filepath);
                } else {
                    TaskPhoto taskPhoto = savePhoto(null, filepath);

                    Intent selectActivity = new Intent(getApplicationContext(), PhotoCommentActivity.class);
                    selectActivity.putExtra("TaskPhotoID", taskPhoto.taskPhotoID);
                    startActivity(selectActivity);
                }

                photoList = TaskPhoto.getPhotosForTask(taskID);
                adapter.update(photoList);
            }
        }
    }

    private TaskPhoto savePhoto(String taskPhotoID, String imageLocation) {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude = 0;
        double latitude = 0;

        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        TaskPhoto taskPhoto = null;

        if (taskPhotoID != null) {
            taskPhoto = TaskPhoto.getTaskPhoto(taskPhotoID);
        } else {
            taskPhoto = new TaskPhoto();
            taskPhoto.taskPhotoID = UUID.randomUUID().toString();
        }

        taskPhoto.taskID = taskID;
        taskPhoto.longitude = longitude;
        taskPhoto.latitude = latitude;
        taskPhoto.imageLocation = imageLocation;
        taskPhoto.userID = sp.getString("UserID", "");
        taskPhoto.time = new Date();
        taskPhoto.shiftID = shiftID;

        TaskPhoto.saveTaskPhoto(taskPhoto);

        return taskPhoto;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation changes
        outState.putString("filepath", filepath);
        outState.putString("retakeTaskPhotoID", retakeTaskPhotoID);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
        retakeTaskPhotoID = savedInstanceState.getString("retakeTaskPhotoID");
    }
}
