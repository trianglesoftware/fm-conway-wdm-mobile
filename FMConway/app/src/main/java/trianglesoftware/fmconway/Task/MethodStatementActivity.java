package trianglesoftware.fmconway.Task;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.MethodStatement;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class MethodStatementActivity extends FMConwayActivityBase {
    private SharedPreferences sp;
    private String[] selectedMethodStatementIDs;
    List<MethodStatementAdapter.Item> allItems = new LinkedList<>();

    // inputs
    private TextView headerButton;
    private Button backButton;
    private Button homeButton;
    private ListView methodStatementListView;
    private MethodStatementAdapter methodStatementAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_method_statements);

        Bundle extras = getIntent().getExtras();
        selectedMethodStatementIDs = extras.getStringArray("MethodStatementIDs");
        if (selectedMethodStatementIDs == null) {
            selectedMethodStatementIDs = new String[0];
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        headerButton = findViewById(R.id.header_title);
        headerButton.setText("Method Statements");

        backButton = findViewById(R.id.back);
        homeButton = findViewById(R.id.back_to_home);

        // nav buttons
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
                startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startIntent.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(startIntent);
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // listview
        methodStatementAdapter = new MethodStatementAdapter(this, getLayoutInflater());
        methodStatementListView = findViewById(R.id.method_statements_list);
        methodStatementListView.setAdapter(methodStatementAdapter);

        // search
        ((SearchView)findViewById(R.id.search_view)).setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filter(s);
                return true;
            }
        });

        findViewById(R.id.search_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchView)findViewById(R.id.search_view)).onActionViewExpanded();
            }
        });

        // load
        load();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void load() {
        allItems.clear();
        methodStatementAdapter.items.clear();
        List<String> listSelectedMethodStatementIDs = Arrays.asList(selectedMethodStatementIDs);

        List<MethodStatement> methodStatements = MethodStatement.getAll();
        for (MethodStatement methodStatement : methodStatements) {
            MethodStatementAdapter.Item item = new MethodStatementAdapter.Item();
            item.methodStatement = methodStatement;
            item.checked = listSelectedMethodStatementIDs.contains(methodStatement.methodStatementID);
            item.visible = true;

            allItems.add(item);
            methodStatementAdapter.items.add(item);
        }

        methodStatementAdapter.notifyDataSetChanged();
    }

    private void filter(String search) {
        methodStatementAdapter.items.clear();

        for (MethodStatementAdapter.Item item : allItems) {
            item.visible = FMConwayUtils.isNullOrWhitespace(search) || item.methodStatement.methodStatementTitle.toLowerCase().contains(search.toLowerCase());
            if (item.visible) {
                methodStatementAdapter.items.add(item);
            }
        }

        methodStatementAdapter.notifyDataSetChanged();
    }

    public void rowClick(View view) {
        int position = methodStatementListView.getPositionForView(view);
        MethodStatementAdapter.Item selectedItem = methodStatementAdapter.getItem(position);

        methodStatementAdapter.items.clear();

        for (MethodStatementAdapter.Item item : allItems) {
            if (item.methodStatement.methodStatementID.equals(selectedItem.methodStatement.methodStatementID)) {
                item.checked = !item.checked;
            }

            if (item.visible) {
                methodStatementAdapter.items.add(item);
            }
        }

        methodStatementAdapter.notifyDataSetChanged();
    }

    public void confirmClick(View view) {
        List<MethodStatementAdapter.Item> selectedItems = new LinkedList<>();
        for (MethodStatementAdapter.Item item : allItems) {
            if (item.checked) {
                selectedItems.add(item);
            }
        }

        String[] selectedMethodStatementIDs = new String[selectedItems.size()];
        String[] selectedMethodStatementTitles = new String[selectedItems.size()];

        for (int i = 0; i < selectedItems.size(); i++) {
            MethodStatementAdapter.Item selectedItem = selectedItems.get(i);
            selectedMethodStatementIDs[i] = selectedItem.methodStatement.methodStatementID;
            selectedMethodStatementTitles[i] = selectedItem.methodStatement.methodStatementTitle;
        }

        Intent intent = new Intent();
        intent.putExtra("MethodStatementIDs", selectedMethodStatementIDs);
        intent.putExtra("MethodStatementTitles", selectedMethodStatementTitles);

        setResult(RESULT_OK, intent);

        finish();
    }

}
