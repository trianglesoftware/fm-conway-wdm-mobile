package trianglesoftware.fmconway.Task;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipmentAsset;
import trianglesoftware.fmconway.Equipment.SelectEquipmentTypeActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class TaskEquipmentActivity extends FMConwayActivity {
    private String shiftID;
    private String userID;
    private String taskID;
    private String jobPackID;
    private int taskCount;
    private ListView equipmentList;
    private List<TaskEquipment> taskEquipment;
    private Task task;
    private TaskEquipmentAdapter taskEquipmentAdapter;
    private SharedPreferences sp;
    private boolean isInstallation;

    private Button equipmentConfirm;
    private Button addEquipment;
    private TextView listHeader;
    private EditText notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            taskID = extras.getString("TaskID","");
            jobPackID = extras.getString("JobPackID","");
            userID = extras.getString("UserID","");
            taskCount = extras.getInt("TaskCount");
            isInstallation = extras.getBoolean("IsInstallation");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        super.onCreate(savedInstanceState);

        task = Task.GetTask(taskID);

        Task.StartTask(taskID);

        equipmentConfirm = (Button)findViewById(R.id.confirm);
        listHeader = (TextView)findViewById(R.id.list_header);
        addEquipment = (Button) findViewById(R.id.add_equipment_confirm);
        notes = (EditText) findViewById(R.id.text_notes);

        equipmentList = (ListView) findViewById(R.id.equipment_list);
        taskEquipmentAdapter = new TaskEquipmentAdapter(this, getLayoutInflater());
        taskEquipmentAdapter.taskID = taskID;
        equipmentList.setAdapter(taskEquipmentAdapter);
        
        if (Objects.equals(task.tag, "ShowNotes")) {
            notes.setText(task.notes);
        } else {
            LinearLayout notesContainer = (LinearLayout) findViewById(R.id.notes_container);
            notesContainer.setVisibility(View.GONE);
        }

        equipmentList.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ShrinkPage(!hasFocus);
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_equipment_task;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Equipment";
    }

    @Override
    public void onResume(){
        super.onResume();

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"TaskEquipmentActivity");
        }
    }

    private void ShrinkPage(boolean shrink)
    {
        int taskEquipments = taskEquipmentAdapter.getCount();
        if (shrink && taskEquipments > 0) {
            equipmentConfirm.setVisibility(View.GONE);
            addEquipment.setVisibility(View.GONE);
            listHeader.setVisibility(View.GONE);
        } else {
            equipmentConfirm.setVisibility(View.VISIBLE);
            addEquipment.setVisibility(View.VISIBLE);
            listHeader.setVisibility(View.VISIBLE);
        }
    }

    private void GetData() throws Exception
    {
        taskEquipment = TaskEquipment.GetTaskEquipmentForTask(taskID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < taskEquipment.size(); i++) {
            jsonArray.put(taskEquipment.get(i).getJSONObject());
        }

        taskEquipmentAdapter.UpdateData(jsonArray);
    }

    public void confirmClick(View view) throws Exception
    {
        if (taskEquipment.size() == 0)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Continue");
            alertDialog.setMessage("Please confirm that no equipment was set out for this task.");

            alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    confirmContinue();
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        } else {
            // check assets
            for (int i = 0; i < taskEquipmentAdapter.getCount(); i++) {
                JSONObject taskEquipment = taskEquipmentAdapter.getItem(i);
                int quantityRequired = taskEquipment.getInt("Quantity");
                boolean isVmsOrAsset = !taskEquipment.getString("VmsOrAsset").isEmpty();
                int emptyAssets = taskEquipment.getInt("EmptyAssets");
                int totalAssets = taskEquipment.getInt("TotalAssets");
                
                if (isVmsOrAsset && (quantityRequired != totalAssets || emptyAssets != 0)) {
                    Toast.makeText(getApplicationContext(), "Please update asset numbers", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Continue");
            alertDialog.setMessage("Confirm equipment and proceed to next task?");

            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    confirmContinue();
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    }
    
    private void confirmContinue()
    {
        // if installation task and collection task exists later on in work instruction then copy equipment and assets 
        if (isInstallation) {
            String activityID = Activity.getActivityIdByTaskID(taskID);
            String collectionTaskID = Task.getEquipmentCollectionTaskIdByActivityID(activityID);

            if (!TextUtils.isEmpty(collectionTaskID)) {
                TaskEquipment.copyTaskEquipmentAndAssets(taskID, collectionTaskID);
            }
        }

        if (Objects.equals(task.tag, "ShowNotes")) {
            Task.StoreTaskDetails(task.taskID, notes.getText().toString());
        }

        Task.EndTask(task.taskID);
        finish();

        /*if (task.taskOrder < taskCount) {
            task = Task.GetTaskFromActivity(task.activityID);
            TaskSelection(task, taskCount, jobPackID);
        }*/
    }

    public void addEquipmentClick(View view)
    {
        Intent selectType = new Intent(getApplicationContext(), SelectEquipmentTypeActivity.class);
        selectType.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        selectType.putExtra("ShiftID", shiftID);
        selectType.putExtra("UserID", userID);
        selectType.putExtra("TaskID", taskID);
        selectType.putExtra("IsInstallation", isInstallation);
        startActivity(selectType);
    }

    public void quantity_down(View view)
    {
        int pos = equipmentList.getPositionForView((View) view.getParent());
        JSONObject selected = taskEquipmentAdapter.getItem(pos);

        final String taskEquipmentID = selected.optString("TaskEquipmentID");

        final String equipmentName = selected.optString("Name");

        int quantity = TaskEquipment.GetTaskEquipmentQuantity(taskEquipmentID);

        if (quantity < 2)
        {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:

                            TaskEquipment.deleteAllTaskEquipment(taskEquipmentID);

                            try {
                                GetData();
                            }
                            catch (Exception e)
                            {
                                ErrorLog.CreateError(e,"TaskEquipmentActivity");
                            }

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this ,R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("Delete equipment " + equipmentName + "?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
        }
        else
        {
            TaskEquipment.StoreTaskDetails(taskEquipmentID, quantity - 1);
        }

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "TaskEquipmentActivity");
        }
    }

    public void quantity_up(View view)
    {
        int pos = equipmentList.getPositionForView((View) view.getParent());
        JSONObject selected = taskEquipmentAdapter.getItem(pos);

        String taskEquipmentID = selected.optString("TaskEquipmentID");

        int quantity = TaskEquipment.GetTaskEquipmentQuantity(taskEquipmentID);

        TaskEquipment.StoreTaskDetails(taskEquipmentID, quantity + 1);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "TaskEquipmentActivity");
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    try {
                        GetData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
    
    public void assets_Click(View view) throws Exception {
        int position = equipmentList.getPositionForView(view);
        JSONObject taskEquipment = taskEquipmentAdapter.getItem(position);
        String taskEquipmentID = taskEquipment.getString("TaskEquipmentID");
        int quantityRequired = taskEquipment.getInt("Quantity");

        // create empty assets if required
        List<TaskEquipmentAsset> taskEquipmentAssets = TaskEquipmentAsset.byTaskEquipment(taskEquipmentID);
        int newAssets = quantityRequired - taskEquipmentAssets.size();
        if (newAssets > 0) {
            for (int i = 1; i <= newAssets; i++) {
                TaskEquipmentAsset taskEquipmentAsset = new TaskEquipmentAsset();
                taskEquipmentAsset.TaskEquipmentAssetID = UUID.randomUUID().toString();
                taskEquipmentAsset.TaskEquipmentID = taskEquipmentID;
                taskEquipmentAsset.ItemNumber = taskEquipmentAssets.size() + i;
                taskEquipmentAsset.AssetNumber = "";
                taskEquipmentAsset.ShiftID = shiftID;
                taskEquipmentAsset.insert();
            }
        }
        
        Intent intent = new Intent(getApplicationContext(), TaskEquipmentAssetActivity.class);
        intent.putExtra("ShiftID", shiftID);
        intent.putExtra("TaskEquipmentID", taskEquipmentID);
        intent.putExtra("QuantityRequired", quantityRequired);
        startActivity(intent);
    }

}
