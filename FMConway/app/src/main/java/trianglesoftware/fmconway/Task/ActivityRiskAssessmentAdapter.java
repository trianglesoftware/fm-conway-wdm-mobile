package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityRiskAssessment;
import trianglesoftware.fmconway.R;

public class ActivityRiskAssessmentAdapter extends BaseAdapter {
    public List<ActivityRiskAssessment> items;
    private Context context;
    private LayoutInflater inflater;

    public ActivityRiskAssessmentAdapter(Context context, LayoutInflater inflater) {
        this.items = new LinkedList<>();
        this.context = context;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ActivityRiskAssessment getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ActivityRiskAssessmentAdapter.ViewHolder holder;

        if (view == null) {
            view = inflater.inflate(R.layout.row_activity_risk_assessment, null);

            holder = new ActivityRiskAssessmentAdapter.ViewHolder();
            holder.roadTextView = view.findViewById(R.id.road);
            holder.isCompleted = view.findViewById(R.id.complete);

            view.setTag(holder);
        } else {
            holder = (ActivityRiskAssessmentAdapter.ViewHolder) view.getTag();
        }

        ActivityRiskAssessment activityRiskAssessment = items.get(position);

        if (activityRiskAssessment.cleansingLogID != null) {
            holder.isCompleted.setImageResource(R.drawable.circle_checked);
        } else {
            holder.isCompleted.setImageResource(R.drawable.circle_unchecked);
        }


        if (activityRiskAssessment.roadName != null) {
            holder.roadTextView.setText(activityRiskAssessment.roadName);
        }


        return view;
    }

    public static class ViewHolder {
        public TextView roadTextView;
        public ImageView isCompleted;
    }
}