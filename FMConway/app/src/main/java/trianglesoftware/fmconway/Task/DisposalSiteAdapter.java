package trianglesoftware.fmconway.Task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.DisposalSite;
import trianglesoftware.fmconway.R;

public class DisposalSiteAdapter extends BaseAdapter {
    public List<Item> items;
    private Context context;
    private final LayoutInflater inflater;

    public DisposalSiteAdapter(Context context, LayoutInflater inflater) {
        this.items = new LinkedList<>();
        this.context = context;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_disposal_site, null);

            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Item item = getItem(position);
        holder.name.setText(item.disposalSite.name);
        holder.checkbox.setChecked(item.checked);

        return convertView;
    }

    public static class Item {
        public DisposalSite disposalSite;
        public boolean checked;
        public boolean visible;
    }

    public static class ViewHolder {
        public TextView name;
        public CheckBox checkbox;
    }
}
