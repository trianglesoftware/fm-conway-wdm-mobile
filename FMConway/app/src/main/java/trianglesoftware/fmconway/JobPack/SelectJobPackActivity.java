package trianglesoftware.fmconway.JobPack;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Activity.SelectActivity;
import trianglesoftware.fmconway.Briefing.SelectBriefingActivity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectJobPackActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String userID;
    private JobPackAdapter jobPackAdapter;
    private String url;
    private SharedPreferences sp;
    private int fromButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
            fromButton = extras.getInt("FromButton");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView jobPackList = (ListView) findViewById(R.id.select_job_pack_list);
        jobPackAdapter = new JobPackAdapter(this, getLayoutInflater());
        jobPackList.setAdapter(jobPackAdapter);
        jobPackList.setOnItemClickListener(this);

        ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage("Sending shift details please wait...");
        mDialog.setCancelable(false);

        try {
            List<JobPack> jobs = GetData();

            if (jobs.size() == 1) {
                JobPack pack = jobs.get(0);

                String selected = pack.jobPackID;

                switch (fromButton) {
                    //From the Go To Work button - Load Activity List
                    case 0:
                        Intent selectActivity = new Intent(getApplicationContext(), SelectActivity.class);
                        selectActivity.putExtra("JobPackID", selected);
                        selectActivity.putExtra("ShiftID", shiftID);
                        selectActivity.putExtra("UserID", userID);
                        selectActivity.putExtra("ReadOnly", false);
                        startActivity(selectActivity);
                        break;
                    //From the Safety First Button - Load Briefing List
                    case 1:
                        Intent selectBriefingActivity = new Intent(getApplicationContext(), SelectBriefingActivity.class);
                        selectBriefingActivity.putExtra("JobPackID", selected);
                        selectBriefingActivity.putExtra("ShiftID", shiftID);
                        selectBriefingActivity.putExtra("UserID", userID);
                        startActivity(selectBriefingActivity);
                        break;
                    //From the Activity Button - Load Activity List - Read Only
                    case 2:
                        Intent selectActivityRO = new Intent(getApplicationContext(), SelectActivity.class);
                        selectActivityRO.putExtra("JobPackID", selected);
                        selectActivityRO.putExtra("ShiftID", shiftID);
                        selectActivityRO.putExtra("ReadOnly", true);
                        selectActivityRO.putExtra("UserID", userID);
                        startActivity(selectActivityRO);
                        break;
                    default:
                        break;
                }

            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectJobPackActivityGetData");
        }

        url = this.getResources().getString(R.string.QUERY_URL);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_job_pack;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Job Packs";
    }

    private List<JobPack> GetData() throws Exception
    {
        List<JobPack> jobPacks = JobPack.GetJobPacksForShift(shiftID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < jobPacks.size(); i++) {
            jsonArray.put(jobPacks.get(i).getJSONObject());
        }

        jobPackAdapter.UpdateData(jsonArray);

        return jobPacks;
    }

    public static List<JobPack> GetData(String ShiftID) throws Exception
    {
        List<JobPack> jobPacks = JobPack.GetJobPacksForShift(ShiftID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < jobPacks.size(); i++) {
            jsonArray.put(jobPacks.get(i).getJSONObject());
        }

        return jobPacks;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        switch(fromButton)
        {
            //From the Go To Work button - Load Activity List
            case 0:
                Intent selectActivity = new Intent(getApplicationContext(), SelectActivity.class);
                selectActivity.putExtra("JobPackID", selected);
                selectActivity.putExtra("ShiftID", shiftID);
                selectActivity.putExtra("UserID", userID);
                selectActivity.putExtra("ReadOnly", false);
                startActivity(selectActivity);
                break;
            //From the Safety First Button - Load Briefing List
            case 1:
                Intent selectBriefingActivity = new Intent(getApplicationContext(), SelectBriefingActivity.class);
                selectBriefingActivity.putExtra("JobPackID", selected);
                selectBriefingActivity.putExtra("ShiftID", shiftID);
                selectBriefingActivity.putExtra("UserID", userID);
                startActivity(selectBriefingActivity);
                break;
            //From the Activity Button - Load Activity List - Read Only
            case 2:
                Intent selectActivityRO = new Intent(getApplicationContext(), SelectActivity.class);
                selectActivityRO.putExtra("JobPackID", selected);
                selectActivityRO.putExtra("ShiftID", shiftID);
                selectActivityRO.putExtra("UserID", userID);
                selectActivityRO.putExtra("ReadOnly", true);
                startActivity(selectActivityRO);
                break;
            default:
                break;
        }
    }
}
