package trianglesoftware.fmconway.Briefing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.Signature;

public class SelectBriefingActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String jobPackID;
    private String shiftID;
    private String userID;
    private BriefingAdapter briefingAdapter;
    private ProgressDialog mDialog;
    private Briefing currentBriefing;
    private Date briefingStart;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            jobPackID = extras.getString("JobPackID","");
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        super.onCreate(savedInstanceState);

        ListView briefingList = (ListView) findViewById(R.id.select_briefing_list);
        briefingAdapter = new BriefingAdapter(this, getLayoutInflater());
        briefingList.setAdapter(briefingAdapter);
        briefingList.setOnItemClickListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectBriefingActivityGetData");
        }

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Please Wait...");
        mDialog.setCancelable(false);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_briefing;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Safety First";
    }

    private void GetData() throws Exception
    {
        List<Briefing> briefings = Briefing.GetBriefingsForJobPack(jobPackID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < briefings.size(); i++) {
            jsonArray.put(briefings.get(i).getJSONObject());
        }

        briefingAdapter.UpdateData(jsonArray);
    }

    public static boolean CheckBriefingsConfirmed(String ShiftID)
    {
        List<Briefing> briefings = Briefing.GetBriefingsForShift(ShiftID);
        boolean briefingsSigned = false;
        int briefingSignedNumber = 0;
        for(int i = 0; i < briefings.size(); i++)
        {
            List<Staff> staffToSign = Staff.GetStaffForShift(Shift.GetShiftID(briefings.get(i).briefingID));
            int staffSigned = 0;
            for (int j = 0; j < staffToSign.size(); j++)
            {
                if(BriefingChecklistSignature.CheckStaffSignatureExists(briefings.get(i).briefingID, staffToSign.get(j).staffID))
                {
                    staffSigned += 1;
                }
            }
            if(staffSigned == staffToSign.size())
            {
                briefingSignedNumber += 1;
            }
        }

        if (briefingSignedNumber == briefings.size())
        {
            briefingsSigned = true;
        }

        return briefingsSigned;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        currentBriefing = Briefing.GetBriefing(selected);

        briefingStart = new Date();

        switch(currentBriefing.briefingTypeID)
        {
            //Toolbox Talk
//            case 1:
//                //Create answers if not already created
//                Checklist checklist = BriefingChecklist.GetBriefingChecklist(selected);
//                if(!BriefingChecklistAnswer.CheckIfBriefingChecklistCreated(selected, checklist.getChecklistID())) {
//                    List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklist.getChecklistID());
//                    for (int j = 0; j < checklistQuestions.size(); j++) {
//                        BriefingChecklistAnswer bca = new BriefingChecklistAnswer();
//                        bca.setBriefingID(selected);
//                        bca.setChecklistID(checklist.getChecklistID());
//                        bca.setChecklistQuestionID(checklistQuestions.get(j).getChecklistQuestionID());
//                        bca.setAnswer(false);
//                        BriefingChecklistAnswer.addBriefingChecklistAnswer(bca);
//                    }
//                }
//                Intent toolboxTalk = new Intent(getApplicationContext(), ToolboxTalkActivity.class);
//                toolboxTalk.putExtra("ShiftID", shiftID);
//                toolboxTalk.putExtra("BriefingID", selected);
//                toolboxTalk.putExtra("UserID", userID);
//                startActivity(toolboxTalk);
//                break;
            //Free Text
            case 3:
                Intent freeText = new Intent(getApplicationContext(), FreeTextBriefingActivity.class);
                freeText.putExtra("ShiftID", shiftID);
                freeText.putExtra("BriefingID", selected);
                freeText.putExtra("UserID", userID);
                startActivity(freeText);
                break;
            //Document
            case 4:
                //View then sign
                mDialog.show();

                if (currentBriefing.imageLocation != null) {
                    String file = currentBriefing.imageLocation;
                    String exten = "";

                    int i = file.lastIndexOf('.');
                    if (i > 0) {
                        exten = file.substring(i + 1);
                    }

                    //String extension = MimeTypeMap.getFileExtensionFromUrl(currentBriefing.getImageLocation());
                    if (!Objects.equals(exten, "")) {
                        try {
                            String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(exten);
                            Uri uri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", new File(file));
                            
                            Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            fileIntent.setDataAndType(uri, type);

                            startActivity(fileIntent);
                        }
                        catch (Exception e)
                        {
                            //ErrorLog.CreateError(e, "BriefingPDF");
                            Toast.makeText(this.getApplicationContext(), "Cannot open file.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(this.getApplicationContext(), "Invalid File Type.", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    // some method statements don't have an attached document as it's not mandatory, call onResume to allow skipping these
                    Toast.makeText(this.getApplicationContext(), "Cannot open file.", Toast.LENGTH_LONG).show();
                    onResume();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mDialog.dismiss();

        if(currentBriefing != null)
        {
            boolean briefingsConfirmed = CheckBriefingsConfirmed(shiftID);

            if(briefingsConfirmed)
            {
                finish();

                Intent startMenuActivity = new Intent(getApplicationContext(), StartActivity.class);
                startMenuActivity.putExtra("ShiftID",shiftID);
                startMenuActivity.putExtra("UserID",userID);
                startActivity(startMenuActivity);
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            List<Staff> staffToSign = Staff.GetStaffForShift(Shift.GetShiftID(currentBriefing.briefingID));
            //int staffSigned = 0;
            for (int i = 0; i < staffToSign.size(); i++)
            {
                if(!BriefingChecklistSignature.CheckStaffSignatureExists(currentBriefing.getBriefingID(), staffToSign.get(i).staffID))
                {
                    //Load Signature Intent for every staff member in shift
                    Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
                    signatureIntent.putExtra("BriefingID", currentBriefing.briefingID);
                    signatureIntent.putExtra("StaffID", staffToSign.get(i).staffID);
                    signatureIntent.putExtra("BriefingStart", dateFormat.format(briefingStart));
                    signatureIntent.putExtra("BriefingEnd", dateFormat.format(new Date()));
                    signatureIntent.putExtra("LoggedIn", sp.getString("UserID",""));
                    startActivity(signatureIntent);
                }
            }
//            if(staffSigned < staffToSign.size())
//            {
//                for (int i = 0; i < staffToSign.size(); i++)
//                {
//                    //Load Signature Intent for every staff member in shift
//                    Intent signatureIntent = new Intent(getApplicationContext(), Signature.class);
//                    signatureIntent.putExtra("BriefingID", currentBriefing.getBriefingID());
//                    signatureIntent.putExtra("StaffID", staffToSign.get(i).getStaffID());
//                    signatureIntent.putExtra("BriefingStart", dateFormat.format(briefingStart));
//                    signatureIntent.putExtra("BriefingEnd", dateFormat.format(new Date()));
//                    startActivity(signatureIntent);
//                }
//            }
        }
    }
}
