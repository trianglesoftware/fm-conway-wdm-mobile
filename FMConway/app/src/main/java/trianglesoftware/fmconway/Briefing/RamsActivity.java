package trianglesoftware.fmconway.Briefing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.File;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.ToolboxTalk.ToolboxTalkActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class RamsActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String jobPackID;
    private ListView ramsList;
    private BriefingAdapter ramsAdapter;
    private ProgressDialog mDialog;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            jobPackID = extras.getString("JobPackID","");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView ramsList = (ListView) findViewById(R.id.select_rams_list);
        ramsAdapter = new BriefingAdapter(this, getLayoutInflater());
        ramsList.setAdapter(ramsAdapter);
        ramsList.setOnItemClickListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"RAMSActivityGetData");
        }

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Please Wait...");
        mDialog.setCancelable(false);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_rams;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "RAMS";
    }

    private void GetData() throws Exception
    {
        List<Briefing> rams = Briefing.GetRAMSForJobPack(jobPackID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < rams.size(); i++) {
            jsonArray.put(rams.get(i).getJSONObject());
        }

        ramsAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Briefing currentBriefing = Briefing.GetBriefing(selected);

        switch(currentBriefing.briefingTypeID) {
            //Toolbox Talk
            case 2:
                //Create answers if not already created
                Checklist checklist = BriefingChecklist.GetBriefingChecklist(selected);
                if (!BriefingChecklistAnswer.CheckIfBriefingChecklistCreated(selected, checklist.checklistID)) {
                    List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklist.checklistID);
                    for (int j = 0; j < checklistQuestions.size(); j++) {
                        BriefingChecklistAnswer bca = new BriefingChecklistAnswer();
                        bca.briefingID = selected;
                        bca.checklistID = checklist.checklistID;
                        bca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                        bca.userID = sp.getString("UserID", "");
                        bca.shiftID = shiftID;
                        //bca.setAnswer(false);
                        BriefingChecklistAnswer.addBriefingChecklistAnswer(bca);
                    }
                }
                Intent toolboxTalk = new Intent(getApplicationContext(), ToolboxTalkActivity.class);
                toolboxTalk.putExtra("ShiftID", shiftID);
                toolboxTalk.putExtra("BriefingID", selected);
                startActivity(toolboxTalk);
                break;
            //Method Statement
            case 1:
                mDialog.show();

                try {
                    String extension = MimeTypeMap.getFileExtensionFromUrl(currentBriefing.getImageLocation());
                    if (extension != null && extension != null) {
                        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                        Uri uri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", new File(currentBriefing.imageLocation));
                        
                        Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                        fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        fileIntent.setDataAndType(uri, type);
                        
                        startActivity(fileIntent);
                    } else {
                        Toast.makeText(this.getApplicationContext(), "Invalid File Type.", Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e)
                {
                    ErrorLog.CreateError(e,this.getClass().getSimpleName());

                    Toast.makeText(getApplicationContext(), "An error has occurred.", Toast.LENGTH_LONG).show();
                }
                mDialog.dismiss();
                break;
            default:
                break;
        }

    }

    public void ramsClick(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getResources().getString(R.string.RAMS_LINK)));
        startActivity(browserIntent);
    }
}
