package trianglesoftware.fmconway.Briefing;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class FreeTextBriefingActivity extends FMConwayActivity {
    private String shiftID;
    private String briefingID;
    private String userID;
    private TextView briefingDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID", "");
            briefingID = extras.getString("BriefingID","");
            userID = extras.getString("UserID","");
        }

        super.onCreate(savedInstanceState);

        briefingDetails = (TextView)findViewById(R.id.briefing_details);

        briefingDetails.setText(Briefing.GetBriefing(briefingID).getDetails());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_free_text_briefing;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return Briefing.GetBriefing(briefingID).getName();
    }

    public void confirmBriefing(View view)
    {
        finish();
    }
}
