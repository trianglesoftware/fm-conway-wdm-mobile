package trianglesoftware.fmconway.Drawing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.File;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPackDocument;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.ToolboxTalk.ToolboxTalkActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectDrawingActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String jobPackID;
    private String shiftID;
    private String userID;
    private DrawingAdapter drawingAdapter;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            jobPackID = extras.getString("JobPackID","");
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
        }

        super.onCreate(savedInstanceState);

        ListView documentList = (ListView) findViewById(R.id.select_document_list);
        drawingAdapter = new DrawingAdapter(this, getLayoutInflater());
        documentList.setAdapter(drawingAdapter);
        documentList.setOnItemClickListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectDrawingActivityGetData");
        }

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Please Wait...");
        mDialog.setCancelable(false);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_drawing;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Drawings";
    }

    private void GetData() throws Exception
    {
        List<JobPackDocument> jobPackDocuments = JobPackDocument.GetJobPackDocuments(jobPackID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < jobPackDocuments.size(); i++) {
            jsonArray.put(jobPackDocuments.get(i).getJSONObject());
        }


        List<Briefing> rams = Briefing.GetRAMSForJobPack(jobPackID);

        for(int i = 0; i < rams.size(); i++) {
            jsonArray.put(rams.get(i).getJSONObject());
        }

        drawingAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag(R.id.docID);
        boolean isRAMS = (boolean) view.findViewById(R.id.text_equipment_type).getTag(R.id.isRams);

        try {
            if (!isRAMS) {
                mDialog.show();

                JobPackDocument doc = JobPackDocument.GetJobPackDocument(selected);
                String extension = "";

                if( doc == null || doc.imageLocation == null){
                    return;
                }

                int i = doc.imageLocation.lastIndexOf('.');
                if (i > 0) {
                    extension = doc.imageLocation.substring(i + 1);
                }
                if (extension != null) {
                    Uri uri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", new File(doc.imageLocation));
                    String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    
                    Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                    fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    fileIntent.setDataAndType(uri, type);
                    
                    startActivity(fileIntent);
                } else {
                    Toast.makeText(this.getApplicationContext(), "Invalid File Type.", Toast.LENGTH_LONG).show();
                }
            } else {
                Briefing currentBriefing = Briefing.GetBriefing(selected);

                switch (currentBriefing.briefingTypeID) {
                    //Toolbox Talk
                    case 1:
                        //Create answers if not already created
                        Checklist checklist = BriefingChecklist.GetBriefingChecklist(selected);
                        if (checklist != null) {
                            if (!BriefingChecklistAnswer.CheckIfBriefingChecklistCreated(selected, checklist.checklistID)) {
                                List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklist.checklistID);
                                for (int j = 0; j < checklistQuestions.size(); j++) {
                                    BriefingChecklistAnswer bca = new BriefingChecklistAnswer();
                                    bca.briefingID = selected;
                                    bca.checklistID = checklist.checklistID;
                                    bca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                                    bca.answer = false;
                                    bca.shiftID = shiftID;
                                    BriefingChecklistAnswer.addBriefingChecklistAnswer(bca);
                                }
                            }
                            Intent toolboxTalk = new Intent(getApplicationContext(), ToolboxTalkActivity.class);
                            toolboxTalk.putExtra("ShiftID", shiftID);
                            toolboxTalk.putExtra("BriefingID", selected);
                            startActivity(toolboxTalk);
                        }
                        break;
                    //Method Statement
                    case 2:
                    case 5:
                        mDialog.show();
                        String extension = "";
                        int i = currentBriefing.imageLocation.lastIndexOf('.');
                        if (i > 0) {
                            extension = currentBriefing.imageLocation.substring(i + 1);
                        }
                        if (extension != null && !TextUtils.isEmpty(extension)) {
                            String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                            Uri uri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", new File(currentBriefing.imageLocation));
                            
                            Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            fileIntent.setDataAndType(uri, type);
                            
                            startActivity(fileIntent);
                        } else {
                            Toast.makeText(this.getApplicationContext(), "Invalid File Type.", Toast.LENGTH_LONG).show();
                        }
                        mDialog.dismiss();
                        break;
                    default:
                        break;
                }
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,this.getClass().getSimpleName());

            Toast.makeText(getApplicationContext(), "An error has occurred.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mDialog.dismiss();
    }
}
