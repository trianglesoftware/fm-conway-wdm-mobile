package trianglesoftware.fmconway.Comment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Note;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectCommentActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private CommentAdapter commentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
        }

        super.onCreate(savedInstanceState);

        ListView commentList = (ListView) findViewById(R.id.select_comment_list);
        commentAdapter = new CommentAdapter(this, getLayoutInflater());
        commentList.setAdapter(commentAdapter);
        commentList.setOnItemClickListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectCommentGetData");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_comment;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Comments";
    }

    @Override
    public void onResume(){
        super.onResume();

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectCommentGetData");
        }
    }

    private void GetData() throws Exception
    {
        List<Note> comments = Note.GetNotesForShift(shiftID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < comments.size(); i++) {
            jsonArray.put(comments.get(i).getJSONObject());
        }

        commentAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Intent commentActivity = new Intent(getApplicationContext(), CommentActivity.class);
        commentActivity.putExtra("CommentID", selected);
        commentActivity.putExtra("ShiftID", shiftID);
        startActivity(commentActivity);
    }

    public void addComment(View view)
    {
        Intent commentActivity = new Intent(getApplicationContext(), CommentActivity.class);
        commentActivity.putExtra("CommentID", "");
        commentActivity.putExtra("ShiftID", shiftID);
        startActivity(commentActivity);

        finish();
    }
}
