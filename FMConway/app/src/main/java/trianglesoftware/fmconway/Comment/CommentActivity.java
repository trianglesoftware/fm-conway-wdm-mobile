package trianglesoftware.fmconway.Comment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

import java.util.Date;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Note;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class CommentActivity extends FMConwayActivityBase {
    private String commentID;
    private String shiftID;
    private EditText comment;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_comment);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            commentID = extras.getString("CommentID","");
            shiftID = extras.getString("ShiftID","");
        }

        comment = (EditText)findViewById(R.id.comment_detail);

        if(!Objects.equals(commentID,"")) {
            Note note = Note.GetNote(commentID);
            comment.setText(note.noteDetails);
        }
    }
     public void submitComment(View view)
    {
        if(!Objects.equals(commentID,""))
        {
            //Update
            Note.UpdateNote(commentID, comment.getText().toString());
        }
        else{
            //Add
            try {
                Note note = new Note();
                note.shiftID = shiftID;
                note.noteDetails = comment.getText().toString();
                note.noteDate = new Date();
                note.userID = sp.getString("UserID", "");
                note.isNew = true;
                Note.AddNote(note);
            }
            catch (Exception e)
            {
                ErrorLog.CreateError(e,"Add note");
            }
        }

        //finish();

        Intent selectCommentActivity = new Intent(getApplicationContext(), SelectCommentActivity.class);
        selectCommentActivity.putExtra("ShiftID", shiftID);
        startActivity(selectCommentActivity);
    }
}
