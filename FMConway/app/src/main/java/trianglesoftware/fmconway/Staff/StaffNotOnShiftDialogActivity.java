package trianglesoftware.fmconway.Staff;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class StaffNotOnShiftDialogActivity extends FMConwayActivityBase {
    private EditText notOnReason;
    private String shiftID;
    private String staffID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_not_on_shift_dialog);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            staffID = extras.getString("StaffID","");
        }

        notOnReason = (EditText)findViewById(R.id.editText_NotOnShift);
    }

    public void submitReason(View view) {
        ShiftStaff shiftStaff = ShiftStaff.GetShiftStaff(shiftID, staffID);
        shiftStaff.reasonNotOn = notOnReason.getText().toString();
        shiftStaff.notOnShift = true;
        ShiftStaff.UpdateShiftStaff(shiftStaff);

        Intent staffList = new Intent(getApplicationContext(), SelectStaffActivity.class);
        staffList.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        staffList.putExtra("ShiftID", shiftID);
        startActivity(staffList);
    }
}
