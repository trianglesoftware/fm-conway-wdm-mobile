package trianglesoftware.fmconway.Staff;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectStaffActivity extends FMConwayActivity {
    private String shiftID;
    private String userID;
    private ListView staffList;
    private StaffListAdapter staffListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
        }

        super.onCreate(savedInstanceState);

        staffList = (ListView)findViewById(R.id.select_staff_list);
        staffListAdapter = new StaffListAdapter(this, getLayoutInflater());
        staffList.setAdapter(staffListAdapter);

        try {
            getData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectStaffActivity");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        saveAgencyEmployeeNames();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_staff;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Confirm Your Team";
    }

    private void getData() throws Exception
    {
        List<Staff> staff = Staff.GetStaffForShift(shiftID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < staff.size(); i++) {
            jsonArray.put(staff.get(i).getJSONObject());
        }

        staffListAdapter.UpdateData(jsonArray);
    }

    public void removeStaffClick(View view)
    {
        int pos = staffList.getPositionForView((View)view.getParent());
        JSONObject selected = staffListAdapter.getItem(pos);
        //Load dialog intent with reason text field -> Once a reason is entered set not on shift to true and enter reason the refresh data adapter
        Intent removeDialog = new Intent(getApplicationContext(), StaffNotOnShiftDialogActivity.class);
        removeDialog.putExtra("StaffID", selected.optString("StaffID"));
        removeDialog.putExtra("ShiftID", shiftID);
        startActivity(removeDialog);
    }

    public void viewStaffCerts(View view)
    {
        int pos = staffList.getPositionForView((View)view.getParent());
        JSONObject selected = staffListAdapter.getItem(pos);
        //Load new Intent with list of staff records
        Intent certificateIntent = new Intent(getApplicationContext(), SelectCertificateActivity.class);
        certificateIntent.putExtra("StaffID", selected.optString("StaffID"));
        certificateIntent.putExtra("ShiftID", shiftID);
        startActivity(certificateIntent);
    }

    public void addStaffClick(View view)
    {
        //Load new dialog intent with autocomplete search to select staff not on shift
        Intent addNewDialog = new Intent(getApplicationContext(), SelectNewStaffDialogActivity.class);
        addNewDialog.putExtra("ShiftID", shiftID);
        startActivity(addNewDialog);
    }
    
    public void saveAgencyEmployeeNames() {
        try {
            for (int i = 0; i < staffList.getChildCount(); i++) {
                JSONObject jsonObject = staffListAdapter.getItem(i);
                if (jsonObject.optBoolean("IsAgency")) {
                    StaffListAdapter.ViewHolder viewHolder = (StaffListAdapter.ViewHolder) staffList.getChildAt(i).getTag();
                    String agencyEmployeeName = viewHolder.agencyEmployeeNameEditText.getText().toString();
                    Staff.updateAgencyEmployeeName(shiftID, jsonObject.getString("StaffID"), agencyEmployeeName);
                }
            }
        }
        catch (Exception ex)
        {
            Toast.makeText(getApplicationContext(), "Save associate names failed", Toast.LENGTH_LONG).show();
        }
    }

    public void confirmStaffClick(View view)
    {
        try {
            saveAgencyEmployeeNames();
            
            if (!Staff.haveAllAgencyEmployeesBeenNamedForShift(shiftID)) {
                Toast.makeText(getApplicationContext(), "Please name any agency associates", Toast.LENGTH_LONG).show();
                return;
            }
            
            if (Shift.GetShiftCompletionStatus(shiftID) == 0) {
                Shift.SetCompletionStatus(shiftID, 1);
            }

            //Set Staff Start Time
            List<Staff> staff = Staff.GetStaffForShift(shiftID);
            for (int i = 0; i < staff.size(); i++) {
                ShiftStaff.SetStartTime(staff.get(i).staffID, shiftID);
            }
            
            finish();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ConfirmStaffClick");
        }
        
        //Confirm and move on to next intent -> Selecting job packs
        //Intent selectJobActivity = new Intent(getApplicationContext(), SelectJobPackActivity.class);
        //selectJobActivity.putExtra("ShiftID", shiftID);
        //selectJobActivity.putExtra("UserID", userID);
        //startActivity(selectJobActivity);
    }
}
