package trianglesoftware.fmconway.Staff;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 02/03/2016.
 */
class StaffListAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public StaffListAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_staff, null);

            holder = new ViewHolder();
            holder.middleRowTextView = (TextView) convertView.findViewById(R.id.text_middlerow);
            holder.agencyEmployeeNameEditText = (EditText) convertView.findViewById(R.id.text_agency_employee_name);
            holder.firstAidIcon = (ImageView)convertView.findViewById(R.id.first_aid_icon);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);
        String forename = jsonObject.optString("Forename", "");
        String staffType = jsonObject.optString("StaffType", "");
        boolean firstAider = jsonObject.optBoolean("FirstAider", false);
        String staffID = jsonObject.optString("StaffID", "");
        String agencyEmployeeName = jsonObject.optString("AgencyEmployeeName", "");
        boolean isAgency = jsonObject.optBoolean("IsAgency", false);

        //String staffName = staffType + " - " + forename;
        String staffName = forename;

        holder.middleRowTextView.setText(staffName);
        holder.middleRowTextView.setTag(staffID);

        if (!firstAider) {
            holder.firstAidIcon.setVisibility(View.INVISIBLE);
        }
        
        if (isAgency) {
            holder.middleRowTextView.setVisibility(View.GONE);
            holder.agencyEmployeeNameEditText.setText(agencyEmployeeName);
            holder.agencyEmployeeNameEditText.setHint(forename);
            holder.agencyEmployeeNameEditText.setHintTextColor(Color.parseColor("#e8e8e8"));
        } else {
            holder.agencyEmployeeNameEditText.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView middleRowTextView;
        public EditText agencyEmployeeNameEditText;
        public ImageView firstAidIcon;
    }
}
