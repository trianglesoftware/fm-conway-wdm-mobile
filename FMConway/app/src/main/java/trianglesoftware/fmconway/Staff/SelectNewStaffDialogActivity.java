package trianglesoftware.fmconway.Staff;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.CustomAutoCompleteTextChangedListener;
import trianglesoftware.fmconway.Utilities.CustomAutoCompleteView;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectNewStaffDialogActivity extends FMConwayActivityBase {
    public CustomAutoCompleteView newStaff;
    private String shiftID;
    private String newStaffID;
    public String[] item = new String[] {"Please search..."};
    public ArrayAdapter<String> myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_select_new_staff_dialog);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID");
        }

        newStaff = (CustomAutoCompleteView)findViewById(R.id.autoComplete_NewStaff);
        newStaff.setThreshold(1);
        newStaff.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this, 1));

        myAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, item);
        newStaff.setAdapter(myAdapter);
    }

    public String[] getItemsFromDb(String searchTerm) {
        // add items on the array dynamically
        List<Staff> staff = Staff.GetStaffNotOnShift(searchTerm);
        int rowCount = staff.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (Staff record : staff) {

            item[x] = record.getForename();
            x++;
        }

        return item;
    }

    public void submitStaff(View view) {
        Staff staff = Staff.GetStaffFromName(newStaff.getText().toString());
        if(staff.staffID != null) {
            //Check if staff already on shift
            ShiftStaff currentShiftStaff = ShiftStaff.GetShiftStaff(shiftID, staff.staffID);
            if(currentShiftStaff.staffID != null)
            {
                currentShiftStaff.reasonNotOn = "";
                currentShiftStaff.notOnShift = false;
                ShiftStaff.UpdateShiftStaff(currentShiftStaff);
            }
            else {
                ShiftStaff ss = new ShiftStaff();
                ss.shiftID = shiftID;
                ss.staffID = staff.staffID;
                ShiftStaff.addShiftStaff(ss);
            }

            Intent staffList = new Intent(getApplicationContext(), SelectStaffActivity.class);
            staffList.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            staffList.putExtra("ShiftID", shiftID);
            startActivity(staffList);
        }
        else{
            Toast.makeText(getApplicationContext(), "This is not a valid staff member", Toast.LENGTH_LONG).show();
        }
    }

    public void cancelStaff(View view)
    {
        Intent staffList = new Intent(getApplicationContext(), SelectStaffActivity.class);
        staffList.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        staffList.putExtra("ShiftID", shiftID);
        startActivity(staffList);
    }
}
