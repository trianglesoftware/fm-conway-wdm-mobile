package trianglesoftware.fmconway.Staff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.File;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.StaffRecord;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectCertificateActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String staffID;
    private String staffName;
    private CertificateAdapter certificateAdapter;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            staffID = extras.getString("StaffID","");
            shiftID = extras.getString("ShiftID","");
        }

        staffName = Staff.GetStaffName(staffID);

        super.onCreate(savedInstanceState);

        ListView certificateList = (ListView) findViewById(R.id.select_certificate_list);
        certificateAdapter = new CertificateAdapter(this, getLayoutInflater());
        certificateList.setAdapter(certificateAdapter);
        certificateList.setOnItemClickListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectCertificateActivity");
        }

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Please Wait...");
        mDialog.setCancelable(false);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_certificate;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return staffName;
    }

    private void GetData() throws Exception
    {
        List<StaffRecord> staffRecords = StaffRecord.GetStaffRecords(staffID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < staffRecords.size(); i++) {
            jsonArray.put(staffRecords.get(i).getJSONObject());
        }

        certificateAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mDialog.show();

        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        StaffRecord rec = StaffRecord.GetStaffRecord(selected);
        String file = rec.imageLocation;
        String exten = "";

        int i = file.lastIndexOf('.');
        if (i > 0) {
            exten = file.substring(i + 1);
        }
        
        if (!Objects.equals(exten, "")) {
            try {
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(exten);
                Uri uri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", new File(file));
                
                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileIntent.setDataAndType(uri, type);

                startActivity(fileIntent);
            }
            catch (Exception e)
            {
                Toast.makeText(this.getApplicationContext(), "Cannot open file.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this.getApplicationContext(), "Invalid File Type.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mDialog.dismiss();
    }
}
