package trianglesoftware.fmconway.Staff;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class StaffSignOffAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public StaffSignOffAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_sign_off, null);

            holder = new ViewHolder();
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);
            holder.signOff = (Button)convertView.findViewById(R.id.sign_off);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String staffName = "";
        String staffID = "";

        if(jsonObject.has("Forename"))
        {
            staffName = jsonObject.optString("Forename");
        }

        if(jsonObject.has("StaffID"))
        {
            staffID = jsonObject.optString("StaffID");
        }

        holder.signOff.setBackgroundColor(Color.parseColor("#41AB45"));

        holder.middleRowTextView.setText(staffName);
        holder.middleRowTextView.setTag(staffID);

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView middleRowTextView;
        public Button signOff;
    }
}
