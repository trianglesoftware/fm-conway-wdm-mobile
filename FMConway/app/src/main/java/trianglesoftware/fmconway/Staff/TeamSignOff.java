package trianglesoftware.fmconway.Staff;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Main.SubmitToOffice;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;

public class TeamSignOff extends FMConwayActivity {
    private String shiftID;
    private ListView staffList;
    private StaffSignOffAdapter staffAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
        }

        super.onCreate(savedInstanceState);

        staffList = (ListView)findViewById(R.id.select_staff_list);
        staffAdapter = new StaffSignOffAdapter(this, getLayoutInflater());
        staffList.setAdapter(staffAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "TeamSignOffActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_team_sign_off;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Team Sign Off";
    }

    private void GetData() throws Exception
    {
        List<ShiftStaff> staff = ShiftStaff.GetShiftStaff(shiftID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < staff.size(); i++) {
            jsonArray.put(staff.get(i).getJSONObject());
        }

        staffAdapter.UpdateData(jsonArray);
    }

    public void signOffClick(View view)
    {
        int pos = staffList.getPositionForView((View) view.getParent());
        JSONObject selected = staffAdapter.getItem(pos);

        Intent staffHoursActivity = new Intent(getApplicationContext(), StaffHoursActivity.class);
        staffHoursActivity.putExtra("ShiftID", shiftID);
        staffHoursActivity.putExtra("StaffID", selected.optString("StaffID"));
        startActivity(staffHoursActivity);

        //ShiftStaff.SetEndTime(selected.optString("StaffID"), shiftID);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "TeamSignOffActivity");
        }
    }

    public void continueClick(View view) {
        //List<ShiftStaff> shiftStaff = ShiftStaff.GetShiftStaff(shiftID);

        finish();

        //Go to submit to office page
        Intent submit = new Intent(getApplicationContext(), SubmitToOffice.class);
        submit.putExtra("ShiftID", shiftID);
        startActivity(submit);
    }
}
