package trianglesoftware.fmconway.Staff;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class StaffHoursActivity extends FMConwayActivity {
    private String shiftID;
    private String staffID;
    private String staffName;

    private ShiftStaff shiftStaff;

    private TextView staffLabel;

    private EditText workedHours;
    private EditText workedMins;
    private EditText breakHours;
    private EditText breakMins;
    private EditText travelHours;
    private EditText travelMins;

    private Button ipvButton;
    private Button paidBreakButton;
    private Button onCallButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            staffID = extras.getString("StaffID", "");
            shiftID = extras.getString("ShiftID", "");
        }

        super.onCreate(savedInstanceState);

        staffLabel = (TextView) findViewById(R.id.staff_name);

        workedHours = (EditText) findViewById(R.id.worked_hours);
        workedMins = (EditText) findViewById(R.id.worked_mins);
        breakHours = (EditText) findViewById(R.id.break_hours);
        breakMins = (EditText) findViewById(R.id.break_mins);
        travelHours = (EditText) findViewById(R.id.travel_hours);
        travelMins = (EditText) findViewById(R.id.travel_mins);

        ipvButton = (Button) findViewById(R.id.ipv);
        onCallButton = (Button) findViewById(R.id.onCall);
        paidBreakButton = (Button) findViewById(R.id.paidBreak);

        staffName = Staff.GetStaffName(staffID);
        staffLabel.setText("Shift Hours for " + staffName);

        shiftStaff = ShiftStaff.GetShiftStaff(shiftID, staffID);
        if (shiftStaff.workedHours > 0) {
            workedHours.setText(String.valueOf(shiftStaff.workedHours));
        }
        if (shiftStaff.workedMins > 0) {
            workedMins.setText(String.valueOf(shiftStaff.workedMins));
        }
        if (shiftStaff.breakHours > 0) {
            breakHours.setText(String.valueOf(shiftStaff.breakHours));
        }
        if (shiftStaff.breakMins > 0) {
            breakMins.setText(String.valueOf(shiftStaff.breakMins));
        }
        if (shiftStaff.travelHours > 0) {
            travelHours.setText(String.valueOf(shiftStaff.travelHours));
        }
        if (shiftStaff.travelMins > 0) {
            travelMins.setText(String.valueOf(shiftStaff.travelMins));
        }
        
        CheckAllowances();
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_staff_hours;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Staff Hours";
    }

    public void paidBreakClick(View view) {
        shiftStaff.paidBreak = !shiftStaff.paidBreak;

        ShiftStaff.UpdateShiftStaff(shiftStaff);

        CheckAllowances();
    }

    public void onCallClick(View view) {
        shiftStaff.onCall = !shiftStaff.onCall;

        ShiftStaff.UpdateShiftStaff(shiftStaff);

        CheckAllowances();
    }

    public void ipvClick(View view) {
        shiftStaff.ipv = !shiftStaff.ipv;

        ShiftStaff.UpdateShiftStaff(shiftStaff);

        CheckAllowances();
    }

    public void submitClick(View view) {
        shiftStaff.workedHours = parseInt(workedHours.getText().toString(), 0);
        shiftStaff.workedMins = parseInt(workedMins.getText().toString(), 0);
        shiftStaff.breakHours = parseInt(breakHours.getText().toString(), 0);
        shiftStaff.breakMins = parseInt(breakMins.getText().toString(), 0);
        shiftStaff.travelHours = parseInt(travelHours.getText().toString(), 0);
        shiftStaff.travelMins = parseInt(travelMins.getText().toString(), 0);
        
        if (shiftStaff.workedHours == 0 && shiftStaff.workedMins == 0) { 
            Toast.makeText(getApplicationContext(), "Please enter a value.", Toast.LENGTH_LONG).show();
            return;
        }
        
        if (shiftStaff.workedMins >= 60 || shiftStaff.breakMins >= 60 || shiftStaff.travelMins >= 60) {
            Toast.makeText(getApplicationContext(), "Any minute fields must be less than or equal to 59.", Toast.LENGTH_LONG).show();
            return;
        }
        
        ShiftStaff.UpdateShiftStaff(shiftStaff);
        ShiftStaff.SetEndTime(staffID, shiftID);
        
        Intent staffSignOff = new Intent(getApplicationContext(), TeamSignOff.class);
        staffSignOff.putExtra("ShiftID", shiftID);
        startActivity(staffSignOff);
    }
    
    private int parseInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    private void CheckAllowances() {
        if (shiftStaff.ipv) {
            ipvButton.setBackgroundColor(Color.parseColor("#41ab45"));
        } else {
            ipvButton.setBackgroundColor(Color.parseColor("#CFCFC4"));
        }

        if (shiftStaff.paidBreak) {
            paidBreakButton.setBackgroundColor(Color.parseColor("#41ab45"));
        } else {
            paidBreakButton.setBackgroundColor(Color.parseColor("#CFCFC4"));
        }

        if (shiftStaff.onCall) {
            onCallButton.setBackgroundColor(Color.parseColor("#41ab45"));
        } else {
            onCallButton.setBackgroundColor(Color.parseColor("#CFCFC4"));
        }
    }
    
}
