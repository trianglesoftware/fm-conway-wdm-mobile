package trianglesoftware.fmconway.ToolboxTalk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseHelper;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;


public class ToolboxTalkActivity extends FMConwayActivity {
    private String briefingID;
    private String shiftID;
    private ListView briefingChecklistQuestionList;
    private SharedPreferences sp;
    private Button home;
    private Button back;
    private ToolboxTalkChecklistQuestionAdapter toolboxTalkChecklistQuestionAdapter;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            briefingID = extras.getString("BriefingID","");
        }

        super.onCreate(savedInstanceState);

        briefingChecklistQuestionList = (ListView)findViewById(R.id.toolbox_checklist_list);
        home = (Button)findViewById(R.id.back_to_home);
        back = (Button)findViewById(R.id.back);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        toolboxTalkChecklistQuestionAdapter = new ToolboxTalkChecklistQuestionAdapter(this, getLayoutInflater());
        briefingChecklistQuestionList.setAdapter(toolboxTalkChecklistQuestionAdapter);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(false);
            }
        });

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ToolboxTalkActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_toolbox_talk;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Toolbox Talk";
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void GetData() throws Exception
    {
        List<BriefingChecklistAnswer> briefingChecklistAnswers = BriefingChecklistAnswer.GetBriefingChecklistAnswers(briefingID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < briefingChecklistAnswers.size(); i++) {
            jsonArray.put(briefingChecklistAnswers.get(i).getJSONObject());
        }

        toolboxTalkChecklistQuestionAdapter.UpdateData(jsonArray);
    }

    public void answerYes(View view)
    {
        int pos = briefingChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = toolboxTalkChecklistQuestionAdapter.getItem(pos);

        BriefingChecklistAnswer.SetToolboxAnswer(selected.optString("ChecklistQuestionID"), briefingID, true);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ToolboxTalkActivity");
        }
    }

    public void answerNo(View view)
    {
        int pos = briefingChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = toolboxTalkChecklistQuestionAdapter.getItem(pos);

        BriefingChecklistAnswer.SetToolboxAnswer(selected.optString("ChecklistQuestionID"), briefingID, false);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ToolboxTalkActivity");
        }
    }

    private boolean CheckComplete()
    {
        List<BriefingChecklistAnswer> briefingChecklistAnswers = BriefingChecklistAnswer.GetBriefingChecklistAnswers(briefingID);

        boolean complete = true;

        for(int i = 0; i < briefingChecklistAnswers.size(); i++) {
            complete = !briefingChecklistAnswers.get(i).isNew;
        }

        return complete;
    }

    public void confirmToolboxTalk(View view)
    {
        if(CheckComplete())
        {
            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed()
    {
        if (CheckComplete()) {

            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();
        }

    }

    public void returnToMenu(final boolean menu)
    {
        if (CheckComplete()) {

            if (menu) {
                if (Objects.equals(shiftID,"")) {
                    Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
                    noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    noShiftIntent.putExtra("ShiftID", shiftID);
                    noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(noShiftIntent);
                } else {
                    Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
                    startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startIntent.putExtra("ShiftID", shiftID);
                    startIntent.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(startIntent);
                }

            }
            else
            {finish();}
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();
        }
    }
}
