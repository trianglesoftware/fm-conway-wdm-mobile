package trianglesoftware.fmconway.Enums;

public enum TaskTypeEnum {
    DEFAULT(0),
    Traffic_Count(1),
    Area_Call(2),
    Checklist(3),
    Install(4),
    Free_Text(5),
    Free_Text_And_Details(6),
    Signature(7),
    Equipment_Install(8),
    Equipment_Collection(9),
    Photos(10),
    Weather(11),
    Battery(12),
    Activity_Details(13),
    Cleansing_Log(14),
    Unloading_Process(15);

    int value;

    TaskTypeEnum(int value) {
        this.value = value;
    }

    public static TaskTypeEnum get(int value) {
        for (TaskTypeEnum v : values()) {
            if (v.value == value) {
                return v;
            }
        }
        return DEFAULT;
    }

    public int getID() {
        return value;
    }
}

