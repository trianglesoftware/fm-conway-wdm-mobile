package trianglesoftware.fmconway.MaintenanceCheck;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class MaintenanceActivity extends FMConwayActivityBase {
    private String maintenanceID;
    private String activityID;
    private String userID;
    private String shiftID;
    private EditText description;
    private ListView maintenanceChecks;
    private SharedPreferences sp;
    private MaintenanceChecklistQuestionAdapter checklistQuestionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_maintenance);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            maintenanceID = extras.getString("MaintenanceID","");
            activityID = extras.getString("ActivityID","");
            userID = extras.getString("UserID","");
            shiftID = extras.getString("ShiftID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        description = (EditText)findViewById(R.id.maintenance_detail);

        if(!Objects.equals(maintenanceID,"")) {
            MaintenanceCheck maintenanceCheck = MaintenanceCheck.GetMaintenanceCheck(maintenanceID);
            description.setText(maintenanceCheck.description);
        }

        maintenanceChecks = (ListView)findViewById(R.id.maintenance_questions_list);
        checklistQuestionAdapter = new MaintenanceChecklistQuestionAdapter(this, getLayoutInflater());
        maintenanceChecks.setAdapter(checklistQuestionAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"MaintenanceActivityGetData");
        }
    }

    @Override
    public void onBackPressed()
    {
        if (CheckComplete()) {

            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();
        }

    }

    private void GetData() throws Exception
    {
        List<MaintenanceCheckAnswer> maintenanceCheckAnswers = MaintenanceCheckAnswer.GetMaintenanceCheckAnswers(maintenanceID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < maintenanceCheckAnswers.size(); i++) {
            jsonArray.put(maintenanceCheckAnswers.get(i).getJSONObject());
        }

        checklistQuestionAdapter.UpdateData(jsonArray);
    }

    private boolean CheckComplete()
    {
        List<MaintenanceCheckAnswer> maintenanceCheckAnswers = MaintenanceCheckAnswer.GetMaintenanceCheckAnswers(maintenanceID);

        boolean complete = true;

        for(int i = 0; i < maintenanceCheckAnswers.size(); i++) {
            complete = !maintenanceCheckAnswers.get(i).isNew;
        }

        return complete;
    }

    public void answerYes(View view)
    {
        int pos = maintenanceChecks.getPositionForView((View) view.getParent());
        JSONObject selected = checklistQuestionAdapter.getItem(pos);

        MaintenanceCheckAnswer.SetMaintenanceAnswer(selected.optString("ChecklistQuestionID"), maintenanceID, true);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"MaintenanceActivityGetData");
        }
    }

    public void answerNo(View view)
    {
        int pos = maintenanceChecks.getPositionForView((View) view.getParent());
        JSONObject selected = checklistQuestionAdapter.getItem(pos);

        MaintenanceCheckAnswer.SetMaintenanceAnswer(selected.optString("ChecklistQuestionID"), maintenanceID, false);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"MaintenanceActivityGetData");
        }
    }

    public void addDetail(View view)
    {
        int pos = maintenanceChecks.getPositionForView((View) view.getParent());
        JSONObject selected = checklistQuestionAdapter.getItem(pos);

        Intent addDetailActivity = new Intent(getApplicationContext(), MaintenanceDetailActivity.class);
        addDetailActivity.putExtra("MaintenanceID", maintenanceID);
        addDetailActivity.putExtra("ChecklistQuestionID", selected.optString("ChecklistQuestionID"));
        addDetailActivity.putExtra("ActivityID", activityID);
        addDetailActivity.putExtra("ShiftID", shiftID);
        startActivity(addDetailActivity);
    }

    public void submitMaintenance(View view)
    {
        //Update
        if (CheckComplete()) {
            MaintenanceCheck.UpdateMaintenanceCheck(maintenanceID, description.getText().toString());

            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();
        }
    }
    }
