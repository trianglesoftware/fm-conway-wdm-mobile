package trianglesoftware.fmconway.MaintenanceCheck;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckDetail;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckDetailPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

/**
 * Created by Adam.Patrick on 31/08/2016.
 */
public class MaintenanceDetailActivity extends FMConwayActivityBase {
    private String maintenanceID;
    private String checklistQuestionID;
    private String activityID;
    private String shiftID;
    private EditText maintenanceDetail;
    private SharedPreferences sp;

    private MaintenanceCheckDetail detail;
    private GridView gridView;
    private ArrayList<MaintenanceCheckDetailPhoto> photoList;
    private MaintenanceCheckImageAdapter adapter;

    private String filepath;
    private final int REQUEST_CODE = 10;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_maintenance_detail);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            maintenanceID = extras.getString("MaintenanceID","");
            checklistQuestionID = extras.getString("ChecklistQuestionID","");
            activityID = extras.getString("ActivityID","");
            shiftID = extras.getString("ShiftID");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        maintenanceDetail = (EditText) findViewById(R.id.maintenance_detail_description);

        detail = MaintenanceCheckDetail.GetMaintenanceCheckDetail(maintenanceID, checklistQuestionID);

        if (detail != null)
        {
            maintenanceDetail.setText(detail.getDescription());
        }

        gridView = (GridView)findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = MaintenanceCheckDetailPhoto.getPhotosForMaintenanceCheckDetail(maintenanceID, checklistQuestionID);

        adapter = new MaintenanceCheckImageAdapter(this, photoList);

        gridView.setAdapter(adapter);
    }

    public void submitMaintenanceDetail(View view) {
        if (photoList.size() == 0) {
            Toast.makeText(getApplicationContext(), "At least one photo is required", Toast.LENGTH_LONG).show();
            return;
        }

        if (detail != null) {
            MaintenanceCheckDetail.UpdateMaintenanceCheckDetails(maintenanceID, checklistQuestionID, maintenanceDetail.getText().toString());
        } else {
            MaintenanceCheckDetail newDetail = new MaintenanceCheckDetail();

            newDetail.setChecklistQuestionID(checklistQuestionID);
            newDetail.setMaintenanceCheckID(maintenanceID);
            newDetail.setDescription(maintenanceDetail.getText().toString());
            newDetail.setActivityID(activityID);

            MaintenanceCheckDetail.AddMaintenanceCheckDetail(newDetail);
        }

        finish();
    }

    public void addMaintenancePhoto(View view)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                AddData(filepath);

                photoList = MaintenanceCheckDetailPhoto.getPhotosForMaintenanceCheckDetail(maintenanceID, checklistQuestionID);
                adapter = new MaintenanceCheckImageAdapter(this, photoList);
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        }
    }

    private void AddData(String imageLocation) {

        if(detail != null)
        {
            MaintenanceCheckDetail newDetail = new MaintenanceCheckDetail();

            newDetail.setChecklistQuestionID(checklistQuestionID);
            newDetail.setMaintenanceCheckID(maintenanceID);
            newDetail.setDescription(maintenanceDetail.getText().toString());
            newDetail.setActivityID(activityID);

            MaintenanceCheckDetail.AddMaintenanceCheckDetail(newDetail);
        }

        LocationManager lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude =  0;
        double latitude =  0;

        if(location != null) {
            longitude =  location.getLongitude();
            latitude =  location.getLatitude();
        }

        MaintenanceCheckDetailPhoto photo = new MaintenanceCheckDetailPhoto();
        photo.setChecklistQuestionID(checklistQuestionID);
        photo.setMaintenanceCheckID(maintenanceID);
        photo.setLongitude(longitude);
        photo.setLatitude(latitude);
        photo.setImageLocation(imageLocation);
        photo.setActivityID(activityID);
        photo.setUserID(sp.getString("UserID", ""));
        photo.shiftID = shiftID;

        MaintenanceCheckDetailPhoto.addMaintenanceCheckDetailPhoto(photo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putString("filepath", filepath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
    }

}
