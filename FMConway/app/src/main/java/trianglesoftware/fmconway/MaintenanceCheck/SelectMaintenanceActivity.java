package trianglesoftware.fmconway.MaintenanceCheck;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheckAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class SelectMaintenanceActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String activityID;
    private String userID;
    private MaintenanceAdapter maintenanceAdapter;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            activityID = extras.getString("ActivityID","");
            userID = extras.getString("UserID","");
        }
        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        if (!Objects.equals(userID,""))
        {
            userID = sp.getString("UserID", "");
        }

        ListView maintenanceList = (ListView) findViewById(R.id.select_maintenance_list);
        maintenanceAdapter = new MaintenanceAdapter(this, getLayoutInflater());
        maintenanceList.setAdapter(maintenanceAdapter);
        maintenanceList.setOnItemClickListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectMaintenanceActivity");
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectMaintenanceActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_maintenance;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Maintenance Checks";
    }

    private void GetData() throws Exception
    {
        List<MaintenanceCheck> maintenanceChecks = MaintenanceCheck.GetMaintenanceChecksForActivity(activityID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < maintenanceChecks.size(); i++) {
            jsonArray.put(maintenanceChecks.get(i).getJSONObject());
        }

        maintenanceAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Intent maintenanceActivity = new Intent(getApplicationContext(), MaintenanceActivity.class);
        maintenanceActivity.putExtra("MaintenanceID", selected);
        maintenanceActivity.putExtra("ActivityID", activityID);
        maintenanceActivity.putExtra("ShiftID", shiftID);
        maintenanceActivity.putExtra("UserID", userID);
        startActivity(maintenanceActivity);
    }

    public void addMaintenance(View view)
    {
        //Create Maintenance check and checklist and pass in maintenanceID
        MaintenanceCheck maintenanceCheck = new MaintenanceCheck();
        maintenanceCheck.activityID = activityID;
        maintenanceCheck.time = new Date();
        MaintenanceCheck.AddMaintenanceCheck(maintenanceCheck);

        MaintenanceCheck latestCheck = MaintenanceCheck.GetLatestCheck();

        List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForMaintenanceChecklist();
        for (int j = 0; j < checklistQuestions.size(); j++) {
            MaintenanceCheckAnswer mca = new MaintenanceCheckAnswer();
            mca.maintenanceCheckID = latestCheck.maintenanceCheckID;
            mca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
            mca.activityID = activityID;
            //mca.setAnswer(false); // Leave as null?
            MaintenanceCheckAnswer.addMaintenanceCheckAnswer(mca);
        }

        Intent maintenanceActivity = new Intent(getApplicationContext(), MaintenanceActivity.class);
        maintenanceActivity.putExtra("MaintenanceID", latestCheck.getMaintenanceCheckID());
        maintenanceActivity.putExtra("ActivityID", activityID);
        maintenanceActivity.putExtra("ShiftID", shiftID);
        maintenanceActivity.putExtra("UserID", userID);
        startActivity(maintenanceActivity);

        //finish();
    }

    public void backToTask(View view)
    {
        finish();
    }
}
