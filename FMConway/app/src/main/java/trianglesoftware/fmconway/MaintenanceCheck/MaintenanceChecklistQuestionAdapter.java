package trianglesoftware.fmconway.MaintenanceCheck;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 11/03/2016.
 */
class MaintenanceChecklistQuestionAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public MaintenanceChecklistQuestionAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_checklist_question_maintenance, null);

            holder = new ViewHolder();
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);
            holder.addDetail = (Button)convertView.findViewById(R.id.add_detail);
            holder.answerYes = (Button)convertView.findViewById(R.id.answer_yes);
            holder.answerNo = (Button)convertView.findViewById(R.id.answer_no);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String checklistQuestion = "";
        String checklistQuestionID = "";
        boolean answer = false;
        boolean isNew = false;

        if(jsonObject.has("Question"))
        {
            checklistQuestion = jsonObject.optString("Question");
        }

        if(jsonObject.has("ChecklistQuestionID"))
        {
            checklistQuestionID = jsonObject.optString("ChecklistQuestionID");
        }

        if(jsonObject.has("Answer"))
        {
            answer = jsonObject.optBoolean("Answer");
        }

        if(jsonObject.has("IsNew"))
        {
            isNew = jsonObject.optBoolean("IsNew");
        }

        holder.middleRowTextView.setTag(checklistQuestionID);
        holder.middleRowTextView.setText(checklistQuestion);

        if (isNew)
        {
            holder.answerNo.setBackgroundColor(Color.parseColor("#d6d7d7"));
            holder.answerYes.setBackgroundColor(Color.parseColor("#d6d7d7"));
        }
        else {
            if (answer) {
                holder.answerYes.setBackgroundColor(Color.parseColor("#41ab45"));
                holder.answerNo.setBackgroundColor(Color.parseColor("#d6d7d7"));
            } else {
                holder.answerYes.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.answerNo.setBackgroundColor(Color.parseColor("#FF6961"));
            }
        }

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView middleRowTextView;
        public Button answerYes;
        public Button answerNo;
        public Button addDetail;
    }
}
