package trianglesoftware.fmconway.MaintenanceCheck;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
class MaintenanceAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public MaintenanceAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_standard, null);

            holder = new ViewHolder();
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String maintenanceDescription = "";
        String maintenanceTime = "";
        String maintenanceID = "";

        if(jsonObject.has("Description"))
        {
            maintenanceDescription = jsonObject.optString("Description");
        }

        if(jsonObject.has("Time"))
        {
            maintenanceTime = jsonObject.optString("Time");
        }

        if(jsonObject.has("MaintenanceCheckID"))
        {
            maintenanceID = jsonObject.optString("MaintenanceCheckID");
        }

        holder.middleRowTextView.setTag(maintenanceID);

        if (!Objects.equals(maintenanceDescription, "")) {
            holder.middleRowTextView.setText(String.format("%s - %s", maintenanceTime, maintenanceDescription));
        }
        else
        {
            holder.middleRowTextView.setText(maintenanceTime);
        }

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView middleRowTextView;
    }
}
