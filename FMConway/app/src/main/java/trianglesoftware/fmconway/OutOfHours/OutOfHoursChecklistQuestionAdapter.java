package trianglesoftware.fmconway.OutOfHours;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

public class OutOfHoursChecklistQuestionAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private JSONArray mJsonArray;


    public OutOfHoursChecklistQuestionAdapter(Context context, LayoutInflater inflater){
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }


    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RiskAssessmentViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_riskassessment_checklist_question, null);

            holder = new RiskAssessmentViewHolder();
            holder.question = (TextView) convertView.findViewById(R.id.question);
            holder.low = (Button) convertView.findViewById(R.id.low);
            holder.low.setTag(2);
            holder.medium = (Button) convertView.findViewById(R.id.medium);
            holder.medium.setTag(1);
            holder.high = (Button) convertView.findViewById(R.id.high);
            holder.high.setTag(0);

            convertView.setTag(holder);
        } else {
            holder = (RiskAssessmentViewHolder) convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String checklistQuestion = jsonObject.optString("Question", "");
        String checklistQuestionID = jsonObject.optString("ChecklistQuestionID", "");
        int answer = jsonObject.optInt("Answer", 0);
        boolean isNew = jsonObject.optBoolean("IsNew", false);

        holder.question.setTag(checklistQuestionID);
        holder.question.setText(checklistQuestion);

        if (isNew) {
            holder.low.setBackgroundColor(Color.parseColor("#d6d7d7"));
            holder.medium.setBackgroundColor(Color.parseColor("#d6d7d7"));
            holder.high.setBackgroundColor(Color.parseColor("#d6d7d7"));
        } else {
            if (answer == 0) {
                holder.low.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.medium.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.high.setBackgroundColor(Color.parseColor("#41ab45"));
            } else if (answer == 1) {
                holder.low.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.medium.setBackgroundColor(Color.parseColor("#41ab45"));
                holder.high.setBackgroundColor(Color.parseColor("#d6d7d7"));
            } else if (answer == 2) {
                holder.low.setBackgroundColor(Color.parseColor("#41ab45"));
                holder.medium.setBackgroundColor(Color.parseColor("#d6d7d7"));
                holder.high.setBackgroundColor(Color.parseColor("#d6d7d7"));
            }
        }

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class RiskAssessmentViewHolder {
        public TextView question;
        public Button low;
        public Button medium;
        public Button high;
        public Button no;
    }
}
