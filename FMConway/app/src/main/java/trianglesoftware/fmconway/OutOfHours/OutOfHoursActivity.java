package trianglesoftware.fmconway.OutOfHours;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityMethodStatement;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityPriority;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivityType;
import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.Customer;
import trianglesoftware.fmconway.Database.DatabaseObjects.Depot;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.MethodStatement;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Task.FreeTextDetailPhotoActivity;
import trianglesoftware.fmconway.Task.MethodStatementActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

public class OutOfHoursActivity extends FMConwayActivityBase {
    private String userID;
    private SharedPreferences sp;
    private List<String> methodStatementIDs;

    // controls
    private TextView headerButton;
    private Button backButton;
    private Button homeButton;
    private Spinner customerSpinner;
    private Spinner depotSpinner;
    private EditText startDateEditText;
    private EditText startTimeEditText;
    private EditText endTimeEditText;
    private Spinner activityTypeSpinner;
    private Spinner activityPrioritySpinner;
    private EditText addressLine1EditText;
    private EditText addressLine2EditText;
    private EditText boroughEditText;
    private EditText cityEditText;
    private EditText countyEditText;
    private EditText postcodeEditText;
    private ListView methodStatementListView;
    private OutOfHoursMethodStatementAdapter methodStatementAdapter;

    // request codes
    private int methodStatementActivityRequestCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outofhours);

        Bundle extras = getIntent().getExtras();
        userID = extras.getString("UserID");

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        methodStatementIDs = new LinkedList<>();

        headerButton = findViewById(R.id.header_title);
        headerButton.setText("Add Out Of Hours");

        backButton = findViewById(R.id.back);
        homeButton = findViewById(R.id.back_to_home);

        customerSpinner = findViewById(R.id.customer);
        depotSpinner = findViewById(R.id.depot);
        startDateEditText = findViewById(R.id.start_date);
        startTimeEditText = findViewById(R.id.start_time);
        endTimeEditText = findViewById(R.id.end_time);
        activityTypeSpinner = findViewById(R.id.job_type);
        activityPrioritySpinner = findViewById(R.id.activity_priority);
        addressLine1EditText = findViewById(R.id.address_line_1);
        addressLine2EditText = findViewById(R.id.address_line_2);
        boroughEditText = findViewById(R.id.borough);
        cityEditText = findViewById(R.id.city);
        countyEditText = findViewById(R.id.county);
        postcodeEditText = findViewById(R.id.postcode);

        methodStatementListView = findViewById(R.id.method_statements_list);

        // nav buttons
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
                startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startIntent.putExtra("UserID", sp.getString("UserID", ""));
                startActivity(startIntent);
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // adapters
        List<Customer> customers = Customer.getLookupItems();
        ArrayAdapter<Customer> customerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, customers);
        customerSpinner.setAdapter(customerAdapter);

        List<Depot> depots = Depot.getLookupItems();
        ArrayAdapter<Depot> depotAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, depots);
        depotSpinner.setAdapter(depotAdapter);

        List<ActivityType> activityTypes = ActivityType.getLookupItems();
        ArrayAdapter<ActivityType> activityTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, activityTypes);
        activityTypeSpinner.setAdapter(activityTypeAdapter);

        List<ActivityPriority> activityPriorities = ActivityPriority.getLookupItems(null);
        ArrayAdapter<ActivityPriority> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, activityPriorities);
        activityPrioritySpinner.setAdapter(adapter);

        // method statements
        methodStatementAdapter = new OutOfHoursMethodStatementAdapter(this, getLayoutInflater());
        methodStatementListView = findViewById(R.id.method_statements_list);
        methodStatementListView.setAdapter(methodStatementAdapter);

        // load
        String today = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        startDateEditText.setText(today);
        startTimeEditText.setText("09:00");
        endTimeEditText.setText("17:00");

        // debug
        /*customerSpinner.setSelection(1);
        depotSpinner.setSelection(1);
        activityTypeSpinner.setSelection(2);
        addressLine1EditText.setText("asdasd");
        methodStatementIDs.add("5B6F5026-7DB5-4237-BBC4-ECEF8A7B0623".toLowerCase());*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshMethodStatements();
    }

    public void refreshMethodStatements() {
        List<MethodStatement> methodStatements = new LinkedList<>();

        for (MethodStatement methodStatement : MethodStatement.getAll()) {
            if (methodStatementIDs.contains(methodStatement.methodStatementID)) {
                methodStatements.add(methodStatement);
            }
        }

        methodStatementAdapter.items = methodStatements;
        methodStatementAdapter.notifyDataSetChanged();

        FMConwayUtils.setListViewHeightBasedOnChildren(methodStatementListView);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void addMethodStatements(View view) {
        Intent methodStatementActivity = new Intent(getApplicationContext(), MethodStatementActivity.class);
        methodStatementActivity.putExtra("MethodStatementIDs", methodStatementIDs.toArray(new String[0]));
        startActivityForResult(methodStatementActivity, methodStatementActivityRequestCode);
    }

    public void rowClick(View view) {
        final int position = methodStatementListView.getPositionForView(view);
        MethodStatement selectedItem = methodStatementAdapter.getItem(position);

        FMConwayUtils.confirmDialog(this, "Remove this method statement?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                methodStatementIDs.remove(position);
                refreshMethodStatements();
            }
        });
    }

    public void createShift(View view) {
        if (!validate()) {
            return;
        }

        FMConwayUtils.confirmDialog(this, "Create out of hours shift, are you sure?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                save();
            }
        });
    }

    public void save() {
        try {
            Customer customer = (Customer) customerSpinner.getSelectedItem();
            Depot depot = (Depot) depotSpinner.getSelectedItem();
            String startDateString = startDateEditText.getText().toString();
            String startTimeString = startTimeEditText.getText().toString();
            String endTimeString = endTimeEditText.getText().toString();
            ActivityType activityType = (ActivityType) activityTypeSpinner.getSelectedItem();
            ActivityPriority activityPriority = (ActivityPriority) activityPrioritySpinner.getSelectedItem();
            String addressLine1 = addressLine1EditText.getText().toString();
            String addressLine2 = addressLine2EditText.getText().toString();
            String borough = boroughEditText.getText().toString();
            String city = cityEditText.getText().toString();
            String county = countyEditText.getText().toString();
            String postcode = postcodeEditText.getText().toString();

            // get startDate and endDate
            SimpleDateFormat startDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Date startDate = startDateFormat.parse(startDateString);
            Date endDate = startDate;

            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date startTime = timeFormat.parse(startTimeString);
            Date endTime = timeFormat.parse(endTimeString);

            // start date
            Calendar timeCalendar = Calendar.getInstance();
            timeCalendar.setTime(startTime);

            Calendar dateCalendar = Calendar.getInstance();
            dateCalendar.setTime(startDate);
            dateCalendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
            dateCalendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
            dateCalendar.set(Calendar.SECOND, 0);
            dateCalendar.set(Calendar.MILLISECOND, 0);

            startDate = dateCalendar.getTime();

            // end date
            timeCalendar.setTime(endTime);

            dateCalendar.setTime(endDate);
            dateCalendar.add(Calendar.DAY_OF_MONTH, endTime.before(startTime) ? 1 : 0);
            dateCalendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
            dateCalendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));

            endDate = dateCalendar.getTime();

            // activity
            Activity activity = new Activity();
            activity.activityID = UUID.randomUUID().toString();
            activity.activityTypeID = activityType.activityTypeID;
            activity.name = "";
            activity.customer = customer.customerName;
            activity.customerPhoneNumber = customer.phoneNumber;
            activity.contract = ""; // not known until job is assigned
            activity.contractNumber = ""; // not known until job is assigned
            activity.scheme = "";
            activity.scopeOfWork = "";
            activity.areaCallNumber = null;
            activity.areaCallProtocol = null;
            activity.firstCone = "";
            activity.lastCone = "";
            activity.distance = 0;
            activity.road = "";
            activity.cWayDirection = "";
            activity.installationTime = new Date();
            activity.removalTime = new Date();
            activity.healthAndSafety = "";
            activity.worksInstructionNumber = "0"; // default
            activity.jobPackID = UUID.randomUUID().toString();
            activity.worksInstructionComments = "";
            activity.maintenanceChecklistReminderInMinutes = 0;
            activity.siteName = null;
            activity.clientReferenceNumber = "";
            activity.activityPriorityID = activityPriority.activityPriorityID;
            activity.operator = null;
            activity.customerID = customer.customerID;
            activity.isOutOfHours = true;
            activity.workInstructionStartDate = startDate;
            activity.workInstructionFinishDate = endDate;
            activity.depotID = depot.depotID;
            activity.orderNumber = 1;

            // briefings and method statements
            List<Briefing> briefings = new LinkedList<>();
            List<ActivityMethodStatement> activityMethodStatements = new LinkedList<>();

            for (String methodStatementID : methodStatementIDs) {
                MethodStatement methodStatement = MethodStatement.get(methodStatementID);

                Briefing briefing = new Briefing();
                briefing.briefingID = UUID.randomUUID().toString();
                briefing.briefingTypeID = 4; // method statement
                briefing.jobPackID = activity.jobPackID;
                briefing.name = methodStatement.methodStatementTitle;
                briefing.details = "";
                briefing.documentID = methodStatement.documentID;
                briefing.imageLocation = null;
                briefing.documentName = methodStatement.documentName;
                briefing.contentType = methodStatement.contentType;
                briefing.userID = sp.getString("UserID", "");

                // copy method statements imageLocation to briefings imageLocation
                if (methodStatement.imageLocation != null) {
                    String timeStamp = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
                    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "FMConway" + File.separator + timeStamp);

                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {
                            Toast.makeText(getApplicationContext(), "Unable to create data directory.", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }

                    String filename = methodStatement.documentName;
                    filename = filename.replace(" ", "_");
                    filename = filename.replace("&", "");

                    File mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename);

                    try {
                        FMConwayUtils.copyFile(methodStatement.imageLocation, mediaFile.getPath());
                        briefing.imageLocation = mediaFile.getPath();
                    } catch (Exception ex) {
                        Toast.makeText(getApplicationContext(), "Unable to copy method statement.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                briefings.add(briefing);

                ActivityMethodStatement activityMethodStatement = new ActivityMethodStatement();
                activityMethodStatement.activityMethodStatementID = UUID.randomUUID().toString();
                activityMethodStatement.activityID = activity.activityID;
                activityMethodStatement.methodStatementID = methodStatement.methodStatementID;
                activityMethodStatement.isActive = true;

                activityMethodStatements.add(activityMethodStatement);
            }

            // job packs
            JobPack jobPack = new JobPack();
            jobPack.jobPackID = activity.jobPackID;
            jobPack.shiftID = UUID.randomUUID().toString();
            jobPack.jobPackName = ""; // not known until job is assigned

            // shift progresses
            List<String> shiftProgressStatuses = new LinkedList<>();
            shiftProgressStatuses.add("NotStarted");
            shiftProgressStatuses.add("ChecksCompleted");
            shiftProgressStatuses.add("TasksStarted");
            shiftProgressStatuses.add("Completed");
            shiftProgressStatuses.add("Cancelled");

            List<ShiftProgress> shiftStaffs = new LinkedList<>();
            for (String shiftProgressStatus : shiftProgressStatuses) {
                ShiftProgress shiftProgress = new ShiftProgress();
                shiftProgress.shiftID = jobPack.shiftID;
                shiftProgress.progress = shiftProgressStatus;

                Calendar cal = Calendar.getInstance();
                cal.set(1, 1, 2, 23, 58, 45);

                if (shiftProgressStatus.equals("NotStarted")) {
                    shiftProgress.createdDate = new Date();
                    shiftProgress.sent = false;
                } else {
                    // for some reason the "sent" default value is true then switched to false when a date is set
                    shiftProgress.createdDate = cal.getTime();
                    shiftProgress.sent = true;
                }

                shiftStaffs.add(shiftProgress);
            }

            // shift staff
            ShiftStaff shiftStaff = new ShiftStaff();
            shiftStaff.staffID = sp.getString("StaffID", "");
            shiftStaff.shiftID = jobPack.shiftID;
            shiftStaff.notOnShift = false;
            shiftStaff.reasonNotOn = "";
            shiftStaff.startTime = null;
            shiftStaff.endTime = null;
            shiftStaff.firstAider = false;
            shiftStaff.workedHours = 0;
            shiftStaff.workedMins = 0;
            shiftStaff.breakHours = 0;
            shiftStaff.breakMins = 0;
            shiftStaff.travelHours = 0;
            shiftStaff.travelMins = 0;
            shiftStaff.ipv = false;
            shiftStaff.paidBreak = false;
            shiftStaff.onCall = false;
            shiftStaff.agencyEmployeeName = "";

            // shift
            Shift shift = new Shift();
            shift.shiftID = jobPack.shiftID;
            shift.dayShift = true;
            shift.userID = sp.getString("UserID", "");
            shift.shiftDate = startDate;
            shift.shiftCompletionStatus = 0;
            shift.isCancelled = false;
            shift.shiftDetails = "Out of Hours"; // normally would be the ContractTitle
            shift.shiftTypes = "";

            String address = "";
            address += !FMConwayUtils.isNullOrWhitespace(addressLine1) ? " " + addressLine1 : "";
            address += !FMConwayUtils.isNullOrWhitespace(addressLine2) ? " " + addressLine2 : "";
            address += !FMConwayUtils.isNullOrWhitespace(borough) ? " " + borough : "";
            address += !FMConwayUtils.isNullOrWhitespace(city) ? " " + city : "";
            address += !FMConwayUtils.isNullOrWhitespace(county) ? " " + county : "";
            address += !FMConwayUtils.isNullOrWhitespace(postcode) ? " " + postcode : "";
            shift.address = address.trim();

            shift.trafficManagementActivities = "";
            shift.cancellationReason = "";
            shift.srwNumber = "";
            shift.projCode = "";
            shift.addressLine1 = addressLine1;
            shift.addressLine2 = addressLine2;
            shift.borough = borough;
            shift.city = city;
            shift.county = county;
            shift.postcode = postcode;
            shift.updatedDate = null;

            // tasks
            List<Task> tasks = new LinkedList<>();

            if (activityType.name.equals("Jetting")) {
                tasks.add(createTask(UUID.randomUUID().toString(), "Start Travel", 1, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Arrive on Site", 2, 10, null, activity.activityID, true, true, false, false, false, false, "ShowNotes", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Risk Assessment", 3, 3, "dbf40fbd-f66e-42b5-a5de-7126dfb311cf", activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Start Work", 4, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Loading Process", 5, 10, null, activity.activityID, true, false, true, false, false, false, "ShowNotes|NotesRequired", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Activity Details", 6, 13, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Cleansing Log", 7, 14, null, activity.activityID, false, false, false, false, false, false, "", false));
                tasks.add(createTask(UUID.randomUUID().toString(), "Unloading Process", 8, 15, null, activity.activityID, false, false, false, false, false, false, "", false));
                tasks.add(createTask(UUID.randomUUID().toString(), "Leave Site", 9, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "End Travel", 10, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
            } else if (activityType.name.equals("Tankering")) {
                tasks.add(createTask(UUID.randomUUID().toString(), "Start Travel", 1, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Arrive on Site", 2, 10, null, activity.activityID, true, true, false, false, false, false, "ShowNotes", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Risk Assessment", 3, 3, "a00fabb1-783a-47c3-894a-25e0531e5e45", activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Start Work", 4, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Loading Process", 5, 10, null, activity.activityID, true, false, true, false, false, false, "ShowNotes|NotesRequired", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Activity Details", 6, 13, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Unloading Process", 7, 15, null, activity.activityID, false, false, false, false, false, false, "", false));
                tasks.add(createTask(UUID.randomUUID().toString(), "Leave Site", 8, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "End Travel", 9, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
            } else if (activityType.name.equals("Jetting New")) {
                String checklistID = Checklist.getJettingNewRiskAssessmentID();

                tasks.add(createTask(UUID.randomUUID().toString(), "Risk Assessment", 1, 3, checklistID, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Start Travel", 2, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Arrive on Site", 3, 10, null, activity.activityID, true, true, false, false, false, false, "ShowNotes", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Start Work", 4, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Loading Process", 5, 10, null, activity.activityID, true, false, true, false, false, false, "ShowNotes|NotesRequired", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Activity Details", 6, 16, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "Cleansing Log", 7, 14, null, activity.activityID, false, false, false, false, false, false, "", false));
                tasks.add(createTask(UUID.randomUUID().toString(), "Unloading Process", 8, 15, null, activity.activityID, false, false, false, false, false, false, "", false));
                tasks.add(createTask(UUID.randomUUID().toString(), "Leave Site", 9, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
                tasks.add(createTask(UUID.randomUUID().toString(), "End Travel", 10, 5, null, activity.activityID, false, false, false, false, false, false, "", true));
            }

            // save
            Activity.addActivity(activity);

            for (Briefing briefing : briefings) {
                Briefing.addBriefing(briefing);
                if (briefing.imageLocation != null) {
                    Briefing.SetImageLocation(briefing, briefing.imageLocation);
                }
            }

            for (ActivityMethodStatement activityMethodStatement : activityMethodStatements) {
                ActivityMethodStatement.save(activityMethodStatement);
            }

            JobPack.addJobPack(jobPack);

            for (ShiftProgress shiftProgress : shiftStaffs) {
                ShiftProgress.addShiftProgress(shiftProgress);
            }

            ShiftStaff.addShiftStaff(shiftStaff);

            Shift.addShift(shift);

            for (Task task : tasks) {
                Task.addTask(task);
            }

            // open shift list
            Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
            startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startIntent.putExtra("UserID", sp.getString("UserID", ""));
            startActivity(startIntent);
            finish();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "Save OutOfHoursActivity");
            Toast.makeText(getApplicationContext(), "An error occurred - unable to save.", Toast.LENGTH_SHORT).show();
        }
    }

    public Task createTask(String taskID, String name, int taskOrder, int taskTypeID, String checklistID, String activityID, boolean photosRequired, boolean showDetails,
                           boolean showClientSignature, boolean clientSignatureRequired, boolean showStaffSignature, boolean staffSignatureRequired, String tag, boolean isMandatory) {
        Task task = new Task();
        task.taskID = taskID;
        task.name = name;
        task.taskOrder = taskOrder;
        task.taskTypeID = taskTypeID;
        task.areaID = "null";
        task.checklistID = checklistID;
        task.activityID = activityID;
        task.startTime = null;
        task.endTime = null;
        task.notes = "";
        task.trafficCount = null;
        task.photosRequired = photosRequired;
        task.showDetails = showDetails;
        task.showClientSignature = showClientSignature;
        task.clientSignatureRequired = clientSignatureRequired;
        task.showStaffSignature = showStaffSignature;
        task.staffSignatureRequired = staffSignatureRequired;
        task.tag = tag;
        task.longitude = 0;
        task.latitude = 0;
        task.weatherConditionID = null;
        task.isMandatory = isMandatory;

        return task;
    }

    public boolean validate() {
        Customer customer = (Customer) customerSpinner.getSelectedItem();
        if (customer.customerID == null) {
            Toast.makeText(getApplicationContext(), "Customer is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        Depot depot = (Depot) depotSpinner.getSelectedItem();
        if (depot.depotID == null) {
            Toast.makeText(getApplicationContext(), "Depot is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        String startTimeString = startTimeEditText.getText().toString();
        if (FMConwayUtils.isNullOrWhitespace(startTimeString)) {
            Toast.makeText(getApplicationContext(), "Start Time is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        Pattern timePattern = Pattern.compile("^\\d\\d:\\d\\d$", Pattern.UNICODE_CASE);
        if (!timePattern.matcher(startTimeString).matches()) {
            Toast.makeText(getApplicationContext(), "Start Time format must be HH:MM", Toast.LENGTH_LONG).show();
            return false;
        }

        String endTimeString = endTimeEditText.getText().toString();
        if (FMConwayUtils.isNullOrWhitespace(endTimeString)) {
            Toast.makeText(getApplicationContext(), "End Time is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!timePattern.matcher(endTimeString).matches()) {
            Toast.makeText(getApplicationContext(), "End Time format must be HH:MM", Toast.LENGTH_LONG).show();
            return false;
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        try {
            timeFormat.parse(startTimeString);
            timeFormat.parse(endTimeString);
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Unable to parse start / end time", Toast.LENGTH_LONG).show();
            return false;
        }

        ActivityType activityType = (ActivityType) activityTypeSpinner.getSelectedItem();
        if (activityType.activityTypeID == null) {
            Toast.makeText(getApplicationContext(), "Job Type is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        List<String> acceptedActivityTypes = new LinkedList<>();
        acceptedActivityTypes.add("Tankering");
        acceptedActivityTypes.add("Jetting");
        acceptedActivityTypes.add("Jetting New");
        acceptedActivityTypes.add("Cyclical Works");

        if (!acceptedActivityTypes.contains(activityType.name)) {
            Toast.makeText(getApplicationContext(), "Out of hours '" + activityType.name + "' job type is not currently supported", Toast.LENGTH_LONG).show();
            return false;
        }

        ActivityPriority activityPriority = (ActivityPriority) activityPrioritySpinner.getSelectedItem();
        if (activityPriority.activityPriorityID == null) {
            Toast.makeText(getApplicationContext(), "Activity Priority is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        String addressLine1 = addressLine1EditText.getText().toString();
        if (FMConwayUtils.isNullOrWhitespace(addressLine1)) {
            Toast.makeText(getApplicationContext(), "Address Line 1 is a required field", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == methodStatementActivityRequestCode) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();

                String[] methodStatementIDs = extras.getStringArray("MethodStatementIDs");
                String[] methodStatementTitles = extras.getStringArray("MethodStatementTitles");

                this.methodStatementIDs = new LinkedList<>(Arrays.asList(methodStatementIDs));
                refreshMethodStatements();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putStringArray("methodStatementIDs", methodStatementIDs.toArray(new String[0]));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        methodStatementIDs = new LinkedList<>(Arrays.asList(savedInstanceState.getStringArray("methodStatementIDs")));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
