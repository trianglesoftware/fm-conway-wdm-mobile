package trianglesoftware.fmconway.OutOfHours;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;

public class OutOfHoursChecklistActivity extends FMConwayActivityBase {
    private String shiftID;
    private String oohChecklistID;
    private String outOfHoursID;
    private String checklistID;
    private SharedPreferences sp;
    private OutOfHoursChecklistQuestionAdapter oohChecklistQuestionAdapter;
    private ListView oohChecklistQuestionList;
    private Button home;
    private Button back;
    private TextView header;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_ooh_checklist);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            outOfHoursID = extras.getString("OutOfHoursID", "");
            checklistID = extras.getString("ChecklistID", "");
            oohChecklistID = extras.getString("OOHChecklistID", "");
        }
        sp = PreferenceManager.getDefaultSharedPreferences(this);

        oohChecklistQuestionList = (ListView) findViewById(R.id.ooh_checklist_list);

        oohChecklistQuestionAdapter = new OutOfHoursChecklistQuestionAdapter(this, getLayoutInflater());
        oohChecklistQuestionList.setAdapter(oohChecklistQuestionAdapter);

        home = (Button) findViewById(R.id.back_to_home);
        back = (Button) findViewById(R.id.back);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(false);
            }
        });

        header = (TextView) findViewById(R.id.header_title);
        if (header != null) {
            header.setText("Add Risk Assessment");
        }

        if (oohChecklistID == null || oohChecklistID == "") {
            createChecklist();
        }

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "OOHChecklistActivity");
        }
    }

    private void createChecklist() {
        if (checklistID == null || checklistID != "") {
            OutOfHoursChecklist c = new OutOfHoursChecklist();
            c.oohChecklistID = UUID.randomUUID().toString();
            c.outOfHoursID = outOfHoursID;
            c.checklistID = checklistID;

            oohChecklistID = OutOfHoursChecklist.addOOHChecklist(c);

            if (oohChecklistID != null) {
                List<ChecklistQuestion> questions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklistID);
                for (ChecklistQuestion q : questions) {
                    OutOfHoursChecklistAnswer a = new OutOfHoursChecklistAnswer();
                    a.oohChecklistAnswerID = UUID.randomUUID().toString();
                    a.oohChecklistID = oohChecklistID;
                    a.checklistQuestionID = q.checklistQuestionID;
                    a.reason = "";
                    a.time = new Date();

                    OutOfHoursChecklistAnswer.addOOHChecklistAnswer(a);
                }
            }
        }
    }

    protected String setShiftID() {
        return shiftID;
    }


    private void GetData() throws Exception {
        List<OutOfHoursChecklistAnswer> oohChecklistAnswers = OutOfHoursChecklistAnswer.GetOOHChecklistAnswers(oohChecklistID);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < oohChecklistAnswers.size(); i++) {
            jsonArray.put(oohChecklistAnswers.get(i).getJSONObject());
        }

        oohChecklistQuestionAdapter.UpdateData(jsonArray);
    }

    public void riskAssessmentAnswer(View view) {
        int pos = oohChecklistQuestionList.getPositionForView((View) view.getParent());
        JSONObject selected = oohChecklistQuestionAdapter.getItem(pos);

        String oohChecklistAnswerID = selected.optString("OOHChecklistAnswerID");
        int answer = (int) view.getTag();

        OutOfHoursChecklistAnswer.SetAnswer(selected.optString("ChecklistQuestionID"), oohChecklistID, answer);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "OOHChecklistActivity");
        }

        Intent intent = new Intent(getApplicationContext(), OutOfHoursChecklistPhotoActivity.class);
        intent.putExtra("OOHChecklistAnswerID", oohChecklistAnswerID);

        startActivity(intent);
    }

    private boolean CheckComplete() {
        List<OutOfHoursChecklistAnswer> taskChecklistAnswers = OutOfHoursChecklistAnswer.GetOOHChecklistAnswers(oohChecklistID);

        boolean complete = true;

        for (int i = 0; i < taskChecklistAnswers.size(); i++) {
            complete = !taskChecklistAnswers.get(i).isNew;
        }

        return complete;
    }

    private void returnToMenu(final boolean menu) {
        if (CheckComplete()) {

            if (menu) {
                    Intent noShiftIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
                    noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    noShiftIntent.putExtra("ShiftID", shiftID);
                    noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                    startActivity(noShiftIntent);
            } else {
                finish();
            }
        } else {
            //Toast.makeText(getApplicationContext(), "Please complete the checklist before proceeding", Toast.LENGTH_LONG).show();

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            OutOfHoursChecklistAnswer.deleteOOHChecklistAnswersForCheck(oohChecklistID);
                            OutOfHoursChecklist.deleteOOHChecklist(oohChecklistID);

                            if (menu) {
                                Intent noShiftIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
                                noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                noShiftIntent.putExtra("ShiftID", shiftID);
                                noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
                                startActivity(noShiftIntent);
                                finish();
                            } else {
                                Intent startIntent = new Intent(getApplicationContext(), OutOfHoursActivity.class);
                                startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startIntent.putExtra("ShiftID", shiftID);
                                startIntent.putExtra("UserID", sp.getString("UserID", ""));
                                startIntent.putExtra("OutOfHoursID", outOfHoursID);
                                startActivity(startIntent);
                                finish();
                            }

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("Continue to cancel checklist?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }

    public void saveClick(View view) {
        if (CheckComplete()) {
            finish();
        } else {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            OutOfHoursChecklistAnswer.deleteOOHChecklistAnswersForCheck(oohChecklistID);
                            OutOfHoursChecklist.deleteOOHChecklist(oohChecklistID);


                            finish();

                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("Continue to cancel checklist?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
    }

}
