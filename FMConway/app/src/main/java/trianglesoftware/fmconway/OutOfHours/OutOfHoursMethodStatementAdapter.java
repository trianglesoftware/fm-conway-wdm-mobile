package trianglesoftware.fmconway.OutOfHours;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.MethodStatement;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursPhoto;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Task.MethodStatementAdapter;

public class OutOfHoursMethodStatementAdapter extends BaseAdapter {
    public List<MethodStatement> items;
    private Context context;
    private final LayoutInflater inflater;

    public OutOfHoursMethodStatementAdapter(Context context, LayoutInflater inflater) {
        this.items = new LinkedList<>();
        this.context = context;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public MethodStatement getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final OutOfHoursMethodStatementAdapter.ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_out_of_hours_method_statement, null);

            holder = new OutOfHoursMethodStatementAdapter.ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(holder);
        } else {
            holder = (OutOfHoursMethodStatementAdapter.ViewHolder) convertView.getTag();
        }

        MethodStatement item = getItem(position);
        holder.name.setText(item.methodStatementTitle);

        return convertView;
    }

    public static class ViewHolder {
        public TextView name;
    }
}
