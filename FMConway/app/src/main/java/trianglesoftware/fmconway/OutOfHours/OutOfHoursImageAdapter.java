package trianglesoftware.fmconway.OutOfHours;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHoursPhoto;
import trianglesoftware.fmconway.R;

public class OutOfHoursImageAdapter extends BaseAdapter {
    private final Context mContext;
    private ArrayList<OutOfHoursPhoto> photoList = new ArrayList<>();

    public OutOfHoursImageAdapter(Context c, ArrayList<OutOfHoursPhoto> PhotoList) {
        mContext = c;
        this.photoList = PhotoList;
    }

    public int getCount() {
        return photoList.size();
    }

    public Object getItem(int position) {
        return photoList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ImageView imageView = new ImageView(mContext);
        imageView.setHorizontalScrollBarEnabled(true);
        imageView.setImageBitmap(photoList.get(position).getImageData());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setRotation(90);
        imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
        imageView.setTag(photoList.get(position).oohPhotoID);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:

                                OutOfHoursPhoto.deleteOutOfHoursPhotoFile(imageView.getTag().toString());
                                OutOfHoursPhoto.deleteOutOfHoursPhoto(imageView.getTag().toString());

                                photoList.remove(position);

                                Update();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:

                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext , R.style.Base_Theme_AppCompat_Light_Dialog));
                builder.setMessage("Delete Photo?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }
        });
        return imageView;
    }

    private void Update()
    {
        this.notifyDataSetChanged();
    }
}
