package trianglesoftware.fmconway.OutOfHours;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Accident.SelectAccidentActivity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.OutOfHours;
import trianglesoftware.fmconway.Observation.SelectObservationActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class SelectOutOfHoursActivity extends FMConwayActivityBase implements AdapterView.OnItemClickListener {
    private String shiftID;
    private String userID;
    private SelectOutOfHoursAdapter adapter;
    private SharedPreferences sp;
    private Button home;
    private Button back;
    private TextView header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_select_out_of_hours);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            userID = extras.getString("UserID", "");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView oohList = (ListView) findViewById(R.id.select_ooh_list);
        adapter = new SelectOutOfHoursAdapter(this, getLayoutInflater());
        oohList.setAdapter(adapter);
        oohList.setOnItemClickListener(this);

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "SelectOOHActivity");
        }

        header = (TextView) findViewById(R.id.header_title);
        home = (Button) findViewById(R.id.back_to_home);
        back = (Button) findViewById(R.id.back);

        if (header != null) {
            header.setText("Out Of Hours");
        }
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToMenu(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "SelectOOHActivity");
        }
    }

    private void GetData() throws Exception {
        /*List<OutOfHours> ooh = OutOfHours.GetAllOutOfHours();

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < ooh.size(); i++) {
            jsonArray.put(ooh.get(i).getJSONObject());
        }

        adapter.UpdateData(jsonArray);*/
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Intent intent = new Intent(getApplicationContext(), OutOfHoursActivity.class);
        intent.putExtra("OutOfHoursID", selected);
        intent.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(intent);
    }

    public void addOutOfHours(View view) {

        Intent intent = new Intent(getApplicationContext(), OutOfHoursActivity.class);
        intent.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(intent);

        finish();
    }

    private void accidentClick(View view) {
        Intent selectObservationActivity = new Intent(getApplicationContext(), SelectAccidentActivity.class);
        selectObservationActivity.putExtra("ShiftID", shiftID);
        startActivity(selectObservationActivity);
    }

    private void returnToMenu(final boolean menu) {
        if (menu) {
            Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
            startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startIntent.putExtra("UserID", sp.getString("UserID", ""));
            startActivity(startIntent);
            finish();

        } else {
            finish();
        }
    }
}
