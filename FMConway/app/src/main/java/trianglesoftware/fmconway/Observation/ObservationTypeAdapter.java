package trianglesoftware.fmconway.Observation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
class ObservationTypeAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public ObservationTypeAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_standard, null);

            holder = new ViewHolder();
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String observationType = "";
        int observationTypeID = 0;

        if(jsonObject.has("Name"))
        {
            observationType = jsonObject.optString("Name");
        }

        if(jsonObject.has("ObservationTypeID"))
        {
            observationTypeID = jsonObject.optInt("ObservationTypeID");
        }

        holder.middleRowTextView.setTag(observationTypeID);
        holder.middleRowTextView.setText(observationType);

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView middleRowTextView;
    }
}
