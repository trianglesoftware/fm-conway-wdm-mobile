package trianglesoftware.fmconway.Observation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Observation;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectObservationActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private ObservationAdapter observationAdapter;
    private Button observationButton;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView observationList = (ListView) findViewById(R.id.select_observation_list);
        observationAdapter = new ObservationAdapter(this, getLayoutInflater());
        observationList.setAdapter(observationAdapter);
        observationList.setOnItemClickListener(this);

        observationButton = (Button) findViewById(R.id.observationButton);

        observationButton.setEnabled(false);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectObservationActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_observation;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Observations";
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectObservationActivity");
        }
    }

    private void GetData() throws Exception
    {
        List<Observation> observations = Observation.GetObservationsForShift(shiftID, sp.getString("UserID", ""));

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < observations.size(); i++) {
            jsonArray.put(observations.get(i).getJSONObject());
        }

        observationAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Intent observationActivity = new Intent(getApplicationContext(), ObservationActivity.class);
        observationActivity.putExtra("ObservationID", selected);
        observationActivity.putExtra("ShiftID", shiftID);
        startActivity(observationActivity);
    }

    public void addObservation(View view)
    {

        Intent addObservationActivity = new Intent(getApplicationContext(), ObservationActivity.class);
        addObservationActivity.putExtra("ShiftID", shiftID);
        startActivity(addObservationActivity);

        finish();
    }
}
