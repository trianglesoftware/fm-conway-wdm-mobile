package trianglesoftware.fmconway.Observation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
class ObservationAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public ObservationAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_standard, null);

            holder = new ViewHolder();
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String observationText = "";
        String observationID = "";

        if(jsonObject.has("ObservationDescription"))
        {
            observationText = jsonObject.optString("ObservationDescription");
        }

        if(jsonObject.has("ObservationID"))
        {
            observationID = jsonObject.optString("ObservationID");
        }

        holder.middleRowTextView.setTag(observationID);
        holder.middleRowTextView.setText(observationText);

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView middleRowTextView;
    }
}
