package trianglesoftware.fmconway.Observation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;

import org.json.JSONArray;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Observation;
import trianglesoftware.fmconway.Database.DatabaseObjects.ObservationPhoto;
import trianglesoftware.fmconway.Database.DatabaseObjects.ObservationType;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class ObservationActivity extends FMConwayActivityBase implements AdapterView.OnItemSelectedListener {
    private String shiftID;
    private String observationID;
    private EditText personsInvolved;
    private EditText location;
    private EditText description;
    private EditText cause;
    //private EditText depot;
    private ObservationTypeAdapter observationTypeAdapter;
    private int observationTypeID = 1;

    private GridView gridView;
    private ArrayList<ObservationPhoto> photoList;
    private ObservationImageAdapter adapter;

    private SharedPreferences sp;

    private String filepath;
    private final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_observation);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            observationID = extras.getString("ObservationID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        personsInvolved = (EditText)findViewById(R.id.person_involved_editText);
        location = (EditText)findViewById(R.id.location_editText);
        description = (EditText)findViewById(R.id.observation_description_editText);
        cause = (EditText)findViewById(R.id.cause_editText);
        //depot = (EditText)findViewById(R.id.depot_editText);
        //Spinner observationType = (Spinner) findViewById(R.id.observation_type_spinner);

//        observationTypeAdapter = new ObservationTypeAdapter(this, getLayoutInflater());
//        observationType.setAdapter(observationTypeAdapter);
//        observationType.setOnItemSelectedListener(this);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ObservationActivityGetData");
        }

        gridView = (GridView)findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = ObservationPhoto.getPhotosForObservation(observationID);

        adapter = new ObservationImageAdapter(this, photoList);

        gridView.setAdapter(adapter);

        if(!Objects.equals(observationID,""))
        {
            Observation ob = Observation.GetObservation(observationID);
            personsInvolved.setText(ob.peopleInvolved);
            location.setText(ob.location);
            description.setText(ob.observationDescription);
            cause.setText(ob.causeOfProblem);
            //depot.setText(ob.getChevronDepot());
            //observationType.setSelection(ob.getObservationTypeID() - 1);
        }
    }

    public void addObservationPhoto(View view)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                AddData(filepath);

                photoList = ObservationPhoto.getPhotosForObservation(observationID);
                adapter = new ObservationImageAdapter(this, photoList);
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        }
    }

    private void AddData(String imageLocation) {

        if(Objects.equals(observationID,""))
        {
            Observation observation = new Observation();
            observation.shiftID = shiftID;
            observation.observationDescription = description.getText().toString();
            observation.causeOfProblem = cause.getText().toString();
            observation.peopleInvolved = personsInvolved.getText().toString();
            observation.location = location.getText().toString();
            //observation.setChevronDepot(depot.getText().toString());
            //observation.setObservationTypeID(observationTypeID);
            observation.observationDate = new Date();
            observation.isNew = true;
            observation.userID = sp.getString("UserID", "");
            Observation.AddObservation(observation);

            observationID = Observation.GetLatestObservation();
        }

        LocationManager lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude =  0;
        double latitude =  0;

        if(location != null) {
            longitude =  location.getLongitude();
            latitude =  location.getLatitude();
        }

        ObservationPhoto photo = new ObservationPhoto();
        photo.observationID = observationID;
        photo.longitude = longitude;
        photo.latitude = latitude;
        photo.imageLocation = imageLocation;
        photo.userID = sp.getString("UserID", "");
        photo.time = new Date();

        ObservationPhoto.addObservationPhoto(photo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putString("filepath", filepath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
    }

    private void GetData() throws Exception
    {
        List<ObservationType> observationTypes = ObservationType.GetObservationTypes();

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < observationTypes.size(); i++) {
            jsonArray.put(observationTypes.get(i).getJSONObject());
        }

        observationTypeAdapter.UpdateData(jsonArray);
    }

    public void submitObservation(View view)
    {
        Observation obs = new Observation();
        obs.shiftID = shiftID;
        obs.observationDescription = description.getText().toString();
        obs.causeOfProblem = cause.getText().toString();
        obs.peopleInvolved = personsInvolved.getText().toString();
        obs.location = location.getText().toString();
        //obs.setChevronDepot(depot.getText().toString());
        //obs.setObservationTypeID(observationTypeID);
        obs.userID = sp.getString("UserID", "");

        if(!Objects.equals(observationID,""))
        {
            //Update
            obs.observationID = observationID;
            Observation.UpdateObservation(obs);
        }
        else{
            //Add
            obs.observationDate = new Date();
            obs.isNew = true;
            Observation.AddObservation(obs);
        }

        finish();

        if (!Objects.equals(shiftID,""))
        {
            Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
            startIntent.putExtra("ShiftID", shiftID);
            startIntent.putExtra("UserID",sp.getString("UserID", ""));
            startActivity(startIntent);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        observationTypeID = (int) view.findViewById(R.id.text_equipment_type).getTag();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
