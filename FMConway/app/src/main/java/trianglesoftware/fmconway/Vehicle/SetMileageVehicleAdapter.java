package trianglesoftware.fmconway.Vehicle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class SetMileageVehicleAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public SetMileageVehicleAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_mileage, null);

            holder = new ViewHolder();
            holder.topRowTextView = (TextView)convertView.findViewById(R.id.text_toprow);
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);
            holder.bottomRowTextView = (TextView)convertView.findViewById(R.id.text_bottomrow);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String vehicleMake = "";
        String vehicleModel = "";
        String vehicleRegistration = "";
        String vehicleID = "";

        if(jsonObject.has("Make"))
        {
            vehicleMake = jsonObject.optString("Make");
        }

        if(jsonObject.has("Model"))
        {
            vehicleModel = jsonObject.optString("Model");
        }

        if(jsonObject.has("Registration"))
        {
            vehicleRegistration = jsonObject.optString("Registration");
        }

        if(jsonObject.has("VehicleID"))
        {
            vehicleID = jsonObject.optString("VehicleID");
        }

        holder.topRowTextView.setText(vehicleMake);
        holder.topRowTextView.setTag(vehicleID);
        holder.middleRowTextView.setText(vehicleModel);
        holder.bottomRowTextView.setText(vehicleRegistration);

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView topRowTextView;
        public TextView middleRowTextView;
        public TextView bottomRowTextView;
    }
}
