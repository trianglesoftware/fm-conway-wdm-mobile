package trianglesoftware.fmconway.Vehicle;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class VehicleDriverActivity extends FMConwayActivity {
    //intent
    private String shiftID;
    private String vehicleID;

    //controls
    private ListView list;
    private VehicleDriverAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            vehicleID = extras.getString("VehicleID", "");
        }

        super.onCreate(savedInstanceState);

        adapter = new VehicleDriverAdapter(this, getLayoutInflater());
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vehicle_driver;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Vehicle Driver";
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "VehicleDriverActivity");
        }
    }

    private void GetData() {
        adapter.Entities = ShiftStaff.GetShiftStaff(shiftID);
        adapter.StaffID = ShiftVehicle.getStaffID(shiftID, vehicleID);
        adapter.notifyDataSetChanged();
    }

    public void rowClick(View view) {
        String oldStaffID = ShiftVehicle.getStaffID(shiftID, vehicleID);

        int position = list.getPositionForView(view);
        ShiftStaff shiftStaff = adapter.getItem(position);

        String newStaffID = null;
        if (!Objects.equals(shiftStaff.staffID, oldStaffID)) {
            newStaffID = shiftStaff.staffID;
        }

        ShiftVehicle.setStaffID(shiftID, vehicleID, newStaffID);
        GetData();
    }

    public void confirmClick(View view) {
        // data is saved on rowClick() so just finish
        finish();
    }
}
