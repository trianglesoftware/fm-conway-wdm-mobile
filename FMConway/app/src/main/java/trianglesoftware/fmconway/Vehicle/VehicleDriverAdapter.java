package trianglesoftware.fmconway.Vehicle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.R;

public class VehicleDriverAdapter extends BaseAdapter {
    public List<ShiftStaff> Entities;
    public String StaffID;
    private final LayoutInflater inflater;

    public VehicleDriverAdapter(Context context, LayoutInflater inflater)
    {
        this.inflater = inflater;
        Entities = new LinkedList<>();
    }

    @Override
    public int getCount() {
        return Entities.size();
    }

    @Override
    public ShiftStaff getItem(int position) {
        return Entities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.row_vehicle_driver, null);

            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        ShiftStaff shiftStaff = getItem(position);
        holder.name.setText(shiftStaff.staffFullname);

        if (Objects.equals(shiftStaff.staffID, StaffID)) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView name;
        public CheckBox checkbox;
    }
}
