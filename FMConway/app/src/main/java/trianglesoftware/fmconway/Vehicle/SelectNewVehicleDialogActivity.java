package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.CustomAutoCompleteTextChangedListener;
import trianglesoftware.fmconway.Utilities.CustomAutoCompleteView;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectNewVehicleDialogActivity extends FMConwayActivityBase {
    public CustomAutoCompleteView newVehicle;
    private String shiftID;
    private String userID;
    private String jobPackID;
    private String newVehicleID;
    private SharedPreferences sp;
    public String[] item = new String[] {"Please search..."};
    public ArrayAdapter<String> myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_new_vehicle_dialog);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            jobPackID = extras.getString("JobPackID","");
            userID = extras.getString("UserID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        newVehicle = (CustomAutoCompleteView)findViewById(R.id.autoComplete_NewVehicle);
        newVehicle.setThreshold(1);
        newVehicle.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this, 2));

        myAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, item);
        newVehicle.setAdapter(myAdapter);
    }

    public String[] getItemsFromDb(String searchTerm) {
        // add items on the array dynamically
        List<Vehicle> vehicle = Vehicle.GetVehiclesNotOnShift(searchTerm);
        int rowCount = vehicle.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (Vehicle record : vehicle) {

            item[x] = record.getMake() + " " + record.getModel() + " " + record.getRegistration();
            x++;
        }

        return item;
    }

    public void submitVehicle(View view) {
        Vehicle vehicle = Vehicle.GetVehicleFromName(newVehicle.getText().toString());
        if(vehicle.vehicleID != null) {
            //Check if vehicle already on shift
            ShiftVehicle currentShiftVehicle = ShiftVehicle.GetShiftVehicle(shiftID, vehicle.vehicleID, sp.getString("UserID", ""));
            if(currentShiftVehicle != null && !Objects.equals(currentShiftVehicle.vehicleID,""))
            {
                currentShiftVehicle.reasonNotOn = "";
                currentShiftVehicle.notOnShift = false;
                ShiftVehicle.UpdateShiftVehicle(currentShiftVehicle);
            }
            else {
                ShiftVehicle jpv = new ShiftVehicle();
                jpv.shiftID = shiftID;
                jpv.vehicleID = vehicle.vehicleID;
                jpv.userID = sp.getString("UserID","");
                ShiftVehicle.addShiftVehicles(jpv);
            }

            Intent vehicleList = new Intent(getApplicationContext(), SelectVehicleActivity.class);
            vehicleList.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            vehicleList.putExtra("ShiftID", shiftID);
            vehicleList.putExtra("UserID", userID);
            startActivity(vehicleList);
        }
        else{
            Toast.makeText(getApplicationContext(), "This is not a valid vehicle", Toast.LENGTH_LONG).show();
        }
    }

    public void cancelVehicle(View view)
    {
        finish();
    }
}
