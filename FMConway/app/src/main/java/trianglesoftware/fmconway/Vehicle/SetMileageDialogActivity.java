package trianglesoftware.fmconway.Vehicle;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SetMileageDialogActivity extends FMConwayActivityBase {
    private String shiftID;
    private String vehicleID;
    private boolean endOfShift;
    private EditText startMileage;
    private EditText endMileage;
    private LinearLayout endMileageSection;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_mileage_dialog);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            vehicleID = extras.getString("VehicleID","");
            endOfShift = extras.getBoolean("EndOfShift");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        startMileage = (EditText)findViewById(R.id.editText_StartMileage);
        endMileage = (EditText)findViewById(R.id.editText_EndMileage);
        endMileageSection = (LinearLayout) findViewById(R.id.end_mileage_section);

        ShiftVehicle jpv =  ShiftVehicle.GetShiftVehicle(shiftID, vehicleID, sp.getString("UserID", ""));
        startMileage.setText(String.valueOf(jpv.startMileage));
        endMileage.setText(String.valueOf(jpv.endMileage));

        if(endOfShift)
        {
            endMileageSection.setVisibility(View.VISIBLE);
        }
    }

    public void saveMileage(View view)
    {
        //Update Mileage
        ShiftVehicle.ChangeMileage(true, Integer.parseInt(startMileage.getText().toString()), 0, shiftID, vehicleID);
        ShiftVehicle.ChangeMileage(false,0, Integer.parseInt(endMileage.getText().toString()), shiftID, vehicleID);

        finish();
    }
}
