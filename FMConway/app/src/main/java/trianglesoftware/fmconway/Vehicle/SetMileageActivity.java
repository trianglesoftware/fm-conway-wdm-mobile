package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class SetMileageActivity extends FMConwayActivity {
    private String shiftID;
    private ListView vehicleList;
    private SetMileageVehicleAdapter vehicleAdapter;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        vehicleList = (ListView)findViewById(R.id.select_vehicle_list);
        vehicleAdapter = new SetMileageVehicleAdapter(this, getLayoutInflater());
        vehicleList.setAdapter(vehicleAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SetMileageActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_set_mileage;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Set Mileage";
    }

    private void GetData() throws Exception
    {
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, sp.getString("UserID", ""));

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < vehicles.size(); i++) {
            jsonArray.put(vehicles.get(i).getJSONObject());
        }

        vehicleAdapter.UpdateData(jsonArray);
    }

    public void setMileage(View view)
    {
        int pos = vehicleList.getPositionForView((View)view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);

        Intent mileageDialog = new Intent(getApplicationContext(), SetMileageDialogActivity.class);
        mileageDialog.putExtra("ShiftID", shiftID);
        mileageDialog.putExtra("VehicleID", selected.optString("VehicleID"));
        mileageDialog.putExtra("EndOfShift", true);
        startActivity(mileageDialog);
    }
}
