package trianglesoftware.fmconway.Vehicle;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Jamie.Dobson on 08/03/2016.
 */
public class VehicleAdapter extends BaseAdapter {
    public String ShiftID;
    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public VehicleAdapter(Context context, LayoutInflater inflater) {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_vehicle, null);

            holder = new ViewHolder();
            holder.topRowTextView = (TextView) convertView.findViewById(R.id.text_toprow);
            holder.middleRowTextView = (TextView) convertView.findViewById(R.id.text_equipment_type);
            holder.bottomRowTextView = (TextView) convertView.findViewById(R.id.text_bottomrow);
            holder.driverButton = (Button) convertView.findViewById(R.id.button_driver);
            holder.checklistButton = (Button) convertView.findViewById(R.id.checklists);

            //Button removeVehicle = (Button) convertView.findViewById(R.id.removeVehicleButton);
            //removeVehicle.setVisibility(View.GONE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        //String vehicleMake = "";
        String vehicleModel = "";
        String vehicleRegistration = "";
        String driver = "";
        String staffID = "";

        String vehicleID = "";

        if (jsonObject.has("Make") && jsonObject.has("Model")) {
            vehicleModel = jsonObject.optString("Make") + ' ' + jsonObject.optString("Model");
        }

        //if(jsonObject.has("Model")) {
        //    vehicleModel = jsonObject.optString("Model");
        //}

        if (jsonObject.has("Registration")) {
            vehicleRegistration = jsonObject.optString("Registration");
        }

        if (jsonObject.has("VehicleID")) {
            vehicleID = jsonObject.optString("VehicleID");
        }

        if (jsonObject.has("Driver")) {
            driver = jsonObject.optString("Driver");
        }

        if (jsonObject.has("StaffID")) {
            staffID = jsonObject.optString("StaffID");
        }

        // driver validation
        if (!TextUtils.isEmpty(staffID)) {
            holder.driverButton.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_blue));
            holder.driverButton.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_light));
        } else {
            holder.driverButton.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_yellow));
            holder.driverButton.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_black));
        }
        
        /*boolean isVehicleChecklistComplete = isVehicleChecklistComplete(vehicleID);
        if (isVehicleChecklistComplete) {
            holder.checklistButton.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_blue));
            holder.checklistButton.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_light));
        } else {
            holder.checklistButton.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_yellow));
            holder.checklistButton.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_black));
        }*/

        // set text
        holder.topRowTextView.setText(vehicleModel);
        holder.topRowTextView.setTag(vehicleID);
        holder.topRowTextView.setVisibility(FMConwayUtils.isNullOrWhitespace(vehicleModel) ? View.GONE : View.VISIBLE);

        holder.middleRowTextView.setText(driver);
        holder.middleRowTextView.setVisibility(FMConwayUtils.isNullOrWhitespace(driver) ? View.GONE : View.VISIBLE);

        holder.bottomRowTextView.setText(vehicleRegistration);
        holder.bottomRowTextView.setVisibility(FMConwayUtils.isNullOrWhitespace(vehicleRegistration) ? View.GONE : View.VISIBLE);

        return convertView;
    }

    private boolean isVehicleChecklistComplete(String vehicleID) {
        // does vehicle checklist exist
        String vehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(vehicleID, ShiftID, true);
        if (TextUtils.isEmpty(vehicleChecklistID)) {
            return false;
        }

        // check all questions answered
        List<VehicleChecklistAnswer> vehicleChecklistAnswers = VehicleChecklistAnswer.GetVehicleChecklistAnswers(vehicleChecklistID);
        for (int i = 0; i < vehicleChecklistAnswers.size(); i++) {
            if (vehicleChecklistAnswers.get(i).isNew) {
                return false;
            }
        }

        // check mileage
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(FMConwayApp.getContext());
        ShiftVehicle shiftVehicle = ShiftVehicle.GetShiftVehicle(ShiftID, vehicleID, sp.getString("UserID", ""));
        if (shiftVehicle.startMileage == 0) {
            return false;
        }

        // check photo
        boolean photoAdded = !TextUtils.isEmpty(VehicleChecklist.getPhoto(vehicleChecklistID));
        if (!photoAdded) {
            return false;
        }

        return true;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView topRowTextView;
        public TextView middleRowTextView;
        public TextView bottomRowTextView;
        public Button driverButton;
        public Button checklistButton;
    }
}
