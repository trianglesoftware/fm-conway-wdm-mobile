package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.VehicleChecklist.VehicleChecklistActivity;

public class SelectVehicleWithoutShiftActivity extends FMConwayActivityBase {
    private String userID;
    private String shiftID;
    private ListView vehicleList;
    private VehicleNotOnShiftAdapter vehicleAdapter;
    private Button back;
    private Button home;
    private TextView header;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_select_vehicle_without_shift);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        back = (Button) findViewById(R.id.back);
        home = (Button) findViewById(R.id.back_to_home);
        header = (TextView) findViewById(R.id.header_title);

        back.setVisibility(View.INVISIBLE);
        home.setVisibility(View.INVISIBLE);

        header.setText("Vehicles Without Completed Checklists");

        super.onCreate(savedInstanceState);

        vehicleList = (ListView)findViewById(R.id.select_vehicle_list);
        vehicleAdapter = new VehicleNotOnShiftAdapter(this, getLayoutInflater());
        vehicleList.setAdapter(vehicleAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectVehicleWithoutShiftActivity");
        }
    }

    private void GetData() throws Exception
    {
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForToday(sp.getString("UserID", ""));

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < vehicles.size(); i++) {
            jsonArray.put(vehicles.get(i).getJSONObject());
        }

        vehicleAdapter.UpdateData(jsonArray);
    }

    public void viewChecklists(View view) {
        int pos = vehicleList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);

        try {
            ShiftVehicle sv = ShiftVehicle.GetShiftVehicle("", selected.optString("VehicleID"), sp.getString("UserID", ""));
            shiftID = sv.shiftID;
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "GetShiftVehicle");
        }



//        if(ShiftVehicle.CheckLoadingSheetIsConfirmed(jobPackID, selected.optInt("VehicleID")))
//        {
        //Create Vehicle Checklist and Answers
        List<Checklist> checklists = Checklist.GetChecklistsForVehicle(selected.optString("VehicleID"));
        for (int i = 0; i < checklists.size(); i++) {
            //Check if this checklist has already been created
            if (!VehicleChecklist.CheckIfChecklistCreated(selected.optString("VehicleID"), shiftID, checklists.get(i).checklistID, true)) {
                VehicleChecklist vc = new VehicleChecklist();
                vc.vehicleChecklistID = UUID.randomUUID().toString();
                vc.vehicleID = selected.optString("VehicleID");
                vc.checklistID = checklists.get(i).checklistID;
                vc.shiftID = shiftID;
                vc.startOfShift = true;
                vc.adhoc = true;
                vc.userID = sp.getString("UserID", "");
                VehicleChecklist.addVehicleChecklist(vc);

                String newVehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(selected.optString("VehicleID"), shiftID, checklists.get(i).checklistID);

                List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklists.get(i).checklistID);
                for (int j = 0; j < checklistQuestions.size(); j++) {
                    VehicleChecklistAnswer vca = new VehicleChecklistAnswer();
                    vca.vehicleChecklistID = newVehicleChecklistID;
                    vca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                    vca.userID = sp.getString("UserID", "");
                    //vca.setAnswer(true);
                    VehicleChecklistAnswer.addVehicleChecklistAnswerNew(vca);
                }

                Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
                vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
                vehicleChecklistQuestions.putExtra("VehicleChecklistID", newVehicleChecklistID);
                vehicleChecklistQuestions.putExtra("VehicleID", selected.optString("VehicleID"));
                vehicleChecklistQuestions.putExtra("UserID", sp.getString("UserID", ""));
                vehicleChecklistQuestions.putExtra("StartOfShift", true);
                vehicleChecklistQuestions.putExtra("NoShift", true);
                startActivity(vehicleChecklistQuestions);
            } else {
                List<VehicleChecklist> vc = VehicleChecklist.GetVehicleChecklists(shiftID, selected.optString("VehicleID"),true);
                Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
                vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
                vehicleChecklistQuestions.putExtra("VehicleChecklistID", vc.get(0).vehicleChecklistID);
                vehicleChecklistQuestions.putExtra("VehicleID", selected.optString("VehicleID"));
                vehicleChecklistQuestions.putExtra("UserID", sp.getString("UserID", ""));
                vehicleChecklistQuestions.putExtra("StartOfShift", true);
                vehicleChecklistQuestions.putExtra("NoShift", true);
                startActivity(vehicleChecklistQuestions);
            }
        }
    }

}
