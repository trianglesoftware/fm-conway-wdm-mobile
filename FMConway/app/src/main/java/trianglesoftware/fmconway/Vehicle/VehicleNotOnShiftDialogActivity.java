package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class VehicleNotOnShiftDialogActivity extends FMConwayActivityBase {
    private EditText notOnReason;
    private String shiftID;
    private String vehicleID;
    private String userID;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_vehicle_not_on_shift_dialog);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            vehicleID = extras.getString("VehicleID","");
            userID = extras.getString("UserID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        notOnReason = (EditText)findViewById(R.id.editText_NotOnShift);
    }

    public void submitReason(View view) {
        ShiftVehicle shiftVehicle = ShiftVehicle.GetShiftVehicle(shiftID, vehicleID, sp.getString("UserID", ""));
        shiftVehicle.reasonNotOn = notOnReason.getText().toString();
        shiftVehicle.notOnShift = true;
        ShiftVehicle.UpdateShiftVehicle(shiftVehicle);

        Intent vehicleList = new Intent(getApplicationContext(), SelectVehicleActivity.class);
        vehicleList.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        vehicleList.putExtra("ShiftID", shiftID);
        vehicleList.putExtra("UserID", userID);
        startActivity(vehicleList);
    }
}
