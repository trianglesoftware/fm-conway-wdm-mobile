package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheet;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.LoadingSheet.LoadingSheetActivity;
import trianglesoftware.fmconway.Main.BeforeLeaveActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class ConfirmLoadingSheetActivity extends FMConwayActivity {
    private String shiftID;
    private String userID;
    private ListView vehicleList;
    private LoadingSheetVehicleAdapter vehicleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
        }

        super.onCreate(savedInstanceState);

        vehicleList = (ListView)findViewById(R.id.select_vehicle_list);
        vehicleAdapter = new LoadingSheetVehicleAdapter(this, getLayoutInflater());
        vehicleList.setAdapter(vehicleAdapter);

        try {
            CheckLoadingSheetsConfirmed();
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ConfirmLoadingSheetActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_confirm_loading_sheet;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Loading Sheets";
    }

    private void GetData() throws Exception
    {
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < vehicles.size(); i++) {
            jsonArray.put(vehicles.get(i).getJSONObject());
        }

        vehicleAdapter.UpdateData(jsonArray);
    }

    private void CheckLoadingSheetsConfirmed() throws Exception
    {
        int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
        if(shiftStatus >= 0) { // > 3
            boolean loadingSheetsConfirmed = true;
            List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
            for (int i = 0; i < vehicles.size(); i++) {
                if (!ShiftVehicle.CheckLoadingSheetIsConfirmed(shiftID, vehicles.get(i).vehicleID)) {
                    loadingSheetsConfirmed = false;
                }
            }

            if (loadingSheetsConfirmed) {

//                if (shiftStatus == 4) {
//                    Shift.SetCompletionStatus(shiftID, 5);

                    finish();

                    Intent startBeforeLeaveActivity = new Intent(getApplicationContext(), BeforeLeaveActivity.class);
                    startBeforeLeaveActivity.putExtra("ShiftID",shiftID);
                    startBeforeLeaveActivity.putExtra("UserID",userID);
                    startActivity(startBeforeLeaveActivity);
                //}
            }
        }
    }

    public void viewLoadingSheets(View view)
    {

        int pos = vehicleList.getPositionForView((View)view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);

        String loadingSheetID = LoadingSheet.GetLoadingSheetID(shiftID, selected.optString("VehicleID"));
        Intent loadingSheetActivity = new Intent(getApplicationContext(), LoadingSheetActivity.class);
        loadingSheetActivity.putExtra("VehicleID", selected.optString("VehicleID"));
        loadingSheetActivity.putExtra("ShiftID", shiftID);
        loadingSheetActivity.putExtra("UserID", userID);
        loadingSheetActivity.putExtra("LoadingSheetID", loadingSheetID );
        startActivity(loadingSheetActivity);
    }
}
