package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Staff.TeamSignOff;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.VehicleChecklist.VehicleChecklistActivity;

public class VehicleSignOff extends FMConwayActivity {
    private String shiftID;
    private ListView vehicleList;
    private SetMileageVehicleAdapter vehicleAdapter;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
        }

        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        vehicleList = (ListView)findViewById(R.id.select_vehicle_list);
        vehicleAdapter = new SetMileageVehicleAdapter(this, getLayoutInflater());
        vehicleList.setAdapter(vehicleAdapter);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "VehicleSignOff");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vehicle_sign_off;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Vehicle Sign Off";
    }

    private void GetData() throws Exception
    {
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, sp.getString("UserID", ""));

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < vehicles.size(); i++) {
            jsonArray.put(vehicles.get(i).getJSONObject());
        }

        vehicleAdapter.UpdateData(jsonArray);
    }

    public void checklistClick(View view)
    {
        int pos = vehicleList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);

        List<Checklist> checklists = Checklist.GetChecklistsForVehicle(selected.optString("VehicleID"));
        for(int i = 0; i < checklists.size(); i++) {
            //Check if this checklist has already been created
            if(!VehicleChecklist.CheckIfChecklistCreated(selected.optString("VehicleID"), shiftID, checklists.get(i).checklistID, false)) {
                VehicleChecklist vc = new VehicleChecklist();
                vc.vehicleChecklistID = UUID.randomUUID().toString();
                vc.vehicleID = selected.optString("VehicleID");
                vc.checklistID = checklists.get(i).checklistID;
                vc.shiftID = shiftID;
                vc.startOfShift = false;
                vc.adhoc = false;
                vc.userID = sp.getString("UserID", "");
                VehicleChecklist.addVehicleChecklist(vc);

                String newVehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(selected.optString("VehicleID"), shiftID, checklists.get(i).checklistID);

                List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklists.get(i).checklistID);
                for (int j = 0; j < checklistQuestions.size(); j++) {
                    VehicleChecklistAnswer vca = new VehicleChecklistAnswer();
                    vca.vehicleChecklistID = newVehicleChecklistID;
                    vca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                    vca.userID = sp.getString("UserID", "");
                    //vca.setAnswer(true);
                    VehicleChecklistAnswer.addVehicleChecklistAnswerNew(vca);
                }

                Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
                vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
                vehicleChecklistQuestions.putExtra("VehicleChecklistID", newVehicleChecklistID);
                vehicleChecklistQuestions.putExtra("VehicleID", selected.optString("VehicleID"));
                vehicleChecklistQuestions.putExtra("StartOfShift", false);
                vehicleChecklistQuestions.putExtra("UserID", sp.getString("UserID",""));
                startActivity(vehicleChecklistQuestions);
            }
            else
            {
                List<VehicleChecklist> vc = VehicleChecklist.GetVehicleChecklists(shiftID, selected.optString("VehicleID"), false);
                Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
                vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
                vehicleChecklistQuestions.putExtra("VehicleChecklistID", vc.get(0).vehicleChecklistID);
                vehicleChecklistQuestions.putExtra("VehicleID", selected.optString("VehicleID"));
                vehicleChecklistQuestions.putExtra("UserID", sp.getString("UserID",""));
                vehicleChecklistQuestions.putExtra("StartOfShift", false);
                startActivity(vehicleChecklistQuestions);
            }
        }
    }

    public void continueClick(View view)
    {
        //Check finish checklists are done
//        boolean checklistsCompleted = true;
//        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID);
//        for (int i = 0; i < vehicles.size(); i++)
//        {
//            List<Checklist> vehicleChecklists = Checklist.GetChecklistsForVehicle(vehicles.get(i).getVehicleID());
//            for(int j = 0; j < vehicleChecklists.size(); j++) {
//                if(!VehicleChecklist.CheckIfChecklistCreated(vehicles.get(i).getVehicleID(), shiftID, vehicleChecklists.get(j).getChecklistID(), false))
//                {
//                    checklistsCompleted = false;
//                }
//            }
//        }

        if(ChecklistsCompleted(shiftID,sp.getString("UserID",""))) {
            finish();

            Intent staffSignOff = new Intent(getApplicationContext(), TeamSignOff.class);
            staffSignOff.putExtra("ShiftID", shiftID);
            startActivity(staffSignOff);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please complete all vehicle checklists before proceeding.", Toast.LENGTH_LONG).show();
        }
    }

    public static boolean ChecklistsCompleted(String ShiftID, String UserID)
    {
        boolean checklistsCompleted = true;
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(ShiftID, UserID);
        for (int i = 0; i < vehicles.size(); i++)
        {
            List<Checklist> vehicleChecklists = Checklist.GetChecklistsForVehicle(vehicles.get(i).vehicleID);
            for(int j = 0; j < vehicleChecklists.size(); j++) {
                if(!VehicleChecklist.CheckIfChecklistCreated(vehicles.get(i).vehicleID, ShiftID, vehicleChecklists.get(j).checklistID, false))
                {
                    checklistsCompleted = false;
                }
            }
        }

        return checklistsCompleted;
    }
}
