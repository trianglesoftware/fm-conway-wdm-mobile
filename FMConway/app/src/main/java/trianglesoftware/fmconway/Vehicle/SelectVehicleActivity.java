package trianglesoftware.fmconway.Vehicle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ChecklistQuestion;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklistAnswer;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.VehicleChecklist.VehicleChecklistActivity;


public class SelectVehicleActivity extends FMConwayActivity {
    private String userID;
    private String shiftID;
    private ListView vehicleList;
    private VehicleAdapter vehicleAdapter;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            userID = extras.getString("UserID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        super.onCreate(savedInstanceState);

        vehicleList = (ListView)findViewById(R.id.select_vehicle_list);
        vehicleAdapter = new VehicleAdapter(this, getLayoutInflater());
        vehicleAdapter.ShiftID = shiftID;
        vehicleList.setAdapter(vehicleAdapter);
        
        //Button addVehicle = (Button) findViewById(R.id.add_vehicle);
        //addVehicle.setVisibility(View.GONE);

        try {
             GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectVehicleActivity");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "SelectVehicleActivity");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_vehicle;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Confirm your Vehicles";
    }

    private void GetData() throws Exception {
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < vehicles.size(); i++) {
            jsonArray.put(vehicles.get(i).getJSONObject());
        }

        vehicleAdapter.UpdateData(jsonArray);
    }

    public void removeVehicle(View view)
    {
        int pos = vehicleList.getPositionForView((View)view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);
        //Load dialog intent with reason text field -> Once a reason is entered set not on shift to true and enter reason the refresh data adapter
        Intent removeDialog = new Intent(getApplicationContext(), VehicleNotOnShiftDialogActivity.class);
        removeDialog.putExtra("VehicleID", selected.optString("VehicleID"));
        removeDialog.putExtra("ShiftID", shiftID);
        removeDialog.putExtra("UserID", userID);
        startActivity(removeDialog);
    }

    public void addVehicleClick(View view)
    {
        //Load new dialog intent with autocomplete search to select staff not on shift
        Intent addNewDialog = new Intent(getApplicationContext(), SelectNewVehicleDialogActivity.class);
        addNewDialog.putExtra("ShiftID", shiftID);
        addNewDialog.putExtra("UserID", userID);
        startActivity(addNewDialog);
    }

    public void confirmVehicleClick(View view)
    {
        User user = User.getUser(sp.getString("UserID", ""));
        boolean isCrewMember = Objects.equals(user.employeeType, "Crew Member");
        
        try {
            if (Shift.GetShiftCompletionStatus(shiftID) >= 0) { // = 2
                // skip validation for Crew Members
                if (isCrewMember) {
                    finish();
                    return;
                }
                
                /*List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
                for (int i = 0; i < vehicles.size(); i++) {
                    boolean isVehicleChecklistComplete = isVehicleChecklistComplete(vehicles.get(i).vehicleID);
                    if (!isVehicleChecklistComplete) {
                        Toast.makeText(getApplicationContext(), "Please complete all vehicle checklists before proceeding.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }*/

                for (int i = 0; i < vehicleAdapter.getCount(); i++) {
                    JSONObject vehicle = vehicleAdapter.getItem(i);
                    String staffID = vehicle.optString("StaffID", "");

                    if (TextUtils.isEmpty(staffID)) {
                        Toast.makeText(getApplicationContext(), "Please select a driver for each vehicle.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                Shift.SetCompletionStatus(shiftID, 3);
                
                finish();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ConfirmVehicleClick");
        }
    }

    private boolean isVehicleChecklistComplete(String vehicleID) {
        // does vehicle checklist exist
        String vehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(vehicleID, shiftID, true);
        if (TextUtils.isEmpty(vehicleChecklistID)) {
            return false;
        }

        // check all questions answered
        List<VehicleChecklistAnswer> vehicleChecklistAnswers = VehicleChecklistAnswer.GetVehicleChecklistAnswers(vehicleChecklistID);
        for (int i = 0; i < vehicleChecklistAnswers.size(); i++) {
            if (vehicleChecklistAnswers.get(i).isNew) {
                return false;
            }
        }

        // check mileage
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(FMConwayApp.getContext());
        ShiftVehicle shiftVehicle = ShiftVehicle.GetShiftVehicle(shiftID, vehicleID, sp.getString("UserID", ""));
        if (shiftVehicle.startMileage == 0) {
            return false;
        }

        // check photo
        boolean photoAdded = !TextUtils.isEmpty(VehicleChecklist.getPhoto(vehicleChecklistID));
        if (!photoAdded) {
            return false;
        }

        return true;
    }

    public void setMileage(View view)
    {
        int pos = vehicleList.getPositionForView((View)view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);

        Intent mileageDialog = new Intent(getApplicationContext(), SetMileageDialogActivity.class);
        mileageDialog.putExtra("ShiftID", shiftID);
        mileageDialog.putExtra("UserID", userID);
        mileageDialog.putExtra("VehicleID", selected.optString("VehicleID"));
        mileageDialog.putExtra("EndOfShift", false);
        startActivity(mileageDialog);
    }

    public void viewChecklists(View view)
    {
        int pos = vehicleList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);

//        if(ShiftVehicle.CheckLoadingSheetIsConfirmed(jobPackID, selected.optInt("VehicleID")))
//        {
        //Create Vehicle Checklist and Answers
        List<Checklist> checklists = Checklist.GetChecklistsForVehicle(selected.optString("VehicleID"));
        for(int i = 0; i < checklists.size(); i++) {
            //Check if this checklist has already been created
            if(!VehicleChecklist.CheckIfChecklistCreated(selected.optString("VehicleID"), shiftID, checklists.get(i).checklistID, true)) {
                VehicleChecklist vc = new VehicleChecklist();
                vc.vehicleChecklistID = UUID.randomUUID().toString();
                vc.vehicleID = selected.optString("VehicleID");
                vc.checklistID = checklists.get(i).checklistID;
                vc.shiftID = shiftID;
                vc.startOfShift = true;
                vc.adhoc = false;
                vc.userID = sp.getString("UserID", "");
                VehicleChecklist.addVehicleChecklist(vc);

                String newVehicleChecklistID = VehicleChecklist.GetVehicleChecklistID(selected.optString("VehicleID"), shiftID, checklists.get(i).checklistID);

                List<ChecklistQuestion> checklistQuestions = ChecklistQuestion.GetChecklistQuestionsForChecklist(checklists.get(i).checklistID);
                for (int j = 0; j < checklistQuestions.size(); j++) {
                    VehicleChecklistAnswer vca = new VehicleChecklistAnswer();
                    vca.vehicleChecklistID = newVehicleChecklistID;
                    vca.checklistQuestionID = checklistQuestions.get(j).checklistQuestionID;
                    vca.userID = sp.getString("UserID", "");
                    //vca.setAnswer(true);
                    VehicleChecklistAnswer.addVehicleChecklistAnswerNew(vca);
                }

                Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
                vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
                vehicleChecklistQuestions.putExtra("VehicleChecklistID", newVehicleChecklistID);
                vehicleChecklistQuestions.putExtra("VehicleID", selected.optString("VehicleID"));
                vehicleChecklistQuestions.putExtra("UserID", userID);
                vehicleChecklistQuestions.putExtra("StartOfShift", true);
                startActivity(vehicleChecklistQuestions);
            }
            else{
                List<VehicleChecklist> vc = VehicleChecklist.GetVehicleChecklists(shiftID, selected.optString("VehicleID"), true);
                Intent vehicleChecklistQuestions = new Intent(getApplicationContext(), VehicleChecklistActivity.class);
                vehicleChecklistQuestions.putExtra("ShiftID", shiftID);
                vehicleChecklistQuestions.putExtra("VehicleChecklistID", vc.get(0).vehicleChecklistID);
                vehicleChecklistQuestions.putExtra("VehicleID", selected.optString("VehicleID"));
                vehicleChecklistQuestions.putExtra("UserID", userID);
                vehicleChecklistQuestions.putExtra("StartOfShift", true);
                startActivity(vehicleChecklistQuestions);
            }
        }
    }
    
    public void setDriverClick(View view) throws Exception {
        int pos = vehicleList.getPositionForView((View) view.getParent());
        JSONObject selected = vehicleAdapter.getItem(pos);
        String vehicleID = selected.getString("VehicleID");
        
        Intent intent = new Intent(getApplicationContext(), VehicleDriverActivity.class);
        intent.putExtra("ShiftID", shiftID);
        intent.putExtra("VehicleID", vehicleID);
        startActivity(intent);
    }
}
