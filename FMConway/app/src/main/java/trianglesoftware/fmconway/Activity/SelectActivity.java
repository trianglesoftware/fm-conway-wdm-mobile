package trianglesoftware.fmconway.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Task.TaskDisplayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String jobPackID;
    private String shiftID;
    private String userID;
    private String jobPackName;
    private boolean readOnly;
    private ActivityAdapter activityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobPackID = extras.getString("JobPackID", "");
            shiftID = extras.getString("ShiftID", "");
            readOnly = extras.getBoolean("ReadOnly");
            userID = extras.getString("UserID", "");
        }

        jobPackName = JobPack.GetJobPackName(jobPackID);

        super.onCreate(savedInstanceState);

        ListView activityList = (ListView) findViewById(R.id.select_activity_list);
        activityAdapter = new ActivityAdapter(this, getLayoutInflater());
        activityList.setAdapter(activityAdapter);
        activityList.setOnItemClickListener(this);

        List<Activity> activities = Activity.GetActivitiesForJobPack(jobPackID);
        if (activities.size() == 1) {
            Activity activity = activities.get(0);
            selectActivity(activity.activityID, activity.name);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return jobPackName;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            GetData();
        } catch (Exception e) {
            ErrorLog.CreateError(e, "SelectActivityGetData");
        }
    }

    private void GetData() throws Exception {
        List<Activity> activities = Activity.GetActivitiesForJobPack(jobPackID);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < activities.size(); i++) {
            jsonArray.put(activities.get(i).getJSONObject());
        }

        activityAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String activityID = (String) view.findViewById(R.id.text_equipment_type).getTag();
        String activityName = view.findViewById(R.id.text_toprow).getTag().toString();

        selectActivity(activityID, activityName);
    }

    // a copy of this method has been moved into StartActivity.java
    private void selectActivity(String activityID, String activityName) {
        if (!readOnly) {
            Task task = Task.GetTaskFromActivity(activityID);
            int taskCount = Task.GetTotalTasksForActivity(activityID);

            if (task != null) {
                TaskSelection(task, taskCount, jobPackID);
            } else {
                Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
                selectMaintenanceActivity.putExtra("ActivityID", activityID);
                selectMaintenanceActivity.putExtra("ShiftID", shiftID);
                selectMaintenanceActivity.putExtra("UserID", userID);
                startActivity(selectMaintenanceActivity);
            }
        } else {
            Intent taskDisplayIntent = new Intent(getApplicationContext(), TaskDisplayActivity.class);
            taskDisplayIntent.putExtra("ActivityID", activityID);
            taskDisplayIntent.putExtra("JobPackID", jobPackID);
            taskDisplayIntent.putExtra("ShiftID", shiftID);
            taskDisplayIntent.putExtra("UserID", userID);
            taskDisplayIntent.putExtra("ActivityName", activityName);
            startActivity(taskDisplayIntent);
        }
    }

    public void drawingClick(View view) {
        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        selectDrawingActivity.putExtra("UserID", userID);
        startActivity(selectDrawingActivity);
    }
}
