package trianglesoftware.fmconway.Activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.Database.DatabaseObjects.MaintenanceCheck;
import trianglesoftware.fmconway.R;

/**
 * Created by Jamie.Dobson on 04/03/2016.
 */
class ActivityAdapter extends BaseAdapter {

    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public ActivityAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_activity, null);

            holder = new ViewHolder();
            holder.bigNumberTextView = (TextView)convertView.findViewById(R.id.text_bigNumber);
            holder.topRowTextView = (TextView)convertView.findViewById(R.id.text_toprow);
            holder.middleRowTextView = (TextView)convertView.findViewById(R.id.text_equipment_type);
            holder.bottomRowTextView = (TextView)convertView.findViewById(R.id.text_bottomrow);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);

        String activityName = "";
        String customerName = "";
        String contractName = "";
        String schemeName = "";
        String activityID = "";
        boolean latestCheck = false;

        if(jsonObject.has("Name"))
        {
            activityName = jsonObject.optString("Name");
        }

        if(jsonObject.has("Customer"))
        {
            customerName = jsonObject.optString("Customer");
        }

        if(jsonObject.has("Contract"))
        {
            contractName = jsonObject.optString("Contract");
        }

        if(jsonObject.has("Scheme"))
        {
            schemeName = jsonObject.optString("Scheme");
        }

        if(jsonObject.has("ActivityID"))
        {
            activityID = jsonObject.optString("ActivityID");

            latestCheck = MaintenanceCheck.CheckLatestTime(activityID, true);
        }


        holder.bigNumberTextView.setText(String.valueOf(position + 1));
        if(latestCheck)
        {
            holder.middleRowTextView.setText(contractName);
        }
        else
        {
            holder.middleRowTextView.setText(contractName + " - Maintenance Check is Due");
        }
        holder.middleRowTextView.setTag(activityID);
        holder.topRowTextView.setText(customerName);
        holder.topRowTextView.setTag(activityName);
        holder.bottomRowTextView.setText(schemeName);

        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView bigNumberTextView;
        public TextView topRowTextView;
        public TextView middleRowTextView;
        public TextView bottomRowTextView;
    }
}
