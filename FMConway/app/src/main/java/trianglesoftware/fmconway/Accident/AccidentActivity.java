package trianglesoftware.fmconway.Accident;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.Accident;
import trianglesoftware.fmconway.Database.DatabaseObjects.AccidentPhoto;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class AccidentActivity extends FMConwayActivityBase {
    private String shiftID;
    private String accidentID;
    private EditText accidentText;
    private EditText location;
    private EditText registrations;
    private EditText personReporting;
    //private EditText depot;
    private EditText peopleInvolved;
    private EditText witnesses;
    private EditText actionTaken;

    private GridView gridView;
    private ArrayList<AccidentPhoto> photoList;
    private AccidentImageAdapter adapter;

    private SharedPreferences sp;

    private String filepath;
    private final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accident);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
            accidentID = extras.getString("AccidentID","");
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        accidentText = (EditText)findViewById(R.id.accident_detail);
        location = (EditText)findViewById(R.id.location_editText);
        registrations = (EditText)findViewById(R.id.registration_editText);
        personReporting = (EditText)findViewById(R.id.person_reporting_editText);
        //depot = (EditText)findViewById(R.id.depot_editText);
        peopleInvolved = (EditText)findViewById(R.id.people_involved_editText);
        witnesses = (EditText)findViewById(R.id.witnesses_editText);
        actionTaken = (EditText)findViewById(R.id.action_editText);

        gridView = (GridView)findViewById(R.id.photo_gridView);

        photoList = new ArrayList<>();
        photoList = AccidentPhoto.getPhotosForAccident(accidentID);

        adapter = new AccidentImageAdapter(this, photoList);

        gridView.setAdapter(adapter);

        if(!Objects.equals(accidentID,"")) {
            Accident acc = Accident.GetAccident(accidentID);
            accidentText.setText(acc.accidentDetails);
            location.setText(acc.location);
            personReporting.setText(acc.personReporting);
            //depot.setText(acc.getChevronDepot());
            peopleInvolved.setText(acc.peopleInvolved);
            witnesses.setText(acc.witnesses);
            actionTaken.setText(acc.actionTaken);
            registrations.setText(acc.registrations);
        }
    }

    public void addAccidentPhoto(View view)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = getOutputMediaFile();
        Uri fileUri = FileProvider.getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".provider", file);
        filepath = file.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, REQUEST_CODE);
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"FMConway");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        return new File(mediaStorageDir.getPath() + File.separator + "Photo-"+timeStamp+".jpg");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                AddData(filepath);

                photoList = AccidentPhoto.getPhotosForAccident(accidentID);
                adapter = new AccidentImageAdapter(this, photoList);
                adapter.notifyDataSetChanged();
                gridView.setAdapter(adapter);
            }
        }
    }

    private void AddData(String imageLocation) {

        if(Objects.equals(accidentID,""))
        {
            Accident accident = new Accident();
            accident.shiftID = shiftID;
            accident.accidentDetails = accidentText.getText().toString();
            accident.registrations = registrations.getText().toString();
            accident.peopleInvolved = peopleInvolved.getText().toString();
            accident.location = location.getText().toString();
            //accident.setChevronDepot(depot.getText().toString());
            accident.personReporting = personReporting.getText().toString();
            accident.witnesses = witnesses.getText().toString();
            accident.actionTaken = actionTaken.getText().toString();
            accident.accidentDate = new Date();
            accident.isNew = true;
            accident.userID = sp.getString("UserID", "");
            Accident.AddAccident(accident);

            accidentID = Accident.GetLatestAccidentID();
        }

        LocationManager lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double longitude =  0;
        double latitude =  0;

        if(location != null) {
            longitude =  location.getLongitude();
            latitude =  location.getLatitude();
        }

        AccidentPhoto photo = new AccidentPhoto();
        photo.accidentID = accidentID;
        photo.longitude = longitude;
        photo.latitude = latitude;
        photo.imageLocation = imageLocation;
        photo.userID = sp.getString("UserID", "");
        photo.time = new Date();

        AccidentPhoto.addAccidentPhoto(photo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putString("filepath", filepath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        filepath = savedInstanceState.getString("filepath");
    }

    public void submitAccident(View view)
    {
        Accident accident = new Accident();
        accident.shiftID = shiftID;
        accident.accidentDetails = accidentText.getText().toString();
        accident.registrations = registrations.getText().toString();
        accident.peopleInvolved = peopleInvolved.getText().toString();
        accident.location = location.getText().toString();
        //accident.setChevronDepot(depot.getText().toString());
        accident.personReporting = personReporting.getText().toString();
        accident.witnesses = witnesses.getText().toString();
        accident.actionTaken = actionTaken.getText().toString();
        accident.userID = sp.getString("UserID", "");

        if(!Objects.equals(accidentID,""))
        {
            //Update
            accident.accidentID = accidentID;
            Accident.UpdateAccident(accident);
        }
        else{
            //Add
            accident.accidentDate = new Date();
            accident.isNew = true;
            Accident.AddAccident(accident);
        }

        finish();

        finish();

        if (!Objects.equals(shiftID,""))
        {
            Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
            startIntent.putExtra("ShiftID", shiftID);
            startIntent.putExtra("UserID", sp.getString("UserID",""));
            startActivity(startIntent);
        }

    }

}
