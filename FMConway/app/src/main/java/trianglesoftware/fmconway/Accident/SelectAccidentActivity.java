package trianglesoftware.fmconway.Accident;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Accident;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;


public class SelectAccidentActivity extends FMConwayActivity implements AdapterView.OnItemClickListener {
    private String shiftID;
    private AccidentAdapter accidentAdapter;
    private SharedPreferences sp;
    private Button accidentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID","");
        }

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        ListView accidentList = (ListView) findViewById(R.id.select_accident_list);
        accidentAdapter = new AccidentAdapter(this, getLayoutInflater());
        accidentList.setAdapter(accidentAdapter);
        accidentList.setOnItemClickListener(this);

        accidentButton = (Button)findViewById(R.id.accidentButton);
        accidentButton.setEnabled(false);

        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectAccidentActivityGetData");
        };
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_accident;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Accidents";
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            GetData();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e,"SelectAccidentActivityGetData");
        }
    }

    private void GetData() throws Exception
    {
        List<Accident> accidents = Accident.GetAccidentsForShift(shiftID, sp.getString("UserID", ""));

        JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < accidents.size(); i++) {
                jsonArray.put(accidents.get(i).getJSONObject());
            }



        accidentAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) view.findViewById(R.id.text_equipment_type).getTag();

        Intent accidentActivity = new Intent(getApplicationContext(), AccidentActivity.class);
        accidentActivity.putExtra("AccidentID", selected);
        accidentActivity.putExtra("ShiftID", shiftID);
        startActivity(accidentActivity);
    }

    public void addAccident(View view)
    {

        Intent addAccidentActivity = new Intent(getApplicationContext(), AccidentActivity.class);
        addAccidentActivity.putExtra("ShiftID", shiftID);
        startActivity(addAccidentActivity);

        finish();
    }
}
