package trianglesoftware.fmconway.Main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.view.View;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.Connectivity;

public class SubmitToOffice extends FMConwayActivity {
    private String shiftID;
    private SharedPreferences sp;
    private String url;
    private ProgressDialog dialog;
    private boolean error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID");
        }

        super.onCreate(savedInstanceState);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        url = this.getResources().getString(R.string.QUERY_URL);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Sending to office...");
        dialog.setCancelable(false);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_submit_to_office;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "End of Shift";
    }

    public void submitClick(View view)
    {
        if(Connectivity.isConnectedFast(this.getApplicationContext())) {
            dialog.show();
            String userName = (sp.getString("Username", ""));
            String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
            DataSync sync = new DataSync(shiftID, sp.getString("UserID", ""), url, encodedHeader);

            sync.PushData(true, new DataSync.PushDataListener() {
                @Override
                public void onComplete(boolean errors) {
                    if (errors) {
                        dialog.dismiss();

                        Intent mainActivity = new Intent(SubmitToOffice.this, MainActivity.class);
                        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mainActivity.putExtra("Error", errors);
                        mainActivity.putExtra("Success", false);
                        startActivity(mainActivity);
                    } else {
                        dialog.dismiss();
                        String shiftID = Shift.GetShiftForUser(sp.getString("UserID", ""));

                        if (!Objects.equals(shiftID, "")) {
                            Intent shiftList = new Intent(SubmitToOffice.this, SelectShiftActivity.class);
                            shiftList.putExtra("UserID", sp.getString("UserID", ""));
                            shiftList.putExtra("RunSyncOnResume", true);
                            startActivity(shiftList);
                        } else {
                            Intent mainActivity = new Intent(SubmitToOffice.this, MainActivity.class);
                            mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mainActivity.putExtra("Error", errors);
                            mainActivity.putExtra("Success", true);
                            startActivity(mainActivity);

                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    //TODO: Inform user of fatal error!

                    Intent mainActivity = new Intent(SubmitToOffice.this, MainActivity.class);
                    mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mainActivity.putExtra("Error", true);
                    startActivity(mainActivity);
                    dialog.dismiss();

                }
            });
        }
        else{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Low Signal");
            alertDialog.setMessage("Please ensure you have a Wi-Fi or 4G signal before submitting your shift.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SubmitToOffice.this, R.style.Base_Theme_AppCompat_Dialog);
//        alertDialog.setTitle("End Shift");
//        alertDialog.setMessage("Click yes to end your shift for the day.");
//
//        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //If here all checks passed.  Start sync back to server
//
//            }
//        });
//        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        AlertDialog alert = alertDialog.create();
//        alert.show();
    }
}
