package trianglesoftware.fmconway.Main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;
import java.util.List;

import trianglesoftware.fmconway.Accident.SelectAccidentActivity;
import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Enums.TaskTypeEnum;
import trianglesoftware.fmconway.Observation.SelectObservationActivity;
import trianglesoftware.fmconway.OutOfHours.OutOfHoursActivity;
import trianglesoftware.fmconway.OutOfHours.SelectOutOfHoursActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.DebugActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.MultipleClickHandler;
import trianglesoftware.fmconway.Vehicle.SelectVehicleWithoutShiftActivity;

public class NoShiftActivity extends FMConwayActivityBase {
    private SharedPreferences sp;
    private String url;
    private ProgressDialog mDialog;
    private Toast multipleClickToast;
    
    //intents
    private String shiftID;
    private String userID;
    private boolean hasShifts;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_shift);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        url = this.getResources().getString(R.string.QUERY_URL);

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Downloading new data...");
        mDialog.setCancelable(false);
		mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            userID = extras.getString("UserID", "");
            hasShifts = extras.getBoolean("HasShifts", false);
        }
        
        if (hasShifts) {
            TextView header = (TextView) findViewById(R.id.header);
            header.setText("");
        }

        // open debug menu
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest") {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }

        new MultipleClickHandler(headerLogo, 5, new MultipleClickHandler.OnMultipleClickListener() {
            @Override
            public void onClick(int click) {
                if (click >= 2) {
                    if (multipleClickToast != null) {
                        multipleClickToast.cancel();
                    }

                    multipleClickToast = Toast.makeText(getApplicationContext(), "Options menu clicked: " + click, Toast.LENGTH_SHORT);
                    multipleClickToast.show();
                }
            }

            @Override
            public void onFinalClick() {
                Intent startIntent = new Intent(getApplicationContext(), DebugActivity.class);
                startActivity(startIntent);
            }
        });

        //DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        //    @Override
        //    public void onClick(final DialogInterface dialog, int which) {
        //        switch (which){
        //            case DialogInterface.BUTTON_POSITIVE:
        //
        //                String userName = (sp.getString("Username", ""));
        //                final String userID = sp.getString("UserID", "");
        //                String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        //
        //                final DataDownload dd = new DataDownload(url, userID,true, encodedHeader,getApplicationContext());
        //                dd.DownloadNewData(mDialog, new DataDownload.DataDownloadListener(){
        //                    @Override
        //                    public void onComplete(boolean errors) {
        //                        Log.d("DataDownload", "Complete");
        //                        if (!errors)
        //                        {
        //                            String shiftID = Shift.GetShiftForUser(userID);
        //
        //                            if (!Objects.equals(shiftID,"")) {
        //                                Intent startIntent = new Intent(getApplicationContext(), StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //                                startIntent.putExtra("ShiftID", shiftID);
        //                                startIntent.putExtra("UserID", userID);
        //                                getApplicationContext().startActivity(startIntent);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();
        //                        }
        //                    }
        //
        //                    @Override
        //                    public void onError() {
        //                        Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();
        //                    }
        //                });
        //
        //                break;
        //
        //            case DialogInterface.BUTTON_NEGATIVE:
        //
        //                break;
        //        }
        //    }
        //};
        //
        //
        //AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this ,R.style.Base_Theme_AppCompat_Light_Dialog));
        //builder.setMessage("No active shift. Check for incomplete shift from previous day?").setPositiveButton("Yes", dialogClickListener)
        //        .setNegativeButton("No", dialogClickListener).show();
    }

    public void syncClick(View view)
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:

                            String userName = (sp.getString("Username", ""));
                            String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
                            DataSync sync = new DataSync("", sp.getString("UserID", ""), url, encodedHeader);

                            sync.PushAandO(new DataSync.PushDataListener() {
                                @Override
                                public void onComplete(boolean errors) {

                                    if (errors)
                                    {
                                        dialog.dismiss();

                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(NoShiftActivity.this, R.style.Base_Theme_AppCompat_Light_Dialog);
                                        alertDialog.setTitle("Failed to submit");
                                        alertDialog.setMessage("Failed to submit to office, please try again.");
                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        AlertDialog alert = alertDialog.create();
                                        alert.show();
                                    }
                                    else {
                                        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                                        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        mainActivity.putExtra("Success", true);
                                        startActivity(mainActivity);

                                        dialog.dismiss();
                                    }
                                }

                                @Override
                                public void onError(Throwable t) {

                                    dialog.dismiss();

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(NoShiftActivity.this, R.style.Base_Theme_AppCompat_Light_Dialog);
                                    alertDialog.setTitle("Failed to submit");
                                    alertDialog.setMessage("Failed to submit to office, please try again.");
                                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog alert = alertDialog.create();
                                    alert.show();

                                }
                            });

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
            builder.setMessage("This will remove your accidents, observations, out of hours shifts, and vehicle checklists from the device and upload them to the office. Do you wish to continue?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
        else
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Low Signal");
            alertDialog.setMessage("Please ensure you have a Wi-Fi or 4G signal before submitting your data.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
        //
//        String userName = (sp.getString("Username", ""));
//        String encodedHeader = "Basic " + Base64.encodeToString((userName+":"+sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
//        DataSync sync = new DataSync(0, Integer.parseInt(sp.getString("UserID", "")), url, encodedHeader);
//
//            sync.PushAandO(new DataSync.PushDataListener() {
//                @Override
//                public void onComplete(boolean errors) {
//
//                    Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
//                    mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(mainActivity);
//                }
//
//                @Override
//                public void onError(Throwable t) {
//                    //TODO: Inform user of fatal error!
//
//                }
//            });

    }

    public void logoutClick(View view)
    {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:

                        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainActivity);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this ,R.style.Base_Theme_AppCompat_Light_Dialog));
        builder.setMessage("Continue to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void observationClick(View view)
    {
//        Intent selectObservationActivity = new Intent(getApplicationContext(), ObservationActivity.class);
//        startActivity(selectObservationActivity);

        Intent selectObservationActivity = new Intent(getApplicationContext(), SelectObservationActivity.class);
        selectObservationActivity.putExtra("ShiftID", "");
        startActivity(selectObservationActivity);
    }

    private void riskAssessmentClick(View view) {
        JobPack jobPack = JobPack.GetJobPacksForShift(shiftID).get(0);
        Activity activity = Activity.GetActivitiesForJobPack(jobPack.jobPackID).get(0);
        List<Task> tasks = Task.GetTasksForActivity(activity.activityID);

        for (Task task : tasks) {
            if (TaskTypeEnum.get(task.taskTypeID) == TaskTypeEnum.Checklist) {
                Checklist checklist = Checklist.getChecklist(task.checklistID);
                if (checklist.isJettingRiskAssessment || checklist.isTankeringRiskAssessment || checklist.isCCTVRiskAssessment) {
                    TaskSelectionBase(task, tasks.size(), jobPack.jobPackID, shiftID);
                    return;
                }
            }
        }

        Toast.makeText(getApplicationContext(), "No risk assessment checklist task found in this workflow", Toast.LENGTH_LONG).show();
    }

    public void accidentClick(View view)
    {
//        Intent selectAccidentActivity = new Intent(getApplicationContext(), AccidentActivity.class);
//        startActivity(selectAccidentActivity);

        Intent selectAccidentActivity = new Intent(getApplicationContext(), SelectAccidentActivity.class);
        selectAccidentActivity.putExtra("ShiftID", "");
        startActivity(selectAccidentActivity);
    }

    public void defectClick(View view)
    {
        Intent selectVehicleActivity = new Intent(getApplicationContext(), SelectVehicleWithoutShiftActivity.class);
        selectVehicleActivity.putExtra("ShiftID", "");
        selectVehicleActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectVehicleActivity);
    }

    public void oohClick(View view) {
        /*Intent intent = new Intent(getApplicationContext(), SelectOutOfHoursActivity.class);
        intent.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(intent);*/

        Intent intent = new Intent(getApplicationContext(), OutOfHoursActivity.class);
        intent.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(intent);
    }
}
