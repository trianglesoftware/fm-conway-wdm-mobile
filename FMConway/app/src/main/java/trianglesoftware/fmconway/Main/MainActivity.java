package trianglesoftware.fmconway.Main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.DataDownload;
import trianglesoftware.fmconway.Database.Database;
import trianglesoftware.fmconway.Database.DatabaseHelper;
import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Utilities.Connectivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;


public class MainActivity extends FMConwayActivityBase {
    private static String QUERY_URL = "";
    private static String APK_URL = "";

    private EditText username;
    private EditText password;
    private String encodedHeader;
    private boolean errors;
    private boolean success;

    private ProgressDialog mDialog;

    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            errors = extras.getBoolean("Error");
            success = extras.getBoolean("Success");
        }

        if (errors)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Failed to submit");
            alertDialog.setMessage("Failed to submit to office, please try again.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }

        if (success)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Submitted to Office");
            alertDialog.setMessage("Data successfully submitted to office.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }

        DatabaseHelper db = new DatabaseHelper(this);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        QUERY_URL = this.getResources().getString(R.string.QUERY_URL);
        APK_URL = this.getResources().getString(R.string.APK_URL);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Downloading new data...");
        mDialog.setCancelable(false);
		mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        // logo
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest") {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }

        // default user / pass
        if (!FMConwayUtils.isNullOrWhitespace(getResources().getString(R.string.default_username))) {
            username.setText(getResources().getString(R.string.default_username));
            password.setText(getResources().getString(R.string.default_password));
        } else {
            String userName = sp.getString("Username", "");
            username.setText(userName);
            password.setText("");
            
            if (!FMConwayUtils.isNullOrWhitespace(userName)) {
                password.requestFocus();
            }
        }

        // developer auto login
        if (BuildConfig.DEBUG &&
                !FMConwayUtils.isNullOrWhitespace(username.getText().toString()) &&
                !FMConwayUtils.isNullOrWhitespace(password.getText().toString()) &&
                !FMConwayApp.oneTimeAutoLoggedIn) {
            FMConwayApp.oneTimeAutoLoggedIn = true;
            Button loginButton = findViewById(R.id.login_button);
            //loginButton.performClick();
        }

        // version label
        TextView versionTextView = findViewById(R.id.version);
        versionTextView.setText("Version: " + BuildConfig.VERSION_NAME);

        Database dataTest = new Database(this.getApplicationContext());
    }

    public void loginClick(View view) {
        if (!username.getText().toString().equals("") && !password.getText().toString().equals("")) {
            mDialog.show();
            Login(username.getText().toString(), password.getText().toString());
        }
    }

    private void Login(final String username, final String password) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        encodedHeader = "Basic " + Base64.encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        client.addHeader("Authorization", encodedHeader);

        final Context context = getApplicationContext();

        if (Connectivity.isConnectedFast(this.getApplicationContext())) {
            client.get(QUERY_URL + "SignOn", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String response = "";
                    try {
                        response = new String(responseBody, "UTF-8");
                        JSONObject obj = new JSONObject(response);

                        FMConwayUtils.hideKeyboard(MainActivity.this);

                        if (obj != null) {

                            final String userID = obj.getString("UserID");
                            final String staffID = obj.getString("StaffID");
                            int appVersion = Integer.valueOf(obj.getString("AppVersion"));

                            // updates are managed / rolled out by FM Conway WDM IT team, auto updates disabled
                            boolean autoUpdates = false;

                            if (appVersion > BuildConfig.VERSION_CODE && autoUpdates) // Download location not yet implemented on server
                            {
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface dialog, int which) {
                                        switch (which){
                                            case DialogInterface.BUTTON_POSITIVE:

                                                // download new APK
                                                UpdateAPKTask task = new UpdateAPKTask();
                                                task.execute(getApplicationContext(), mDialog);

                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:

                                                SharedPreferences.Editor editor = sp.edit();
                                                editor.putString("Username", username);
                                                editor.putString(username, password);
                                                editor.putString("UserID", userID);
                                                editor.putString("StaffID", staffID);
                                                editor.apply();
                                                LoginSuccess(username, true, userID);

                                                break;
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this ,R.style.Base_Theme_AppCompat_Light_Dialog));

                                String appVersionTwoDecimal = String.format(Locale.ENGLISH, "%.2f", (double)appVersion / 100);

                                builder.setMessage("An update is available (version " + appVersionTwoDecimal + "), do you want to download it now?").setPositiveButton("Yes", dialogClickListener)
                                        .setNegativeButton("No", dialogClickListener).show();

                                mDialog.dismiss();
                            }
                            else
                            {
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString("Username", username);
                                editor.putString(username, password);
                                editor.putString("UserID", userID);
                                editor.putString("StaffID", staffID);
                                editor.apply();
                                LoginSuccess(username, true, userID);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        mDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Error. Could not log in.", Toast.LENGTH_LONG).show();
                    }
                    //response = response.replace("\"", "");

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    mDialog.dismiss();
                    if (!password.equals("")) {
                        Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("FMConway", statusCode + " " + error.getMessage());
                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid Username or Password", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("Username", "");
                        editor.putString(username, "");
                        editor.putString("UserID", "");
                        editor.putString("StaffID", "");
                        editor.apply();
                    }
                }
            });
        } else {
            FMConwayUtils.hideKeyboard(MainActivity.this);

            if (sp.getString(username, "").equals(password)) {
                if (!sp.getString("UserID", "").equals("")) {
                    LoginSuccess(username, false, sp.getString("UserID", ""));
                } else {
                    mDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "This user has not logged in before. Please connect to a network.", Toast.LENGTH_LONG).show();
                }
            } else {
                mDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please ensure you have a Wi-Fi or 4G signal before logging in.", Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mDialog != null) {
            mDialog.dismiss();
        }
    }

    private void downloadNewData(final String userID, String syncDate) {
        final Context ctx = this;

        final DataDownload dd = new DataDownload(QUERY_URL, userID, syncDate, encodedHeader, getApplicationContext());
        dd.DownloadNewData(mDialog, new DataDownload.DataDownloadListener() {
            @Override
            public void onComplete(boolean errors) {
                Log.d("DataDownload", "Complete");

                mDialog.dismiss();

                //SyncDate should be updated when downloadNewData or DownloadNewShifts completes
                Date date = new Date(System.currentTimeMillis());
                SharedPreferences.Editor editor = sp.edit();
                editor.putLong("SyncDate", date.getTime());
                editor.apply();

                String shiftID = Shift.GetShiftForUser(userID);

                if (!errors) {
                    if (!Objects.equals(shiftID, "")) {

                        Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startIntent.putExtra("UserID", userID);
                        getApplicationContext().startActivity(startIntent);

                    } else {

                        ShiftVehicle.deleteShiftVehiclesForUser(userID);

                        dd.DownloadVehiclesOnShiftsToday(new DataDownload.DataDownloadProgressListener() {
                            @Override
                            public void onComplete() {
                                Intent noShiftIntent = new Intent(ctx, NoShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                ctx.startActivity(noShiftIntent);
                            }

                            @Override
                            public void onError(Throwable t) {
                                ErrorLog.CreateError(t, "Data Download");
                                Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();

                    // allow users access to previously downloaded shifts as the error may come from a refresh or different shift
                    if (!Objects.equals(shiftID, "")) {
                        Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startIntent.putExtra("UserID", userID);
                        getApplicationContext().startActivity(startIntent);
                    }
                }
            }

            @Override
            public void onError() {
                mDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();

                String shiftID = Shift.GetShiftForUser(userID);

                // allow users access to previously downloaded shifts as the error may come from a refresh or different shift
                if (!Objects.equals(shiftID, "")) {
                    Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startIntent.putExtra("UserID", userID);
                    getApplicationContext().startActivity(startIntent);
                }
            }
        });
    }

    private void LoginSuccess(String username, boolean connected, final String userID) {
        if (connected) {
            //Log user in and download new data
            mDialog.show();
            String shiftID = "";
            try
            {
                shiftID = Shift.GetShiftForUser(userID);
            }
            catch (Exception e)
            {
                ErrorLog.CreateError(e, "LoginGetShiftForUser");
            }

            encodedHeader = "Basic " + Base64.encodeToString((username + ":" + sp.getString(username, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
            if (!Objects.equals(shiftID,"")) {
                mDialog.dismiss();

                Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
                startIntent.putExtra("UserID", userID);
                startIntent.putExtra("RunSyncOnResume", true);
                startActivity(startIntent);

            } else {
                // used to get an early date (1970-01-01)
                Date syncDate = new Date(sp.getLong("nonExistingKey", 0));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String dateString = dateFormat.format(syncDate);

                downloadNewData(userID, dateString);
            }

        } else {
            //Log user in don't download data
            //If shift for this user for today load staff intent else message box saying no shift available
            String shiftID = Shift.GetShiftForUser(userID);
            mDialog.dismiss();
            if (!Objects.equals(shiftID,"")) {

                Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startIntent.putExtra("UserID", userID);
                getApplicationContext().startActivity(startIntent);

            } else {
                Intent noShiftIntent = new Intent(this.getApplicationContext(), NoShiftActivity.class);
                startActivity(noShiftIntent);
            }

            Toast.makeText(getApplicationContext(), "No internet connection, unable to check for new shifts", Toast.LENGTH_LONG).show();
        }
    }

    private static class UpdateAPKTask extends AsyncTask
    {
        final String fileLocation = Environment.getExternalStorageDirectory() + "/fmconway/download/";
        final String fileName = "app-release.apk";
        Context context = null;
        ProgressDialog mdialog = null;
        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                context = (Context) objects[0];
                mdialog = (ProgressDialog)objects[1];
                downloadAPK(fileLocation, fileName, APK_URL);
            }
            catch (Exception e)
            {
                ErrorLog.CreateError(e,"APKTask");
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            boolean res = (boolean) o;
            
            mdialog.dismiss();
            if (res) {
                try {

                    if (Build.VERSION.SDK_INT > 23) {

                        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", new File(fileLocation + fileName));

                        Intent installIntent = new Intent(Intent.ACTION_VIEW);
                        installIntent.setDataAndType(uri, "application/vnd.android.package-archive");

                        installIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        context.startActivity(installIntent);

                    }else{
                        Intent installIntent = new Intent(Intent.ACTION_VIEW);
                        installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        installIntent.setDataAndType(Uri.parse("file://" + fileLocation + fileName), "application/vnd.android.package-archive");
                        context.startActivity(installIntent);
                    }

                } catch (Exception e) {
                    ErrorLog.CreateError(e, "APKTask - onPostExecute");
                }
            } else {
                Toast.makeText(context, "Download error, please try again.", Toast.LENGTH_LONG).show();
            }

        }
    }

    public static void downloadAPK(String fileLocation, String fileName, String url) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

        try {
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(1000 * 180 /*2 mins*/);
            connection.connect();

            int respCode = connection.getResponseCode();
            
            if (respCode == HttpURLConnection.HTTP_OK) {
                File file = new File(fileLocation);
                file.mkdirs();

                File outputFile = new File(file, fileName);
                
                if (outputFile.exists()) {
                    outputFile.delete();
                    outputFile = new File(file, fileName);
                }

                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = connection.getInputStream();

                byte[] buffer = new byte[1024];
                int len1;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();

                outputFile.setReadable(true, false);

            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                StringBuilder sb = new StringBuilder();
                sb.append("Bad response (" + respCode + ")\r\n");
                sb.append("URL " + url + "/application/app-release.apk" + "\r\n");

                String line = null;

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();

                throw new IOException(sb.toString());
            }

        } finally {
            connection.disconnect();
        }
    }
}
