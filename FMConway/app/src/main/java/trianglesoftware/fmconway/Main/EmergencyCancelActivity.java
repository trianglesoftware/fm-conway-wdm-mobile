package trianglesoftware.fmconway.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Staff.TeamSignOff;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Vehicle.VehicleSignOff;

public class EmergencyCancelActivity extends FMConwayActivityBase {
    // intents
    private String shiftID;

    // controls
    EditText cancellationReasonEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_cancel);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID");
        }

        cancellationReasonEditText = (EditText) findViewById(R.id.cancellation_reason);

        String cancellationReason = Shift.GetCancellationReason(shiftID);
        cancellationReasonEditText.setText(cancellationReason);
    }

    public void submit(View view) {
        String cancellationReason = cancellationReasonEditText.getText().toString();
        
        if (FMConwayUtils.isNullOrWhitespace(cancellationReason)) {
            Toast.makeText(getApplicationContext(), "A cancellation reason is required", Toast.LENGTH_SHORT).show();
            return;
        }

        Shift.CancelShift(shiftID);
        Shift.UpdateCancellationReason(shiftID, cancellationReason);

        if (!ShiftProgress.DoesProgressExist(shiftID, "Cancelled")) {
            ShiftProgress.SetShiftProgress(shiftID, "Cancelled");
            ShiftProgress.SyncProgress();
        }

        /*Intent vehicleSignOff = new Intent(getApplicationContext(), VehicleSignOff.class);
        vehicleSignOff.putExtra("ShiftID", shiftID);
        startActivity(vehicleSignOff);*/

        Intent staffSignOff = new Intent(getApplicationContext(), TeamSignOff.class);
        staffSignOff.putExtra("ShiftID", shiftID);
        startActivity(staffSignOff);

        finish();
    }
    
}
