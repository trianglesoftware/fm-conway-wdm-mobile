package trianglesoftware.fmconway.Main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Accident.SelectAccidentActivity;
import trianglesoftware.fmconway.Briefing.SelectBriefingActivity;
import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Comment.SelectCommentActivity;
import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.Activity;
import trianglesoftware.fmconway.Database.DatabaseObjects.ActivitySignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Briefing;
import trianglesoftware.fmconway.Database.DatabaseObjects.BriefingChecklistSignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.JobPack;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.Drawing.SelectDrawingActivity;
import trianglesoftware.fmconway.Enums.TaskTypeEnum;
import trianglesoftware.fmconway.JobPack.SelectJobPackActivity;
import trianglesoftware.fmconway.MaintenanceCheck.SelectMaintenanceActivity;
import trianglesoftware.fmconway.Observation.SelectObservationActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Shift.SelectShiftActivity;
import trianglesoftware.fmconway.Staff.SelectStaffActivity;
import trianglesoftware.fmconway.Staff.TeamSignOff;
import trianglesoftware.fmconway.Task.TaskDisplayActivity;
import trianglesoftware.fmconway.Task.TasksActivity;
import trianglesoftware.fmconway.Utilities.DebugActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Utilities.MultipleClickHandler;
import trianglesoftware.fmconway.Vehicle.SelectVehicleActivity;

public class StartActivity extends FMConwayActivityBase {
    private String shiftID;
    private String userID;
    private String url;
    private SharedPreferences sp;
    private Button cancelButton;
    //private TextView cancelSpacer;
    private Button shiftListButton;
    private Button deleteOutOfHoursButton;
    private ImageView beforeLeaveImage;
    private ImageView goToWorkImage;
    private Toast multipleClickToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID");
            try {
                userID = extras.getString("UserID");
            } catch (Exception e) {
                userID = sp.getString("UserID", "");
            }
        }

        url = this.getResources().getString(R.string.QUERY_URL);

        Button observationButton = (Button) findViewById(R.id.observationButton);
        observationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observationClick(v);
            }
        });

        Button riskAssessmentButton = (Button) findViewById(R.id.riskAssessmentButton);
        riskAssessmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                riskAssessmentClick(v);
            }
        });

        Button accidentButton = (Button) findViewById(R.id.accidentButton);
        accidentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accidentClick(v);
            }
        });

        Button commentButton = (Button) findViewById(R.id.commentButton);
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentClick(v);
            }
        });

        shiftListButton = (Button) findViewById(R.id.shiftListButton);

        beforeLeaveImage = (ImageView) findViewById(R.id.before_image);
        goToWorkImage = (ImageView) findViewById(R.id.goToWork_image);

        LinearLayout preworkChecksLayout = (LinearLayout) findViewById(R.id.prework_checks_layout);
        LinearLayout tasksLayout = (LinearLayout) findViewById(R.id.tasks_layout);
        LinearLayout endOfShiftLayout = (LinearLayout) findViewById(R.id.end_of_shift_layout);
        Button cancel = (Button) findViewById(R.id.cancel);

        // crew member restrictions
        User user = User.getUser(sp.getString("UserID", ""));
        if (Objects.equals(user.employeeType, "Crew Member")) {
            tasksLayout.setVisibility(View.INVISIBLE);
            endOfShiftLayout.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.INVISIBLE);
        }

        // out of hours buttons
        deleteOutOfHoursButton = findViewById(R.id.delete_out_of_hours_button);

        Activity activity = Activity.GetActivitiesForShift(shiftID).get(0);
        if (!activity.isOutOfHours) {
            deleteOutOfHoursButton.setVisibility(View.GONE);
        }

        // open debug menu
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest") {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }

        new MultipleClickHandler(headerLogo, 5, new MultipleClickHandler.OnMultipleClickListener() {
            @Override
            public void onClick(int click) {
                if (click >= 2) {
                    if (multipleClickToast != null) {
                        multipleClickToast.cancel();
                    }

                    multipleClickToast = Toast.makeText(getApplicationContext(), "Options menu clicked: " + click, Toast.LENGTH_SHORT);
                    multipleClickToast.show();
                }
            }

            @Override
            public void onFinalClick() {
                Intent startIntent = new Intent(getApplicationContext(), DebugActivity.class);
                startActivity(startIntent);
            }
        });

        String jobType = Activity.getActivityTypeByShift(shiftID);

        if (jobType.equals("Jetting New")) {
            //get checklist task
            Task task = Task.getChecklistTask(shiftID);

            if (task.startTime == null) {

                int taskCount = Task.GetTotalTasksForActivity(task.activityID);
                String jobPackID = task.GetJobPackForTask(task.taskID);

                TaskSelectionBase(task, taskCount, jobPackID, shiftID);

                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            // enable GPS tracking if required
            FMConwayApp.getContext().startGpsTracking();

            // inputs
            beforeLeaveImage = (ImageView) findViewById(R.id.before_image);
            cancelButton = (Button) findViewById(R.id.cancel);
            //cancelSpacer = (TextView) findViewById(R.id.cancel_spacer);

            if (CheckComplete()) {
                goToWorkImage.setImageResource(R.drawable.go_to_work_half);
            }

            if (Shift.CheckIfCancelled(shiftID)) {
                beforeLeaveImage.setEnabled(false);
                beforeLeaveImage.setImageResource(R.drawable.before_leave_half);
                goToWorkImage.setEnabled(false);
                goToWorkImage.setImageResource(R.drawable.go_to_work_half);
                cancelButton.setBackgroundColor(getResources().getColor(R.color.fmconway_green));
            }

            // people complete
            List<ShiftStaff> shiftStaff = ShiftStaff.GetShiftStaff(shiftID);
            boolean staffConfirmed = true;
            for (int i = 0; i < shiftStaff.size(); i++) {
                if (shiftStaff.get(i).startTime == null) {
                    staffConfirmed = false;
                }
            }

            if (staffConfirmed) {
                ImageView peopleImage = findViewById(R.id.people_image);
                peopleImage.setImageResource(R.drawable.people_half);
            }

            // shift status
            boolean hasConfirmedOrNoBriefings = SelectBriefingActivity.CheckBriefingsConfirmed(shiftID) || Briefing.GetBriefingsForShift(shiftID).size() == 0;

            if (staffConfirmed && hasConfirmedOrNoBriefings) {
                ImageView activityImage = findViewById(R.id.activity_image);
                activityImage.setImageResource(R.drawable.shift_activity_half);
            }

            if (staffConfirmed) {
                // not sure where this is actually used, but feel like this could potentially overwrite any future values unless this is final state
                Shift.SetCompletionStatus(shiftID, 5);
            }

            // shift progress (relies on any Shift.SetCompletionStatus calls to be run first)
            int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);

            // pre works checks complete
            if (shiftStatus > 4) {
                beforeLeaveImage.setImageResource(R.drawable.before_leave_half);
            }

            // ChecksCompleted when health and safety signed (if required) and staffConfirmed
            if (hasConfirmedOrNoBriefings) {
                // the shiftStatus check could probably just be replaced with a staffConfirmed checked
                if (shiftStatus > 4 && !ShiftProgress.DoesProgressExist(shiftID, "ChecksCompleted")) {
                    ShiftProgress.SetShiftProgress(shiftID, "ChecksCompleted");
                    ShiftProgress.SyncProgress();
                }
            }

            if (ShiftProgress.DoesProgressExist(shiftID, "Completed")) {
                cancelButton.setVisibility(View.INVISIBLE);
            }

            ShiftProgress.SyncProgress();

            // vehicles complete
            ImageView vehicleChecklistImage = findViewById(R.id.vehicle_checklist_image);

            boolean driverAssigned = false;

            List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
            for (int i = 0; i < vehicles.size(); i++) {
                String vehicleID = vehicles.get(i).vehicleID;
                String staffID = vehicles.get(i).staffID;
                int startMileage = vehicles.get(i).startMileage;

                if (!FMConwayUtils.isNullOrWhitespace(staffID)) {
                    driverAssigned = true;
                    break;
                }
            }

            if (driverAssigned) {
                vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist_half);
            } else {
                vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist);
            }

        } catch (Exception e) {
            ErrorLog.CreateError(e, this.getClass().getSimpleName());
            Toast.makeText(getApplicationContext(), "An error has occurred.", Toast.LENGTH_LONG).show();
        }
    }

    public void activityClick(View view) {
        try {
            int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
            if (shiftStatus >= 0) {
                //Load read only list of job packs then activities then task list
                /*Intent selectJobActivity = new Intent(getApplicationContext(), SelectJobPackActivity.class);
                selectJobActivity.putExtra("ShiftID", shiftID);
                selectJobActivity.putExtra("UserID", userID);
                selectJobActivity.putExtra("FromButton", 2);
                startActivity(selectJobActivity);*/

                Activity activity = Activity.GetActivitiesForShift(shiftID).get(0);

                Intent taskDisplayIntent = new Intent(getApplicationContext(), TaskDisplayActivity.class);
                taskDisplayIntent.putExtra("ActivityID", activity.activityID);
                taskDisplayIntent.putExtra("JobPackID", activity.jobPackID);
                taskDisplayIntent.putExtra("ShiftID", shiftID);
                taskDisplayIntent.putExtra("UserID", userID);
                taskDisplayIntent.putExtra("ActivityName", activity.name);
                startActivity(taskDisplayIntent);

                if (shiftStatus == 1) {
                    Shift.SetCompletionStatus(shiftID, 2);
                }
            } else {
                Toast.makeText(getApplicationContext(), "Please confirm staff members before proceeding", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "ActivityClick");
        }
    }

    public void peopleClick(View view) {
        //Load staff screen
        Intent selectStaffIntent = new Intent(getApplicationContext(), SelectStaffActivity.class);
        selectStaffIntent.putExtra("ShiftID", shiftID);
        selectStaffIntent.putExtra("UserID", userID);
        startActivity(selectStaffIntent);
    }

    public void vehicleClick(View view) {
        try {
            int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
            if (shiftStatus >= 0) {
                Intent selectVehicleActivity = new Intent(getApplicationContext(), SelectVehicleActivity.class);
                selectVehicleActivity.putExtra("ShiftID", shiftID);
                selectVehicleActivity.putExtra("UserID", userID);
                startActivity(selectVehicleActivity);
            } else {
                Toast.makeText(getApplicationContext(), "Please view activity details before proceeding", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "VehicleClick");
        }
    }

    public void shiftClick(View view) {
        Intent startIntent = new Intent(getApplicationContext(), SelectShiftActivity.class);
        startIntent.putExtra("UserID", userID);
        startActivity(startIntent);
    }

    public void beforeLeaveClick(View view) {
        //Next staging screen (Before Leaving Activity)
        Intent beforeLeaveIntent = new Intent(getApplicationContext(), BeforeLeaveActivity.class);
        beforeLeaveIntent.putExtra("ShiftID", shiftID);
        beforeLeaveIntent.putExtra("UserID", userID);
        startActivity(beforeLeaveIntent);
    }

    public void goToWorkClick(View view) {
        try {
            boolean bypassChecks = getResources().getBoolean(R.bool.bypass_tasks_checks);
            int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);

            if (!bypassChecks) {
                // people complete
                List<ShiftStaff> shiftStaff = ShiftStaff.GetShiftStaff(shiftID);
                boolean staffConfirmed = true;
                for (int i = 0; i < shiftStaff.size(); i++) {
                    if (shiftStaff.get(i).startTime == null) {
                        staffConfirmed = false;
                    }
                }

                if (!staffConfirmed) {
                    Toast.makeText(getApplicationContext(), "Please confirm staff members in the people section", Toast.LENGTH_LONG).show();
                    return;
                }

                // vehicles complete
                boolean driverAssigned = false;

                List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
                for (int i = 0; i < vehicles.size(); i++) {
                    String staffID = vehicles.get(i).staffID;
                    if (!TextUtils.isEmpty(staffID)) {
                        driverAssigned = true;
                    }
                }

                if (!driverAssigned) {
                    Toast.makeText(getApplicationContext(), "Please confirm driver in vehicles section", Toast.LENGTH_LONG).show();
                    return;
                }

                // shift activity complete
                boolean hasConfirmedOrNoBriefings = SelectBriefingActivity.CheckBriefingsConfirmed(shiftID) || Briefing.GetBriefingsForShift(shiftID).size() == 0;

                if (!hasConfirmedOrNoBriefings) {
                    Toast.makeText(getApplicationContext(), "Please sign health and safety in the shift activity section", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            // open tasks
            selectActivity(false);

        } catch (Exception e) {
            ErrorLog.CreateError(e, "GoToWorkClick");
        }
    }

    private void selectActivity(boolean readOnly) {
        JobPack jobPack = JobPack.GetJobPacksForShift(shiftID).get(0);
        Activity activity = Activity.GetActivitiesForJobPack(jobPack.jobPackID).get(0);

        if (!readOnly) {
            //Task task = Task.GetTaskFromActivity(activity.activityID);
            //int taskCount = Task.GetTotalTasksForActivity(activity.activityID);

            // open TasksActivity
            Intent tasksActivityIntent = new Intent(getApplicationContext(), TasksActivity.class);
            tasksActivityIntent.putExtra("JobPackID", jobPack.jobPackID);
            tasksActivityIntent.putExtra("ShiftID", shiftID);
            tasksActivityIntent.putExtra("ActivityID", activity.activityID);
            startActivity(tasksActivityIntent);

            //TaskSelectionBase(task, taskCount, jobPack.jobPackID, shiftID);
        } else {
            Intent taskDisplayIntent = new Intent(getApplicationContext(), TaskDisplayActivity.class);
            taskDisplayIntent.putExtra("ActivityID", activity.activityID);
            taskDisplayIntent.putExtra("JobPackID", jobPack.jobPackID);
            taskDisplayIntent.putExtra("ShiftID", shiftID);
            taskDisplayIntent.putExtra("UserID", userID);
            taskDisplayIntent.putExtra("ActivityName", activity.name);
            startActivity(taskDisplayIntent);
        }
    }

    public void endShiftClick(View view) {
        try {
            boolean bypassChecks = getResources().getBoolean(R.bool.bypass_end_of_shift_checks);

            // if not bypassChecks or cancelled then check all mandatory tasks are complete (have endTime)
            if (!bypassChecks && !Shift.CheckIfCancelled(shiftID)) {
                Activity activity = Activity.GetActivitiesForShift(shiftID).get(0);
                List<Task> tasks = Task.GetTasksForActivity(activity.activityID);

                for (int i = 0; i < tasks.size(); i++) {
                    Task task = tasks.get(i);
                    if (task.isMandatory && task.endTime == null) {
                        Toast.makeText(this.getApplicationContext(), "All mandatory tasks must be completed.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "EndShiftClick");
        }

        Intent staffSignOff = new Intent(getApplicationContext(), TeamSignOff.class);
        staffSignOff.putExtra("ShiftID", shiftID);
        startActivity(staffSignOff);
    }

    private void observationClick(View view) {
        Intent selectObservationActivity = new Intent(getApplicationContext(), SelectObservationActivity.class);
        selectObservationActivity.putExtra("ShiftID", shiftID);
        startActivity(selectObservationActivity);
    }

    private void riskAssessmentClick(View view) {
        JobPack jobPack = JobPack.GetJobPacksForShift(shiftID).get(0);
        Activity activity = Activity.GetActivitiesForJobPack(jobPack.jobPackID).get(0);
        List<Task> tasks = Task.GetTasksForActivity(activity.activityID);

        for (Task task : tasks) {
            if (TaskTypeEnum.get(task.taskTypeID) == TaskTypeEnum.Checklist) {
                Checklist checklist = Checklist.getChecklist(task.checklistID);
                if (checklist.isJettingRiskAssessment || checklist.isTankeringRiskAssessment || checklist.isCCTVRiskAssessment) {
                    TaskSelectionBase(task, tasks.size(), jobPack.jobPackID, shiftID);
                    return;
                }
            }
        }

        Toast.makeText(getApplicationContext(), "No risk assessment checklist task found in this workflow", Toast.LENGTH_LONG).show();
    }

    private void accidentClick(View view) {
        Intent selectAccidentActivity = new Intent(getApplicationContext(), SelectAccidentActivity.class);
        selectAccidentActivity.putExtra("ShiftID", shiftID);
        startActivity(selectAccidentActivity);
    }

    private void commentClick(View view) {
        Intent selectCommentActivity = new Intent(getApplicationContext(), SelectCommentActivity.class);
        selectCommentActivity.putExtra("ShiftID", shiftID);
        startActivity(selectCommentActivity);
    }

    public void cancelClick(View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(getApplicationContext(), EmergencyCancelActivity.class);
                        intent.putExtra("ShiftID", shiftID);
                        startActivity(intent);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
        builder.setMessage("Continue to cancel shift?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private boolean CheckComplete() {
        try {
            Activity activity = Activity.GetActivitiesForShift(shiftID).get(0);
            List<Task> tasks = Task.GetTasksForActivity(activity.activityID);

            for (int i = 0; i < tasks.size(); i++) {
                Task task = tasks.get(i);
                if (task.isMandatory && task.endTime == null) {
                    return false;
                }
            }
        } catch (Exception e) {
            ErrorLog.CreateError(e, "StartAcitivityCheckComplete");
            return false;
        }

        return true;
    }

    public void deleteOutOfHoursClick(View view) {
        FMConwayUtils.confirmDialog(this, "Delete out of hours shift?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String userName = (sp.getString("Username", ""));
                String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
                DataSync sync = new DataSync(shiftID, userID, url, encodedHeader);
                sync.deleteShift();
                finish();
            }
        });
    }

    public void drawingClick(View view) {
        Activity activity = Activity.GetActivitiesForShift(shiftID).get(0);

        Intent selectDrawingActivity = new Intent(getApplicationContext(), SelectDrawingActivity.class);
        selectDrawingActivity.putExtra("JobPackID", activity.jobPackID);
        selectDrawingActivity.putExtra("ShiftID", shiftID);
        startActivity(selectDrawingActivity);
    }

    public void maintenanceClick(View view) {
        Activity activity = Activity.GetActivitiesForShift(shiftID).get(0);

        Intent selectMaintenanceActivity = new Intent(getApplicationContext(), SelectMaintenanceActivity.class);
        selectMaintenanceActivity.putExtra("ActivityID", activity.getActivityID());
        selectMaintenanceActivity.putExtra("ShiftID", shiftID);
        selectMaintenanceActivity.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(selectMaintenanceActivity);
    }

}
