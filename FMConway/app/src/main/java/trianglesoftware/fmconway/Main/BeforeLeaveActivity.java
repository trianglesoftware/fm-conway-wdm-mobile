package trianglesoftware.fmconway.Main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import trianglesoftware.fmconway.Database.DatabaseObjects.ActivitySignature;
import trianglesoftware.fmconway.Database.DatabaseObjects.Checklist;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftUpdate;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftVehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.Staff;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Database.DatabaseObjects.Vehicle;
import trianglesoftware.fmconway.Database.DatabaseObjects.VehicleChecklist;
import trianglesoftware.fmconway.JobPack.SelectJobPackActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Staff.SelectStaffActivity;
import trianglesoftware.fmconway.Task.TaskDisplayActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Vehicle.ConfirmLoadingSheetActivity;
import trianglesoftware.fmconway.Vehicle.SelectVehicleActivity;

public class BeforeLeaveActivity extends FMConwayActivity {
    private String shiftID;
    private String userID;
    private int vehiclesNumber;
    private ImageView peopleImage;
    private ImageView activityImage;
    private ImageView vehicleChecklistImage;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID");
            userID = extras.getString("UserID");
        }

        super.onCreate(savedInstanceState);
        
        //CheckShiftStatus();

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        
        LinearLayout container = (LinearLayout) findViewById(R.id.container);
        LinearLayout people = (LinearLayout) findViewById(R.id.people_layout);
        LinearLayout shiftActivity = (LinearLayout) findViewById(R.id.shift_activity_layout);
        LinearLayout vehicles = (LinearLayout) findViewById(R.id.vehicles_layout);
        
        // crew member restrictions
        User user = User.getUser(sp.getString("UserID", ""));
        if (Objects.equals(user.employeeType, "Crew Member")) {
            people.setVisibility(View.INVISIBLE);
            shiftActivity.setVisibility(View.INVISIBLE);
            
            // shift vehicles button to top
            container.removeView(vehicles);
            container.addView(vehicles, 0);
        }
        
        List<Vehicle> vehiclesForShift = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
        for (Vehicle vehicle : vehiclesForShift) {
            ShiftUpdate.propagateVehicleChecklistsIfPossible(shiftID, vehicle.vehicleID);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        try{
            CheckShiftStatus();
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "CheckShiftStatus");
        }

        if (ShiftVehicle.GetVehiclesForShift(shiftID, userID).size() == 0)
        {
            vehicleChecklistImage = (ImageView)findViewById(R.id.vehicle_checklist_image);
            vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist_half);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_before_leave;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "In the Yard";
    }

    public void CheckShiftStatus() throws Exception
    {
        peopleImage = (ImageView)findViewById(R.id.people_image);
        activityImage = (ImageView)findViewById(R.id.activity_image);
        vehicleChecklistImage = (ImageView)findViewById(R.id.vehicle_checklist_image);

        List<ShiftStaff> shiftStaff = ShiftStaff.GetShiftStaff(shiftID);
        boolean staffConfirmed = true;
        for (int i = 0; i < shiftStaff.size(); i++)
        {
            if (shiftStaff.get(i).startTime == null)
            {
                staffConfirmed = false;
            }
        }

        if (staffConfirmed)
        {
            peopleImage.setImageResource(R.drawable.people_half);
        }

        List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
        List<trianglesoftware.fmconway.Database.DatabaseObjects.Activity> activities = trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetActivitiesForShift(shiftID);
        int staffSigned = 0;
        boolean allSigned = true;
        for (int i = 0; i < staffToSign.size(); i++)
        {
            for (int j = 0; j < activities.size(); j++) {
                if (ActivitySignature.CheckActivtySignatureExists(activities.get(j).activityID, staffToSign.get(i).staffID)) {
                    staffSigned += 1;
                }
            }
        }
        if(staffSigned < (staffToSign.size() * activities.size()))
        {
            allSigned = false;
        }
        if (allSigned) {
            activityImage.setImageResource(R.drawable.shift_activity_half);
        }

        boolean driverAssigned = true;
        
        List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
        for (int i = 0; i < vehicles.size(); i++) {
            String vehicleID = vehicles.get(i).vehicleID;
            String staffID = vehicles.get(i).staffID;
            int startMileage = vehicles.get(i).startMileage;

            if (TextUtils.isEmpty(staffID)) {
                driverAssigned = false;
            }
        }

        if (driverAssigned) {
            vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist_half);
        } else {
            vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist);
        }

        if (staffConfirmed)
        {
            Shift.SetCompletionStatus(shiftID, 5);
        }

        vehiclesNumber = ShiftVehicle.GetVehiclesForShift(shiftID, userID).size();

        if (vehiclesNumber == 0)
        {
            vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist_half);
        }


//        int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);

//        boolean allSigned = true;

//        if (shiftStatus > 0)
//        {
//            peopleImage.setImageResource(R.drawable.people_half);
//        }
//        if (shiftStatus > 1)
//        {
//            List<Staff> staffToSign = Staff.GetStaffForShift(shiftID);
//            List<trianglesoftware.chevron.Database.DatabaseObjects.Activity> activities = trianglesoftware.chevron.Database.DatabaseObjects.Activity.GetActivitiesForShift(shiftID);
//            int staffSigned = 0;
//
//            for (int i = 0; i < staffToSign.size(); i++)
//            {
//                for (int j = 0; j < activities.size(); j++) {
//                    if (ActivitySignature.CheckActivtySignatureExists(activities.get(j).getActivityID(), staffToSign.get(i).getStaffID())) {
//                        staffSigned += 1;
//                    }
//                }
//            }
//            if(staffSigned < (staffToSign.size() * activities.size()))
//            {
//                allSigned = false;
//            }
//            if (allSigned) {
//                activityImage.setImageResource(R.drawable.shift_activity_half);
//            }
//        }
//        if (shiftStatus > 2 && allSigned)
//        {
//            vehicleChecklistImage.setImageResource(R.drawable.vehicle_checklist_half);
//        }
//        if (shiftStatus > 4 && allSigned)
//        {
//            loadingSheetImage.setImageResource(R.drawable.loading_sheets_half);
//        }
    }

    public void peopleClick(View view)
    {
        //Load staff screen
        Intent selectStaffIntent = new Intent(getApplicationContext(), SelectStaffActivity.class);
        selectStaffIntent.putExtra("ShiftID", shiftID);
        selectStaffIntent.putExtra("UserID", userID);
        startActivity(selectStaffIntent);
    }

    public void activityClick(View view)
    {
        try {
            int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
            if (shiftStatus >= 0) {
                //Load read only list of job packs then activities then task list
                Intent selectJobActivity = new Intent(getApplicationContext(), SelectJobPackActivity.class);
                selectJobActivity.putExtra("ShiftID", shiftID);
                selectJobActivity.putExtra("UserID", userID);
                selectJobActivity.putExtra("FromButton", 2);
                startActivity(selectJobActivity);

                if (shiftStatus == 1) {
                    Shift.SetCompletionStatus(shiftID, 2);
                }

//                if (shiftStatus == 2 && ShiftVehicle.GetVehiclesForShift(shiftID, userID).size() == 0) {
//                    Shift.SetCompletionStatus(shiftID, 5);
//                }
            } else {
                Toast.makeText(getApplicationContext(), "Please confirm staff members before proceeding", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e)
        {
            ErrorLog.CreateError(e, "ActivityClick");
        }
    }

    public void vehicleClick(View view)
    {
        if (vehiclesNumber > 0) {
            try {
                int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
                if (shiftStatus >= 0) {
                    boolean allSigned = true;
                    if (allSigned) {
                        Intent selectVehicleActivity = new Intent(getApplicationContext(), SelectVehicleActivity.class);
                        selectVehicleActivity.putExtra("ShiftID", shiftID);
                        selectVehicleActivity.putExtra("UserID", userID);
                        startActivity(selectVehicleActivity);
                    } else {
                        Toast.makeText(getApplicationContext(), "All staff must confirm and sign the activity details before proceeding", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please view activity details before proceeding", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                ErrorLog.CreateError(e, "VehicleClick");
            }
        }
    }

    public void loadingSheetClick(View view)
    {
        if (vehiclesNumber > 0) {
            try {
                int shiftStatus = Shift.GetShiftCompletionStatus(shiftID);
                if (shiftStatus >= 0) { // > 2
                    boolean checklistsCompleted = true;
//                List<Vehicle> vehicles = ShiftVehicle.GetVehiclesForShift(shiftID, userID);
//                for (int i = 0; i < vehicles.size(); i++) {
//                    List<Checklist> vehicleChecklists = Checklist.GetChecklistsForVehicle(vehicles.get(i).getVehicleID());
//                    for (int j = 0; j < vehicleChecklists.size(); j++) {
//                        if (!VehicleChecklist.CheckIfChecklistCreated(vehicles.get(i).getVehicleID(), shiftID, vehicleChecklists.get(j).getChecklistID(), true)) {
//                            checklistsCompleted = false;
//                        }
//                    }
//                }

                    if (checklistsCompleted) {
                        Intent loadingSheetActivity = new Intent(getApplicationContext(), ConfirmLoadingSheetActivity.class);
                        loadingSheetActivity.putExtra("ShiftID", shiftID);
                        loadingSheetActivity.putExtra("UserID", userID);
                        startActivity(loadingSheetActivity);

                        if (shiftStatus == 3) {
                            Shift.SetCompletionStatus(shiftID, 4);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please complete all vehicle checklists before proceeding.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please confirm all vehicles before proceeding", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                ErrorLog.CreateError(e, "LoadingSheetClick");
            }
        }
    }

}
