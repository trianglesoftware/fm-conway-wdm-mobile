package trianglesoftware.fmconway.Shift;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.content.Intent;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import trianglesoftware.fmconway.BuildConfig;
import trianglesoftware.fmconway.Database.DataDownload;
import trianglesoftware.fmconway.Database.DataSync;
import trianglesoftware.fmconway.Database.DatabaseObjects.ErrorLog;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgress;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftProgressStarted;
import trianglesoftware.fmconway.Database.DatabaseObjects.ShiftStaff;
import trianglesoftware.fmconway.Database.DatabaseObjects.User;
import trianglesoftware.fmconway.Main.MainActivity;
import trianglesoftware.fmconway.Main.NoShiftActivity;
import trianglesoftware.fmconway.Main.StartActivity;
import trianglesoftware.fmconway.OutOfHours.OutOfHoursActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.DebugActivity;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Database.DatabaseObjects.Shift;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;
import trianglesoftware.fmconway.Utilities.MultipleClickHandler;
import trianglesoftware.fmconway.Vehicle.SelectVehicleActivity;

public class SelectShiftActivity extends FMConwayActivityBase implements AdapterView.OnItemClickListener {
    private String userID;
    private boolean runSyncOnResume;
    private String url;
    private SharedPreferences sp;
    private ShiftAdapter shiftAdapter;
    private ProgressDialog uDialog;
    private ProgressDialog mDialog;
    private ProgressDialog sDialog;
    private Toast multipleClickToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_shift);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userID = extras.getString("UserID", "");
            runSyncOnResume = extras.getBoolean("RunSyncOnResume", false);
        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        url = this.getResources().getString(R.string.QUERY_URL);

        ListView shiftList = findViewById(R.id.select_shift_list);
        shiftAdapter = new ShiftAdapter(this, getLayoutInflater());
        shiftList.setAdapter(shiftAdapter);
        shiftList.setOnItemClickListener(this);

        uDialog = new ProgressDialog(this);
        uDialog.setMessage("Uploading data...");
        uDialog.setCancelable(false);
        uDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Downloading new data...");
        mDialog.setCancelable(false);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        sDialog = new ProgressDialog(this);
        sDialog.setMessage("Checking for new shifts...");
        sDialog.setCancelable(false);
        sDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        User user = User.getUser(sp.getString("UserID", ""));
        if (!Objects.equals(user.employeeType, "Crew Member")) {
            Button submitVehicleChecklists = (Button) findViewById(R.id.pushButton);
            submitVehicleChecklists.setVisibility(View.GONE);
        }

        Button noShiftButton = (Button) findViewById(R.id.noShiftButton);
        noShiftButton.setVisibility(View.GONE);

        // open debug menu
        ImageView headerLogo = findViewById(R.id.header_logo);

        if (BuildConfig.BUILD_TYPE == "apptest") {
            headerLogo.setImageResource(R.drawable.fmconway_logo_test);
        }

        new MultipleClickHandler(headerLogo, 5, new MultipleClickHandler.OnMultipleClickListener() {
            @Override
            public void onClick(int click) {
                if (click >= 2) {
                    if (multipleClickToast != null) {
                        multipleClickToast.cancel();
                    }

                    multipleClickToast = Toast.makeText(getApplicationContext(), "Options menu clicked: " + click, Toast.LENGTH_SHORT);
                    multipleClickToast.show();
                }
            }

            @Override
            public void onFinalClick() {
                Intent startIntent = new Intent(getApplicationContext(), DebugActivity.class);
                startActivity(startIntent);
            }
        });
    }

    public void onResume() {
        super.onResume();

        if (runSyncOnResume) {
            // GetData() is called on downloadNewAndUpdateExistingShifts() complete
            downloadNewData();
            runSyncOnResume = false;
        } else {
            try {
                GetData();
            } catch (Exception e) {
                Log.e("FMConway", e.toString());
            }
        }
    }

    public void GetData() throws Exception {
        List<Shift> allShifts = Shift.GetShiftsForUser(userID);
        List<Shift> shifts = new LinkedList<>();

        String currentStaffID = sp.getString("StaffID", "");

        for (int i = 0; i < allShifts.size(); i++) {
            Shift shift = allShifts.get(i);
            List<ShiftStaff> shiftStaffs = ShiftStaff.GetShiftStaff(shift.shiftID);

            // filter shifts to only show shifts up until today
            Date shiftDate = FMConwayUtils.truncateTime(shift.shiftDate);
            Date today = FMConwayUtils.truncateTime(new Date());

            if (shiftDate.after(today)) {
                continue;
            }

            // filter shifts if employee is no longer on shift
            boolean isOnShift = false;
            for (ShiftStaff shiftStaff : shiftStaffs) {
                if (shiftStaff.staffID.equalsIgnoreCase(currentStaffID)) {
                    isOnShift = true;
                    break;
                }
            }

            if (!isOnShift) {
                continue;
            }

            shifts.add(shift);
        }

        // if all shifts complete / cancelled redirect to no shifts activity
        if (shifts.size() == 0) {
            Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
            noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            noShiftIntent.putExtra("ShiftID", "");
            noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
            startActivity(noShiftIntent);
            finish();
            return;
        }

        // order by shift date then by order number
        Collections.sort(shifts, new Comparator<Shift>() {
            @Override
            public int compare(Shift o1, Shift o2) {
                Date shiftDate1 = o1.shiftDate != null ? FMConwayUtils.truncateTime(o1.shiftDate) : new Date();
                Date shiftDate2 = o2.shiftDate != null ? FMConwayUtils.truncateTime(o2.shiftDate) : new Date();

                if (shiftDate1.equals(shiftDate2)) {
                    return Integer.compare(o1.orderNumber, o2.orderNumber);
                } else if (shiftDate1.after(shiftDate2)) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < shifts.size(); i++) {
            Shift shift = shifts.get(i);

            trianglesoftware.fmconway.Database.DatabaseObjects.Activity activity = trianglesoftware.fmconway.Database.DatabaseObjects.Activity.GetActivitiesForShift(shift.shiftID).get(0);
            ShiftProgress shiftProgress = ShiftProgress.GetCurrentShiftProgress(shift.shiftID);

            shift.progress = shiftProgress.progress;

            JSONObject obj = shift.getJSONObject();
            obj.put("Comments", activity.worksInstructionComments);
            obj.put("WorkInstructionNumber", activity.worksInstructionNumber);
            obj.put("Contract", activity.contract);
            obj.put("ContractNumber", activity.contractNumber);

            jsonArray.put(obj);
        }

        shiftAdapter.UpdateData(jsonArray);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final String shiftID = (String) view.findViewById(R.id.details).getTag();

        if (!ShiftProgress.DoesProgressExist(shiftID, "ChecksCompleted")) {
            final ShiftProgressStarted shiftProgress = new ShiftProgressStarted();
            shiftProgress.shiftID = shiftID;
            shiftProgress.sent = true;
            shiftProgress.progress = "ChecksCompleted";
            shiftProgress.createdDate = new Date();
            shiftProgress.shiftUpdatedDate = Shift.getShiftUpdatedDate(shiftID);

            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Confirmation");
            alertDialog.setMessage("Do you wish to start this task? If you select yes your operations team will not be able to change the vehicles or crew after this point.");

            alertDialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ShiftProgress.PushShiftProgressStarted(shiftProgress, new DataSync.PushDataListenerProgressResponse() {
                        @Override
                        public void onComplete(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                String result = obj.getString("Result");

                                final String text;

                                if (result != null) {
                                    if (result.equals("Unconfirmed")) {
                                        text = "This task is currently being updated. Contact your operations manager.";
                                    } else if (result.equals("Outdated")) {
                                        text = "This task has been updated, click 'download shifts' to get latest update.";
                                    } else if (result.equals("Error")) {
                                        // save and continue if there is an unexpected error
                                        text = null;
                                    } else {
                                        text = null;
                                    }
                                } else {
                                    text = null;
                                }

                                if (text != null) {
                                    SelectShiftActivity.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            FMConwayUtils.warningDialog(SelectShiftActivity.this, text, null);
                                        }
                                    });
                                } else {
                                    ShiftProgress.saveShiftProgress(shiftProgress);
                                    startShiftWithFinishDateCheck(shiftID);
                                }
                            } catch (Exception e) {
                                ErrorLog.CreateError(e, "PushShiftProgressStarted", response);
                                ShiftProgress.saveShiftProgress(shiftProgress);
                                startShiftWithFinishDateCheck(shiftID);
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            ErrorLog.CreateError(t, "PushShiftProgressStarted");
                            ShiftProgress.saveShiftProgress(shiftProgress);
                            startShiftWithFinishDateCheck(shiftID);
                        }
                    });
                }
            });
            alertDialog.setNegativeButton("Cancel", null);

            AlertDialog alert = alertDialog.create();
            alert.show();
        } else {
            startShiftWithFinishDateCheck(shiftID);
        }
    }

    // warn user if now > shift start date + 14 hour
    private void startShiftWithFinishDateCheck(final String shiftID) {
        Date shiftDate = Shift.GetShiftDate(shiftID);

        Calendar shiftDateAddForteenHours = Calendar.getInstance();
        shiftDateAddForteenHours.setTime(shiftDate);
        shiftDateAddForteenHours.add(Calendar.HOUR, 14);

        Date now = new Date();

        if (shiftDateAddForteenHours.getTime().before(now)) {
            String shiftDateString = FMConwayUtils.getLongDateStringWithoutSeconds(shiftDate);

            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Confirmation");
            alertDialog.setMessage("This task was scheduled for " + shiftDateString + ".\nAre you sure you want to open this task?\n\nAlternatively click 'Download Shifts' to download any new tasks");

            alertDialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    openShift(shiftID);
                }
            });
            alertDialog.setNegativeButton("Cancel", null);

            AlertDialog alert = alertDialog.create();
            alert.show();
        } else {
            openShift(shiftID);
        }
    }

    private void openShift(String shiftID) {
        User user = User.getUser(sp.getString("UserID", ""));
        if (Objects.equals(user.employeeType, "Crew Member")) {
            Intent selectVehicleActivity = new Intent(getApplicationContext(), SelectVehicleActivity.class);
            selectVehicleActivity.putExtra("ShiftID", shiftID);
            selectVehicleActivity.putExtra("UserID", userID);
            startActivity(selectVehicleActivity);
        } else {
            Intent startIntent = new Intent(getApplicationContext(), StartActivity.class);
            startIntent.putExtra("ShiftID", shiftID);
            startIntent.putExtra("UserID", userID);
            startActivity(startIntent);
        }
    }

    public void logoutClick(View view) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainActivity);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
        builder.setMessage("Continue to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void syncClick(View view) {
        User user = User.getUser(sp.getString("UserID", ""));
        boolean isCrewMember = Objects.equals(user.employeeType, "Crew Member");

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Low Signal");
            alertDialog.setMessage("Please ensure you have a Wi-Fi or 4G signal before submitting your data.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.create().show();
            return;
        }

        String message = "Download latest shifts. Do you wish to continue?";

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                downloadNewData();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        alertDialog.create().show();
    }

    public void pushClick(View view) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog);
            alertDialog.setTitle("Low Signal");
            alertDialog.setMessage("Please ensure you have a Wi-Fi or 4G signal before submitting your data.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.create().show();
            return;
        }

        final Activity base = this;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Base_Theme_AppCompat_Light_Dialog));
        alertDialog.setMessage("Upload vehicle checklists. Do you wish to continue?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                uDialog.show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<Shift> shifts = Shift.GetShiftsForUser(userID);
                        for (int j = 0; j < shifts.size(); j++) {
                            String shiftID = shifts.get(j).shiftID;
                            pushAandO(shiftID);
                        }
                    }
                }).start();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        alertDialog.create().show();
    }

    public void noShiftClick(View view) {
        Intent noShiftIntent = new Intent(getApplicationContext(), NoShiftActivity.class);
        noShiftIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        noShiftIntent.putExtra("ShiftID", "");
        noShiftIntent.putExtra("UserID", sp.getString("UserID", ""));
        noShiftIntent.putExtra("HasShifts", true);
        startActivity(noShiftIntent);
    }

    public void pushAandO(String shiftID) {
        String userName = (sp.getString("Username", ""));
        String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        final Activity base = this;

        // push accidents, observations, vehicle checklists and nominated driver
        DataSync sync = new DataSync(shiftID, sp.getString("UserID", ""), url, encodedHeader);
        sync.PushAandO(new DataSync.PushDataListener() {
            @Override
            public void onComplete(final boolean errors) {
                base.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        uDialog.dismiss();
                        if (errors) {
                            Toast.makeText(getApplicationContext(), "An error occurred - Please try again.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Upload complete.", Toast.LENGTH_SHORT).show();
                            downloadNewData();
                        }
                    }
                });
            }

            @Override
            public void onError(final Throwable t) {
                base.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        uDialog.dismiss();
                        Log.e("FMConway", "SelectShiftActivity " + t.getMessage());
                        Toast.makeText(getApplicationContext(), "An error occurred - Please try again.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    // similar to MainActivity.downloadNewData
    private void downloadNewData() {
        String userName = (sp.getString("Username", ""));
        String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

        String QUERY_URL = this.getResources().getString(R.string.QUERY_URL);

        mDialog.show();

        final Context ctx = this;

        // used to get an early date (1970-01-01)
        Date syncDate = new Date(sp.getLong("nonExistingKey", 0));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String dateString = dateFormat.format(syncDate);

        final DataDownload dd = new DataDownload(QUERY_URL, userID, dateString, encodedHeader, getApplicationContext());
        dd.DownloadNewData(mDialog, new DataDownload.DataDownloadListener() {
            @Override
            public void onComplete(boolean errors) {
                // onResume is called twice for some reason, causing GetData to potentially redirect user to NoShifts page before this onComplete is ran,
                // this in turn causes mDialog to throw IllegalArgumentException not attached to window manager, so check if activity is finishing and if so return
                //if (SelectShiftActivity.this.isFinishing()) {
                //    return;
                //}

                mDialog.dismiss();

                //SyncDate should be updated when downloadNewData or DownloadNewShifts completes
                Date date = new Date(System.currentTimeMillis());
                SharedPreferences.Editor editor = sp.edit();
                editor.putLong("SyncDate", date.getTime());
                editor.apply();

                if (errors) {
                    Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();
                }

                try {
                    GetData();
                } catch (Exception e) {
                }
            }

            @Override
            public void onError() {
                Toast.makeText(getApplicationContext(), "Download error, please try again.", Toast.LENGTH_LONG).show();
                mDialog.dismiss();

                try {
                    GetData();
                } catch (Exception e) {
                }
            }
        });
    }

    // download new shifts
    private void downloadNewShifts() {
        String userName = (sp.getString("Username", ""));
        String encodedHeader = "Basic " + Base64.encodeToString((userName + ":" + sp.getString(userName, "")).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

        Date syncDate = new Date(sp.getLong("SyncDate", 0));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String dateString = dateFormat.format(syncDate);

        String QUERY_URL = this.getResources().getString(R.string.QUERY_URL);

        final DataDownload dd = new DataDownload(QUERY_URL, userID, dateString, encodedHeader, getApplicationContext());
        dd.DownloadNewShifts(mDialog, new DataDownload.DataDownloadListener() {
            @Override
            public void onComplete(boolean errors) {
                mDialog.dismiss();

                //SyncDate should be updated when downloadNewData or DownloadNewShifts completes
                Date date = new Date(System.currentTimeMillis());
                SharedPreferences.Editor editor = sp.edit();
                editor.putLong("SyncDate", date.getTime());
                editor.apply();

                try {
                    GetData();
                } catch (Exception e) {
                }
            }

            @Override
            public void onError() {
                mDialog.dismiss();

                Toast.makeText(getApplicationContext(), "An error occurred whilst downloading new shifts", Toast.LENGTH_LONG).show();

                try {
                    GetData();
                } catch (Exception e) {
                }
            }
        });
    }

    public void oohClick(View view) {
        /*Intent intent = new Intent(getApplicationContext(), SelectOutOfHoursActivity.class);
        intent.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(intent);*/

        Intent intent = new Intent(getApplicationContext(), OutOfHoursActivity.class);
        intent.putExtra("UserID", sp.getString("UserID", ""));
        startActivity(intent);
    }
}
