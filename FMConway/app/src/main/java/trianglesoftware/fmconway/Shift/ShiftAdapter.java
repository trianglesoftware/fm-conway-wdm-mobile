package trianglesoftware.fmconway.Shift;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayApp;
import trianglesoftware.fmconway.Utilities.FMConwayUtils;

/**
 * Created by Adam.Patrick on 27/02/2017.
 */

class ShiftAdapter extends BaseAdapter{
    private JSONArray mJsonArray;
    private final LayoutInflater mInflater;

    public ShiftAdapter(Context context, LayoutInflater inflater)
    {
        mInflater = inflater;
        mJsonArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        return mJsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_shift, null);

            holder = new ViewHolder();
            holder.DetailsContainerLayout = (LinearLayout)convertView.findViewById(R.id.details_container);
            holder.DetailsTextView = (TextView)convertView.findViewById(R.id.details);
            holder.WorkInstructionNumberTextView = (TextView)convertView.findViewById(R.id.work_instruction_number);
            holder.AddressTextView = (TextView)convertView.findViewById(R.id.address);
            holder.ActivityTextView = (TextView)convertView.findViewById(R.id.traffic_management_activities);
            holder.SRWNumberTextView = (TextView)convertView.findViewById(R.id.srw_number);
            holder.ContractTextView = (TextView)convertView.findViewById(R.id.contract);
            holder.JobTypeTextView = (TextView)convertView.findViewById(R.id.job_type);
            holder.JobCustomerProjectCodeTextView = (TextView)convertView.findViewById(R.id.job_proj_code);
            holder.TaskTypeTextView = (TextView)convertView.findViewById(R.id.task_type);
            holder.TaskDetailsTextView = (TextView)convertView.findViewById(R.id.task_details);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);
        
        String shiftDetails = jsonObject.optString("ShiftDetails", "");
        String shiftID = jsonObject.optString("ShiftID", "");
        String progress = jsonObject.optString("Progress", "");
        String address = jsonObject.optString("Address", "");
        String trafficManagementActivities = jsonObject.optString("TrafficManagementActivities", "");
        String srwNumber = jsonObject.optString("SRWNumber", "");
        String shiftTypes = jsonObject.optString("ShiftTypes", "");
        String projCode = jsonObject.optString("ProjCode", "");
        String activityType = jsonObject.optString("ActivityType", "");
        String taskDetails = jsonObject.optString("Comments", "");
        String workInstructionNumber = jsonObject.optString("WorkInstructionNumber", "");

        String contractTitle = jsonObject.optString("Contract", "");
        String contractNumber = jsonObject.optString("ContractNumber", "");
        String contractSeperator = !contractTitle.isEmpty() && !contractNumber.isEmpty() ? " / " : "";
        String contract = (contractTitle + contractSeperator + contractNumber).trim();

        holder.DetailsTextView.setText(shiftDetails);
        holder.DetailsTextView.setTag(shiftID);

        // if mobile device, split progress onto new line
        try {
            boolean isTablet = FMConwayUtils.isTablet(FMConwayApp.getContext());
            if (!isTablet) {
                String[] splits = shiftDetails.split(" - ");
                holder.DetailsTextView.setText(splits[0] + " - " + splits[1] + "\n" + splits[2]);
            }
        }
        catch (Exception ex) {
            // ignore split
        }

        holder.WorkInstructionNumberTextView.setText(workInstructionNumber);

        holder.AddressTextView.setText("Address: " + address);
        holder.ActivityTextView.setText("Activity: " + trafficManagementActivities);
        holder.SRWNumberTextView.setText("SRW Number: " + srwNumber);
        holder.ContractTextView.setText("Contract: " + contract);
        holder.JobTypeTextView.setText("Job Type: " + shiftTypes);
        holder.JobCustomerProjectCodeTextView.setText("Customer Project Code: " + projCode);
        holder.TaskTypeTextView.setText("Task Type: " + activityType);
        holder.TaskDetailsTextView.setText("Task Details: " + taskDetails);

        switch (progress){
            case "NotStarted":
                holder.DetailsContainerLayout.setBackgroundColor(Color.parseColor("#fed6d6"));
                break;
            case "ChecksCompleted":
                holder.DetailsContainerLayout.setBackgroundColor(Color.parseColor("#f8fecc"));
                break;
            case "TasksStarted":
                holder.DetailsContainerLayout.setBackgroundColor(Color.parseColor("#bcc6ff"));
                break;
            case "Completed":
                holder.DetailsContainerLayout.setBackgroundColor(Color.parseColor("#d2ffd4"));
                break;
            default:
                break;
        }
        
        if (TextUtils.isEmpty(address)) {
            holder.AddressTextView.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(contract)) {
            holder.ContractTextView.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(projCode)) {
            holder.JobCustomerProjectCodeTextView.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(trafficManagementActivities)) {
            holder.ActivityTextView.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(srwNumber)) {
            holder.SRWNumberTextView.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(shiftTypes)) {
            holder.JobTypeTextView.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(taskDetails)) {
            holder.TaskDetailsTextView.setVisibility(View.GONE);
        }
        
        return convertView;
    }

    public void UpdateData(JSONArray jsonArray) {
        mJsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public LinearLayout DetailsContainerLayout;
        public TextView DetailsTextView;
        public TextView WorkInstructionNumberTextView;
        public TextView AddressTextView;
        public TextView ActivityTextView;
        public TextView SRWNumberTextView;
        public TextView ContractTextView;
        public TextView JobTypeTextView;
        public TextView JobCustomerProjectCodeTextView;
        public TextView TaskTypeTextView;
        public TextView TaskDetailsTextView;
    }
}
