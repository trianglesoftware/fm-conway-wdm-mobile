package trianglesoftware.fmconway.LoadingSheetEquipment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentItemChecklistAnswer;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipmentItem;
import trianglesoftware.fmconway.EquipmentItemChecklist.EquipmentItemChecklistActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.FMConwayActivity;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;

public class LoadingSheetEquipmentActivity extends FMConwayActivity {
    /*intents*/
    private String shiftID;
    private String userID;
    private String loadingSheetEquipmentID;
    private int quantity;
    private String checklistID;
    
    /*other*/
    private ListView loadingSheetEquipmentList;
    private LoadingSheetEquipmentAdapter loadingSheetEquipmentAdapter;
    private Button copy_checklist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shiftID = extras.getString("ShiftID", "");
            userID = extras.getString("UserID", "");
            loadingSheetEquipmentID = extras.getString("LoadingSheetEquipmentID", "");
            quantity = extras.getInt("Quantity", 0);
            checklistID = extras.getString("ChecklistID", "");
        }

        super.onCreate(savedInstanceState);

        copy_checklist = (Button) findViewById(R.id.copy_checklist);

        loadingSheetEquipmentList = (ListView) findViewById(R.id.loading_sheet_equipment_list);
        loadingSheetEquipmentAdapter = new LoadingSheetEquipmentAdapter(this, getLayoutInflater());
        loadingSheetEquipmentList.setAdapter(loadingSheetEquipmentAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_loading_sheet_equipment;
    }

    @Override
    protected String setShiftID() {
        return shiftID;
    }

    @Override
    protected String setHeader() {
        return "Loading Sheet Equipment";
    }

    @Override
    public void onResume() {
        super.onResume();
        
        List<LoadingSheetEquipmentItem> items = LoadingSheetEquipmentItem.get(loadingSheetEquipmentID);
        loadingSheetEquipmentAdapter.UpdateData(items);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    
    public void checklist_Click(View view) {
        LoadingSheetEquipmentAdapter.ViewHolder viewHolder = (LoadingSheetEquipmentAdapter.ViewHolder)view.getTag();

        Intent intent = new Intent(getApplicationContext(), EquipmentItemChecklistActivity.class);
        intent.putExtra("ShiftID", shiftID);
        intent.putExtra("UserID", userID);
        intent.putExtra("LoadingSheetEquipmentItemID", viewHolder.LoadingSheetEquipmentItemID);
        intent.putExtra("ChecklistID", viewHolder.ChecklistID);
        startActivity(intent);
    }
    
    public void copyChecklist_Click(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Copy Checklist");
        builder.setMessage("This checklists answers will be applied to all other checklists. Continue?");
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String loadingSheetEquipmentItemID = null;
                for (int i = 0; i < loadingSheetEquipmentList.getChildCount(); i++) {
                    View row = loadingSheetEquipmentList.getChildAt(i);
                    LoadingSheetEquipmentAdapter.ViewHolder viewHolder = (LoadingSheetEquipmentAdapter.ViewHolder) row.getTag();
                    if (viewHolder.IsSelected) {
                        loadingSheetEquipmentItemID = viewHolder.LoadingSheetEquipmentItemID;
                        break;
                    }
                }
                
                copyChecklist(loadingSheetEquipmentItemID);

                List<LoadingSheetEquipmentItem> items = LoadingSheetEquipmentItem.get(loadingSheetEquipmentID);
                loadingSheetEquipmentAdapter.UpdateData(items);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);

        AlertDialog dialog = builder.create();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.show();
    }
    
    private void copyChecklist(String loadingSheetEquipmentItemID) {
        List<EquipmentItemChecklistAnswer> newAnswers = EquipmentItemChecklistAnswer.getAnswers(loadingSheetEquipmentItemID);
        
        for (int i = 0; i < loadingSheetEquipmentList.getChildCount(); i++) {
            View row = loadingSheetEquipmentList.getChildAt(i);
            LoadingSheetEquipmentAdapter.ViewHolder viewHolder = (LoadingSheetEquipmentAdapter.ViewHolder) row.getTag();
            
            if (viewHolder.LoadingSheetEquipmentItemID != loadingSheetEquipmentItemID) {
                List<EquipmentItemChecklistAnswer> oldAnswers = EquipmentItemChecklistAnswer.getAnswers(viewHolder.LoadingSheetEquipmentItemID);
                
                for (int a = 0; a < oldAnswers.size(); a++) {
                    boolean answerFound = false;
                    EquipmentItemChecklistAnswer oldAnswer = oldAnswers.get(a);
                    
                    for (int b = 0; b < newAnswers.size(); b++) {
                        EquipmentItemChecklistAnswer newAnswer = newAnswers.get(b);
                        
                        if (oldAnswer.ChecklistQuestionID.equals(newAnswer.ChecklistQuestionID)) {
                            oldAnswer.Answer = newAnswer.Answer;
                            oldAnswer.Reason = newAnswer.Reason;
                            answerFound = true;
                            break;
                        }
                    }
                    if (!answerFound) {
                        oldAnswer.Answer = null;
                    }

                    oldAnswer.updateAnswer();
                    oldAnswer.updateReason();
                }
            }
        }
    }
    
    public void loadingSheetEquipmentRow_Click(View view) { 
        LinearLayout linearLayout = (LinearLayout)view;
        LoadingSheetEquipmentAdapter.ViewHolder viewHolder = (LoadingSheetEquipmentAdapter.ViewHolder)linearLayout.getTag();
        
        /*deselect other rows*/
        for (int i = 0; i < loadingSheetEquipmentList.getChildCount(); i++) {
            View row = loadingSheetEquipmentList.getChildAt(i);
            if (row != view) {
                LoadingSheetEquipmentAdapter.ViewHolder viewHolder1 = (LoadingSheetEquipmentAdapter.ViewHolder) row.getTag();
                if (viewHolder1.IsSelected) {
                    viewHolder1.IsSelected = false;
                    SetBackgroundColour((LinearLayout)row, -1);
                }
            }
        }
        
        /*select / deselect current row*/
        if(viewHolder.IsSelected) {
            viewHolder.IsSelected = false;
            SetBackgroundColour(linearLayout, -1);
            copy_checklist.setBackgroundColor(getResources().getColor(R.color.fmconway_disabled));
            copy_checklist.setEnabled(false);
        } else {
            viewHolder.IsSelected = true;
            SetBackgroundColour(linearLayout, getResources().getColor(R.color.fmconway_yellow));
            copy_checklist.setBackgroundColor(getResources().getColor(R.color.fmconway_green));
            copy_checklist.setEnabled(true);
        }
    }
    
    public void SetBackgroundColour(LinearLayout linearLayout, int colour) {
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            View view = linearLayout.getChildAt(i);
            if (view.getClass().equals(TextView.class)) {
                view.setBackgroundColor(colour);
            } else if (view instanceof LinearLayout) {
                view.setBackgroundColor(colour);
                SetBackgroundColour((LinearLayout)view, colour);
            }
        }
    }
    
}
