package trianglesoftware.fmconway.LoadingSheetEquipment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import trianglesoftware.fmconway.Database.DatabaseObjects.Equipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.EquipmentType;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheet;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipment;
import trianglesoftware.fmconway.Database.DatabaseObjects.Task;
import trianglesoftware.fmconway.Database.DatabaseObjects.TaskEquipment;
import trianglesoftware.fmconway.LoadingSheet.LoadingSheetActivity;
import trianglesoftware.fmconway.R;
import trianglesoftware.fmconway.Utilities.CustomAutoCompleteTextChangedListener;
import trianglesoftware.fmconway.Utilities.CustomAutoCompleteView;
import trianglesoftware.fmconway.Utilities.ExceptionHandler;
import trianglesoftware.fmconway.Utilities.FMConwayActivityBase;

public class SelectNewLoadingSheetEquipDialogActivity extends FMConwayActivityBase {

    public CustomAutoCompleteView newItem;
    private String shiftID;
    private String userID;
    private String loadingSheetID;
    private String vehicleID;
    public String[] item = new String[] {"Please search..."};
    public ArrayAdapter<String> myAdapter;
    public EditText quantityEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_select_new_equip_dialog);
        quantityEditText = (EditText) findViewById(R.id.quantity);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            shiftID = extras.getString("ShiftID");
            userID = extras.getString("UserID");
            loadingSheetID = extras.getString("LoadingSheetID");
            vehicleID = extras.getString("VehicleID");
        }

        newItem = (CustomAutoCompleteView)findViewById(R.id.autoComplete_NewItem);
        newItem.setThreshold(1);
        newItem.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this, 3));

        myAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, item);
        newItem.setAdapter(myAdapter);
    }

    public String[] getItemsFromDb(String searchTerm) {
        // add items on the array dynamically
        List<Equipment> equipment = Equipment.GetAllEquipment(searchTerm);
        int rowCount = equipment.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (Equipment record : equipment) {

            item[x] = record.getName();
            x++;
        }

        return item;
    }

    public void submitItem(View view) {
        String quantText = quantityEditText.getText().toString();
        if (quantText.length() > 0) {
            Integer quantity = (Integer) Integer.parseInt(quantText);
            Equipment equipment = Equipment.GetEquipmentFromName(newItem.getText().toString());
            if (equipment.equipmentID != null) {
                //check if loading Sheet exists
                if (loadingSheetID.length() == 0) {
                    LoadingSheet ls = new LoadingSheet();
                    ls.loadingSheetID = java.util.UUID.randomUUID().toString();
                    ls.vehicleID = vehicleID;
                    ls.shiftID = shiftID;

                    LoadingSheet.addLoadingSheet(ls);
                    loadingSheetID = ls.loadingSheetID;
                }
                LoadingSheetEquipment lsEquipment = new LoadingSheetEquipment();
                lsEquipment.loadingSheetEquipmentID = java.util.UUID.randomUUID().toString();
                lsEquipment.equipmentID = equipment.equipmentID;
                lsEquipment.loadingSheetID = loadingSheetID;
                lsEquipment.shiftID = shiftID;
                lsEquipment.quantity = quantity;
                lsEquipment.sheetOrder = LoadingSheetEquipment.GetNextSheetOrder(loadingSheetID);
                lsEquipment.vmsOrAsset = EquipmentType.getVmsOrAsset(equipment.equipmentTypeID).vmsOrAsset;

                LoadingSheetEquipment.addLoadingSheetEquipment(lsEquipment);

                Task InstallDeinstallTask = Task.InstallDeinstallTask(shiftID);

                if (InstallDeinstallTask.taskID != null) {
                    TaskEquipment t = new TaskEquipment();
                    t.TaskEquipmentID = java.util.UUID.randomUUID().toString();
                    t.ShiftID = shiftID;
                    t.TaskID = InstallDeinstallTask.taskID;
                    t.EquipmentID = equipment.equipmentID;
                    t.Quantity = quantity;
                    t.UserID = userID;

                    TaskEquipment.addTaskEquipment(t);
                }

                Intent loadingSheet = new Intent(getApplicationContext(), LoadingSheetActivity.class);
                loadingSheet.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                loadingSheet.putExtra("VehicleID", vehicleID);
                loadingSheet.putExtra("ShiftID", shiftID);
                loadingSheet.putExtra("UserID", userID);
                loadingSheet.putExtra("LoadingSheetID", loadingSheetID);
                startActivity(loadingSheet);

            } else {
                Toast.makeText(getApplicationContext(), "Invalid Equipment name", Toast.LENGTH_LONG).show();
            }
        } else{
            Toast.makeText(getApplicationContext(), "Please add a Quantity", Toast.LENGTH_LONG).show();
        }
    }

    public void cancelItem(View view)
    {
        Intent loadingSheet = new Intent(getApplicationContext(), LoadingSheetActivity.class);
        loadingSheet.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        loadingSheet.putExtra("VehicleID", vehicleID);
        loadingSheet.putExtra("ShiftID", shiftID);
        loadingSheet.putExtra("UserID", userID);
        loadingSheet.putExtra("LoadingSheetID", loadingSheetID);
        startActivity(loadingSheet);
    }
}
