package trianglesoftware.fmconway.LoadingSheetEquipment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;
import trianglesoftware.fmconway.Database.DatabaseObjects.LoadingSheetEquipmentItem;
import trianglesoftware.fmconway.R;

import static android.app.PendingIntent.getActivity;

class LoadingSheetEquipmentAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private List<LoadingSheetEquipmentItem> items;

    public LoadingSheetEquipmentAdapter(Context context, LayoutInflater inflater) {
        this.inflater = inflater;
        items = new LinkedList<LoadingSheetEquipmentItem>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public LoadingSheetEquipmentItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LoadingSheetEquipmentItem item = getItem(position);

        ViewHolder view;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_loading_sheet_equipment, null);

            view = new ViewHolder();
            view.ItemNumber = (TextView) convertView.findViewById(R.id.text_item_number);
            view.EquipmentName = (TextView) convertView.findViewById(R.id.text_equipment_name);
            view.Checklist = (Button) convertView.findViewById(R.id.text_checklist);
            view.Checklist.setTag(view);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }
        
        view.LoadingSheetEquipmentItemID = item.LoadingSheetEquipmentItemID;
        view.ChecklistID = item.ChecklistID;
        view.ItemNumber.setText(String.valueOf(item.ItemNumber));
        view.EquipmentName.setText(item.EquipmentName);

        if (item.UnansweredQuestions > 0) {
            view.Checklist.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_yellow));
            view.Checklist.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_black));
        } else {
            view.Checklist.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.fmconway_blue));
            view.Checklist.setTextColor(convertView.getContext().getResources().getColor(R.color.fmconway_text_light));
        }

        return convertView;
    }

    public void UpdateData(List<LoadingSheetEquipmentItem> loadingSheetEquipmentItems) {
        this.items = loadingSheetEquipmentItems;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        public String LoadingSheetEquipmentItemID;
        public String ChecklistID;
        public boolean IsSelected;
        
        public TextView ItemNumber;
        public TextView EquipmentName;
        public Button Checklist;
    }
}
